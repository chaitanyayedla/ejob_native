package com.goodyear.ejob;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goodyear.ejob.adapter.EJobPagerAdapter;
import com.goodyear.ejob.blutooth.BluetoothService;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.DBModel;
import com.goodyear.ejob.fragment.EjobAdditionalServicesFragment;
import com.goodyear.ejob.fragment.EjobInformationFragment;
import com.goodyear.ejob.fragment.EjobSignatureBoxFragment;
import com.goodyear.ejob.fragment.EjobTimeLocationFragment;
import com.goodyear.ejob.fragment.EjobViewPager;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.BluetoothDialogActivityConnector;
import com.goodyear.ejob.interfaces.IEjobFragmentVisible;
import com.goodyear.ejob.interfaces.IFragmentCommunicate;
import com.goodyear.ejob.interfaces.ISaveIconEjobFragment;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.job.operation.helpers.EditJob;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.job.operation.helpers.PressureCheck;
import com.goodyear.ejob.job.operation.helpers.PressureSwap;
import com.goodyear.ejob.job.operation.helpers.Retorque;
import com.goodyear.ejob.job.operation.helpers.SaveJob;
import com.goodyear.ejob.job.operation.helpers.SwapPressureSavedDataOnStop;
import com.goodyear.ejob.ui.SafeDatePickerDialog;
import com.goodyear.ejob.ui.SafeTimePickerDialog;
import com.goodyear.ejob.util.CameraUtility;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.EjobSaveIconStates;
import com.goodyear.ejob.util.InspectionState;
import com.goodyear.ejob.util.Job;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.ReservePWTUtils;
import com.goodyear.ejob.util.TireActionType;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.TyreState;
import com.goodyear.ejob.util.Validation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class EjobFormActionActivity extends FragmentActivity implements
		InactivityHandler, VehicleSkeletonFragment.PressureDialogCreated,
		BluetoothDialogActivityConnector, ISaveIconEjobFragment {

	private static final String SAVE_ICON_STATE = "save_icon_state";
	private EjobViewPager mViewPager;
	private String mBackPressMessage;
	public static EJobPagerAdapter sViewAdapter;
	private static Context context;
	private static String sReceivedDateErrorMessage;
	private static String sStartedDateErrorMessage;
	private static String sReachedDateErrorMessage;
	public static ArrayList<Tyre> sTyreInfo;
	public static String sJobID;
	private static DBModel sDb;
	private static String sLocationErrorMessage;
	public static boolean bool = false;
	/** Alert dialog */
	private AlertDialog mAlertDialog;
	private String mYesLabel;
	private String mNoLabel;
	private static String mJobsaved;
	/**
	 * Final variable for Ejob Signature Box NOTES_INTENT
	 */
	public static final int Ejob_SIGNATURE_NOTES_INTENT = 105;

	private PressureCheck mPressureDialog;
	private PressureSwap mPressureDialogForSwap;
	private final String TAG = "EjobFormActionActivity";
	private boolean mSkipDialogCancelation;
	private SwapPressureSavedDataOnStop mSwapSavedDataOnStop;
	private static boolean mStopErrorMessageIfSavingFromBackPress;
	private int currentPage;
	private int mSaveIconState = EjobSaveIconStates.ICON_GRAY;
	private static String Log_TAG = "Ejob Form Action Activity";

	//CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
	//Message for Wrong Odometer Value
	public static String MSG_WRONG_ODO_VALUE = "Wrong OdoMeter Value";

	//User Trace logs trace tag
	private static final String TRACE_TAG = "Ejob Form";
	private String Last_Recorded_Odometer_Reading;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ejob_form_action);
		// if comes from update then set the job id
		// from DB

		//User Trace logs
		LogUtil.TraceInfo(TRACE_TAG,"none",TRACE_TAG,false,false,true);
		Last_Recorded_Odometer_Reading= "Last Recorded Odometer Reading";
		context = this;
		Constants.IS_FORM_ELEMENTS_CLEANED = false;
		if (savedInstanceState == null) {
			if (getIntent() != null) {
				boolean comesFromUpdate = getIntent().hasExtra(
						EditJob.KEY_COMES_FROM_EDIT);
				if (Constants.COMESFROMUPDATE) {
					Constants.COMESFROMUPDATE = comesFromUpdate;
					sJobID = Constants.jobIdEdit;
				} else {
					sJobID = String.valueOf(UUID.randomUUID());
				}
			}
			// variable to maintain gps lock
			Constants.GPS_LOCK = false;
			EjobTimeLocationFragment.DATE_TIME_SAVED = Constants.TIME_LOCATION_SET;
			Constants.TIME_LOCATION_SET = false;
			if (Constants.COMESFROMVIEW) {
				boolean isAddtionalServicePerformed = false;
				if (Constants.JOB_ITEMLIST != null
						&& Constants.JOB_ITEMLIST.size() > 0) {
					for (JobItem jobItem : Constants.JOB_ITEMLIST) {
						if (TireActionType.SERVICEMATERIAL.equals(jobItem
								.getActionType())) {
							isAddtionalServicePerformed = true;
							break;
						}
					}
				}
				if (isAddtionalServicePerformed) {
					Constants.NOADDITIONALSERVICES = false;
					EJobPagerAdapter.count = 4;
				} else {
					Constants.NOADDITIONALSERVICES = true;
					/*Jira EJOB-63 : Changing adapter pager count from 3 to 4, as we show summary page for
					* a completed job while viewing it*/
					EJobPagerAdapter.count = 4;
				}
			} else {
				EJobPagerAdapter.count = 2;
			}
			getSupportFragmentManager().beginTransaction()
					.add(R.id.viewpagercontainer, new PlaceholderFragment())
					.commit();
		} else {
			mSaveIconState = savedInstanceState.getInt(SAVE_ICON_STATE);
			if (savedInstanceState.getBoolean("save") && mSaveIconState == EjobSaveIconStates.ICON_BLACK) {
				EjobInformationFragment.finalSaveButton();
				EjobTimeLocationFragment.finalSaveButton();
			}else if(mSaveIconState == EjobSaveIconStates.ICON_RED){
				EjobInformationFragment.enableSaveButton();
				EjobTimeLocationFragment.enableSaveButton();
			}
			if (savedInstanceState.getBoolean("retorqueStarted")) {
				Retorque retorque = new Retorque(Constants.SELECTED_TYRE, this);
				VehicleSkeletonFragment.mRetroqueStarted = true;

				int pos = savedInstanceState.getInt("currentposition");
				if (pos == 3) {
					Retorque.retorqueDone = savedInstanceState
							.getBoolean("retorquedone");
					if (!Retorque.retorqueDone) {
						Retorque.loadSavedVaues = true;
						Retorque.retorqueValueSaved = savedInstanceState
								.getInt("retorqueval");
						Retorque.wrenchNumberSaved = savedInstanceState
								.getString("wrenchval");
						Retorque.retorqueLabelIssuedSaved = savedInstanceState
								.getBoolean("labelissued");

						retorque.startTorqueSettingsDialog();
					}
				} else if (pos == 1) {
					retorque.startWasWheelRemoved();
				} else if (pos == 2) {
					retorque.startRecommendTorqueSettingsAppliedDialog();
				} else if(pos == 4)
				{
					retorque.startAreTheTorqueSettingsTheSameForEachAxle();
				}
			}
			if (savedInstanceState.getBoolean("torstarted")) {
				// if third dialog box is open store values
				PressureCheck.axlePosSaved = savedInstanceState
						.getInt("axlepos");
				PressureCheck.axlePressureSaved = savedInstanceState
						.getFloat("axlepressure");

				PressureCheck.loadSavedPressure = savedInstanceState
						.getBoolean("dialogset");
				VehicleSkeletonFragment.mDoRetroque = savedInstanceState
						.getBoolean("doretorqueaftertor");
				if (savedInstanceState.containsKey("pressureset")
						&& !savedInstanceState.getString("pressureset").equals(
								"")) {
					PressureCheck.axlePressureSaved = CommonUtils
							.parseFloat(savedInstanceState
									.getString("pressureset"));
				}
				LogUtil.i(TAG, "on create starting connection -------------");
				PressureCheck pressureCheck = new PressureCheck(
						Constants.SELECTED_TYRE, this);
				mPressureDialog = pressureCheck;
				// startBluetoothListner();
				pressureCheck.startCreatePressureDialog();

			}
			if (savedInstanceState.getBoolean("SwapPressureStarted")) {
				mSwapSavedDataOnStop = null;
				// if third dialog box is open store values
				mPressureDialogAllreadyLoaded = true;
				PressureSwap.axlePosSaved = savedInstanceState
						.getInt("axlepos");
				PressureSwap.axlePressureSaved = savedInstanceState
						.getFloat("axlepressure");

				PressureSwap.loadSavedPressure = savedInstanceState
						.getBoolean("dialogset");
				VehicleSkeletonFragment.mDoRetroque = savedInstanceState
						.getBoolean("doretorqueaftertor");
				if (savedInstanceState.containsKey("pressureset")
						&& !savedInstanceState.getString("pressureset").equals(
								"")) {
					PressureSwap.axlePressureSaved = CommonUtils
							.parseFloat(savedInstanceState
									.getString("pressureset"));
				}
				int swapposition = savedInstanceState
						.getInt("swap_position_tyre");

				if (swapposition == 1) {
					PressureSwap PressureSwap = new PressureSwap(
							Constants.SELECTED_TYRE, this);
					PressureSwap.startCreatePressureDialog();
					mPressureDialogForSwap = PressureSwap;
				} else if (swapposition == 2) {
					PressureSwap PressureSwap = new PressureSwap(
							Constants.SECOND_SELECTED_TYRE, this);
					PressureSwap.startCreatePressureDialog();
					mPressureDialogForSwap = PressureSwap;
				} else if (swapposition == 3) {
					PressureSwap PressureSwap = new PressureSwap(
							Constants.THIRD_SELECTED_TYRE, this);
					PressureSwap.startCreatePressureDialog();
					mPressureDialogForSwap = PressureSwap;
				} else if (swapposition == 4) {
					PressureSwap PressureSwap = new PressureSwap(
							Constants.FOURTH_SELECTED_TYRE, this);
					PressureSwap.startCreatePressureDialog();
					mPressureDialogForSwap = PressureSwap;
				}
			}
		}
		loadTitle();
		//Visual Remark List Initialization
		Constants.mGlobalVisualRemarks=new ArrayList<>();
		restoreDialogState(savedInstanceState);
	}

	/**
	 * Loading Title from DB
	 */
	private void loadTitle() {
		DatabaseAdapter label = new DatabaseAdapter(context);
		label.createDatabase();
		label.open();

		mJobsaved = label.getLabel(Constants.Jobsaved);
		mYesLabel = label.getLabel(Constants.YES_LABEL);
		mNoLabel = label.getLabel(Constants.NO_LABEL);
		mBackPressMessage = label.getLabel(Constants.CONFIRM_BACK);
		sLocationErrorMessage = label
				.getLabel(Constants.LOCATION_REQUIRED_MESSAGE);
		Constants.sLblMultiplePaired = label
				.getLabel(Constants.MULTIPLE_PAIRED_DEVICES_FOUND);
		Constants.sLblPleasePairTLogik = label
				.getLabel(Constants.PLEASE_PAIR_TLOGIK);
		Constants.sLblNoPairedDevices = label
				.getLabel(Constants.NO_PAIRED_DEVICES);
		Constants.INCOMPLETE_SWAP_ERROR_MESSAGE = label.getLabel(Constants.Label_INCOMPLETE_SWAP_ERROR_MESSAGE);
		Last_Recorded_Odometer_Reading = label.getLabel(Constants.Label_Last_Recorded_Odometer_Reading);
		MSG_WRONG_ODO_VALUE = label.getLabel(Constants.Label_Wrong_OdoMeter_Value);
		label.close();

	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		mViewPager = (EjobViewPager) findViewById(R.id.pager);
		sViewAdapter = new EJobPagerAdapter(getSupportFragmentManager());
		mViewPager.setOnPageChangeListener(new SimpleOnPageChangeListener() {

			@Override
			public void onPageSelected(int pagerIndex) {
				/**
				 * if location is not set then come back to the previous
				 * fragment.
				 */
				LogUtil.d("EjobFormActionActivity", "onPageSelected: "
						+ pagerIndex);
				if (pagerIndex >= 2
						&& EjobTimeLocationFragment.sValueLocation != null
						&& EjobTimeLocationFragment.sValueLocation.getText()
								.toString().trim().equals("")) {
					LogUtil.d("EjobFormActionActivity",
							"onPageSelected: Toast Diaplayed");
					CommonUtils.toastHandler(sLocationErrorMessage, context);
					mViewPager.setCurrentItem(1, true);
				}
				blockSignatureScreen(pagerIndex);
				if (pagerIndex > 0) {
					currentPage = pagerIndex;
					Fragment selectedFragment = mViewPager.getActiveFragment(
							getSupportFragmentManager(), pagerIndex);
					LogUtil.d("EJobFormActionActivity", "selectedFragment:: "
							+ selectedFragment);

					// call the fragment is visible to user method of selected
					// fragment
					// this is required as onResume of method of the fragment is
					// not called every time
					((IEjobFragmentVisible) selectedFragment)
							.fragmentIsVisibleToUser();

					if (selectedFragment instanceof IFragmentCommunicate) {
						((IFragmentCommunicate) selectedFragment)
								.hideKeyBoard(true);
					}
				}
			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
				super.onPageScrolled(position, positionOffset,
						positionOffsetPixels);
				if (position == 1
						&& positionOffsetPixels > 0
						&& EjobTimeLocationFragment.sValueLocation != null
						&& EjobTimeLocationFragment.sValueLocation.getText()
								.toString().trim().equals("")) {
					mViewPager.setCurrentItem(1, true);
				}
				blockSignatureScreen(position);
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
					if (arg0 == 1 && !Constants.COMESFROMVIEW) {
						EjobTimeLocationFragment.sDateRecievedSaved = (String) EjobTimeLocationFragment.sRecivedOnDate
								.getText();
						EjobTimeLocationFragment.sDateOnSiteSaved = (String) EjobTimeLocationFragment.sReachedOnDate
								.getText();
						EjobTimeLocationFragment.sDateStartedSaved = (String) EjobTimeLocationFragment.sStartedOnDate
								.getText();
						EjobTimeLocationFragment.sDateFinishedSaved = (String) EjobTimeLocationFragment.sFinishedOnDate
								.getText();
						EjobTimeLocationFragment.sTimeRecievedSaved = (String) EjobTimeLocationFragment.sRecivedOnTime
								.getText();
						EjobTimeLocationFragment.sTimeOnSiteSaved = (String) EjobTimeLocationFragment.sReachedOnTime
								.getText();
						EjobTimeLocationFragment.sTimeStartedSaved = (String) EjobTimeLocationFragment.sStartedOnTime
								.getText();
						EjobTimeLocationFragment.sTimeFinishedSaved = (String) EjobTimeLocationFragment.sFinishedOnTime
								.getText();
						Date recievedDate = EjobTimeLocationFragment
								.getDate(R.id.value_recievedOnDate);

						Date startedDate = EjobTimeLocationFragment
								.getDate(R.id.value_startedOnDate);
						Date reachedDate = EjobTimeLocationFragment
								.getDate(R.id.value_reachedSiteOnDate);
						Date finishedDate = EjobTimeLocationFragment
								.getDate(R.id.value_finishedOnDate);
						loadErrorMessages();
						if (Validation.verifyDate(recievedDate, startedDate) == 2) {
							EJobPagerAdapter.count = 2;
							EjobFormActionActivity.sViewAdapter
									.notifyDataSetChanged();
							CommonUtils.notify(sReceivedDateErrorMessage, context);
						} else if (Validation.verifyDate(startedDate, reachedDate) == 2) {
							EJobPagerAdapter.count = 2;
							EjobFormActionActivity.sViewAdapter
									.notifyDataSetChanged();
							CommonUtils.notify(sStartedDateErrorMessage, context);
						} else if (Validation.verifyDate(reachedDate, finishedDate) == 2) {
							EJobPagerAdapter.count = 2;
							EjobFormActionActivity.sViewAdapter
									.notifyDataSetChanged();
							CommonUtils.notify(sReachedDateErrorMessage, context);
						}
						//CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
						//Condition Check for Odometer value
						else if (arg0 == 1
								&& Constants.IsOdometerValueIsCorrect==1) {
							EJobPagerAdapter.count = 1;
							String msg = MSG_WRONG_ODO_VALUE + "\n"+Last_Recorded_Odometer_Reading+" : " + Constants.LastRecordedOdometerValue;
							Toast.makeText(context, msg,
									Toast.LENGTH_LONG).show();
							EjobFormActionActivity.sViewAdapter
									.notifyDataSetChanged();
						}

						else if (arg0 == 1
								&& EjobTimeLocationFragment.sValueLocation
								.getText().toString().trim().equals("")) {
							EJobPagerAdapter.count = 3;
							EjobFormActionActivity.sViewAdapter
									.notifyDataSetChanged();
							// Auto Focus And Show Keyboard(Location) (STARTS)
							CommonUtils
									.setFocus(EjobTimeLocationFragment.sValueLocation);
						} else {
							boolean isDismountedTireExists = false;
							ArrayList<Tyre> tempTireList;
							if (VehicleSkeletonFragment.tyreInfo == null) {
								tempTireList = Constants.TIRE_INFO;
							} else {
								tempTireList = VehicleSkeletonFragment.tyreInfo;
							}
							if (tempTireList != null) {
								for (Tyre tire : tempTireList) {
									if (tire.isDismounted()
											&& !tire.getPosition().endsWith("$")) {
										isDismountedTireExists = true;
									}
								}
							}
							/**
							 * CR :: 447
							 * Condition checking whether the selected vehicle is partially or fully Inspected
							 * @return
							 */
								SharedPreferences preferences = context.getSharedPreferences(
										Constants.GOODYEAR_CONF, 0);
								SharedPreferences.Editor editor = preferences.edit();


								//Key to store PARTIALLY_INSPECTED_WITH_OPERATION information
								Constants.PARTIALLY_INSPECTED_WITH_OPERATION = "PARTIALLY_INSPECTED_WITH_OPERATION" + "_" + Constants.VALIDATE_USER + "_" + Constants.JobRefNumberForPartialInspectionCheck;

								try {
									LogUtil.DBLog(TAG, "On Page roll", "PARTIALLY_INSPECTED_WITH_OPERATION key value : " + Constants.PARTIALLY_INSPECTED_WITH_OPERATION);
								} catch (Exception e) {
									LogUtil.DBLog(TAG, "On Page roll", "PARTIALLY_INSPECTED_WITH_OPERATION key value : Exception" + e.getMessage());
								}

								editor.putInt(Constants.PARTIALLY_INSPECTED_WITH_OPERATION, EjobSignatureBoxFragment.partiallyInspectedWithOperation());
								editor.commit();

								LogUtil.i(TAG, "onPageScrollStateChanged - PartialInspection Status : " + String.valueOf(EjobSignatureBoxFragment.partiallyInspectedWithOperation()));
								LogUtil.DBLog(TAG, "onPageScrollStateChanged", "PartialInspection Status : " + String.valueOf(EjobSignatureBoxFragment.partiallyInspectedWithOperation()));
								boolean supportInspectionNavigation;
							//based on Inspection Setting... we check for different case
							if (preferences.getString(Constants.INSPECTION, "").equalsIgnoreCase("ON")) {
								if (EjobSignatureBoxFragment.partiallyInspectedWithOperation() == InspectionState.ONLY_INSPECTION_PARTIAL) {
									supportInspectionNavigation = true;
								} else {
									supportInspectionNavigation = false;
								}
							} else {
								supportInspectionNavigation = false;
							}

							if (isDismountedTireExists
									|| supportInspectionNavigation) {
								if (Constants.JOBTYPEREGULAR) {
									if (Constants.GEMOTRY_CHECK_CORRECTION) {
										EJobPagerAdapter.count = 6;
									} else {
										EJobPagerAdapter.count = 7;
									}
								} else {
									EJobPagerAdapter.count = 6;
								}
							} else {
								if (Constants.JOBTYPEREGULAR) {
									if (Constants.GEMOTRY_CHECK_CORRECTION) {
										EJobPagerAdapter.count = 7;
									} else {
										EJobPagerAdapter.count = 8;
									}
								} else {
									EJobPagerAdapter.count = 7;
								}
							}
							if (Constants.onReturnBool) {
								EJobPagerAdapter.count = 3;
								CommonUtils.toastHandler(
										Constants.INCOMPLETE_SWAP_ERROR_MESSAGE, context);
							}
							EjobFormActionActivity.sViewAdapter
									.notifyDataSetChanged();
						}
					}
			}
		});
		mViewPager.setAdapter(sViewAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ejob_form_action, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			LogUtil.TraceInfo(TRACE_TAG, "none", "Settings", false, false, false);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_ejob_form_action, container, false);
			return rootView;
		}
	}

	/**
	 * loading Time-Picker Dialog
	 * 
	 * @param view
	 */
	public void showTimePickerDialog(View view) {
		DialogFragment newFragment;
		switch (view.getId()) {
		case R.id.value_recievedOnTime:
			newFragment = new ReceivedOnTimePicker();
			newFragment.show(getFragmentManager(), "timePicker");
			break;
		case R.id.value_startedOnTime:
			newFragment = new StartedOnTimePicker();
			newFragment.show(getFragmentManager(), "timePicker");
			break;
		case R.id.value_reachedSiteOnTime:
			newFragment = new ReachedSiteOnTimePicker();
			newFragment.show(getFragmentManager(), "timePicker");
			break;
		case R.id.value_finishedOnTime:
			newFragment = new FinishedOnTimePicker();
			newFragment.show(getFragmentManager(), "timePicker");
			break;
		}
	}

	/**
	 * lOADING Reserve PWT tires
	 * 
	 * @param view
	 */
	public void loadReseevePWT(View view) {
		Intent loadReservePWT = new Intent(EjobFormActionActivity.this,
				ReservePWT.class);
		loadReservePWT.putExtra("COMING_FROM", "topcorner");
		startActivity(loadReservePWT);
	}

	/**
	 * Saving Job Info
	 * 
	 * @param view
	 */
	public static void saveJobInfo(View view) {
		saveJobInfo(view, false);
	}

	/**
	 * save job data into database
	 * 
	 * @param view
	 *            view clicked by user
	 * @param shouldMoveToEJobList
	 *            should user be moved to {@link EjobList} activity.
	 */
	public static void saveJobInfo(View view, boolean shouldMoveToEJobList) {
		if (checkIfJobCanBeSaved()) {
			if (Constants.onReturnBool) {
				CommonUtils.notify(Constants.INCOMPLETE_SWAP_ERROR_MESSAGE, context);
				return;
			}
			saveToDB(context);
			if (shouldMoveToEJobList) {
				moveToEjobListActivity();
			}

			CommonUtils.notify(mJobsaved, context);
		}
		/*// Bug :: 524 :: clean objects if not saving from back press
		if (!mStopErrorMessageIfSavingFromBackPress && checkIfJobCanBeSaved())
			kk();*/
	}

	/**
	 * this method will check if all validations for time and location are
	 * holding
	 * 
	 * @return true if job can be saved, false otherwise
	 */
	public static boolean checkIfJobCanBeSaved() {
		Date recievedDate = EjobTimeLocationFragment
				.getDate(R.id.value_recievedOnDate);
		Date startedDate = EjobTimeLocationFragment
				.getDate(R.id.value_startedOnDate);
		Date reachedDate = EjobTimeLocationFragment
				.getDate(R.id.value_reachedSiteOnDate);
		Date finishedDate = EjobTimeLocationFragment
				.getDate(R.id.value_finishedOnDate);
		loadErrorMessages();
		if (Validation.verifyDate(recievedDate, startedDate) == 2) {
			if (!mStopErrorMessageIfSavingFromBackPress) {
				CommonUtils.notify(sReceivedDateErrorMessage, context);
			}
			return false;
		} else if (Validation.verifyDate(startedDate, reachedDate) == 2) {
			if (!mStopErrorMessageIfSavingFromBackPress) {
				CommonUtils.notify(sStartedDateErrorMessage, context);
			}
			return false;

		} else if (Validation.verifyDate(reachedDate, finishedDate) == 2) {
			if (!mStopErrorMessageIfSavingFromBackPress) {
				CommonUtils.notify(sReachedDateErrorMessage, context);
			}
			return false;
		} else if (!(EjobTimeLocationFragment.getSetLocation().length() > 0)) {
			if (!mStopErrorMessageIfSavingFromBackPress) {
				CommonUtils.toastHandler(sLocationErrorMessage, context);
			}
			return false;
		}
		return true;
	}

	/**
	 * Loading Error Message
	 */
	private static void loadErrorMessages() {
		DatabaseAdapter label = new DatabaseAdapter(context);
		label.createDatabase();
		label.open();
		sReceivedDateErrorMessage = label
				.getLabel(Constants.TIME_RECEIVED_SHOULD_BE_EARLIER_THEN_STARTED);
		sStartedDateErrorMessage = label
				.getLabel(Constants.TIME_STARTED_SHOULD_BE_EARLIER_THEN_ON_SITE);
		sReachedDateErrorMessage = label
				.getLabel(Constants.TIME_ON_SITE_SHOULD_BE_EARLIER_THEN_FINISHED);
		sLocationErrorMessage = label
				.getLabel(Constants.LOCATION_REQUIRED_MESSAGE);
		label.close();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(SAVE_ICON_STATE, mSaveIconState);
		float[] arraypressure;
		if (VehicleSkeletonFragment.mTorStarted == true) {
			PressureCheck.savePressureCheck();
			outState.putBoolean("torstarted", true);
			// if third dialog box is open store values
			if (PressureCheck.getDialogSet()) {
				outState.putBoolean("dialogset", PressureCheck.getDialogSet());
				outState.putInt("axlepos", PressureCheck.getAxlePos());
				outState.putFloat("axlepressure",
						PressureCheck.getAxlePressure());
				outState.putString("pressureset",
						PressureCheck.getPressureSet());
				outState.putBoolean("doretorqueaftertor",
						PressureCheck.isDoRetorque());
			}
		}
		if (VehicleSkeletonFragment.mSwapPressureStarted == true) {
			PressureSwap.savePressureCheck();
			outState.putBoolean("SwapPressureStarted", true);
			mSkipDialogCancelation = true;
			// if third dialog box is open store values
			if (PressureSwap.getDialogSet()) {
				outState.putBoolean("dialogset", PressureSwap.getDialogSet());
				outState.putInt("axlepos", PressureSwap.getAxlePos());
				outState.putFloat("axlepressure",
						PressureSwap.getAxlePressure());
				outState.putString("pressureset", PressureSwap.getPressureSet());
				outState.putBoolean("doretorqueaftertor",
						PressureSwap.isDoRetorque());
				outState.putInt("swap_position_tyre",
						PressureSwap.position_swap_tyre);
				mSwapSavedDataOnStop = new SwapPressureSavedDataOnStop();
				mSwapSavedDataOnStop.setmIsDialogSet(PressureSwap
						.getDialogSet());
				mSwapSavedDataOnStop
						.setmAxlePosition(PressureSwap.getAxlePos());
				mSwapSavedDataOnStop.setmPressure(PressureSwap
						.getAxlePressure());
				mSwapSavedDataOnStop.setmPressureSet(PressureSwap
						.getPressureSet());
				mSwapSavedDataOnStop.setmDoRetorqueAfter(PressureSwap
						.isDoRetorque());
				mSwapSavedDataOnStop
						.setmPressureSwapPos(PressureSwap.position_swap_tyre);
			}
		}
		// check if re torque dialog is open
		else if (VehicleSkeletonFragment.mRetroqueStarted == true) {
			outState.putBoolean("retorqueStarted", true);
			// else store current position
			outState.putInt("currentposition", Retorque.currentPosition);
			// if third dialog box is open store values
			if (Retorque.currentPosition == 3) {
				Retorque.saveStateOfRetorque();
				outState.putInt("retorqueval", Retorque.retorqueValueSaved);
				outState.putString("wrenchval", Retorque.wrenchNumberSaved);
				outState.putBoolean("labelissued",
						Retorque.retorqueLabelIssuedSaved);
				outState.putBoolean("retorquedone", Retorque.retorqueDone);
			}
		} else {
			// put re torque not set
			outState.putBoolean("retorqueStarted", false);
		}
		if (!Constants.COMESFROMVIEW) {
			outState.putBoolean("save",
					EjobTimeLocationFragment.isSaveEnabled());
		}
		saveDialogState(outState);
	}

	/**
	 * Returns true if there is any empty position on a vehicle
	 * 
	 * @return
	 */
	public static Boolean iSEmptyTyreInVehicle() {
		boolean check = false;
		try {

			for (Tyre currentTire : VehicleSkeletonFragment.tyreInfo) {
				if (currentTire.getTyreState() == TyreState.EMPTY
						|| currentTire.isDismounted()) {
					if (!currentTire.getPosition().endsWith("$")) {
						check = false;
						break;
					}
				} else {
					check = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return check;
	}

	/**
	 * Storing Tires in DB
	 */
	private static void saveToDB(Context context) {
		LogUtil.DBLog(Log_TAG, "Save To DB", "Start");
		System.out.println("" + Constants.TIRE_INFO);
		sDb = new DBModel(context);
		Job.setStatus("1"); // for incompleted
		EjobTimeLocationFragment.updateJobObject();
		EjobInformationFragment.updateJobObject();
		LogUtil.DBLog(Log_TAG, "Save To DB", "Before CallSaveQueries Call");
		callSaveQueries(context);
		LogUtil.DBLog(Log_TAG, "Save To DB", "After CallSaveQueries Call");
		DatabaseAdapter db_adapter = new DatabaseAdapter(context);
		try {
			db_adapter.createDatabase();
			db_adapter.open();
			/**
			 * Condition blocking Job Reference number updation in Account table during edit mode
			 */
			if(!Constants.COMESFROMUPDATE) {
				db_adapter.updateAccountTable(Constants.JOBREFNUMBERVALUE);
			}


			//Method to update FromJobID (tyre table)
			db_adapter.updateJobIDForDismountedReusableTyre(String.valueOf(Constants.JOBREFNUMBERVALUE));
			LogUtil.DBLog(Log_TAG, "Save To DB", "updateJobIDForDismountedReusableTyre");

		} catch (Exception e2) {
			LogUtil.DBLog(Log_TAG, "Save To DB", "Exception : "+e2.getMessage());
			e2.printStackTrace();
		} finally {
			db_adapter.close();
		}
		LogUtil.DBLog(Log_TAG, "Save To DB", "END");
	}

	private static void moveToEjobListActivity() {
		Intent startEjobList = new Intent(context, EjobList.class);
		startEjobList.putExtra("sentfrom", "ejobform");
		context.startActivity(startEjobList);
		if (!mStopErrorMessageIfSavingFromBackPress && checkIfJobCanBeSaved())
			cleanConfigurationBeforeClose();
		Constants.COMESFROMUPDATE = false;
	}

	/**
	 * 
	 */
	private static void cleanConfigurationBeforeClose() {
		// remove config data
		VehicleSkeletonFragment.mAxleRecommendedPressure = null;
		Retorque.retorqueDone = false;
		Constants.TIRE_INFO = null;
		VehicleSkeletonFragment.tyreInfo = null;
		VehicleSkeletonFragment.mJobItemList = null;
		VehicleSkeletonFragment.mTireImageItemList = null;
		VehicleSkeletonFragment.mJobcorrectionList = null;
		Constants.JOB_ITEMLIST = null;
		Constants.JOB_CORRECTION_LIST = null;
		Constants.vehicleConfiguration = null;
		Constants.JOB_TIREIMAGE_ITEMLIST = null;
		Constants.UPDATE_APPLIED_ON_EDIT = false;
		Retorque.torqueSettings = null;
		Constants.TORQUE_SETTINGS_ARRAY = null;
		Job.clearJobObject();
		if(EjobAdditionalServicesFragment.sSwitchBtns!=null) {
			EjobAdditionalServicesFragment.sSwitchBtns.clear();
		}
		if(EjobAdditionalServicesFragment.sServiceValues!=null) {
			EjobAdditionalServicesFragment.sServiceValues.clear();
		}
		if(VehicleSkeletonFragment.mReserveTyreList!=null) {
			VehicleSkeletonFragment.mReserveTyreList.clear();
		}
		if(ReservePWTUtils.mReserveTyreList!=null) {
			ReservePWTUtils.mReserveTyreList.clear();
		}
		VehicleSkeletonFragment.resetBoolValue();
		if(Constants.Search_TM_ListItems!=null) {
			Constants.Search_TM_ListItems.clear();
		}
		Constants.jobIdEdit = null;
		Constants.IS_FORM_ELEMENTS_CLEANED = true;
		Constants.IS_SIGANTURE_DONE = false;
		CameraUtility.resetParams();
		// Cleaning the Static variables assigned while create a job to avoid
		// values carried over when a different user logs in.
		Constants.SAP_VENDORCODE_ID = "";
		Constants.VENDER_NAME = "";
		Constants.SAP_CONTRACTNUMBER_ID = "";
		Constants.SAP_CUSTOMER_ID = "";
		Constants.SAPCONTRACTNUMBER = "";
		Constants.SAP_CUSTOMER_ID_JOB = "";
		if(Constants.CHECKED_JOB_LIST!=null) {
			Constants.CHECKED_JOB_LIST.clear();
		}
		Constants.JOB_CORRECTION_COUNT_SWAP = 0;


		//Cleaning - last recorded odometer value
		Constants.LastRecordedOdometerValue = 0L;
		Constants.IsOdometerValueIsCorrect = 0;

	}

	/**
	 * 
	 */
	private static void callSaveQueries(Context context) {
		LogUtil.DBLog(Log_TAG, "Call Save Queries", "Start");
		sDb.open();

		if (Constants.COMESFROMUPDATE) {
			LogUtil.DBLog(Log_TAG, "Call Save Queries", "Call Update Queries Method (SaveJob) COMESFROMUPDATE - " + Constants.COMESFROMUPDATE );
			SaveJob.callUpdateQueries(sDb, sJobID,context);
			//Constants.COMESFROMUPDATE = false;
		} else {
			LogUtil.DBLog(Log_TAG, "Call Save Queries", "Call Update Queries Method (SaveJob) COMESFROMUPDATE - " + Constants.COMESFROMUPDATE );
			SaveJob.callUpdateQueries(sDb, sJobID,context);
		}
		sDb.close();
		LogUtil.DBLog(Log_TAG, "Call Save Queries", "End");
	}

	public void showDatePickerDialog(View view) {
		// EjobInformationFragment.enableSaveButton();

		DialogFragment newFragment;
		switch (view.getId()) {
		case R.id.value_recievedOnDate:

			newFragment = new ReceivedDatePicker();
			newFragment.show(getFragmentManager(), "datePicker");

			break;

		case R.id.value_startedOnDate:

			newFragment = new StartedOnDatePicker();
			newFragment.show(getFragmentManager(), "datePicker");

			break;

		case R.id.value_reachedSiteOnDate:

			newFragment = new ReachedOnDatePicker();
			newFragment.show(getFragmentManager(), "datePicker");

			break;

		case R.id.value_finishedOnDate:
			newFragment = new FinishedOnDatePicker();
			newFragment.show(getFragmentManager(), "datePicker");
			break;
		}
	}

	public static class ReceivedDatePicker extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		private SafeDatePickerDialog mSafeDatePickerDialog;

		@Override
		public Dialog onCreateDialog(Bundle savedInstance) {

			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DATE);

			String[] date = DateTimeUTC.getSplittedDate(
					EjobTimeLocationFragment.sRecivedOnDate.getText()
							.toString(), context);

			if (date != null & date.length == 3) {
				year = Integer.parseInt(date[2]);
				month = Integer.parseInt(date[1]) - 1;
				day = Integer.parseInt(date[0]);
			}

			mSafeDatePickerDialog = new SafeDatePickerDialog(getActivity(),
					this, year, month, day);
			return mSafeDatePickerDialog;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			if (!mSafeDatePickerDialog.isStopped()) {
				// update user activity
				InactivityUtils.updateActivityOfUser();

				String outPutDateStr = DateTimeUTC.splittedDateToString(year,
						(month + 1), day, context);
				if (!TextUtils.isEmpty(outPutDateStr)) {
					EjobTimeLocationFragment.changeDate(outPutDateStr,
							R.id.value_recievedOnDate);
				}
			}
		}
	}

	public static class StartedOnDatePicker extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		private SafeDatePickerDialog mSafeDatePickerDialog;

		@Override
		public Dialog onCreateDialog(Bundle savedInstance) {

			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DATE);

			String[] date = DateTimeUTC.getSplittedDate(
					EjobTimeLocationFragment.sStartedOnDate.getText()
							.toString(), context);

			if (date != null & date.length == 3) {
				year = Integer.parseInt(date[2]);
				month = Integer.parseInt(date[1]) - 1;
				day = Integer.parseInt(date[0]);
			}

			mSafeDatePickerDialog = new SafeDatePickerDialog(getActivity(),
					this, year, month, day);
			return mSafeDatePickerDialog;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			if (!mSafeDatePickerDialog.isStopped()) {
				// update user activity
				InactivityUtils.updateActivityOfUser();

				String outPutDateStr = DateTimeUTC.splittedDateToString(year,
						(month + 1), day, context);
				if (!TextUtils.isEmpty(outPutDateStr)) {
					EjobTimeLocationFragment.changeDate(outPutDateStr,
							R.id.value_startedOnDate);
				}
			}
		}
	}

	public static class ReachedOnDatePicker extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		private SafeDatePickerDialog mSafeDatePickerDialog;

		@Override
		public Dialog onCreateDialog(Bundle savedInstance) {
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DATE);

			String[] date = DateTimeUTC.getSplittedDate(
					EjobTimeLocationFragment.sReachedOnDate.getText()
							.toString(), context);

			if (date != null & date.length == 3) {
				year = Integer.parseInt(date[2]);
				month = Integer.parseInt(date[1]) - 1;
				day = Integer.parseInt(date[0]);
			}

			mSafeDatePickerDialog = new SafeDatePickerDialog(getActivity(),
					this, year, month, day);
			return mSafeDatePickerDialog;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			if (!mSafeDatePickerDialog.isStopped()) {
				// update user activity
				InactivityUtils.updateActivityOfUser();

				String outPutDateStr = DateTimeUTC.splittedDateToString(year,
						(month + 1), day, context);
				if (!TextUtils.isEmpty(outPutDateStr)) {
					EjobTimeLocationFragment.changeDate(outPutDateStr,
							R.id.value_reachedSiteOnDate);
				}
			}
		}
	}

	public static class FinishedOnDatePicker extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		private SafeDatePickerDialog mSafeDatePickerDialog;

		@Override
		public Dialog onCreateDialog(Bundle savedInstance) {
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DATE);

			String[] date = DateTimeUTC.getSplittedDate(
					EjobTimeLocationFragment.sFinishedOnDate.getText()
							.toString(), context);

			if (date != null & date.length == 3) {
				year = Integer.parseInt(date[2]);
				month = Integer.parseInt(date[1]) - 1;
				day = Integer.parseInt(date[0]);
			}

			mSafeDatePickerDialog = new SafeDatePickerDialog(getActivity(),
					this, year, month, day);
			return mSafeDatePickerDialog;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			if (!mSafeDatePickerDialog.isStopped()) {
				// update user activity
				InactivityUtils.updateActivityOfUser();

				String outPutDateStr = DateTimeUTC.splittedDateToString(year,
						(month + 1), day, context);
				if (!TextUtils.isEmpty(outPutDateStr)) {
					EjobTimeLocationFragment.changeDate(outPutDateStr,
							R.id.value_finishedOnDate);
				}
			}
		}
	}

	public static class ReceivedOnTimePicker extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		private SafeTimePickerDialog mSafeTimePickerDialog;

		@Override
		public Dialog onCreateDialog(Bundle savedInstance) {
			String[] time = EjobTimeLocationFragment.sRecivedOnTime.getText()
					.toString().split(":");

			int hour = Integer.parseInt(time[0]);
			int min = Integer.parseInt(time[1]);
			mSafeTimePickerDialog = new SafeTimePickerDialog(getActivity(),
					this, hour, min, true);
			return mSafeTimePickerDialog;
		}

		@Override
		public void onTimeSet(android.widget.TimePicker view, int hourOfDay,
				int minute) {
			if (!mSafeTimePickerDialog.isStopped()) {
				// update user activity
				InactivityUtils.updateActivityOfUser();

				String min = checkMinutes(minute);
				String hours = checkHours(hourOfDay);
				EjobTimeLocationFragment.changeTime(hours + ":" + min,
						R.id.value_recievedOnTime);
			}
		}
	}

	@Override
	public void onBackPressed() {
		if (Constants.onReturnBool) {
			LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press : Msg : "+Constants.INCOMPLETE_SWAP_ERROR_MESSAGE, true, false, false);
			CommonUtils.notify(Constants.INCOMPLETE_SWAP_ERROR_MESSAGE, context);
			return;
		}
		if (Constants.COMESFROMVIEW) {
			LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press", true, false, false);
			cleanConfigurationBeforeClose();
			Intent startEjobList = new Intent(getBaseContext(), EjobList.class);
			startActivity(startEjobList);

		} else {
			createDialogOnBackPress();
		}
	}

	private AlertDialog mBackAlertDialog;

	private void saveDialogState(Bundle state) {
		state.putBoolean("mBackAlertDialog",
				(mBackAlertDialog != null && mBackAlertDialog.isShowing()));
	}

	private void restoreDialogState(Bundle state) {
		if (state != null) {
			if (state.getBoolean("mBackAlertDialog")) {
				createDialogOnBackPress();
			}
		}
	}

	private void createDialogOnBackPress() {
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", true, true, false);
		LayoutInflater li = LayoutInflater.from(EjobFormActionActivity.this);
		View promptsView = li
				.inflate(R.layout.dialog_logout_confirmation, null);

		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
				EjobFormActionActivity.this);
		alertDialogBuilder.setView(promptsView);

		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		// Setting The Info
		TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
		infoTv.setText(mBackPressMessage);

		alertDialogBuilder.setCancelable(false).setPositiveButton(mYesLabel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
						Constants.onReturnBool = false;
						boolean isJobPresent = false;
						if (sViewAdapter != null) {

							if (currentPage >= 0) {
								Fragment selectedFragment = mViewPager
										.getActiveFragment(
												getSupportFragmentManager(),
												currentPage);
								LogUtil.d("EJobFormActionActivity",
										"selectedFragment:: "
												+ selectedFragment);
								if (selectedFragment instanceof IFragmentCommunicate) {
									isJobPresent = ((IFragmentCommunicate) selectedFragment)
											.onActivityBackPress();
								}
							}
						}
						// cleanConfigurationBeforeClose();

						if (VehicleSkeletonFragment.mJobItemList != null
								&& !isJobPresent) {
							isJobPresent = VehicleSkeletonFragment.mJobItemList
									.size() > 0;
						}
						if (Constants.COMESFROMUPDATE
								&& Constants.JOB_ITEMLIST != null
								&& !isJobPresent) {
							isJobPresent = Constants.JOB_ITEMLIST.size() > 0;
						}
						LogUtil.d("EjobFormActionActivity::",
								"isJobPresent :: " + isJobPresent);
						if (isJobPresent) {
							mStopErrorMessageIfSavingFromBackPress = true;
							saveJobInfo(null);
							mStopErrorMessageIfSavingFromBackPress = false;
							bool = true;
							cleanConfigurationBeforeClose();
							Intent startEjobList = new Intent(getBaseContext(),
									EjobList.class);
							startEjobList.putExtra("sentfrom", "ejobform");
							startActivity(startEjobList);
							dialog.dismiss();
						} else {
							// TODO Need to check this variable for future fix.
							// FIX Delete Issue
							EjobFormActionActivity.bool = false;
							EditJob.sJobItemToDelete = "";
							Intent startEjobList = new Intent(getBaseContext(),
									EjobList.class);
							startEjobList.putExtra("sentfrom", "ejobform");
							startActivity(startEjobList);
							cleanConfigurationBeforeClose();
							dialog.dismiss();
						}
						VehicleSkeletonFragment.resetBoolValue();
						VehicleSkeletonFragment.resetTyres();
					}
				});

		alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);
						// if user select "No", just cancel this dialog and
						// continue
						// with app
						dialog.cancel();
					}
				});

		// create alert dialog
		mBackAlertDialog = alertDialogBuilder.create();

		// show it
		mBackAlertDialog.show();
		mBackAlertDialog.setCanceledOnTouchOutside(false);

	}

	public static class StartedOnTimePicker extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		private SafeTimePickerDialog mSafeTimePickerDialog;

		@Override
		public Dialog onCreateDialog(Bundle savedInstance) {
			String[] time = EjobTimeLocationFragment.sStartedOnTime.getText()
					.toString().split(":");

			int hour = Integer.parseInt(time[0]);
			int min = Integer.parseInt(time[1]);
			mSafeTimePickerDialog = new SafeTimePickerDialog(getActivity(),
					this, hour, min, true);
			return mSafeTimePickerDialog;
		}

		@Override
		public void onTimeSet(android.widget.TimePicker view, int hourOfDay,
				int minute) {
			if (!mSafeTimePickerDialog.isStopped()) {
				// update user activity
				InactivityUtils.updateActivityOfUser();

				String min = checkMinutes(minute);
				String hours = checkHours(hourOfDay);
				EjobTimeLocationFragment.changeTime(hours + ":" + min,
						R.id.value_startedOnTime);
			}
		}
	}

	public static class ReachedSiteOnTimePicker extends DialogFragment
			implements TimePickerDialog.OnTimeSetListener {

		private SafeTimePickerDialog mSafeTimePickerDialog;

		@Override
		public Dialog onCreateDialog(Bundle savedInstance) {
			String[] time = EjobTimeLocationFragment.sReachedOnTime.getText()
					.toString().split(":");

			int hour = Integer.parseInt(time[0]);
			int min = Integer.parseInt(time[1]);
			mSafeTimePickerDialog = new SafeTimePickerDialog(getActivity(),
					this, hour, min, true);
			return mSafeTimePickerDialog;
		}

		@Override
		public void onTimeSet(android.widget.TimePicker view, int hourOfDay,
				int minute) {
			if (!mSafeTimePickerDialog.isStopped()) {
				// update user activity
				InactivityUtils.updateActivityOfUser();

				String min = checkMinutes(minute);
				String hours = checkHours(hourOfDay);
				EjobTimeLocationFragment.changeTime(hours + ":" + min,
						R.id.value_reachedSiteOnTime);
			}
		}
	}

	public static class FinishedOnTimePicker extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		private SafeTimePickerDialog mSafeTimePickerDialog;

		@Override
		public Dialog onCreateDialog(Bundle savedInstance) {
			String[] time = EjobTimeLocationFragment.sFinishedOnTime.getText()
					.toString().split(":");

			int hour = Integer.parseInt(time[0]);
			int min = Integer.parseInt(time[1]);
			mSafeTimePickerDialog = new SafeTimePickerDialog(getActivity(),
					this, hour, min, true);
			return mSafeTimePickerDialog;
		}

		@Override
		public void onTimeSet(android.widget.TimePicker view, int hourOfDay,
				int minute) {
			if (!mSafeTimePickerDialog.isStopped()) {
				// update user activity
				InactivityUtils.updateActivityOfUser();

				String min = checkMinutes(minute);
				String hours = checkHours(hourOfDay);
				EjobTimeLocationFragment.changeTime(hours + ":" + min,
						R.id.value_finishedOnTime);
			}
		}
	}

	/**
	 * if minutes less then 10 add 0 as prefix
	 * 
	 * @param min
	 *            minutes to check
	 * @return modified minutes value
	 */
	private static String checkMinutes(int min) {
		if (min < 10) {
			return "0" + min;
		} else {
			return String.valueOf(min);
		}
	}

	private static String checkHours(int hour) {
		if (hour < 10) {
			return "0" + hour;
		} else {
			return String.valueOf(hour);
		}
	}

	@Override
	protected void onDestroy() {
		try {
			if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
				mBackAlertDialog.dismiss();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	private void blockSignatureScreen(int pagerIndex) {
		if (mViewPager != null) {
			int selectedPageIndex = mViewPager.getCurrentItem();
			LogUtil.d("TAG", "_" + selectedPageIndex);
			if (selectedPageIndex <= 0) {
				return;
			}
			if (sViewAdapter != null) {
				Fragment fragment = sViewAdapter.getItem(selectedPageIndex);
				if (fragment instanceof EjobSignatureBoxFragment) {
					if (VehicleSkeletonFragment.mJobItemList != null
							&& VehicleSkeletonFragment.mJobItemList.size() > 0) {
						boolean isNonDeletedItemPresent = false;
						for (int i = 0; i < VehicleSkeletonFragment.mJobItemList
								.size(); i++) {
							JobItem jobItem = VehicleSkeletonFragment.mJobItemList
									.get(i);
							if (jobItem.getDeleteStatus() == -1) {
								isNonDeletedItemPresent = true;
								break;
							}
						}
						if (!isNonDeletedItemPresent) {
							mViewPager.setCurrentItem((selectedPageIndex - 1),
									true);
						}
					} else {
						mViewPager
								.setCurrentItem((selectedPageIndex - 1), true);
					}
				}
			}
		}
	}

	private BluetoothService mBTService;
	private BroadcastReceiver mReceiver;
	BTTask task;
	private boolean mPressureDialogAllreadyLoaded;

	private void initiateBT() {
		int status = mBTService.getPairedStatus();
		switch (status) {
		case 0:
			CommonUtils.notify(Constants.sLblMultiplePaired, this);
			break;
		case 1:
			task = new BTTask();
			task.execute();
			break;
		case 2:
			CommonUtils.notify(Constants.sLblPleasePairTLogik, this);
			break;
		case 3:
			CommonUtils.notify(Constants.sLblNoPairedDevices, this);
			break;
		}
	}

	class BTTask extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... f_url) {
			if (isCancelled()) {
				LogUtil.e("Regroove", "doInBackground Async Task Cancelled  > ");
				return "";
			}
			try {
				mBTService.connectToDevice(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if (isCancelled()) {
				LogUtil.e("Regroove", "onPostExecute Async Task Cancelled  > ");
				return;
			}
			try {
				if (mBTService.isConnected()) {
					if (mPressureDialog != null)
						mPressureDialog.togglePressureField(false);
					else {
						mPressureDialogForSwap.togglePressureField(false);
						mPressureDialogForSwap
								.toggleSecondDialogueController(false);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	protected void onStop() {
		LogUtil.i(TAG, "executing onstop -------------");
		stopBluetooth();
		if (mPressureDialogForSwap != null) {
			mPressureDialogForSwap.dismissDialog();
			mSkipDialogCancelation = true;
		}
		super.onStop();
	}

	public void startBluetoothListner() {
		LogUtil.i(TAG, "starting bluetooth listner -------------");
		if (mBTService != null) {
			mBTService.stop();
		}

		mBTService = new BluetoothService(this);

		IntentFilter intentFilter = new IntentFilter("BLUETOOTH_SENDER");
		mReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.hasExtra("BT_DATA_PRE")) {
					String pre_value = intent.getStringExtra("BT_DATA_PRE");

					if (CommonUtils.getPressureUnit(context).equals(
							Constants.PSI_PRESSURE_UNIT)) {
						String barValue;
						// change PSI to BAR
						barValue = CommonUtils.getPressureValue(context,
								CommonUtils.parseFloat(pre_value));
						if (mPressureDialog != null) {
							mPressureDialog.setmPressureValue(String
									.valueOf(barValue));
						} else {
							mPressureDialogForSwap.setmPressureValue(String
									.valueOf(barValue));
						}
					} else {
						if (mPressureDialog != null) {
							mPressureDialog.setmPressureValue(pre_value);
						} else {
							mPressureDialogForSwap.setmPressureValue(pre_value);
						}
					}
				} else if (intent.hasExtra("BT_STATUS_IS_DISCONNECTED")) {
					boolean isDisconnected = intent.getBooleanExtra(
							"BT_STATUS_IS_DISCONNECTED", false);
					LogUtil.i(
							"Bluetooth in my Operation",
							"Broadcast came to my oeration:: " + isDisconnected
									+ " mBTService: "
									+ mBTService.isConnected());
					if (mPressureDialog != null)
						mPressureDialog.togglePressureField(isDisconnected);
					else {
						mPressureDialogForSwap
								.togglePressureField(isDisconnected);
						mPressureDialogForSwap
								.toggleSecondDialogueController(isDisconnected);
					}
					if (isDisconnected) {
						mBTService.stop();
						initiateBT();
						// mBTService.beginListeningData(0);
					} else {
						LogUtil.i("TOR", "######## Makeing stopworker false ");
						// mBTService.resetSocket();
						mBTService.startReading();
						// mBTService.beginListeningData(0);
					}
				} else if (intent.hasExtra("BT_RETRY_FAIL")) {
					initiateBT();
				}
			}
		};
		this.registerReceiver(mReceiver, intentFilter);
		initiateBT();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.fragment.VehicleSkeletonFragment.PressureDialogCreated
	 * #pressureDialogStarted
	 * (com.goodyear.ejob.job.operation.helpers.PressureCheck)
	 */
	@Override
	public void pressureDialogStarted(PressureCheck pressure) {
		mPressureDialog = pressure;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentActivity#onStart()
	 */
	@Override
	protected void onStart() {
		LogoutHandler.setCurrentActivity(this);
		LogUtil.i(TAG, "on start -------------");
		if (mPressureDialog != null || mPressureDialogForSwap != null) {
			startBluetoothListner();
			if (mSwapSavedDataOnStop != null) {
				startPressureDialogForSwap(
						mSwapSavedDataOnStop.getmAxlePosition(),
						mSwapSavedDataOnStop.getmPressure(),
						mSwapSavedDataOnStop.ismIsDialogSet(),
						mSwapSavedDataOnStop.getmPressureSwapPos(),
						mSwapSavedDataOnStop.ismDoRetorqueAfter());
			}
			System.out.println(mSwapSavedDataOnStop);
			mSwapSavedDataOnStop = null;
		}
		super.onStart();

	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	private void startPressureDialogForSwap(int axlepos, float axlepressure,
			boolean dialogset, int swapPosition, boolean doRetorque) {
		// if third dialog box is open store values
		mPressureDialogAllreadyLoaded = true;
		PressureSwap.axlePosSaved = axlepos;
		PressureSwap.axlePressureSaved = axlepressure;

		PressureSwap.loadSavedPressure = dialogset;
		VehicleSkeletonFragment.mDoRetroque = doRetorque;
		int swapposition = swapPosition;

		if (swapposition == 1) {
			PressureSwap PressureSwap = new PressureSwap(
					Constants.SELECTED_TYRE, this);
			PressureSwap.startCreatePressureDialog();
			mPressureDialogForSwap = PressureSwap;
		} else if (swapposition == 2) {
			PressureSwap PressureSwap = new PressureSwap(
					Constants.SECOND_SELECTED_TYRE, this);
			PressureSwap.startCreatePressureDialog();
			mPressureDialogForSwap = PressureSwap;
		} else if (swapposition == 3) {
			PressureSwap PressureSwap = new PressureSwap(
					Constants.THIRD_SELECTED_TYRE, this);
			PressureSwap.startCreatePressureDialog();
			mPressureDialogForSwap = PressureSwap;
		} else if (swapposition == 4) {
			PressureSwap PressureSwap = new PressureSwap(
					Constants.FOURTH_SELECTED_TYRE, this);
			PressureSwap.startCreatePressureDialog();
			mPressureDialogForSwap = PressureSwap;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.BluetoothDialogActivityConnector#closeConnection
	 * ()
	 */
	@Override
	public void closeConnection() {
		LogUtil.i(TAG, "on close connection -------------");
		stopBluetooth();
		mPressureDialog = null;
		mPressureDialogForSwap = null;
	}

	private void stopBluetooth() {
		LogUtil.i(TAG, "stop bluetooth connection -------------");
		if (mPressureDialog != null || mPressureDialogForSwap != null) {
			LogUtil.i("BT",
					"****************** onStop *****************:mBTService: "
							+ mBTService);
			try {
				if (null != mBTService) {
					mBTService.stop();
					if (task != null && !task.isCancelled()) {
						mBTService.asyncCancel(true);
						task.cancel(true);
						task = null;
					}
				}
				if (null != mReceiver) {
					this.unregisterReceiver(mReceiver);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.fragment.VehicleSkeletonFragment.PressureDialogCreated
	 * #pressureDialogStartedForSwap
	 * (com.goodyear.ejob.job.operation.helpers.PressureSwap)
	 */
	@Override
	public void pressureDialogStartedForSwap(PressureSwap pressureSwap) {
		mPressureDialogForSwap = pressureSwap;
		startBluetoothListner();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		if (checkIfJobCanBeSaved()) {
			if (currentPage >= 0) {
				Fragment selectedFragment = mViewPager.getActiveFragment(
						getSupportFragmentManager(), currentPage);
				LogUtil.d("EJobFormActionActivity", "selectedFragment:: "
						+ selectedFragment);
				if (selectedFragment instanceof IFragmentCommunicate) {
					((IFragmentCommunicate) selectedFragment)
							.onActivityBackPress();
				}
			}
			EjobTimeLocationFragment.updateJobObject();
			InactivityUtils.logoutFromActivityAboveEjobForm(this);
		} else {
			InactivityUtils.logoutFromActivityBelowEjobForm(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.ISaveIconEjobFragment#getSaveIconState()
	 */
	@Override
	public int getSaveIconState() {
		if (mSaveIconState > EjobSaveIconStates.ICON_RED) {
			mSaveIconState = EjobSaveIconStates.ICON_GRAY;
		}
		return mSaveIconState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.ISaveIconEjobFragment#setSaveIconState(int)
	 */
	@Override
	public void setSaveIconState(int iconStatus) {
		if (iconStatus > EjobSaveIconStates.ICON_RED) {
			iconStatus = EjobSaveIconStates.ICON_GRAY;
		}
		mSaveIconState = iconStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentActivity#onPause()
	 */
	@Override
	protected void onPause() {
		super.onPause();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentActivity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
	}
}
