package com.goodyear.ejob.exceptions;

/**
 * Created by giriprasanth.vp on 11/23/2015.
 * Exception class to Handle Server Response (Server responses with error)
 */
public class ServerResponsesWithErrorException extends Exception {
    public ServerResponsesWithErrorException(String s)
    {
        super(s);
    }
}
