package com.goodyear.ejob.exceptions;

/**
 * @author shailesh.p
 * 
 */
@SuppressWarnings("serial")
public class EjobSyncException extends Exception {
	public EjobSyncException(String message) {
		super(message);
	}
}
