package com.goodyear.ejob;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.goodyear.ejob.blutooth.BluetoothService;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.job.operation.helpers.PressureCheckListener;
import com.goodyear.ejob.job.operation.helpers.TireImageItem;
import com.goodyear.ejob.performance.IOnPerformanceCallback;
import com.goodyear.ejob.performance.PerformanceBaseModel;
import com.goodyear.ejob.performance.ResponseMessagePerformance;
import com.goodyear.ejob.util.CameraUtility;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.InspectionDataHandler;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.RegrooveFormElements;
import com.goodyear.ejob.util.RegrooveTyre;
import com.goodyear.ejob.util.TireDesignDetails;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.Validation;

import java.util.ArrayList;
import java.util.UUID;

/**
 * @author amitkumar.h
 * @version 1.0
 *          Class provides mechanism to load design page as per the selected
 *          tire(maintained, non-maintained, empty, spare etc). Moreover it
 *          handles the data updates in different tables(JobItem, Tire,
 *          jobCorrection etc) as per the user-entered tire-data during
 *          ReGroove Operation.
 */
public class RegrooveTireOperation extends Activity implements
        InactivityHandler {
    private Tyre mSelectedTire;
    private Spinner mBrand;
    private Spinner size;
    private Spinner asp;
    private Spinner rim;
    private Spinner design;
    private Spinner type;
    private TextView serialNumber;
    private TextView wheelPosition;
    private TextView pressureUnit;
    private EditText beforeNSKOne;
    private EditText beforeNSKTwo;
    private EditText beforeNSKThree;
    private EditText afterNSKOne;
    private EditText afterNSKTwo;
    private EditText afterNSKThree;
    /**
     * String message from DB for Please Select Brand
     */
    private String mPleaseSelectBrand = "";

    /**
     * ImageView for Camera icon
     */
    private ImageView mCamera_icon;

    private DatabaseAdapter mDbHelper;

    private ArrayList<String> mBrandArrayList;
    private ArrayList<String> mSizeArrayList;
    private ArrayList<String> mTyreTRIMArrayList;
    private ArrayList<String> mTyreTASPArrayList;
    private ArrayList<String> mDesignArrayList;
    private ArrayList<String> mFullDesignArrayList;
    private ArrayAdapter<String> mSizeDataAdapter, mRimDataAdapter,
            mASPDataAdapter, mDesignDataAdapter, mFullDesignDataAdapter;
    private String mSelectedBrandName;
    private String mSelectedtyreSize;
    private String mSelectedTyreTASP;
    private String mSelectedtyreTRIM;
    private String mSelectedTyreDesign;
    private String mSelectedTyreDetailedDesign;
    private String mSelectedSAPMaterialCodeID;

    private Boolean isBlueToothAvailable;
    private RegrooveTyre regrooveInfo;
    private boolean isSettingsPSI;

    private EditText setPressure;

    private TextView serialNumberLabel;
    private TextView wheelPositionLabel;

    private TextView pressureLabel;
    private TextView beforeLabel;
    private TextView afterLabel;
    private TextView nskOneLabel;
    private TextView nskTwoLabel;
    private TextView nskThreeLabel;
    private TextView regrooveTypeLabel;
    private RadioGroup rgRegrooveType;
    private RadioButton regrooveType;
    private RadioButton regrooveTypeYes;
    private RadioButton regrooveTypeNo;
    private static boolean flag = false, isRadioBtnChecked = false;
    private String reGrooveTypeValue = "";
    private String REGROOVE_IS_REQUIRED = "";

    private TextView dimensionLabel;
    private TextView aspLabel;
    private TextView rimLabel;
    private TextView designLabel;
    private TextView sizeLabel;
    private TextView brandLabel;
    private TextView typeLabel;
    private ScrollView mainScrollBar;

    private String nskOneBeforeRequired;
    private String nskTwoBeforeRequired;
    private String nskThreeBeforeRequired;

    private String nskOneAfterRequired;
    private String nskTwoAfterRequired;
    private String nskThreeAfterRequired;
    private String pressureRequired;
    private String backPressMessage;
    private String mYesLabel;
    private String mNoLabel;

    private String nskOneCannotBeLessThenBefore;
    private String nskTwoCannotBeLessThenBefore;
    private String nskThreeCannotBeLessThenBefore;

    private static String noteText = "";
    float recommendedpressure;
    int axlePosition;
    private boolean changeMade;
    public static final int NOTES_INTENT = 104;
    public static final int DECIMAL_LENGTH = 8;

    public boolean swiped = false;

    private BluetoothService mBTService;
    private BroadcastReceiver mReceiver;
    private int nskCounter = 1;
    private boolean mIsSizeSpinnerEditable = true;
    /**
     * string variable to store external ID
     */
    private String external_id = "";
    /**
     * Last Spinner Selection Index, This variable using Spinner manipulation
     * while orientation
     */
    private int mLastSpinnerSelection = 0;
    /**
     * IsOrientationChagned, This variable using Spinner manipulation while
     * orientation
     */
    private boolean mIsOrientationChanged = false;

    /**
     * Default value for Spinner
     */
    private String mDefaultValueForSpinner = "Please Select";
    /**
     * rib label in selected language
     */
    private String mRibLabel;
    /**
     * block label in selected language
     */
    private String mBlockLabel;
    private String mNotesLabel;

    /**
     * Final Converted Pressure
     */
    private String mFinalConvertedPressure;
    private RegrooveFormElements mFormElements;
    protected String strbluetoothconnectionfalied;
    private String mLblRegrooveTypeRequired = "Regroove type is required";
    private String mLblTyreTypeRequired = "Please Select a Type";
    private String mLblBrandIsRequired = "Please Select a Brand";
    Bundle savedInstanceState;

    /**
     * Final variable for PHOTO_INTENT
     */
    public static final int PHOTO_INTENT = 105;
    private static final String TAG = "RegroovetireOperation";

    /**
     * boolean variable for is Camera enabled
     */
    private boolean mCameraEnabled = false;
    BTTask task;
    private TextView mJobDetailsLabel;
    private String labelFromDB;

    //User Trace logs trace tag
    private static final String TRACE_TAG = "Regroove";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regroove_tire_operation);
        //User Trace logs
        LogUtil.TraceInfo(TRACE_TAG, "none", "OP", true, false, true);
        if (savedInstanceState == null) {
            noteText = "";
        }
        this.savedInstanceState = savedInstanceState;
        Intent intent = getIntent();
        recommendedpressure = intent.getExtras().getFloat(
                VehicleSkeletonFragment.mAXLEPRESSURE);
        axlePosition = intent.getExtras().getInt(
                VehicleSkeletonFragment.mAXLEPRESSURE_POS);
        mSelectedTire = Constants.SELECTED_TYRE;
        openDB();
        initialize();
        mSelectedSAPMaterialCodeID = Constants.SELECTED_TYRE
                .getSapMaterialCodeID();
        loadLabelsFromDB();

        if (savedInstanceState != null) {
            nskCounter = savedInstanceState.getInt("nsk_counter", 0);
            mIsOrientationChanged = true;
            mSelectedBrandName = savedInstanceState.getString("BRAND");
            if (savedInstanceState.containsKey("SIZE")) {
                mSelectedtyreSize = savedInstanceState.getString("SIZE");
            }
            if (savedInstanceState.containsKey("ASP")) {
                mSelectedTyreTASP = savedInstanceState.getString("ASP");
            }
            if (savedInstanceState.containsKey("RIM")) {
                mSelectedtyreTRIM = savedInstanceState.getString("RIM");
            }
            if (savedInstanceState.containsKey("DESIGN")) {
                mSelectedTyreDesign = savedInstanceState.getString("DESIGN");
            }
            if (savedInstanceState.containsKey("FULLDETAIL")) {
                mSelectedTyreDetailedDesign = savedInstanceState
                        .getString("FULLDETAIL");
            }
            mIsSizeSpinnerEditable = savedInstanceState.getBoolean(
                    "mIsSizeSpinnerEditable", true);
            checkLatestSpinnerSelectionBeforeOrientationChanges();
        }

        if (TextUtils.isEmpty(mSelectedTyreDetailedDesign)
                || mDefaultValueForSpinner.equals(mSelectedTyreDetailedDesign)) {
            TireDesignDetails detail = CommonUtils
                    .getDetailsFromTyreInSameAxle(mSelectedTire);
            if (null != detail) {
                mSelectedtyreSize = detail.getSize();
                mSelectedTyreTASP = detail.getAsp();
                mSelectedtyreTRIM = detail.getRim();
                mIsSizeSpinnerEditable = false;
            }
        }

        if (!mIsSizeSpinnerEditable) {
            loadSizeDetailsFromAxle();
        }

        if (Constants.onBrandBool
                || Constants.SELECTED_TYRE.getSerialNumber().equalsIgnoreCase(
                "")
                || TextUtils
                .isEmpty(Constants.SELECTED_TYRE.getDesignDetails())
                || mDefaultValueForSpinner.equals(mSelectedTyreDetailedDesign)) {
            loadTyreDetailsOnBrandCorrection();
        } else {
            loadTyreDetails();
        }
        captureInitialForm(savedInstanceState);
        restoreDialogState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mBrand.getCount() != 0) {
            outState.putString("BRAND", mBrand.getSelectedItem().toString());
        }
        if (size.getCount() != 0) {
            outState.putString("SIZE", size.getSelectedItem().toString());
        }
        if (asp.getCount() != 0) {
            outState.putString("ASP", asp.getSelectedItem().toString());
        }
        if (rim.getCount() != 0) {
            outState.putString("RIM", rim.getSelectedItem().toString());
        }
        if (design.getCount() != 0) {
            outState.putString("DESIGN", design.getSelectedItem().toString());
        }
        if (type.getCount() != 0) {
            outState.putString("FULLDETAIL", type.getSelectedItem().toString());
        }
        outState.putBoolean("mIsSizeSpinnerEditable", mIsSizeSpinnerEditable);
        if (mBTService != null) {
            outState.putBoolean("isworkerstopped", mBTService.isWorkerThread());
        }
        outState.putInt("nsk_counter", nskCounter);
        saveDialogState(outState);
        saveInitialState(outState);
    }

    @Override
    protected void onStop() {
        LogUtil.i("BT",
                "****************** onStop *****************:mBTService: "
                        + mBTService);
        try {
            if (null != mBTService) {
                mBTService.stop();
                if (task != null && !task.isCancelled()) {
                    mBTService.asyncCancel(true);
                    task.cancel(true);
                    task = null;
                }
            }
            if (null != mReceiver) {
                unregisterReceiver(mReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onStop();
    }

    @Override
    public void onUserInteraction() {
        InactivityUtils.updateActivityOfUser();
    }

    @Override
    protected void onStart() {
        // set user inactivity handler for this activity
        LogoutHandler.setCurrentActivity(RegrooveTireOperation.this);

        afterNSKOne.setEnabled(true);
        afterNSKTwo.setEnabled(true);
        afterNSKThree.setEnabled(true);

        if (!mBlockPressureFromBluetooth)
            setPressure.setEnabled(true);
        if (mBTService != null) {
            mBTService.stop();
        }

        mBTService = new BluetoothService(this);
        if (savedInstanceState != null) {
            boolean isWorkerThread = savedInstanceState
                    .getBoolean("isworkerstopped");
            mBTService.setWorkerThread(isWorkerThread);
        }
        IntentFilter intentFilter = new IntentFilter("BLUETOOTH_SENDER");
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("BT_DATA_NSK")) {
                    String nsk_value = intent.getStringExtra("BT_DATA_NSK");
                    if (nskCounter == 1) {
                        afterNSKOne.setText(nsk_value);
                        afterNSKTwo.setText(nsk_value);
                        afterNSKThree.setText(nsk_value);
                        nskCounter = 2;
                    } else if (nskCounter == 2) {
                        afterNSKTwo.setText(nsk_value);
                        nskCounter = 3;
                    } else if (nskCounter == 3) {
                        nskCounter = 1;
                        afterNSKThree.setText(nsk_value);
                    }
                } else if (intent.hasExtra("BT_DATA_PRE")) {
                    String pre_value = intent.getStringExtra("BT_DATA_PRE");
                    if (!mBlockPressureFromBluetooth) {
                        if (isSettingsPSI) {
                            String barValue;
                            // change PSI to BAR
                            barValue = CommonUtils.getPressureValue(context,
                                    CommonUtils.parseFloat(pre_value));
                            setPressure.setText(String.valueOf(barValue));
                        } else {
                            setPressure.setText(pre_value);
                        }
                    }
                } else if (intent.hasExtra("BT_CONN_STATUS")) {
                    Toast.makeText(getApplicationContext(),
                            strbluetoothconnectionfalied, Toast.LENGTH_LONG)
                            .show();
                } else if (intent.hasExtra("BT_STATUS_IS_DISCONNECTED")) {
                    boolean isDisconnected = intent.getBooleanExtra(
                            "BT_STATUS_IS_DISCONNECTED", false);
                    LogUtil.i(
                            "Bluetooth in my Operation",
                            "Broadcast came to my oeration:: " + isDisconnected
                                    + " mBTService: "
                                    + mBTService.isConnected());
                    afterNSKOne.setEnabled(isDisconnected);
                    afterNSKTwo.setEnabled(isDisconnected);
                    afterNSKThree.setEnabled(isDisconnected);
                    if (!mBlockPressureFromBluetooth)
                        setPressure.setEnabled(isDisconnected);
                    if (isDisconnected) {
                        mBTService.stop();
                        initiateBT();
                        // mBTService.beginListeningData(0);
                    } else {
                        LogUtil.i("TOR", "######## Makeing stopworker false ");
                        // mBTService.resetSocket();
                        mBTService.startReading();
                        // mBTService.beginListeningData(0);
                    }
                } else if (intent.hasExtra("BT_RETRY_FAIL")) {
                    initiateBT();
                }
            }
        };
        registerReceiver(mReceiver, intentFilter);
        initiateBT();
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        Swiper swipeDetector = new Swiper(getApplicationContext(),
                mainScrollBar);
        mainScrollBar.setOnTouchListener(swipeDetector);

    }

    private void initiateBT() {
        int status = mBTService.getPairedStatus();
        switch (status) {
            case 0:
                Toast.makeText(getApplicationContext(),
                        Constants.sLblMultiplePaired, Toast.LENGTH_LONG).show();
                break;
            case 1:
                task = new BTTask();
                task.execute();
                break;
            case 2:
                Toast.makeText(getApplicationContext(),
                        Constants.sLblPleasePairTLogik, Toast.LENGTH_LONG).show();
                break;
            case 3:
                Toast.makeText(getApplicationContext(),
                        Constants.sLblNoPairedDevices, Toast.LENGTH_LONG).show();
                break;
        }
    }

    class BTTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... f_url) {
            if (isCancelled()) {
                LogUtil.e("Regroove", "doInBackground Async Task Cancelled  > ");
                return "";
            }
            try {
                mBTService.connectToDevice(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (isCancelled()) {
                LogUtil.e("Regroove", "onPostExecute Async Task Cancelled  > ");
                return;
            }
            try {
                if (mBTService.isConnected()) {
                    afterNSKOne.setEnabled(false);
                    afterNSKTwo.setEnabled(false);
                    afterNSKThree.setEnabled(false);
                    setPressure.setEnabled(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        // unregisterReceiver(mReceiver);
        // mBTService.stop();
    }

    private void loadLabelsFromDB() {

        DatabaseAdapter label = new DatabaseAdapter(this);
        label.createDatabase();
        label.open();

        strbluetoothconnectionfalied = mDbHelper
                .getLabel(Constants.BLUETOOTH_CONNECTION_FAILED);

        String activityName = label.getLabel(Constants.REGROOV_ACTIVITY_NAME);
        TextView title = new TextView(this);
        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        title.setBackgroundColor(Color.BLACK);
        title.setText(activityName);
        setTitle(activityName);

        ((TextView) findViewById(R.id.lbl_tireDetails)).setText(label
                .getLabel(Constants.LABEL_TIRE_DETAILS));
        mNotesLabel = label.getLabel(Constants.NOTES_ACTIVITY_NAME);
        invalidateOptionsMenu();
        // header
        String labelFromDB = label.getLabel(Constants.PRESSURE_LABEL);
        pressureLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.SERIAL_NUMBER);
        serialNumberLabel.setText(labelFromDB);

        // labelFromDB = label.getLabel(Constants.BEFORE_LABEL);
        // wheelPositionLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.BEFORE_LABEL);
        beforeLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.AFTER_LABEL);
        afterLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.NSK1_LABEL);
        nskOneLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.NSK2_LABEL);
        nskTwoLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.NSK3_LABEL);
        nskThreeLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.REGROOVE_TYPE_LABEL);

        regrooveTypeLabel.setText(labelFromDB);
        labelFromDB = label.getLabel(Constants.REGROOVE_RIB_LABEL);
        mRibLabel = labelFromDB;
        regrooveTypeYes.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.REGROOVE_BLOCK_LABEL);
        mBlockLabel = labelFromDB;
        regrooveTypeNo.setText(labelFromDB);
        //

        labelFromDB = label.getLabel(Constants.DIMENSION_LABEL);
        dimensionLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.ASP_LABEL);
        aspLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.RIM_LABEL);
        rimLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.DESIGN_LABEL);
        designLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.SIZE_LABEL);
        sizeLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.BRAND_LABEL);
        brandLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.TYPE_LABEL);
        typeLabel.setText(labelFromDB);

        labelFromDB = label.getLabel(Constants.REGROOVE_TIRE_LABEL);

        nskOneBeforeRequired = label
                .getLabel(Constants.NSKONE_BEFORE_IS_REQUIRED_MESSAGE);
        nskTwoBeforeRequired = label
                .getLabel(Constants.NSKTWO_BEFORE_IS_REQUIRED_MESSAGE);
        nskThreeBeforeRequired = label
                .getLabel(Constants.NSKTHREE_BEFORE_IS_REQUIRED_MESSAGE);

        nskOneAfterRequired = label
                .getLabel(Constants.NSKONE_AFTER_IS_REQUIRED_MESSAGE);
        nskTwoAfterRequired = label
                .getLabel(Constants.NSKTWO_AFTER_IS_REQUIRED_MESSAGE);
        nskThreeAfterRequired = label
                .getLabel(Constants.NSKTHREE_AFTER_IS_REQUIRED_MESSAGE);

        nskOneCannotBeLessThenBefore = label
                .getLabel(Constants.NSKONE_CANNOT_BE_LESS_THEN_BEFORE);
        nskTwoCannotBeLessThenBefore = label
                .getLabel(Constants.NSKTWO_CANNOT_BE_LESS_THEN_BEFORE);
        nskThreeCannotBeLessThenBefore = label
                .getLabel(Constants.NSKTHREE_CANNOT_BE_LESS_THEN_BEFORE);

        pressureRequired = label.getLabel(Constants.PRESSURE_REQUIRED);

        backPressMessage = label.getLabel(Constants.CONFIRM_BACK);

        mYesLabel = label.getLabel(Constants.YES_LABEL);
        mNoLabel = label.getLabel(Constants.NO_LABEL);

        // dimensionLabel.setText(labelFromDB);
        // REGROOVE_IS_REQUIRED =
        // label.getLabel(Constants.REGROOVE_IS_REQUIRED);
        REGROOVE_IS_REQUIRED = label
                .getLabel(Constants.REGROOVE_TYPE_IS_REQUIRED);

        mLblTyreTypeRequired = label.getLabel(Constants.TYRE_TYPE_IS_REQUIRED);
        mLblBrandIsRequired = label.getLabel(Constants.TYRE_BRAND_IS_REQUIRED);
        labelFromDB = mDbHelper.getLabel(Constants.JOB_DETAILS);
        mJobDetailsLabel.setText(labelFromDB);
        labelFromDB = mDbHelper.getLabel(Constants.PLEASE_SELECT_LABEL);
        mDefaultValueForSpinner = labelFromDB;
        mPleaseSelectBrand = mDbHelper.getLabel(Constants.SELECT_BRAND);
        Constants.sLblCheckPressureValue = mDbHelper.getLabel(Constants.CHECK_PRESSURE_LABEL);
        label.close();
    }

    /**
     * Initialize the UI elements
     */
    private void initialize() {

        SharedPreferences preferences = getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);

        if (preferences.getString(Constants.PRESSUREUNIT, "").equals(
                Constants.PSI_PRESSURE_UNIT)) {
            isSettingsPSI = true;
        }
        mDesignArrayList = new ArrayList<String>();
        mFullDesignArrayList = new ArrayList<String>();
        mBrandArrayList = new ArrayList<String>();
        mSizeArrayList = new ArrayList<String>();
        mTyreTRIMArrayList = new ArrayList<String>();
        mTyreTASPArrayList = new ArrayList<String>();
        serialNumberLabel = (TextView) findViewById(R.id.lbl_serialNo);

        TextView wheelPositionLabel = (TextView) findViewById(R.id.lbl_wp);
        wheelPositionLabel.setText(Constants.sLblWheelPos);

        pressureLabel = (TextView) findViewById(R.id.lbl_pressure);
        beforeLabel = (TextView) findViewById(R.id.lbl_nskBefore);
        afterLabel = (TextView) findViewById(R.id.lbl_nskAfter);
        nskOneLabel = (TextView) findViewById(R.id.lbl_nsk1);
        nskTwoLabel = (TextView) findViewById(R.id.lbl_nsk2);
        nskThreeLabel = (TextView) findViewById(R.id.lbl_nsk3);
        regrooveTypeLabel = (TextView) findViewById(R.id.lbl_regrooveType);
        rgRegrooveType = (RadioGroup) findViewById(R.id.rg_regRooveType);
        regrooveTypeYes = (RadioButton) findViewById(R.id.value_regRooveYES);
        regrooveTypeNo = (RadioButton) findViewById(R.id.value_regRooveNO);
        //
        dimensionLabel = (TextView) findViewById(R.id.lbl_dimenssion);
        aspLabel = (TextView) findViewById(R.id.lbl_dimenssionASP);
        rimLabel = (TextView) findViewById(R.id.lbl_dimenssionRIM);
        designLabel = (TextView) findViewById(R.id.lbl_make);
        sizeLabel = (TextView) findViewById(R.id.lbl_dimenssionSiz);
        brandLabel = (TextView) findViewById(R.id.lbl_brand);
        typeLabel = (TextView) findViewById(R.id.lbl_type);
        mJobDetailsLabel = (TextView) findViewById(R.id.lbl_jobDetails);
        mBrand = (Spinner) findViewById(R.id.value_brand);
        size = (Spinner) findViewById(R.id.value_dimenssionSize);
        asp = (Spinner) findViewById(R.id.value_dimenssionASP);
        rim = (Spinner) findViewById(R.id.value_dimenssionRIM);
        design = (Spinner) findViewById(R.id.value_design);
        type = (Spinner) findViewById(R.id.value_type);
        serialNumber = (TextView) findViewById(R.id.value_serialNo);
        wheelPosition = (TextView) findViewById(R.id.value_wp_regroove);
        // CR:451
        mCamera_icon = (ImageView) findViewById(R.id.camera_icon);
        mCamera_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent photoIntent = new Intent(RegrooveTireOperation.this,
                        CameraActivity.class);
                photoIntent.putExtra("CURNT_IMAGE_COUNT",
                        CameraUtility.getImageCount());
                startActivity(photoIntent);
            }
        });

        beforeNSKOne = (EditText) findViewById(R.id.value_nsk1Before);
        beforeNSKOne
                .setFilters(new InputFilter[]{new DecimalDigitsInputFilterNSK(
                        3, 2)});

        beforeNSKTwo = (EditText) findViewById(R.id.value_nsk2Before);
        beforeNSKTwo
                .setFilters(new InputFilter[]{new DecimalDigitsInputFilterNSK(
                        3, 2)});

        beforeNSKThree = (EditText) findViewById(R.id.value_nsk3Before);
        beforeNSKThree
                .setFilters(new InputFilter[]{new DecimalDigitsInputFilterNSK(
                        3, 2)});

        afterNSKOne = (EditText) findViewById(R.id.value_nsk1After);
        // should be set to zero
        afterNSKOne.setText("0");
        // afterNSKOne.setFilters(new InputFilter[] { nskFilter,new
        // InputFilter.LengthFilter(DECIMAL_LENGTH) });
        afterNSKOne
                .setFilters(new InputFilter[]{new DecimalDigitsInputFilterNSK(
                        3, 2)});
        afterNSKOne.setSelection(afterNSKOne.getText().length());// Moving The
        // Cursor To
        // Right Of
        // Text
        afterNSKOne.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    // set the re groove object
                    regrooveInfo.setNsk1After(afterNSKOne.getText().toString());
                    // check if a NSK is less the before NSK
                    if (!afterNSKOne.getText().toString().equals("")) {
                        // set change made true;
                        changeMade = true;

                        float nskOneBefore = CommonUtils
                                .parseFloat(beforeNSKOne.getText().toString());
                        if (!(nskOneBefore >= CommonUtils
                                .parseFloat(afterNSKOne.getText().toString()))) {
                            regrooveInfo.setNsk1After(afterNSKOne.getText()
                                    .toString());
                            if (mIsOrientationChanged) {
                                return;
                            }

                            // change nsk 2 and 3
                            afterNSKTwo.setText(afterNSKOne.getText()
                                    .toString());
                            afterNSKThree.setText(afterNSKOne.getText()
                                    .toString());
                        } else {
                            afterNSKOne.setText("");
                            CommonUtils.notify(nskOneCannotBeLessThenBefore,
                                    getBaseContext());
                        }
                    }
                }
            }
        });

        // Moving The Cursor To Next Line(Starts)
        afterNSKOne.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    beforeNSKTwo.requestFocus();
                    handled = true;
                }
                return handled;
            }
        });

        // Moving The Cursor To Next Line(Ends)

        afterNSKTwo = (EditText) findViewById(R.id.value_nsk2After);
        afterNSKTwo.setText("0");
        afterNSKTwo
                .setFilters(new InputFilter[]{new DecimalDigitsInputFilterNSK(
                        3, 2)});
        afterNSKTwo.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    // set the re groove object
                    regrooveInfo.setNsk2After(afterNSKTwo.getText().toString());

                    // check if a NSK is less the before NSK
                    if (!afterNSKTwo.getText().toString().equals("")) {
                        // set change made true;
                        changeMade = true;

                        float nskOneBefore = CommonUtils
                                .parseFloat(beforeNSKTwo.getText().toString());
                        if (!(nskOneBefore >= CommonUtils
                                .parseFloat(afterNSKTwo.getText().toString()))) {
                            regrooveInfo.setNsk1After(afterNSKTwo.getText()
                                    .toString());
                        } else {
                            afterNSKTwo.setText("");
                            CommonUtils.notify(nskTwoCannotBeLessThenBefore,
                                    getBaseContext());
                        }
                    }
                }
            }
        });

        // Moving The Cursor To Next Line(Starts)
        afterNSKTwo.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    beforeNSKThree.requestFocus();
                    handled = true;
                }
                return handled;
            }
        });

        // Moving The Cursor To Next Line(Ends)

        afterNSKThree = (EditText) findViewById(R.id.value_nsk3After);
        afterNSKThree.setText("0");
        afterNSKThree
                .setFilters(new InputFilter[]{new DecimalDigitsInputFilterNSK(
                        3, 2)});
        afterNSKThree.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    // set the re groove object
                    regrooveInfo.setNsk3After(afterNSKThree.getText()
                            .toString());
                    // check if a NSK is less the before NSK
                    if (!afterNSKThree.getText().toString().equals("")) {
                        // set change made true;
                        changeMade = true;

                        float nskOneBefore = CommonUtils
                                .parseFloat(beforeNSKThree.getText().toString());
                        if (!(nskOneBefore >= CommonUtils
                                .parseDouble(afterNSKThree.getText().toString()))) {
                            regrooveInfo.setNsk1After(afterNSKThree.getText()
                                    .toString());
                        } else {
                            afterNSKThree.setText("");
                            CommonUtils.notify(nskThreeCannotBeLessThenBefore,
                                    getBaseContext());
                        }
                    }
                }
            }
        });

        // get regroove object
        regrooveInfo = mSelectedTire.getRegrooveInfo();
        pressureUnit = (TextView) findViewById(R.id.lbl_pressureunit);
        setPressure = (EditText) findViewById(R.id.value_pressure);
        mainScrollBar = (ScrollView) findViewById(R.id.parentnode);
        setValuetoPressueView();
        // update UI
        updateVauesToUIFromTire();
    }

    private void loadTyreDetails() {
        loadSelectedBrand();
        loadSelectedSize();
        loadSelectedASP();
        loadSelectedRIM();
        loadSelectedDesign();
        loadSelectedDesignDetails();
    }

    /**
     * Method loading and setting the size details from same axle for the
     * corresponding selected tire
     */
    private void loadSizeDetailsFromAxle() {
        loadSelectedSize();
        loadSelectedASP();
        loadSelectedRIM();
    }

    /**
     * set tire info on the screen and update the re groove object
     */
    private void updateVauesToUIFromTire() {
        isBlueToothAvailable = Validation.isBlueToothEnabled();
        // set values from tyre to UI
        mSelectedBrandName = mSelectedTire.getBrandName();
        mSelectedtyreSize = mSelectedTire.getSize();
        mSelectedTyreTASP = mSelectedTire.getAsp();
        mSelectedtyreTRIM = mSelectedTire.getRim();
        mSelectedTyreDesign = mSelectedTire.getDesign();
        mSelectedTyreDetailedDesign = mSelectedTire.getDesignDetails();

        if ("".equals(mSelectedTire.getSerialNumber())) {
            serialNumber.setText(Constants.EDITED_SERIAL_NUMBER);
        } else {
            serialNumber.setText(mSelectedTire.getSerialNumber());
        }
        wheelPosition.setText(mSelectedTire.getPosition());

        beforeNSKOne.setText(mSelectedTire.getNsk());
        // update re groove object
        regrooveInfo.setNsk1Before(beforeNSKOne.getText().toString());

        beforeNSKTwo.setText(mSelectedTire.getNsk2());
        // update re groove object
        regrooveInfo.setNsk2Before(beforeNSKTwo.getText().toString());

        beforeNSKThree.setText(mSelectedTire.getNsk3());
        // update re groove object
        regrooveInfo.setNsk3Before(beforeNSKThree.getText().toString());

        // update re groove type
        regrooveInfo.setReGrooveType(Constants.REGROOVE_RIB);

        beforeNSKOne.addTextChangedListener(new nskOneBeforeChanged());
        beforeNSKTwo.addTextChangedListener(new nskTwoBeforeChanged());
        beforeNSKThree.addTextChangedListener(new nskThreeBeforeChanged());
        afterNSKOne.addTextChangedListener(new nskOneAfterChanged());
        afterNSKTwo.addTextChangedListener(new nskTwoAfterChanged());
        afterNSKThree.addTextChangedListener(new nskThreeAfterChanged());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.regroove_tire_operation, menu);
        if (mNotesLabel != null) {
            menu.findItem(R.id.action_save).setTitle(mNotesLabel);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (areChangesMade()) {
            createDialogOnBackPress();
        } else {
            LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press", false, false, false);
            super.onBackPressed();
        }
    }

    ;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_save) {
            LogUtil.TraceInfo(TRACE_TAG, "Option", "Save", false, true, false);
            Intent startNotesActivity = new Intent(this, NoteActivity.class);
            startNotesActivity.putExtra("sentfrom", "regroove");
            startNotesActivity.putExtra("lastnote", noteText);
            startActivityForResult(startNotesActivity,
                    RegrooveTireOperation.NOTES_INTENT);
        }
        return super.onOptionsItemSelected(item);
    }

    private class RegrooveTypeChangeHandler implements OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            if (isChecked) {
                regrooveInfo.setReGrooveType(Constants.REGROOVE_RIB);
            } else {
                regrooveInfo.setReGrooveType(Constants.REGROOVE_BLOCK);
            }
        }

    }

    private class nskThreeBeforeChanged implements TextWatcher {

        @Override
        public void afterTextChanged(Editable nskThree) {
            regrooveInfo.setNsk3Before(nskThree.toString());
            changeMade = true;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            String strEnteredVal = beforeNSKThree.getText().toString();
            if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
                Float num = CommonUtils.parseFloat(strEnteredVal);
                if (num <= 30) {
                    // Toast.makeText(getApplicationContext(),"Less than and equal to 30.",
                    // Toast.LENGTH_LONG).show();
                } else {
                    // Toast.makeText(getApplicationContext(),"Greater than 30.",
                    // Toast.LENGTH_LONG).show();
                    beforeNSKThree.setText("");
                }
            } else if (strEnteredVal.equals(".")) {
                beforeNSKThree.setText("");
            }

        }
    }

    private class nskOneBeforeChanged implements TextWatcher {

        @Override
        public void afterTextChanged(Editable nskOne) {
            changeMade = true;
            // set before nsk when nsk1 is changed
            beforeNSKTwo.setText(nskOne.toString());
            beforeNSKThree.setText(nskOne.toString());

            regrooveInfo.setNsk1Before(nskOne.toString());
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            String strEnteredVal = beforeNSKOne.getText().toString();
            if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
                Float num = CommonUtils.parseFloat(strEnteredVal);
                if (num <= 30) {
                    // Toast.makeText(getApplicationContext(),"Less than and equal to 30.",
                    // Toast.LENGTH_LONG).show();
                } else {
                    // Toast.makeText(getApplicationContext(),"Greater than 30.",
                    // Toast.LENGTH_LONG).show();
                    beforeNSKOne.setText("");
                }
            } else if (strEnteredVal.equals(".")) {
                beforeNSKOne.setText("");
            }

        }
    }

    private class nskOneAfterChanged implements TextWatcher {

        @Override
        public void afterTextChanged(Editable nskOne) {
            changeMade = true;
            // set before nsk when nsk1 is changed
            afterNSKTwo.setText(nskOne.toString());
            afterNSKThree.setText(nskOne.toString());
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            String strEnteredVal = afterNSKOne.getText().toString();
            if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
                Float num = CommonUtils.parseFloat(strEnteredVal);
                if (num <= 30) {
                    // Toast.makeText(getApplicationContext(),"Less than and equal to 30.",
                    // Toast.LENGTH_LONG).show();
                } else {
                    // Toast.makeText(getApplicationContext(),"Greater than 30.",
                    // Toast.LENGTH_LONG).show();
                    afterNSKOne.setText("");
                }
            } else if (strEnteredVal.equals(".")) {
                afterNSKOne.setText("");
            }

        }
    }

    private class nskTwoAfterChanged implements TextWatcher {

        @Override
        public void afterTextChanged(Editable nskOne) {
            changeMade = true;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            String strEnteredVal = afterNSKTwo.getText().toString();
            if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
                Float num = CommonUtils.parseFloat(strEnteredVal);
                if (num <= 30) {
                    // Toast.makeText(getApplicationContext(),"Less than and equal to 30.",
                    // Toast.LENGTH_LONG).show();
                } else {
                    // Toast.makeText(getApplicationContext(),"Greater than 30.",
                    // Toast.LENGTH_LONG).show();
                    afterNSKTwo.setText("");
                }
            } else if (strEnteredVal.equals(".")) {
                afterNSKTwo.setText("");
            }

        }
    }

    private class nskThreeAfterChanged implements TextWatcher {

        @Override
        public void afterTextChanged(Editable nskOne) {
            changeMade = true;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            String strEnteredVal = afterNSKThree.getText().toString();
            if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
                Float num = CommonUtils.parseFloat(strEnteredVal);
                if (num <= 30) {
                    // Toast.makeText(getApplicationContext(),"Less than and equal to 30.",
                    // Toast.LENGTH_LONG).show();
                } else {
                    // Toast.makeText(getApplicationContext(),"Greater than 30.",
                    // Toast.LENGTH_LONG).show();
                    afterNSKThree.setText("");
                }
            } else if (strEnteredVal.equals(".")) {
                afterNSKThree.setText("");
            }

        }
    }

    private class nskTwoBeforeChanged implements TextWatcher {

        @Override
        public void afterTextChanged(Editable nskTwo) {
            regrooveInfo.setNsk2Before(nskTwo.toString());
            changeMade = true;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            String strEnteredVal = beforeNSKTwo.getText().toString();
            if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
                Float num = CommonUtils.parseFloat(strEnteredVal);
                if (num <= 30) {
                    // Toast.makeText(getApplicationContext(),"Less than and equal to 30.",
                    // Toast.LENGTH_LONG).show();
                } else {
                    // Toast.makeText(getApplicationContext(),"Greater than 30.",
                    // Toast.LENGTH_LONG).show();
                    beforeNSKTwo.setText("");
                }
            } else if (strEnteredVal.equals(".")) {
                beforeNSKTwo.setText("");
            }

        }
    }

    InputFilter nskFilter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence changedText, int arg1,
                                   int arg2, Spanned oldText, int arg4, int arg5) {
            if (!(oldText.toString() + changedText).equals("")) {
                double nskValue = CommonUtils.parseDouble(oldText.toString()
                        + changedText);
                if (nskValue > 30) {
                    return "";
                }
            }
            return null;
        }
    };

    /**
     * @author amitkumar.h
     *         Class providing the animation when user swipes out
     *         from the activity It also handles the actions performed when use
     *         is swiping out from the activity after matching all the required
     *         conditions
     */
    public class Swiper implements OnTouchListener, OnClickListener {
        float startX, startY;
        float endX, endY;
        int selectedPositionToDelete;
        ArrayAdapter<String> adapterList;
        ScrollView view;
        private Context ctx;
        public static final float MINIMUM_MOVEMENT_REQUIRED = 100;

        public Swiper(Context ctx, ScrollView view) {
            this.ctx = ctx;
            this.view = view;
            view.setOnClickListener(this);
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Log.i("Sandeep", "Inside touch event");
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    swiped = false;
                    startX = event.getX();
                    startY = event.getY();
                    break;
                case MotionEvent.ACTION_UP:

                    break;

                case MotionEvent.ACTION_MOVE:
                    endX = event.getX();
                    endY = event.getY();

                    if (Math.abs(endX - startX) > MINIMUM_MOVEMENT_REQUIRED
                            && !swiped) {
                        swiped = true;
                        Log.i("Sandeep", "swipe detected");
                        if (mBrand.getSelectedItem().toString()
                                .equalsIgnoreCase(mPleaseSelectBrand)) {
                            if(mLblBrandIsRequired!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "Brand Validation","Msg : " +mLblBrandIsRequired,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "Brand Validation","Msg : Check Brand",false,false,false);
                            }
                            CommonUtils.notify(mLblBrandIsRequired,
                                    getBaseContext());
                        } else if (type.getSelectedItemPosition() < 0
                                || type.getSelectedItem().toString()
                                .equals(mDefaultValueForSpinner)) {
                            if(mLblTyreTypeRequired!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "Type Validation","Msg : " +mLblTyreTypeRequired,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "Type Validation","Msg : Check Type",false,false,false);
                            }
                            CommonUtils.notify(mLblTyreTypeRequired,
                                    getBaseContext());
                        } else if (!regrooveTypeYes.isChecked()
                                && !regrooveTypeNo.isChecked()) {
                            if(REGROOVE_IS_REQUIRED!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "Regroove Validation","Msg : " +REGROOVE_IS_REQUIRED,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "Type Validation","Msg : Check Regroove",false,false,false);
                            }
                            CommonUtils.notify(REGROOVE_IS_REQUIRED,
                                    getBaseContext());
                        } else if (checkIfEmptyOrZero(afterNSKOne.getText()
                                .toString())) {
                            if(nskOneAfterRequired!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK one After Validation","Msg : " +nskOneAfterRequired,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK one After Validation","Msg : Check NSK one After",false,false,false);
                            }
                            CommonUtils.notify(nskOneAfterRequired,
                                    getBaseContext());
                        } else if (checkIfEmptyOrZero(afterNSKTwo.getText()
                                .toString())) {
                            if(nskTwoAfterRequired!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Two After Validation","Msg : " +nskTwoAfterRequired,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Two After Validation","Msg : Check NSK Two After",false,false,false);
                            }
                            CommonUtils.notify(nskTwoAfterRequired,
                                    getBaseContext());
                        } else if (checkIfEmptyOrZero(afterNSKThree.getText()
                                .toString())) {
                            if(nskThreeAfterRequired!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Three After Validation","Msg : " +nskThreeAfterRequired,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Three After Validation","Msg : Check NSK Three After",false,false,false);
                            }
                            CommonUtils.notify(nskThreeAfterRequired,
                                    getBaseContext());
                        } else if (checkIfEmptyOrZero(beforeNSKOne.getText()
                                .toString())) {
                            if(nskOneBeforeRequired!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK One Before Validation","Msg : " +nskOneBeforeRequired,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK One Before Validation","Msg : Check NSK One Before",false,false,false);
                            }
                            CommonUtils.notify(nskOneBeforeRequired,
                                    getBaseContext());
                        } else if (checkIfEmptyOrZero(beforeNSKTwo.getText()
                                .toString())) {
                            if(nskTwoBeforeRequired!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Two Before Validation","Msg : " +nskTwoBeforeRequired,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Two Before Validation","Msg : Check NSK Two Before",false,false,false);
                            }
                            CommonUtils.notify(nskTwoBeforeRequired,
                                    getBaseContext());
                        } else if (checkIfEmptyOrZero(beforeNSKThree.getText()
                                .toString())) {
                            if(nskThreeBeforeRequired!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Three Before Validation","Msg : " +nskThreeBeforeRequired,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Three Before Validation","Msg : Check NSK Three Before",false,false,false);
                            }
                            CommonUtils.notify(nskThreeBeforeRequired,
                                    getBaseContext());
                        } else if (setPressure.getText().toString().equals("")
                                || setPressure.getText().toString().equals(" ")) {
                            if(pressureRequired!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "Pressure Validation","Msg : " +pressureRequired,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "Pressure Validation","Msg : Check Pressure",false,false,false);
                            }
                            CommonUtils.notify(pressureRequired, getBaseContext());
                        } else if (!CommonUtils
                                .pressureValueValidation(setPressure)) {
                            if(Constants.sLblCheckPressureValue!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "Pressure Value Validation","Msg : " +Constants.sLblCheckPressureValue,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "Pressure Value Validation","Msg : Check Pressure Value",false,false,false);
                            }
                            CommonUtils.notify(Constants.sLblCheckPressureValue,
                                    getBaseContext());
                        } else if ((CommonUtils.parseFloat(beforeNSKOne.getText()
                                .toString()) >= CommonUtils.parseFloat(afterNSKOne
                                .getText().toString()))) {

                            afterNSKOne.setText("");

                            if(nskOneCannotBeLessThenBefore!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Diff Value Validation","Msg : " +nskOneCannotBeLessThenBefore,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Diff Value Validation","Msg : Check NSK Value : nsk One Cannot Be Less Than Before",false,false,false);
                            }
                            CommonUtils.notify(nskOneCannotBeLessThenBefore,
                                    getBaseContext());

                        } else if ((CommonUtils.parseDouble(beforeNSKTwo.getText()
                                .toString()) >= CommonUtils.parseFloat(afterNSKTwo
                                .getText().toString()))) {

                            afterNSKTwo.setText("");
                            if(nskTwoCannotBeLessThenBefore!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Diff Value Validation","Msg : " +nskTwoCannotBeLessThenBefore,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Diff Value Validation","Msg : Check NSK Value : nsk Two Cannot Be Less Than Before",false,false,false);
                            }
                            CommonUtils.notify(nskTwoCannotBeLessThenBefore,
                                    getBaseContext());

                        } else if ((CommonUtils.parseFloat(beforeNSKThree.getText()
                                .toString()) >= CommonUtils
                                .parseFloat(afterNSKThree.getText().toString()))) {

                            afterNSKThree.setText("");
                            if(nskThreeCannotBeLessThenBefore!=null)
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Diff Value Validation","Msg : " +nskThreeCannotBeLessThenBefore,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_TAG, "NSK Diff Value Validation","Msg : Check NSK Value : nsk Three Cannot Be Less Than Before",false,false,false);
                            }
                            CommonUtils.notify(nskThreeCannotBeLessThenBefore,
                                    getBaseContext());

                        } else {
                            new PerformanceBaseModel(RegrooveTireOperation.this,
                                    callback, false).perform();
                            // onReturnToSkeleton();
                        }
                    }
                    break;
            }
            return false;
        }

        @Override
        public void onClick(View arg0) {
        }
    }

    /**
     * check if empty or zero
     *
     * @param value
     * @return
     */
    private boolean checkIfEmptyOrZero(String value) {
        float value_float;
        if (TextUtils.isEmpty(value)) {
            value_float = 0;
        } else {
            value_float = CommonUtils.parseFloat(value);
        }
        if (value_float <= 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method handling the data-updates for the tables(JobItem, tire,
     * JobCorrection etc) for the selected tire. It checks all the mandatory
     * fields then allows user to navigate back to Vehicle Skeleton after
     * updating the data
     */
    private void onReturnToSkeleton() {

        // To hide the keyboard
        try {
            CommonUtils.hideKeyboard(this, getWindow().getDecorView()
                    .getRootView().getWindowToken());
        } catch (Exception e) {
            LogUtil.i("TOR on activity return", "could not close keyboard");
        }

        // set swiped done
        swiped = true;

        // set the tire re groove status to true
        mSelectedTire.setReGrooved(true);

        mFinalConvertedPressure = CommonUtils.getFinalConvertedPressureValue(
                recommendedpressure, setPressure.getText().toString(),
                isSettingsPSI);

        // updating nsk values to RegrooveInfo
        regrooveInfo.setNsk1After(afterNSKOne.getText().toString());
        regrooveInfo.setNsk2After(afterNSKTwo.getText().toString());
        regrooveInfo.setNsk3After(afterNSKThree.getText().toString());

        // udpdate selected tire nsk and pressrue
        Constants.SELECTED_TYRE.setRegrooveInfo(regrooveInfo);
        Constants.SELECTED_TYRE.setNsk(afterNSKOne.getText().toString());
        Constants.SELECTED_TYRE.setNsk2(afterNSKTwo.getText().toString());
        Constants.SELECTED_TYRE.setNsk3(afterNSKThree.getText().toString());
        Constants.SELECTED_TYRE.setPressure(mFinalConvertedPressure);

        updatePressureValue();
        Constants.SELECTED_TYRE.setBrandName(mBrand.getSelectedItem()
                .toString());
        Constants.SELECTED_TYRE.setSize(size.getSelectedItem().toString());
        Constants.SELECTED_TYRE.setAsp(asp.getSelectedItem().toString());
        Constants.SELECTED_TYRE.setRim(rim.getSelectedItem().toString());
        Constants.SELECTED_TYRE.setDesign(design.getSelectedItem().toString());
        Constants.SELECTED_TYRE.setDesignDetails(type.getSelectedItem()
                .toString());
        Constants.SELECTED_TYRE.setNsk(afterNSKOne.getText().toString());
        Constants.SELECTED_TYRE.setNsk2(afterNSKTwo.getText().toString());
        Constants.SELECTED_TYRE.setNsk3(afterNSKThree.getText().toString());
        Constants.SELECTED_TYRE.setOrignalNSK(CommonUtils.getMinNskValue(
                beforeNSKOne.getText().toString(), beforeNSKTwo.getText()
                        .toString(), beforeNSKThree.getText().toString()));
        updateJobItem();
        //Condition aaded to check and update inspection data for the selected tire
        if(!Constants.SELECTED_TYRE.isInspected()) {
            InspectionDataHandler.getMyInstance(getApplicationContext()).updateJobitemForInspection(Constants.SELECTED_TYRE,false);
        }

        //User Trace logs
        try {
            String traceData;
            traceData = "\n\t\tTyre Details : " + Constants.SELECTED_TYRE.getBrandName() +
                    "| " + Constants.SELECTED_TYRE.getSize() +
                    "| " + Constants.SELECTED_TYRE.getAsp() +
                    "| " + Constants.SELECTED_TYRE.getRim() +
                    "| " + Constants.SELECTED_TYRE.getDesign() +
                    "| " + Constants.SELECTED_TYRE.getDesignDetails() +
                    "\n\t\tJob Details : ";

            if(regrooveInfo.getReGrooveType()==1)
            {
                traceData = traceData+ "RIB | ";
            }
            else
            {
                traceData = traceData+ "Block | ";
            }

            traceData = traceData+"NSK Before - "+
                    ", " + beforeNSKOne.getText().toString() +
                    ", " + beforeNSKTwo.getText().toString() +
                    ", " + beforeNSKThree.getText().toString() +
                    "| NSK After - " + Constants.SELECTED_TYRE.getNsk() +
                    ", " + Constants.SELECTED_TYRE.getNsk2() +
                    ", " + Constants.SELECTED_TYRE.getNsk3() +
                    "| " +Constants.SELECTED_TYRE.getPressure();


            traceData = traceData+	"| " + Constants.SELECTED_TYRE.getRimType();
            LogUtil.TraceInfo(TRACE_TAG, "none", "Data : " + traceData, false, false, false);
        }
        catch (Exception e)
        {
            LogUtil.TraceInfo(TRACE_TAG, "Data : Exception : ", e.getMessage(), false, true, false);
        }


        // close keyboard if open
        try {
            CommonUtils.hideKeyboard(this, getWindow().getDecorView()
                    .getRootView().getWindowToken());
        } catch (Exception e) {
            LogUtil.i("Mount PWT on activity return",
                    "could not close keyboard");
        }
        getSAPMaterialCodeID();
        if (TextUtils.isEmpty(Constants.SELECTED_TYRE.getSerialNumber())) {
            Constants.SELECTED_TYRE
                    .setSerialNumber(Constants.EDITED_SERIAL_NUMBER);
        }
        Constants.SELECTED_TYRE
                .setSapMaterialCodeID(mSelectedSAPMaterialCodeID);

    }

    /**
     * Method Updating Data for the selected tire in JobItem table with
     * the corresponding Regroove ActionType
     */
    private void updateJobItem() {
		try {
			LogUtil.DBLog(TAG,"JobItem Data","Insertion Initialized");
        external_id = String.valueOf(UUID.randomUUID());
        int jobID = getJobItemIDForThisJobItem();
        int len = VehicleSkeletonFragment.mJobItemList.size();
        JobItem jobItem = new JobItem();
        jobItem.setActionType("4"); // 4 for re-groove
        jobItem.setAxleNumber("0"); // for axle service
        jobItem.setAxleServiceType("0"); // for axle service
        jobItem.setCasingRouteCode(""); // for dismount tire
        jobItem.setDamageId("");
        jobItem.setExternalId(external_id);//
        jobItem.setGrooveNumber(null);
        jobItem.setJobId(String.valueOf(jobID));
        jobItem.setMinNSK(CommonUtils.getMinNskValue(afterNSKOne.getText()
                .toString(), afterNSKTwo.getText().toString(), afterNSKThree
                .getText().toString()));
        jobItem.setNote(noteText);
        jobItem.setNskOneAfter(afterNSKOne.getText().toString());
        jobItem.setNskOneBefore(beforeNSKOne.getText().toString());
        jobItem.setNskThreeAfter(afterNSKThree.getText().toString());
        jobItem.setNskThreeBefore(beforeNSKThree.getText().toString());
        jobItem.setNskTwoAfter(afterNSKTwo.getText().toString());
        jobItem.setNskTwoBefore(beforeNSKTwo.getText().toString());
        jobItem.setOperationID("");
        jobItem.setPressure(mFinalConvertedPressure);
        // recommended pressure for axle
        jobItem.setRecInflactionDestination(String.valueOf(recommendedpressure));
        // pressure from JSON
        jobItem.setRecInflactionOrignal("0");
        jobItem.setRegrooved("False"); // true false
        jobItem.setReGrooveNumber(null);// alway null
        jobItem.setRegrooveType(String.valueOf(regrooveInfo.getReGrooveType()));
        jobItem.setRemovalReasonId("");
        jobItem.setRepairCompany(null);
        jobItem.setRimType("0"); // for tor
        jobItem.setSapCodeTilD("0");
        jobItem.setSequence(String.valueOf(len + 1));
        jobItem.setServiceCount("0");// addtinal servcies
        jobItem.setServiceID("0");// axle additional
        jobItem.setSwapType("0");// swap
        jobItem.setThreaddepth("0");// tire mgmt
        jobItem.setTorqueSettings(""); // update from retorque
        jobItem.setTyreID(mSelectedTire.getExternalID());
        jobItem.setWorkOrderNumber(null);

        // Fix:: Edited serial no was not coming, so added serial no here. on
        // 15-11
        jobItem.setSerialNumber(Constants.SELECTED_TYRE.getSerialNumber());
        jobItem.setBrandName(Constants.SELECTED_TYRE.getBrandName());
        jobItem.setFullDesignDetails(Constants.SELECTED_TYRE.getDesignDetails());
        // Till Here
		LogUtil.DBLog(TAG,"Job Item Data","Inserted"+jobItem.toString() );
        VehicleSkeletonFragment.mJobItemList.add(jobItem);
        noteText = "";
        updateTireImages(String.valueOf(jobID));
	} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtil.DBLog(TAG,"Job Item Data","Exception--"+e.getMessage());
			e.printStackTrace();
		}
    }

    /**
     * inserting tire images in TyreImage Object
     */
    private void updateTireImages(String id) {
        if (Constants.sBitmapPhoto1 != null) {
            TireImageItem item1 = new TireImageItem();
            item1.setJobItemId(id);
            if (mCameraEnabled)
                item1.setImageType("0");
            else
                item1.setImageType("1");
            item1.setDamageDescription(Constants.DAMAGE_NOTES1);
            item1.setTyreImage(CameraUtility.getByteFromBitmap(Constants.sBitmapPhoto1,
                    Constants.sBitmapPhoto1Quality));
            item1.setJobExternalId(external_id);
            VehicleSkeletonFragment.mTireImageItemList.add(item1);
        }
        if (Constants.sBitmapPhoto2 != null) {
            TireImageItem item2 = new TireImageItem();
            item2.setJobItemId(id);
            item2.setImageType("1");
            item2.setDamageDescription(Constants.DAMAGE_NOTES2);
            item2.setTyreImage(CameraUtility.getByteFromBitmap(Constants.sBitmapPhoto2,
                    Constants.sBitmapPhoto2Quality));
            item2.setJobExternalId(external_id);
            VehicleSkeletonFragment.mTireImageItemList.add(item2);
        }
        if (Constants.sBitmapPhoto3 != null) {
            TireImageItem item3 = new TireImageItem();
            item3.setJobItemId(id);
            item3.setImageType("1");
            item3.setDamageDescription(Constants.DAMAGE_NOTES3);
            item3.setTyreImage(CameraUtility.getByteFromBitmap(Constants.sBitmapPhoto3,
                    Constants.sBitmapPhoto3Quality));
            item3.setJobExternalId(external_id);
            VehicleSkeletonFragment.mTireImageItemList.add(item3);
        }
    }

    /**
     * Method returning maximum count of Job present in the JobItem table
     *
     * @return: Max Job Count
     */
    private int getJobItemIDForThisJobItem() {
        int countJobItemsNotPresent = 0;
        mDbHelper.open();
        int currentJobItemCountInDB = mDbHelper.getJobItemCount();
        Cursor cursor = null;
        try {
            cursor = mDbHelper.getJobItemValues();
            if (!CursorUtils.isValidCursor(cursor)) {
                return countJobItemsNotPresent + 1
                        + VehicleSkeletonFragment.mJobItemList.size();
            }
            for (JobItem jobItem : VehicleSkeletonFragment.mJobItemList) {
                boolean flag = false;
                for (boolean hasItem = cursor.moveToFirst(); hasItem; hasItem = cursor
                        .moveToNext()) {
                    if (jobItem.getExternalId().equals(
                            cursor.getString(cursor
                                    .getColumnIndex("ExternalID")))) {
                        countJobItemsNotPresent++;
                        break;
                    }
                }
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            mDbHelper.close();
            // CursorUtils.closeCursor(cursor);
        }
        return currentJobItemCountInDB - countJobItemsNotPresent
                + VehicleSkeletonFragment.mJobItemList.size() + 1;
    }

    private String minNSKSet() {
        float smallest;
        float a = CommonUtils.parseFloat(afterNSKOne.getText().toString());
        float b = CommonUtils.parseFloat(afterNSKTwo.getText().toString());
        float c = CommonUtils.parseFloat(afterNSKThree.getText().toString());
        if (a < b && a < c) {
            smallest = a;
        } else if (b < c && b < a) {
            smallest = b;
        } else {
            smallest = c;
        }
        return String.valueOf(smallest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RegrooveTireOperation.NOTES_INTENT) {
            if (data != null) {
                noteText = data.getExtras().getString("mNoteEditText");
            }
        }
    }

    private AlertDialog mBackAlertDialog;

    /**
     * Method Capturing the dialog state before orientation change
     */
    private void saveDialogState(Bundle state) {
        state.putBoolean("mBackAlertDialog",
                (mBackAlertDialog != null && mBackAlertDialog.isShowing()));
    }

    /**
     * Method Capturing the dialog state after orientation change
     */
    private void restoreDialogState(Bundle state) {
        if (state != null) {
            if (state.getBoolean("mBackAlertDialog")) {
                createDialogOnBackPress();
            }
        }
    }

    @Override
    protected void onDestroy() {
        try {
            if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
                mBackAlertDialog.dismiss();
            }
            if (mDbHelper != null) {
                mDbHelper.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    /**
     * Method handling the action performed where user presses the back button
     * Alert Box created with two option: YES and NO
     */
    private void createDialogOnBackPress() {
        // if (changeMade) {//as per the issues (1.Back navigation always asks
        // for cancellation. In this case it is asking for saving. This is
        // wrong, 2.Sometimes back navigation doesn't ask anything. Simply
        // navigates back to the skeleton screen.)
        LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", false, true, false);
        LayoutInflater li = LayoutInflater.from(RegrooveTireOperation.this);
        View promptsView = li
                .inflate(R.layout.dialog_logout_confirmation, null);

        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
                RegrooveTireOperation.this);
        alertDialogBuilder.setView(promptsView);

        // update user activity for dialog layout
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // update user activity on button click
                alertDialogBuilder.updateInactivityForDialog();

                InactivityUtils.updateActivityOfUser();
            }
        });

        // Setting The Info
        TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
        // infoTv.setText(Constants.CONFIRM_SAVE);// as per the issue it should
        // not ask for save, it should ask for cancel dialogue, issue:(Back
        // navigation always asks for cancellation. In this case it is asking
        // for saving. This is wrong.)
        infoTv.setText(backPressMessage);

        alertDialogBuilder.setCancelable(false).setPositiveButton(mYesLabel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // update user activity on button click
                        alertDialogBuilder.updateInactivityForDialog();
                        LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
                        // if this button is clicked, close
                        // current activity
                        // onReturnToSkeleton();as per the issue it should not
                        // ask for save, it should ask for cancel dialogue,
                        // issue:(Back navigation always asks for cancellation.
                        // In this case it is asking for saving. This is wrong.)
                        Constants.JOB_CORRECTION_LIST = VehicleSkeletonFragment.mJobcorrectionList;
                        CameraUtility.resetParams();
                        finish();

                    }
                });

        alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // update user activity on button click
                        alertDialogBuilder.updateInactivityForDialog();
                        LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);
                        // Intent intentReturn = new Intent();
                        // intentReturn.putExtra("regroove", false);
                        // setResult(,
                        // intentReturn);
                        // finish();
                        dialog.cancel();
                    }
                });
        // create alert dialog
        mBackAlertDialog = alertDialogBuilder.create();

        // show it
        mBackAlertDialog.show();
        mBackAlertDialog.setCanceledOnTouchOutside(false);
    }

    /**
     * Method Getting Tire SAPMaterialCodeID from local DB3 file as per the
     * selected DetailedDesign
     */
    public void getSAPMaterialCodeID() {
        Cursor mCursor = null;
        try {
            mDbHelper.open();
            mCursor = mDbHelper.getSAPMaterialCodeID(mSelectedtyreSize,
                    mSelectedTyreTASP, mSelectedtyreTRIM, mSelectedTyreDesign,
                    mSelectedTyreDetailedDesign, mSelectedBrandName);
            if (CursorUtils.isValidCursor(mCursor)) {
                mCursor.moveToFirst();
                mSelectedSAPMaterialCodeID = mCursor.getString(
                        mCursor.getColumnIndex("ID")).toString();
                CursorUtils.closeCursor(mCursor);
                if (TextUtils
                        .isEmpty(Constants.SELECTED_TYRE.getSerialNumber())) {
                    if (Constants.onBrandBool == true
                            || Constants.onBrandBoolForJOC == true) {
                        Constants.NEW_SAPMATERIAL_CODE = mSelectedSAPMaterialCodeID;
                        VehicleSkeletonFragment.updateCorrectedBrandInDB();
                    }
                    if (!Constants.SELECTED_TYRE.getSerialNumber()
                            .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)) {
                        VehicleSkeletonFragment
                                .updateCorrectedSerialNumberInDB();
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CursorUtils.closeCursor(mCursor);
            // mDbHelper.close();
        }
    }

    /**
     * loading data for in Spinners
     */
    private void loadSelectedBrand() {
        mBrandArrayList.clear();
        mBrandArrayList.add(mSelectedTire.getBrandName());
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mBrandArrayList);
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBrand.setAdapter(dataAdapter);
        mBrand.setEnabled(false);
    }

    /**
     * Method loading tire-size on UI element as per the selected tire
     */
    private void loadSelectedSize() {
        mSizeArrayList.clear();
        mSizeArrayList.add(mSelectedtyreSize);
        mSizeDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mSizeArrayList);
        mSizeDataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        size.setAdapter(mSizeDataAdapter);
        if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED) {
            size.setEnabled(true);
        }else{
            size.setEnabled(false);
        }
    }

    /**
     * Method loading tire-ASP on UI element as per the selected tire
     */
    private void loadSelectedASP() {
        mTyreTASPArrayList.clear();
        mTyreTASPArrayList.add(mSelectedTyreTASP);
        mASPDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mTyreTASPArrayList);
        mASPDataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        asp.setAdapter(mASPDataAdapter);
        if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED) {
            asp.setEnabled(true);
        }else{
            asp.setEnabled(false);
        }
    }

    /**
     * Method loading tire-RIM on UI element as per the selected tire
     */
    private void loadSelectedRIM() {
        mTyreTRIMArrayList.clear();
        mTyreTRIMArrayList.add(mSelectedtyreTRIM);
        mRimDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
        mRimDataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rim.setAdapter(mRimDataAdapter);
        rim.setEnabled(false);
    }

    /**
     * Method loading tire-Design on UI element as per the selected tire
     */
    private void loadSelectedDesign() {
        mDesignArrayList.clear();
        mDesignArrayList.add(mSelectedTire.getDesign());
        mDesignDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mDesignArrayList);
        mDesignDataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        design.setAdapter(mDesignDataAdapter);
        design.setEnabled(false);
    }

    /**
     * Method loading tire-Design-Details on UI element as per the selected tire
     */
    private void loadSelectedDesignDetails() {
        mFullDesignArrayList.clear();
        mFullDesignArrayList.add(mSelectedTire.getDesignDetails());
        mFullDesignDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mFullDesignArrayList);
        mFullDesignDataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type.setAdapter(mFullDesignDataAdapter);
        type.setEnabled(false);
    }

    /**
     * Method loading tire BrandName from the local DB3 file and populating on
     * UI element as per the selected tire
     */
    private void loadTyreDetailsOnBrandCorrection() {
        Cursor mCursor = null;
        try {
            if (null != mDbHelper) {
                mDbHelper.open();
            }
            mCursor = mDbHelper.getBrandNameForSWAP();
            if (!CursorUtils.isValidCursor(mCursor)) {
                return;
            }
            mCursor.moveToFirst();
            mBrandArrayList.clear();
            mBrandArrayList.add(mPleaseSelectBrand);
            while (!mCursor.isAfterLast()) {
                mBrandArrayList.add(mCursor.getString(mCursor
                        .getColumnIndex("Description")));
                mCursor.moveToNext();
            }
            CursorUtils.closeCursor(mCursor);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, mBrandArrayList);
            dataAdapter
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mBrand.setAdapter(dataAdapter);
            mBrand.setEnabled(true);
            if (mSelectedTire.getSerialNumber().equals(
                    Constants.EDITED_SERIAL_NUMBER)) {
                for (int i = 0; i < mBrandArrayList.size(); i++) {
                    if (mBrandArrayList.get(i).equals(mSelectedBrandName)) {
                        mBrand.setSelection(i);
                        break;
                    }
                }
            }
            mBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView adapter, View v, int i,
                                           long lng) {
                    mBrand.setOnItemSelectedListener(listenerSelectBrandName);
                }

                @Override
                public void onNothingSelected(AdapterView arg0) {
                    Toast.makeText(getApplicationContext(),
                            Constants.sLblNothingSelected, Toast.LENGTH_SHORT)
                            .show();
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CursorUtils.closeCursor(mCursor);
            // mDbHelper.close();
        }
    }

    OnItemSelectedListener listenerSelectBrandName = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {
            mSelectedBrandName = mBrand.getSelectedItem().toString();
            if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED){
                mIsSizeSpinnerEditable = true;
            }
            if (mIsSizeSpinnerEditable) {
                clearPreviousSelections(2);
                if (0 < mBrand.getSelectedItemPosition()) {
                    getTyreSizes();
                } else {
                    clearSpinnersData(2);
                }
            } else {
                clearPreviousSelections(5);
                if (0 < mBrand.getSelectedItemPosition()) {
                    getTyreDesign();
                } else {
                    clearSpinnersData(5);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView arg0) {
            Toast.makeText(getApplicationContext(),
                    Constants.sLblNothingSelected, Toast.LENGTH_SHORT).show();
        }
    };
    private boolean mBlockPressureFromBluetooth;

    /**
     * Method Getting Tire Sizes from local DB3 file as per the selected
     * tire-brand
     */
    public void getTyreSizes() {
        Cursor mCursor = null;
        try {
            if (null != mDbHelper) {
                mDbHelper.open();
            }
            mCursor = mDbHelper.getTyreSizeFromBrand(mSelectedBrandName);
            if (!CursorUtils.isValidCursor(mCursor)) {
                clearSpinnersData(2);
                return;
            }
            mCursor.moveToFirst();
            mSizeArrayList.clear();
            // mSizeArrayList.add("");
            while (!mCursor.isAfterLast()) {
                mSizeArrayList.add(mCursor.getString(mCursor
                        .getColumnIndex("TSize")));
                mCursor.moveToNext();
            }
            CursorUtils.closeCursor(mCursor);
            loadTyreSize();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CursorUtils.closeCursor(mCursor);
            // mDbHelper.close();
        }
    }

    /**
     * Method populating Tire Size after getting from local DB3 file as per the
     * selected tire-brand
     */
    private void loadTyreSize() {
        try {
            mSizeDataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, mSizeArrayList);
            mSizeDataAdapter
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            size.setAdapter(mSizeDataAdapter);

            for (int i = 0; i < mSizeArrayList.size(); i++) {
                if (mSizeArrayList.get(i).equals(mSelectedtyreSize)) {
                    LogUtil.i(TAG, "Selected tyre Size outside onItemSelected----- " + mSelectedtyreSize);
                    size.setSelection(i);
                    break;
                }
            }
            size.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView adapter, View v, int i,
                                           long lng) {
                    mSelectedtyreSize = size.getSelectedItem().toString();
                    LogUtil.i(TAG, "Selected tyre Size inside onItemSelected----- " + mSelectedtyreSize);
                    clearPreviousSelections(3);
                    if (0 <= size.getSelectedItemPosition()) {
                        getTyreASP();
                    } else {
                        clearSpinnersData(3);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView arg0) {
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method Getting Tire ASP from local DB3 file as per the selected
     * tire-brand
     */
    public void getTyreASP() {
        Cursor mCursor = null;
        try {
            if (null != mDbHelper) {
                mDbHelper.open();
            }
            mCursor = mDbHelper.getTyreASPFromSize(mSelectedtyreSize,
                    mSelectedBrandName);
            if (!CursorUtils.isValidCursor(mCursor)) {
                clearSpinnersData(3);
                return;
            }
            mCursor.moveToFirst();
            mTyreTASPArrayList.clear();
            // mTyreTASPArrayList.add("");
            while (!mCursor.isAfterLast()) {
                mTyreTASPArrayList.add(mCursor.getString(mCursor
                        .getColumnIndex("TASP")));
                mCursor.moveToNext();
            }
            CursorUtils.closeCursor(mCursor);
            loadTyreTASP();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CursorUtils.closeCursor(mCursor);
            // mDbHelper.close();
        }
    }

    /**
     * Method populating Tire ASP after getting from local DB3 file as per the
     * selected tire-brand
     */
    private void loadTyreTASP() {
        mASPDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mTyreTASPArrayList);
        mASPDataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        asp.setAdapter(mASPDataAdapter);

        for (int i = 0; i < mTyreTASPArrayList.size(); i++) {
            if (mTyreTASPArrayList.get(i).equals(mSelectedTyreTASP)) {
                asp.setSelection(i);
                break;
            }
        }
        asp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView adapter, View v, int i,
                                       long lng) {
                mSelectedTyreTASP = asp.getSelectedItem().toString();
                clearPreviousSelections(4);
                if (0 <= asp.getSelectedItemPosition()) {
                    getTyreRIM();
                } else {
                    clearSpinnersData(4);
                }
            }

            @Override
            public void onNothingSelected(AdapterView arg0) {
            }
        });
    }

    /**
     * Method Getting Tire RIM from local DB3 file as per the selected
     * tire-brand
     */
    public void getTyreRIM() {
        Cursor mCursor = null;
        try {
            if (null != mDbHelper) {
                mDbHelper.open();
            }
            mCursor = mDbHelper.getTyreRimFromSize(mSelectedtyreSize,
                    mSelectedTyreTASP, mSelectedBrandName);
            if (!CursorUtils.isValidCursor(mCursor)) {
                clearSpinnersData(4);
                return;
            }
            mCursor.moveToFirst();
            mTyreTRIMArrayList.clear();
            // mTyreTRIMArrayList.add("");
            while (!mCursor.isAfterLast()) {
                mTyreTRIMArrayList.add(mCursor.getString(mCursor
                        .getColumnIndex("TRIM")));
                mCursor.moveToNext();
            }
            CursorUtils.closeCursor(mCursor);
            loadTyreTRIM();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CursorUtils.closeCursor(mCursor);
            // mDbHelper.close();
        }
    }

    /**
     * Method populating Tire RIM after getting from local DB3 file as per the
     * selected tire-brand
     */
    private void loadTyreTRIM() {
        mRimDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
        mRimDataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rim.setAdapter(mRimDataAdapter);

        for (int i = 0; i < mTyreTRIMArrayList.size(); i++) {
            if (mTyreTRIMArrayList.get(i).equals(mSelectedtyreTRIM)) {
                rim.setSelection(i);
                break;
            }
        }
        rim.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView adapter, View v, int i,
                                       long lng) {
                mSelectedtyreTRIM = rim.getSelectedItem().toString();
                clearPreviousSelections(5);
                if (0 <= rim.getSelectedItemPosition()) {
                    getTyreDesign();
                } else {
                    clearSpinnersData(5);
                }
            }

            @Override
            public void onNothingSelected(AdapterView arg0) {
            }
        });
    }

    /**
     * Method Getting Tire Design from local DB3 file as per the selected
     * tire-brand
     */
    public void getTyreDesign() {
        Cursor mCursor = null;
        try {
            if (null != mDbHelper) {
                mDbHelper.open();
            }
            mCursor = mDbHelper.getTyreDesign(mSelectedtyreSize,
                    mSelectedTyreTASP, mSelectedtyreTRIM, mSelectedBrandName);
            if (!CursorUtils.isValidCursor(mCursor)) {
                clearSpinnersData(5);
                return;
            }
            mCursor.moveToFirst();
            mDesignArrayList.clear();
            mDesignArrayList.add(mDefaultValueForSpinner);
            while (!mCursor.isAfterLast()) {
                mDesignArrayList.add(mCursor.getString(mCursor
                        .getColumnIndex("Deseign")));
                mCursor.moveToNext();
            }
            CursorUtils.closeCursor(mCursor);
            loadTyreDesign();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CursorUtils.closeCursor(mCursor);
            // mDbHelper.close();
        }
    }

    /**
     * Method populating Tire Design after getting from local DB3 file as per
     * the selected tire-brand
     */
    private void loadTyreDesign() {
        mDesignDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mDesignArrayList);
        mDesignDataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        design.setAdapter(mDesignDataAdapter);
        for (int i = 0; i < mDesignArrayList.size(); i++) {
            if (mDesignArrayList.get(i).equals(mSelectedTyreDesign)) {
                design.setSelection(i);
                break;
            }
        }
        design.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView adapter, View v, int i,
                                       long lng) {
                mSelectedTyreDesign = design.getSelectedItem().toString();
                clearPreviousSelections(6);
                if (0 < design.getSelectedItemPosition()) {
                    getTyreDetailedDesign();// type
                } else {
                    clearSpinnersData(6);
                }
            }

            @Override
            public void onNothingSelected(AdapterView arg0) {

            }
        });
    }

    /**
     * Method Getting Tire Design-Details from local DB3 file as per the
     * selected tire-brand
     */
    public void getTyreDetailedDesign() {
        Cursor mCursor = null;
        try {
            if (null != mDbHelper) {
                mDbHelper.open();
            }
            mCursor = mDbHelper.getTyreDetailedDesign(mSelectedtyreSize,
                    mSelectedTyreTASP, mSelectedtyreTRIM, mSelectedTyreDesign,
                    mSelectedBrandName);
            if (!CursorUtils.isValidCursor(mCursor)) {
                clearSpinnersData(6);
                return;
            }
            mCursor.moveToFirst();
            mFullDesignArrayList.clear();
            mFullDesignArrayList.add(mDefaultValueForSpinner);
            while (!mCursor.isAfterLast()) {
                mFullDesignArrayList.add(mCursor.getString(mCursor
                        .getColumnIndex("FullTireDetails")));
                mCursor.moveToNext();
            }
            CursorUtils.closeCursor(mCursor);
            loadTyreDetailedDesign();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CursorUtils.closeCursor(mCursor);
            // mDbHelper.close();
        }
    }

    /**
     * Method populating Tire Design-Details after getting from local DB3 file
     * as per the selected tire-brand
     */
    private void loadTyreDetailedDesign() {
        mFullDesignDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mFullDesignArrayList);
        mFullDesignDataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type.setAdapter(mFullDesignDataAdapter);
        for (int i = 0; i < mFullDesignArrayList.size(); i++) {
            if (mFullDesignArrayList.get(i).equals(mSelectedTyreDetailedDesign)) {
                type.setSelection(i);
                break;
            }
        }
        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView adapter, View v, int i,
                                       long lng) {
                mSelectedTyreDetailedDesign = type.getSelectedItem().toString();
                Constants.FULLDESIGN_SELECTED = mSelectedTyreDetailedDesign;
            }

            @Override
            public void onNothingSelected(AdapterView arg0) {

            }
        });
    }

    /**
     * Method opening the instance of DataBase Adaptor after initialization
     */
    public void openDB() {
        try {
            mDbHelper = new DatabaseAdapter(this);
            mDbHelper.createDatabase();
            mDbHelper.open();
        } catch (SQLException e4) {
            e4.printStackTrace();
        }
    }

    /**
     * When spinner value is null then dependent spinner values refreshing.
     */
    private void clearSpinnersData(int i) {
        // Don't add break statement, It's sequence of execution
        switch (i) {
            case 1: // Brand Spinner
            case 2: // Size Spinner
                mSizeArrayList.clear();
                if (null != mSizeDataAdapter) {
                    mSizeDataAdapter.notifyDataSetChanged();
                }
            case 3: // ASP Spinner
                mTyreTASPArrayList.clear();
                if (null != mASPDataAdapter) {
                    mASPDataAdapter.notifyDataSetChanged();
                }
            case 4: // Rim Spinner
                mTyreTRIMArrayList.clear();
                if (null != mRimDataAdapter) {
                    mRimDataAdapter.notifyDataSetChanged();
                }
            case 5: // Design Spinner
                mDesignArrayList.clear();
                if (null != mDesignDataAdapter) {
                    mDesignDataAdapter.notifyDataSetChanged();
                }
            case 6: // FullDetails Spinner
                mFullDesignArrayList.clear();
                if (mFullDesignDataAdapter != null) {
                    mFullDesignDataAdapter.notifyDataSetChanged();
                }
        }
    }

    /**
     * Method Clearing the Previous Selection Values of the spinners present in
     * the UI
     */
    private void clearPreviousSelections(int i) {
        if (mIsOrientationChanged && i < mLastSpinnerSelection) {
            if (i == 6 && mLastSpinnerSelection == 7) {
                mIsOrientationChanged = false;
            }
            return;
        } else if (mIsOrientationChanged) {
            mIsOrientationChanged = false;
        }
        // Don't add break statement, It's sequence of execution
        switch (i) {
            case 1: // Brand Spinner
            case 2: // Size Spinner
                mSelectedtyreSize = "";
            case 3: // ASP Spinner
                mSelectedTyreTASP = "";
            case 4: // Rim Spinner
                mSelectedtyreTRIM = "";
            case 5: // Design Spinner
                mSelectedTyreDesign = "";
            case 6: // FullDetails Spinner
                mSelectedTyreDetailedDesign = "";
        }
    }

    /**
     * Method Checking the Previous Selected Spinner just before the change in
     * orientation
     */
    private void checkLatestSpinnerSelectionBeforeOrientationChanges() {
        if (null == mSelectedBrandName
                || mPleaseSelectBrand.equals(mSelectedBrandName)) {
            mLastSpinnerSelection = 1;
        } else if (null == mSelectedtyreSize || "".equals(mSelectedtyreSize)) {
            mLastSpinnerSelection = 2;
        } else if (null == mSelectedTyreTASP || "".equals(mSelectedTyreTASP)) {
            mLastSpinnerSelection = 3;
        } else if (null == mSelectedtyreTRIM || "".equals(mSelectedtyreTRIM)) {
            mLastSpinnerSelection = 4;
        } else if (null == mSelectedTyreDesign
                || mDefaultValueForSpinner.equals(mSelectedTyreDesign)) {
            mLastSpinnerSelection = 5;
        } else if (null == mSelectedTyreDetailedDesign
                || mDefaultValueForSpinner.equals(mSelectedTyreDetailedDesign)) {
            mLastSpinnerSelection = 6;
        } else {
            mLastSpinnerSelection = 7;
        }
    }

    /**
     * If axle have pressure apply the value then disable otherwise it's
     * editable Setting the Pressure
     */
    private void setValuetoPressueView() {
        if (recommendedpressure > 0) {
            setPressure.setText(CommonUtils.getPressureValue(
                    RegrooveTireOperation.this, recommendedpressure));
            pressureUnit.setText(CommonUtils
                    .getPressureUnit(RegrooveTireOperation.this));
            setPressure.setEnabled(false);
            mBlockPressureFromBluetooth = true;
        } else {
            if (isSettingsPSI) {
                setPressure
                        .setFilters(new InputFilter[]{new DecimalDigitsInputFilterNSK(
                                4, 2)});
            } else {
                setPressure
                        .setFilters(new InputFilter[]{new DecimalDigitsInputFilterNSK(
                                3, 2)});
            }
            /*Bug 728 : Adding context to show toast for higher pressure value*/
            setPressure.addTextChangedListener(new PressureCheckListener(
                    setPressure, isSettingsPSI, getApplicationContext()));
            pressureUnit.setText(CommonUtils
                    .getPressureUnit(RegrooveTireOperation.this));
            // Moving The Cursor To Next Line(Starts)
            afterNSKThree
                    .setOnEditorActionListener(new OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId,
                                                      KeyEvent event) {
                            boolean handled = false;
                            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                                setPressure.requestFocus();
                                handled = true;
                            }
                            return handled;
                        }
                    });
        }
    }

    /**
     * Method Updating recommended axle pressure
     */
    private void updatePressureValue() {
        boolean isPressureEnabled = setPressure.isEnabled();
        if ((isPressureEnabled || !mBlockPressureFromBluetooth)
                && (null != VehicleSkeletonFragment.mAxleRecommendedPressure)) {
            VehicleSkeletonFragment.mAxleRecommendedPressure.set(axlePosition,
                    CommonUtils.parseFloat(mFinalConvertedPressure));
        }
    }

    /**
     * Method handling bundle object that contains the details of the selected tire
     *
     * @param bundle
     */
    private void captureInitialForm(Bundle bundle) {
        if (bundle != null) {
            mFormElements = bundle.getParcelable("initialFormElements");
        } else {
            mFormElements = new RegrooveFormElements();
            mFormElements.setBrand(mBrand.getSelectedItemPosition());
            mFormElements.setDesign(design.getSelectedItemPosition());
            mFormElements.setType(type.getSelectedItemPosition());
            mFormElements.setAfterNsk1(afterNSKOne.getText().toString());
            mFormElements.setAfterNsk2(afterNSKTwo.getText().toString());
            mFormElements.setAfterNsk3(afterNSKThree.getText().toString());
            mFormElements.setBeforeNsk1(beforeNSKOne.getText().toString());
            mFormElements.setBeforeNsk2(beforeNSKTwo.getText().toString());
            mFormElements.setBeforeNsk3(beforeNSKThree.getText().toString());
            mFormElements.setPressure(setPressure.getText().toString());
            mFormElements.setRegrooveType(rgRegrooveType
                    .getCheckedRadioButtonId());
        }
    }

    /**
     * Method saving bundle object that contains the details of the selected tire
     *
     * @param bundle
     */
    private void saveInitialState(Bundle bundle) {
        bundle.putParcelable("initialFormElements", mFormElements);
    }

    /**
     * Method returning the true if any changes made on the details of the loaded screen
     *
     * @return
     */
    private boolean areChangesMade() {
        if (mFormElements == null) {
            return true;
        }
        boolean isSameAsInistial = true;
        isSameAsInistial &= mFormElements.getAfterNsk1().equals(
                afterNSKOne.getText().toString());
        isSameAsInistial &= mFormElements.getAfterNsk2().equals(
                afterNSKTwo.getText().toString());
        isSameAsInistial &= mFormElements.getAfterNsk3().equals(
                afterNSKThree.getText().toString());
        isSameAsInistial &= mFormElements.getBeforeNsk1().equals(
                beforeNSKOne.getText().toString());
        isSameAsInistial &= mFormElements.getBeforeNsk2().equals(
                beforeNSKTwo.getText().toString());
        isSameAsInistial &= mFormElements.getBeforeNsk3().equals(
                beforeNSKThree.getText().toString());
        isSameAsInistial &= mFormElements.getPressure().equals(
                setPressure.getText().toString());
        isSameAsInistial &= mFormElements.getRegrooveType() == rgRegrooveType
                .getCheckedRadioButtonId();
        isSameAsInistial &= mFormElements.getBrand() == mBrand
                .getSelectedItemPosition();
        isSameAsInistial &= mFormElements.getDesign() == design
                .getSelectedItemPosition();
        isSameAsInistial &= mFormElements.getType() == type
                .getSelectedItemPosition();
        return !isSameAsInistial;
    }

    IOnPerformanceCallback callback = new IOnPerformanceCallback() {

        @Override
        public void updateTables() {
            onReturnToSkeleton();
        }

        @Override
        public void showProgressBar(boolean toShow) {
            if (toShow) {
                // Show Progress Bar here
            } else {
                // Hide Progress bar here
            }
        }

        @Override
        public void onPostExecute(ResponseMessagePerformance message) {
            // Start Activity
            Intent intentReturn = new Intent();
            intentReturn.putExtra("regroove", true);
            // set return code to 2
            setResult(VehicleSkeletonFragment.REGROOVE_INTENT, intentReturn);
            Constants.JOB_CORRECTION_LIST = VehicleSkeletonFragment.mJobcorrectionList;
            CameraUtility.resetParams();
            finish();
            RegrooveTireOperation.this.overridePendingTransition(
                    R.anim.slide_left_in, R.anim.slide_left_out);
        }

        @Override
        public String onPreExecute() {
            String messageStr = PerformanceBaseModel.MESSAGE_STR;
            // verify re-groove type selected
            int checkedRadioButtonId = rgRegrooveType.getCheckedRadioButtonId();
            if (checkedRadioButtonId != -1) {
                regrooveType = (RadioButton) findViewById(checkedRadioButtonId);
                reGrooveTypeValue = regrooveType.getText().toString().trim();
                if (reGrooveTypeValue.equals(mRibLabel)) {
                    regrooveInfo.setReGrooveType(Constants.REGROOVE_RIB);
                } else if (reGrooveTypeValue.equals(mBlockLabel)) {
                    regrooveInfo.setReGrooveType(Constants.REGROOVE_BLOCK);
                }
            } else {
                messageStr = REGROOVE_IS_REQUIRED;
                LogUtil.i("Regroove ", REGROOVE_IS_REQUIRED);
                // return;
            }
            return messageStr;
        }

        @Override
        public void onError(ResponseMessagePerformance message) {
            CommonUtils.notify(message.getErrorMessage(),
                    getApplicationContext());
        }

        @Override
        public void closePreviousTask() {

        }
    };

    @Override
    public void logUserOutDueToInactivity() {
        noteText = "";
        InactivityUtils.logoutFromActivityAboveEjobForm(this);
    }
}