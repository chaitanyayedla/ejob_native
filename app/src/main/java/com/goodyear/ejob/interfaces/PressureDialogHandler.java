package com.goodyear.ejob.interfaces;

/**
 * This interface defines operations for any job action that requires pressure
 * pop up dialog.
 */
public interface PressureDialogHandler {

	int FIRST_TIRE = 0;
	int SECOND_TIRE = 1;
	int THIRD_TIRE = 2;
	int FORTH_TIRE = 3;

	/**
	 * This method is called from pressure dialog to update the pressure set by
	 * user.
	 * 
	 * @param pressureSwapPosition
	 *            This defines which tire (tires being swapped) pressure would be
	 *            updated.
	 * @param pressureValue
	 *            Pressure value set by user in the dialog.
	 */
	void updatePressureOnSave(int pressureSwapPosition, float pressureValue);

	/**
	 * This method should be called to start the pressure dialog.
	 * 
	 * @param tirePositionForPressureDialog
	 *            This states for which tire (in the tires being swaped)
	 *            pressure dialog should be started.
	 * 
	 * @param pressureValue
	 *            A default pressure value (if any) to be set in dialog. If no
	 *            default value then pass null.
	 */
	void startPressureDialog(int tirePositionForPressureDialog,
			String pressureValue);
}
