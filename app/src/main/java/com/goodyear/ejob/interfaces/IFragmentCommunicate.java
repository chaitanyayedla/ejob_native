package com.goodyear.ejob.interfaces;

/**
 * Created by giriprasanth.vp on 10/20/2015.
 */
public interface IFragmentCommunicate {
    public boolean onActivityBackPress();

    public void hideKeyBoard(boolean toHide);
}
