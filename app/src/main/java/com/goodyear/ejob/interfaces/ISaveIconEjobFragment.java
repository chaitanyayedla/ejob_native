package com.goodyear.ejob.interfaces;

import com.goodyear.ejob.EjobFormActionActivity;

/**
 * This is an Interface for passing save icon information across the
 * {@link EjobFormActionActivity} and its fragments.
 */
public interface ISaveIconEjobFragment {

	/**
	 * gets the save icon status in fragments from
	 * {@link EjobFormActionActivity}
	 * 
	 * @return the save icon state set by fragments
	 */
	public int getSaveIconState();

	/**
	 * sets the save icon status from fragments
	 * 
	 * @param iconStatus
	 */
	public void setSaveIconState(int iconStatus);
}
