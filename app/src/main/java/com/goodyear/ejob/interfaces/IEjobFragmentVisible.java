package com.goodyear.ejob.interfaces;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.adapter.EJobPagerAdapter;

/**
 * This interface is used to determine when the fragment is visible to the user.
 * It should be implemented by all fragments in {@link EJobPagerAdapter}.
 */
public interface IEjobFragmentVisible {

	/**
	 * This method is called when the activity is visible to the user. See
	 * onpageselected() in {@link EjobFormActionActivity} for implementation
	 * details.
	 */
	public void fragmentIsVisibleToUser();
}
