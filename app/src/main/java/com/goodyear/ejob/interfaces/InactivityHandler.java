package com.goodyear.ejob.interfaces;

/**
 * This interface is to handle inactivity of the user. Implement this interface
 * in every Activity. The onStart() method must set the current handler in
 * Logouthandler
 */
public interface InactivityHandler {

	/**
	 * This method is the call back method for inactvity when the inactivity
	 * count down timer times out, handle how the logout should be implemented.
	 * 
	 */
	public void logUserOutDueToInactivity();
}
