
package com.goodyear.ejob.interfaces;
/**
 * @author shailesh.p
 *
 */
public interface BluetoothDialogActivityConnector {
	public void closeConnection();
}
