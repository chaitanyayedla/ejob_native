
package com.goodyear.ejob.interfaces;
/**
 * @author amitkumar.h
 *
 */
public interface IPressureDialogHandler {

	/**
	 * called from pressure dialog to update the user set pressure
	 */
	void updatePressureOnSave(int pressureSwapPosition,float pressureValue);
}
