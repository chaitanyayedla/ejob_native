
package com.goodyear.ejob.interfaces;

import android.content.Context;

/**
 * @author amitkumar.h
 *
 */
public interface AlertDialogHandler {
	/**
	 * handler for on click of ok
	 */
	public void onClickOK(Context context);
}
