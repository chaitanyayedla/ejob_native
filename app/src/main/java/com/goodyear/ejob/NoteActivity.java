package com.goodyear.ejob;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.EjobSignatureBoxFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.job.operation.helpers.Retorque;
import com.goodyear.ejob.job.operation.helpers.TorqueSettings;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.LogUtil;

public class NoteActivity extends Activity implements InactivityHandler {

	public static boolean isNotesSaved = false;
	/**
	 * From which screen this activity was started from
	 */
	private String startedFrom = "";
	private String sapCode;
	/**
	 * Position of the wheel
	 */
	private String wheelPos;
	/**
	 * Flag to store the whether wheel is removed or not
	 */
	private int isWheelRemoved;
	/**
	 * Job id for which current notes is for
	 */
	private String jobid;
	/**
	 * boolean flag which tells whether to return to regroove with notes
	 */
	private boolean returnToRegrooveWithNotes;
	/**
	 * boolean flag which tells whether to return to regroove with notes
	 */
	private boolean returnToTurnOnRimWithNotes;
	/**
	 * boolean flag which tells whether to return to inspection with notes
	 */
	private boolean returnToInspectionWithNotes;

	/**
	 * boolean flag which tells whether to return to Ejob Signature Box with notes
	 */
	private boolean returnToEjobSignatureBoxWithNotes;
	/**
	 * boolean flag whether to enable the save menu icon in action bar
	 */
	private boolean mEnableSave = false;
	// VIEWS
	/**
	 * EditText which takes the notes
	 */
	private EditText mNoteEditText;
	private String mConfirmMsg;
	private String mYesLabel;
	private String mNoLabel;
	private String initialText = null;

	//User Trace logs trace tag
	private static final String TRACE_TAG = "Notes";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note);
		//User Trace logs
		LogUtil.TraceInfo(TRACE_TAG, "none", TRACE_TAG, false, false, true);
		isNotesSaved = false;
		mNoteEditText = (EditText) findViewById(R.id.note);
		Intent intent = getIntent();
		if (intent.getExtras() != null) {
			startedFrom = intent.getExtras().getString("sentfrom");
		}
		if (startedFrom.equals("regroove")) {
			returnToRegrooveWithNotes = true;
			mNoteEditText.setText(intent.getExtras().getString("lastnote"));
		}
		if (startedFrom.equals("turnonrim")) {
			returnToTurnOnRimWithNotes = true;
			mNoteEditText.setText(intent.getExtras().getString("lastnote"));
		}
		if (startedFrom.equals("inspection")) {
			returnToInspectionWithNotes = true;
			mNoteEditText.setText(intent.getExtras().getString("lastnote"));
		}
		//Ejob Signature Box Note (Reason for Partially Inspected Job)
		if (startedFrom.equals("EjobSignatureBox")) {
			returnToEjobSignatureBoxWithNotes = true;
			//mNoteEditText.setText(intent.getExtras().getString("lastnote"));
		}
		if (startedFrom.equals("retorque")) {
			sapCode = intent.getExtras().getString(Constants.SAPCODEWP);
			wheelPos = intent.getExtras().getString(Constants.WHEELPOS);
			isWheelRemoved = intent.getExtras()
					.getInt(Constants.ISWHEELREMOVED);
			jobid = intent.getExtras().getString(Constants.JOBID);
		}
		DatabaseAdapter label = new DatabaseAdapter(this);
		label.createDatabase();
		label.open();
		String activityName = label.getLabel(Constants.NOTES_ACTIVITY_NAME);
		setTitle(activityName);

		mConfirmMsg = label.getLabel(Constants.CONFIRM_BACK);
		mYesLabel = label.getLabel(Constants.YES_LABEL);
		mNoLabel = label.getLabel(Constants.NO_LABEL);

		label.close();
		mNoteEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i,
					int i1, int i2) {
			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1,
					int i2) {
				boolean enableSave = !initialText.equals(charSequence
						.toString());
				if (enableSave != mEnableSave) {
					mEnableSave = enableSave;
					invalidateOptionsMenu();
				}
			}

			@Override
			public void afterTextChanged(Editable editable) {
			}
		});
		if (savedInstanceState != null
				&& savedInstanceState.containsKey("initialState")) {
			initialText = savedInstanceState.getString("initialState");
		} else {
			initialText = mNoteEditText.getText().toString();
		}
		restoreDialogState(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.note, menu);
		MenuItem saveItem = menu.findItem(R.id.action_image_save);
		saveItem.setEnabled(mEnableSave);
		saveItem.setIcon(mEnableSave ? R.drawable.savejob_navigation
				: R.drawable.unsavejob_navigation);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			LogUtil.TraceInfo(TRACE_TAG, "Option", "Settings", false, true, false);
			return true;
		} else if (id == R.id.action_image_save) {
			if (returnToRegrooveWithNotes) {
				LogUtil.TraceInfo(TRACE_TAG, "Option", "Save : Notes :"+mNoteEditText.getText()
						.toString(), false, true, false);
				Intent intentReturn = new Intent();
				intentReturn.putExtra("mNoteEditText", mNoteEditText.getText()
						.toString());
				// set return code to 2
				setResult(RegrooveTireOperation.NOTES_INTENT, intentReturn);
				finish();
			} else if (returnToTurnOnRimWithNotes) {
				LogUtil.TraceInfo(TRACE_TAG, "Option", "Save : Notes :"+mNoteEditText.getText()
						.toString(), false, true, false);
				Intent intentReturn = new Intent();
				intentReturn.putExtra("mNoteEditText", mNoteEditText.getText()
						.toString());
				// set return code to 2
				setResult(TurnOnRim.NOTES_INTENT, intentReturn);
				finish();
			}else if (returnToInspectionWithNotes) {
				LogUtil.TraceInfo(TRACE_TAG, "Option", "Save : Notes :"+mNoteEditText.getText()
						.toString(), false, true, false);
				Intent intentReturn = new Intent();
				intentReturn.putExtra("mNoteEditText", mNoteEditText.getText()
						.toString());
				// set return code to 2
				setResult(InspectionActivity.NOTES_INTENT, intentReturn);
				finish();
			}
			//Ejob Signature Box Note (Reason for Partially Inspected Job) - result block
			else if (returnToEjobSignatureBoxWithNotes) {
				LogUtil.TraceInfo(TRACE_TAG, "Option", "Save : Notes :"+mNoteEditText.getText()
						.toString(), false, true, false);
				Intent intentReturn = new Intent();
				intentReturn.putExtra("mEjobSignatureNoteEditText", mNoteEditText.getText()
						.toString());
				// set return code to 2
				setResult(EjobSignatureBoxFragment.NOTES_INTENT, intentReturn);
				finish();
			}
			else if (startedFrom.equals("retorque")) {
				LogUtil.TraceInfo(TRACE_TAG, "Option", "Save : Notes :"+mNoteEditText.getText()
						.toString(), false, true, false);
				// torque setting to added to arraylist
				TorqueSettings torqueSet = new TorqueSettings();
				torqueSet.setWheelPos(wheelPos);
				torqueSet.setSapCodeWP(sapCode);
				torqueSet.setNoManufacturerTorqueSettingsReason(mNoteEditText
						.getText().toString());

				//Bug 645 : Doing a TOR create an empty re-torque in the mdw
				//Wrench number should be empty
				torqueSet.setWrenchNumber("");
				torqueSet.setJobId(jobid);
				// wheel removed
				torqueSet.setIsWheelRemoved(1);
				Retorque.torqueSettings.add(torqueSet);
				Constants.TORQUE_SETTINGS_ARRAY = Retorque.torqueSettings;
				finish();
			}
			isNotesSaved = true;
		}

		// hide keyboard if displayed
		try {
			CommonUtils.hideKeyboard(this, getWindow().getDecorView()
					.getRootView().getWindowToken());
		} catch (Exception e) {
			LogUtil.i("Note on activity return", "could not close keyboard");
		}

		return super.onOptionsItemSelected(item);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onSaveInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("initialState", initialText);
		saveDialogState(outState);
		super.onSaveInstanceState(outState);
	}

	private AlertDialog mBackAlertDialog;

	private void saveDialogState(Bundle state) {
		state.putBoolean("mBackAlertDialog",
				(mBackAlertDialog != null && mBackAlertDialog.isShowing()));
	}

	private void restoreDialogState(Bundle state) {
		if (state != null) {
			if (state.getBoolean("mBackAlertDialog")) {
				createDialogOnBackPress();
			}
		}
	}

	@Override
	protected void onDestroy() {
		try {
			if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
				mBackAlertDialog.dismiss();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		if (mNoteEditText.getText().toString().equals(initialText)) {
			LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press", false, false, false);
			goBack();
		} else {
			createDialogOnBackPress();
		}
	}

	/**
	 * Used to show the alert dialog which prompts when user tries to exit
	 * without saving
	 */
	private void createDialogOnBackPress() {
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", false, true, false);
		LayoutInflater li = LayoutInflater.from(NoteActivity.this);
		View promptsView = li
				.inflate(R.layout.dialog_logout_confirmation, null);
		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
				NoteActivity.this);
		alertDialogBuilder.setView(promptsView);
		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		// Setting The Info
		TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
		infoTv.setText(mConfirmMsg);
		alertDialogBuilder.setCancelable(false).setPositiveButton(mYesLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
						goBack();
					}
				});
		alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);
						dialog.cancel();
					}
				});
		mBackAlertDialog = alertDialogBuilder.create();
		mBackAlertDialog.show();
		mBackAlertDialog.setCanceledOnTouchOutside(false);
	}

	private void goBack() {
		isNotesSaved = false;
		finish();
	}

	@Override
	public void onStart() {
		super.onStart();
		// set user inactivity handler for this activity
		LogoutHandler.setCurrentActivity(this);
	}

	@Override
	public void onUserInteraction() {
		// update user inactivity
		InactivityUtils.updateActivityOfUser();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		InactivityUtils.logoutFromActivityAboveEjobForm(this);
	}
}
