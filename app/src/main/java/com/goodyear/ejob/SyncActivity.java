package com.goodyear.ejob;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Bundle;
import android.os.Messenger;
import android.support.v7.internal.view.ActionBarPolicy;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.goodyear.ejob.adapter.EjobListAdapter;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.service.*;
import com.goodyear.ejob.util.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Rajesh on 11/27/2014.
 */
public class SyncActivity extends Activity implements
        SyncBaseModel.SyncCallback {

    public static final String IS_DIALOG_SHOWN = "isDialogShown";
    public static final String SENT_FROM = "sentfrom";
    public static final String SETTINGS_ACTIVITY = "settings";
    public static final String LOGIN_ACTIVITY = "login";
    public static final String EJOB_LIST = "ejobList";
    private long mStartTime;
    private DatabaseAdapter mDBHelper;
    private SyncTime mSyncTime;
    private GYProgressDialog mGYProgressDialog;
    private EjobListAdapter mAdapter;
    private ListView mLvJoblist;

    private String mLblTitle;
    private String mLicencePlateNOLabel;
    private String mCusNameLabel;
    private String mDateLabel;
    private String mRefNOLabel;
    private String mOkLabel;
    private String threeDaysOld;
    private String mLblTireManagement;
    private String mLblIncompleteJobs;
    private String mLblDeletedJobs;
    private String mLblCompletedJobs;
    private ArrayList<Integer> mSelectedItemIds;
    private AlertDialog mErrorDialog;
    private String mDialogMsg;
    private String mDialogTitle;
    private static MessageHandler mMessageHandler;
    private String activityResult = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ArrayList<String> checkedList = null;
        setContentView(R.layout.activity_ejob_list);
        if (mMessageHandler != null) {
            mMessageHandler.setSyncCallBack(this);
        }
        setUpActionBar();

		/* #ForHalosys, adding offline*/
        /*SharedPreferences sharedPref = getSharedPreferences(Constants.FIRST_TIME_LOGIN_PREFERENCE, getApplicationContext().MODE_PRIVATE);
        Boolean isFirstTime = sharedPref.getBoolean(Constants.FIRST_TIME_LOGIN_KEY + "_" + Constants.VALIDATE_USER, false);
        LogUtil.d("SyncActivity ", "1 Constants.VALIDATE_USER : " + Constants.VALIDATE_USER);
        LogUtil.d("SyncActivity ", " isFirstTime boolean : " + isFirstTime);

        try {
            if (Validation.isNetworkAvailable(this) && isFirstTime) {
                LogUtil.d("SyncActivity ", " Db doesn't exist, showing offline pin dialog");
                Intent landingIntent = new Intent(SyncActivity.this, OfflinePinDialog.class);
                startActivity(landingIntent);
            } else {
                LogUtil.d("SyncActivity ", " Db exists NOT showing offline pin dialog");
            }
        } catch (Exception e) {
            Log.d("SyncActivity ", "Exception " + e);
        } finally {
            SharedPreferences.Editor editor = sharedPref.edit();
            LogUtil.d("SyncActivity ", "2 Constants.VALIDATE_USER : " + Constants.VALIDATE_USER);
            editor.putBoolean(Constants.FIRST_TIME_LOGIN_KEY + "_" + Constants.VALIDATE_USER, false);
            editor.commit();
        }*/

        if (savedInstanceState == null) {
            openDB();
            if (getIntent().hasExtra(EjobList.CHECKED_ITEMS)) {
                checkedList = getIntent().getStringArrayListExtra(
                        EjobList.CHECKED_ITEMS);
            }
                // sync process starts from here.
                boolean isSyncing = startInitialSync(getIntent());
                if (!isSyncing) {
                    launchEJobListActivity();
                    return;
                }

        } else {
            if (savedInstanceState.getBoolean(IS_DIALOG_SHOWN)) {
                showProgress();
            }
            mSelectedItemIds = savedInstanceState
                    .getIntegerArrayList("SelectedItems");
        }
        getLabels(savedInstanceState);
        bindLabelsToViews();
        setUpListView(checkedList);
        restoreDialogState(savedInstanceState);
        closeDb();
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 1) {

            String message = data.getStringExtra("MESSAGE");
            activityResult = message;
            LogUtil.d("SyncActivity ", " ..message .. " + message);
            LogUtil.d("SyncActivity ", " ..activityResult .. " + activityResult);
            launchEJobListActivity();

        }
    }*/


    /**
     * This method is used to create and initialize ActionBar
     */
    private void setUpActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setCustomView(R.layout.actionbarimages);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
        ActionBarPolicy.get(this).showsOverflowMenuButton();
    }

    private void setUpListView(ArrayList<String> checkedList) {
        boolean checkedValuePresent;
        if (checkedList != null)
            checkedValuePresent = true;
        else
            checkedValuePresent = false;

        if (EjobList.sListItems != null) {
            mAdapter = new EjobListAdapter(this, EjobList.sListItems,
                    checkedValuePresent);
            mAdapter.setCheckedJobList(checkedList);
            mLvJoblist = (ListView) findViewById(R.id.lv_joblist);
            mLvJoblist.setDivider(null);
            mLvJoblist.setDividerHeight(0);
            if (mSelectedItemIds == null) {
                mSelectedItemIds = new ArrayList<>();
            }
            mAdapter.setSelectedItemIds(mSelectedItemIds);
            mLvJoblist.setAdapter(mAdapter);
        }
    }

    private void getLabels(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            restoreLabels(savedInstanceState);
            return;
        }
        mLblTitle = mDBHelper.getLabel(Constants.LABEL_EJOBS);
        mLicencePlateNOLabel = mDBHelper
                .getLabel(Constants.LICENCE_PLATE_FROM_DB);
        mCusNameLabel = mDBHelper.getLabel(Constants.CUST_NAME_FROM_DB);
        mDateLabel = mDBHelper.getLabel(Constants.DATE_FROM_DB);
        mRefNOLabel = mDBHelper.getLabel(Constants.REF_NUMBER_LABEL);
        mOkLabel = mDBHelper.getLabel(Constants.LABEL_OK);
        threeDaysOld = mDBHelper.getLabel(5);
        mLblTireManagement = mDBHelper
                .getLabel(Constants.TIRE_MANAGEMENT_LABEL);
        mLblIncompleteJobs = mDBHelper
                .getLabel(Constants.INCOMPLETE_JOBS_LABEL);
        mLblDeletedJobs = mDBHelper.getLabel(Constants.DELETED_JOBS_LABEL);
        mLblCompletedJobs = mDBHelper.getLabel(Constants.COMPLETED_JOBS_LABEL);
        Constants.sLblNoNetwork = mDBHelper.getLabel(Constants.NO_NETWORK);
        Constants.ERROR_NETWORK_SYNC_ERROR = mDBHelper
                .getLabel(Constants.CHECK_INTERNET_CONNECTION_LABEL);
    }

    private void restoreLabels(Bundle savedInstanceState) {
        mLblTitle = savedInstanceState.getString("mLblTitle");
        mLicencePlateNOLabel = savedInstanceState
                .getString("mLicencePlateNOLabel");
        mCusNameLabel = savedInstanceState.getString("mCusNameLabel");
        mDateLabel = savedInstanceState.getString("mDateLabel");
        mRefNOLabel = savedInstanceState.getString("mRefNOLabel");
        mOkLabel = savedInstanceState.getString("mOkLabel");
        threeDaysOld = savedInstanceState.getString("threeDaysOld");
        mLblTireManagement = savedInstanceState.getString("mLblTireManagement");
        mLblIncompleteJobs = savedInstanceState.getString("mLblIncompleteJobs");
        mLblDeletedJobs = savedInstanceState.getString("mLblDeletedJobs");
        mLblCompletedJobs = savedInstanceState.getString("mLblCompletedJobs");
    }

    private void saveLabels(Bundle outState) {
        outState.putString("mLblTitle", mLblTitle);
        outState.putString("mLicencePlateNOLabel", mLicencePlateNOLabel);
        outState.putString("mCusNameLabel", mCusNameLabel);
        outState.putString("mDateLabel", mDateLabel);
        outState.putString("mRefNOLabel", mRefNOLabel);
        outState.putString("mOkLabel", mOkLabel);
        outState.putString("threeDaysOld", threeDaysOld);
        outState.putString("mLblTireManagement", mLblTireManagement);
        outState.putString("mLblIncompleteJobs", mLblIncompleteJobs);
        outState.putString("mLblDeletedJobs", mLblDeletedJobs);
        outState.putString("mLblCompletedJobs", mLblCompletedJobs);
    }

    private void bindLabelsToViews() {
        TextView titleTxt = (TextView) findViewById(R.id.title_text);
        titleTxt.setText(mLblTitle);

        TextView mLicencePlate = (TextView) findViewById(R.id.TextView05);
        TextView customerName = (TextView) findViewById(R.id.TextView03);
        TextView mDate = (TextView) findViewById(R.id.TextView02);
        TextView refNO = (TextView) findViewById(R.id.TextView04);

        mLicencePlate.setText(mLicencePlateNOLabel);
        customerName.setText(mCusNameLabel);
        mDate.setText(mDateLabel);
        refNO.setText(mRefNOLabel);

        TextView tireManagementLabel = (TextView) findViewById(R.id.textView2);
        TextView incompleteJobsLabel = (TextView) findViewById(R.id.textView3);
        TextView completedJobsLabel = (TextView) findViewById(R.id.textView4);
        TextView deletedJobsLabel = (TextView) findViewById(R.id.textView5);

        if (tireManagementLabel != null) {
            tireManagementLabel.setText(mLblTireManagement);
        }
        if (incompleteJobsLabel != null) {
            incompleteJobsLabel.setText(mLblIncompleteJobs);
        }
        if (completedJobsLabel != null) {
            completedJobsLabel.setText(mLblCompletedJobs);
        }
        if (deletedJobsLabel != null) {
            deletedJobsLabel.setText(mLblDeletedJobs);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveLabels(outState);
        outState.putIntegerArrayList("SelectedItems", mSelectedItemIds);
        outState.putBoolean(IS_DIALOG_SHOWN,
                (mGYProgressDialog != null && mGYProgressDialog.isShowing()));
        saveDialogState(outState);

        //During Orientation change dialog will be recreated
        //Resetting IS_SHOWING_DIALOG Constants
        if (Constants.APP_UPGRADE_STATUS || Constants.Force_Download_Sync_STATUS) {
            Constants.IS_SHOWING_DIALOG = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissProgress();
        dismissErrorDialog();
    }

    private void showProgress() {
        dismissProgress();
        mGYProgressDialog = new GYProgressDialog(this);
        mGYProgressDialog.show();
    }

    private void dismissProgress() {
        if (mGYProgressDialog != null && mGYProgressDialog.isShowing()) {
            mGYProgressDialog.dismiss();
        }
        mGYProgressDialog = null;
    }

    public void openDB() {
        try {
            mDBHelper = new DatabaseAdapter(this);
            mDBHelper.createDatabase();
            mDBHelper.open();
        } catch (SQLException e4) {
            e4.printStackTrace();
        }
    }

    private void closeDb() {
        if (mDBHelper != null) {
            mDBHelper.close();
        }
    }

    /**
     * This method checks the intent and identifies which sync should be started
     * based on the intent passed by the caller of {@link SyncActivity}.
     *
     * @param intent intent from the caller of sync
     * @return true is sync process is started, false otherwise.
     */
    private boolean startInitialSync(Intent intent) {
        String startedFrom = getCallerActivity(intent);
        String lang = null;
        if (startedFrom != null && startedFrom.equals(SETTINGS_ACTIVITY)) {
            lang = intent.getExtras().getString("language");
        }
        // FIX:: if login with new USER
        if (startedFrom != null && startedFrom.equals(LOGIN_ACTIVITY)) {
            EjobList.sListItems = new ArrayList<>();
        }
        Settings setting = mDBHelper.getSettingsInfo();
        if (lang != null && !lang.equals(setting.getLanguage())) {
            setting.setLanguage(lang);
            try {
                if (Validation.isNetworkAvailable(this)) {
                    Log.i("SyncActivity ", "Starting Translation Sync::: " + lang);
                    Bundle bundle = new Bundle();
                    bundle.putInt(SyncUtils.SYNC_OPTIONS_KEY,
                            SyncUtils.SyncOptions.TRANSLATE_SYNC.getValue());
                    bundle.putString(SyncUtils.SYNC_LANG_KEY, lang);
                    /*Halosys
					startSyncService(bundle);*/
                    return true;
                } else {
                    showAlertDialog(Constants.ERROR_NO_NETWORK_TO_SYNC, null);
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mSyncTime = mDBHelper.getSyncTimeInfo();

        try {
            long syncTimeInLong = Long.parseLong(mSyncTime.getValue());
            if (Math.abs(DateTimeUTC
                    .daysBetweenCurDateTimeNgivenDateTime(syncTimeInLong)) > 365) {
                Constants.Force_Download_Sync_STATUS = true;
                LogUtil.i("Sync Activity--", "Day Difference : " + Math.abs(DateTimeUTC
                        .daysBetweenCurDateTimeNgivenDateTime(syncTimeInLong)));
				startSyncService(SyncUtils.SyncOptions.DOWNLOAD_SYNC);
                return true;
            }
            // if coming from login
            else if (startedFrom != null && startedFrom.equals(LOGIN_ACTIVITY)
                    && Validation.isNetworkAvailable(this)) {
                Date lastSyncDate = DateTimeUTC
                        .convertMillisecondsToUTCDate(syncTimeInLong);
                Calendar cal1 = Calendar.getInstance();
                Calendar cal2 = Calendar.getInstance();
                cal1.setTime(lastSyncDate);
                cal2.setTime(new Date());
                boolean dayDifferenceInSync = (cal1.get(Calendar.YEAR) == cal2
                        .get(Calendar.YEAR))
                        && (cal1.get(Calendar.DAY_OF_YEAR) != cal2
                        .get(Calendar.DAY_OF_YEAR));
                if (dayDifferenceInSync) {
					/*Halosys
					startSyncService(SyncUtils.SyncOptions.UPLOAD_SYNC);*/
                    return true;
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return false;
        }
        if (EJOB_LIST.equals(startedFrom)) {
            if (intent.hasExtra("SelectedItems"))
                mSelectedItemIds = intent
                        .getIntegerArrayListExtra("SelectedItems");
			/*Halosys
			startSyncService(SyncUtils.SyncOptions.UPLOAD_SYNC);*/
            return true;
        }
        return false;
    }

    /**
     * This method returns the activity from where the sync process has been
     * started
     *
     * @param intent intent from the caller
     * @return class name as string
     */
    private String getCallerActivity(Intent intent) {
        if (intent.getExtras() != null) {
            return intent.getExtras().getString(SENT_FROM);
        }
        return null;
    }

    /**
     * This method starts the sync service with the with the correct option
     * select by the user
     *
     * @param syncOptions enum value defining which sync to start
     */
    private void startSyncService(SyncUtils.SyncOptions syncOptions) {
        Bundle bundle = new Bundle();
        bundle.putInt(SyncUtils.SYNC_OPTIONS_KEY, syncOptions.getValue());
        startSyncService(bundle);
    }

    /**
     * When the sync type is decided this method is used to start sync service.
     *
     * @param bundle bundle containing information about the sync type to be
     *               started.
     */
    private void startSyncService(Bundle bundle) {
        showProgress();
        mStartTime = System.currentTimeMillis();
        mMessageHandler = new MessageHandler(this);
        Intent intent = new Intent(this, EjobSyncService.class);
        bundle.putParcelable("MESSENGER", new Messenger(mMessageHandler));
        intent.putExtras(bundle);
        startService(intent);
    }

    @Override
    public void onCanceled(int currentId) {
    }

    @Override
    public void onSyncComplete(ResponseMessage baseModel) {
        dismissProgress();
        Log.i("EjobList", "onSyncComplete ResponseMessage:" + baseModel);
        if (baseModel != null) {
            Log.i("EjobList", "ResponseMessage :" + baseModel.getSyncOption()
                    + " ::Message:: " + baseModel.getMessage());
            long endTime = System.currentTimeMillis();
            Log.i("EjobList", "Final Time :" + +(endTime - mStartTime) / 1000);
            int syncOption = baseModel.getSyncOption();
            switch (SyncUtils.SyncOptions.values()[syncOption]) {
                case UPLOAD_SYNC:
                case TRANSLATE_SYNC:
                    onUploadSyncComplete(baseModel);
                    break;
                case DOWNLOAD_SYNC:
                    onDownloadSyncComplete(baseModel);
                    break;
                default:
                    launchEJobListActivity();
                    break;
            }
        }
        mMessageHandler = null;
    }

    /**
     * Based on the Download Sync response Message, this method will show alertDialog or launch EjobListActivity
     *
     * @param baseModel download sync response message
     */
    private void onDownloadSyncComplete(ResponseMessage baseModel) {
        String errorMessage = baseModel.getMessage();
        if (!TextUtils.isEmpty(errorMessage) && errorMessage.contains("error")) {
            if (errorMessage.contains("server_down")) {
                errorMessage = Constants.ERROR_SERVER_DOWN_ERROR;
            } else if (errorMessage.contains("DB_DELETED")) {
                errorMessage = Constants.ERROR_DB_DELETED;
            } else {
                errorMessage = Constants.ERROR_NETWORK_SYNC_ERROR;
            }
            showAlertDialog(errorMessage, Constants.sLblNoNetwork);
        } else {
            launchEJobListActivity();
        }
    }

    /**
     * Based on the Upload Sync response Message, this method shows show alertDialog or launch EjobListActivity
     *
     * @param baseModel upload sync response message
     */
    private void onUploadSyncComplete(ResponseMessage baseModel) {
        Log.i("EjobList", "Starting EjobListActivity After Sync/Translation:: ");
        String errorMessage = baseModel.getMessage();
        if (!TextUtils.isEmpty(errorMessage) && errorMessage.contains("error")) {
            if (errorMessage.contains("server_down")) {
                errorMessage = Constants.ERROR_SERVER_DOWN_ERROR;
            } else if (errorMessage.contains("DB_DELETED")) {
                errorMessage = Constants.ERROR_DB_DELETED;
            } else {
                errorMessage = Constants.ERROR_NETWORK_SYNC_ERROR;
            }
            showAlertDialog(errorMessage, null);
        } else {
            launchEJobListActivity();
        }
    }

    /**
     * This method launch's EjobList Activity
     */
    private void launchEJobListActivity() {
        finish();
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, EjobList.class);
        startActivity(intent);
    }

    /**
     * This method, pop-up Dialog based on alert message from Sync Module
     *
     * @param errorMsg alert message
     * @param title    alert message title
     */
    public void showAlertDialog(String errorMsg, String title) {
        final Context mContext;
        mContext = this.getApplicationContext();
        dismissProgress();// just to be sure
        mDialogTitle = title;
        mDialogMsg = errorMsg;
        final EjobAlertDialog builder = new EjobAlertDialog(this);

        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li
                .inflate(R.layout.dialog_simple_one_textview, null);
        builder.setView(promptsView);

        // set user activity for
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });

        final TextView textMessage = (TextView) promptsView
                .findViewById(R.id.text_message);
        final TextView dialogTitle = (TextView) promptsView
                .findViewById(R.id.text_dialog_title);

        // setting dialog title
        dialogTitle.setVisibility(TextView.VISIBLE);
        dialogTitle.setText(mDialogTitle);

        // Setting Dialog Message
        textMessage.setText(mDialogMsg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // update user activity on button click
                builder.updateInactivityForDialog();

                //Logs Added
                LogUtil.i("SyncActivity", "Forcelogout-condition check");
                LogUtil.i("SyncActivity", "Constants.APP_UPGRADE_STATUS - " + Constants.APP_UPGRADE_STATUS);
                LogUtil.i("SyncActivity", "Constants.Force_Download_Sync_STATUS - " + Constants.Force_Download_Sync_STATUS);
                LogUtil.i("SyncActivity", "Constants.IS_SHOWING_DIALOG - " + Constants.IS_SHOWING_DIALOG);
                //Added Force Logout Module
                //If force sync is failed - APP_UPGRADE_STATUS will be true
                if (Constants.APP_UPGRADE_STATUS || Constants.Force_Download_Sync_STATUS) {
                    LogUtil.i("SyncActivity", "Forcelogout-condition check");
                    Constants.IS_SHOWING_DIALOG = false;
                    CommonUtils.forceLogout(mContext);
                }
                dialog.dismiss();
                goBackToCallerActivity();
            }
        });

        if (Constants.APP_UPGRADE_STATUS || Constants.Force_Download_Sync_STATUS) {
            LogUtil.i("SyncActivity", "Show dialog : Constants.IS_SHOWING_DIALOG - " + Constants.IS_SHOWING_DIALOG);
            if (!Constants.IS_SHOWING_DIALOG) {
                Constants.IS_SHOWING_DIALOG = true;
                LogUtil.i("SyncActivity", "show dialog : ++ - ");
                mErrorDialog = builder.create();
                mErrorDialog.show();
            }
        } else {
            LogUtil.i("SyncActivity", "show dialog : + - ");
            mErrorDialog = builder.create();
            mErrorDialog.show();
        }
    }

    /**
     * This Method identifies the callerActivity and calls the method which will redirect to callerActivity
     */
    private void goBackToCallerActivity() {
        String callerActivity = getCallerActivity(getIntent());
        if (callerActivity.equals(LOGIN_ACTIVITY)) {
            goBackToLoginActivity();
        } else {
            launchEJobListActivity();
        }
    }

    /**
     * This method launch's Goodyear Ejob Main Activity
     */
    private void goBackToLoginActivity() {
        Intent intent = new Intent(this, GoodYear_eJob_MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void saveDialogState(Bundle outState) {
        if (mErrorDialog != null && mErrorDialog.isShowing()) {
            outState.putBoolean("errorDialogShowing", true);
            outState.putString("dialog_msg", mDialogMsg);
            outState.putString("dialog_title", mDialogTitle);
        }
    }

    private void restoreDialogState(Bundle bundle) {
        if (bundle != null && bundle.getBoolean("errorDialogShowing")) {
            String msg = bundle.getString("dialog_msg");
            String title = bundle.getString("dialog_title");
            showAlertDialog(msg, title);
        }
    }

    private void dismissErrorDialog() {
        if (mErrorDialog != null && mErrorDialog.isShowing()) {
            mErrorDialog.dismiss();
        }
    }
}