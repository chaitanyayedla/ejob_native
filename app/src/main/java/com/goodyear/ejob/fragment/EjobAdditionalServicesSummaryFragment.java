package com.goodyear.ejob.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.interfaces.IEjobFragmentVisible;
import com.goodyear.ejob.interfaces.ISaveIconEjobFragment;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.EjobSaveIconStates;
import com.goodyear.ejob.util.JobBundle;
import com.goodyear.ejob.util.TireActionType;
import com.goodyear.ejob.util.Tyre;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Good Year
 */
public class EjobAdditionalServicesSummaryFragment extends Fragment implements
        IEjobFragmentVisible {

    public static final String TAG = EjobAdditionalServicesSummaryFragment.class
            .getSimpleName();
    private static int COLOR_WHITE = Color.parseColor("#E6E6E6");
    private static int COLOR_GREY = Color.parseColor("#D8D8D8");

    //private JobItem mJobItem;
    private Context mContext;
    public int mActionType = -1;
    public int mSapCode = -1;
    public String mService = "";
    public int mServiceCount = 0;

    /**
     * UI Views
     */
    public LinearLayout mServiceContainer;
    public View mSummaryRowLL;

    public TextView mLabelAccount;
    public TextView mValueAccount;

    public TextView mLabelVehId;
    public TextView mValueVehId;

    public TextView mLabelFleetNumber;
    public TextView mValueFleetNumber;

    private TextView mLabelRefNumber;
    public TextView mValueRefNumber;

    public TextView mLblService;
    public TextView mLblQuantity;

    public TextView mValueService;
    public TextView mValueQuantity;

    public Button mSaveButton;
    public ImageView mJobTypeIcon;
    private TextView mFragmentTitle;

    private LinearLayout mLinearHistoryContainer;
    private static ISaveIconEjobFragment saveIconHandlerActivity;
    private ArrayList<String> historyPositionsAdded;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        saveIconHandlerActivity = (ISaveIconEjobFragment) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.additional_services_summary,
                container, false);

        initalize(rootView);
        loadLabelsFromDB(rootView);
        createTireHistory();
        // Displaying The Additional Services Summary
        displayAdditionalServicesSummary(rootView, inflater);

        return rootView;
    }

    // CR - 525

    /**
     * This method initializes the history process, re-arranges the tyre objects
     * and loads the operations for the respective tires.
     */
    private void createTireHistory() {
        // TODO Auto-generated method stub
        historyPositionsAdded = new ArrayList<>();
        // sort tires in order of position
        // for more detail see Tyre.java
        if(VehicleSkeletonFragment.tyreInfo!=null) {
            Collections.sort(VehicleSkeletonFragment.tyreInfo);
            for (Tyre tyre : VehicleSkeletonFragment.tyreInfo) {

                //Commented for Fix : Bug 657 (when tyres are non maintained and inspected the eJob summary screen only shows the maintained tyres)
//                if (tyre.getTyreState() == TyreState.NON_MAINTAINED)
//                    continue;
                // get the operations for this tire
                ArrayList<JobItem> operationsPerformed = getTireOperationListIfWorkedOn(tyre);
                if (operationsPerformed != null && !operationsPerformed.isEmpty()) {
                    displayHistoryForTire(operationsPerformed, tyre);
                }
            }
        }
    }

    public void createPositionHeader(Tyre tyre) {
        TextView tirePosition = new TextView(mContext);
        String tirePos = tyre.getPosition();
        if (tirePos.contains("$")) {
            tirePos = tirePos.substring(0, tirePos.length() - 1);
        }
        tirePosition.setText(tirePos);
        tirePosition.setTypeface(null, Typeface.BOLD);
        tirePosition.setTextSize(20);
        mLinearHistoryContainer.addView(tirePosition);
    }

    // CR - 525

    /**
     * This method creates the Position node and displays the history of the
     * tire
     *
     * @param operationsPerformed list of job items
     * @param tyre                Tire for which the history should be displayed
     */
    private void displayHistoryForTire(ArrayList<JobItem> operationsPerformed,
                                       Tyre tyre) {
        int index = -1;
        String tirePos = tyre.getPosition();
        /* Bug 683 : Remove the summary of jobs performed on a tyre before dismount*/
        boolean isTyreDismountBool = tyre.isDismounted();

        if (tirePos.contains("$")) {
            tirePos = tirePos.substring(0, tirePos.length() - 1);
        }
        if (historyPositionsAdded.contains(tirePos)) {
            index = getIndexIfPositionExists(tirePos);
        } else {
            historyPositionsAdded.add(tirePos);
            createPositionHeader(tyre);
        }

        // loop over the operations and display history for the tire
        for (JobItem jobItem : operationsPerformed) {
            switch (jobItem.getActionType()) {
                case TireActionType.MOUNTPW:
                    if(isTyreDismountBool){
                        // do nothing
                    } else {
                        createHistoryForMount(getlabelByID(Constants.MOUNT_PWT_TIRE_LABEL), tyre, index);
                    }
                    break;
                case TireActionType.MOUNTNEW:
                    if(isTyreDismountBool){
                        // do nothing
                    } else {
                        createHistoryForMount(getlabelByID(Constants.Label_Summary_Mount_new), tyre, index);
                    }
                    break;
                case TireActionType.REGROOVE:
                    if(isTyreDismountBool){
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.REGROOV_ACTIVITY_NAME), index);
                    }
                    break;
                case TireActionType.REMOVE:
                    createHistoryViewForDismount(getlabelByID(Constants.Label_Summary_Dismount), tyre, jobItem, index);
                    break;
                case TireActionType.SWAP:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        if (TireActionType.SWAP_ACTION_PHYSICAL.equals(jobItem.getSwapType())) {
                            createHistoryViewForSwap(getlabelByID(Constants.Label_Summary_Physical_swap), jobItem, index);
                        } else {
                            createHistoryViewForSwap(getlabelByID(Constants.Label_Summary_Logical_swap), jobItem, index);
                        }
                    }

                    break;
                case TireActionType.TOR:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.TURN_ON_RIM_ACTIVITY_NAME), index);
                    }
                    break;
                case TireActionType.INSPECTION:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        //Added Inspection summary for ejob summary
                        createHistoryViewWithHeader(getlabelByID(Constants.LABEL_INSPECTION), index);
                    }
                    break;
                case TireActionType.VALVEFITTED:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Valve_fitted), index);
                    }
                    break;
                case TireActionType.HIGHTPRESSUREVALUECAPFITTED:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_High_pressure_valve_cap_fitted),index);
                    }
                    break;
                case TireActionType.VALUEFIXINGSUPPORT:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Valve_fixing_support), index);
                    }
                    break;
                case TireActionType.DYNAMICBALANCING:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Dynamic_balancing), index);
                    }
                    break;
                case TireActionType.STATICBALANCING:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Static_balancing), index);
                    }
                    break;
                case TireActionType.RIGIDVALVEEXTENSINFITTED:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Rigid_valve_extension_fitted),
                                index);
                    }
                    break;
                case TireActionType.FLEXIBLEVALVEEXTENSINFITTED:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Flexible_valve_extension_fitted),
                                index);
                    }
                    break;
                case TireActionType.PUNCTUREDREPAIRREPAIRED:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Puncture_repair), index);
                    }
                    break;
                case TireActionType.MINORREPAIR:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Minor_repair), index);
                    }
                    break;
                case TireActionType.MAJORREPAIR:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Major_repair), index);
                    }
                    break;
                case TireActionType.TYREREPAIREANDREINFORCEMENT:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Tyre_repair_and_reinforcement),
                                index);
                    }
                    break;
                case TireActionType.BREAKSPOTREPAIR:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Brake_spot_repair), index);
                    }
                    break;
                case TireActionType.MAJORREPAIRWITHVULCANISATION:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Major_repair_with_vulcanisation),
                                index);
                    }
                    break;
                case TireActionType.TIREFILL:
                    if (isTyreDismountBool) {
                        // do nothing
                    } else {
                        createHistoryViewWithHeader(getlabelByID(Constants.Label_Tyre_fill), index);
                    }
                    break;
            }
        }
    }

    /**
     * This method checks if tire position is already added if yes then the job items are added
     * below the existing tire position label.
     *
     * @param tyrePos tire position to check
     * @return index position
     */
    private int getIndexIfPositionExists(String tyrePos) {
        int itrator = -1;
        boolean isPositionFound = false;

        for (itrator = 0; itrator < mLinearHistoryContainer.getChildCount(); itrator++) {
            View attachedView = mLinearHistoryContainer.getChildAt(itrator);
            if (!(attachedView instanceof TextView)) {
                continue;
            } else {
                if (isPositionFound) {
                    break;
                }
                TextView positionView = (TextView) attachedView;
                if (positionView.getText().toString().equals(tyrePos)) {
                    isPositionFound = true;
                }
            }
        }
        return itrator;
    }

    // CR - 525

    /**
     * This method creates history for both mount new and mount pwt tire
     * operation.
     *
     * @param operationName Operation name to be displayed
     * @param tyre          Tire to be displayed
     */
    private void createHistoryForMount(String operationName, Tyre tyre,
                                       int index) {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.tire_history_tire_info, null, false);
        TextView brandname = (TextView) view
                .findViewById(R.id.text_history_brand);
        brandname.setText(tyre.getBrandName());

        TextView modelName = (TextView) view
                .findViewById(R.id.text_history_model);
        modelName.setText(tyre.getDesignDetails());

        TextView operationNameHeader = (TextView) view
                .findViewById(R.id.text_history_operation_name);
        operationNameHeader.setText(operationName);

        addViewToHistoryContainer(view, index);
    }

    /**
     * This method adds the given history to the history container.
     *
     * @param view  View to add to history container
     * @param index index postion to add to.
     */
    private void addViewToHistoryContainer(View view, int index) {
        // if (index > 0)
        // mLinearHistoryContainer.addView(view, index);
        // else
        mLinearHistoryContainer.addView(view);
    }

    // CR - 525

    /**
     * This method creates history for Dismount operation
     *
     * @param operationName Operation name to be displayed
     * @param tyre          Tire to be shown.
     */
    private void createHistoryViewForDismount(String operationName, Tyre tyre,
                                              JobItem jobItem, int index) {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.tire_history_tire_info, null, false);
        TextView brandname = (TextView) view
                .findViewById(R.id.text_history_brand);
        brandname.setText(tyre.getBrandName());

        TextView modelName = (TextView) view
                .findViewById(R.id.text_history_model);
        modelName.setText(tyre.getDesignDetails());

        TextView operationNameHeader = (TextView) view
                .findViewById(R.id.text_history_operation_name);
        operationNameHeader.setText(operationName);

        TextView threadDepth = (TextView) view
                .findViewById(R.id.text_history_thread_depth);
        threadDepth.setVisibility(TextView.VISIBLE);
        threadDepth.setText("NSK " + tyre.getNsk());

        TextView removalReason = (TextView) view
                .findViewById(R.id.text_history_removal_reason);
        removalReason.setVisibility(TextView.VISIBLE);
        removalReason.setText(getRemovalReasonFromDB(jobItem
                .getRemovalReasonId()));

        addViewToHistoryContainer(view, index);
    }

    // CR - 525

    /**
     * Get the removal reason from DB for dismount operation.
     *
     * @param removalReasonId removal reason id of dismount operation
     */
    private String getRemovalReasonFromDB(String removalReasonId) {
        DatabaseAdapter dbAdapter = new DatabaseAdapter(mContext);
        dbAdapter.open();
        String reason = dbAdapter.getRemovalReasonFromId(removalReasonId);
        dbAdapter.close();
        return reason;
    }

    // CR - 525

    /**
     * This method creates history with only header.
     *
     * @param operationName Operation name to be displayed
     */
    private void createHistoryViewWithHeader(String operationName, int index) {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.tire_history_tire_info, null, false);
        TextView brandname = (TextView) view
                .findViewById(R.id.text_history_brand);
        brandname.setVisibility(TextView.GONE);

        TextView modelName = (TextView) view
                .findViewById(R.id.text_history_model);
        modelName.setVisibility(TextView.GONE);

        TextView operationNameHeader = (TextView) view
                .findViewById(R.id.text_history_operation_name);
        operationNameHeader.setText(operationName);

        addViewToHistoryContainer(view, index);
    }

    // CR - 525

    /**
     * This method creates history for swap operation.
     *
     * @param operationName Operation name to be displayed
     */
    private void createHistoryViewForSwap(String operationName,
                                          JobItem jobItem, int index) {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.tire_history_tire_info, null, false);
        TextView brandname = (TextView) view
                .findViewById(R.id.text_history_brand);
        brandname.setVisibility(TextView.GONE);

        TextView modelName = (TextView) view
                .findViewById(R.id.text_history_model);
        modelName.setVisibility(TextView.GONE);

        TextView operationNameHeader = (TextView) view
                .findViewById(R.id.text_history_operation_name);
        operationNameHeader.setText(operationName);

        TextView originalPosition = (TextView) view
                .findViewById(R.id.text_history_model);
        originalPosition.setVisibility(TextView.VISIBLE);
        originalPosition.setText(jobItem.getSwapOriginalPosition());

        addViewToHistoryContainer(view, index);
    }

    // CR - 525

    /**
     * This method checks the job item and returns the list of job items for
     * performed operations
     *
     * @param tyre Tire to check in job items
     * @return List of job item if actions were performed on the tire
     */
    private ArrayList<JobItem> getTireOperationListIfWorkedOn(Tyre tyre) {
        ArrayList<JobItem> jobOperationPerformedOnTire = new ArrayList<>();
        if (tyre == null)
            return null;

        // check for operations in the job item list.
        for (JobItem jobItem : VehicleSkeletonFragment.mJobItemList) {
            if (jobItem.getTyreID().equals(tyre.getExternalID())) {
                jobOperationPerformedOnTire.add(jobItem);
            }
        }
        return jobOperationPerformedOnTire;
    }

    /**
     * Loading The Labels From DB
     *
     * @param rootView
     */
    private void loadLabelsFromDB(View rootView) {
        String labelFromDB = "";
        DatabaseAdapter dbHelper = new DatabaseAdapter(mContext);
        dbHelper.createDatabase();
        dbHelper.open();
        try {
            mFragmentTitle.setText(dbHelper
                    .getLabel(Constants.VEHICLE_SUMMARY_TITLE));
            mLblService.setText(dbHelper.getLabel(Constants.LABEL_SERVICE));
            mLblQuantity.setText(dbHelper.getLabel(Constants.LABEL_QUANTITY));
            // header
            labelFromDB = dbHelper.getLabel(Constants.ACCOUNT_LABEL);
            mLabelAccount.setText(labelFromDB);
            labelFromDB = dbHelper.getLabel(Constants.VEH_ID_LABEL);
            mLabelVehId.setText(labelFromDB);
            mLabelRefNumber.setText("#");
            if (!Constants.COMESFROMUPDATE && !Constants.COMESFROMVIEW) {
                mValueFleetNumber.setText(Constants.FLEETNUM);
                mValueRefNumber.setText(Constants.JOBREFNUMBER);
                mValueAccount.setText(Constants.ACCOUNTNAME);
                mValueVehId.setText(Constants.VEHID);
            } else if (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW) {
                mValueRefNumber.setText(JobBundle.getmRefNO().toString());
                mValueVehId.setText(JobBundle.getmLPlate().toString());
                mValueAccount.setText(JobBundle.getmAccountName());
                mValueFleetNumber.setText(JobBundle.getmFleetNO());
            }
            labelFromDB = dbHelper.getLabel(Constants.FLEET_NO);
            mLabelFleetNumber.setText(labelFromDB);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (dbHelper != null) {
                dbHelper.close();
            }
        }
    }

    /**
     * Initializing The UI Labels
     *
     * @param view
     */
    private void initalize(View view) {
        // header
        mFragmentTitle = (TextView) view.findViewById(R.id.textView1);
        mLabelAccount = (TextView) view.findViewById(R.id.lbl_account);
        mValueAccount = (TextView) view.findViewById(R.id.value_account);

        mLabelVehId = (TextView) view.findViewById(R.id.lbl_vehId);
        mValueVehId = (TextView) view.findViewById(R.id.value_vehId);

        mLabelFleetNumber = (TextView) view.findViewById(R.id.lbl_fleetNo);
        mValueFleetNumber = (TextView) view.findViewById(R.id.value_fleetNo);

        mLabelRefNumber = (TextView) view.findViewById(R.id.lbl_jobRefNo);
        mValueRefNumber = (TextView) view.findViewById(R.id.value_jobRefNo);

        mSaveButton = (Button) view.findViewById(R.id.btn_save);

        mLinearHistoryContainer = (LinearLayout) view
                .findViewById(R.id.linear_tire_history_container);

        if (Constants.COMESFROMVIEW) {
            mSaveButton.setVisibility(View.INVISIBLE);
        } else {
            mSaveButton.setVisibility(View.VISIBLE);
        }

		mSaveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/**
				 * Condition added to disable save button once clicked during Save Job in DB
				 */
				if (saveIconHandlerActivity.getSaveIconState() == EjobSaveIconStates.ICON_BLACK) {
					disableSaveButton();
					EjobFormActionActivity.saveJobInfo(v, true);
				} else if (saveIconHandlerActivity.getSaveIconState() == EjobSaveIconStates.ICON_RED) {
					disableSaveButton();
					EjobFormActionActivity.saveJobInfo(v, false);
					finalSaveButton();
				}
			}
		});
		// job icon on header
		mJobTypeIcon = (ImageView) view.findViewById(R.id.jobIcon);
		if (!Constants.JOBTYPEREGULAR) {
			mJobTypeIcon.setImageResource(R.drawable.breakdown_job);
		}
		// Form
		mServiceContainer = (LinearLayout) view
				.findViewById(R.id.serviceContainer);
		mLblService = (TextView) view.findViewById(R.id.lbl_service);
		mLblQuantity = (TextView) view.findViewById(R.id.lbl_Quantity); 
	}

    /**
     * Will display the additional Services
     *
     * @param rootView
     * @param inflater
     */
    private void displayAdditionalServicesSummary(View rootView,
                                                  LayoutInflater inflater) {
        DatabaseAdapter dbHelper = new DatabaseAdapter(mContext);
        dbHelper.createDatabase();
        dbHelper.open();
        if (VehicleSkeletonFragment.mJobItemList != null
                && VehicleSkeletonFragment.mJobItemList.size() > 0) {
            for (int i = 0; i < VehicleSkeletonFragment.mJobItemList.size(); i++) {
                JobItem jobItem = VehicleSkeletonFragment.mJobItemList.get(i);
                if (jobItem != null) {
                    try {
                        mActionType = Integer.valueOf(jobItem.getActionType());
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    if (mActionType == 5 && jobItem.getDeleteStatus() != 1) {
                        try {
                            mSapCode = Integer.valueOf(jobItem.getServiceID());
                            mServiceCount = Integer.valueOf(jobItem
                                    .getServiceCount());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                        if (mSapCode != -1) {
                            mService = dbHelper.getServiceByserviceId(mSapCode);
                            mSummaryRowLL = inflater.inflate(
                                    R.layout.additional_services_summary_row,
                                    null);
                            if (i % 2 == 1) {
                                mSummaryRowLL.setBackgroundColor(COLOR_WHITE); // color.white
                            } else {
                                mSummaryRowLL.setBackgroundColor(COLOR_GREY); // color.ltgreay
                            }
                            mValueService = (TextView) mSummaryRowLL
                                    .findViewById(R.id.value_serviceSummary);
                            mValueQuantity = (TextView) mSummaryRowLL
                                    .findViewById(R.id.value_QuantitySummary);
                            mValueService.setText(mService);
                            mValueQuantity.setText(String
                                    .valueOf(mServiceCount));
                            mServiceContainer.addView(mSummaryRowLL);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            CommonUtils.hideKeyboard(mContext, getView().getWindowToken());
        }
    }

    /**
     * Method enabling the save icon as per the user selection on the UI
     * It will always turn the save icon color into red if there will be data to be saved
     * for the local database file
     */
    public void enableSaveButton() {
        if (mSaveButton != null) {
            mSaveButton.setEnabled(true);
            saveIconHandlerActivity
                    .setSaveIconState(EjobSaveIconStates.ICON_RED);
            mSaveButton
                    .setBackgroundResource(R.drawable.continue_savejob_navigation);
        }
    }

    /**
     * Method disabling the save icon as per the user selection on the UI
     * It will always turn the save icon color into grey if there will not be any
     * data to be saved for the local database file
     */
    public void disableSaveButton() {
        if (mSaveButton != null) {
            mSaveButton.setEnabled(false);
            saveIconHandlerActivity
                    .setSaveIconState(EjobSaveIconStates.ICON_GRAY);
            mSaveButton.setBackgroundResource(R.drawable.unsavejob_navigation);
        }
    }

    /**
     * This method will always turn the save icon color into black
     * if there will not be any data to be saved for the local database file
     * It will navigate user back to the eJoblist
     */
    public void finalSaveButton() {
        if (mSaveButton != null) {
			mSaveButton.setEnabled(true);
            saveIconHandlerActivity
                    .setSaveIconState(EjobSaveIconStates.ICON_BLACK);
            mSaveButton.setBackgroundResource(R.drawable.savejob_navigation);
        }
    }

    /**
     * This method is called from view pager listener when this fragment is
     * selected. It updates the save icon based on the status of save button in
     * last fragment.
     */
    private void updateSaveButtonOnUIBasedOnLastFragment() {
        // TODO Auto-generated method stub
        if (saveIconHandlerActivity == null)
            return;
        int saveIconState = saveIconHandlerActivity.getSaveIconState();
        switch (saveIconState) {
            case EjobSaveIconStates.ICON_GRAY:
                disableSaveButton();
                break;
            case EjobSaveIconStates.ICON_BLACK:
                finalSaveButton();
                break;
            case EjobSaveIconStates.ICON_RED:
                enableSaveButton();
                break;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.goodyear.ejob.interfaces.IEjobFragmentVisible#fragmentIsVisibleToUser
     * ()
     */
    @Override
    public void fragmentIsVisibleToUser() {
        updateSaveButtonOnUIBasedOnLastFragment();
    }

    /*
     * (non-Javadoc)
     *
     * @see android.support.v4.app.Fragment#onDetach()
     */
    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        saveIconHandlerActivity = null;
    }

    public String getlabelByID(int labelID)
    {
        DatabaseAdapter label = new DatabaseAdapter(mContext);
        label.createDatabase();
        label.open();
        String mLabelName = label
                .getLabel(labelID);
        label.close();
        return mLabelName;
    }
}
