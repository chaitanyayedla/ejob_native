package com.goodyear.ejob.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.adapter.EJobPagerAdapter;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.interfaces.IEjobFragmentVisible;
import com.goodyear.ejob.interfaces.IFragmentCommunicate;
import com.goodyear.ejob.interfaces.ISaveIconEjobFragment;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.EjobSaveIconStates;
import com.goodyear.ejob.util.GPS_Utils;
import com.goodyear.ejob.util.Job;
import com.goodyear.ejob.util.JobBundle;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.Validation;

import java.util.Date;

/**
 * The activity must implement the {@link ISaveIconEjobFragment} to add this
 * Fragment. See {@link ISaveIconEjobFragment} for more information.
 */
public class EjobTimeLocationFragment extends Fragment implements
		IFragmentCommunicate, OnClickListener,
		IEjobFragmentVisible {
	private GPS_Utils gps;

	// android
	private static ISaveIconEjobFragment saveIconHandlerActivity;
	public static TextView sLatitude;
	public static TextView sLongitude;
	public static TextView sLocationGPS;

	public static TextView sRecivedOnTime;
	public static TextView sStartedOnTime;
	public static TextView sReachedOnTime;
	public static TextView sFinishedOnTime;

	public static TextView sRecivedOnDate;
	public static TextView sStartedOnDate;
	public static TextView sReachedOnDate;
	public static TextView sFinishedOnDate;

	public static TextView sLabelRecivedOn;
	public static TextView sLabelStartedOn;
	public static TextView sLabelReachedOn;
	public static TextView sLabelFinishedOn;
	public static TextView sLabelStockLocation;
	public static TextView sLabelLocation;

	public static TextView sLabelAccount;
	public static TextView sValueAccount;
	public static TextView sLabelVehId;
	public static TextView sValueVehId;
	public static TextView sLabelFleetNumber;
	public static TextView sValueFleetNumber;
	public static TextView sLabelRefNumber;
	public static TextView sValueRefnum;

	public static EditText sValueLocation;
	public static EditText sStockLocation;
	public static Button sSaveButton;
	public static ImageView sJobTypeIcon;

	public static TextView sActivityName;

	public static String sNO_GPS;
	public static Context context;
	public static View view;

	public static String sTimeRecievedSaved;
	public static String sTimeStartedSaved;
	public static String sTimeOnSiteSaved;
	public static String sTimeFinishedSaved;

	public static String sDateRecievedSaved;
	public static String sDateStartedSaved;
	public static String sDateOnSiteSaved;
	public static String sDateFinishedSaved;

	public static boolean DATE_TIME_SAVED;
	private static String sLocationErrorMessage;

	public int mCountScreenNotVisibleToUser;

	private int saveIconState;

	private static TextView sLabelLatitude;
	private static TextView sLabelLongitude;

	//User Trace logs trace tag
	private static final String TRACE_TAG = "Job Form - 2";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.ejob_time_location,
				container, false);

		context = getActivity();
		view = rootView;

		// loadLabelsFromDB(rootView);
		gps = new GPS_Utils(getActivity());
		if (!gps.canGetLocation())
			gps.showSettingsAlert();

		initalize(rootView);
		loadLabelsFromDB(rootView);

		// Retaining The Values On Orientation Change(Starts)
		if (savedInstanceState != null) {
			sDateRecievedSaved = savedInstanceState
					.getString("date_receivedOn");
			sTimeRecievedSaved = savedInstanceState
					.getString("time_receivedOn");
			sRecivedOnDate.setText(sDateRecievedSaved);
			sRecivedOnTime.setText(sTimeRecievedSaved);

			sDateStartedSaved = savedInstanceState.getString("date_startedOn");
			sTimeStartedSaved = savedInstanceState.getString("time_startedOn");
			sStartedOnDate.setText(sDateStartedSaved);
			sStartedOnTime.setText(sTimeStartedSaved);

			sDateOnSiteSaved = savedInstanceState.getString("date_reachedOn");
			sTimeOnSiteSaved = savedInstanceState.getString("time_reachedOn");
			sReachedOnDate.setText(sDateOnSiteSaved);
			sReachedOnTime.setText(sTimeOnSiteSaved);

			sDateFinishedSaved = savedInstanceState
					.getString("date_finishedOn");
			sTimeFinishedSaved = savedInstanceState
					.getString("time_finishedOn");
			sFinishedOnDate.setText(sDateFinishedSaved);
			sFinishedOnTime.setText(sTimeFinishedSaved);

		}

		// Retaining The Values On Orientation Change(Ends)
		if(Constants.COMESFROMUPDATE && savedInstanceState == null){
			finalSaveButton();
		}
		return rootView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onAttach(android.app.Activity)
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		// add the save icon handler
		saveIconHandlerActivity = (ISaveIconEjobFragment) activity;
	}

	/**
	 * set data from GPS
	 */
	private void setGPSLocation() {
		if (gps.canGetLocation()) {
			sLatitude.setText(String.valueOf(gps.getLatitude()));
			sLongitude.setText(String.valueOf(gps.getLongitude()));
			sLocationGPS.setText(String.valueOf(gps.getAddress()));
			if (gps.getLatitude() == 0.0) {
				// CommonUtils.notify(sNO_GPS, getActivity());
			}
		}
	}

	/**
	 * get data from already stored GPS
	 */
	private void setRetrievedGPSLocation() {
		String latitude = "";
		String longitude = "";
		String address = "";
		if (!TextUtils.isEmpty(Job.getGpsLocation())) {
			String[] splittedGpsLocation = Job.getGpsLocation().split("\\|");
			if (splittedGpsLocation.length == 3) {
				latitude = splittedGpsLocation[0];
				longitude = splittedGpsLocation[1];
				address = splittedGpsLocation[2];
			} else if (splittedGpsLocation.length == 2) {
				latitude = splittedGpsLocation[0];
				longitude = splittedGpsLocation[1];
			}
		}
		sLatitude.setText(latitude);
		sLongitude.setText(longitude);
		sLocationGPS.setText(address);

	}

	@Override
	public void onResume() {
		super.onResume();
		System.out.println("onResume---");

		boolean isJobPresent = false;
		if (Constants.jobIdEdit != null) {
			DatabaseAdapter label = new DatabaseAdapter(getActivity());
			label.createDatabase();
			label.open();
			isJobPresent = label.isJobPresentAtJobItem(Constants.jobIdEdit);
			label.close();
		}

		if (isJobPresent) {
			setRetrievedGPSLocation();

		} else {
			System.out.println("onResume --out Condityion");
			setGPSLocation();
			if (sLatitude != null && sLongitude != null && sLocationGPS != null) {
				if (Constants.COMESFROMUPDATE && !Constants.COMESFROMVIEW)

					Job.setGpsLocation(sLatitude.getText().toString().trim()
							+ "|" + sLongitude.getText().toString().trim()
							+ "|" + sLocationGPS.getText().toString().trim());

			}
			// updateLocationOnChange();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onSaveInstanceState(android.os.Bundle)
	 */
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (null != outState) {
			if (sRecivedOnDate != null) {
				outState.putString("date_receivedOn", sRecivedOnDate.getText()
						.toString());
			}
			if (sRecivedOnTime != null) {
				outState.putString("time_receivedOn", sRecivedOnTime.getText()
						.toString());
			}
			if (sStartedOnDate != null) {
				outState.putString("date_startedOn", sStartedOnDate.getText()
						.toString());
			}
			if (sStartedOnTime != null) {
				outState.putString("time_startedOn", sStartedOnTime.getText()
						.toString());
			}
			if (sReachedOnDate != null) {
				outState.putString("date_reachedOn", sReachedOnDate.getText()
						.toString());
			}
			if (sReachedOnTime != null) {
				outState.putString("time_reachedOn", sReachedOnTime.getText()
						.toString());
			}
			if (sFinishedOnDate != null) {
				outState.putString("date_finishedOn", sFinishedOnDate.getText()
						.toString());
			}
			if (sFinishedOnTime != null) {
				outState.putString("time_finishedOn", sFinishedOnTime.getText()
						.toString());
			}
		}
	}

	private void initalize(View rootView) {
		String currentTime;
		String currentDate;
		System.out.println("initalize---");
		sRecivedOnDate = (TextView) rootView
				.findViewById(R.id.value_recievedOnDate);

		sStartedOnDate = (TextView) rootView
				.findViewById(R.id.value_startedOnDate);

		sReachedOnDate = (TextView) rootView
				.findViewById(R.id.value_reachedSiteOnDate);

		sFinishedOnDate = (TextView) rootView
				.findViewById(R.id.value_finishedOnDate);

		sRecivedOnTime = (TextView) rootView
				.findViewById(R.id.value_recievedOnTime);

		sStartedOnTime = (TextView) rootView
				.findViewById(R.id.value_startedOnTime);

		sReachedOnTime = (TextView) rootView
				.findViewById(R.id.value_reachedSiteOnTime);

		sFinishedOnTime = (TextView) rootView
				.findViewById(R.id.value_finishedOnTime);

		if (!DATE_TIME_SAVED) {
			currentDate = DateTimeUTC.getCurrentDate(context);
			currentTime = DateTimeUTC.getCurrentTime();

			sRecivedOnDate.setText(currentDate);

			sStartedOnDate.setText(currentDate);

			sReachedOnDate.setText(currentDate);

			sFinishedOnDate.setText(currentDate);

			sRecivedOnTime.setText(currentTime);

			sStartedOnTime.setText(currentTime);

			sReachedOnTime.setText(currentTime);

			sFinishedOnTime.setText(currentTime);

			EjobTimeLocationFragment.DATE_TIME_SAVED = true;

		} else {

			sRecivedOnTime.setText(sTimeRecievedSaved);

			sStartedOnTime.setText(sTimeStartedSaved);

			sReachedOnTime.setText(sTimeOnSiteSaved);

			sFinishedOnTime.setText(sTimeFinishedSaved);

			sRecivedOnDate.setText(sDateRecievedSaved);

			sStartedOnDate.setText(sDateStartedSaved);

			sReachedOnDate.setText(sDateOnSiteSaved);

			sFinishedOnDate.setText(sDateFinishedSaved);


			String traceMsg="";

			try
			{
				if(sTimeRecievedSaved!=null)
				{
					traceMsg = traceMsg + " " +sTimeRecievedSaved+" ";
				}
				else
				{
					traceMsg = traceMsg + " ";
				}

				if(sDateRecievedSaved!=null)
				{
					traceMsg = traceMsg + "| " +sDateRecievedSaved+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(sTimeStartedSaved!=null)
				{
					traceMsg = traceMsg + "| " +sTimeStartedSaved+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(sDateStartedSaved!=null)
				{
					traceMsg = traceMsg + "| " +sDateStartedSaved+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(sTimeOnSiteSaved!=null)
				{
					traceMsg = traceMsg + "| " +sTimeOnSiteSaved+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(sDateOnSiteSaved!=null)
				{
					traceMsg = traceMsg + "| " +sDateOnSiteSaved+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(sTimeFinishedSaved!=null)
				{
					traceMsg = traceMsg + "| " +sTimeFinishedSaved+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(sDateFinishedSaved!=null)
				{
					traceMsg = traceMsg + "| " +sDateFinishedSaved+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				LogUtil.TraceInfo(TRACE_TAG, "Initial Job Time Details : ", traceMsg, true, true, true);
			}
			catch (Exception e)
			{
				LogUtil.TraceInfo(TRACE_TAG,"Initial Job Time Details : Exception : ",e.getMessage(),true,true,true);
			}

		}
		sLatitude = (TextView) rootView.findViewById(R.id.value_locLatitude);
		sLongitude = (TextView) rootView.findViewById(R.id.value_locLongitude);
		sLocationGPS = (TextView) rootView
				.findViewById(R.id.value_locationAddress);
		DatabaseAdapter label = new DatabaseAdapter(context);
		label.createDatabase();
		label.open();
		String gpsNotAvailable = label.getLabel(Constants.GPS_NOT_AVAILABLE);
		sLocationGPS.setText(gpsNotAvailable);
		label.close();
		/*
		 * if(Constants.COMESFROMVIEW){ String gpsLoc = Job.getGpsLocation();
		 * if(!gpsLoc.isEmpty()||!gpsLoc.equals("")){ String[] tokensVal =
		 * gpsLoc.split("|"); latitude.setText(tokensVal[0]);
		 * longitude.setText(tokensVal[1]); locationGPS.setText(tokensVal[2]); }
		 * }
		 */

		sActivityName = (TextView) rootView.findViewById(R.id.textView1);

		// lables
		sLabelRecivedOn = (TextView) rootView.findViewById(R.id.lbl_recievedOn);
		sLabelStartedOn = (TextView) rootView.findViewById(R.id.lbl_startedOn);
		sLabelReachedOn = (TextView) rootView
				.findViewById(R.id.lbl_reachedSiteOn);
		sLabelFinishedOn = (TextView) rootView
				.findViewById(R.id.lbl_finishedOn);
		sLabelStockLocation = (TextView) rootView
				.findViewById(R.id.lbl_stockLocation);
		sLabelLocation = (TextView) rootView.findViewById(R.id.lbl_location);

		sLabelAccount = (TextView) rootView.findViewById(R.id.lbl_account);
		sValueAccount = (TextView) rootView.findViewById(R.id.value_account);

		sLabelVehId = (TextView) rootView.findViewById(R.id.lbl_vehId);
		sValueVehId = (TextView) rootView.findViewById(R.id.value_vehId);

		sLabelFleetNumber = (TextView) rootView.findViewById(R.id.lbl_fleetNo);
		sValueFleetNumber = (TextView) rootView
				.findViewById(R.id.value_fleetNo);

		sLabelLatitude = (TextView) rootView.findViewById(R.id.lbl_locLatitude);
		sLabelLongitude = (TextView) rootView.findViewById(R.id.lbl_locLongitude);

		sSaveButton = (Button) rootView.findViewById(R.id.btn_save);
		sSaveButton.setEnabled(false);
		sSaveButton.setOnClickListener(this);

		sValueLocation = (EditText) rootView.findViewById(R.id.value_location);
		sStockLocation = (EditText) rootView
				.findViewById(R.id.value_stockLocation);
		// set field limit
		// sValueLocation.setFilters(new InputFilter[] { new
		// InputFilter.LengthFilter(50) });
		sValueLocation.setFilters(new InputFilter[] {
				new InputFilter.AllCaps(), new InputFilter.LengthFilter(50) });
		sValueLocation.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				boolean handled = false;
				if (actionId == EditorInfo.IME_ACTION_NEXT) {
					sStockLocation.requestFocus();
					handled = true;
				}
				return handled;
			}
		});

		// sStockLocation.setFilters(new InputFilter[] { new
		// InputFilter.LengthFilter(50) });
		sStockLocation.setFilters(new InputFilter[] {
				new InputFilter.AllCaps(), new InputFilter.LengthFilter(50) });

		sJobTypeIcon = (ImageView) rootView.findViewById(R.id.jobIcon);
		if (!Constants.JOBTYPEREGULAR) {
			sJobTypeIcon.setImageResource(R.drawable.breakdown_job);
		}
		sLabelRefNumber = (TextView) rootView.findViewById(R.id.lbl_jobRefNo);
		sLabelRefNumber.setText("#");
		sValueRefnum = (TextView) rootView.findViewById(R.id.value_jobRefNo);
		sValueRefnum.setText(Constants.JOBREFNUMBER);

		if (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW) {
			sValueLocation.setText(JobBundle.getLocation().toString());
			sStockLocation.setText(JobBundle.getmStockLocation().toString());
			sValueRefnum.setText(JobBundle.getmRefNO().toString());
			sValueVehId.setText(JobBundle.getmLPlate().toString());
			sValueAccount.setText(JobBundle.getmAccountName());
			sValueFleetNumber.setText(JobBundle.getmFleetNO());

			String latitude = "";
			String longitude = "";
			String address = "";
			if (!TextUtils.isEmpty(Job.getGpsLocation())) {
				String[] splittedGpsLocation = Job.getGpsLocation()
						.split("\\|");
				if (splittedGpsLocation.length == 3) {
					latitude = splittedGpsLocation[0];
					longitude = splittedGpsLocation[1];
					address = splittedGpsLocation[2];
				} else if (splittedGpsLocation.length == 2) {
					latitude = splittedGpsLocation[0];
					longitude = splittedGpsLocation[1];
				}
			}

			sLatitude.setText(latitude);
			sLongitude.setText(longitude);
			sLocationGPS.setText(address);

			String traceMsg="";
			try
			{
				if(JobBundle.getLocation()!=null)
				{
					traceMsg = traceMsg + " " +JobBundle.getLocation()+" ";
				}
				else
				{
					traceMsg = traceMsg + " ";
				}

				if(latitude!=null)
				{
					traceMsg = traceMsg + "| " +latitude+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(longitude!=null)
				{
					traceMsg = traceMsg + "| " +longitude+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(address!=null)
				{
					traceMsg = traceMsg + "| " +address+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(JobBundle.getmStockLocation()!=null)
				{
					traceMsg = traceMsg + "| " +JobBundle.getmStockLocation()+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				LogUtil.TraceInfo(TRACE_TAG, "Initial Job Location Details : ", traceMsg, true, true, true);
			}
			catch (Exception e)
			{
				LogUtil.TraceInfo(TRACE_TAG,"Initial Job Location Details : Exception : ",e.getMessage(),true,true,true);
			}


		}

	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		int saveIconStatus;
		if (isVisibleToUser) {
			saveIconStatus = saveIconHandlerActivity.getSaveIconState();
		} else {
			saveIconStatus = EjobSaveIconStates.ICON_GRAY;
		}
		if (isVisibleToUser) {
			sValueLocation.addTextChangedListener(new LocationTextChanged());
			if (!sValueLocation.getText().toString().trim().equals("")
					&& saveIconStatus == EjobSaveIconStates.ICON_RED) {
				enableSaveButton();
				EjobInformationFragment.enableSaveButton();
			} else if (!sValueLocation.getText().toString().trim().equals("")
					&& saveIconStatus == EjobSaveIconStates.ICON_BLACK) {
				finalSaveButton();
			} else {
				if(Constants.COMESFROMUPDATE){
					enableSaveButton();
					EjobInformationFragment.enableSaveButton();
				}else{
					disableSaveButton();
					EjobInformationFragment.disableSaveButton();
				}
				
			}
		} else {
			mCountScreenNotVisibleToUser++;
			if (mCountScreenNotVisibleToUser > 0) {
				updateJobObject();
				mCountScreenNotVisibleToUser = 0;
			}
		}
	}

	/**
	 * @param rootView
	 */
	private void loadLabelsFromDB(View rootView) {
		DatabaseAdapter label = new DatabaseAdapter(getActivity());
		label.createDatabase();
		label.open();

		String activityname = label.getLabel(Constants.EJOB_FORM_ACTIVITY_NAME);
		sActivityName.setText(activityname);
		// header
		String labelFromDB = label.getLabel(Constants.ACCOUNT_LABEL);
		sLabelAccount.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.VEH_ID_LABEL);
		sLabelVehId.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.TIME_RECIEVED_LABEL);
		sLabelRecivedOn.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.TIME_STARTED_LABEL);
		sLabelStartedOn.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.TIME_ON_SITE_LABEL);
		sLabelReachedOn.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.TIME_FINISHED_LABEL);
		sLabelFinishedOn.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.STOCK_LOCATION_LABEL);
		sLabelStockLocation.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.FLEET_NO);
		sLabelFleetNumber.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.LOCATION_LABEL);
		sLabelLocation.setText(labelFromDB + "*");

		labelFromDB = label.getLabel(Constants.GPS_LAT);
		sLabelLatitude.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.GPS_LNG);
		sLabelLongitude.setText(labelFromDB);

		sNO_GPS = label.getLabel(Constants.GPS_ERROR_MESSAGE);

		// form
		labelFromDB = label.getLabel(Constants.ACCOUNT_LABEL);
		sLabelAccount.setText(labelFromDB);

		if (!Constants.COMESFROMUPDATE && !Constants.COMESFROMVIEW) {
			sValueAccount.setText(Constants.ACCOUNTNAME);
			sValueVehId.setText(Constants.VEHID);
			sValueFleetNumber.setText(Constants.FLEETNUM);
			sLabelRefNumber.setText("#");
			sValueRefnum.setText(Constants.JOBREFNUMBER);
		}

		sLocationErrorMessage = label
				.getLabel(Constants.LOCATION_REQUIRED_MESSAGE);
		label.close();
	}

	public static void changeTime(String time, int viewId) {
		switch (viewId) {
		case R.id.value_recievedOnTime:
			sRecivedOnTime.setText(time);
			break;
		case R.id.value_startedOnTime:
			sStartedOnTime.setText(time);
			break;
		case R.id.value_reachedSiteOnTime:
			sReachedOnTime.setText(time);
			break;
		case R.id.value_finishedOnTime:
			sFinishedOnTime.setText(time);
			break;
		default:
			break;
		}
	}

	public static void changeDate(String date, int viewId) {
		switch (viewId) {
		case R.id.value_recievedOnDate:
			sRecivedOnDate.setText(date);
			break;
		case R.id.value_startedOnDate:
			sStartedOnDate.setText(date);
			break;
		case R.id.value_reachedSiteOnDate:
			sReachedOnDate.setText(date);
			break;
		case R.id.value_finishedOnDate:
			sFinishedOnDate.setText(date);
			break;
		default:
			break;
		}
	}
	/**
	 * Method enabling the save icon as per the user selection on the UI
	 * It will always turn the save icon color into red if there will be data to be saved 
	 * for the local database file
	 */
	public static void enableSaveButton() {
		if (sSaveButton != null) {
			sSaveButton.setEnabled(true);
			Constants.FINAL_SAVE_STATE = false;
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_RED);
			sSaveButton
					.setBackgroundResource(R.drawable.continue_savejob_navigation);
		}
	}
	/**
	 * Method disabling the save icon as per the user selection on the UI
	 * It will always turn the save icon color into grey if there will not be any 
	 * data to be saved for the local database file
	 */
	public static void disableSaveButton() {
		if (sSaveButton != null) {
			sSaveButton.setEnabled(false);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_GRAY);
			sSaveButton.setBackgroundResource(R.drawable.unsavejob_navigation);
		}
	}
	/**
	 * This method will always turn the save icon color into black 
	 * if there will not be any data to be saved for the local database file
	 * It will navigate user back to the eJoblist
	 */
	public static void finalSaveButton() {
		if (sSaveButton != null) {
			sSaveButton.setEnabled(true);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_BLACK);
			sSaveButton.setBackgroundResource(R.drawable.savejob_navigation);
		}
	}

	public static Date getDate(int viewId) {
		switch (viewId) {
		case R.id.value_recievedOnDate:
			return getReachedDate(sRecivedOnTime, sRecivedOnDate);

		case R.id.value_startedOnDate:
			return getReachedDate(sStartedOnTime, sStartedOnDate);

		case R.id.value_reachedSiteOnDate:
			return getReachedDate(sReachedOnTime, sReachedOnDate);

		case R.id.value_finishedOnDate:
			return getReachedDate(sFinishedOnTime, sFinishedOnDate);
		default:
			return null;
		}
	}

	public static Date getReachedDate(TextView timeView, TextView dateView) {
		String[] date = DateTimeUTC.getSplittedDate(dateView.getText()
				.toString(), context);
		String[] time = DateTimeUTC.getSplittedTime(timeView.getText()
				.toString(), context);

		int year = 0;
		int month = 0;
		int day = 0;
		int hour = 0;
		int min = 0;

		if (date.length == 3 && time.length == 2) {
			try {
				year = Integer.parseInt(date[2]);
				month = Integer.parseInt(date[1]);
				day = Integer.parseInt(date[0]);
				hour = Integer.parseInt(time[0]);
				min = Integer.parseInt(time[1]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return Validation.getDate(year, month, day, hour, min, 0);
	}

	public static String getSetLocation() {
		return sValueLocation.getText().toString().trim();
	}

	public static void updateLocationOnChange() {
		if (sLatitude != null && sLongitude != null && sLocationGPS != null) {
			if (!Constants.COMESFROMUPDATE && !Constants.COMESFROMVIEW)

				Job.setGpsLocation(sLatitude.getText().toString().trim() + "|"
						+ sLongitude.getText().toString().trim() + "|"
						+ sLocationGPS.getText().toString().trim());

		}
	}

	public static void updateJobObject() {
		if (sValueLocation != null) {
			Job.setLocation(sValueLocation.getText().toString().trim());
		}
		if (sRecivedOnDate != null && sRecivedOnTime != null) {
			Long receivedTicks = DateTimeUTC.getDateInTicks(sRecivedOnDate,
					sRecivedOnTime, context);
			Job.setTimeReceived(String.valueOf(receivedTicks));
		}
		if (sStartedOnDate != null && sStartedOnTime != null) {
			Long startedTicks = DateTimeUTC.getDateInTicks(sStartedOnDate,
					sStartedOnTime, context);
			Job.setTimeStarted(String.valueOf(startedTicks));
		}
		if (sReachedOnDate != null && sReachedOnTime != null) {
			Long reachedTicks = DateTimeUTC.getDateInTicks(sReachedOnDate,
					sReachedOnTime, context);
			Job.setTimeOnSite(String.valueOf(reachedTicks));
		}
		if (sFinishedOnDate != null && sFinishedOnTime != null) {
			Long finishedTicks = DateTimeUTC.getDateInTicks(sFinishedOnDate,
					sFinishedOnTime, context);
			Job.setTimeFinished(String.valueOf(finishedTicks));
		}
		if (sLatitude != null && sLongitude != null && sLocationGPS != null) {
			if (!Constants.COMESFROMUPDATE && !Constants.COMESFROMVIEW)
				Job.setGpsLocation(sLatitude.getText().toString().trim() + "|"
						+ sLongitude.getText().toString().trim() + "|"
						+ sLocationGPS.getText().toString().trim());
		}
		if (sStockLocation != null) {
			Job.setStockLocation(sStockLocation.getText().toString().trim());
		}
	}

	public static boolean isSaveEnabled() {
		return sSaveButton.isEnabled();
	}

	private static class LocationTextChanged implements TextWatcher {

		@Override
		public void afterTextChanged(Editable arg0) {
			if (sValueLocation.getText().toString().trim().equals("")) {
				EJobPagerAdapter.count = 2;
				if (EjobFormActionActivity.sViewAdapter != null) {
					EjobFormActionActivity.sViewAdapter.notifyDataSetChanged();
				}

			}
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {

		}

		@Override
		public void onTextChanged(CharSequence text, int arg1, int arg2,
				int arg3) {
			String changed = text.toString();
			if (!changed.trim().equals("")) {
				if (Constants.JOBTYPEREGULAR) {

					EJobPagerAdapter.count = 8;
				} else {
					EJobPagerAdapter.count = 7;
				}

				if (EjobFormActionActivity.sViewAdapter != null) {
					EjobFormActionActivity.sViewAdapter.notifyDataSetChanged();
				}
				enableSaveButton();
				EjobInformationFragment.enableSaveButton();
			} else {
				disableSaveButton();
				CommonUtils.toastHandler(sLocationErrorMessage, context);
			}
		}
	}
	protected void hideKeyboard(Context context) {
		try {
		InputMethodManager in = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(getView().getWindowToken(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#
	 * onActivityBackPress()
	 */
	@Override
	public boolean onActivityBackPress() {
		return false;
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDetach() {
		super.onDetach();
		saveIconHandlerActivity = null;
	}

	/* (non-Javadoc)
	 * @see com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#hideKeyBoard(boolean)
	 */
	@Override
	public void hideKeyBoard(boolean toHide) {
		if (toHide) {
			hideKeyboard(this.getActivity());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View clickedView) {
		int clickedViewID = clickedView.getId();
		switch (clickedViewID) {
		case R.id.btn_save:
			if (saveIconHandlerActivity.getSaveIconState() == EjobSaveIconStates.ICON_BLACK) {
				disableSaveButton();
				EjobFormActionActivity.saveJobInfo(view, true);
			} else if (saveIconHandlerActivity.getSaveIconState() == EjobSaveIconStates.ICON_RED) {
				disableSaveButton();
				EjobFormActionActivity.saveJobInfo(view, false);
				finalSaveButton();
			}
		}
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.IEjobFragmentVisible#fragmentIsVisibleToUser
	 * ()
	 */
	@Override
	public void fragmentIsVisibleToUser() {
		updateSaveButtonOnUIBasedOnLastFragment();
	}
	
	/**
	 * This method is called from view pager listener when this fragment is
	 * selected. It updates the save icon based on the status of save button in
	 * last fragment.
	 */
	private void updateSaveButtonOnUIBasedOnLastFragment() {
		// TODO Auto-generated method stub
		if (saveIconHandlerActivity == null)
			return;
		saveIconState = saveIconHandlerActivity.getSaveIconState();
		switch (saveIconState) {
		case EjobSaveIconStates.ICON_GRAY:
			disableSaveButton();
			break;
		case EjobSaveIconStates.ICON_BLACK:
			finalSaveButton();
			break;
		case EjobSaveIconStates.ICON_RED:
			enableSaveButton();
			break;
		}
	}
}
