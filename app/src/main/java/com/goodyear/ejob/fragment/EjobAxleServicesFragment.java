package com.goodyear.ejob.fragment;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.interfaces.IEjobFragmentVisible;
import com.goodyear.ejob.interfaces.IFragmentCommunicate;
import com.goodyear.ejob.interfaces.ISaveIconEjobFragment;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.EjobSaveIconStates;
import com.goodyear.ejob.util.JobBundle;
import com.goodyear.ejob.util.SpinnerObject;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.VehicleSkeletonInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Good Year
 * 
 */
public class EjobAxleServicesFragment extends Fragment implements
		IFragmentCommunicate, IEjobFragmentVisible {

	private static String sPASSIVE = "";
	private static String sDRIVE = "";
	private static String sLIFT = "";
	private static String sSTEERING = "";
	private static String sSTEERnDRIVE = "";

	// private Cursor mCursor;
	public int[] mSpinnerValues;
	public ArrayList<Spinner> mSpinnerViews = new ArrayList<Spinner>();
	private SpinnerObject mSpinnerObject;
	private List<SpinnerObject> mSpinnerObjList;
	private int mActionType = 7;
	private int mServiceId = 870017;
	private int mAxleNumber = 0;
	private int mGeometryCheck = 0;
	private int mGeometryCorrection = 0;
	private int mAxleTypeId = 0;
	private String mAxleType = "";
	private String mAxleTypeValue = "";
	private JobItem mJobItem;
	private boolean mNeedToSave = false;
	private boolean mSaveButtonClicked = false;

	/**
	 * UI Views
	 */
	private View mRootView;
	private LinearLayout mServiceContainer;
	private View mAxleViewLL;

	private TextView mLabelAccount;
	private TextView mValueAccount;

	private TextView mLabelVehId;
	private TextView mValueVehId;

	public TextView mLabelFleetNumber;
	private TextView mValueFleetNumber;

	private TextView mLabelRefNumber;
	private TextView mValueRefNumber;
	private TextView mLblAxleServicesTitle;
	private TextView mLblAxleSpecification;
	private TextView mLblAxleType;
	private ImageView mJobTypeIcon;
	private Button mSaveJob;
	Spinner spAxleTypeValue;
	Bundle savedInstanceState;
	private Tyre mTyreForAxleService;

	private ISaveIconEjobFragment saveIconHandlerActivity;
	private static final String TAG = "AxleService";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.savedInstanceState = savedInstanceState;
		mRootView = inflater.inflate(R.layout.axle_services, container, false);
		initalize();
		loadLabelsFromDB();
		mSpinnerViews.clear();
		loadAxleServices(inflater);
		//Fix : to Retain Previously Selected Spinner Value (Axle services are not getting saved locally once tapped on save button)
		retainAxleServices();
		return mRootView;
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			mNeedToSave = true;
			retainAxleServices();
		} else {
			if (mNeedToSave) {
				saveAxleServices();
				mNeedToSave = false;
			}
		}
	}

	/**
	 * This methods will load all axle services based on
	 * sapContractorId,geometryCheck and geometryCorrection
	 * 
	 * @param rootView
	 * @param inflater
	 */
	private void loadAxleServices(LayoutInflater inflater) {
		Cursor cursorContract = null;
		DatabaseAdapter dbHelper = null;
		try {
			if (Constants.vehicleConfiguration != null
					&& !Constants.vehicleConfiguration.equals("")) {
				VehicleSkeletonInfo vehicleconfg = new VehicleSkeletonInfo(
						Constants.vehicleConfiguration);
				vehicleconfg.getVehicleSkeletonInfo();
				vehicleconfg.getAxlecount();
				String[] axleinfo = vehicleconfg.getVehicleSkeletonInfo();

				if (axleinfo != null && axleinfo.length > 0) {
					dbHelper = new DatabaseAdapter(getActivity());
					dbHelper.createDatabase();
					dbHelper.open();

					cursorContract = dbHelper.getContaractTyrePolicy(Integer
							.parseInt(Constants.SAP_CONTRACTNUMBER_ID));
					if (CursorUtils.isValidCursor(cursorContract)
							&& cursorContract.moveToFirst()) {
						mGeometryCheck = cursorContract.getInt(0);
						mGeometryCorrection = cursorContract.getInt(1);
					}
					CursorUtils.closeCursor(cursorContract);
					mSpinnerObjList = new ArrayList<SpinnerObject>();
					String lblNone = dbHelper.getLabel(Constants.LABEL_NONE);
					String lblGeometryCheck = dbHelper
							.getLabel(Constants.LABEL_GEOMETRY_CHECK);
					String lblGeoCheckNCorrection = dbHelper
							.getLabel(Constants.LABEL_GEOMETRY_CHECK_N_CORRECTION);
					if ((mGeometryCheck == 0) && (mGeometryCorrection == 0)) {
						mAxleTypeId = 0;
						mAxleTypeValue = lblNone;

						mSpinnerObject = new SpinnerObject(mAxleTypeId,
								mAxleTypeValue);
						mSpinnerObjList.add(mSpinnerObject);
					} else if ((mGeometryCheck == 1)
                                && (mGeometryCorrection == 0)) {
                            mAxleTypeId = 0;
                            mAxleTypeValue = lblNone;

                            mSpinnerObject = new SpinnerObject(mAxleTypeId,
                                    mAxleTypeValue);
                            mSpinnerObjList.add(mSpinnerObject);

                            mAxleTypeId = 1;
                            mAxleTypeValue = lblGeometryCheck;

                            mSpinnerObject = new SpinnerObject(mAxleTypeId,
                                    mAxleTypeValue);
                            mSpinnerObjList.add(mSpinnerObject);

					} else if ((mGeometryCheck == 0)
                                    && (mGeometryCorrection == 1)) {
                                mAxleTypeId = 0;
                                mAxleTypeValue = lblNone;

                                mSpinnerObject = new SpinnerObject(mAxleTypeId,
                                        mAxleTypeValue);
                                mSpinnerObjList.add(mSpinnerObject);

                                mAxleTypeId = 2;
                                mAxleTypeValue = lblGeoCheckNCorrection;

                                mSpinnerObject = new SpinnerObject(mAxleTypeId,
                                        mAxleTypeValue);
                                mSpinnerObjList.add(mSpinnerObject);
                            } else if ((mGeometryCheck == 1)
                                    && (mGeometryCorrection == 1)) {
                                mAxleTypeId = 0;
                                mAxleTypeValue = lblNone;

                                mSpinnerObject = new SpinnerObject(mAxleTypeId,
                                        mAxleTypeValue);

                                mSpinnerObjList.add(mSpinnerObject);

                                mAxleTypeId = 1;
                                mAxleTypeValue = lblGeometryCheck;

                                mSpinnerObject = new SpinnerObject(mAxleTypeId,
                                        mAxleTypeValue);
                                mSpinnerObjList.add(mSpinnerObject);

                                mAxleTypeId = 2;
                                mAxleTypeValue = lblGeoCheckNCorrection;

                                mSpinnerObject = new SpinnerObject(mAxleTypeId,
                                        mAxleTypeValue);
                                mSpinnerObjList.add(mSpinnerObject);
                    }

					ArrayAdapter<SpinnerObject> dataAdapter = new ArrayAdapter<SpinnerObject>(
							getActivity(),
							android.R.layout.simple_spinner_item,
							mSpinnerObjList);

					if (mSpinnerValues == null) {
						mSpinnerValues = new int[axleinfo.length];
						for (int i = 0; i < axleinfo.length; i++) {
							mSpinnerValues[i] = 0;
						}
					}
					mSpinnerViews.clear();
					for (int i = 0; i < axleinfo.length; i++) {
						mAxleType = axleinfo[i].trim();
						mAxleType = getAxleType(mAxleType);
						mAxleType = (i + 1) + ". " + mAxleType;

						mAxleViewLL = inflater.inflate(
								R.layout.axle_services_row, null);
						mLblAxleType = (TextView) mAxleViewLL
								.findViewById(R.id.lbl_axleType);
						mLblAxleType.setText(mAxleType);
						// mLblAxleType.setGravity(Gravity.CENTER_VERTICAL);
						mLblAxleType.setPadding(0, 0, 0, 5);

						spAxleTypeValue = (Spinner) mAxleViewLL
								.findViewById(R.id.value_axleType);
						spAxleTypeValue.setId(i);
						spAxleTypeValue.setAdapter(dataAdapter);
						mSpinnerViews.add(spAxleTypeValue);
						mServiceContainer.addView(mAxleViewLL);
						spAxleTypeValue
								.setOnItemSelectedListener(new OnItemSelectedListener() {
									@Override
									public void onItemSelected(
											AdapterView<?> parent, View view,
											int pos, long id) {
										spAxleTypeValue
												.setOnItemSelectedListener(axleSpinnerListener);
										if (null != spAxleTypeValue
												&& !spAxleTypeValue
														.getSelectedItem()
														.toString()
														.equalsIgnoreCase(
																"None")) {
											int saveIconState = saveIconHandlerActivity.getSaveIconState();
//									    	if(saveIconState == EjobSaveIconStates.ICON_RED){
									    	enableSaveButton();
//									    	}else if(saveIconState == EjobSaveIconStates.ICON_BLACK){
//									    		finalSaveButton();
//									    	}
										} 
									}

									@Override
									public void onNothingSelected(
											AdapterView<?> parent) {

									}
								});
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(cursorContract);
			if (dbHelper != null) {
				dbHelper.close();
			}
		}
	}

	private OnItemSelectedListener axleSpinnerListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			if (null != spAxleTypeValue
					&& spAxleTypeValue.getSelectedItemPosition() == 0) {
				// Do Nothing
			} else {
				int saveIconState = saveIconHandlerActivity.getSaveIconState();
//		    	if(saveIconState == EjobSaveIconStates.ICON_RED){
		    	enableSaveButton();
//		    	}else if(saveIconState == EjobSaveIconStates.ICON_BLACK){
//		    		finalSaveButton();
//		    	}
			}

			saveAxleServices();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	};

	/**
	 * Retaining The Axle Services On Page Swipe
	 */
	private void retainAxleServices() {
		if (VehicleSkeletonFragment.mJobItemList != null) {
			if (VehicleSkeletonFragment.mJobItemList.size() > 0) {
				for (JobItem job : VehicleSkeletonFragment.mJobItemList) {
					if (job.getActionType().equals("7")) {
						try {
							mSpinnerViews.get(
									Integer.parseInt(job.getAxleNumber()) - 1)
									.setSelection(
											Integer.parseInt(job
													.getAxleServiceType()),
											true);
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

	}

	/**
	 * Loading The Labels From Translation Table
	 * 
	 * @param rootView
	 */
	private void loadLabelsFromDB() {
		String labelFromDB = "";
		DatabaseAdapter dbHelper = new DatabaseAdapter(getActivity());
		dbHelper.createDatabase();
		dbHelper.open();

		try {
			labelFromDB = dbHelper.getLabel(Constants.ACCOUNT_LABEL);
			mLabelAccount.setText(labelFromDB);
			labelFromDB = dbHelper.getLabel(Constants.VEH_ID_LABEL);
			mLabelVehId.setText(labelFromDB);
			mLabelRefNumber.setText("#");
			if (!Constants.COMESFROMUPDATE && !Constants.COMESFROMVIEW) {
				mValueAccount.setText(Constants.ACCOUNTNAME);
				mValueVehId.setText(Constants.VEHID);
				mValueRefNumber.setText(Constants.JOBREFNUMBER);
				mValueFleetNumber.setText(Constants.FLEETNUM);
			} else if (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW) {
				mValueRefNumber.setText(JobBundle.getmRefNO().toString());
				mValueVehId.setText(JobBundle.getmLPlate().toString());
				mValueAccount.setText(JobBundle.getmAccountName());
				mValueFleetNumber.setText(JobBundle.getmFleetNO());
			}

			labelFromDB = dbHelper.getLabel(Constants.AXLE_SERVICES_TITLE);
			mLblAxleServicesTitle.setText(labelFromDB);

			labelFromDB = dbHelper.getLabel(Constants.WORK_ON_AXLE_LABEL);
			mLblAxleSpecification.setText(labelFromDB);

			labelFromDB = dbHelper.getLabel(Constants.DRIVE).trim();
			sDRIVE = labelFromDB;

			labelFromDB = dbHelper.getLabel(Constants.PASSIVE).trim();
			sPASSIVE = labelFromDB;

			labelFromDB = dbHelper.getLabel(Constants.LIFT).trim();
			sLIFT = labelFromDB;

			labelFromDB = dbHelper.getLabel(Constants.STEERS).trim();
			sSTEERING = labelFromDB;
			labelFromDB = dbHelper.getLabel(Constants.FLEET_NO);
			mLabelFleetNumber.setText(labelFromDB);
			labelFromDB = dbHelper.getLabel(Constants.STEERnDRIVE).trim();
			sSTEERnDRIVE = labelFromDB;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbHelper != null) {
				dbHelper.close();
			}
		}

	}

	/**
	 * Initializing The UI Labels
	 * 
	 * @param view
	 */
	private void initalize() {

		// header
		mLabelAccount = (TextView) mRootView.findViewById(R.id.lbl_account);
		mValueAccount = (TextView) mRootView.findViewById(R.id.value_account);

		mLabelVehId = (TextView) mRootView.findViewById(R.id.lbl_vehId);
		mValueVehId = (TextView) mRootView.findViewById(R.id.value_vehId);

		mLabelFleetNumber = (TextView) mRootView.findViewById(R.id.lbl_fleetNo);
		mValueFleetNumber = (TextView) mRootView
				.findViewById(R.id.value_fleetNo);

		mLabelRefNumber = (TextView) mRootView.findViewById(R.id.lbl_jobRefNo);
		mValueRefNumber = (TextView) mRootView
				.findViewById(R.id.value_jobRefNo);

		// form
		mLblAxleServicesTitle = (TextView) mRootView
				.findViewById(R.id.lbl_axle_services_title);
		mLblAxleSpecification = (TextView) mRootView
				.findViewById(R.id.lbl_axleSpecification);
		mServiceContainer = (LinearLayout) mRootView
				.findViewById(R.id.serviceContainer);

		// job icon on header
		mJobTypeIcon = (ImageView) mRootView.findViewById(R.id.jobIcon);
		if (!Constants.JOBTYPEREGULAR) {
			mJobTypeIcon.setImageResource(R.drawable.breakdown_job);
		}
		mSaveJob = (Button) mRootView.findViewById(R.id.btn_save);
		mSaveJob.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				saveAxleServices();
				/**
				 * Condition added to disable save button once clicked during Save Job in DB
				 */
				if (saveIconHandlerActivity.getSaveIconState() == EjobSaveIconStates.ICON_BLACK) {
					disableSaveButton();
					EjobFormActionActivity.saveJobInfo(v, true);
				} else if (saveIconHandlerActivity.getSaveIconState() == EjobSaveIconStates.ICON_RED) {
					disableSaveButton();
					EjobFormActionActivity.saveJobInfo(v, false);
					finalSaveButton();
				}
			
			}
		});
	}

	/**
	 * The method will save the axle services details.
	 */
	public void saveAxleServices() {
		if (mServiceContainer != null) {
			try {
				if (VehicleSkeletonFragment.mJobItemList != null) {
					int noOfLL = mServiceContainer.getChildCount();
					for (int i = 0; i < noOfLL; i++) {
						mAxleViewLL = mServiceContainer.getChildAt(i);
						if (mSpinnerViews != null && mSpinnerViews.size() > i) {
							mSpinnerObject = (SpinnerObject) mSpinnerViews.get(
									i).getSelectedItem();
							mAxleTypeId = mSpinnerObject.getId();
							mAxleNumber = i + 1;
							System.out.println("Axle Number : " + mAxleNumber);
							System.out.println("AxleTypeId : " + mAxleTypeId);

							if (!checkAxleJOb(mAxleNumber)) {
								// Saving In The Axle Object
								if (mSpinnerViews.get(i)
										.getSelectedItemPosition() != 0) {
									mJobItem = new JobItem();
									mJobItem.setActionType(String
											.valueOf(mActionType));// Axle
																	// Services
																	// Action
																	// Type 7
									mJobItem.setAxleNumber(String
											.valueOf(mAxleNumber));// Axle
																	// Services
																	// Action
																	// Type
									mJobItem.setAxleServiceType(String
											.valueOf(mAxleTypeId));// Axle
																	// Services
																	// Action
																	// Type for
																	// dismount
																	// tire
									mJobItem.setDamageId("");
									mJobItem.setExternalId(String.valueOf(UUID
											.randomUUID()));//
									mJobItem.setGrooveNumber(null);
									mJobItem.setJobId("");
									mJobItem.setMinNSK("0");
									mJobItem.setNote(null);
									mJobItem.setNskOneAfter("0");
									mJobItem.setNskOneBefore("0");
									mJobItem.setNskThreeAfter("0");
									mJobItem.setNskThreeBefore("0");
									mJobItem.setNskTwoAfter("0");
									mJobItem.setNskTwoBefore("0");
									mJobItem.setOperationID("");
									mJobItem.setPressure("0");
									// recommended pressure for axle
									mJobItem.setRecInflactionDestination("0");
									// pressure from JSON
									mJobItem.setRecInflactionOrignal("0");
									mJobItem.setRegrooved("false"); // true
																	// false
									mJobItem.setReGrooveNumber(null);// alway
																		// null
									mJobItem.setRegrooveType("0");
									mJobItem.setRemovalReasonId("");
									mJobItem.setRepairCompany(null);
									mJobItem.setRimType("0"); // for tor
									mJobItem.setSequence(String
											.valueOf(VehicleSkeletonFragment.mJobItemList
													.size() + 1));
									mJobItem.setServiceCount("0");// addtinal
																	// servcies
									mJobItem.setServiceID(String
											.valueOf(mServiceId));// Axle
																	// Services
									mJobItem.setSwapType("0");// swap
									mJobItem.setThreaddepth("0");// tire mgmt
									mJobItem.setTorqueSettings("0"); // update
																		// from
																		// retorque
									mJobItem.setWorkOrderNumber(null);
									mJobItem.setCasingRouteCode(""); //balnk went in windows db
									/**
									 * Bug 577
									 */
									mTyreForAxleService = getTyreSAPCodeTI();
									mJobItem.setSapCodeTilD(mTyreForAxleService.getSapCodeTI());
									mJobItem.setTyreID(mTyreForAxleService.getExternalID());
									mTyreForAxleService = null;
									VehicleSkeletonFragment.mJobItemList
											.add(mJobItem);
								}
							} else {
								if (Constants.COMESFROMUPDATE) {
									updateAxleJOb(mAxleNumber);
								} else {
									if (mSpinnerViews.get(i)
											.getSelectedItemPosition() != 0) {
										updateAxleJOb(mAxleNumber);
									} else {
										removeAxleJOb(mAxleNumber);
									}
								}

							}
						}
					}
					Constants.JOB_ITEMLIST = VehicleSkeletonFragment.mJobItemList;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * The method will check for job existence in Job item list
	 * 
	 * @param axleNumber
	 * @return boolean
	 */
	public boolean checkAxleJOb(int axleNumber) {
		boolean isAxleJob = false;
		if (VehicleSkeletonFragment.mJobItemList != null) {
			if (VehicleSkeletonFragment.mJobItemList.size() > 0) {
				for (int i = 0; i < VehicleSkeletonFragment.mJobItemList.size(); i++) {
					try {
						if (7 == Integer
								.parseInt(VehicleSkeletonFragment.mJobItemList
										.get(i).getActionType())) {
							if (axleNumber == Integer
									.parseInt(VehicleSkeletonFragment.mJobItemList
											.get(i).getAxleNumber())) {
								isAxleJob = true;
							}
						}
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
					continue;
				}
			}
		}

		return isAxleJob;
	}

	/**
	 * The method will use to update the existing jobs
	 * 
	 * @param axleNumber
	 * 
	 */
	public void updateAxleJOb(int axleNumber) {
		if (VehicleSkeletonFragment.mJobItemList != null) {
			if (VehicleSkeletonFragment.mJobItemList.size() > 0) {
				for (int i = 0; i < VehicleSkeletonFragment.mJobItemList.size(); i++) {
					try {
						if (7 == Integer
								.parseInt(VehicleSkeletonFragment.mJobItemList
										.get(i).getActionType())) {
							if (axleNumber == Integer
									.parseInt(VehicleSkeletonFragment.mJobItemList
											.get(i).getAxleNumber())) {
								VehicleSkeletonFragment.mJobItemList.get(i)
										.setAxleServiceType(
												String.valueOf(mAxleTypeId));
							}
						}
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
					continue;
				}
			}
		}
	}

	/**
	 * The method will use to remove the existing jobs
	 * 
	 * @param axleNumber
	 * 
	 */
	public void removeAxleJOb(int axleNumber) {
		if (VehicleSkeletonFragment.mJobItemList != null) {
			if (VehicleSkeletonFragment.mJobItemList.size() > 0) {
				for (int i = 0; i < VehicleSkeletonFragment.mJobItemList.size(); i++) {
					try {
						if (7 == Integer
								.parseInt(VehicleSkeletonFragment.mJobItemList
										.get(i).getActionType())) {
							if (axleNumber == Integer
									.parseInt(VehicleSkeletonFragment.mJobItemList
											.get(i).getAxleNumber())) {
								VehicleSkeletonFragment.mJobItemList.remove(i);
							}
						}
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
					continue;
				}
			}
		}

	}

	/**
	 * This method will return the axleType based on their short name
	 * 
	 * @param axleType
	 * @return AxleType
	 */
	public String getAxleType(String axleType) {
		String axleTypeValue = "";
		try {
			if (axleType.equals("ST")) {
				axleTypeValue = sSTEERING;
			} else if (axleType.equals("DR")) {
				axleTypeValue = sDRIVE;
			} else if (axleType.equals("LI")) {
				axleTypeValue = sLIFT;
			} else if (axleType.equals("DU")) {
				axleTypeValue = sPASSIVE;
			} else if (axleType.equals("SD")) {
				axleTypeValue = sSTEERnDRIVE;
			} else if (axleType.equals("DR|DU")) {
				axleTypeValue = sDRIVE;
			} else if (axleType.equals("DR|ST")) {
				axleTypeValue = sSTEERnDRIVE;
				System.out.println("Axle Type(DR|ST) : " + axleTypeValue);
			} else if (axleType.equals("ST|DU")) {
				axleTypeValue = sSTEERING;
			} else if (axleType.equals("")) {
				axleTypeValue = sPASSIVE;
			} else {
				axleTypeValue = sPASSIVE;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return axleTypeValue;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		if (!mSaveButtonClicked) {
			saveAxleServices();
		}
		super.onSaveInstanceState(outState);
	}

	protected void hideKeyboard(Context context) {
		try {
			InputMethodManager in = (InputMethodManager) context
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			in.hideSoftInputFromWindow(getView().getWindowToken(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#
	 * onActivityBackPress()
	 */
	@Override
	public boolean onActivityBackPress() {
		return false;
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#hideKeyBoard
	 * (boolean)
	 */
	@Override
	public void hideKeyBoard(boolean toHide) {
		if (toHide) {
			hideKeyboard(this.getActivity());
		}
	}
	/**
	 * Method enabling the save icon as per the user selection on the UI
	 * It will always turn the save icon color into red if there will be data to be saved 
	 * for the local database file
	 */
	public void enableSaveButton() {
		if (mSaveJob != null) {
			mSaveJob.setEnabled(true);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_RED);
			mSaveJob.setBackgroundResource(R.drawable.continue_savejob_navigation);
		}
	}
	/**
	 * Method disabling the save icon as per the user selection on the UI
	 * It will always turn the save icon color into grey if there will not be any 
	 * data to be saved for the local database file
	 */
	public void disableSaveButton() {
		if (mSaveJob != null) {
			mSaveJob.setEnabled(false);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_GRAY);
			mSaveJob.setBackgroundResource(R.drawable.unsavejob_navigation);
		}
	}
	/**
	 * This method will always turn the save icon color into black 
	 * if there will not be any data to be saved for the local database file
	 * It will navigate user back to the eJoblist
	 */
	public void finalSaveButton() {
		if (mSaveJob != null) {
			mSaveJob.setEnabled(true);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_BLACK);
			mSaveJob.setBackgroundResource(R.drawable.savejob_navigation);
		}
	}

	/**
	 * This method is called from view pager listener when this fragment is
	 * selected. It updates the save icon based on the status of save button in
	 * last fragment.
	 */
	private void updateSaveButtonOnUIBasedOnLastFragment() {
		// TODO Auto-generated method stub
		if (saveIconHandlerActivity == null)
			return;
		int saveIconState = saveIconHandlerActivity.getSaveIconState();
		switch (saveIconState) {
		case EjobSaveIconStates.ICON_GRAY:
			disableSaveButton();
			break;
		case EjobSaveIconStates.ICON_BLACK:
			finalSaveButton();
			break;
		case EjobSaveIconStates.ICON_RED:
			enableSaveButton();
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.IEjobFragmentVisible#fragmentIsVisibleToUser
	 * ()
	 */
	@Override
	public void fragmentIsVisibleToUser() {
		updateSaveButtonOnUIBasedOnLastFragment();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onAttach(android.app.Activity)
	 */
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		saveIconHandlerActivity = (ISaveIconEjobFragment) activity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDetach()
	 */
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		saveIconHandlerActivity = null;
	}

	/**
	 * CR 577
	 *
	 * @return
	 */
	private Tyre getTyreSAPCodeTI() {
		Tyre axleTyre = new Tyre();
		if (null == VehicleSkeletonFragment.tyreInfo){
			VehicleSkeletonFragment.tyreInfo = Constants.TIRE_INFO;
		}
		for (int i = 0; i < VehicleSkeletonFragment.tyreInfo.size(); i++) {
			if (null != VehicleSkeletonFragment.tyreInfo.get(i).getSapCodeTI()
					&& null != VehicleSkeletonFragment.tyreInfo.get(i)
					.getExternalID()) {
				axleTyre.setSapCodeTI(VehicleSkeletonFragment.tyreInfo.get(i).getSapCodeTI());
				axleTyre.setExternalID(VehicleSkeletonFragment.tyreInfo.get(i)
						.getExternalID());
				return axleTyre;
			}
		}
		return axleTyre;
	}

}
