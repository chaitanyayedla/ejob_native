package com.goodyear.ejob.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.EjobList;
import com.goodyear.ejob.R;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.DBModel;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.interfaces.IEjobFragmentVisible;
import com.goodyear.ejob.interfaces.IFragmentCommunicate;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.job.operation.helpers.Retorque;
import com.goodyear.ejob.job.operation.helpers.SaveJob;
import com.goodyear.ejob.ui.SafeDatePickerDialog;
import com.goodyear.ejob.ui.SafeTimePickerDialog;
import com.goodyear.ejob.util.CommonInformationalDialog;
import com.goodyear.ejob.util.CommonInformationalDialog.MyOnClickListener;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.InspectionState;
import com.goodyear.ejob.util.Job;
import com.goodyear.ejob.util.JobBundle;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.ReservePWTUtils;
import com.goodyear.ejob.util.SpinnerObject;
import com.goodyear.ejob.util.SpinnerObject1;
import com.goodyear.ejob.util.TableColumns.JobTableColumn;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Good Year
 */
public class EjobSignatureBoxFragment extends Fragment implements
        IFragmentCommunicate, IEjobFragmentVisible {

    public static final String TAG = EjobSignatureBoxFragment.class
            .getSimpleName();
    public static final String BUNDLE_SPLITTED_TIME = "bundleSplittedTime";
    public static final String BUNDLE_SPLITTED_DATE = "bundleSplittedDate";
    public static String MSG_SIGN_SIGNATURE_FORM = "";
    public static String MSG_SEL_REASON_NO_ODO = "";
    public static String MSG_SIGNATURE_FORM_SAVED = "";
    public static String MSG_FINSIH_TIME_AFTER_START_TIME = "";
    public static String MSG_JOB_IS_EMPTY = "";
    public static final int SIGNATURE_COLOR_FLAG_W = 1; // white
    public static final int SIGNATURE_COLOR_FLAG_B = 0; // black

    //CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
    //Message for Wrong Odometer Value
    public static String MSG_WRONG_ODO_VALUE = "Wrong OdoMeter Value";

    public DialogFragment mNewFragmentDate;
    public DialogFragment mNewFragmentTime;
    public JobItem mJobItem;
    public List<SpinnerObject> mSpinnerObjList;
    public List<SpinnerObject1> mSpinnerObjList1;
    public SpinnerObject mSpinnerObject;
    public SpinnerObject1 mSpinnerObject1;
    public int mActionType = 0;
    public int mSapCode = 0;
    public String mService = "";
    public int mServiceCount = 0;
    public int mMissingOdometerReasonID = 0;// sign
    public int mVehicleUnattendedStatus = 0;// sign
    public int mThirtyMinutesRetorqueStatus = 0;// sign
    public String mTimeFinished = "";// sign
    public int mAgreeStatus = 0;// sign
    public String mDriverName = "";// sign
    public String mVehicleODO = "";// sign
    public int mOdometerReasonId = 0;
    public String mOdometerReasonValue = "";
    public String mOdometerCode = "";
    // default signature reason id value replicated from windows device
    // internal bug fixed: rotation issue
    public static final String DEFAULT_SIGNATURE_ID_VAL = "00000000-0000-0000-0000-000000000000";
    public String mSignatureReasonId = DEFAULT_SIGNATURE_ID_VAL;
    public String mSignatureReasonValue = "";
    public int mDays = 0;
    // public boolean mIsSignatureDone = false;
    // Signature Params(Starts)
    public Context mContext;
    public SignatureWidget mSignature;
    public int mSignatureColor = 0;
    public String mTempDir;
    public int mCount = 1;
    public String mCurrent = null;
    private Bitmap mBitmap;
    public File mypath;
    public byte[] mSendImage;
    // Time Dialog (Starts)
    ReceivedOnTimePicker mNewTimePicker;
    public String mDatePickVal;
    public String mTimePickVal;
    public long mJobStartTime;
    public long mJobFinishedTime;
    public long mJobValidityDate;
    public String mJobID;
    private DBModel mDb;
    private ArrayAdapter<SpinnerObject> dataAdapter;
    private Bundle mInstanceState;
    public int mCountScreenNotVisibleToUser;
    private int mRetrievedAgreeStatus;
    private int mRetrvdThrtyMinRetorqStatus;
    private int mRetrvdVehUnatndStatus;
    private String mRetrievedSignatureReasonId = "";
    private String mRetrievedOdometerCode = "";
    protected int mSpinnerNoOdoMeterSelectedPos;
    protected int mSpinnerNoSignaturePos;
    public int mCountSpinnerNoSignature;
    public int mCountSpinnerNoOdoMeterReason;
    public boolean mIsDialogVisible;
    private String mDialogSetTime;
    private String mDialogSetDate;
    public int mScreenLoaded;

    /**
     * UI Views
     */
    public View mView;
    public LinearLayout mContent;
    public TextView mLblSignatureBoxTitle;
    public TextView mLabelAccount;
    public TextView mValueAccount;
    public TextView mLabelVehId;
    public TextView mValueVehId;
    public TextView mLabelFleetNumber;
    public TextView mValueFleetNumber;
    public TextView mLabelRefNumber;
    public TextView mValueRefNumber;
    public TextView mLblAgreeStatus;
    public TextView mLblRretorqueCompleted;
    public TextView mLblVehUnattended;
    public TextView mLblPrimaryODO;
    public EditText mValuePrimaryODO;
    public TextView mLblDriversName;
    public EditText mValueDriversName;
    public TextView mInput;
    public TextView mInputTime;
    public static Spinner sSpinnerNoOdoMeterReason;
    public static Spinner sSpinnerNoSignature;
    public Switch mSwitchBtnAgreeStatus;
    public Switch mSwitchBtnRetorqueCompltd;
    public Switch mSwitchBtnVehUnuttended;
    public Button mSaveButton;
    public Button mCloseButton;
    public ImageView mJobTypeIcon;
    public LinearLayout mLL_retorqueCompleted;
    public AlertDialog mAlert;
    public CommonInformationalDialog mInfoDialogFrag;
    boolean mIsInfoDialogVisible;
    private String mLabelYes;
    private String mLabelNo;
    private String mOkLabel;
    private String mCancelLabel;
    private String mDateDialogTitle;
    private AlertDialog mFinishDateTimeDialog;
    private String mFinishTimeDialogTitle;
    private int mRetainDateTimeDialog;

    /**
     * Final variable for NOTES_INTENT
     */
    public static final int NOTES_INTENT = 105;

    //User Trace logs trace tag
    private static final String TRACE_TAG = "Signature";

    private String Last_Recorded_Odometer_Reading;

    //Numeric Input Filter for PrimaryOdometer value
    private static InputFilter numericFilter = new InputFilter() {
        Pattern mPattern;

        @Override
        public CharSequence filter(CharSequence changedText, int arg1,
                                   int arg2, Spanned oldText, int arg4, int arg5) {
            mPattern = Pattern.compile("^[\\p{N}]+$|^$");
            Matcher matcher = mPattern
                    .matcher(oldText.toString() + changedText);
            if (!matcher.matches()) {
                return "";
            }
            return null;
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mScreenLoaded = 0;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.signature_box, container,
                false);
        Last_Recorded_Odometer_Reading= "Last Recorded Odometer Reading";
        mInstanceState = savedInstanceState;
        mNewFragmentDate = new ReceivedDatePicker();
        mNewFragmentTime = new ReceivedOnTimePicker();
        mCountSpinnerNoSignature = 0;
        mCountSpinnerNoOdoMeterReason = 0;
        mDialogSetDate = "";
        mDialogSetTime = "";
        if (mInstanceState == null
                && (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW)) {
            loadJobDatafromDb();
        }
        initalize(rootView);
        loadLabelsFromDB(rootView);

        if (mIsInfoDialogVisible) {
            showJobEmptyWarningDialog();
        }
        if (mIsDialogVisible) {
            showDateAlert();
        } else if (savedInstanceState != null
                && savedInstanceState.getBoolean("isFinishDateTimeDialog")) {
            showFinishDateTimeDialog();
        }
        if (savedInstanceState != null) {
            if (savedInstanceState.getInt("mRetainDateTimeDialog") == 1) {
                showFinishDateTimeDialog();
            }
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mInstanceState != null) {
            if (mOdometerReasonId != 0) {
                enableSaveButton();
            } else if (!mSignatureReasonId.equals(DEFAULT_SIGNATURE_ID_VAL)) {
                enableSaveButton();
            } else if (Job.getSignatureImage() != null) {
                enableSaveButton();
            }
        }
        if (Constants.COMESFROMVIEW) {
            disableViews();
        }
    }

    /**
     * Disable views on view mode
     */
    private void disableViews() {
        mSaveButton.setVisibility(View.GONE);
        sSpinnerNoOdoMeterReason.setEnabled(false);
        sSpinnerNoSignature.setEnabled(false);
        mSignature.setEnabled(false);
        mSignature.setOnTouchListener(null);
        mCloseButton.setOnClickListener(null);
    }

    /**
     * Load Job data from database
     */
    private void loadJobDatafromDb() {
        DatabaseAdapter test = new DatabaseAdapter(mContext);
        test.open();
        Cursor cursorJobTable = test.getAllValuesFromJobTable();
        if (CursorUtils.isValidCursor(cursorJobTable)) {
            if (cursorJobTable.moveToFirst()) {
                byte[] cameraImageView = cursorJobTable.getBlob(cursorJobTable
                        .getColumnIndex(JobTableColumn.COL_SIGNATURE_IMAGE));
                if (cameraImageView != null && !cameraImageView.equals("")) {
                    mBitmap = getPhoto(cameraImageView);
                }
                mRetrievedAgreeStatus = cursorJobTable.getInt(cursorJobTable
                        .getColumnIndex(JobTableColumn.COL_AGREE_STATUS));
                mRetrvdThrtyMinRetorqStatus = cursorJobTable
                        .getInt(cursorJobTable
                                .getColumnIndex(JobTableColumn.COL_THIRTY_MINUTES_ROTQUE_STATUS));
                mRetrvdVehUnatndStatus = cursorJobTable
                        .getInt(cursorJobTable
                                .getColumnIndex(JobTableColumn.COL_VEHICLE_UNATTENEDED_STATUS));
                mRetrievedSignatureReasonId = cursorJobTable
                        .getString(cursorJobTable
                                .getColumnIndex(JobTableColumn.COL_SIGNATURE_REASON_ID));
                mRetrievedOdometerCode = cursorJobTable
                        .getString(cursorJobTable
                                .getColumnIndex(JobTableColumn.COL_MISSING_ODOMETER_REASON_ID));
            }
            CursorUtils.closeCursor(cursorJobTable);
        }
        test.close();
    }

    /**
     * This method converts byte array into bitmap
     *
     * @param image in byte array
     * @return bitmap data
     */
    public Bitmap getPhoto(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    private MyOnClickListener myOnClickListener = new MyOnClickListener() {
        @Override
        public void onOkButtonClick() {
            mIsInfoDialogVisible = false;
            cleanConfigurationBeforeClose();
            Intent startEjobList = new Intent(mContext, EjobList.class);
            startEjobList.putExtra("sentfrom", "ejobform");
            startActivity(startEjobList);
        }

        @Override
        public void onCancelButtonClick() {
            mIsInfoDialogVisible = false;
        }
    };

    /**
     * This method will show pop-up dialog which indicates job is empty and no
     * actin performed
     */
    private void showJobEmptyWarningDialog() {
        LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Job Empty Warning", false, true, false);
        mInfoDialogFrag = CommonInformationalDialog
                .newInstance(MSG_JOB_IS_EMPTY);
        mInfoDialogFrag.setOkListener(myOnClickListener);
        mInfoDialogFrag.show(getFragmentManager(), "fragment_alert");
        mIsInfoDialogVisible = true;
    }

    /**
     * This method intializes the view elements and set necessary data to them
     */
    @SuppressWarnings("deprecation")
    private void initalize(View view) {
        mLblSignatureBoxTitle = (TextView) view
                .findViewById(R.id.signature_box_title);
        // header
        mLabelAccount = (TextView) view.findViewById(R.id.lbl_account);
        mValueAccount = (TextView) view.findViewById(R.id.value_account);
        mLabelVehId = (TextView) view.findViewById(R.id.lbl_vehId);
        mValueVehId = (TextView) view.findViewById(R.id.value_vehId);
        mLabelFleetNumber = (TextView) view.findViewById(R.id.lbl_fleetNo);
        mValueFleetNumber = (TextView) view.findViewById(R.id.value_fleetNo);
        mLabelRefNumber = (TextView) view.findViewById(R.id.lbl_jobRefNo);
        mValueRefNumber = (TextView) view.findViewById(R.id.value_jobRefNo);
        // job icon on header
        mJobTypeIcon = (ImageView) view.findViewById(R.id.jobIcon);
        if (!Constants.JOBTYPEREGULAR) {
            mJobTypeIcon.setImageResource(R.drawable.breakdown_job);
        }
        // Form
        mLblAgreeStatus = (TextView) view.findViewById(R.id.lbl_agreeStatus);
        mLblRretorqueCompleted = (TextView) view
                .findViewById(R.id.lbl_retorqueCompleted);
        mLblVehUnattended = (TextView) view
                .findViewById(R.id.lbl_vehUnuttended);
        mLblPrimaryODO = (TextView) view.findViewById(R.id.lbl_primaryODO);
        mValuePrimaryODO = (EditText) view.findViewById(R.id.value_primaryODO);
        // length of primary odo length should be less then 12
        /*Jira EJOB-97 : Fix for higher odometer value, length cannot be more than 9*/
        mValuePrimaryODO.setFilters(new InputFilter[] {
                new InputFilter.LengthFilter(9), numericFilter });
        if (Job.getVehicleODO() != null) {
            mValuePrimaryODO.setText(Job.getVehicleODO());
        }
        mValuePrimaryODO.addTextChangedListener(new PrimaryODOTextChanged());


        mLblDriversName = (TextView) view.findViewById(R.id.lbl_driversName);
        mValueDriversName = (EditText) view
                .findViewById(R.id.value_driversName);
        // CR 446
        mLL_retorqueCompleted = (LinearLayout) view
                .findViewById(R.id.ll_retorqueCompleted);
        if (Constants.TORQUE_ENABLE.equalsIgnoreCase("ON")) {
            mLL_retorqueCompleted.setVisibility(View.INVISIBLE);
        }
        mValueDriversName.setFilters(new InputFilter[]{
                new InputFilter.AllCaps(), new InputFilter.LengthFilter(25),
                CommonUtils.alphaFilter});
        if (Job.getDriverName() != null) {
            mValueDriversName.setText(Job.getDriverName());
        }
        if (Constants.COMESFROMVIEW) {
            mValuePrimaryODO.setEnabled(false);
            mValueDriversName.setEnabled(false);
        } else {
            mValuePrimaryODO.setEnabled(true);
            mValueDriversName.setEnabled(true);
        }
        mSaveButton = (Button) view.findViewById(R.id.btn_save);
        disableSaveButton();
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                LogUtil.TraceInfo(TRACE_TAG, "none", "Save - BT", false, false, false);
                generateSignature();
                retainJob();
                if (isFromSigned() == false) {
                    LogUtil.TraceInfo(TRACE_TAG, "Save", "Msg : "+MSG_SIGN_SIGNATURE_FORM, false, false, false);
                    CommonUtils.notifyLong(MSG_SIGN_SIGNATURE_FORM, mContext);
                } else if (Job.getVehicleODO().trim().equals("")
                        && sSpinnerNoOdoMeterReason.getSelectedItemPosition() < 1) {
                    LogUtil.TraceInfo(TRACE_TAG, "Save", "Msg : " + MSG_SEL_REASON_NO_ODO, false, false, false);
                    CommonUtils.notifyLong(MSG_SEL_REASON_NO_ODO, mContext);
                }

                //CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
                //Checking Last Recorded Odometer Value with User Entered Odometer Value
                else if (!Job.getVehicleODO().trim().equals("")
                        && sSpinnerNoOdoMeterReason.getSelectedItemPosition() < 1 &&
                        Long.parseLong(Job.getVehicleODO())<Constants.LastRecordedOdometerValue)
                {

                    String msg = MSG_WRONG_ODO_VALUE + "\n"+Last_Recorded_Odometer_Reading+" : " + Constants.LastRecordedOdometerValue;
                    LogUtil.TraceInfo(TRACE_TAG, "Save", "Msg : " + msg, false, false, false);
                    CommonUtils.notifyLong(msg, mContext);
                }

                else{

                    try{
                        String Trace_Data=mSwitchBtnAgreeStatus.isChecked() + " | "
                                + mSwitchBtnRetorqueCompltd.isChecked() + " | "
                                + mSwitchBtnVehUnuttended.isChecked() + " | "
                                + mValuePrimaryODO.getText().toString() + " | "
                                + sSpinnerNoOdoMeterReason.getSelectedItem() + " | "
                                + mValueDriversName.getText().toString();

                        LogUtil.TraceInfo(TRACE_TAG, "none", "Data : " + Trace_Data, false, false, false);
                    }catch (Exception e)
                    {
                        LogUtil.TraceInfo(TRACE_TAG, "Data", "Exception : "+e.getMessage(), false, true, false);
                    }
                    if (VehicleSkeletonFragment.mJobItemList != null
                            && VehicleSkeletonFragment.mJobItemList.size() > 0) {
                        // showDateAlert();
                        /*
                        *Condition Checking the Inspection Button Status along with the
                        *partially inspected status of the vehicle to complete a job
                        */
                        SharedPreferences preferences = mContext.getSharedPreferences(
                                Constants.GOODYEAR_CONF, 0);

                        if(preferences.getString(Constants.INSPECTION, "").equalsIgnoreCase("ON")) {
                            //Only for Operation with inspection (partial) needs Reason
                            if (partiallyInspectedWithOperation()==InspectionState.OPERATION_WITH_INSPECTION_PARTIAL) {
                                Constants.EjobSignatureBoxNotes="";
                                //Alert Dialog for Partially Inspected Jobs
                                partialInspectionAlertDialog();


                            } else {
                                showFinishDateTimeDialog();
                            }
                        }else{
                            showFinishDateTimeDialog();
                        }

                    } else {
                        /*
						 * Toast.makeText(mContext, MSG_JOB_IS_EMPTY,
						 * Toast.LENGTH_LONG).show();
						 */
                        showJobEmptyWarningDialog();
                    }
                }
            }

        });

        sSpinnerNoOdoMeterReason = (Spinner) view
                .findViewById(R.id.spinner_noODOmeterReasion);
        sSpinnerNoOdoMeterReason.setEnabled(false);
        sSpinnerNoOdoMeterReason
                .setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        mSpinnerObject = (SpinnerObject) sSpinnerNoOdoMeterReason
                                .getSelectedItem();
                        if (mCountSpinnerNoOdoMeterReason > 0) {
                            if (mSpinnerObject.getId() != 0) {
                                enableSaveButton();
                            } else {
                                if (mValuePrimaryODO != null
                                        && TextUtils.isEmpty(mValuePrimaryODO
                                        .getText().toString().trim())) {
                                    disableSaveButton();
                                }
                            }
                        }
                        mCountSpinnerNoOdoMeterReason++;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {

                    }
                });

        sSpinnerNoSignature = (Spinner) view
                .findViewById(R.id.spinner_noSignature);
        sSpinnerNoSignature.setEnabled(false);

        sSpinnerNoSignature
                .setOnItemSelectedListener(new OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1,
                                               int arg2, long arg3) {
                        sSpinnerNoSignature
                                .setOnItemSelectedListener(spinnerNoSignatureListener);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                    }
                });

        mSwitchBtnAgreeStatus = (Switch) view
                .findViewById(R.id.value_agreeStatusSwitchBtn);
        if (Constants.COMESFROMVIEW) {
            mSwitchBtnAgreeStatus.setEnabled(false);
        } else {
            mSwitchBtnAgreeStatus.setEnabled(true);
        }
        mSwitchBtnAgreeStatus
                .setOnCheckedChangeListener(mListenerSwchBtnAgreeStatus);
        mSwitchBtnRetorqueCompltd = (Switch) view
                .findViewById(R.id.value_retorqueCompletedSwitchBtn);
        if (Constants.COMESFROMVIEW) {
            mSwitchBtnRetorqueCompltd.setEnabled(false);
        } else {
            mSwitchBtnRetorqueCompltd.setEnabled(true);
        }
        mSwitchBtnRetorqueCompltd
                .setOnCheckedChangeListener(mListenerSwchBtnRetorque);

        mSwitchBtnVehUnuttended = (Switch) view
                .findViewById(R.id.value_vehUnuttended);
        if (Constants.COMESFROMVIEW) {
            mSwitchBtnVehUnuttended.setEnabled(false);
        } else {
            mSwitchBtnVehUnuttended.setEnabled(true);
        }
        mSwitchBtnVehUnuttended
                .setOnCheckedChangeListener(mListenerSwchBtnVehUnattend);

        // Initializing The Signature
        mContent = (LinearLayout) view.findViewById(R.id.ll_Signature);
        mSignature = new SignatureWidget(mContext, null);
        mSignature.sePaintColor(SIGNATURE_COLOR_FLAG_B);
        mSignature.setBackgroundColor(Color.WHITE);
        mSignature.setOnTouchListener(mSignatureTouchListener);
        resetDateAlertFlag();
        if (mInstanceState != null) {
            mBitmap = Constants.RETAIN_BITMAP;
            mAgreeStatus = mInstanceState.getInt("retainAgreeStatus");
            mThirtyMinutesRetorqueStatus = mInstanceState
                    .getInt("retainThirtyMinutesRetorqueStatus");
            mVehicleUnattendedStatus = mInstanceState
                    .getInt("retainVehicleUnattendedStatus");
            // internal bug fixed: rotation issue
            mSignatureReasonId = mInstanceState.getString(
                    "retainSignatureReasonId", "");
            mSignatureReasonValue = mInstanceState
                    .getString("retainSignatureReasonValue");
            mOdometerReasonId = mInstanceState.getInt("retainOdometerReasonId");
            mOdometerReasonValue = mInstanceState
                    .getString("retainOdometerReasonValue");
            mIsDialogVisible = mInstanceState.getBoolean("isDialogVisible");
            if (mIsDialogVisible) {
                mDialogSetDate = mInstanceState.getString("retainCurrentDate");
                mDialogSetTime = mInstanceState.getString("retainCurrentTime");
            }
            if (mAgreeStatus == 1) {
                mSwitchBtnAgreeStatus.setChecked(true);
            } else if (mAgreeStatus == 0) {
                mSwitchBtnAgreeStatus.setChecked(false);
            }
            if (mThirtyMinutesRetorqueStatus == 1) {
                mSwitchBtnRetorqueCompltd.setChecked(true);
            } else if (mThirtyMinutesRetorqueStatus == 0) {
                mSwitchBtnRetorqueCompltd.setChecked(false);
            }
            if (mVehicleUnattendedStatus == 1) {
                mSwitchBtnVehUnuttended.setChecked(true);
            } else if (mVehicleUnattendedStatus == 0) {
                mSwitchBtnVehUnuttended.setChecked(false);
            }
            mIsInfoDialogVisible = mInstanceState
                    .getBoolean("isInfoDialogVisible");
        }
        mContent.addView(mSignature, LayoutParams.FILL_PARENT,
                LayoutParams.FILL_PARENT);
        mView = mContent;
        Constants.RETAIN_BITMAP = null;
        // Signature Close Button(Starts)
        mCloseButton = (Button) view.findViewById(R.id.btn_close);
        mCloseButton.setOnClickListener(mCloseButtonClickListner);
        // Signature Close Button(Ends)

        // enable spinner if no primary odo
        if (mValuePrimaryODO.getText().toString().trim().equals("")) {
            sSpinnerNoOdoMeterReason.setEnabled(true);
        } else {
            sSpinnerNoOdoMeterReason.setSelection(0);
            sSpinnerNoOdoMeterReason.setEnabled(false);
        }
    }

    private OnItemSelectedListener spinnerNoSignatureListener = new OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {
            mSpinnerObject1 = (SpinnerObject1) sSpinnerNoSignature
                    .getSelectedItem();
			/*
			 * if (mCountSpinnerNoSignature > 0) {
			 * 
			 * } mCountSpinnerNoSignature++;
			 */
            if (TextUtils.isEmpty(mSpinnerObject1.getId())
                    || mSpinnerObject1.getId().equals(DEFAULT_SIGNATURE_ID_VAL)) {
                disableSaveButton();
                mSignatureReasonId = DEFAULT_SIGNATURE_ID_VAL;
            } else {
                enableSaveButton();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {

        }
    };

    private OnCheckedChangeListener mListenerSwchBtnVehUnattend = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {

            if (isChecked) {
                LogUtil.d(TAG, "Vehicle Unattended Switch Button is Checked.");
                mVehicleUnattendedStatus = 1;
                sSpinnerNoSignature.setEnabled(true);
                disableSaveButton();
                mSignature.clear();
                mSignature.sePaintColor(SIGNATURE_COLOR_FLAG_W);

                mSwitchBtnAgreeStatus.setChecked(false);

            } else {
                mVehicleUnattendedStatus = 0;
                mSignature.clear();
                sSpinnerNoSignature.setSelection(0);
                sSpinnerNoSignature.setEnabled(false);
                mSignatureReasonId = DEFAULT_SIGNATURE_ID_VAL;
                LogUtil.d(TAG, "Vehicle Unattended Switch Button is UnChecked.");
                mSignature.sePaintColor(SIGNATURE_COLOR_FLAG_B);
                disableSaveButton();
            }

        }
    };

    private OnCheckedChangeListener mListenerSwchBtnRetorque = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            mThirtyMinutesRetorqueStatus = isChecked ? 1 : 0;
        }
    };

    private OnCheckedChangeListener mListenerSwchBtnAgreeStatus = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            mAgreeStatus = isChecked ? 1 : 0;
            mSwitchBtnVehUnuttended.setChecked(isChecked ? false : true);
            if (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW
                    || mInstanceState != null) {
                if (mScreenLoaded == 0) {
                    mScreenLoaded = 1;
                } else {
                    mSignature.clear();
                }
            } else {
                if (mScreenLoaded == 0) {
                    mScreenLoaded = 1;
                } else {
                    mSignature.clear();
                }
            }

        }
    };

    private OnTouchListener mSignatureTouchListener = new OnTouchListener() {
        // Setting on Touch Listener for handling the touch inside
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            // Disallow touch request for parent scroll on touch of child view
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        }
    };

    private OnClickListener mCloseButtonClickListner = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            mSignature.clear();
            Constants.IS_SIGANTURE_DONE = false;
            Job.setSignatureImage(null);
            disableSaveButton();
        }
    };

    /**
     * Load Label data from database
     */
    private void loadLabelsFromDB(View rootView) {
        String labelFromDB = "";
        DatabaseAdapter dbHelper = new DatabaseAdapter(mContext);
        dbHelper.createDatabase();
        dbHelper.open();
        try {
            labelFromDB = dbHelper.getLabel(Constants.FLEET_NO);
            mLabelFleetNumber.setText(labelFromDB);
            mDateDialogTitle = dbHelper
                    .getLabel(Constants.LABEL_DATE_DIALOG_TITLE);
            mFinishTimeDialogTitle = dbHelper.getLabel(194);
            mCancelLabel = dbHelper.getLabel(Constants.CANCEL_LABEL);
            mOkLabel = dbHelper.getLabel(Constants.LABEL_OK);

            labelFromDB = dbHelper.getLabel(Constants.SIGNATURE_BOX_TITLE_LBL);
            mLblSignatureBoxTitle.setText(labelFromDB);
            // header
            labelFromDB = dbHelper.getLabel(Constants.ACCOUNT_LABEL);
            mLabelAccount.setText(labelFromDB);
            labelFromDB = dbHelper.getLabel(Constants.VEH_ID_LABEL);
            mLabelVehId.setText(labelFromDB);
            mLabelRefNumber.setText("#");
            if (!Constants.COMESFROMUPDATE && !Constants.COMESFROMVIEW) {
                mValueAccount.setText(Constants.ACCOUNTNAME);
                mValueVehId.setText(Constants.VEHID);
                mValueRefNumber.setText(Constants.JOBREFNUMBER);
                mValueFleetNumber.setText(Constants.FLEETNUM);
            } else if (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW) {
                mValueRefNumber.setText(JobBundle.getmRefNO().toString());
                mValueVehId.setText(JobBundle.getmLPlate().toString());
                mValueAccount.setText(JobBundle.getmAccountName());
                mValueFleetNumber.setText(JobBundle.getmFleetNO());
            }
            mLabelYes = dbHelper.getLabel(Constants.YES_LABEL);
            mLabelNo = dbHelper.getLabel(Constants.NO_LABEL);

            mSwitchBtnAgreeStatus.setTextOn(mLabelYes);
            mSwitchBtnAgreeStatus.setTextOff(mLabelNo);

            mSwitchBtnRetorqueCompltd.setTextOn(mLabelYes);
            mSwitchBtnRetorqueCompltd.setTextOff(mLabelNo);

            mSwitchBtnVehUnuttended.setTextOn(mLabelYes);
            mSwitchBtnVehUnuttended.setTextOff(mLabelNo);

            labelFromDB = dbHelper.getLabel(Constants.AGREE_STATUS_LABEL);
            mLblAgreeStatus.setText(labelFromDB);
            labelFromDB = dbHelper.getLabel(Constants.RETORQE_LABEL);
            mLblRretorqueCompleted.setText(labelFromDB);
            labelFromDB = dbHelper.getLabel(Constants.VEHICLE_UNUTTENDED_LABEL);
            mLblVehUnattended.setText(labelFromDB);
            labelFromDB = dbHelper.getLabel(Constants.PRIMARY_ODO_LABEL);
            mLblPrimaryODO.setText(labelFromDB);
            labelFromDB = dbHelper.getLabel(Constants.DRIVER_NAME_LABEL);
            mLblDriversName.setText(labelFromDB);
            MSG_SIGN_SIGNATURE_FORM = dbHelper
                    .getLabel(Constants.SIGN_SIGNATURE_FORM);
            MSG_SEL_REASON_NO_ODO = dbHelper
                    .getLabel(Constants.SEL_REASON_NO_ODO);
            MSG_SIGNATURE_FORM_SAVED = dbHelper
                    .getLabel(Constants.SIGNATURE_FORM_SAVED);
            MSG_FINSIH_TIME_AFTER_START_TIME = dbHelper
                    .getLabel(Constants.FINSIH_TIME_AFTER_START_TIME);

            MSG_JOB_IS_EMPTY = dbHelper.getLabel(Constants.JOB_IS_EMPTY);

            Last_Recorded_Odometer_Reading = dbHelper.getLabel(Constants.Label_Last_Recorded_Odometer_Reading);
            MSG_WRONG_ODO_VALUE = dbHelper.getLabel(Constants.Label_Wrong_OdoMeter_Value);

            //label for Inspection
            Constants.COMPLETE_JOB_WITH_PARTIAL_INSPECTION_ALERT = dbHelper.getLabel(Constants.LABEL_WANT_TO_COMPLETE_JOB_WITHOUT_ALL_TYRES_INSPECTED);

            // Loading The Odometer Reason
            mSpinnerObjList = new ArrayList<SpinnerObject>();
            mSpinnerObject = new SpinnerObject(0,
                    dbHelper.getLabel(Constants.NO_ODOMETER_REASION));
            mSpinnerObjList.add(mSpinnerObject);
            int odometerCurCount = 1;
            Cursor cursorOdometer = null;
            cursorOdometer = dbHelper.getOdometerReason();
            if (CursorUtils.isValidCursor(cursorOdometer)) {
                if (cursorOdometer.moveToFirst()) {
                    do {
                        int odometerReasonIdStr = cursorOdometer.getInt(0);
                        String odometerReasonValueStr = cursorOdometer
                                .getString(1);
                        String odometerCodeStr = cursorOdometer.getString(2);
                        mSpinnerObject = new SpinnerObject(odometerReasonIdStr,
                                odometerReasonValueStr, odometerCodeStr);
                        mSpinnerObjList.add(mSpinnerObject);
                        if (mRetrievedOdometerCode.equals(odometerCodeStr)) {
                            mSpinnerNoOdoMeterSelectedPos = odometerCurCount;
                        }
                        odometerCurCount++;
                    } while (cursorOdometer.moveToNext());
                }
                CursorUtils.closeCursor(cursorOdometer);
            }

            dataAdapter = new ArrayAdapter<SpinnerObject>(mContext,
                    android.R.layout.simple_spinner_item, mSpinnerObjList);
            sSpinnerNoOdoMeterReason.setAdapter(dataAdapter);

            // Loading The Signature Reason
            mSpinnerObjList1 = new ArrayList<SpinnerObject1>();

            mSpinnerObject1 = new SpinnerObject1(DEFAULT_SIGNATURE_ID_VAL,
                    dbHelper.getLabel(Constants.NO_SIGNATURE_REASION));
            mSpinnerObjList1.add(mSpinnerObject1);
            int signatureCurCount = 1;
            Cursor cursorSignature = null;
            cursorSignature = dbHelper.getSignatureReason();
            if (CursorUtils.isValidCursor(cursorSignature)
                    && cursorSignature.moveToFirst()) {
                do {
                    String signatureReasonIdStr = cursorSignature.getString(0);
                    String signatureReasonValueStr = cursorSignature
                            .getString(1);
                    mSpinnerObject1 = new SpinnerObject1(signatureReasonIdStr,
                            signatureReasonValueStr);
                    mSpinnerObjList1.add(mSpinnerObject1);
                    if (mRetrievedSignatureReasonId
                            .equals(signatureReasonIdStr)) {
                        mSpinnerNoSignaturePos = signatureCurCount;
                    }
                    signatureCurCount++;
                } while (cursorSignature.moveToNext());
                CursorUtils.closeCursor(cursorSignature);
            }
            ArrayAdapter<SpinnerObject1> adapter = new ArrayAdapter<SpinnerObject1>(
                    mContext, android.R.layout.simple_spinner_item,
                    mSpinnerObjList1);
            sSpinnerNoSignature.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (dbHelper != null) {
                dbHelper.close();
            }
        }
        if (mInstanceState == null) {
            if (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW) {
                if (Job.getVehicleODO() != null) {
                    mValuePrimaryODO.setText(Job.getVehicleODO());
                }
                if (Job.getDriverName() != null) {
                    mValueDriversName.setText(Job.getDriverName());
                }
                mSwitchBtnVehUnuttended
                        .setChecked(mRetrvdVehUnatndStatus == 1 ? true : false);
                mSwitchBtnAgreeStatus
                        .setChecked(mRetrievedAgreeStatus == 1 ? true : false);
                mSwitchBtnRetorqueCompltd
                        .setChecked(mRetrvdThrtyMinRetorqStatus == 1 ? true
                                : false);
                if (mValuePrimaryODO.getText().toString().trim().equals("")) {
                    sSpinnerNoOdoMeterReason
                            .setSelection(mSpinnerNoOdoMeterSelectedPos);
                }
                if (mRetrvdVehUnatndStatus == 1) {
                    sSpinnerNoSignature.setSelection(mSpinnerNoSignaturePos);
                }
                if (mBitmap != null) {
                    Constants.IS_SIGANTURE_DONE = true;
                }
            }
        }

    }

    public void showFinishDateTimeDialog() {

        LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Finish Date Time", false, true, false);
        mRetainDateTimeDialog = 1;
        final EjobAlertDialog mFinishDateTimeDialog = new EjobAlertDialog(
                mContext);
        LayoutInflater li = LayoutInflater.from(mContext);
        View promptsView = li
                .inflate(R.layout.dialog_simple_one_textview, null);
        mFinishDateTimeDialog.setView(promptsView);

        // set user activity for
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });

        final TextView textMessage = (TextView) promptsView
                .findViewById(R.id.text_message);

        textMessage.setText(mDateDialogTitle);

        mFinishDateTimeDialog.setPositiveButton(mLabelYes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // update user activity
                        LogUtil.TraceInfo(TRACE_TAG, "Finish Date Time", "BT - Yes", false, true, false);
                        mFinishDateTimeDialog.updateInactivityForDialog();
                        setCurrentTimeAsFinishTime(
                                DateTimeUTC.getCurrentDate(mContext),
                                DateTimeUTC.getCurrentTime());
                        // ((EjobAlertDialog)
                        // mFinishDateTimeDialog).updateInactivityForDialog();
                        mRetainDateTimeDialog = -1;
                        dialogInterface.dismiss();
                    }
                });
        mFinishDateTimeDialog.setNegativeButton(mLabelNo,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // update user activity
                        LogUtil.TraceInfo(TRACE_TAG, "Finish Date Time", "BT - No", false, true, false);
                        mFinishDateTimeDialog.updateInactivityForDialog();
                        mRetainDateTimeDialog = -1;
                        dialogInterface.dismiss();
                        showDateAlert();
                    }
                });
        mFinishDateTimeDialog.setCancelable(false).create();
        mFinishDateTimeDialog.show();

    }

    /**
     * Alert Dialog to confirm that "current time as eJob finish time"
     */
    public void showDateAlert() {
        LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Date Alert", false, true, false);
        if (mAlert != null && mAlert.isShowing()) {
            return;
        }
        final EjobAlertDialog builder = new EjobAlertDialog(mContext);
        mIsDialogVisible = true;
        // FIXME the title 194 should not be used here, this is only temporary
        builder.setTitle(mFinishTimeDialogTitle);
        mInput = new TextView(mContext);
        mInputTime = new TextView(mContext);
        mInput.setTypeface(Typeface.DEFAULT_BOLD);
        mInputTime.setTypeface(Typeface.DEFAULT_BOLD);
        mInput.setBackgroundResource(R.drawable.picker);
        mInputTime.setBackgroundResource(R.drawable.picker);
        mInput.setPadding(10, 20, 0, 10);
        mInputTime.setPadding(10, 20, 0, 10);
        Date finishedDateTimeFromDB = null;
        if (!TextUtils.isEmpty(Job.getTimeFinished())) {
            finishedDateTimeFromDB = DateTimeUTC.toDate(Long.parseLong(Job
                    .getTimeFinished()));
        }
        if (TextUtils.isEmpty(mDialogSetDate)) {
            if (TextUtils.isEmpty(Job.getTimeFinished())) {
                mDialogSetDate = DateTimeUTC.getCurrentDate(mContext);
            } else {
                mDialogSetDate = DateTimeUTC.convertDateToString(
                        finishedDateTimeFromDB, mContext);
            }
        }
        if (TextUtils.isEmpty(mDialogSetTime)) {
            if (TextUtils.isEmpty(Job.getTimeFinished())) {
                mDialogSetTime = DateTimeUTC.getCurrentTime();
            } else {
                mDialogSetTime = DateTimeUTC.convertTimeToString(
                        finishedDateTimeFromDB, mContext);
            }

        }

        mInput.setText(mDialogSetDate);
        mInputTime.setText(mDialogSetTime);
        mInput.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // update user activity
                InactivityUtils.updateActivityOfUser();

                if (mNewFragmentDate.getDialog() != null) {
                    if (mNewFragmentDate.getDialog().isShowing()) {
                        return;
                    }
                }
                Bundle bundle = new Bundle();
                try {
                    String curDateStr = mInput.getText().toString().trim();
                    bundle.putIntArray(BUNDLE_SPLITTED_DATE, DateTimeUTC
                            .convertStringArrayToIntArray(DateTimeUTC
                                    .getSplittedDate(curDateStr, mContext)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mNewFragmentDate.setArguments(bundle);
                mNewFragmentDate.show(getFragmentManager(), "datePicker");
            }
        });
        mInputTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // update user activity
                InactivityUtils.updateActivityOfUser();

                if (mNewFragmentTime.getDialog() != null) {
                    if (mNewFragmentTime.getDialog().isShowing()) {
                        return;
                    }
                }
                Bundle bundle = new Bundle();
                try {
                    String curTimeStr = mInputTime.getText().toString().trim();
                    bundle.putIntArray(BUNDLE_SPLITTED_TIME, DateTimeUTC
                            .convertStringArrayToIntArray(DateTimeUTC
                                    .getSplittedTime(curTimeStr, mContext)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mNewFragmentTime.setArguments(bundle);
                mNewFragmentTime.show(getFragmentManager(), "timePicker");
            }
        });

        TableLayout.LayoutParams tableParams = new TableLayout.LayoutParams(
                TableLayout.LayoutParams.WRAP_CONTENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams rowParams = new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        TableLayout tableLayout = new TableLayout(mContext);
        tableLayout.setLayoutParams(tableParams);
        TableRow tableRow = new TableRow(mContext);
        tableRow.setLayoutParams(tableParams);
        mInput.setLayoutParams(rowParams);
        // update user activity when user interacts with layout
        tableLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();

            }
        });
        tableRow.addView(mInput);
        int leftMargin = 60;
        int topMargin = 0;
        int rightMargin = 0;
        int bottomMargin = 0;
        rowParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
        mInputTime.setLayoutParams(rowParams);
        tableRow.addView(mInputTime);
        tableLayout.addView(tableRow);
        builder.setView(tableLayout);
        builder.setPositiveButton(mOkLabel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        setCurrentTimeAsFinishTime(mInput, mInputTime);
                        builder.updateInactivityForDialog();
                    }
                });
        builder.setNegativeButton(mCancelLabel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        builder.updateInactivityForDialog();
                    }
                });
        mAlert = builder.create();
        mAlert.setCanceledOnTouchOutside(false);
        mAlert.show();
    }

    private void setCurrentTimeAsFinishTime(TextView input, TextView inputTime) {
        String finishDate = input.getText().toString();
        String finishTime = inputTime.getText().toString();
        setCurrentTimeAsFinishTime(finishDate, finishTime);
    }

    private void setCurrentTimeAsFinishTime(String finishDate, String finishTime) {

        getDeviceInfoForNoteField();
        mJobStartTime = DateTimeUTC.getDateInTicks(
                EjobTimeLocationFragment.sStartedOnDate,
                EjobTimeLocationFragment.sStartedOnTime, mContext);
        mJobFinishedTime = DateTimeUTC.getMillisecondsForSpecificDate(
                finishDate, finishTime, mContext);
        mDays = 3;// Adding 3 days to find jobValidity Date
        mJobValidityDate = DateTimeUTC
                .getMillisecondsAfterNdaysFromAspecificDate(finishDate,
                        finishTime, mDays, mContext);
        LogUtil.i(TAG, "Job Start Time : " + mJobStartTime);
        LogUtil.i(TAG, "Job Finished Time : " + mJobFinishedTime);
        LogUtil.i(TAG, "Job Validity Date : " + mJobValidityDate);
        if (mJobFinishedTime > mJobStartTime) {
            Toast.makeText(mContext, MSG_SIGNATURE_FORM_SAVED,
                    Toast.LENGTH_LONG).show();
            LogUtil.i(TAG, MSG_SIGNATURE_FORM_SAVED);

            //Update reserve PWT Tyre information
            updateReservedTyreInfo();

            mDb = new DBModel(mContext);
            mDb.open();
            Job.setStatus("0"); // completed
            Job.setTimeFinished(String.valueOf(mJobFinishedTime));
            Job.setJobValidityDate(String.valueOf(mJobValidityDate));
            SaveJob.callUpdateQueries(mDb, EjobFormActionActivity.sJobID,
                    mContext);
            SharedPreferences preferences = getActivity().getSharedPreferences(
                    Constants.GOODYEAR_CONF, 0);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(Constants.PARTIALLY_INSPECTYED, partiallyInspected());
            updateJobRefNumInAccountTabFromLaunchJob();
            cleanConfigurationBeforeClose();
            if(mDb!=null){
                mDb.close();
            }
            Intent startEjobList = new Intent(mContext, EjobList.class);
            startEjobList.putExtra("sentfrom", "ejobform");
            startActivity(startEjobList);
        } else {
            Toast.makeText(mContext, MSG_FINSIH_TIME_AFTER_START_TIME,
                    Toast.LENGTH_LONG).show();
            LogUtil.w(TAG, MSG_FINSIH_TIME_AFTER_START_TIME);
        }
        resetDateAlertFlag();
    }

    /**
     * Updating The Job Reference Number
     */
    private void updateJobRefNumInAccountTabFromLaunchJob() {
        DatabaseAdapter db = new DatabaseAdapter(mContext);
        try {
            db.createDatabase();
            db.open();
            /**
             * Condition blocking Job Reference number updation in Account table during edit mode
             */
            if(!Constants.COMESFROMUPDATE) {
                db.updateAccountTable(Constants.JOBREFNUMBERVALUE);
            }

            //Method to update FromJobID (tyre table)
            db.updateJobIDForDismountedReusableTyre(String.valueOf(Constants.JOBREFNUMBERVALUE));
            LogUtil.DBLog("SignatureBox", "updateJobRefNumInAccountTabFromLaunchJob", "updateJobIDForDismountedReusableTyre");


        } catch (Exception e2) {
            e2.printStackTrace();
            LogUtil.DBLog("SignatureBox", "updateJobRefNumInAccountTabFromLaunchJob", "Exception : "+e2.getMessage());
        }finally {
            if(db != null) {
                db.close();
            }
			}

    }

    /**
     * Method to Update Reserved PWT Tyre
     */
    private void updateReservedTyreInfo()
    {
        DatabaseAdapter dbAdapter = new DatabaseAdapter(mContext);
        try {

            dbAdapter.createDatabase();
            dbAdapter.open();
            //Delete unused Reserved PWT Tire
            dbAdapter.deleteReservedUnusedPWTTireBasedOnJobReferenceNumber(String.valueOf(Constants.JOBREFNUMBERVALUE));
            LogUtil.DBLog(TAG, "updateReservedTyreInfo", "Done");
        }
        catch (Exception e)
        {
            LogUtil.DBLog(TAG, "updateReservedTyreInfo", "Exception : "+e.getMessage());
        }
        finally {
            dbAdapter.close();
        }
    }

    /**
     * Releasing The resources after save
     */
    private void cleanConfigurationBeforeClose() {
        VehicleSkeletonFragment.mAxleRecommendedPressure = null;
        Retorque.retorqueDone = false;
        Constants.TIRE_INFO = null;
        VehicleSkeletonFragment.tyreInfo = null;
        VehicleSkeletonFragment.mJobItemList = null;
        VehicleSkeletonFragment.mJobcorrectionList = null;

        VehicleSkeletonFragment.mTireImageItemList = null;
        Constants.JOB_ITEMLIST = null;
        Constants.JOB_CORRECTION_LIST = null;
        Constants.JOB_TIREIMAGE_ITEMLIST = null;
        Constants.vehicleConfiguration = null;
        Constants.UPDATE_APPLIED_ON_EDIT = false;
        Retorque.torqueSettings = null;
        Constants.TORQUE_SETTINGS_ARRAY = null;
        Job.clearJobObject();
        EjobAdditionalServicesFragment.sServiceValues.clear();
        EjobAdditionalServicesFragment.sSwitchBtns.clear();
        VehicleSkeletonFragment.mReserveTyreList.clear();
        ReservePWTUtils.mReserveTyreList.clear();
        VehicleSkeletonFragment.resetBoolValue();
        Constants.HAS_INSPECTED_TIRE = false;
        Constants.Search_TM_ListItems.clear();
        Job.setSignatureImage(null);
        Constants.IS_FORM_ELEMENTS_CLEANED = true;
        Constants.IS_SIGANTURE_DONE = false;
        // Cleaning the Static variables assigned while create a job to avoid
        // values carried over when a different user logs in.
        Constants.SAP_VENDORCODE_ID = "";
        Constants.VENDER_NAME = "";
        Constants.SAP_CONTRACTNUMBER_ID = "";
        Constants.SAP_CUSTOMER_ID = "";
        Constants.SAPCONTRACTNUMBER = "";
        Constants.SAP_CUSTOMER_ID_JOB = "";
        Constants.CHECKED_JOB_LIST.clear();
        Constants.JOB_CORRECTION_COUNT_SWAP = 0;
        Constants.JOBREFNUMBERVALUE = 0;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == NOTES_INTENT) {
            if (data != null) {
                Constants.EjobSignatureBoxNotes = data.getExtras().getString("mEjobSignatureNoteEditText");
                showFinishDateTimeDialog();
            }
        }
    }


    /**
     * This is a date dialog fragment class. It shows/sets the selected/current
     * date
     */
    @SuppressLint("ValidFragment")
    public class ReceivedDatePicker extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        private SafeDatePickerDialog mSafeDatePickerDialog;

        @Override
        public Dialog onCreateDialog(Bundle savedInstance) {
            int[] splitteddate = null;
            if (getArguments().containsKey(BUNDLE_SPLITTED_DATE)) {
                splitteddate = getArguments().getIntArray(BUNDLE_SPLITTED_DATE);
            }
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            if (splitteddate != null && splitteddate.length >= 3) {
                year = splitteddate[2];
                month = splitteddate[1] - 1;
                day = splitteddate[0];
            }
            mSafeDatePickerDialog = new SafeDatePickerDialog(getActivity(),
                    this, year, month, day);
            return mSafeDatePickerDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            // update user activity
            InactivityUtils.updateActivityOfUser();

            if (!mSafeDatePickerDialog.isStopped()) {
                String curDate = DateTimeUTC.splittedDateToString(year,
                        (month + 1), day, mContext);
                if (!TextUtils.isEmpty(curDate)) {
                    if (mInput != null) {
                        mInput.setText(curDate);
                    }
                }
            }
        }
    }

    /**
     * This is a Listener class for the EditText valuePrimaryODO which
     * implements TextWatcher
     */
    private class PrimaryODOTextChanged implements TextWatcher {
        @Override
        public void afterTextChanged(Editable text) {
            String changed = text.toString();
            //Fix - Bug : 702 (restrict users from entering 0 (zero) as a tacho calue)
            if (changed.equals("") ||changed.trim().matches(EjobInformationFragment.Regex_For_Zero_Entry) ) {
                if (!Constants.COMESFROMVIEW) {
                    sSpinnerNoOdoMeterReason.setEnabled(true);
                }
            } else {
                enableSaveButton();
                dataAdapter.notifyDataSetChanged();
                sSpinnerNoOdoMeterReason.setSelection(0, true);
                sSpinnerNoOdoMeterReason.setEnabled(false);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            //Fix - Bug : 702 (restrict users from entering 0 (zero) as a tacho calue)
            if (!TextUtils.isEmpty(mValuePrimaryODO.getText().toString().trim())) {
                if(mValuePrimaryODO.getText().toString().trim().matches(EjobInformationFragment.Regex_For_Zero_Entry))
                {
                    mValuePrimaryODO.setText("");
                    sSpinnerNoOdoMeterReason.setEnabled(true);
                }
            }

        }
    }

    /**
     * Submit button enable/disable
     */
    public void diableEnableSpinner() {
        boolean isReady = mValuePrimaryODO.getText().toString().length() > 0;
        if (isReady) {
            sSpinnerNoOdoMeterReason.setEnabled(false);
        } else {
            dataAdapter.notifyDataSetChanged();
            sSpinnerNoOdoMeterReason.setSelection(0, true);
            sSpinnerNoOdoMeterReason.setEnabled(true);
        }
    }

    /**
     * This is a custom view where user can do signature
     */
    public class SignatureWidget extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();
        private Paint bitmapPaint = new Paint(Paint.DITHER_FLAG);

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        // Constructor
        public SignatureWidget(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
        }

        public void sePaintColor(int signColor) {
            if (signColor == SIGNATURE_COLOR_FLAG_W) {
                paint.setColor(Color.TRANSPARENT);
            } else {
                paint.setColor(Color.BLACK);
            }
        }

        /**
         * Get signature as bitmap
         *
         * @param v
         * @return Bitmap
         */
        public Bitmap getSignatureBitmap(View v) {
            int width = 0;
            int height = 0;
            Bitmap bitmap = null;

            if (v != null) {
                width = v.getWidth();
                height = v.getHeight();
            }

            if (width > 0 && height > 0) {
                bitmap = Bitmap.createBitmap(v.getWidth(), v.getHeight(),
                        Bitmap.Config.RGB_565);
                Canvas canvas = new Canvas(bitmap);
                v.draw(canvas);
                try {
                    ByteArrayOutputStream array = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, array);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return bitmap;
        }

        /**
         * Clear signature
         */
        public void clear() {
            path.reset();
            if (mBitmap != null) {
                mBitmap.recycle();
                mBitmap = null;
            }
            invalidate();
            Constants.IS_SIGANTURE_DONE = false;
            Job.setSignatureImage(null);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            if (mBitmap != null) {
                canvas.drawBitmap(mBitmap, 0, 0, bitmapPaint);
            }
            if (!Constants.COMESFROMVIEW) {
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeJoin(Paint.Join.ROUND);
                paint.setStrokeWidth(STROKE_WIDTH);
                canvas.drawPath(path, paint);
            }
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (mSwitchBtnVehUnuttended != null
                            && !mSwitchBtnVehUnuttended.isChecked()) {
                        Constants.IS_SIGANTURE_DONE = true;
                    } else {
                        Constants.IS_SIGANTURE_DONE = false;
                    }

                    enableSaveButton();
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:
                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }
            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }

    private String checkMinutes(int min) {
        if (min < 10) {
            return "0" + min;
        } else {
            return String.valueOf(min);
        }
    }

    /**
     * This is a date dialog fragment class. It shows/sets the selected/current
     * time
     */
    @SuppressLint("ValidFragment")
    public class ReceivedOnTimePicker extends DialogFragment implements
            TimePickerDialog.OnTimeSetListener {

        private SafeTimePickerDialog mSafeTimePickerDialog;

        @Override
        public Dialog onCreateDialog(Bundle savedInstance) {
            int[] splittedtime = null;
            if (getArguments().containsKey(BUNDLE_SPLITTED_TIME)) {
                splittedtime = getArguments().getIntArray(BUNDLE_SPLITTED_TIME);
            }
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int min = c.get(Calendar.MINUTE);
            if (splittedtime != null && splittedtime.length >= 2) {
                hour = splittedtime[0];
                min = splittedtime[1];
            }
            mSafeTimePickerDialog = new SafeTimePickerDialog(mContext, this,
                    hour, min, DateFormat.is24HourFormat(mContext));
            return mSafeTimePickerDialog;
        }

        @Override
        public void onTimeSet(android.widget.TimePicker view, int hourOfDay,
                              int minute) {
            if (mSafeTimePickerDialog.isStopped()) {
                return;
            }
            String min = checkMinutes(minute);
            String time = hourOfDay + ":" + min;
            if (mInputTime != null) {
                mInputTime.setText(time);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mNewFragmentDate.getDialog() != null) {
            if (mNewFragmentDate.getDialog().isShowing()) {
                mNewFragmentDate.dismiss();
            }
        }
        if (mNewFragmentTime.getDialog() != null) {
            if (mNewFragmentTime.getDialog().isShowing()) {
                mNewFragmentTime.dismiss();
            }
        }

        if (mInfoDialogFrag != null && mInfoDialogFrag.getDialog() != null) {
            if (mInfoDialogFrag.getDialog().isShowing()) {
                mInfoDialogFrag.dismiss();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        try {
            if (!Constants.IS_FORM_ELEMENTS_CLEANED) {
                if (mView != null) {
                    mView.setDrawingCacheEnabled(true);
                    Constants.RETAIN_BITMAP = mSignature
                            .getSignatureBitmap(mView);

                    outState.putInt("retainAgreeStatus", mAgreeStatus);
                    outState.putInt("retainThirtyMinutesRetorqueStatus",
                            mThirtyMinutesRetorqueStatus);
                    outState.putInt("retainVehicleUnattendedStatus",
                            mVehicleUnattendedStatus);
                }

                if (!Constants.IS_FORM_ELEMENTS_CLEANED) {

                }
                if (mValuePrimaryODO != null) {
                    String valuePrimaryOdo = mValuePrimaryODO.getText()
                            .toString().trim();
                    outState.putString("retainValuePrimaryODO", valuePrimaryOdo);
                    Job.setVehicleODO(valuePrimaryOdo);
                }
                if (mValueDriversName != null) {
                    String driverName = mValueDriversName.getText().toString()
                            .trim();
                    outState.putString("retainValueDriversName", driverName);
                    Job.setDriverName(driverName);
                }
                if (sSpinnerNoSignature != null) {
                    outState.putString("retainSignatureReasonId",
                            ((SpinnerObject1) sSpinnerNoSignature
                                    .getSelectedItem()).getId());
                    outState.putString("retainSignatureReasonValue",
                            ((SpinnerObject1) sSpinnerNoSignature
                                    .getSelectedItem()).getValue());
                }
                if (sSpinnerNoOdoMeterReason != null) {
                    outState.putInt("retainOdometerReasonId",
                            ((SpinnerObject) sSpinnerNoOdoMeterReason
                                    .getSelectedItem()).getId());
                    outState.putString("retainOdometerReasonValue",
                            ((SpinnerObject) sSpinnerNoOdoMeterReason
                                    .getSelectedItem()).getValue());
                }
                mIsDialogVisible = mAlert != null && mAlert.isShowing();
                if (mIsDialogVisible) {
                    mAlert.dismiss();
                }
                outState.putBoolean("isDialogVisible", mIsDialogVisible);
                if (mIsDialogVisible) {
                    if (mInput != null) {
                        outState.putString("retainCurrentDate", mInput
                                .getText().toString().trim());
                    }
                    if (mInputTime != null) {
                        outState.putString("retainCurrentTime", mInputTime
                                .getText().toString().trim());
                    }

                }
                outState.putBoolean("isInfoDialogVisible", mIsInfoDialogVisible);
                outState.putBoolean("isFinishDateTimeDialog",
                        (mFinishDateTimeDialog != null && mFinishDateTimeDialog
                                .isShowing()));
                outState.putInt("mRetainDateTimeDialog", mRetainDateTimeDialog);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            mCountScreenNotVisibleToUser++;
            if (mCountScreenNotVisibleToUser > 1) {
                generateSignature();
                retainJob();
                mCountScreenNotVisibleToUser = 0;
            }
            mScreenLoaded = 0;
        } else {
            if (isFromSigned()) {
                enableSaveButton();
            }
            retrieveRetainedJob();
            mScreenLoaded = 0;
        }
    }

    /**
     * Retrieve retained job
     */
    private void retrieveRetainedJob() {
        if (!TextUtils.isEmpty(Job.getVehicleODO()) && mValuePrimaryODO != null) {
            mValuePrimaryODO.setText(Job.getVehicleODO());
        }
        if (!TextUtils.isEmpty(Job.getDriverName())
                && mValueDriversName != null) {
            mValueDriversName.setText(Job.getDriverName());
        }
    }

    /**
     * Retain the Job details
     */
    public void retainJob() {
        mOdometerReasonId = ((SpinnerObject) sSpinnerNoOdoMeterReason
                .getSelectedItem()).getId();
        mSignatureReasonId = ((SpinnerObject1) sSpinnerNoSignature
                .getSelectedItem()).getId();
        mOdometerCode = ((SpinnerObject) sSpinnerNoOdoMeterReason
                .getSelectedItem()).getCode();
        if (mOdometerReasonId == 0) {
            Job.setMissingOdometerReasonID("");
        } else {
            Job.setMissingOdometerReasonID(mOdometerCode);
        }
        Job.setAgreeStatus(String.valueOf(mAgreeStatus));
        Job.setSignatureReasonID(mSignatureReasonId);
        Job.setVehicleUnattendedStatus(String.valueOf(mVehicleUnattendedStatus));
        Job.setThirtyMinutesRetorqueStatus(String
                .valueOf(mThirtyMinutesRetorqueStatus));
        if (mValuePrimaryODO != null) {
            Job.setVehicleODO(mValuePrimaryODO.getText().toString().trim());
        }
        if (mValueDriversName != null) {
            Job.setDriverName(mValueDriversName.getText().toString().trim());
        }
        if (mSignatureReasonId.equals(DEFAULT_SIGNATURE_ID_VAL)) {
            Job.setSignatureImage(mSendImage);
        } else {
            Job.setSignatureImage(null);
        }
        Job.setStatus("1"); // incomplete status: 1
    }

    /**
     * Generates the signature from custom view
     */
    public void generateSignature() {
        if (Constants.IS_SIGANTURE_DONE) {
            if (mView != null) {
                mView.setDrawingCacheEnabled(true);
                if (mSignature != null) {
                    mBitmap = mSignature.getSignatureBitmap(mView);
                    if (mBitmap != null) {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        mSendImage = stream.toByteArray();
                    }
                }
            }
        } else {
            mSendImage = null;
        }
    }

    /**
     * reset the current date & time and flag
     */
    public void resetDateAlertFlag() {
        mIsDialogVisible = false;
        mDialogSetDate = "";
        mDialogSetTime = "";
    }

    /**
     * Check if the Signature form is being either signed or any reason has been
     * selected if vehicle is unattended
     */
    private boolean isFromSigned() {
        boolean isSigned = true;

        if ((!mSwitchBtnVehUnuttended.isChecked() && Job.getSignatureImage() == null)// !mIsSignatureDone)
                || (mSwitchBtnVehUnuttended.isChecked() && mSignatureReasonId
                .equals(DEFAULT_SIGNATURE_ID_VAL))) {
            isSigned = false;
        }

        return isSigned;
    }

    protected void hideKeyboard(Context context) {
        try {
            InputMethodManager in = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#
     * onActivityBackPress()
     */
    @Override
    public boolean onActivityBackPress() {
        return false;

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#hideKeyBoard
     * (boolean)
     */
    @Override
    public void hideKeyBoard(boolean toHide) {
        if (toHide) {
            hideKeyboard(this.getActivity());
        }
    }

    public static String getScreenResolution(Context context) {

        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

		/*
		 * WindowManager wm = (WindowManager)
		 * context.getSystemService(Context.WINDOW_SERVICE); Display display =
		 * wm.getDefaultDisplay(); DisplayMetrics metrics = new
		 * DisplayMetrics(); display.getMetrics(metrics); int width =
		 * metrics.widthPixels; int height = metrics.heightPixels;
		 */

        return "{" + height + "," + width + "}";
    }

    /**
     * Method enabling the save icon as per the user selection on the UI It will
     * always turn the save icon color into black if there will be data to be
     * saved for the local database file and then will navigate back to the
     * ejobList
     */
    public void enableSaveButton() {
        if (mSaveButton != null) {
            mSaveButton.setEnabled(true);
            mSaveButton.setBackgroundResource(R.drawable.savejob_navigation);
        }
    }

    /**
     * Method disabling the save icon as per the user selection on the UI It
     * will always turn the save icon color into grey if there will not be any
     * data to be saved for the local database file
     */
    public void disableSaveButton() {
        if (mSaveButton != null) {
            mSaveButton.setEnabled(false);
            mSaveButton.setBackgroundResource(R.drawable.unsavejob_navigation);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.goodyear.ejob.interfaces.IEjobFragmentVisible#fragmentIsVisibleToUser
     * ()
     */
    @Override
    public void fragmentIsVisibleToUser() {
        // TODO: add save icon implementaion
    }

    /**
     * CR:: 469 This method is collecting all the info related to the
     * application installed and the device details It then inserts all the
     * collected data in the note field of Job table
     */
    private void getDeviceInfoForNoteField() {
        String mAppversionNumber = "";
        try {
            PackageInfo pinfo;
            pinfo = getActivity().getPackageManager().getPackageInfo(
                    getActivity().getPackageName(), 0);
            mAppversionNumber = pinfo.versionName;
        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String deviceBrand = Build.BRAND.toUpperCase();
        String deviceModel = Build.MODEL.toUpperCase();
        String deviceManufacturer = Build.MANUFACTURER.toUpperCase();
        String deviceOSVersion = Build.VERSION.RELEASE;

        Constants.FULL_DEVICE_DETAILS = mAppversionNumber + " | " + deviceBrand
                + " | " + deviceModel + " | " + deviceManufacturer + " | "
                + deviceOSVersion + " | " + getScreenResolution(mContext);
    }

    /**
     * CR :: 447
     * Method checking whether the selected vehicle is partially or fully Inspected
     * @return
     */
    public static boolean partiallyInspected() {
        int count=0;
    if (null == VehicleSkeletonFragment.tyreInfo){
        VehicleSkeletonFragment.tyreInfo = Constants.TIRE_INFO;
    }
        for (int j = 0; j < VehicleSkeletonFragment.tyreInfo.size(); j++) {
            if (VehicleSkeletonFragment.tyreInfo.get(j).isInspected()) {
                if (!VehicleSkeletonFragment.tyreInfo.get(j).isDismounted()) {
                    count++;
                }
            }
        }
        for (int z = 0; z < VehicleSkeletonFragment.tyreInfo.size(); z++) {
            if (VehicleSkeletonFragment.tyreInfo.get(z).isDismounted()) {
                count++;
            }
        }
        if(count==VehicleSkeletonFragment.tyreInfo.size()) {
            return false;
        }
        else {
            return true;
        }

    }

    /**
     * CR :: 447
     * Method checking whether the selected vehicle is partially with operation or fully Inspected
     * @return
     */
    public static int partiallyInspectedWithOperation() {
//        int mCount=0;
//        int iCount=0;
//        if (null == VehicleSkeletonFragment.mJobItemList){
//            VehicleSkeletonFragment.mJobItemList = Constants.JOB_ITEMLIST;
//        }
//         for (int i = 0; i < VehicleSkeletonFragment.tyreInfo.size(); i++) {
//            if (!VehicleSkeletonFragment.tyreInfo.get(i).isDismounted()) {
//                mCount++;
//                String tyreID = VehicleSkeletonFragment.tyreInfo.get(i).getExternalID();
//
//                for (int j = 0; j < VehicleSkeletonFragment.mJobItemList.size(); j++) {
//                    if (tyreID.equalsIgnoreCase(VehicleSkeletonFragment.mJobItemList.get(j).getTyreID())) {
//                        if (VehicleSkeletonFragment.mJobItemList.get(j).getInspectedWithOperation().equalsIgnoreCase("1")) {
//                            iCount++;
//                            break;
//                        }
//                    }
//                }
//            }
//        }
//        if(mCount==iCount) {
//            return false;
//        }
//        else {
//            return true;
//        }


        int mCount=0;
        int iCount=0;
        int tyreWithOperation=0;
        int tyreWithInspection=0;
        int tyreWithOperationInspection=0;

        if (null == VehicleSkeletonFragment.mJobItemList){
            VehicleSkeletonFragment.mJobItemList = Constants.JOB_ITEMLIST;
        }
        if (null == VehicleSkeletonFragment.tyreInfo){
            VehicleSkeletonFragment.tyreInfo = Constants.TIRE_INFO;
        }

        if(VehicleSkeletonFragment.tyreInfo!=null && VehicleSkeletonFragment.mJobItemList!=null) {
            for (int i = 0; i < VehicleSkeletonFragment.tyreInfo.size(); i++) {

                //Checking for Mounted Tyres and non-spare tyres
                if (!VehicleSkeletonFragment.tyreInfo.get(i).isDismounted() && !Boolean
                        .valueOf(VehicleSkeletonFragment.tyreInfo.get(i).isSpare())) {

                    mCount++;

                    String tyreID = VehicleSkeletonFragment.tyreInfo.get(i).getExternalID();

                    int noOfOperation = 0;
                    int noOfOperationWithInspection = 0;
                    int noOfInspection = 0;

                    //we checking for no Of Operation, no Of Operation With Inspection, no Of Inspection Only
                    for (int j = 0; j < VehicleSkeletonFragment.mJobItemList.size(); j++) {

                        if (tyreID.equalsIgnoreCase(VehicleSkeletonFragment.mJobItemList.get(j).getTyreID())) {
                            if (VehicleSkeletonFragment.mJobItemList.get(j).getActionType().equalsIgnoreCase("26")) {
                                noOfInspection++;
                                if (VehicleSkeletonFragment.mJobItemList.get(j).getInspectedWithOperation().equalsIgnoreCase("1")) {
                                    noOfOperationWithInspection++;
                                }
                            } else {
                                noOfOperation++;
                                if (VehicleSkeletonFragment.mJobItemList.get(j).getInspectedWithOperation().equalsIgnoreCase("1")) {
                                    noOfOperationWithInspection++;
                                }
                            }
                        }
                    }

                    //Icount Check
                    if (noOfOperationWithInspection > 0 || noOfInspection > 0 || noOfOperation > 0) {
                        iCount++;
                        if (noOfOperation > 0) {
                            tyreWithOperation++;
                        }
                        if (noOfInspection > 0) {
                            tyreWithInspection++;
                        }
                        if (noOfOperationWithInspection > 0) {
                            tyreWithOperationInspection++;
                        }
                    }
                }
            }

            //No Operation and No Inspection
            if (iCount == 0) {
                return InspectionState.NO_INSPECTION_NO_OPERATION;
            }

            //all tyre Inspected (Mixed or only Inspection or Only Operation)
            else if (mCount == iCount) {
                if (tyreWithInspection == 0) {
                    return InspectionState.ONLY_OPERATION_COMPLETE;
                } else if (tyreWithOperation == 0) {
                    return InspectionState.ONLY_INSPECTION_COMPLETE;
                } else if (tyreWithOperation > tyreWithInspection) {
                    return InspectionState.OPERATION_WITH_INSPECTION_COMPLETE;
                } else if (tyreWithOperation < tyreWithInspection) {
                    return InspectionState.OPERATION_WITH_INSPECTION_COMPLETE;
                } else {
                    return InspectionState.OPERATION_WITH_INSPECTION_COMPLETE;
                }
            }
            //Partially Inspected
            else {
                if (tyreWithOperation == 0) {
                    return InspectionState.ONLY_INSPECTION_PARTIAL;
                } else if (tyreWithInspection == 0) {
                    return InspectionState.ONLY_OPERATION_PARTIAL;
                } else if (tyreWithOperationInspection == tyreWithOperation && tyreWithOperationInspection == tyreWithInspection) {
                    return InspectionState.OPERATION_WITH_INSPECTION_PARTIAL;
                } else {
                    return InspectionState.OPERATION_WITH_INSPECTION_PARTIAL;
                }

            }
        }
        else
        {
            return InspectionState.EMPTY_DATA;
        }
    }

    /**
     * Method to show alert for partially inspection
     */
    private void partialInspectionAlertDialog() {
        LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Partial Inspection", false, true, false);
        // get prompts.xml view
        LayoutInflater inflator = LayoutInflater.from(mContext);
        View promptsView = inflator.inflate(R.layout.dialog_yes_no, null);
        TextView dialogText = (TextView) promptsView
                .findViewById(R.id.dialog_text_message);

        // load message
        DatabaseAdapter label = new DatabaseAdapter(mContext);
        label.createDatabase();
        label.open();

        //CONFIRM_SAVE
        dialogText.setText(Constants.COMPLETE_JOB_WITH_PARTIAL_INSPECTION_ALERT);

        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(mContext);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        // set user activity for
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton(label.getLabel(Constants.YES_LABEL),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        //No space to added notes. As per client request notes activity is commented
//                        //Call to Note Activity
//                        Intent noteIntent = new Intent(mContext, NoteActivity.class);
//
//                        noteIntent.putExtra("sentfrom", "EjobSignatureBox");
//                        startActivityForResult(noteIntent, EjobFormActionActivity.Ejob_SIGNATURE_NOTES_INTENT );
                        LogUtil.TraceInfo(TRACE_TAG, "Partial Inspection", "BT - Yes", false, true, false);
                        //Without notes activity Finish time dialog will prompt
                        showFinishDateTimeDialog();
                    }
                });
        // set dialog message
        alertDialogBuilder.setCancelable(false).setNegativeButton(label.getLabel(Constants.NO_LABEL),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        LogUtil.TraceInfo(TRACE_TAG, "Partial Inspection", "BT - No", false, true, false);
                        dialog.dismiss();
                    }
                });

        if(label != null) {
            label.close();
        }
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }
}