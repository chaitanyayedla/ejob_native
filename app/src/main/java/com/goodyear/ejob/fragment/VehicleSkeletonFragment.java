package com.goodyear.ejob.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.goodyear.ejob.CameraPreviewActivity;
import com.goodyear.ejob.DefineTireActivity;
import com.goodyear.ejob.DismountTireActivity;
import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.InspectionActivity;
import com.goodyear.ejob.MountPWTActivity;
import com.goodyear.ejob.MountTireActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.RegrooveTireOperation;
import com.goodyear.ejob.ReservePWT;
import com.goodyear.ejob.TurnOnRim;
import com.goodyear.ejob.WOTActivity;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.DBModel;
import com.goodyear.ejob.dbmodel.JobCorrection;
import com.goodyear.ejob.dbmodel.ReservedTyre;
import com.goodyear.ejob.dbmodel.VisualRemark;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.interfaces.IEjobFragmentVisible;
import com.goodyear.ejob.interfaces.IFragmentCommunicate;
import com.goodyear.ejob.interfaces.ISaveIconEjobFragment;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.job.operation.helpers.PressureCheck;
import com.goodyear.ejob.job.operation.helpers.PressureSwap;
import com.goodyear.ejob.job.operation.helpers.Retorque;
import com.goodyear.ejob.job.operation.helpers.TireImageHelpers;
import com.goodyear.ejob.job.operation.helpers.TireImageItem;
import com.goodyear.ejob.job.operation.helpers.TorqueSettings;
import com.goodyear.ejob.ui.jobcreation.AutoSwapImpl;
import com.goodyear.ejob.ui.jobcreation.LogicalSwapActivity;
import com.goodyear.ejob.ui.jobcreation.LogicalSwapDifferentSize;
import com.goodyear.ejob.ui.jobcreation.PhysicalSwapActivity;
import com.goodyear.ejob.ui.jobcreation.PhysicalSwapDifferentSize;
import com.goodyear.ejob.ui.jobcreation.UpdateSwapJobItem;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.EjobSaveIconStates;
import com.goodyear.ejob.util.Job;
import com.goodyear.ejob.util.JobBundle;
import com.goodyear.ejob.util.JsonParser;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.ReservePWTUtils;
import com.goodyear.ejob.util.TireActionType;
import com.goodyear.ejob.util.TireDesignDetails;
import com.goodyear.ejob.util.TireUtils;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.TyreState;
import com.goodyear.ejob.util.VehicleSkeletonInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * For bug- 464::Replaced all buttons of the tires and labels with ImageButton as
 * Image background resource of buttons stretched to occupy the whole button/layout
 * But an ImageButton resizes to image-size itself
 */

/**
 * Fragment to build vehicle skeleton and tire operations options
 *
 * @author amitkumar.h
 */
public class VehicleSkeletonFragment extends Fragment implements
        IFragmentCommunicate, IEjobFragmentVisible {
    private ISaveIconEjobFragment saveIconHandlerActivity;
    private EjobFormActionActivity ejobFormParentActivity;
    public Context mContext;
    DatabaseAdapter mDbHelper;
    private static LinearLayout mVehicle;
    public static ArrayList<Tyre> tyreInfo;
    private static ImageButton mLastSelected;
    private int mLastSelectedResource;
    private TextView mLastViewInfo;
    // private EditText userInput;
    private AlertDialog mAlertDialog_action;
    private Tyre mSelectedTyre;
    public static final int REGROOVE_INTENT = 101;
    public static final int TOR_INTENT = 102;
    public static final int DISMOUNT_INTENT = 103;
    public static final int MOUNT_INTENT = 104;
    public static final int MOUNT_PWT_INTENT = 105;
    public static final int WOT_INTENT = 106;
    public static final int INSPECTION_INTENT = 26;
    public static final int LOGICAL_SWAP_INTENT = 24;
    public static final int PHYSICAL_SWAP_DRAG_INITIAL_INTENT = 72;
    public static final int PHYSICAL_SWAP_DRAG_FIANL_INTENT = 96;
    public static final int DEFINE_TIRE_INTENT = 107;
    private ImageButton mTireIL;
    private ImageButton mTireOL;
    private ImageButton mTireOR;
    private ImageButton mTireL;
    private ImageButton mTireR;
    private ImageButton mTireIR;
    private static String mPosition_one;
    private static String mPosition_two;
    private static String mPosition_three;
    private static String mPosition_four;
    private TextView mValue_TyreBrand;
    private TextView nValue_TyreBrandDesign;
    private TextView mlvl_TyreBrandConfirm;
    private TextView mlbl_TyreBrand;
    private TextView mlbl_TyreBrandDesign;
    private String mFirstCorrectedSerial;
    public static final String mAXLEPRESSURE = "axlepressure";
    public static final String mAXLEPRESSURE_POS = "axlepos";
    private static String CHOOSE_DIFFERENT_TIRE_TO_SWAP = "Choose Different Tire to Swap";
    long t1, t2;
    int mCounter = -1; // This is used to swap the tires using dragging
    public static ArrayList<JobItem> mJobItemList = null;
    public static ArrayList<TireImageItem> mTireImageItemList = null;
    public static ArrayList<JobCorrection> mJobcorrectionList = null;
    public static ArrayList<Float> mAxleRecommendedPressure;
    public static ArrayList<String> mUserInputSerialVector;
    public static boolean mRetroqueStarted;
    public static boolean mTorStarted;
    public static boolean mSwapPressureStarted = false;
    private String mSourceTyrePosition;
    private String mTargetTyrePosition;
    private AlertDialog mAlertDialog;
    private int mTyreRIMSize;
    private String mFinalPostion;
    private ArrayList<String> mTyreSerialFromDB;
    private static String mEditedUserInput = "";
    /**
     * for orientation (which popup is loaded)
     */
    private int mDialogRefNo;
    private String mSecondaryPosition;
    public static boolean mDoRetroque;
    public static boolean mIsTireMenuDisplayed;
    public static boolean mIsPressureHigh = false;

    private Button mBtnConfirm;
    private Button mBtnEdit;
    private TextView mValue_BrandName;
    private TextView mValue_TyreNSK3;
    private TextView mValue_TyreNSK2;
    private TextView mValue_TyreNSK1;
    private TextView mValue_TyrePressure;
    private TextView mValue_TyreDesign;
    private TextView mValue_TyreSerial;
    private TextView mlbl_BrandName;
    private TextView mlbl_TyreNSK3;
    private TextView mlbl_TyreNSK2;
    private TextView mlbl_TyreNSK1;
    private TextView mlbl_TyrePressure;
    private TextView mlbl_TyrePressure_Measurement;
    private TextView mlbl_TyreDesign;
    private TextView mlbl_TyreSerial;
    private TextView mlbl_SerialConfirmation;
    private String mSerialNumber;
    private String mNskOne;
    private String mNskTwo;
    private String mNskThree;
    private String mDesign;
    private String mBrandConfirmLabel;
    private String mBrand;
    private String mPressure;
    private String mSwapOperationMessage;
    private TextView mlabelAccount;
    private TextView mValueAccount;
    private TextView mlabelVehId;
    private TextView mValueVehId;
    private TextView mLabelFleetNumber;
    private TextView mValueFleetNumber;
    private TextView mLabelRefNumber;
    private TextView mValueRefNumber;
    private TextView mActivityName;
    private ImageView mJobTypeIcon;
    private Button mSaveJob;
    private Button mSaveNavPWT;
    View mViewnew;
    public static ArrayList<HashMap<String, String>> mPressureTorqueAxle_list = new ArrayList<HashMap<String, String>>();
    public static ArrayList<ReservedTyre> mReserveTyreList = new ArrayList<ReservedTyre>();
    Spinner mReserve_pwt_spinner;
    int mReserve_tyre_selected = 0;
    int mHistoryCount = 0;
    private String mBrand_label;
    private boolean dialogInEditMode;
    private DBModel ejobDbInsert;
    private String serialNumberInvaid;
    public static String pos2;
    public static String pos1;
    private static String INITIAL_SERIAL_NUMBER;
    private String serialNumberInEditMode = "";
    private Dialog mTireOperationDialogMenu = null;
    private String mArrowState = null;
    private String mSizeNotAllowed;

    /**
     * OnOrientationChange parameter passing
     * populateAlretForDuplicateSerialNumber()
     */
    private String mTempNumber = "";
    private String mEditLabel;
    private String mConfirmLabel;
    private String mYesLabel;
    private String mNoLabel;
    private String mPhysicalSwapLabel;
    private String mDismountLabel;
    private String mLogicalSwapLabel;
    private String mMountNewTireLabel;
    private String mMountPWTTireLabel;
    private String mRegrooveLabel;
    private String mWotLabel;
    private String mTorLabel;
    private String mStrCHOOSEANOTHEROPERATION, mStrCHOOSEANOTHERTYRE,
            mStrCHOOSEDIFFERENTTYRETOSWAP, mStrNOSWAPONDISMOUNTEDTYRE;
    /**
     * Is note editable or not, in tyre history view
     */
    private boolean mIsNotesInEditMode = false;
    private String mCurentNote = "";
    /**
     * Warning message for Note in edit mode at the selection of other tyres
     */
    private String mSaveNoteMessage = "Please save your note before perform other operation";
    private TextView mRearTxt;
    private TextView mFrontTxt;
    private String mLblNoTyresFound = "No tyres found";
    boolean mIsNoteDialogOrientationChange = false;
    private String mSerialConfirmationLabel;
    private String mOKLabel;
    private String mCancelLabel;
    private String mEnterSerialLabel;
    private String mSelectReserveSerialLabel;
    private String mDuplicateSerialLabel;
    private String mSwapFinalConfirmationMessage = "";
    Dialog serialCorrectionDialog;
    private String mSwapFinalConfirmationMessageFromDB = "";
    private static final String TAG = "VehicleSkeletonFragment";
    private static final String TRACE_INFO = "Vehicle";
    Bundle savedInstanceState;

    //Labels for Inspection Data
    private String mLabelTemperature;
    private String mLabelRecordedPressure;
    private String mLabelCorrectedPressure;
    private String mLabelInspection;
    private String mLabelTyreInspected;
    private String mTyreHOT;
    private String mTyreCOLD;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
        View rootView = inflater.inflate(R.layout.vehicle_skeleton_config,
                container, false);
        mDbHelper = new DatabaseAdapter(mContext);
        ejobDbInsert = new DBModel(mContext);
        initialize(rootView);
        loadLabelsFromDB();

        // AMIT
        if (Constants.SELECTED_TYRE == null
                && Constants.SECOND_SELECTED_TYRE == null) {
            Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
            Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
        }
        // create a vehicle layout
        mVehicle = (LinearLayout) rootView.findViewById(R.id.vehiclebox);
        try {
            mUserInputSerialVector = new ArrayList<String>();
            VehicleSkeletonInfo vehicleconfg = new VehicleSkeletonInfo(
                    Constants.vehicleConfiguration);
            vehicleconfg.getVehicleSkeletonInfo();

            if (Constants.TIRE_INFO == null) {
                tyreInfo = JsonParser.getTyreInfo(Constants.tireJson,
                        vehicleconfg.getAxlecount());
            } else {
                tyreInfo = Constants.TIRE_INFO;
            }
            // axle recommended pressure
            // Always keep after tyreInfo value set
            if (mAxleRecommendedPressure == null) {
                mAxleRecommendedPressure = new ArrayList<Float>();
                initializeAxlePressure(vehicleconfg.getAxlecount());
            }
            if (savedInstanceState != null) {
                mArrowState = savedInstanceState.getString("mArrowState");
                // Getting the data for notes
                mCurentNote = savedInstanceState.getString("mCurentNote", "");
                NoteEditTag = savedInstanceState.getString("NoteEditTag", "");
                mIsNotesInEditMode = savedInstanceState.getBoolean(
                        "mIsNotesInEditMode", false);
                itr_position = savedInstanceState.getInt("itr_position");
                mJobActionType = savedInstanceState.getInt("action_type");
                mIsNoteDialogOrientationChange = savedInstanceState.getBoolean(
                        "mIsNoteDialogOrientationChange", false);
            }

            buildSkeleton(vehicleconfg);
            // initialize job item
            if (Constants.JOB_ITEMLIST == null) {
                mJobItemList = new ArrayList<JobItem>();
            } else {
                mJobItemList = Constants.JOB_ITEMLIST;
            }

            if (Constants.JOB_TIREIMAGE_ITEMLIST == null) {
                mTireImageItemList = new ArrayList<TireImageItem>();
            } else {
                mTireImageItemList = Constants.JOB_TIREIMAGE_ITEMLIST;
            }
            // initialize job correction
            if (Constants.JOB_CORRECTION_LIST == null) {
                mJobcorrectionList = new ArrayList<JobCorrection>();
            } else {
                mJobcorrectionList = Constants.JOB_CORRECTION_LIST;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (savedInstanceState != null) {
            mDialogRefNo = savedInstanceState.getInt("DIALOG_NUMBER");
            mSourceTyrePosition = savedInstanceState
                    .getString("sourceTyrePosition");
            mTargetTyrePosition = savedInstanceState
                    .getString("targetTyrePosition");
            mEditedUserInput = savedInstanceState.getString("userInput", "");
            serialNumberInEditMode = savedInstanceState
                    .getString("serialNumberForDialogEdit");
            mTempNumber = savedInstanceState.getString(
                    "serialNumberForDuplicateAlert", "");
            Object obj = savedInstanceState.getSerializable("selectedTyre");
            if (obj instanceof Tyre) {
                mSelectedTyre = (Tyre) obj;
            }
            dialogInEditMode = savedInstanceState
                    .getBoolean("dialogInEditMode");
            System.out.println("On CReate :: " + mDialogRefNo);
            LogUtil.i(TAG, "On CReate :: " + mDialogRefNo);
            switch (mDialogRefNo) {
            /*
			 * case 1: loadSwapActionDialog(); break;
			 */
                case 2:
                    loadSerialCorrectionDialog();
                    break;
                case 3:
                    loadBrandCorrection();
                    break;
                case 5:
                    loadSwapFinalConfirmation();
                    break;
                case 8:
                    loadActionDialog();//
                    break;
                case 9:
                    loadAnotherTyreDialog();
                    break;
                case 10:
                    loadSwapActionDialog();
                    break;
                case 11:
                    loadSerialMountNewDialog();
                    break;
                case 12:
                    mReserve_tyre_selected = savedInstanceState
                            .getInt("reserve_tyre_selected");
                    loadSerialMountPWTDialog();
                    break;
                case 15:
                    populateAlretForDuplicateSerialNumber(mTempNumber);
                    break;
                case 18:
                    loadLogicalSwapActionDialog();
                    break;
                case 19:
                    showDuplicateSerialNoAlertForDismount();
                    break;
            }
        }
        return rootView;
    }

    private void initialize(View rootView) {
        mActivityName = (TextView) rootView.findViewById(R.id.textView1);
        mlabelAccount = (TextView) rootView.findViewById(R.id.lbl_account);
        mValueAccount = (TextView) rootView.findViewById(R.id.value_account);
        mlabelVehId = (TextView) rootView.findViewById(R.id.lbl_vehId);
        mValueVehId = (TextView) rootView.findViewById(R.id.value_vehId);
        mLabelFleetNumber = (TextView) rootView.findViewById(R.id.lbl_fleetNo);
        mValueFleetNumber = (TextView) rootView
                .findViewById(R.id.value_fleetNo);
        mLabelRefNumber = (TextView) rootView.findViewById(R.id.lbl_jobRefNo);
        mValueRefNumber = (TextView) rootView.findViewById(R.id.value_jobRefNo);
        mFrontTxt = (TextView) rootView.findViewById(R.id.lbl_front);
        mRearTxt = (TextView) rootView.findViewById(R.id.details);
        if (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW) {
            mValueRefNumber.setText(JobBundle.getmRefNO().toString());
            mValueVehId.setText(JobBundle.getmLPlate().toString());
            mValueAccount.setText(JobBundle.getmAccountName());
            mValueFleetNumber.setText(JobBundle.getmFleetNO());
        }
        mSaveJob = (Button) rootView.findViewById(R.id.btn_save);
        mSaveNavPWT = (Button) rootView.findViewById(R.id.pwtNavbtn);
        if (Constants.COMESFROMVIEW) {
            mSaveNavPWT.setVisibility(View.INVISIBLE);
            mSaveJob.setVisibility(View.INVISIBLE);
        }
        // updateSaveButtonOnUIBasedOnLastFragment();
        mSaveJob.setOnClickListener(new OnClickListener() {
            @Override
         		public void onClick(View v) {
				/**
				 * Condition added to disable save button once clicked during Save Job in DB
				 */
				if (saveIconHandlerActivity.getSaveIconState() == EjobSaveIconStates.ICON_BLACK) {
                    LogUtil.TraceInfo(TRACE_INFO, "Save Button - Black", "Save and Move to Ejob List",false,true,false);
					disableSaveButton();
					EjobFormActionActivity.saveJobInfo(v, true);
				} else if (saveIconHandlerActivity.getSaveIconState() == EjobSaveIconStates.ICON_RED) {
                    LogUtil.TraceInfo(TRACE_INFO, "Save Button - Red", "Save",false,true,false);
					disableSaveButton();
					EjobFormActionActivity.saveJobInfo(v, false);
					finalSaveButton();
				}
			}
		});
        // job icon on header
        mJobTypeIcon = (ImageView) rootView.findViewById(R.id.jobIcon);
        if (!Constants.JOBTYPEREGULAR) {
            mJobTypeIcon.setImageResource(R.drawable.breakdown_job);
        }
    }

    /**
     *
     */
    private void updateSaveButtonOnUIBasedOnLastFragment() {
        if (saveIconHandlerActivity == null)
            return;
        int saveIconState = saveIconHandlerActivity.getSaveIconState();
        switch (saveIconState) {
            case EjobSaveIconStates.ICON_GRAY:
                disableSaveButton();
                break;
            case EjobSaveIconStates.ICON_BLACK:
                finalSaveButton();
                break;
            case EjobSaveIconStates.ICON_RED:
                enableSaveButton();
                break;
        }
    }

    /**
     * onPause Called
     */
    @Override
    public void onPause() {
        super.onPause();

    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mDbHelper != null) {
                mDbHelper.close();
            }
            ejobDbInsert.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * onResume Called
     */
    @Override
    public void onResume() {
        super.onResume();

        updateSaveButtonOnUIBasedOnLastFragment();

        try {
            mDbHelper.open();
            ejobDbInsert.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Retorque.startedNotes) {
            new Retorque(null, getActivity())
                    .reCreateRecommendTorqueSettingsAppliedDialog();
        }
    }

    /**
     * load labels from DB
     */
    private void loadLabelsFromDB() {
        DatabaseAdapter mDBlabels;
        mDBlabels = new DatabaseAdapter(mContext);
        try {
            mDBlabels.createDatabase();
            mDBlabels.open();
            mStrCHOOSEANOTHEROPERATION = mDBlabels
                    .getLabel(Constants.CHOOSEANOTHEROPERATION);
            mStrCHOOSEANOTHERTYRE = mDBlabels
                    .getLabel(Constants.CHOOSEANOTHERTYRE);
            mStrCHOOSEDIFFERENTTYRETOSWAP = mDBlabels
                    .getLabel(Constants.CHOOSEDIFFERENTTYRETOSWAP);
            /* Bug 713:Adding new string to show while trying to swap on a dismounted tyre*/
            mStrNOSWAPONDISMOUNTEDTYRE = mDBlabels.getLabel(Constants.NO_SWAP_ON_DISMOUNTED_TYRE);
            mDismountLabel = mDBlabels.getLabel(Constants.LABEL_DISMOUNT_TYRE);
            mPhysicalSwapLabel = mDBlabels
                    .getLabel(Constants.LABEL_PHYSICAL_SWAP);
            mLogicalSwapLabel = mDBlabels
                    .getLabel(Constants.LABEL_LOGICAL_SWAP);
            mMountNewTireLabel = mDBlabels
                    .getLabel(Constants.LABEL_MOUNT_NEW_TYRE);
            mMountPWTTireLabel = mDBlabels
                    .getLabel(Constants.MOUNT_PWT_TIRE_LABEL);
            mRegrooveLabel = mDBlabels
                    .getLabel(Constants.REGROOV_ACTIVITY_NAME);
            mWotLabel = mDBlabels.getLabel(Constants.LABEL_WOT);
            mYesLabel = mDBlabels.getLabel(Constants.YES_LABEL);
            mNoLabel = mDBlabels.getLabel(Constants.NO_LABEL);
            mEditLabel = mDBlabels.getLabel(Constants.LABEL_EDIT);
            mConfirmLabel = mDBlabels.getLabel(Constants.LABEL_CONFIRM);
            mTorLabel = mDBlabels.getLabel(Constants.TURN_ON_RIM_ACTIVITY_NAME);
            mLblNoTyresFound = mDBlabels.getLabel(Constants.NO_TYRES_FOUND);
            String activityname = mDBlabels
                    .getLabel(Constants.EJOB_FORM_VEHICLE_SKELETON);
            mActivityName.setText(activityname);
            String labelFromDB = mDBlabels.getLabel(Constants.ACCOUNT_LABEL);
            mlabelAccount.setText(labelFromDB);
            labelFromDB = mDBlabels.getLabel(Constants.VEH_ID_LABEL);
            mlabelVehId.setText(labelFromDB);
            if (!Constants.COMESFROMUPDATE && !Constants.COMESFROMVIEW) {
                mValueAccount.setText(Constants.ACCOUNTNAME);
                mValueVehId.setText(Constants.VEHID);
                mValueFleetNumber.setText(Constants.FLEETNUM);
                mLabelRefNumber.setText("#");
                mValueRefNumber.setText(Constants.JOBREFNUMBER);
            }
            mSerialNumber = mDBlabels.getLabel(Constants.SERIAL_NUMBER);
            mNskOne = mDBlabels.getLabel(Constants.NSK1_LABEL);
            mNskTwo = mDBlabels.getLabel(Constants.NSK2_LABEL);
            mNskThree = mDBlabels.getLabel(Constants.NSK3_LABEL);
            mDesign = mDBlabels.getLabel(Constants.DESIGN_LABEL);
            mBrand = mDBlabels.getLabel(Constants.BRAND_LABEL);
            // internal bug fixed: rotation issue
            mBrandConfirmLabel = mDBlabels
                    .getLabel(Constants.TYRE_BRAND_IS_REQUIRED);
            mPressure = mDBlabels.getLabel(Constants.PRESSURE_LABEL);
            mBrand_label = mDBlabels.getLabel(Constants.BRAND_LABEL);
            serialNumberInvaid = mDBlabels.getLabel(Constants.SERIAL_INVALID);
            mSwapOperationMessage = mDBlabels
                    .getLabel(Constants.SWAP_OPERATION_MSG);
            mSizeNotAllowed = mDBlabels.getLabel(Constants.SIZE_NOT_ALLOWED);
            mFrontTxt.setText(mDBlabels.getLabel(Constants.VEHICLE_FRONT));
            mRearTxt.setText(mDBlabels.getLabel(Constants.VEHICLE_REAR));
            mSerialConfirmationLabel = mDBlabels
                    .getLabel(Constants.SERIAL_CONFIRMATION_LABEL);
            mOKLabel = mDBlabels.getLabel(Constants.LABEL_OK);
            mCancelLabel = mDBlabels.getLabel(Constants.CANCEL_LABEL);
            mEnterSerialLabel = mDBlabels
                    .getLabel(Constants.PLEASE_ENTER_SERIAL_NUM);
            mSelectReserveSerialLabel = mDBlabels
                    .getLabel(Constants.SELECT_RESERVE_SERIAL_NUM);
            mDuplicateSerialLabel = mDBlabels
                    .getLabel(Constants.DUPLICATE_SERIAL_LABEL);
            mSaveNoteMessage = mDBlabels
                    .getLabel(Constants.SAVE_YOUR_NOTE_LABEL);

            labelFromDB = mDBlabels.getLabel(Constants.FLEET_NO);
            mLabelFleetNumber.setText(labelFromDB);

            mSwapFinalConfirmationMessage = mDBlabels
                    .getLabel(Constants.SWAP_FINAL_CONFIRMATION_LABEL);
            mSwapFinalConfirmationMessage = mSwapFinalConfirmationMessage
                    .replaceAll(Constants.DOTNET_STRING_PLACE_HOLDER_REGEX,
                            Constants.JAVA_STRING_PLACE_HOLDER);
            //Inspection Labels
            mLabelTemperature= mDBlabels.getLabel(Constants.LABEL_TEMPERATURE);
            mLabelRecordedPressure= mDBlabels.getLabel(Constants.LABEL_RECORDED_PRESSURE);
            mLabelCorrectedPressure= mDBlabels.getLabel(Constants.LABEL_CORRECTED_PRESSURE);
            mLabelInspection=mDBlabels.getLabel(Constants.LABEL_INSPECTION);
            mLabelTyreInspected=mDBlabels.getLabel(Constants.LABEL_TYRE_INSPECTED);
            mTyreHOT = mDBlabels.getLabel(Constants.LABEL_TYRE_HOT);
            mTyreCOLD = mDBlabels.getLabel(Constants.LABEL_TYRE_COLD);
            CHOOSE_DIFFERENT_TIRE_TO_SWAP = mDBlabels.getLabel(460);

            System.out.println("mSwapFinalConfirmationMessage-----------"
                    + mSwapFinalConfirmationMessage);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            mDBlabels.close();
        }
    }

    /**
     * initializing Axle Pressure
     */
    private void initializeAxlePressure(int axleCount) {
        for (int axleCounter = 0; axleCounter < axleCount; axleCounter++) {
            mAxleRecommendedPressure.add((float) 0);
        }
        boolean isSpareAdded = false;
        boolean isVehiclehasSpareTyres = false;
        if (Constants.COMESFROMUPDATE && null != tyreInfo) {
            for (Tyre tyre : tyreInfo) {
                // For Spare
                if (tyre.getPosition().contains("SP")) {
                    VehicleSkeletonInfo vehicleconfg = new VehicleSkeletonInfo(
                            Constants.vehicleConfiguration);
                    vehicleconfg.getVehicleSkeletonInfo();
                    if ((vehicleconfg.getAxlecount() + 1) == axleCount) {
                        // Do nothing
                    } else {
                        ++axleCount;
                    }
                    tyre.setPosition(axleCount + tyre.getPosition());
                }
                int tPosition = Integer.parseInt(tyre.getPosition().substring(
                        0, 1));
                int axle = tPosition - 1;
                if (tPosition > axleCount) {
                    // Do nothing
                    continue;
                } else if (!Boolean.parseBoolean(tyre.isSpare())
                        && mAxleRecommendedPressure.get(axle) <= 0) {
                    mAxleRecommendedPressure.set(axle,
                            CommonUtils.parseFloat(tyre.getPressure()));
                } else if (!isSpareAdded
                        && Boolean.parseBoolean(tyre.isSpare())) { // For Spares
                    isVehiclehasSpareTyres = true;
                    if (CommonUtils.parseFloat(tyre.getPressure()) > 0) {
                        mAxleRecommendedPressure.add(CommonUtils
                                .parseFloat(tyre.getPressure()));
                        isSpareAdded = true;
                    }
                }
            }
            if (isVehiclehasSpareTyres && !isSpareAdded) {
                mAxleRecommendedPressure.add((float) 0);
            }
        } else if (!Constants.COMESFROMUPDATE) {
            for (Tyre tyre : tyreInfo) {
                if (Boolean.parseBoolean(tyre.isSpare())) {
                    mAxleRecommendedPressure.add((float) 0);
                    break;
                }
            }
        }
    }

    /**
     * start building the vehicle skeleton using vehicle config
     *
     * @param vehicleconfig vehicle skeleton object with decoded info
     */
    private void buildSkeleton(VehicleSkeletonInfo vehicleconfig) {
        // get the skeleton array
        String[] config = vehicleconfig.getVehicleSkeletonInfo();
        boolean checkDoubleExist = false;

        // check if double mounted exists
        for (int i = 0; i < config.length; i++) {
            String axleInfo = config[i];
            if (axleInfo.contains("DU")) {
                checkDoubleExist = true;
            }
        }
        // position labels OL,IL etc
        if (checkDoubleExist) {
            createTireLablesAxle(true);
        } else {
            createTireLablesAxle(false);
        }
        // margin is just for now. it should change!
        // for each axle
        for (int i = 0, margin = 25; i < config.length; i++) {
            String axleInfo = config[i];
            // if current axle is double axle add double axle
            if (axleInfo.contains("DU")) {
                createDoubleAxle(margin, axleInfo, i + 1);
                // else add a single axle
            } else {
                createSingleAxle(margin, axleInfo, i + 1);
            }
        }
        addSpareTires();
        updateTireImages();
        if (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW) {
            updateRetorqueForTires();
        }
        if (mArrowState != null) {
            final ImageButton tyre = getViewFromUI(mArrowState);
            if (tyre != null) {
                tyre.post(new Runnable() {
                    @Override
                    public void run() {
                        showTireDetails(tyre);
                    }
                });
            }
        }
    }

    /**
     * if coming from update
     */
    private void updateRetorqueForTires() {
        boolean valuesCaptured = false;
        for (TorqueSettings torque : Constants.TORQUE_SETTINGS_ARRAY) {
            if (torque.getIsWheelRemoved() == 0) {
                continue;
            }
            Tyre tire = getTireByPosition(torque.getWheelPos());
            if (null != tire) {
                tire.setRetorqued(true);
                if (!valuesCaptured
                        && Constants.COMESFROMUPDATE
                        && TextUtils.isEmpty(torque
                        .getNoManufacturerTorqueSettingsReason())) {
                    valuesCaptured = true;
                    Retorque.retorqueDone = true;
                    boolean retroqueLabel;
                    if (torque.getRetorqueLabelIssued() == 1) {
                        retroqueLabel = true;
                    } else {
                        retroqueLabel = false;
                    }
                    Retorque.updateWrenchAndTorqueDetails(
                            torque.getWrenchNumber(),
                            String.valueOf(torque.getTorqueSettingsValue()),
                            retroqueLabel);
                }
            }
        }
    }

    /**
     * update tire images
     */
    public static void updateTireImages() {
        try {
            for (Tyre tire : tyreInfo) {
                if (tire.isDismounted()) {
                    int countofTireAtDismountPostion = getCountOfTiresAtPostion(tire
                            .getPosition());
                    if (countofTireAtDismountPostion == 0) {
                        continue;
                    } else if (countofTireAtDismountPostion > 1) {
                        tire.setPosition(tire.getPosition() + "$");
                    }
                }
                int selecteIcon = TireUtils.getTireIcon(tire, false);
                // by default set normal background
                ImageButton selectedTire = getViewFromUI(tire.getPosition());
                System.out.println("selectedTire:::: " + selectedTire);
                if (selectedTire != null) {
                    selectedTire.setBackgroundResource(selecteIcon);
                    mLastSelected = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * check if the last operation performed was dismount
     *
     * @return true if last operation is dismount else false
     */
    private boolean checkIfLastOperationIsDismount(String id) {
        if (null == Constants.JOB_ITEMLIST) {
            LogUtil.d("VS", "Job ItemList is null so returning false");
            return false;
        }
        for (int i = Constants.JOB_ITEMLIST.size() - 1; i >= 0; i--) {
            JobItem jobItem = Constants.JOB_ITEMLIST.get(i);
            if (id.equals(jobItem.getTyreID())) {
                if (TireActionType.SWAP.equals(jobItem.getActionType())) {
                    continue;
                }
                if (TireActionType.REMOVE.equals(jobItem.getActionType())) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * check the count of tire at the position (to check in edit mode if
     * selected position has a mount and dismount tire)
     *
     * @param position
     */
    private static int getCountOfTiresAtPostion(String position) {
        int count = 0;
        if (null == position) {
            return count;
        }
        for (Tyre swapTire : tyreInfo) {
            // map tire selected to tire array
            if (position.equals(swapTire.getPosition())) {
                count++;
            }
        }
        return count;
    }

    /**
     * get the button view from UI
     *
     * @param tirePos tire position to which the position corresponds
     * @return button view
     */
    private static ImageButton getViewFromUI(String tirePos) {
        ImageButton tire;
        // get tire selected
        if (tirePos.contains("SP")) {
            try {
                VehicleSkeletonInfo vehicleconfg = new VehicleSkeletonInfo(
                        Constants.vehicleConfiguration);
                vehicleconfg.getVehicleSkeletonInfo();
                int axelCount = vehicleconfg.getAxlecount();
                if (tirePos.startsWith("SP")) {
                    if ((vehicleconfg.getAxlecount() + 1) == axelCount) {
                        // Do nothing
                    } else {
                        ++axelCount;
                    }
                    tirePos = (axelCount + "" + tirePos);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("getViewFromUI tirePos::: " + tirePos);
        if (mVehicle.findViewWithTag(tirePos) instanceof ImageButton) {
            tire = (ImageButton) mVehicle.findViewWithTag(tirePos);
        } else {
            LinearLayout tireWorkedOnParent = (LinearLayout) mVehicle
                    .findViewWithTag(tirePos);
            if (tireWorkedOnParent == null) {
                return null;
            }
            tireWorkedOnParent.getChildCount();
            tire = (ImageButton) tireWorkedOnParent.getChildAt(0);
        }
        return tire;
    }

    /**
     * add spare tires to the skeleton
     */
    private void addSpareTires() {
        List<ImageButton> spareTyres = new ArrayList<>();
        // create the spare axle
        LinearLayout spareAxle = new LinearLayout(mContext);
        spareAxle.setOrientation(LinearLayout.HORIZONTAL);
        LayoutParams layoutPosition = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutPosition.setMargins(0, 25, 0, 0);
        layoutPosition.gravity = Gravity.CENTER;
        spareAxle.setLayoutParams(layoutPosition);
        int spareCounter = 0;
        int axleCount = mAxleRecommendedPressure.size();
        for (Tyre tire : tyreInfo) {
            if (Boolean.valueOf(tire.isSpare())) {
                String tempPosition = tire.getPosition();
                if (!TextUtils.isEmpty(tempPosition)
                        && tempPosition.endsWith("$")) {
                    continue;
                }
                if (tire.isDismounted()
                        && (getCountOfTiresAtPostion(tempPosition) > 1)) {
                    continue;
                }
                LinearLayout spareTire = new LinearLayout(mContext);
                LayoutParams layoutPosition_oneD = new LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                layoutPosition_oneD.gravity = Gravity.CENTER_HORIZONTAL;
                spareTire.setOrientation(LinearLayout.VERTICAL);
                spareTire.setTag(new String(axleCount + "SP" // Changed to SP
                        + ++spareCounter));
                spareTire.setLayoutParams(layoutPosition_oneD);
                mCounter++;
                spareTire.setOnDragListener(new ChoiceDragListener());
                // create left-outer tire
                mTireOL = new ImageButton(mContext);
                // add inner tire
                mTireL = new ImageButton(mContext);
                mTireL.setTag(new String(axleCount + "SP" + spareCounter));
                mTireL.setOnTouchListener(new ChoiceTouchListener());
                mTireL.setBackgroundResource(R.drawable.normal_tire);
                spareTire.addView(mTireL);
                spareTyres.add(mTireL);
                spareAxle.addView(spareTire);
            }
        }
        int width = getResources().getDimensionPixelSize(
                R.dimen.history_view_width);
        LinearLayout axleTireInfo = new LinearLayout(mContext);
        LayoutParams axleTireInfoPosition = new LayoutParams(width,
                LayoutParams.WRAP_CONTENT);
        axleTireInfoPosition.setMargins(0, 0, 0, 0);
        axleTireInfo.setOrientation(LinearLayout.VERTICAL);
        axleTireInfo.setLayoutParams(axleTireInfoPosition);
        axleTireInfo
                .setBackgroundResource(R.drawable.vehicle_skeleton_tire_info);
        TextView infoText = new TextView(mContext);
        infoText.setGravity(Gravity.CENTER);
        infoText.setTag("spareaxleinfo");
        axleTireInfo.setVisibility(LinearLayout.INVISIBLE);
        axleTireInfo.addView(infoText);
        // add spare tire to the skeleton
        mVehicle.addView(spareAxle);
        ImageView arrow = getArrow(-1);
        mVehicle.addView(arrow);
        mArrowToTyreMappers.add(new ArrowToTyreMapper(arrow, spareTyres));
        mVehicle.addView(axleTireInfo);
    }

    /**
     * Add a single axle to the vehicle
     *
     * @param margintop  margin from the above axle in the vehicle
     * @param axleinfo   the information about the axle (steer, passive, driven)
     * @param axleNumber the axle position
     */
    private void createSingleAxle(int margintop, String axleinfo, int axleNumber) {
        // check all tires are available add
        // non maintained tire if not
        checkAllTiresAvailable(false, axleNumber);
        // create an axle as a linear layout
        LinearLayout axle = new LinearLayout(mContext);
        axle.setOrientation(LinearLayout.HORIZONTAL);
        LayoutParams layoutPosition = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutPosition.gravity = Gravity.CENTER;
        layoutPosition.setMargins(0, margintop, 0, 0);
        axle.setLayoutParams(layoutPosition);
        // inner ll for dx
        LinearLayout llTyre_oneD = new LinearLayout(mContext);
        LayoutParams layoutPosition_oneD = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutPosition_oneD.gravity = Gravity.CENTER_HORIZONTAL;
        llTyre_oneD.setOrientation(LinearLayout.VERTICAL);
        llTyre_oneD.setLayoutParams(layoutPosition_oneD);
        mCounter++;
        llTyre_oneD.setTag(new String(axleNumber + "-OL"));
        llTyre_oneD.setOnDragListener(new ChoiceDragListener());
        // create left-outer tire
        mTireOL = new ImageButton(mContext);
        // add inner tire
        mTireL = new ImageButton(mContext);
        mTireL.setTag(new String(axleNumber + "-OL"));
        mTireL.setOnTouchListener(new ChoiceTouchListener());
        mTireL.setBackgroundResource(R.drawable.normal_tire);
        llTyre_oneD.addView(mTireL);
		/*
		 * if (android.os.Build.VERSION.SDK_INT >
		 * android.os.Build.VERSION_CODES.KITKAT) { mTireL.setLayoutParams(new
		 * LayoutParams(130, LayoutParams.WRAP_CONTENT)); }
		 */
        axle.addView(llTyre_oneD);
        // add the axle type (steer, Driven, Passive)
        int axleType = getAxleType(axleinfo, false);
        // add tire type
        ImageButton type = new ImageButton(mContext);
        LayoutParams btnparams3;
        if ((axleType == R.drawable.passive_axle)
                || (axleType == R.drawable.passive_axle_double)) {
            btnparams3 = new LayoutParams(LayoutParams.WRAP_CONTENT, 25);
        } else {
            btnparams3 = new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
        }
        btnparams3.gravity = Gravity.CENTER;
        btnparams3.setMargins(10, 0, 10, 0); // set right margin
        type.setLayoutParams(btnparams3);
        type.setBackgroundResource(axleType);
        axle.addView(type);
        // inner ll for dx
        LinearLayout llTyre_twoD = new LinearLayout(mContext);
        LayoutParams layoutPosition_twoD = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutPosition_twoD.gravity = Gravity.CENTER_HORIZONTAL;
        llTyre_twoD.setOrientation(LinearLayout.VERTICAL);
        llTyre_twoD.setLayoutParams(layoutPosition_twoD);
        mCounter++;
        llTyre_twoD.setTag(new String(axleNumber + "-OR"));
        llTyre_twoD.setOnDragListener(new ChoiceDragListener());
        // add right tire
        mTireR = new ImageButton(mContext);
        mTireR.setTag(new String(axleNumber + "-OR"));
        mTireR.setOnTouchListener(new ChoiceTouchListener());
        mTireR.setBackgroundResource(R.drawable.normal_tire);
        llTyre_twoD.addView(mTireR);
        axle.addView(llTyre_twoD);
        int width = getResources().getDimensionPixelSize(
                R.dimen.history_view_width);
        LinearLayout axleTireInfo = new LinearLayout(mContext);
        LayoutParams axleTireInfoPosition = new LayoutParams(width,
                LayoutParams.WRAP_CONTENT);
        axleTireInfoPosition.setMargins(0, 0, 0, 0);
        axleTireInfo.setTag(axleNumber + "info");
        axleTireInfo.setOrientation(LinearLayout.VERTICAL);
        axleTireInfo.setLayoutParams(axleTireInfoPosition);
        axleTireInfo
                .setBackgroundResource(R.drawable.vehicle_skeleton_tire_info);
        TextView infoText = new TextView(mContext);
        infoText.setGravity(Gravity.CENTER);
        infoText.setTag(axleNumber + Constants.INFO_TEXT_VIEW_NAME);
        axleTireInfo.setVisibility(LinearLayout.INVISIBLE);
        axleTireInfo.addView(infoText);
        // add axle to vehicle
        mVehicle.addView(axle);
        ImageView arrow = getArrow(axleNumber);
        mVehicle.addView(arrow);
        mArrowToTyreMappers.add(new ArrowToTyreMapper(arrow, Arrays.asList(
                mTireL, mTireR)));
        mVehicle.addView(axleTireInfo);
    }

    /**
     * get axle type for current axle
     *
     * @param axleinfo current axle info
     * @param isDouble is the current axle double
     * @return drawable id for the current axle
     */
    private int getAxleType(String axleinfo, boolean isDouble) {
        if (axleinfo.contains("ST") && axleinfo.contains("DR")) {
            if (isDouble) {
                return R.drawable.steer_drive_axle;
            } else {
                return R.drawable.steer_drive_axle_single;
            }
        } else if (axleinfo.contains("ST")) {
            if (isDouble) {
                return R.drawable.steering_axle;
            } else {
                return R.drawable.steering_axle_single;
            }
        } else if (axleinfo.contains("DR")) {
            if (isDouble) {
                return R.drawable.driven_axle;
            } else {
                return R.drawable.driven_axle_single;
            }
        } else if (axleinfo.contains("LI")) {
            if (isDouble) {
                return R.drawable.lift_axle;
            } else {
                return R.drawable.lift_axle_single;
            }
        } else if (isDouble) {
            return R.drawable.passive_axle_double;
        } else {
            return R.drawable.passive_axle;
        }
    }

    /**
     * add double axle to the vehicle
     *
     * @param margintop  margin from the above axle in the vehicle
     * @param axleinfo   the information about the axle (steer, passive, driven)
     * @param axleNumber the axle position
     */
    private void createDoubleAxle(int margintop, String axleinfo, int axleNumber) {
        // check all tires are available add
        // non maintained tire if not
        checkAllTiresAvailable(true, axleNumber);
        // create a new linear layout
        LinearLayout axle = new LinearLayout(mContext);
        axle.setOrientation(LinearLayout.HORIZONTAL);
        LayoutParams layoutPosition = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutPosition.gravity = Gravity.CENTER;
        layoutPosition.setMargins(0, margintop, 0, 0);
        axle.setLayoutParams(layoutPosition);
        // inner ll for dx
        LinearLayout llTyre_oneD = new LinearLayout(mContext);
        LayoutParams layoutPosition_oneD = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        llTyre_oneD.setOrientation(LinearLayout.VERTICAL);
        llTyre_oneD.setLayoutParams(layoutPosition_oneD);
        mCounter++;
        llTyre_oneD.setTag(new String(axleNumber + "-OL"));
        llTyre_oneD.setOnDragListener(new ChoiceDragListener());
        // create left-outer tire
        mTireOL = new ImageButton(mContext);
        mTireOL.setTag(new String(axleNumber + "-OL"));
        mTireOL.setBackgroundResource(R.drawable.normal_tire);
        mTireOL.setOnTouchListener(new ChoiceTouchListener());
        llTyre_oneD.addView(mTireOL);
        axle.addView(llTyre_oneD);
        // inner ll for dx
        LinearLayout llTyre_twoD = new LinearLayout(mContext);
        LayoutParams layoutPosition_twoD = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        llTyre_twoD.setOrientation(LinearLayout.VERTICAL);
        llTyre_twoD.setLayoutParams(layoutPosition_twoD);
        mCounter++;
        llTyre_twoD.setTag(new String(axleNumber + "-IL"));
        llTyre_twoD.setOnDragListener(new ChoiceDragListener());
        // create left-inner tire
        mTireIL = new ImageButton(mContext);
        mTireIL.setBackgroundResource(R.drawable.normal_tire);
        mTireIL.setTag(new String(axleNumber + "-IL"));
        mTireIL.setOnTouchListener(new ChoiceTouchListener());
        llTyre_twoD.addView(mTireIL);
        axle.addView(llTyre_twoD);
        // add the axle type (steer, Driven, Passive)
        int axleType = getAxleType(axleinfo, true);
        // add axle type
        ImageButton type = new ImageButton(mContext);
        // LayoutParams btnparams3 = new
        // LayoutParams(LayoutParams.WRAP_CONTENT,15);
        LayoutParams btnparams3;
        if ((axleType == R.drawable.passive_axle)
                || (axleType == R.drawable.passive_axle_double)) {
            btnparams3 = new LayoutParams(LayoutParams.WRAP_CONTENT, 25);
        } else {
            btnparams3 = new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
        }
        btnparams3.gravity = Gravity.CENTER;
        btnparams3.setMargins(10, 0, 10, 0); // set right margin
        type.setLayoutParams(btnparams3);
        type.setBackgroundResource(axleType);
        axle.addView(type);
        // inner ll for dx
        LinearLayout llTyre_threeD = new LinearLayout(mContext);
        LayoutParams layoutPosition_threeD = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        llTyre_threeD.setOrientation(LinearLayout.VERTICAL);
        layoutPosition_threeD.setMargins(0, 0, 0, 0);
        llTyre_threeD.setLayoutParams(layoutPosition_threeD);
        mCounter++;
        llTyre_threeD.setTag(new String(axleNumber + "-IR"));
        llTyre_threeD.setOnDragListener(new ChoiceDragListener());
        // add Right-inner tire
        mTireIR = new ImageButton(mContext);
        mTireIR.setBackgroundResource(R.drawable.normal_tire);
        mTireIR.setTag(new String(axleNumber + "-IR"));
        mTireIR.setOnTouchListener(new ChoiceTouchListener());
        llTyre_threeD.addView(mTireIR);
        axle.addView(llTyre_threeD);
        // inner ll for dx
        LinearLayout llTyre_fourD = new LinearLayout(mContext);
        LayoutParams layoutPosition_fourD = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        llTyre_fourD.setOrientation(LinearLayout.VERTICAL);
        llTyre_fourD.setLayoutParams(layoutPosition_fourD);
        mCounter++;
        llTyre_fourD.setTag(new String(axleNumber + "-OR"));
        llTyre_fourD.setOnDragListener(new ChoiceDragListener());
        // add Right-outer tire
        mTireOR = new ImageButton(mContext);
        mTireOR.setBackgroundResource(R.drawable.normal_tire);
        mTireOR.setTag(new String(axleNumber + "-OR"));
        mTireOR.setOnTouchListener(new ChoiceTouchListener());
        llTyre_fourD.addView(mTireOR);
        axle.addView(llTyre_fourD);
        int width = getResources().getDimensionPixelSize(
                R.dimen.history_view_width);
        // axle tire info
        LinearLayout axleTireInfo = new LinearLayout(mContext);
        LayoutParams axleTireInfoPosition = new LayoutParams(width,
                LayoutParams.WRAP_CONTENT);
        axleTireInfoPosition.gravity = Gravity.CENTER;
        axleTireInfo.setOrientation(LinearLayout.VERTICAL);
        axleTireInfoPosition.setMargins(0, 0, 0, 0);
        axleTireInfo
                .setBackgroundResource(R.drawable.vehicle_skeleton_tire_info);
        axleTireInfo.setTag(axleNumber + "info");
        axleTireInfo.setLayoutParams(axleTireInfoPosition);
        TextView infoText = new TextView(mContext);
        infoText.setTag(axleNumber + Constants.INFO_TEXT_VIEW_NAME);
        infoText.setGravity(Gravity.CENTER);
        axleTireInfo.setVisibility(LinearLayout.INVISIBLE);
        axleTireInfo.addView(infoText);
        // add axle to the vehicle
        mVehicle.addView(axle);
        ImageView arrow = getArrow(axleNumber);
        mVehicle.addView(arrow);
        mArrowToTyreMappers.add(new ArrowToTyreMapper(arrow, Arrays.asList(
                mTireOL, mTireIL, mTireOR, mTireIR)));
        mVehicle.addView(axleTireInfo);
    }

    private List<ArrowToTyreMapper> mArrowToTyreMappers = new ArrayList<>();
    private EditText mUserInput;

    private ImageView getArrow(int tag) {
        ImageView imageView = new ImageView(getActivity());
        imageView.setImageResource(R.drawable.ic_popup_pointer);
        LayoutParams layoutParams = new LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        imageView.setTag("arrow_" + tag);
        imageView.setLayoutParams(layoutParams);
        imageView.setVisibility(View.GONE);
        return imageView;
    }

    /**
     * updating arrow down the selected tire
     *
     * @param tyre
     */
    private void updateArrow(ImageButton tyre) {
        for (ArrowToTyreMapper tyreMapper : mArrowToTyreMappers) {
            View arrow = tyreMapper.getArrow();
            if (tyre != null && tyreMapper.getTyres().contains(tyre)) {
                mArrowState = tyre.getTag().toString();
                arrow.setVisibility(View.VISIBLE);
                LayoutParams layoutParams = (LayoutParams) arrow
                        .getLayoutParams();
                layoutParams.leftMargin += getOffsetArrowToTyre(arrow, tyre);
                arrow.requestLayout();
            } else {
                arrow.setVisibility(View.GONE);
            }
        }
    }

    /**
     * getting Offset Arrow To Tyre
     *
     * @param arrow
     * @param tyre
     * @return
     */
    private int getOffsetArrowToTyre(View arrow, ImageButton tyre) {
        int tyreLoc[] = new int[2];
        tyre.getLocationInWindow(tyreLoc);
        int arrowLoc[] = new int[2];
        arrow.getLocationInWindow(arrowLoc);
        return tyreLoc[0] - arrowLoc[0] + tyre.getMeasuredWidth() / 3;
    }

    static class ArrowToTyreMapper {
        private View arrow;
        private List<ImageButton> tyres = new ArrayList<>();

        public ArrowToTyreMapper(View arrow, List<ImageButton> tyres) {
            this.arrow = arrow;
            this.tyres = tyres;
        }

        public View getArrow() {
            return arrow;
        }

        public void setArrow(View arrow) {
            this.arrow = arrow;
        }

        public List<ImageButton> getTyres() {
            return tyres;
        }

        public void setTyres(List<ImageButton> tyres) {
            this.tyres = tyres;
        }
    }

    /**
     * check if all tires are available in tire info
     */
    private void checkAllTiresAvailable(Boolean isDouble, int axleNumber) {
        if (isDouble) {
            String[] tirePosition = {"OL", "IL", "IR", "OR"};
            addPositionIfNotAvailable(tirePosition, axleNumber);
        } else {
            String[] tirePosition = {"OL", "OR"};
            addPositionIfNotAvailable(tirePosition, axleNumber);
        }
    }

    /**
     * add tire if not available in tire info
     */
    private void addPositionIfNotAvailable(String[] tirePosition, int axleNumber) {
        String vechicleId = null;
        String contractNoId = null;
        if (tyreInfo != null && tyreInfo.size() > 0) {
            vechicleId = tyreInfo.get(0).getVehicleJob();
            contractNoId = tyreInfo.get(0).getSapContractNumberID();
        } else {
            return;
        }
        for (String pos : tirePosition) {
            if (getTireByPosition(axleNumber + "-" + pos) == null) {
                Tyre nonMaintainedtire = new Tyre();
                nonMaintainedtire.setPosition(axleNumber + "-" + pos);
                nonMaintainedtire.setSerialNumber("");
                nonMaintainedtire.setDesign("");
                nonMaintainedtire.setDesignDetails("");
                nonMaintainedtire.setNsk("0");
                nonMaintainedtire.setNsk2("0");
                nonMaintainedtire.setNsk3("0");
                nonMaintainedtire.setPressure("0");
                nonMaintainedtire.setVehicleJob(vechicleId);
                nonMaintainedtire.setSapContractNumberID(contractNoId);
                nonMaintainedtire.setTyreState(TyreState.NON_MAINTAINED);
                nonMaintainedtire.setExternalID(String.valueOf(UUID
                        .randomUUID()));
                tyreInfo.add(nonMaintainedtire);
                LogUtil.d(
                        "VehicleSkeleton",
                        "Count tyreInfo addPositionIfNotAvailable::"
                                + tyreInfo.size());
            }
        }
    }

    /**
     * create tire position label
     *
     * @param isDouble set true if double axle present in skeleton
     */
    private void createTireLablesAxle(Boolean isDouble) {
        int marginTop = 10;
        // create a new linear layout

        Drawable drawable = getResources().getDrawable(R.drawable.normal_tire);
        // width of the label is resized dynamically as per the width of the
        // tire
        // 12% of the width for the padding of the labels is added
        int tireWidth = drawable.getIntrinsicWidth();
        tireWidth = (int) (tireWidth + (.12 * tireWidth));
        LinearLayout tirePostionLabelContainer = new LinearLayout(mContext);
        tirePostionLabelContainer.setOrientation(LinearLayout.HORIZONTAL);
        LayoutParams layoutPosition = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutPosition.gravity = Gravity.CENTER;
        layoutPosition.setMargins(0, marginTop, 0, 0);
        tirePostionLabelContainer.setLayoutParams(layoutPosition);
        // inner ll for dx
        LinearLayout tirePositionLableOL = new LinearLayout(mContext);
        // Instead of WrapContent , we are setting the width of the
        // TireImage-Button as the width of label-button
        // to ensure alignment of the tire and the labels
        LayoutParams layoutPosition_oneD = new LayoutParams(tireWidth,
                LayoutParams.WRAP_CONTENT);
        tirePositionLableOL.setOrientation(LinearLayout.HORIZONTAL);
        tirePositionLableOL.setLayoutParams(layoutPosition_oneD);
        // create left-outer tire
        Button labelOL = new Button(mContext);
        labelOL.setText("OL");
        labelOL.setBackgroundColor(Color.TRANSPARENT);
        tirePositionLableOL.addView(labelOL);
        tirePostionLabelContainer.addView(tirePositionLableOL);
        // inner ll for dx
        LinearLayout tirePositionLableIL = new LinearLayout(mContext);
        LayoutParams layoutPosition_twoD = new LayoutParams(tireWidth,
                LayoutParams.WRAP_CONTENT);
        tirePositionLableIL.setLayoutParams(layoutPosition_twoD);
        // create left-inner tire
        Button labelIL = new Button(mContext);
        labelIL.setText("IL");
        labelIL.setBackgroundColor(Color.TRANSPARENT);
        tirePositionLableIL.addView(labelIL);
        if (!isDouble) {
            tirePositionLableIL.setVisibility(LinearLayout.GONE);
        }
        tirePostionLabelContainer.addView(tirePositionLableIL);
        // add axle type
        ImageButton axleType = new ImageButton(mContext);
        LayoutParams btnparams3 = new LayoutParams(LayoutParams.WRAP_CONTENT,
                10);
        btnparams3.gravity = Gravity.CENTER;
        axleType.setLayoutParams(btnparams3);

        // if double axle is present use axle dimension of double other wise
        // take single
        if (!isDouble) {
            axleType.setBackgroundResource(R.drawable.driven_axle_single);
        } else {
            axleType.setBackgroundResource(R.drawable.driven_axle);
        }
        axleType.setVisibility(Button.INVISIBLE);
        tirePostionLabelContainer.addView(axleType);
        // inner ll for dx
        LinearLayout tirePositionLableIR = new LinearLayout(mContext);
        LayoutParams layoutPosition_threeD = new LayoutParams(tireWidth,
                LayoutParams.WRAP_CONTENT);
        layoutPosition_threeD.setMargins(0, 0, 0, 0);
        tirePositionLableIR.setLayoutParams(layoutPosition_threeD);
        // add Right-inner tire
        Button labelIR = new Button(mContext);
        labelIR.setText("IR");
        labelIR.setBackgroundColor(Color.TRANSPARENT);
        if (!isDouble) {
            tirePositionLableIR.setVisibility(LinearLayout.GONE);
            labelIR.setVisibility(Button.GONE);
        }
        tirePositionLableIR.addView(labelIR);
        tirePostionLabelContainer.addView(tirePositionLableIR);
        // inner ll for dx
        LinearLayout tirePositionLableOR = new LinearLayout(mContext);
        LayoutParams layoutPosition_fourD = new LayoutParams(tireWidth,
                LayoutParams.WRAP_CONTENT);
        tirePositionLableOR.setLayoutParams(layoutPosition_fourD);
        // add Right-outer tire
        Button labelOR = new Button(mContext);
        labelOR.setText("OR");
        labelOR.setBackgroundColor(Color.TRANSPARENT);
        tirePositionLableOR.addView(labelOR);
        tirePostionLabelContainer.addView(tirePositionLableOR);
        mVehicle.addView(tirePostionLabelContainer);
    }

    /**
     * get tire object by position
     *
     * @param position position of the tire
     * @return tire object if found else null
     */
    public static Tyre getTireByPosition(String position) {
        if (position == null) {
            return null;
        }
        VehicleSkeletonInfo vehicleconfg = new VehicleSkeletonInfo(
                Constants.vehicleConfiguration);
        vehicleconfg.getVehicleSkeletonInfo();
        int axelCount = vehicleconfg.getAxlecount();
        for (Tyre swapTire : tyreInfo) {
            // For Spare
            if (swapTire.getPosition().contains("SP")) {
                try {
                    if (swapTire.getPosition().startsWith("SP")) {
                        if ((vehicleconfg.getAxlecount() + 1) == axelCount) {
                            // Do nothing
                        } else {
                            ++axelCount;
                        }
                        System.out.println("axelCount::: " + axelCount);
                        swapTire.setPosition(axelCount + ""
                                + swapTire.getPosition());
                    } else {
                        swapTire.setPosition(swapTire.getPosition());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (swapTire.getPosition().equals(position)) {
                return swapTire;
            }
        }
        return null;
    }

    /**
     * clearing TextAreas on calling tire action
     */
    private void clearTextAreasForAction() {
        for (int j = 0; j < tyreInfo.size(); j++) {
            Tyre tire = tyreInfo.get(j);
            if (tire == null) {
                continue;
            }
            String tirePos = tire.getPosition();
            if (!tirePos.endsWith("$")) {
                String textViewTag = tirePos.charAt(0)
                        + Constants.INFO_TEXT_VIEW_NAME;
                TextView textArea = (TextView) mVehicle
                        .findViewWithTag(textViewTag);

                if (textArea == null) {
                    textArea = (TextView) mVehicle
                            .findViewWithTag("spareaxleinfo");
                }
                LinearLayout textAreaParent = (LinearLayout) textArea
                        .getParent();
                for (int k = 0; k < textAreaParent.getChildCount(); k++) {
                    if (k != 0) {
                        textAreaParent.removeViewAt(k);
                    }
                }
                textAreaParent.setVisibility(View.INVISIBLE);
            }
        }
        updateArrow(null);
        mArrowState = null;
    }

    /**
     * Method returning maximum count of Job present in the JobItem table
     *
     * @return: Max Job Count
     */
    private int getJobItemIDForThisJobItem() {
        int countJobItemsNotPresent = 0;
        DatabaseAdapter mDbHelperForID;
        mDbHelperForID = new DatabaseAdapter(mContext);
        mDbHelperForID.createDatabase();
        mDbHelperForID.open();
        int currentJobItemCountInDB = mDbHelper.getJobItemCount();
        Cursor cursor = null;
        try {
            cursor = mDbHelper.getJobItemValues();
            if (!CursorUtils.isValidCursor(cursor)) {
                return countJobItemsNotPresent + 1
                        + VehicleSkeletonFragment.mJobItemList.size();
            }
            for (JobItem jobItem : VehicleSkeletonFragment.mJobItemList) {
                for (boolean hasItem = cursor.moveToFirst(); hasItem; hasItem = cursor
                        .moveToNext()) {
                    if (jobItem.getExternalId().equals(
                            cursor.getString(cursor
                                    .getColumnIndex("ExternalID")))) {
                        countJobItemsNotPresent++;
                        break;
                    }
                }
            }
        } finally {
            mDbHelperForID.close();
            // CursorUtils.closeCursor(cursor);
        }
        return currentJobItemCountInDB - countJobItemsNotPresent
                + VehicleSkeletonFragment.mJobItemList.size() + 1;
    }

    /**
     * displaying selected tire details
     *
     * @param selectedTire
     */
    private void showTireDetails(View selectedTire) {
        try {
            if (Constants.onReturnBool == false) {

                clearTextAreasForAction();
                updateArrow((ImageButton) selectedTire);
                int totalTireCount = tyreInfo.size();
                for (int i = 0; i < totalTireCount; i++) {
                    Tyre tire = tyreInfo.get(i);
                    if (tire.getPosition().contains("SP")) {
                        try {
                            VehicleSkeletonInfo vehicleconfg = new VehicleSkeletonInfo(
                                    Constants.vehicleConfiguration);
                            vehicleconfg.getVehicleSkeletonInfo();
                            int axelCount = vehicleconfg.getAxlecount();
                            if (tire.getPosition().startsWith("SP")) {
                                if ((vehicleconfg.getAxlecount() + 1) == axelCount) {
                                    // Do nothing
                                } else {
                                    ++axelCount;
                                }
                                tire.setPosition((axelCount) + ""
                                        + tire.getPosition());
                            } else {
                                tire.setPosition(tire.getPosition());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    // map tire selected to tire array
                    if (tire.getPosition().equals(selectedTire.getTag())) {
                        try {
                            if (mLastSelected != null) {
                                if (mLastSelectedResource == R.drawable.selected_tire
                                        || mLastSelectedResource == R.drawable.normal_tire) {
                                    updatePreviousTireView();
                                    mLastSelected
                                            .setBackgroundResource(R.drawable.normal_tire);
                                } else if (mLastSelectedResource == R.drawable.dismount_tire
                                        || mLastSelectedResource == R.drawable.dismount_tire_s) {
                                    updatePreviousTireView();
                                    mLastSelected
                                            .setBackgroundResource(R.drawable.dismount_tire);
                                } else if (mLastSelectedResource == R.drawable.worked_wheel_s
                                        || mLastSelectedResource == R.drawable.worked_wheel) {
                                    updatePreviousTireView();
                                    mLastSelected
                                            .setBackgroundResource(R.drawable.worked_wheel);
                                }
                                //Fix : New Icon for Non maintained tyre with Inspection
                                else if(mLastSelectedResource == R.drawable.no_maintained_with_inspection_tire
                                        || mLastSelectedResource == R.drawable.no_maintained_with_inspection_tire_s)
                                {
                                    updatePreviousTireView();
                                    mLastSelected
                                            .setBackgroundResource(R.drawable.no_maintained_with_inspection_tire);
                                }
                                else {
                                    updatePreviousTireView();
                                    mLastSelected
                                            .setBackgroundResource(R.drawable.no_maintained_tire);
                                }
                            }
                            int selecteIcon = TireUtils.getTireIcon(tire, true);
                            // by default set normal background
                            selectedTire.setBackgroundResource(selecteIcon);
                            mLastSelectedResource = selecteIcon;
                            String textViewTag = tire.getPosition().charAt(0)
                                    + Constants.INFO_TEXT_VIEW_NAME;
                            // get details text view using tag
                            TextView textArea = (TextView) mVehicle
                                    .findViewWithTag(textViewTag);
                            if (textArea == null) {
                                textArea = (TextView) mVehicle
                                        .findViewWithTag("spareaxleinfo");
                            }
                            // get parent of details and make it visible
                            LinearLayout textAreaParent = (LinearLayout) textArea
                                    .getParent();
                            boolean isRightTyreSelected = tire.getPosition()
                                    .endsWith("R");
                            ((LayoutParams) textAreaParent.getLayoutParams()).gravity = isRightTyreSelected ? Gravity.RIGHT
                                    : Gravity.LEFT;
                            textAreaParent.setVisibility(LinearLayout.VISIBLE);
                            for (int j = 1; j < textAreaParent.getChildCount(); j++) {
                                textAreaParent.removeViewAt(j);
                            }
                            String pressureUnit = CommonUtils
                                    .getPressureUnit(mContext);
                            // set the details for the tire

                            String nskOneAfter = CommonUtils
                                    .getParsedNSKValue(tire.getNsk());
                            String nskTwoAfter = CommonUtils
                                    .getParsedNSKValue(tire.getNsk2());
                            String nskThreeAfter = CommonUtils
                                    .getParsedNSKValue(tire.getNsk3());
                            String serialNumberVal = tire.getSerialNumber();
                            String designDetailsVal = tire.getDesignDetails();
                            String brandNameVal = tire.getBrandName();
                            if (TextUtils.isEmpty(serialNumberVal)) {
                                serialNumberVal = "NA";
                            }
                            if (TextUtils.isEmpty(designDetailsVal)) {
                                designDetailsVal = "NA";
                            }
                            if (TextUtils.isEmpty(brandNameVal)) {
                                brandNameVal = "NA";
                            }
                            String tireInfoText = "<b>"
                                    + mSerialNumber
                                    + "</b> "
                                    + serialNumberVal
                                    + "  <b> "
                                    + mDesign
                                    + " </b>"
                                    + designDetailsVal
                                    + "</b> "
                                    + "<br/><b>"
                                    + mBrand
                                    + " </b>"
                                    + brandNameVal
                                    + "  <b> "
                                    + mNskOne
                                    + "</b> "
                                    + nskOneAfter
                                    + "mm"
                                    + "  <b> "
                                    + mNskTwo
                                    + "  </b> "
                                    + nskTwoAfter
                                    + "mm"
                                    + "  <b> "
                                    + mNskThree
                                    + "</b> "
                                    + nskThreeAfter
                                    + "mm"
                                    + "  <b> "
                                    + mPressure
                                    + "</b> "
                                    + CommonUtils.getPressureValue(
                                    getActivity(), tire.getPressure())
                                    + " " + pressureUnit;
                            textArea.setText(Html.fromHtml(tireInfoText));
                            if (checkIfLastOperationIsDismount(tire
                                    .getExternalID())) {
                                textArea.setVisibility(View.GONE);
                            } else {
                                textArea.setVisibility(View.VISIBLE);
                            }
                            // get pressure unit
                            mLastViewInfo = textArea;
                            mLastSelected = (ImageButton) selectedTire;
                            showTireHistory(textAreaParent, tire.getPosition());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            // boolean to select tire on Swap
            else if (Constants.onReturnBool == true) {
                for (Tyre tireTwo : tyreInfo) {
                    // map tire selected to tire array
                    String tirePos = tireTwo.getPosition();
                    if (tirePos.equals(selectedTire.getTag())) {
                        try {

                            Constants.onGOBool = true;
                            Constants.SECOND_SELECTED_TYRE = tireTwo;
                            // Fix for CR 449
                            if (CommonUtils.checkDifferentSizeForAxle()) {
                                CommonUtils.notify(mSizeNotAllowed, mContext);
                                resetBoolValue();
                                resetTyres();
                            } else {
                                if (CommonUtils.checkDifferentAxleForSwap()
                                        && !Constants.DIFFERENT_SIZE_ALLOWED) {
                                    CommonUtils.notifyLong(mSizeNotAllowed,
                                            mContext);
                                    if (Constants.INSERT_SWAP_DATA == 1) {
                                        if ((Constants.onDRAGReturnBoolPS == true)
                                                || Constants.onGOBool == true
                                                || Constants.DO_LOGICAL_SWAP) {
                                            if (mJobItemList.size() > 0) {
                                                mJobItemList
                                                        .remove(mJobItemList
                                                                .size() - 1);
                                                Constants.INSERT_SWAP_DATA = 0;
                                            }
                                            resetTyreState();
                                        }
                                    }
                                    // Handling inspection status on back press
                                    if(Constants.TyreIdListToHandlerBackPress!=null) {
                                        for (int x = 0; x < Constants.TyreIdListToHandlerBackPress.size(); x++) {
                                            if(Constants.SELECTED_TYRE.getExternalID().equalsIgnoreCase(Constants.TyreIdListToHandlerBackPress.get(x)))
                                            {
                                                Constants.SELECTED_TYRE.setIsInspected(false);
                                                break;
                                            }
                                        }
                                    }
                                    resetTyres();
                                    resetBoolValue();
                                } else if (Constants.SELECTED_TYRE != Constants.SECOND_SELECTED_TYRE) {
                                    dialogInEditMode = false;
                                    if (!Constants.ONDRAG
                                            && (Constants.SELECTED_TYRE
                                            .getSerialNumber()
                                            .equalsIgnoreCase(
                                                    Constants.EDITED_SERIAL_NUMBER) || Constants.SECOND_SELECTED_TYRE
                                            .getSerialNumber()
                                            .equalsIgnoreCase(
                                                    Constants.EDITED_SERIAL_NUMBER))) {
                                        Constants.DUPLICATE_SERIAL_ENTRY = true;
                                    }
                                    if (!checkDifferentAxle()) {
                                        if (Constants.DO_PHYSICAL_SWAP
                                                && (Constants.SELECTED_TYRE
                                                .getSerialNumber()
                                                .equalsIgnoreCase("") || Constants.SECOND_SELECTED_TYRE
                                                .getSerialNumber()
                                                .equalsIgnoreCase(""))) {
                                            loadSerialMountNewDialog();
                                        } else if (Constants.DO_LOGICAL_SWAP
                                                && (Constants.SECOND_SELECTED_TYRE
                                                .getSerialNumber()
                                                .equalsIgnoreCase(""))) {
                                            loadSerialMountNewDialog();
                                        } else if (Constants.DO_PHYSICAL_SWAP
                                                && Constants.SECOND_SELECTED_TYRE
                                                .getTyreState() == TyreState.EMPTY) {
                                            /* Bug 729 Start:Restrict swap on a dismounted tyre
                                            * Added code to cancel swap operation after a toast message*/
                                            CommonUtils.notify(
                                                    mStrNOSWAPONDISMOUNTEDTYRE,
                                                    mContext);
                                            if (Constants.INSERT_SWAP_DATA == 1) {
                                                if (mJobItemList.size() > 0) {
                                                    // Swap cancellation issue with Inspection
                                                    SharedPreferences preferences = getActivity().getSharedPreferences(
                                                            Constants.GOODYEAR_CONF, 0);
                                                    if (!TextUtils.isEmpty(preferences.getString(Constants.INSPECTION, ""))) {
                                                        Constants.INSPECTION_ENABLE = (preferences.getString(Constants.INSPECTION, ""));
                                                        if (Constants.INSPECTION_ENABLE.equalsIgnoreCase("ON")) {
                                                            mJobItemList.remove(mJobItemList
                                                                    .size() - 1);
                                                            if(!Constants.IS_TYRE_INSPECTED) {
                                                                mJobItemList.remove(mJobItemList
                                                                        .size() - 1);
                                                            }
                                                        }else{
                                                            mJobItemList.remove(mJobItemList
                                                                    .size() - 1);
                                                        }
                                                    }
                                                    Constants.INSERT_SWAP_DATA = 0;
                                                }
                                                resetTyreState();
                                                updateTireImages();
                                            }
                                            // Removing JobCorrection on cancel
                                            if (Constants.JOB_CORRECTION_COUNT_SWAP == 2) {
                                                if (VehicleSkeletonFragment.mJobcorrectionList
                                                        .size() >= 2) {
                                                    VehicleSkeletonFragment.mJobcorrectionList
                                                            .remove(VehicleSkeletonFragment.mJobcorrectionList
                                                                    .size() - 1);
                                                    VehicleSkeletonFragment.mJobcorrectionList
                                                            .remove(VehicleSkeletonFragment.mJobcorrectionList
                                                                    .size() - 1);
                                                }
                                                Constants.JOB_CORRECTION_COUNT_SWAP = 0;
                                            } else if (Constants.JOB_CORRECTION_COUNT_SWAP == 1) {
                                                if (VehicleSkeletonFragment.mJobcorrectionList
                                                        .size() >= 1) {
                                                    VehicleSkeletonFragment.mJobcorrectionList
                                                            .remove(VehicleSkeletonFragment.mJobcorrectionList
                                                                    .size() - 1);
                                                }
                                                Constants.JOB_CORRECTION_COUNT_SWAP = 0;
                                            }
                                            resetBoolValue();
                                            resetTyreState();
                                            /*int jobID = getJobItemIDForThisJobItem();
                                            UpdateSwapJobItem
                                                    .getMyInstance(mContext)
                                                    .updateJobItemForEmptyTire(
                                                            Constants.SECOND_SELECTED_TYRE,
                                                            jobID);
                                            loadSwapFinalConfirmation();*/
                                            /*Bug 729 : End of code changes*/
                                        } else if (Constants.DO_LOGICAL_SWAP
                                                && Constants.SECOND_SELECTED_TYRE
                                                .getTyreState() == TyreState.EMPTY) {
                                            /* Bug 729 Start:Restrict swap on a dismounted tyre
                                            * Added code to cancel swap operation after a toast message*/
                                            CommonUtils.notify(
                                                    mStrNOSWAPONDISMOUNTEDTYRE,
                                                    mContext);
                                            if (Constants.TyreIdListToHandlerBackPress != null) {
                                                for (int x = 0; x < Constants.TyreIdListToHandlerBackPress.size(); x++) {
                                                    if (Constants.SELECTED_TYRE.getExternalID().equalsIgnoreCase(Constants.TyreIdListToHandlerBackPress.get(x))) {
                                                        Constants.SELECTED_TYRE.setIsInspected(false);
                                                        break;
                                                    }
                                                }
                                            }
                                            resetBoolValue();
                                            resetTyreState();
                                            /*int jobID = getJobItemIDForThisJobItem();
                                            UpdateSwapJobItem
                                                    .getMyInstance(mContext)
                                                    .updateJobItemForEmptyTire(
                                                            Constants.SELECTED_TYRE,
                                                            jobID);
                                            UpdateSwapJobItem
                                                    .getMyInstance(mContext)
                                                    .updateJobItemForEmptyTire(
                                                            Constants.SECOND_SELECTED_TYRE,
                                                            jobID);
                                            loadSwapFinalConfirmation();*/
                                            /*Bug 729 End : code changes*/
                                        } else {
                                            loadSerialCorrectionDialog();
                                        }
                                    } else {
                                        if (Constants.INSERT_SWAP_DATA == 1) {
                                            if (mJobItemList.size() > 0) {
                                                mJobItemList
                                                        .remove(mJobItemList
                                                                .size() - 1);
                                            }
                                            resetTyreState();
                                        }
                                        CommonUtils.notify(mSizeNotAllowed,
                                                mContext);
                                        // Handling inspection status on back press
                                        if(Constants.TyreIdListToHandlerBackPress!=null) {
                                            for (int x = 0; x < Constants.TyreIdListToHandlerBackPress.size(); x++) {
                                                if(Constants.SELECTED_TYRE.getExternalID().equalsIgnoreCase(Constants.TyreIdListToHandlerBackPress.get(x)))
                                                {
                                                    Constants.SELECTED_TYRE.setIsInspected(false);
                                                    break;
                                                }
                                            }
                                        }
                                        resetBoolValue();
                                        resetTyres();
                                    }
                                } else {
                                    CommonUtils.notify(
                                            mStrCHOOSEDIFFERENTTYRETOSWAP,
                                            mContext);
                                }
                            }
                            break;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * update previous selected tire image and details
     */
    private void updatePreviousTireView() {
        if (mLastViewInfo != null) {
            LinearLayout textAreaParent = (LinearLayout) mLastViewInfo
                    .getParent();
            textAreaParent.setVisibility(LinearLayout.INVISIBLE);
            mLastViewInfo.setText("");
        }
    }

    /**
     * get tire by tire external id
     *
     * @param extId external id of the tire
     * @return tire object if tire found, otherwise null
     */
    private Tyre getTireByTyreId(String extId) {
        if (extId.equals("") || extId == null) {
            return null;
        }
        for (Tyre tire : tyreInfo) {
            if (tire.getExternalID().equals(extId)) {
                return tire;
            }
        }
        return null;
    }

    /**
     * Opening camera captured images to preview
     *
     * @param jobItemId
     */
    private void openCameraPreview(int jobItemId) {
        LogUtil.TraceInfo(TRACE_INFO, "Tire History","Camera view",false,true,false);
        Intent cameraIntent = new Intent(mContext, CameraPreviewActivity.class);
        cameraIntent.putExtra("FROM_HISTORY", true);
        cameraIntent.putExtra("JOBITEMID", jobItemId);
        startActivity(cameraIntent);
    }

    /**
     * Displaying history of the selected tire
     *
     * @param infoArea
     * @param position
     */
    private void showTireHistory(LinearLayout infoArea, final String position) {
        LogUtil.TraceInfo(TRACE_INFO, "Tire History","Position : " + position,false,true,false);
        if ((null == mJobItemList || 0 == mJobItemList.size())
                && Constants.JOB_ITEMLIST != null) {
            mJobItemList = Constants.JOB_ITEMLIST;
        }
        mHistoryCount = 0;
        final String tireServices = mDbHelper.getLabel(114);
        final String axleServices = mDbHelper.getLabel(146);
        final LinearLayout historyArea = new LinearLayout(mContext);
        historyArea.setOrientation(LinearLayout.VERTICAL);
        LayoutInflater li_action = LayoutInflater.from(mContext);
        View historyHeader = li_action.inflate(
                R.layout.vehicle_skeleton_tyre_details, null);
        final TextView historyLabel = (TextView) historyHeader
                .findViewById(R.id.tyre_history_label);
        final ImageView left_arrow = (ImageView) historyHeader
                .findViewById(R.id.left_arrow);
        final ImageView right_arrow = (ImageView) historyHeader
                .findViewById(R.id.right_arrow);
        LayoutInflater li_services = LayoutInflater.from(mContext);
        final View historyServices = li_services.inflate(
                R.layout.vehicle_history_full_details, null);
        historyServices.setVisibility(View.GONE);
        final TextView history_service_details = (TextView) historyServices
                .findViewById(R.id.history_service_details);
        final ImageView camera_view = (ImageView) historyServices
                .findViewById(R.id.camera_view);
        camera_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.TraceInfo(TRACE_INFO, "Tire History","Camera View",false,false,false);
                openCameraPreview(checkTireImages(position, "-1"));
            }
        });
        historyLabel.setText(Html.fromHtml("<b>"
                + mDbHelper.getLabel(Constants.TIRE_HISTORY) + "</b>"));
        final String wotInfoText = populateWOTData(position);
        final String axleInfoText = populateAxleData(position);
        historyArea.removeAllViews();
        historyArea.setVisibility(View.GONE);
        historyArea.addView(historyHeader);
        populateHistoryData(historyArea, position);
        populateRetorqueSettings(historyArea, position);
        if (historyArea.getChildCount() > 0) {
            if (historyArea.getChildCount() > 1
                    || !axleInfoText.equals("")
                    || (!TextUtils.isEmpty(wotInfoText) && wotInfoText.trim()
                    .length() > 0)) {
                historyArea.setVisibility(View.VISIBLE);
                infoArea.addView(historyArea);
            }
        }
        right_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.TraceInfo(TRACE_INFO, "Tire History","Right Arrow",false,false,false);
                if (mIsNotesInEditMode) {
                    CommonUtils.notifyLong(mSaveNoteMessage, mContext);
                    return;
                }
                mHistoryCount++;
                left_arrow.setVisibility(View.VISIBLE);
                historyServices.setVisibility(View.VISIBLE);
                switch (mHistoryCount) {
                    case 1:
                        String wotInfoForCurrentJobOperation = wotInfoText;
                        // check if images available populateWOTData() adds offset
                        // to wotInfoText
                        // if images are available
                        if (wotInfoForCurrentJobOperation.contains("^")) {
                            String[] wotinfo = wotInfoForCurrentJobOperation
                                    .split("\\^");
                            try {
                                final int wotJobIDForImage = Integer
                                        .parseInt(wotinfo[0]);
                                camera_view.setVisibility(View.VISIBLE);
                                camera_view
                                        .setOnClickListener(new OnClickListener() {

                                            @Override
                                            public void onClick(View v) {
                                                LogUtil.TraceInfo(TRACE_INFO, "Tire History","Right Arrow - Camera View",false,false,false);
                                                if (wotJobIDForImage >= 0) {
                                                    openCameraPreview(wotJobIDForImage);
                                                }
                                            }
                                        });
                            } catch (NumberFormatException e) {
                            }
                            if (wotinfo.length != 1) {
                                wotInfoForCurrentJobOperation = wotinfo[1];
                            }

                        } else {
                            camera_view.setVisibility(View.GONE);
                        }
                        historyLabel.setText(Html.fromHtml("<b>" + tireServices
                                + "</b>"));
                        historyArea.addView(historyServices, 1);
                        if (wotInfoForCurrentJobOperation.equals("")) {
                            history_service_details.setVisibility(View.GONE);
                        } else {
                            history_service_details.setVisibility(View.VISIBLE);
                        }
                        history_service_details.setText(Html
                                .fromHtml(wotInfoForCurrentJobOperation));
                        for (int i = 2; i <= historyArea.getChildCount(); i++) {
                            View childView = historyArea.getChildAt(i);
                            if (childView != null) {
                                childView.setVisibility(View.GONE);
                            }
                        }

                        break;
                    case 2:
                        historyLabel.setText(Html.fromHtml("<b>" + axleServices
                                + "</b>"));
                        right_arrow.setVisibility(View.INVISIBLE);
                        historyArea.removeViewAt(1);
                        historyArea.addView(historyServices, 1);
                        if (axleInfoText.equals("")) {
                            history_service_details.setVisibility(View.GONE);
                        } else {
                            history_service_details.setVisibility(View.VISIBLE);
                        }
                        history_service_details.setText(Html.fromHtml(axleInfoText));
                        for (int i = 2; i <= historyArea.getChildCount(); i++) {
                            View childView = historyArea.getChildAt(i);
                            if (childView != null) {
                                childView.setVisibility(View.GONE);
                            }
                        }
                        camera_view.setVisibility(View.GONE);
                        break;
                }
            }
        });
        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.TraceInfo(TRACE_INFO, "Tire History","Left Arrow",false,false,false);
                mHistoryCount--;
                switch (mHistoryCount) {
                    case 0:
                        historyLabel.setText(Html.fromHtml("<b>"
                                + mDbHelper.getLabel(Constants.TIRE_HISTORY)
                                + "</b>"));
                        left_arrow.setVisibility(View.INVISIBLE);
                        for (int i = 2; i <= historyArea.getChildCount(); i++) {
                            View childView = historyArea.getChildAt(i);
                            if (childView != null) {
                                childView.setVisibility(View.VISIBLE);
                            }
                        }
                        historyArea.removeViewAt(1);
                        camera_view.setVisibility(View.GONE);
                        break;
                    case 1:
                        String wotInfoForCurrentJobOperation = wotInfoText;
                        // check if images available populateWOTData() adds offset
                        // to wotInfoText
                        // if images are available
                        if (wotInfoForCurrentJobOperation.contains("^")) {
                            String[] wotinfo = wotInfoForCurrentJobOperation
                                    .split("\\^");
                            try {
                                final int wotJobIDForImage = Integer
                                        .parseInt(wotinfo[0]);
                                camera_view.setVisibility(View.VISIBLE);
                                camera_view
                                        .setOnClickListener(new OnClickListener() {

                                            @Override
                                            public void onClick(View v) {
                                                if (wotJobIDForImage >= 0) {
                                                    openCameraPreview(wotJobIDForImage);
                                                }
                                            }
                                        });
                            } catch (NumberFormatException e) {
                            }
                            if (wotinfo.length != 1) {
                                wotInfoForCurrentJobOperation = wotinfo[1];
                            }

                        } else {
                            camera_view.setVisibility(View.GONE);
                        }
                        historyLabel.setText(Html.fromHtml("<b>" + tireServices
                                + "</b>"));
                        historyArea.removeViewAt(1);
                        historyArea.addView(historyServices, 1);
                        if (wotInfoForCurrentJobOperation.equals("")) {
                            history_service_details.setVisibility(View.GONE);
                        } else {
                            history_service_details.setVisibility(View.VISIBLE);
                        }
                        right_arrow.setVisibility(View.VISIBLE);
                        history_service_details.setText(Html
                                .fromHtml(wotInfoForCurrentJobOperation));
                        for (int i = 2; i <= historyArea.getChildCount(); i++) {
                            View childView = historyArea.getChildAt(i);
                            if (childView != null) {
                                childView.setVisibility(View.GONE);
                            }
                        }
                        // camera_view.setVisibility(View.GONE);
                        break;
                }
            }
        });
    }

    /**
     * handling images on dismount
     *
     * @param position
     * @return
     */
    private int checkTireImages(String position, String mJobItemID) {
        // CR 457: passing db object to check images in db
        boolean isImageAvailable = TireImageHelpers.checkIfImageExistsWithID(
                mTireImageItemList, Integer.parseInt(mJobItemID), mDbHelper);
        if (isImageAvailable) {
            return Integer.parseInt(mJobItemID);
        } else {
            return -1;
        }
    }

    /**
     * check if image is available for the current job item
     *
     * @param position position
     * @param wotInfo  WOT info text to be displayed
     * @param index    index of the current job item
     * @return WOT info text with job item's imageid
     */
    private String addImageInfoToWot(String position, String wotInfo, int index) {
        int imageID = checkTireImages(position, mJobItemList.get(index)
                .getJobId());
        if (imageID >= 0) {
            if (wotInfo.contains("^")) {
                String[] wotArrWithImageid = wotInfo.split("\\^");
                wotInfo = imageID + "^" + wotArrWithImageid[1];
            } else {
                wotInfo = imageID + "^" + wotInfo;
            }
        }
        return wotInfo;
    }

    /**
     * displaying history for WOT
     *
     * @param position
     * @return
     */
    private String populateWOTData(String position) {
        String wotInfoText = "";
        for (int i = 0; i < mJobItemList.size(); i++) {
            if (getTireByPosition(position).getExternalID().equals(
                    mJobItemList.get(i).getTyreID())) {
                String jobActionType = mJobItemList.get(i).getActionType();
                // check if current job item has images
                // if yes then add this job item with wot info with "^" as
                // offset
                switch (jobActionType) {
                    case "9":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(373)
                                + "</b> " + "<br/>";
                        break;
                    case "10":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(374)
                                + "</b> " + "<br/>";
                        break;
                    case "12":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(376)
                                + "</b> " + "<br/>";
                        break;
                    case "23":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(255)
                                + "</b> " + "<br/>";
                        break;
                    case "13":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(250)
                                + "</b> " + "<br/>";
                        break;
                    case "14":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(251)
                                + "</b> " + "<br/>";
                        break;
                    case "15":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(252)
                                + "</b> " + "<br/>";
                        break;
                    case "16":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(253)
                                + "</b> " + "<br/>";
                        break;
                    case "21":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(259)
                                + "</b> " + "<br/>";
                        break;
                    case "19":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(257)
                                + "</b> " + "<br/>";
                        break;
                    case "22":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(260)
                                + "</b> " + "<br/>";
                        break;
                    case "18":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(256)
                                + "</b> " + "<br/>";
                        break;
                    case "17":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(254)
                                + "</b> " + "<br/>";
                        break;
                    case "20":
                        wotInfoText = addImageInfoToWot(position, wotInfoText, i);
                        wotInfoText = wotInfoText + "<b>" + mDbHelper.getLabel(258)
                                + "</b> " + "<br/>";
                        break;
                }
            }
        }
        return wotInfoText;
    }

    public void loadHistoryDetails(View view) {

        System.out
                .println("Amittttttttttttttttttttttttttttttttt+++++++++++++++");
    }

    /**
     * maintaining data related to axle for history
     *
     * @param position
     * @return
     */
    private String populateAxleData(String position) {
        String axleInfoText = "";
        int axlePos = Character.getNumericValue(position.charAt(0));
        String axleServiceId = "";
        for (int i = 0; i < mJobItemList.size(); i++) {
            if (mJobItemList.get(i).getActionType().equals("7")) {
                if (mJobItemList.get(i).getAxleNumber()
                        .equals(String.valueOf(axlePos))) {
                    axleServiceId = mJobItemList.get(i).getAxleServiceType();
                }
            }
        }
        if (!TextUtils.isEmpty(axleServiceId)) {
            VehicleSkeletonInfo vehicleconfg = new VehicleSkeletonInfo(
                    Constants.vehicleConfiguration);
            String[] axleinfo = vehicleconfg.getVehicleSkeletonInfo();
            String axleType = axleinfo[axlePos - 1];
            if (axleServiceId.equals("0")) {
                axleServiceId = mDbHelper.getLabel(300);
            } else if (axleServiceId.equals("1")) {
                axleServiceId = mDbHelper.getLabel(192);
            } else if (axleServiceId.equals("2")) {
                axleServiceId = mDbHelper.getLabel(193);
            }
            if (axleType.equals("ST")) {
                axleType = mDbHelper.getLabel(264);
            } else if (axleType.equals("DR")) {
                axleType = mDbHelper.getLabel(261);
            } else if (axleType.equals("LT")) {
                axleType = mDbHelper.getLabel(262);
            } else if (axleType.equals("DU")) {
                axleType = mDbHelper.getLabel(263);
            } else {
                axleType = mDbHelper.getLabel(263);
            }
            axleInfoText = "<b>" + mDbHelper.getLabel(137) + "</b> "
                    + "\t\t<b> " + mDbHelper.getLabel(104) + "</b> " + "<br/> "
                    + "<b>" + axleType + "</b> " + "\t\t<b> " + axleServiceId
                    + "</b> ";
        }
        return axleInfoText;
    }

    /**
     * displaying Retorque data on history
     *
     * @param historyArea
     * @param position
     */
    private void populateRetorqueSettings(LinearLayout historyArea,
                                          String position) {
        if (null == Constants.TORQUE_SETTINGS_ARRAY) {
            return;
        }
        Tyre tire = getTireByPosition(position);
        if (tire != null && tire.isRetorqued()) {
            LayoutInflater li_action = LayoutInflater.from(mContext);
            View historyItem = li_action
                    .inflate(R.layout.vehicle_history, null);
            historyItem.setPadding(6, 0, 6, 0);
            TextView history_action_name = (TextView) historyItem
                    .findViewById(R.id.history_action_name);
            final RelativeLayout history_action_details = (RelativeLayout) historyItem
                    .findViewById(R.id.history_action_details);
            TextView details_tire = (TextView) historyItem
                    .findViewById(R.id.tire_details);
            final ImageView notes_icon = (ImageView) historyItem
                    .findViewById(R.id.history_action_notes_icon);
            notes_icon.setVisibility(View.GONE);
            history_action_name.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (history_action_details.isShown()) {
                        history_action_details.setVisibility(View.GONE);
                    } else {
                        history_action_details.setVisibility(View.VISIBLE);
                    }
                }
            });
            TorqueSettings torqueForCurrentTire = null;
            // find the torque setting for the pos
            // and check if wheel was removed
            for (TorqueSettings torque : Constants.TORQUE_SETTINGS_ARRAY) {
                if (position.equals(torque.getWheelPos())
                        && 1 == torque.getIsWheelRemoved()) {
                    torqueForCurrentTire = torque;
                    break;
                }
            }
            if (null == torqueForCurrentTire) {
                return;
            }
            String retroque_tire_details = "";
            if (!TextUtils.isEmpty(torqueForCurrentTire
                    .getNoManufacturerTorqueSettingsReason())) {
                history_action_name.setText(mDbHelper.getLabel(113));
                retroque_tire_details = "<b>"
                        + mDbHelper.getLabel(70)
                        + "</b> "
                        + "\t<b> "
                        + torqueForCurrentTire
                        .getNoManufacturerTorqueSettingsReason()
                        + "</b> " + "<br/>";
                details_tire.setText(Html.fromHtml(retroque_tire_details));
                historyArea.addView(historyItem);
            } else {
                String retorqueLabelIssued = String
                        .valueOf(torqueForCurrentTire.getRetorqueLabelIssued());
                if (retorqueLabelIssued.equals("1")) {
                    retorqueLabelIssued = mDbHelper.getLabel(331);
                } else {
                    retorqueLabelIssued = mDbHelper.getLabel(332);
                }
                history_action_name.setText(mDbHelper.getLabel(113));
                retroque_tire_details = "<b>"
                        + mDbHelper.getLabel(388)
                        + "</b> "
                        + "\t<b> "
                        + String.valueOf(torqueForCurrentTire
                        .getTorqueSettingsValue()) + "</b> " + "<br/>"
                        + "<b>" + mDbHelper.getLabel(236) + "</b> " + "\t<b> "
                        + retorqueLabelIssued + "</b> ";

                if (!TextUtils.isEmpty(torqueForCurrentTire.getWrenchNumber())) {
                    retroque_tire_details += "<br/><b>"
                            + mDbHelper.getLabel(314) + "</b> " + "\t<b> "
                            + torqueForCurrentTire.getWrenchNumber() + "</b> "
                            + "<br/>" + "<b>";
                }
                details_tire.setText(Html.fromHtml(retroque_tire_details));
                historyArea.addView(historyItem);
            }
        }
    }

    public String NoteEditTag = "";

    /**
     * returning whether user is in edit mode of NOTE
     *
     * @param historyArea
     * @param position
     * @return
     */
    public boolean checkNoteEdit(LinearLayout historyArea, String position,
                                 int actionType) {
        // is isNotEqual is true then show note views else don't show.
        boolean isNotEqual = false;
        ViewGroup viewGroup = (ViewGroup) historyArea;
        for (int i = 0; i < historyArea.getChildCount(); i++) {
            View child = viewGroup.getChildAt(i);
            if (child instanceof ViewGroup) {
                if (child.getTag() != null) {
                    String tag = child.getTag().toString();
                    if (tag.equalsIgnoreCase(position)) {
                        View history_action_details = (View) child
                                .findViewById(R.id.history_action_details);

                        View history_action_notes = (View) child
                                .findViewById(R.id.history_action_notes);
                        View notes_icon = (View) child
                                .findViewById(R.id.history_action_notes_icon);

                        View details_notes = (View) child
                                .findViewById(R.id.details_notes);
                        history_action_details.setVisibility(View.VISIBLE);
                        history_action_notes.setVisibility(View.GONE);
                        notes_icon.setVisibility(View.VISIBLE);

                        View details_notes_edit = (View) child
                                .findViewById(R.id.details_notes_edit);
                        String str = ((TextView) details_notes).getText()
                                .toString();
                        mCurentNote = ((TextView) details_notes_edit).getText()
                                .toString();
                        if (!str.equalsIgnoreCase(mCurentNote.trim())) {
                            isNotEqual = false;
                            if (position.indexOf("_") > -1) {
                                System.out.println("position.indexOf()::: "
                                        + position.indexOf("_"));
                                itr_position = Integer.parseInt(position
                                        .substring(position.indexOf("_") + 1));
                            }
                            showAlertForNoteCancle(((TextView) details_notes)
                                            .getText().toString(), mCurentNote,
                                    itr_position, actionType);
                        }/*
						 * else if(mIsNoteDialogOrientationChange){ //This is
						 * for if mIsNotesInEditMode is true and coming from
						 * orientation change and if the dialog is not shown
						 * notes_edit_icon.performClick(); }
						 */ else {
                            isNotEqual = true;
                            mIsNotesInEditMode = false;
                            mIsNoteDialogOrientationChange = false;
                        }
                        break;
                    }
                }
            }
        }
        return isNotEqual;
    }

    /**
     * Displaying the edited NOTE
     *
     * @param historyArea
     * @param position
     */
    public void showNoteEdit(LinearLayout historyArea, String position) {
        ViewGroup viewGroup = (ViewGroup) historyArea;
        for (int i = 0; i < historyArea.getChildCount(); i++) {
            View child = viewGroup.getChildAt(i);
            if (child instanceof ViewGroup) {
                if (child.getTag() != null) {
                    String tag = child.getTag().toString();
                    if (tag.equalsIgnoreCase(position)) {
                        View notes_edit_icon = (View) child
                                .findViewById(R.id.notes_edit_icon);
                        View details_notes_edit = (View) child
                                .findViewById(R.id.details_notes_edit);

                        ((TextView) details_notes_edit).setText(mCurentNote);
                        notes_edit_icon.performClick();
                        break;
                    }
                }
            }
        }
    }

    int itr_position;
    int mJobActionType;

    /**
     * Populating data on History
     *
     * @param historyArea
     * @param position
     */
    private void populateHistoryData(final LinearLayout historyArea,
                                     final String position) {
        for (int i = 0; i < mJobItemList.size(); i++) {
            // itr_position = i;
            JobItem jobItem = mJobItemList.get(i);
            final int actionType = Integer.parseInt(jobItem.getActionType());
            String pos = "";
            if (actionType == TireActionType.REMOVE_INT || TireActionType.MOUNTNEW_INT == actionType || TireActionType.SWAP_INT == actionType
                    || TireActionType.REGROOVE_INT == actionType || TireActionType.TOR_INT == actionType || TireActionType.MOUNTPW_INT == actionType || TireActionType.INSPECTION_INT == actionType) {
                Tyre tire = getTireByTyreId(jobItem.getTyreID());
                if (tire == null) {
                    return;
                }
                pos = tire.getPosition();

                /* Bug 683 : Remove history of a dismounted tyre */
                boolean showJobItem = true;
                if(tire.isDismounted() && !jobItem.getActionType().equals("1"))
                {
                    showJobItem = false;
                }

                if (pos.endsWith("$")) {
                    pos = pos.replace("$", "");
                }
                if (pos.equals(position)) {
                    if(showJobItem) {
                        LayoutInflater li_action = LayoutInflater.from(mContext);
                        String notesFromAction = jobItem.getNote();
                        final int jobItem_indx = i;
                        itr_position = i;
                        mJobActionType = actionType;
                        String regrooveType = "";
                        String regrooved = "";
                        String rimType = "";
                        //Temp String to update temperature and recPressure
                        String temperature = "";
                        String recPressure = "0";
                        String originalPressure = "0";
                        if ("1".equals(jobItem.getRegrooveType())) {
                            regrooveType = mDbHelper
                                    .getLabel(Constants.REGROOVE_RIB_LABEL);
                        } else if ("2".equals(jobItem.getRegrooveType())) {
                            regrooveType = mDbHelper
                                    .getLabel(Constants.REGROOVE_BLOCK_LABEL);
                        }
                        // check re grooved
                        if (!TextUtils.isEmpty(jobItem.getRegrooved())) {
                            if (Boolean.parseBoolean(jobItem.getRegrooved())) {
                                regrooved = mDbHelper.getLabel(Constants.YES_LABEL);
                            } else {
                                regrooved = mDbHelper.getLabel(Constants.NO_LABEL);
                            }
                        }
                        // check rim type
                        if (!TextUtils.isEmpty(jobItem.getRimType())) {
                            if ("1".equals(jobItem.getRimType())) {
                                rimType = mDbHelper.getLabel(Constants.STEEL);
                            } else if ("2".equals(jobItem.getRimType())) {
                                rimType = mDbHelper.getLabel(Constants.ALLOY);
                            }
                        }
                        // check temperature whether hot or cold
                        if (!TextUtils.isEmpty(mJobItemList.get(i).getTemperature())) {
                            if ("1".equals(mJobItemList.get(i).getTemperature())) {
                                temperature = mTyreHOT;
                            } else {
                                temperature = mTyreCOLD;
                            }
                        }

                        // check Rec Pressure (ignore if it is 99.99)
                        if (!TextUtils.isEmpty(mJobItemList.get(i).getRectPressure())) {
                            if (!mJobItemList.get(i).getRectPressure().equalsIgnoreCase(Constants.DEFAULT_INSPECTION_PRESSURE_VALUE)) {
                                //setting rec pressure value based on units
                                recPressure = CommonUtils.getPressureValue(mContext, mJobItemList.get(i).getRectPressure());
                            }

                        }

                        // check original Pressure (ignore if it is 99.99)
                        if (!TextUtils.isEmpty(mJobItemList.get(i).getPressure())) {
                            if (!mJobItemList.get(i).getPressure().equalsIgnoreCase(Constants.DEFAULT_INSPECTION_PRESSURE_VALUE)) {
                                //setting Original pressure value based on units
                                originalPressure = CommonUtils.getPressureValue(mContext, mJobItemList.get(i).getPressure());
                            }
                        }


                        final View historyItem = li_action.inflate(
                                R.layout.vehicle_history, null);
                        String historyTag = position + "_" + i;
                        historyItem.setTag(historyTag);
                        historyItem.setPadding(6, 0, 6, 0);
                        TextView history_action_name = (TextView) historyItem
                                .findViewById(R.id.history_action_name);
                        final RelativeLayout history_action_details = (RelativeLayout) historyItem
                                .findViewById(R.id.history_action_details);
                        final RelativeLayout history_action_notes = (RelativeLayout) historyItem
                                .findViewById(R.id.history_action_notes);
                        TextView details_tire = (TextView) historyItem
                                .findViewById(R.id.tire_details);
                        final TextView details_notes = (TextView) historyItem
                                .findViewById(R.id.details_notes);
                        details_notes.setText(notesFromAction);
                        ImageView tyreImage = (ImageView) historyItem
                                .findViewById(R.id.tyre_image_icon);
                        // if ("1".equals(jobItem.getActionType())) {
                        final int dismountedImgId = checkTireImages(position,
                                jobItem.getJobId());
                        boolean hasImages = dismountedImgId > 0;
                        tyreImage.setVisibility(hasImages ? View.VISIBLE
                                : View.GONE);
                        tyreImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                openCameraPreview(dismountedImgId);
                            }
                        });
                        final EditText details_notes_edit = (EditText) historyItem
                                .findViewById(R.id.details_notes_edit);
                        details_notes_edit
                                .addTextChangedListener(new TextWatcher() {

                                    @Override
                                    public void onTextChanged(CharSequence s,
                                                              int start, int before, int count) {
                                    }

                                    @Override
                                    public void beforeTextChanged(CharSequence s,
                                                                  int start, int count, int after) {
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        mCurentNote = s.toString();
                                    }
                                });
                        final ImageView notes_icon = (ImageView) historyItem
                                .findViewById(R.id.history_action_notes_icon);
                        final ImageView notes_edit_icon = (ImageView) historyItem
                                .findViewById(R.id.notes_edit_icon);
                        final ImageView notes_ok_icon = (ImageView) historyItem
                                .findViewById(R.id.notes_ok_icon);
                        final ImageView notes_arrow_icon = (ImageView) historyItem
                                .findViewById(R.id.notes_arrow_icon);
                        TextView details_notes_label = (TextView) historyItem
                                .findViewById(R.id.details_notes_label);
                        // This is for orientation change
                        notes_edit_icon
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        LogUtil.TraceInfo(TRACE_INFO, "Tire History", "Edit Note", false, true, false);
                                        if (mIsNotesInEditMode
                                                && mIsNoteDialogOrientationChange) {
                                            history_action_notes
                                                    .setVisibility(View.VISIBLE);
                                            CommonUtils
                                                    .setFocus(details_notes_edit);
                                            details_notes.setVisibility(View.GONE);
                                            details_notes_edit
                                                    .setVisibility(View.VISIBLE);
                                            mCurentNote = details_notes_edit
                                                    .getText().toString();
                                            notes_edit_icon
                                                    .setVisibility(View.INVISIBLE);
                                            notes_ok_icon
                                                    .setVisibility(View.VISIBLE);
                                            notes_icon
                                                    .setVisibility(View.INVISIBLE);

                                        } else if (mIsNotesInEditMode) {
                                            checkNoteEdit(historyArea, NoteEditTag,
                                                    actionType);
                                        } else {
                                            mIsNoteDialogOrientationChange = true;
                                            mIsNotesInEditMode = true;
                                            NoteEditTag = historyItem.getTag()
                                                    .toString();

                                            CommonUtils
                                                    .setFocus(details_notes_edit);
                                            details_notes.setVisibility(View.GONE);
                                            details_notes_edit
                                                    .setVisibility(View.VISIBLE);
                                            details_notes_edit
                                                    .setText(details_notes
                                                            .getText().toString());
                                            mCurentNote = details_notes_edit
                                                    .getText().toString();
                                            notes_edit_icon
                                                    .setVisibility(View.INVISIBLE);
                                            notes_ok_icon
                                                    .setVisibility(View.VISIBLE);
                                        }
                                    }
                                });
                        notes_ok_icon
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mIsNotesInEditMode = false;
                                        mCurentNote = "";
                                        NoteEditTag = "";
                                        hideKeyboard();
                                        details_notes.setVisibility(View.VISIBLE);
                                        details_notes_edit.setVisibility(View.GONE);
                                        details_notes.setText(details_notes_edit
                                                .getText().toString());
                                        updateNotesInJobList(jobItem_indx,
                                                actionType, details_notes_edit
                                                        .getText().toString());
                                        notes_ok_icon.setVisibility(View.INVISIBLE);
                                        notes_edit_icon.setVisibility(View.VISIBLE);
                                    }
                                });
                        notes_icon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                boolean isNotEqual = true;
                                if (mIsNotesInEditMode) {
                                    isNotEqual = checkNoteEdit(historyArea,
                                            NoteEditTag, actionType);
                                }
                                if (isNotEqual) {
                                    history_action_details.setVisibility(View.GONE);
                                    history_action_notes
                                            .setVisibility(View.VISIBLE);
                                    notes_icon.setVisibility(View.INVISIBLE);
                                    details_notes.setText(getNotes(jobItem_indx,
                                            actionType));
                                    if (Constants.COMESFROMVIEW) {
                                        notes_edit_icon.setVisibility(View.GONE);
                                        details_notes.setVisibility(View.VISIBLE);
                                        notes_ok_icon.setVisibility(View.INVISIBLE);
                                        details_notes_edit.setVisibility(View.GONE);
                                    } else {
                                        notes_edit_icon.setVisibility(View.VISIBLE);
                                        notes_edit_icon.performClick();
                                    }
                                }
                            }
                        });
                        notes_arrow_icon
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (mIsNotesInEditMode
                                                && historyItem
                                                .getTag()
                                                .toString()
                                                .equalsIgnoreCase(
                                                        NoteEditTag)) {
                                            mCurentNote = details_notes_edit
                                                    .getText().toString();
                                            showAlertForNoteCancle(details_notes
                                                            .getText().toString(),
                                                    mCurentNote, jobItem_indx,
                                                    actionType);
                                        }
                                        hideKeyboard();
                                        history_action_details
                                                .setVisibility(View.VISIBLE);
                                        history_action_notes
                                                .setVisibility(View.GONE);
                                        notes_icon.setVisibility(View.VISIBLE);
                                    }
                                });
                        history_action_name
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        if (mIsNotesInEditMode) {
                                            checkNoteEdit(historyArea, NoteEditTag,
                                                    actionType);
                                        }
                                        notes_icon.setVisibility(View.VISIBLE);
                                        if (history_action_details.isShown()) {
                                            history_action_details
                                                    .setVisibility(View.GONE);
                                        } else {
                                            history_action_details
                                                    .setVisibility(View.VISIBLE);
                                            history_action_notes
                                                    .setVisibility(View.GONE);
                                        }
                                    }
                                });
                        switch (actionType) {
                            case 1:// Dismount
                                history_action_name.setText(mDbHelper.getLabel(174));
                                details_notes_label.setText(mDbHelper
                                        .getLabel(Constants.DISMOUNT_TIRE_NOTES));
                                // For Edit mode is serialNo is null means it is coming
                                // for edit mode
                                String serialNo = mJobItemList.get(i).getSerialNumber();
                                String branName = mJobItemList.get(i).getBrandName();
                                String fullDesign = mJobItemList.get(i)
                                        .getFullDesignDetails();
                                String minNSKDismount = mJobItemList.get(i).getMinNSK();
                                if (TextUtils.isEmpty(serialNo)) {
                                    serialNo = tire.getSerialNumber();
                                    branName = tire.getBrandName();
                                    fullDesign = tire.getDesignDetails();
                                    minNSKDismount = tire.getMinNSK();
                                }
                                String dismount_details_text = "<b>"
                                        + mDbHelper.getLabel(141)
                                        + "</b> "
                                        + "\t<b> "
                                        + position
                                        + "</b> "
                                        + "\t<b>"
                                        + mDbHelper.getLabel(170)
                                        + "</b> "
                                        + "\t<b> "
                                        + serialNo
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(64)
                                        + "</b> "
                                        + "\t<b> "
                                        + branName
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(142)
                                        + "</b> "
                                        + "\t<b> "
                                        + fullDesign
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(59)
                                        + "</b> "
                                        + "\t<b> "
                                        + minNSKDismount
                                        + "mm</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(60)
                                        + "</b> "
                                        + "\t<b> "
                                        + mDbHelper.getCasingRouteFromId(mJobItemList
                                        .get(i).getCasingRouteCode())
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(61)
                                        + "</b> "
                                        + "\t<b> "
                                        + mDbHelper.getRemovalReasonFromId(mJobItemList
                                        .get(i).getRemovalReasonId());
                                if (!TextUtils.isEmpty(regrooved)) {
                                    dismount_details_text = dismount_details_text
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper
                                            .getLabel(Constants.REGROOVED_LABEL)
                                            + "</b> " + "\t<b> " + regrooved + "</b> ";
                                }
                                if (!"".equals(mJobItemList.get(i).getDamageId())) {
                                    dismount_details_text = dismount_details_text
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper.getLabel(317)
                                            + "</b> "
                                            + "\t<b> "
                                            + mDbHelper
                                            .getDamageAreaFromId(mJobItemList
                                                    .get(i).getDamageId())
                                            + "</b> ";
                                }
                                if (!TextUtils.isEmpty(rimType)) {
                                    dismount_details_text = dismount_details_text
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper
                                            .getLabel(Constants.RIM_TYPE_LABEL)
                                            + "</b> " + "\t<b> " + rimType + "</b> ";
                                }
                                details_tire.setText(Html
                                        .fromHtml(dismount_details_text));
                                historyArea.addView(historyItem);
                                break;
                            case 2:// Mount New Tier
                                history_action_name.setText(mDbHelper.getLabel(173));
                                details_notes_label.setText(mDbHelper
                                        .getLabel(Constants.MOUNT_NEW_TIRE));
                                // For Edit mode is serialNo is null means it is coming
                                // for edit mode
                                String serialNoMount = mJobItemList.get(i)
                                        .getSerialNumber();
                                String branNameMount = mJobItemList.get(i)
                                        .getBrandName();
                                String fullDesignMount = mJobItemList.get(i)
                                        .getFullDesignDetails();
                                String minNSKMount = mJobItemList.get(i).getMinNSK();
                                if (TextUtils.isEmpty(serialNoMount)) {
                                    serialNoMount = tire.getSerialNumber();
                                    branNameMount = tire.getBrandName();
                                    fullDesignMount = tire.getDesignDetails();
                                    minNSKMount = tire.getMinNSK();
                                }
                                String mount_details_text = "<b>"
                                        + mDbHelper.getLabel(141)
                                        + "</b> "
                                        + "\t<b> "
                                        + position
                                        + "</b> "
                                        + "\t<b>"
                                        + mDbHelper.getLabel(170)
                                        + "</b> "
                                        + "\t<b> "
                                        + serialNoMount
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(64)
                                        + "</b> "
                                        + "\t<b> "
                                        + branNameMount
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(142)
                                        + "</b> "
                                        + "\t<b> "
                                        + fullDesignMount
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(321)
                                        + "</b> "
                                        + "\t<b> "
                                        + tire.getSize()
                                        + "/"
                                        + tire.getAsp()
                                        + " "
                                        + tire.getRim()
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(386)
                                        + "</b> "
                                        + "\t<b> "
                                        + CommonUtils.getPressureValue(mContext,
                                        tire.getPressure()) + "</b> "
                                        + CommonUtils.getPressureUnit(mContext)
                                        + "<br/>" + "<b>" + mDbHelper.getLabel(387)
                                        + "</b> " + "\t<b> " + minNSKMount + "mm</b> ";
                                details_tire.setText(Html.fromHtml(mount_details_text));
                                historyArea.addView(historyItem);
                                break;
                            case 25:// PW Tier
                                history_action_name.setText(mDbHelper.getLabel(129));
                                details_notes_label.setText(mDbHelper
                                        .getLabel(Constants.MOUNT_PWT_NOTES));
                                String mount_pwt_details_text = "<b>"
                                        + mDbHelper.getLabel(116) + "</b> " + "<br/>"
                                        + "<b>" + mDbHelper.getLabel(170) + "</b> "
                                        + "\t<b> " + tire.getSerialNumber() + "</b> "
                                        + "<br/>" + "<b>" + mDbHelper.getLabel(64)
                                        + "</b> " + "\t<b> " + tire.getBrandName()
                                        + "</b> " + "<br/>" + "<b>"
                                        + mDbHelper.getLabel(142) + "</b> " + "\t<b> "
                                        + tire.getDesignDetails() + "</b> " + "<br/>"
                                        + "<b>" + mDbHelper.getLabel(387) + "</b> "
                                        + "\t<b> " + tire.getMinNSK() + "mm</b> ";
                                details_tire.setText(Html
                                        .fromHtml(mount_pwt_details_text));
                                historyArea.addView(historyItem);
                                break;
                            case 3:// SWAP
                                history_action_name.setText(mDbHelper.getLabel(134));
                            {
                                String serialNoSwap = mJobItemList.get(i)
                                        .getSerialNumber();
                                String branNameSwap = mJobItemList.get(i)
                                        .getBrandName();
                                String fullDesignSwap = mJobItemList.get(i)
                                        .getFullDesignDetails();
                                String minNSKSwap = mJobItemList.get(i).getMinNSK();
                                String jobItemPressure = mJobItemList.get(i)
                                        .getPressure();
                                jobItemPressure = CommonUtils.getPressureValue(
                                        mContext, jobItemPressure);
                                if (TextUtils.isEmpty(serialNoSwap)) {
                                    serialNoSwap = tire.getSerialNumber();
                                    branNameSwap = tire.getBrandName();
                                    fullDesignSwap = tire.getDesignDetails();
                                    // minNSKSwap = tire.getMinNSK();
                                    jobItemPressure = CommonUtils.getPressureValue(
                                            mContext, tire.getPressure());
                                }
                                if (mJobItemList.get(i).getSwapType().equals("4")) {
                                    String swap_details_text = "<b>"
                                            + mDbHelper.getLabel(118)
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper.getLabel(170)
                                            + "</b> "
                                            + "\t<b> "
                                            + serialNoSwap
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper.getLabel(64)
                                            + "</b> "
                                            + "\t<b> "
                                            + branNameSwap
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper.getLabel(142)
                                            + "</b> "
                                            + "\t<b> "
                                            + fullDesignSwap
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper
                                            .getLabel(Constants.NSK_LABEL)
                                            + "</b> "
                                            + "\t<b> "
                                            + minNSKSwap
                                            + "mm</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper.getLabel(306)
                                            + "</b> "
                                            + "\t<b> "
                                            + jobItemPressure
                                            + "</b> "
                                            + "<b>"
                                            + CommonUtils.getPressureUnit(mContext)
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper.getLabel(124)
                                            + "</b> "
                                            + "\t<b> "
                                            + mJobItemList.get(i)
                                            .getSwapOriginalPosition()
                                            + "</b> ";
                                    if (!TextUtils.isEmpty(regrooved)) {
                                        swap_details_text = swap_details_text
                                                + "</b> "
                                                + "<br/>"
                                                + "<b>"
                                                + mDbHelper
                                                .getLabel(Constants.REGROOVED_LABEL)
                                                + "</b> " + "\t<b> " + regrooved
                                                + "</b> ";
                                    }
                                    if (!TextUtils.isEmpty(rimType)) {
                                        swap_details_text = swap_details_text
                                                + "</b> "
                                                + "<br/>"
                                                + "<b>"
                                                + mDbHelper
                                                .getLabel(Constants.RIM_TYPE_LABEL)
                                                + "</b> " + "\t<b> " + rimType
                                                + "</b> ";
                                    }
                                    details_tire.setText(Html
                                            .fromHtml(swap_details_text));
                                } else {
                                    String swap_details_text = "<b>"
                                            + mDbHelper.getLabel(119)
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper.getLabel(170)
                                            + "</b> "
                                            + "\t<b> "
                                            + serialNoSwap
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper.getLabel(64)
                                            + "</b> "
                                            + "\t<b> "
                                            + branNameSwap
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper.getLabel(142)
                                            + "</b> "
                                            + "\t<b> "
                                            + fullDesignSwap
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper
                                            .getLabel(Constants.NSK_LABEL)
                                            + "</b> "
                                            + "\t<b> "
                                            + minNSKSwap
                                            + "mm</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper.getLabel(306)
                                            + "</b> "
                                            + "\t<b> "
                                            + jobItemPressure
                                            + "</b> "
                                            + "<b>"
                                            + CommonUtils.getPressureUnit(mContext)
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper.getLabel(124)
                                            + "</b> "
                                            + "\t<b> "
                                            + mJobItemList.get(i)
                                            .getSwapOriginalPosition()
                                            + "</b> ";
                                    if (!TextUtils.isEmpty(regrooved)) {
                                        swap_details_text = swap_details_text
                                                + "</b> "
                                                + "<br/>"
                                                + "<b>"
                                                + mDbHelper
                                                .getLabel(Constants.REGROOVED_LABEL)
                                                + "</b> " + "\t<b> " + regrooved
                                                + "</b> ";
                                    }
                                    if (!TextUtils.isEmpty(rimType)) {
                                        swap_details_text = swap_details_text
                                                + "</b> "
                                                + "<br/>"
                                                + "<b>"
                                                + mDbHelper
                                                .getLabel(Constants.RIM_TYPE_LABEL)
                                                + "</b> " + "\t<b> " + rimType
                                                + "</b> ";
                                    }
                                    details_tire.setText(Html
                                            .fromHtml(swap_details_text));
                                }
                                details_notes_label.setText(mDbHelper
                                        .getLabel(Constants.SWAP_NOTES));
                                historyArea.addView(historyItem);
                                break;
                            }
                            case 4:// Regroove
                                // For Edit mode is serialNo is null means it is
                                // coming for edit mode
                                String serialNoRegroove = mJobItemList.get(i)
                                        .getSerialNumber();
                                String branNameRegroove = mJobItemList.get(i)
                                        .getBrandName();
                                String fullDesignRegroove = mJobItemList.get(i)
                                        .getFullDesignDetails();

                                if (TextUtils.isEmpty(serialNoRegroove)) {
                                    serialNoRegroove = tire.getSerialNumber();
                                    branNameRegroove = tire.getBrandName();
                                    fullDesignRegroove = tire.getDesignDetails();
                                }
                                history_action_name.setText(mDbHelper.getLabel(200));
                                details_notes_label.setText(mDbHelper
                                        .getLabel(Constants.REGROOE_NOTES));
                                String tor_details_text = "<b>"
                                        + mDbHelper.getLabel(117)
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(170)
                                        + "</b> "
                                        + "\t<b> "
                                        + serialNoRegroove
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(64)
                                        + "</b> "
                                        + "\t<b> "
                                        + branNameRegroove
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(142)
                                        + "</b> "
                                        + "\t<b> "
                                        + fullDesignRegroove
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(126)
                                        + "</b> "
                                        + "\t<b> "
                                        + tire.getOrignalNSK()
                                        + "</b> "
                                        + "mm<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(127)
                                        + "</b> "
                                        + "\t<b> "
                                        + CommonUtils.getMinNskValue(
                                        jobItem.getNskOneAfter(),
                                        jobItem.getNskTwoAfter(),
                                        jobItem.getNskThreeAfter())
                                        + "</b> mm<br/> "
                                        + "<b>"
                                        + mDbHelper
                                        .getLabel(Constants.REGROOVE_TYPE_LABEL)
                                        + "</b> " + "\t<b>" + regrooveType + "</b>";
                                details_tire.setText(Html.fromHtml(tor_details_text));
                                historyArea.addView(historyItem);
                                break;
                            case 8:// TOR
                                // For Edit mode is serialNo is null means it is
                                // coming for edit mode
                                String serialNoTor = mJobItemList.get(i)
                                        .getSerialNumber();
                                String branNameTor = mJobItemList.get(i).getBrandName();
                                String fullDesignTor = mJobItemList.get(i)
                                        .getFullDesignDetails();
                                if (TextUtils.isEmpty(serialNoTor)) {
                                    serialNoTor = tire.getSerialNumber();
                                    branNameTor = tire.getBrandName();
                                    fullDesignTor = tire.getDesignDetails();
                                }
                                history_action_name.setText(mDbHelper.getLabel(135));
                                details_notes_label.setText(mDbHelper
                                        .getLabel(Constants.TOR_NOTES));
                                String tor_details = "<b>"
                                        + mDbHelper.getLabel(112)
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(170)
                                        + "</b> "
                                        + "\t<b> "
                                        + serialNoTor
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(64)
                                        + "</b> "
                                        + "\t<b> "
                                        + branNameTor
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(142)
                                        + "</b> "
                                        + "\t<b> "
                                        + fullDesignTor
                                        + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mDbHelper.getLabel(387)
                                        + "</b> "
                                        + "\t<b> "
                                        + CommonUtils.getMinNskValue(
                                        jobItem.getNskOneAfter(),
                                        jobItem.getNskTwoAfter(),
                                        jobItem.getNskThreeAfter()) + "mm</b>";
                                if (!TextUtils.isEmpty(regrooved)) {
                                    tor_details = tor_details
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper
                                            .getLabel(Constants.REGROOVED_LABEL)
                                            + "</b> " + "\t<b> " + regrooved + "</b> ";
                                }
                                if (!TextUtils.isEmpty(rimType)) {
                                    tor_details = tor_details
                                            + "</b> "
                                            + "<br/>"
                                            + "<b>"
                                            + mDbHelper
                                            .getLabel(Constants.RIM_TYPE_LABEL)
                                            + "</b> " + "\t<b> " + rimType + "</b> ";
                                }
                                ;

                                details_tire.setText(Html.fromHtml(tor_details));
                                historyArea.addView(historyItem);
                                break;
                            case 26:// History content added for Inspection
                                // For Edit mode is serialNo is null means it is
                                // coming for edit mode

                                String inspection_details_text = "";

                                String serialNoInspection = mJobItemList.get(i)
                                        .getSerialNumber();
                                String branNameInspection = mJobItemList.get(i)
                                        .getBrandName();
                                String fullDesignInspection = mJobItemList.get(i)
                                        .getFullDesignDetails();

                                if (TextUtils.isEmpty(serialNoInspection)) {
                                    serialNoInspection = tire.getSerialNumber();
                                    branNameInspection = tire.getBrandName();
                                    fullDesignInspection = tire.getDesignDetails();
                                }

                                history_action_name.setText(mLabelInspection);
                                details_notes_label.setText(mDbHelper
                                        .getLabel(Constants.TOR_NOTES));

                                inspection_details_text = inspection_details_text + "<b>"
                                        + mLabelTyreInspected
                                        + "</b> "
                                        + "<br/>";

                                //As per Client Request, (brand, serial number, pattern) - not needed in Inspection Summary Page for Non maintained tyres

                                if (!TextUtils.isEmpty(serialNoInspection)) {
                                    inspection_details_text = inspection_details_text
                                            + "<b>"
                                            + mDbHelper.getLabel(170)
                                            + "</b> "
                                            + "\t<b> "
                                            + serialNoInspection
                                            + "</b> "
                                            + "<br/>";

                                    //brand name check
                                    if (!TextUtils.isEmpty(branNameInspection)) {
                                        inspection_details_text = inspection_details_text
                                                + "<b>"
                                                + mDbHelper.getLabel(64)
                                                + "</b> "
                                                + "\t<b> "
                                                + branNameInspection
                                                + "</b> "
                                                + "<br/>";
                                    }

                                    //design name check
                                    if (!TextUtils.isEmpty(fullDesignInspection)) {
                                        inspection_details_text = inspection_details_text
                                                + "<b>"
                                                + mDbHelper.getLabel(142)
                                                + "</b> "
                                                + "\t<b> "
                                                + fullDesignInspection
                                                + "</b> "
                                                + "<br/>";
                                    }
                                }

                                //Fix : (Bug  655) - The inspection record should show the latest TD value
                                inspection_details_text = inspection_details_text
                                        + "<b>"
                                        + "TD"
                                        + "</b> "
                                        + "\t<b> "
                                        + CommonUtils.getMinNskValue(tire.getNsk(), tire.getNsk2(), tire.getNsk3())
                                        + "</b> "
                                        + "mm<br/>";

                                inspection_details_text = inspection_details_text
                                        + "<b>"
                                        + mDbHelper
                                        .getLabel(Constants.REGROOVED_LABEL)
                                        + "</b> "
                                        + "\t<b> "
                                        + regrooved + "</b> "
                                        + "<br/>"
                                        + "<b>"
                                        + mLabelTemperature
                                        + "</b> "
                                        + "\t<b> "
                                        + temperature
                                        + "</b><br/> ";

                                //UI changes as per request (Original pressure and rectified pressure is shown only when their values are > 0)

                                //setting blank or null based on orginal pressure value
                                if (!(originalPressure.equalsIgnoreCase("0") || originalPressure.equalsIgnoreCase("0.00") || originalPressure.equalsIgnoreCase("0.0"))) {
                                    inspection_details_text = inspection_details_text
                                            + "<b>"
                                            + mLabelRecordedPressure
                                            + "</b> "
                                            + "\t<b> "
                                            + originalPressure
                                            + "</b>\t"
                                            + CommonUtils.getPressureUnit(mContext)
                                            + "<br/>";
                                }

                                //setting blank or null based on rec pressure value
                                if (!(recPressure.equalsIgnoreCase("0") || recPressure.equalsIgnoreCase("0.00") || recPressure.equalsIgnoreCase("0.0"))) {
                                    inspection_details_text = inspection_details_text
                                            + "<b>"
                                            + mLabelCorrectedPressure
                                            + "</b> "
                                            + "\t<b> "
                                            + recPressure
                                            + "</b>\t"
                                            + CommonUtils.getPressureUnit(mContext)
                                            + "<br/> ";
                                }

                                details_tire.setText(Html.fromHtml(inspection_details_text));
                                historyArea.addView(historyItem);
                                break;
                        }
                    }
                }
            }
        }

        if (mIsNotesInEditMode) {
            showNoteEdit(historyArea, NoteEditTag);
        }
    }

    /**
     * Call popUp for tire Operation
     *
     * @param viewTire
     */
    private void showTireAction(View viewTire) {
        Constants.ONLONG_PRESS = true;
        if (Constants.onReturnBool == false) {
            for (Tyre tire : tyreInfo) {
                if (tire.getPosition().equals(viewTire.getTag())) {
                    try {
                        mSelectedTyre = tire;
                        Constants.SELECTED_TYRE = tire;
                        loadActionDialog();
                        if (mLastSelected != null) {
                            if (mLastSelectedResource == R.drawable.selected_tire
                                    || mLastSelectedResource == R.drawable.normal_tire) {
                                LinearLayout textAreaParent = (LinearLayout) mLastViewInfo
                                        .getParent();
                                textAreaParent
                                        .setVisibility(LinearLayout.INVISIBLE);
                                mLastViewInfo.setText("");
                                mLastSelected
                                        .setBackgroundResource(R.drawable.normal_tire);
                            } else if (mLastSelectedResource == R.drawable.dismount_tire) {
                                if (mLastViewInfo != null) {
                                    LinearLayout textAreaParent = (LinearLayout) mLastViewInfo
                                            .getParent();
                                    textAreaParent
                                            .setVisibility(LinearLayout.INVISIBLE);
                                    mLastViewInfo.setText("");
                                }
                                mLastSelected
                                        .setBackgroundResource(R.drawable.dismount_tire);
                            } else if (mLastSelectedResource == R.drawable.worked_wheel_s
                                    || mLastSelectedResource == R.drawable.worked_wheel) {
                                if (mLastViewInfo != null) {
                                    LinearLayout textAreaParent = (LinearLayout) mLastViewInfo
                                            .getParent();
                                    textAreaParent
                                            .setVisibility(LinearLayout.INVISIBLE);
                                    mLastViewInfo.setText("");
                                }
                                mLastSelected
                                        .setBackgroundResource(R.drawable.worked_wheel);
                            } else {
                                LinearLayout textAreaParent = (LinearLayout) mLastViewInfo
                                        .getParent();
                                textAreaParent
                                        .setVisibility(LinearLayout.INVISIBLE);
                                mLastViewInfo.setText("");
                                mLastSelected
                                        .setBackgroundResource(R.drawable.no_maintained_tire);
                            }
                        }
                        if (tire.getSerialNumber().equals("")) {
                            viewTire.setBackgroundResource(R.drawable.no_maintained_tire_s);
                            mLastSelectedResource = R.drawable.no_maintained_tire_s;
                        } else if (tire.isDismounted()) {
                            if (checkIfLastOperationIsDismount(tire
                                    .getExternalID())) {
                                viewTire.setBackgroundResource(R.drawable.dismount_tire_s);
                                mLastSelectedResource = R.drawable.dismount_tire_s;
                            } else {
                                viewTire.setBackgroundResource(R.drawable.selected_tire);
                                mLastSelectedResource = R.drawable.dismount_tire_s;
                            }
                        } else if (tire.isHasTurnedOnRim()
                                || tire.isReGrooved()
                                || tire.isIsphysicalySwaped()
                                || tire.isLogicalySwaped() || tire.isWorkedOn()) {
                            viewTire.setBackgroundResource(R.drawable.worked_wheel_s);
                            mLastSelectedResource = R.drawable.worked_wheel_s;
                        } else {
                            viewTire.setBackgroundResource(R.drawable.selected_tire);
                            mLastSelectedResource = R.drawable.selected_tire;
                        }
                        String textViewTag = tire.getPosition().charAt(0)
                                + Constants.INFO_TEXT_VIEW_NAME;
                        // get details text view using tag
                        TextView textArea = (TextView) mVehicle
                                .findViewWithTag(textViewTag);

                        if (textArea == null) {
                            textArea = (TextView) mVehicle
                                    .findViewWithTag("spareaxleinfo");
                        }
                        // get parent of details and make it visible
                        LinearLayout textAreaParent = (LinearLayout) textArea
                                .getParent();
                        textAreaParent.setVisibility(LinearLayout.INVISIBLE);
                        for (int j = 1; j < textAreaParent.getChildCount(); j++) {
                            textAreaParent.removeViewAt(j);
                        }
                        String pressureUnit = CommonUtils
                                .getPressureUnit(mContext);
                        // set the details for the tire
                        String tireInfoText = "<b>"
                                + mSerialNumber
                                + "</b> "
                                + tire.getSerialNumber()
                                + "<b> "
                                + mDesign
                                + "</b> "
                                + tire.getDesignDetails()
                                + "<br/> <b> "
                                + mNskOne
                                + "</b> "
                                + tire.getNsk()
                                + "mm"
                                + "\t<b> "
                                + mNskTwo
                                + "</b> "
                                + tire.getNsk2()
                                + "mm"
                                + "\t<b> "
                                + mNskThree
                                + "</b> "
                                + tire.getNsk3()
                                + "mm"
                                + "<b> "
                                + mPressure
                                + "</b> "
                                + CommonUtils.getPressureValue(getActivity(),
                                tire.getPressure()) + " "
                                + pressureUnit;
                        textArea.setText(Html.fromHtml(tireInfoText));
                        textAreaParent.setVisibility(View.GONE);
                        // get pressure unit
                        mLastViewInfo = textArea;

                        mLastSelected = (ImageButton) viewTire;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (Constants.onReturnBool == true) {
            for (Tyre tireTwo : tyreInfo) {
                // map tire selected to tire array
                String tirePos = tireTwo.getPosition();
                if (tirePos.equals(viewTire.getTag())) {
                    try {
                        mSelectedTyre = tireTwo;
                        Constants.SECOND_SELECTED_TYRE = tireTwo;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Clearing all boolean variables before loading Tire Action dialog
     */
    private void clearActionBools() {
        Constants.ONSTATE_WOT = false;
        Constants.ONSTATE_MOUNTPWT = false;
        Constants.ONSTATE_MOUNT = false;
        Constants.ONSTATE_DISMOUNT = false;
        Constants.ONSTATE_REGROOVE = false;
        Constants.ONSTATE_TOR = false;
        Constants.DO_PHYSICAL_SWAP = false;
        Constants.DO_LOGICAL_SWAP = false;
    }

    /**
     * Loading Tire Operation Dialog
     */
    private void loadActionDialog() {
        mDialogRefNo = 8;
        Constants.EDITED_SERIAL_NUMBER = "";
        try {
            clearTextAreasForAction();
            clearActionBools();
            if (null != mTireOperationDialogMenu
                    && mTireOperationDialogMenu.isShowing()) {
                System.out.print("mTireOperationDialogMenu popup canceld");
                LogUtil.i(TAG, "mTireOperationDialogMenu popup canceld");
                return;
            }

            mIsTireMenuDisplayed = true;
            // get prompts.xml view
            LayoutInflater li_action = LayoutInflater.from(mContext);
            final View vehicleMenuOption = li_action.inflate(
                    R.layout.dialog_tyre_action, null);
            updateVehicleMenu(vehicleMenuOption);
            mTireOperationDialogMenu = new Dialog(mContext);
            mTireOperationDialogMenu.setCanceledOnTouchOutside(false);
            mTireOperationDialogMenu
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);

            mTireOperationDialogMenu.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode,
                                     KeyEvent event) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            VehicleSkeletonFragment.mIsTireMenuDisplayed = false;
                            if (Constants.ONPRESS == false) {
                                System.out
                                        .println("loadActionDialog onDismissListener");
                                resetBoolValue();
                                mDialogRefNo = -1;
                            }
                            break;
                        default:
                            break;
                    }
                    return false;
                }
            });
            final Button btnRegroove = (Button) vehicleMenuOption
                    .findViewById(R.id.btnRegrooveTyre);
            btnRegroove.setText(mRegrooveLabel);

            final Button btnTurnOnRim = (Button) vehicleMenuOption
                    .findViewById(R.id.btnTOR);
            btnTurnOnRim.setText(mTorLabel);

            final Button btnPhysicalSwap = (Button) vehicleMenuOption
                    .findViewById(R.id.btnPhysicalSwap);
            btnPhysicalSwap.setText(mPhysicalSwapLabel);

            final Button btnLogicalSwap = (Button) vehicleMenuOption
                    .findViewById(R.id.btnLogicalSwap);
            btnLogicalSwap.setText(mLogicalSwapLabel);

            final Button btnDismountTire = (Button) vehicleMenuOption
                    .findViewById(R.id.btnDismoutTyre);
            btnDismountTire.setText(mDismountLabel);

            final Button btnMountNewTire = (Button) vehicleMenuOption
                    .findViewById(R.id.btnMountNewTyre);
            btnMountNewTire.setText(mMountNewTireLabel);
            final Button btnMountPWT = (Button) vehicleMenuOption
                    .findViewById(R.id.btnMountPWT);
            btnMountPWT.setText(mMountPWTTireLabel);
            final Button btnWOT = (Button) vehicleMenuOption
                    .findViewById(R.id.btnWorkOnTyre);
            btnWOT.setText(mWotLabel);
            // Button added for inspection :: CR-447
            final Button btnInspection = (Button) vehicleMenuOption
                    .findViewById(R.id.btnInspection);
            btnInspection.setText(mLabelInspection);
            btnInspection.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View button) {
                    // update user's inactivity
                    InactivityUtils.updateActivityOfUser();
                    LogUtil.TraceInfo(TRACE_INFO, "Operation", "Inspection - OP", false, false, true);
                    Constants.ONSTATE_INSPECTION = true;
                    Constants.mGlobalVisualRemarks = new ArrayList<VisualRemark>();
                    mDialogRefNo = -1;
                    //Constants.ONSTATE_WOT = true;
                    btnInspection.setBackgroundResource(R.drawable.vehicle_menu_button_clicked);

                    //As per Client Requirement, For Inspection, no serial number correction and brand correction for non maintained and NR tyres
                    // true if Selected tire is non-maintained
//                    if (Constants.SELECTED_TYRE.getSerialNumber()
//                            .equalsIgnoreCase("")) {
//                        loadSerialMountNewDialog();
//                    // true if Selected tire is maintained
//                    } else {
                    Constants.TyreIdListToHandlerBackPress = new ArrayList<String>();
                    Intent intent = new Intent(mContext, InspectionActivity.class);
                    intent.putExtra("SERIAL_NO",
                            Constants.SELECTED_TYRE.getSerialNumber());
                    int pos = Character
                            .getNumericValue(Constants.SELECTED_TYRE
                                    .getPosition().charAt(0)) - 1;
                    intent.putExtra(mAXLEPRESSURE_POS, pos);
                    intent.putExtra(mAXLEPRESSURE,
                            mAxleRecommendedPressure.get(pos));
                    intent.putExtra("WHEEL_POS",
                            Constants.SELECTED_TYRE.getPosition());
                    intent.putExtra("FROM_MOUNT_NEW", false);
                    startActivityForResult(intent, INSPECTION_INTENT);
//                    }


                    mTireOperationDialogMenu.cancel();
                    clearTextAreasForAction();
                }
            });
            // on click of mount wot
            btnWOT.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View button) {
                    // update user's inactivity
                    InactivityUtils.updateActivityOfUser();
                    LogUtil.TraceInfo(TRACE_INFO, "Operation", "Work On Tyre - OP", false, false, true);
                    Constants.TyreIdListToHandlerBackPress=new ArrayList<String>();
                    mDialogRefNo = -1;
                    resetBoolValue();
                    Constants.ONSTATE_WOT = true;
                    btnWOT.setBackgroundResource(R.drawable.vehicle_menu_button_clicked);
                    if (Constants.SELECTED_TYRE.getSerialNumber()
                            .equalsIgnoreCase("")) {
                        loadSerialMountNewDialog();

                    }// changed by Sundeep
                    else if (TextUtils.isEmpty(Constants.SELECTED_TYRE
                            .getBrandName())
                            || TextUtils.isEmpty(Constants.SELECTED_TYRE
                            .getDesignDetails())) {
                        /**
                         * serial correction along with auto swap functionality
                         * needs to be implemented here.
                         *
                         */
                        loadSerialCorrectionDialog();
                    } else {
                        Intent intent = new Intent(mContext, WOTActivity.class);
                        intent.putExtra("SERIAL_NO",
                                Constants.SELECTED_TYRE.getSerialNumber());
                        int pos = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        intent.putExtra(mAXLEPRESSURE_POS, pos);
                        intent.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(pos));
                        intent.putExtra("WHEEL_POS",
                                Constants.SELECTED_TYRE.getPosition());
                        intent.putExtra("FROM_MOUNT_NEW", false);
                        startActivityForResult(intent, WOT_INTENT);
                    }
                    mTireOperationDialogMenu.cancel();
                    clearTextAreasForAction();
                }
            });
            // on click of mount pwt
            btnMountPWT.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View button) {
                    // update user's inactivity
                    InactivityUtils.updateActivityOfUser();
                    LogUtil.TraceInfo(TRACE_INFO, "Operation", "Mount PWT - OP", false, false, true);
                    Constants.TyreIdListToHandlerBackPress=new ArrayList<String>();
                    mDialogRefNo = -1;
                    Constants.ONSTATE_MOUNTPWT = true;
                    loadSerialMountPWTDialog();
                    btnMountPWT
                            .setBackgroundResource(R.drawable.vehicle_menu_button_clicked);
                    mTireOperationDialogMenu.cancel();
                    clearTextAreasForAction();
                }
            });
            // on click of mount new
            btnMountNewTire.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View button) {
                    // update user's inactivity
                    InactivityUtils.updateActivityOfUser();
                    Constants.TyreIdListToHandlerBackPress=new ArrayList<String>();
                    LogUtil.TraceInfo(TRACE_INFO, "Operation", "Mount New Tyre - OP",false,false,true);
                    mDialogRefNo = -1;
                    Constants.ONSTATE_MOUNT = true;
                    loadSerialMountNewDialog();
                    btnMountNewTire
                            .setBackgroundResource(R.drawable.vehicle_menu_button_clicked);
                    mTireOperationDialogMenu.cancel();
                    clearTextAreasForAction();
                }
            });
            // on click of dismount
            btnDismountTire.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View button) {
                    // update user's inactivity
                    InactivityUtils.updateActivityOfUser();
                    Constants.TyreIdListToHandlerBackPress=new ArrayList<String>();
                    LogUtil.TraceInfo(TRACE_INFO, "Operation", "Dismount - OP",false,false,true);
                    mDialogRefNo = -1;
                    resetBoolValue();
                    Constants.ONSTATE_DISMOUNT = true;
                    btnDismountTire
                            .setBackgroundResource(R.drawable.vehicle_menu_button_clicked);
                    if (Constants.SELECTED_TYRE.getSerialNumber()
                            .equalsIgnoreCase("")) {
                        Constants.DISMOUNTED_TYRE = Constants.SELECTED_TYRE;
                        loadSerialMountNewDialog();
                        btnDismountTire
                                .setBackgroundResource(R.drawable.vehicle_menu_button_clicked);
                    } else {
                        Constants.DISMOUNTED_TYRE = Constants.SELECTED_TYRE;
                        loadSerialCorrectionDialog();
                    }
                    dialogInEditMode = false;
                    mTireOperationDialogMenu.cancel();
                    clearTextAreasForAction();
                }
            });
            // on click of re groove
            btnRegroove.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View button) {
                    // update user's inactivity
                    InactivityUtils.updateActivityOfUser();
                    Constants.TyreIdListToHandlerBackPress=new ArrayList<String>();
                    LogUtil.TraceInfo(TRACE_INFO, "Operation", "Regroove - OP",false,false,true);
                    mDialogRefNo = -1;
                    Constants.ONSTATE_REGROOVE = true;
                    btnRegroove
                            .setBackgroundResource(R.drawable.vehicle_menu_button_clicked);
                    // start new re groove activity
                    if (Constants.SELECTED_TYRE.getSerialNumber()
                            .equalsIgnoreCase("")) {
                        loadSerialMountNewDialog();
                    } else {
                        Intent startRegrooveOperation = new Intent(mContext,
                                RegrooveTireOperation.class);
                        int pos = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        startRegrooveOperation.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(pos));
                        startRegrooveOperation.putExtra(mAXLEPRESSURE_POS, pos);
                        startActivityForResult(startRegrooveOperation,
                                VehicleSkeletonFragment.REGROOVE_INTENT);
                    }
                    mIsTireMenuDisplayed = false;
                    mTireOperationDialogMenu.cancel();
                    clearTextAreasForAction();
                }
            });

            btnTurnOnRim.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View button) {
                    // update user's inactivity
                    InactivityUtils.updateActivityOfUser();
                    Constants.TyreIdListToHandlerBackPress=new ArrayList<String>();
                    LogUtil.TraceInfo(TRACE_INFO, "Operation", "Turn on Rim - OP",false,false,true);
                    mDialogRefNo = -1;
                    resetBoolValue();
                    Constants.ONSTATE_TOR = true;
                    dialogInEditMode = false;
                    btnTurnOnRim
                            .setBackgroundResource(R.drawable.vehicle_menu_button_clicked);
                    if (Constants.SELECTED_TYRE.getSerialNumber()
                            .equalsIgnoreCase("")) {
                        loadSerialMountNewDialog();
                    } else {
                        loadSerialCorrectionDialog();
                    }
                    mTireOperationDialogMenu.cancel();
                    clearTextAreasForAction();
                }
            });
            btnPhysicalSwap.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    // update user's inactivity
                    InactivityUtils.updateActivityOfUser();
                    Constants.TyreIdListToHandlerBackPress=new ArrayList<String>();
                    LogUtil.TraceInfo(TRACE_INFO, "Operation", "Physical Swap - OP",false,false,true);
                    mDialogRefNo = -1;
                    resetBoolValue();
                    Constants.ONPRESS = true;
                    Constants.ONDRAG = false;
                    Constants.DO_PHYSICAL_SWAP = true;
                    Constants.DO_LOGICAL_SWAP = false;
                    btnPhysicalSwap
                            .setBackgroundResource(R.drawable.vehicle_menu_button_clicked);
                    if (Constants.SELECTED_TYRE.getSerialNumber()
                            .equalsIgnoreCase("")) {
                        loadSerialMountNewDialog();
                    } else {
                        loadSerialCorrectionDialog();
                    }
                    getTyreState();
                    dialogInEditMode = false;
                    mTireOperationDialogMenu.cancel();
                    clearTextAreasForAction();
                }
            });
            btnLogicalSwap.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    // update user's inactivity
                    InactivityUtils.updateActivityOfUser();
                    Constants.TyreIdListToHandlerBackPress=new ArrayList<String>();
                    LogUtil.TraceInfo(TRACE_INFO, "Operation", "Logical Swap - OP",false,false,true);
                    mDialogRefNo = -1;
                    resetBoolValue();
                    Constants.DO_LOGICAL_SWAP = true;
                    Constants.DO_PHYSICAL_SWAP = false;
                    Constants.onGOBool = false;
                    Constants.onReturnBool = false;
                    Constants.onSwapOptionsBool = true;
                    Constants.ONPRESS = true;
                    Constants.ONDRAG = false;
                    Constants.LOGICAL_SWAP_INITIAL = true;
                    getTyreState();
                    btnLogicalSwap
                            .setBackgroundResource(R.drawable.vehicle_menu_button_clicked);
                    // For Brand ::::: Fix:: SPARE-TYRE
                    if (Constants.SELECTED_TYRE != null
                            && Constants.SELECTED_TYRE.getTyreState() != TyreState.NON_MAINTAINED
                            && (TextUtils.isEmpty(Constants.SELECTED_TYRE
                            .getBrandName()) || TextUtils
                            .isEmpty(Constants.SELECTED_TYRE
                                    .getDesignDetails()))) {
                        Constants.onBrandBlank = true;
                        Constants.onBrandDesignBlank = true;
                        loadSerialCorrectionDialog();
                    } else if (Constants.SELECTED_TYRE != null
                            && Constants.SELECTED_TYRE.getTyreState() != TyreState.NON_MAINTAINED
                            && "NA".equalsIgnoreCase(Constants.SELECTED_TYRE
                            .getBrandName())
                            || "NA".equalsIgnoreCase(Constants.SELECTED_TYRE
                            .getDesignDetails())) {
                        Constants.onBrandBlank = true;
                        Constants.onBrandDesignBlank = true;
                        loadSerialCorrectionDialog();
                    } else if (Constants.SELECTED_TYRE.getSerialNumber()
                            .equalsIgnoreCase("")) {
                        loadSerialMountNewDialog();
                    } else {
                        loadLogicalSwapActionDialog();
                    }
                    clearTextAreasForAction();
                    mTireOperationDialogMenu.cancel();
                }
            });
            // set prompts.xml to alertdialog builder
            mTireOperationDialogMenu.setContentView(vehicleMenuOption);
            mTireOperationDialogMenu.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @param vehicleMenuOption
     */
    private void updateVehicleMenu(View vehicleMenuOption) {
        SharedPreferences preferences = getActivity().getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);
        if (Constants.SELECTED_TYRE.getTyreState() == TyreState.EMPTY) {

            // if tire is empty set only mount tire visible
            vehicleMenuOption.findViewById(R.id.btnMountNewTyre).setVisibility(
                    TextView.VISIBLE);
            vehicleMenuOption.findViewById(R.id.btnMountPWT).setVisibility(
                    TextView.VISIBLE);

            vehicleMenuOption.findViewById(R.id.btnPhysicalSwap).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnLogicalSwap).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnRegrooveTyre).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnTOR).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnWorkOnTyre).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnDismoutTyre).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnInspection).setVisibility(
                    TextView.GONE);
        } else if (Constants.SELECTED_TYRE.getTyreState() == TyreState.PART_WORN) {
            vehicleMenuOption.findViewById(R.id.btnLogicalSwap).setVisibility(
                    TextView.VISIBLE);
            vehicleMenuOption.findViewById(R.id.btnWorkOnTyre).setVisibility(
                    TextView.VISIBLE);

            if (Constants.SELECTED_TYRE.isDismounted()
                    || tireAtThisPositionWasDismounted(Constants.SELECTED_TYRE)) {
                vehicleMenuOption.findViewById(R.id.btnDismoutTyre)
                        .setVisibility(TextView.GONE);
                vehicleMenuOption.findViewById(R.id.btnInspection).setVisibility(
                        TextView.GONE);
            } else {
                vehicleMenuOption.findViewById(R.id.btnDismoutTyre)
                        .setVisibility(TextView.VISIBLE);
            }
            if (!Constants.JOBTYPEREGULAR) {
                vehicleMenuOption.findViewById(R.id.btnPhysicalSwap)
                        .setVisibility(TextView.GONE);
            } else {
                vehicleMenuOption.findViewById(R.id.btnPhysicalSwap)
                        .setVisibility(TextView.VISIBLE);
            }
            if (Constants.SELECTED_TYRE.isReGrooved()
                    || !Constants.JOBTYPEREGULAR) {
                vehicleMenuOption.findViewById(R.id.btnRegrooveTyre)
                        .setVisibility(TextView.GONE);
            } else {
                vehicleMenuOption.findViewById(R.id.btnRegrooveTyre)
                        .setVisibility(TextView.VISIBLE);
            }

            if (Constants.SELECTED_TYRE.hasTurnedOnRim()
                    || !Constants.JOBTYPEREGULAR) {
                vehicleMenuOption.findViewById(R.id.btnTOR).setVisibility(
                        TextView.GONE);
            } else {
                vehicleMenuOption.findViewById(R.id.btnTOR).setVisibility(
                        TextView.VISIBLE);
            }

            vehicleMenuOption.findViewById(R.id.btnMountNewTyre).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnMountPWT).setVisibility(
                    TextView.GONE);
            //447:: Inspection :: True if Inspection button is OFF in Settings
            if (!TextUtils.isEmpty(preferences.getString(Constants.INSPECTION, ""))) {
                Constants.INSPECTION_ENABLE = (preferences.getString(Constants.INSPECTION, ""));
                if (Constants.INSPECTION_ENABLE.equalsIgnoreCase("OFF") || Constants.SELECTED_TYRE.isInspected()) {
                    vehicleMenuOption.findViewById(R.id.btnInspection).setVisibility(
                            TextView.GONE);
                }
                else
                {
                    vehicleMenuOption.findViewById(R.id.btnInspection).setVisibility(
                            TextView.VISIBLE);
                }
            }


        } else if (Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED) {
            vehicleMenuOption.findViewById(R.id.btnLogicalSwap).setVisibility(
                    TextView.VISIBLE);
            vehicleMenuOption.findViewById(R.id.btnWorkOnTyre).setVisibility(
                    TextView.VISIBLE);
            if (Constants.SELECTED_TYRE.isDismounted()) {
                vehicleMenuOption.findViewById(R.id.btnDismoutTyre)
                        .setVisibility(TextView.GONE);
            } else {
                vehicleMenuOption.findViewById(R.id.btnDismoutTyre)
                        .setVisibility(TextView.VISIBLE);
            }

            if (Constants.SELECTED_TYRE.isIsphysicalySwaped()
                    || !Constants.JOBTYPEREGULAR) {
                vehicleMenuOption.findViewById(R.id.btnPhysicalSwap)
                        .setVisibility(TextView.VISIBLE);
            } else {
                vehicleMenuOption.findViewById(R.id.btnPhysicalSwap)
                        .setVisibility(TextView.VISIBLE);
            }

            if (Constants.SELECTED_TYRE.isReGrooved()
                    || !Constants.JOBTYPEREGULAR) {
                vehicleMenuOption.findViewById(R.id.btnRegrooveTyre)
                        .setVisibility(TextView.GONE);
            } else {
                vehicleMenuOption.findViewById(R.id.btnRegrooveTyre)
                        .setVisibility(TextView.VISIBLE);
            }

            if (Constants.SELECTED_TYRE.hasTurnedOnRim()
                    || !Constants.JOBTYPEREGULAR) {
                vehicleMenuOption.findViewById(R.id.btnTOR).setVisibility(
                        TextView.GONE);
            } else {
                vehicleMenuOption.findViewById(R.id.btnTOR).setVisibility(
                        TextView.VISIBLE);
            }
            vehicleMenuOption.findViewById(R.id.btnMountNewTyre).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnMountPWT).setVisibility(
                    TextView.GONE);
            //447:: Inspection :: True if Inspection button is OFF in Settings
            if (!TextUtils.isEmpty(preferences.getString(Constants.INSPECTION, ""))) {
                Constants.INSPECTION_ENABLE = (preferences.getString(Constants.INSPECTION, ""));
                if (Constants.INSPECTION_ENABLE.equalsIgnoreCase("OFF") || Constants.SELECTED_TYRE.isInspected()) {
                    vehicleMenuOption.findViewById(R.id.btnInspection).setVisibility(
                            TextView.GONE);
                }
            }
        } else {
            vehicleMenuOption.findViewById(R.id.btnLogicalSwap).setVisibility(
                    TextView.VISIBLE);
            vehicleMenuOption.findViewById(R.id.btnWorkOnTyre).setVisibility(
                    TextView.VISIBLE);
            if (Constants.SELECTED_TYRE.isIsphysicalySwaped()
                    || !Constants.JOBTYPEREGULAR) {
                vehicleMenuOption.findViewById(R.id.btnPhysicalSwap)
                        .setVisibility(TextView.VISIBLE);
            } else {
                vehicleMenuOption.findViewById(R.id.btnPhysicalSwap)
                        .setVisibility(TextView.VISIBLE);
            }
            vehicleMenuOption.findViewById(R.id.btnRegrooveTyre).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnTOR).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnMountNewTyre).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnMountPWT).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnDismoutTyre).setVisibility(
                    TextView.GONE);
            vehicleMenuOption.findViewById(R.id.btnInspection).setVisibility(
                    TextView.GONE);

            //447:: Inspection :: True if Inspection button is OFF in Settings
            if (!TextUtils.isEmpty(preferences.getString(Constants.INSPECTION, ""))) {
                Constants.INSPECTION_ENABLE = (preferences.getString(Constants.INSPECTION, ""));
                if (Constants.INSPECTION_ENABLE.equalsIgnoreCase("OFF") || Constants.SELECTED_TYRE.isInspected()) {
                    vehicleMenuOption.findViewById(R.id.btnInspection).setVisibility(
                            TextView.GONE);
                }
                else
                {
                    vehicleMenuOption.findViewById(R.id.btnInspection).setVisibility(
                            TextView.VISIBLE);
                }
            }
        }
        if (!Constants.JOBTYPEREGULAR) { // These will never appear in break
            // down job
            vehicleMenuOption.findViewById(R.id.btnPhysicalSwap).setVisibility(
                    View.GONE);
            vehicleMenuOption.findViewById(R.id.btnRegrooveTyre).setVisibility(
                    View.GONE);
            vehicleMenuOption.findViewById(R.id.btnTOR)
                    .setVisibility(View.GONE);
        }
    }

    /**
     * check if the current position was dismounted
     *
     * @param selectedTire tire at current position
     * @return true if dismounted earlier false if not
     */
    private boolean tireAtThisPositionWasDismounted(Tyre selectedTire) {
        String positionTocheck = selectedTire.getPosition();
        for (Tyre tire : VehicleSkeletonFragment.tyreInfo) {
            if ((positionTocheck + "$").equals(tire.getPosition())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns True If the String Contains a digit
     *
     * @param s
     * @return
     */
    public final boolean containsDigit(String s) {
        boolean containsDigit = false;
        if (s != null && !s.isEmpty()) {
            for (char c : s.toCharArray()) {
                if (containsDigit = Character.isDigit(c)) {
                    break;
                }
            }
        }
        return containsDigit;
    }

    /**
     * Returns True If there is less than 3 characters
     *
     * @param s
     * @return
     */
    public final boolean containsMin3(String s) {
        boolean containsDigit = false;
        if (s.length() < 4) {
            return false;
        }
        return containsDigit;
    }

    @SuppressWarnings("rawtypes")
    public void loadReservePWTData() {
        DatabaseAdapter reservePWTDBHandler = null;
        try {
            reservePWTDBHandler = new DatabaseAdapter(mContext);
            reservePWTDBHandler.createDatabase();
            reservePWTDBHandler.open();
            mReserveTyreList.clear();

            if (// Constants.COMESFROMUPDATE &&
                    null != ReservePWTUtils.mReserveTyreList
                            // && ReservePWTUtils.mReserveTyreList.size() <= 0
                            && null != Constants.Search_TM_ListItems
                            && Constants.Search_TM_ListItems.size() > 0) {
                ReservePWTUtils pwtUtils = new ReservePWTUtils();
                pwtUtils.updateReserveTyre(mContext);
            } else if (ReservePWTUtils.mReserveTyreList != null
                    && Constants.Search_TM_ListItems.size() == 0) {
                ReservePWTUtils.mReserveTyreList.clear();
            }
            TireDesignDetails tireDetails = CommonUtils
                    .getDetailsFromTyreInSameAxle(Constants.SELECTED_TYRE);
            String size = tireDetails.getSize();
            String asp = tireDetails.getRim();
            String rim = tireDetails.getAsp();

            /**
             * Condition check for Mount PWT tyre list based on IsDIffSizeAllowedForVehicle boolean value
             */
            if(Constants.contract.getIsDIffSizeAllowedForVehicle().equalsIgnoreCase("true")) {
                ReservePWTUtils.noFilterLoadReserveTireListForCurrentTire();
            }
            else {
                ReservePWTUtils.filterReserveTireListForCurrentTire(mContext, size, asp, rim);
            }


            if (null != ReservePWTUtils.mReserveTyreListForSelectedTyre
                    && ReservePWTUtils.mReserveTyreListForSelectedTyre.size() != 0) {
                mReserveTyreList.addAll(ReservePWTUtils.mReserveTyreListForSelectedTyre);
                mAlertDialog.show();
                ArrayAdapter<ReservedTyre> reservePWTAdapter = new ArrayAdapter<ReservedTyre>(
                        mContext, android.R.layout.simple_spinner_item,
                        mReserveTyreList);
                reservePWTAdapter
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mReserve_pwt_spinner.setAdapter(reservePWTAdapter);
                mReserve_pwt_spinner.setSelection(mReserve_tyre_selected);
                mReserve_pwt_spinner
                        .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView adapter,
                                                       View v, int i, long lng) {
                                mReserve_tyre_selected = i;
                            }

                            @Override
                            public void onNothingSelected(AdapterView arg0) {
                            }
                        });
            } else {
                Toast.makeText(mContext, mLblNoTyresFound, Toast.LENGTH_LONG)
                        .show();
                Intent intentPWT = new Intent(mContext, ReservePWT.class);
                intentPWT.putExtra("COMING_FROM", "selectedtire");
                startActivity(intentPWT);
                mDialogRefNo = 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != reservePWTDBHandler) {
                reservePWTDBHandler.close();
            }
        }
    }

    /**
     * calling Mount PWT Dialog
     */
    private void loadSerialMountPWTDialog() {
        LogUtil.TraceInfo(TRACE_INFO, "Dialog", "Reserved PWT List",false,true,false);
        mDialogRefNo = 12;
        LayoutInflater li = LayoutInflater.from(mContext);
        final View promptsView = li.inflate(R.layout.dialog_serial_mountpwt,
                null);
        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(mContext);
        // get the layout root node
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        // update user inactivity when user interacts with dialog
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });
        alertDialogBuilder.setView(promptsView);
        mAlertDialog = alertDialogBuilder.create();
        mAlertDialog.setCanceledOnTouchOutside(false);
        mReserve_pwt_spinner = (Spinner) promptsView
                .findViewById(R.id.spinner_reserve_tyres);
        loadReservePWTData();
        final Button btnOk = (Button) promptsView.findViewById(R.id.sc_btnOk);
        final Button btnCancel = (Button) promptsView
                .findViewById(R.id.sc_btnCancel);
        final TextView mlbl_SerialConfirmation = (TextView) promptsView
                .findViewById(R.id.sc_ConfirmText);
        mlbl_SerialConfirmation.setText(mSelectReserveSerialLabel);
        btnOk.setText(mOKLabel);
        btnCancel.setText(mCancelLabel);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // update user activity
                alertDialogBuilder.updateInactivityForDialog();

                Intent mountPWTIntent = new Intent(mContext,
                        MountPWTActivity.class);
                mountPWTIntent.putExtra("SERIAL_NO",
                        ((ReservedTyre) mReserve_pwt_spinner.getSelectedItem())
                                .getTyreSerialNo());
                mountPWTIntent.putExtra("TYRE_ID",
                        ((ReservedTyre) mReserve_pwt_spinner.getSelectedItem())
                                .getTyreID());
                int pos = Character.getNumericValue(Constants.SELECTED_TYRE
                        .getPosition().charAt(0)) - 1;
                mountPWTIntent.putExtra(mAXLEPRESSURE,
                        mAxleRecommendedPressure.get(pos));
                mountPWTIntent.putExtra(mAXLEPRESSURE_POS, pos);
                if(((ReservedTyre) mReserve_pwt_spinner.getSelectedItem())
                        .getTyreSerialNo()!=null) {
                    LogUtil.TraceInfo(TRACE_INFO, "Reserved PWT List", "Selected SN : "+((ReservedTyre) mReserve_pwt_spinner.getSelectedItem())
                            .getTyreSerialNo(), false, false, false);
                }
                else
                {
                    LogUtil.TraceInfo(TRACE_INFO, "Reserved PWT List","Selected : None", false, false, false);
                }
                startActivityForResult(mountPWTIntent,
                        VehicleSkeletonFragment.MOUNT_PWT_INTENT);
                mIsTireMenuDisplayed = false;
                Constants.ONSTATE_MOUNTPWT = false;
                mAlertDialog.dismiss();
                mDialogRefNo = 0;
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                LogUtil.TraceInfo(TRACE_INFO, "Reserved PWT List", "Cancel BT", false, false, false);
                mDialogRefNo = 0;
                if (mAlertDialog.isShowing()) {
                    mAlertDialog.dismiss();
                }
            }

        });
    }

    /**
     * calling Mount New tire Dialog
     */
    private void loadSerialMountNewDialog() {
        LogUtil.TraceInfo(TRACE_INFO, "Dialog", "Mount New tyre Serial Number",false,true,false);
        mDialogRefNo = 11;
        LayoutInflater li = LayoutInflater.from(mContext);
        final View promptsView = li.inflate(R.layout.dialog_serial_mountnew,
                null);
        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(mContext);

        // get the layout root node
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        Constants.CLICKED_SERIAL_CONFIRM = false;
        // update user inactivity when user interacts with dialog
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });
        alertDialogBuilder.setView(promptsView);
        mAlertDialog = alertDialogBuilder.create();
        mAlertDialog.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                        mDialogRefNo = -1;
                        if (Constants.DO_LOGICAL_SWAP == false
                                && Constants.DO_PHYSICAL_SWAP == false) {
                            resetBoolValue();
                        }
                        if (Constants.CLICKED_SERIAL_CONFIRM == false) {
                            // Remove JobItem
						/*
						 * if ((Constants.onDRAGReturnBoolPS == true) ||
						 * Constants.onGOBool == true) {
						 */
                            if (Constants.INSERT_SWAP_DATA == 1) {
                                if (mJobItemList.size() > 0) {
                                    // Swap cancellation issue with Inspection
                                    SharedPreferences preferences = getActivity().getSharedPreferences(
                                            Constants.GOODYEAR_CONF, 0);
                                    if (!TextUtils.isEmpty(preferences.getString(Constants.INSPECTION, ""))) {
                                        Constants.INSPECTION_ENABLE = (preferences.getString(Constants.INSPECTION, ""));
                                        if (Constants.INSPECTION_ENABLE.equalsIgnoreCase("ON")) {
                                            mJobItemList.remove(mJobItemList
                                                    .size() - 1);
                                            if(!Constants.IS_TYRE_INSPECTED) {
                                                mJobItemList.remove(mJobItemList
                                                        .size() - 1);
                                            }
                                        }else{
                                            mJobItemList.remove(mJobItemList
                                                    .size() - 1);
                                        }
                                    }
                                    Constants.INSERT_SWAP_DATA = 0;
                                    // Fix:: Cancel Issue
                                    resetTyreState();
                                }
                            }
                            // Removing JobCorrection on cancel
                            if (Constants.JOB_CORRECTION_COUNT_SWAP == 2) {
                                if (VehicleSkeletonFragment.mJobcorrectionList
                                        .size() >= 2) {
                                    VehicleSkeletonFragment.mJobcorrectionList
                                            .remove(VehicleSkeletonFragment.mJobcorrectionList
                                                    .size() - 1);
                                    VehicleSkeletonFragment.mJobcorrectionList
                                            .remove(VehicleSkeletonFragment.mJobcorrectionList
                                                    .size() - 1);
                                }
                                Constants.JOB_CORRECTION_COUNT_SWAP = 0;
                            } else if (Constants.JOB_CORRECTION_COUNT_SWAP == 1) {
                                if (VehicleSkeletonFragment.mJobcorrectionList
                                        .size() >= 1) {
                                    VehicleSkeletonFragment.mJobcorrectionList
                                            .remove(VehicleSkeletonFragment.mJobcorrectionList
                                                    .size() - 1);
                                }
                                Constants.JOB_CORRECTION_COUNT_SWAP = 0;
                            }
                            // Handling inspection status on back press
                            if(Constants.TyreIdListToHandlerBackPress!=null) {
                                for (int x = 0; x < Constants.TyreIdListToHandlerBackPress.size(); x++) {
                                    if(Constants.SELECTED_TYRE.getExternalID().equalsIgnoreCase(Constants.TyreIdListToHandlerBackPress.get(x)))
                                    {
                                        Constants.SELECTED_TYRE.setIsInspected(false);
                                        break;
                                    }
                                }
                            }
                            updateTireImages();
                            resetBoolValue();
                            resetTyres();
                            // }

                        }
                        mAlertDialog.dismiss();
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
        Constants.EDITED_SERIAL_NUMBER = "";
        mAlertDialog.show();
        mAlertDialog.setCanceledOnTouchOutside(false);
        mUserInput = (EditText) promptsView
                .findViewById(R.id.editTextDialogUserInput);
        if (null != mEditedUserInput) {
            mUserInput.setText(mEditedUserInput);
            //
            mUserInput
                    .setFilters(new InputFilter[]{new InputFilter.AllCaps(),
                            new InputFilter.LengthFilter(50)});

            // Putting The Cursor At The End Of The Text
            mUserInput.setSelection(mUserInput.getText().length());
        }
        final Button btnOk = (Button) promptsView.findViewById(R.id.sc_btnOk);
        final Button btnCancel = (Button) promptsView
                .findViewById(R.id.sc_btnCancel);
        final TextView mlbl_SerialConfirmation = (TextView) promptsView
                .findViewById(R.id.sc_ConfirmText);
        mlbl_SerialConfirmation.setText(mEnterSerialLabel);
        btnOk.setText(mOKLabel);
        btnCancel.setText(mCancelLabel);
        if (Constants.SELECTED_TYRE.getTyreState() != TyreState.EMPTY) {
            Constants.tempTyreSerial = Constants.SELECTED_TYRE
                    .getSerialNumber().trim();
            Constants.tempTyrePosition = Constants.SELECTED_TYRE.getPosition();
        }
        if (Constants.SECOND_SELECTED_TYRE != null
                && (Constants.SECOND_TYRE_ACTIVE || Constants.DO_LOGICAL_SWAP)) {
            Constants.tempTyreSerial = Constants.SECOND_SELECTED_TYRE
                    .getSerialNumber().trim();
            Constants.tempTyrePosition = Constants.SECOND_SELECTED_TYRE
                    .getPosition();
        }
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                alertDialogBuilder.updateInactivityForDialog();
                Constants.EDITED_SERIAL_NUMBER = mUserInput.getText()
                        .toString().trim();
                   if(!checkSerialNumberValidation(mUserInput.getText().toString())){
                    CommonUtils.notify(serialNumberInvaid, mContext);
                }
                else if (!TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
                            && !Constants.tempTyreSerial
                            .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                            && AutoSwapImpl
                            .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)) {
                        Constants.tempTyrePositionNew = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER);
                            /** Bug 581
                             * Condition added to check whether the Swapped tire is having the same size details
                             * as per the selected tire
                             */
                            if (!AutoSwapImpl.checkIfSizeAllowed(getTireByPosition(Constants.tempTyrePositionNew))){
                                CommonUtils.notify(mSizeNotAllowed, mContext);
                                Constants.tempTyrePosition = "";
                                Constants.EDITED_SERIAL_NUMBER = "";
                                Constants.tempTyreSerial = "";
                                return;
                            }
                    }
                else if (Constants.ONSTATE_INSPECTION){
                    Constants.EDITED_SERIAL_NUMBER = mUserInput.getText()
                            .toString().trim();
                    Intent intent = new Intent(mContext, InspectionActivity.class);
                    intent.putExtra("SERIAL_NO",
                            Constants.SELECTED_TYRE.getSerialNumber());
                    int pos = Character
                            .getNumericValue(Constants.SELECTED_TYRE
                                    .getPosition().charAt(0)) - 1;
                    intent.putExtra(mAXLEPRESSURE_POS, pos);
                    intent.putExtra(mAXLEPRESSURE,
                            mAxleRecommendedPressure.get(pos));
                    intent.putExtra("WHEEL_POS",
                            Constants.SELECTED_TYRE.getPosition());
                    intent.putExtra("FROM_MOUNT_NEW", false);
                    startActivityForResult(intent, INSPECTION_INTENT);
                    mAlertDialog.dismiss();
                }
                else if (Constants.ONSTATE_TOR == true) {
                    Constants.EDITED_SERIAL_NUMBER = mUserInput.getText()
                            .toString().trim();
                    if (!TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
                            && !Constants.tempTyreSerial
                            .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                            && AutoSwapImpl
                            .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)) {
                        Constants.tempTyrePositionNew = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER);
                        if (getTireByPosition(Constants.tempTyrePositionNew)
                                .getTyreState() != TyreState.EMPTY
                                && !TextUtils
                                .isEmpty(Constants.EDITED_SERIAL_NUMBER)
                                && !Constants.tempTyreSerial
                                .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                                && !Constants.DO_LOGICAL_SWAP
                                && !Constants.DO_PHYSICAL_SWAP) {
                            mAlertDialog.dismiss();
                            loadBrandCorrection();

                        } else {
                            Intent startRegrooveOperation = new Intent(
                                    mContext, TurnOnRim.class);
                            int pos = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            startRegrooveOperation.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(pos));
                            startRegrooveOperation.putExtra(mAXLEPRESSURE_POS,
                                    pos);
                            startActivityForResult(startRegrooveOperation,
                                    VehicleSkeletonFragment.TOR_INTENT);
                            mIsTireMenuDisplayed = false;

                            Constants.ONSTATE_TOR = false;
                            mAlertDialog.dismiss();
                        }
                    } else {
                        Intent startRegrooveOperation = new Intent(mContext,
                                TurnOnRim.class);
                        int pos = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        startRegrooveOperation.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(pos));
                        startRegrooveOperation.putExtra(mAXLEPRESSURE_POS, pos);
                        startActivityForResult(startRegrooveOperation,
                                VehicleSkeletonFragment.TOR_INTENT);
                        mIsTireMenuDisplayed = false;

                        Constants.ONSTATE_TOR = false;
                        mAlertDialog.dismiss();
                    }

                } else if (Constants.ONSTATE_DISMOUNT == true) {
                    Constants.EDITED_SERIAL_NUMBER = mUserInput.getText()
                            .toString().trim();
                    if (!TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
                            && !Constants.tempTyreSerial
                            .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                            && AutoSwapImpl
                            .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)) {
                        Constants.tempTyrePositionNew = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER);
                        if (getTireByPosition(Constants.tempTyrePositionNew)
                                .getTyreState() != TyreState.EMPTY
                                && !TextUtils
                                .isEmpty(Constants.EDITED_SERIAL_NUMBER)
                                && !Constants.tempTyreSerial
                                .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                                && !Constants.DO_LOGICAL_SWAP
                                && !Constants.DO_PHYSICAL_SWAP) {
                            mAlertDialog.dismiss();
                            loadBrandCorrection();

                        } else {
                            Intent dismountTireIntent = new Intent(mContext,
                                    DismountTireActivity.class);
                            startActivityForResult(dismountTireIntent,
                                    VehicleSkeletonFragment.DISMOUNT_INTENT);
                            mIsTireMenuDisplayed = false;
                            Constants.ONSTATE_DISMOUNT = false;
                            mAlertDialog.dismiss();
                        }
                    } else {
                        Intent dismountTireIntent = new Intent(mContext,
                                DismountTireActivity.class);
                        startActivityForResult(dismountTireIntent,
                                VehicleSkeletonFragment.DISMOUNT_INTENT);
                        mIsTireMenuDisplayed = false;
                        Constants.ONSTATE_DISMOUNT = false;
                        mAlertDialog.dismiss();
                    }

                } else if (Constants.DO_LOGICAL_SWAP == true) {
                    Constants.UPDATE_SERIAL_DB = true;
                    Constants.onBrandDesignBool = true;
                    Constants.EDITED_SERIAL_NUMBER = mUserInput.getText()
                            .toString().trim();
                    Constants.OnSerialBool = true;
                    editBrandCorrectionForSwap();
                    mAlertDialog.dismiss();
                } else if (Constants.DO_PHYSICAL_SWAP == true) {
                    Constants.EDITED_SERIAL_NUMBER = mUserInput.getText()
                            .toString().trim();
                    if (Constants.ONDRAG
                            && (Constants.SOURCE_TYRE.getSerialNumber()
                            .equalsIgnoreCase(
                                    Constants.EDITED_SERIAL_NUMBER) || Constants.DES_TYRE
                            .getSerialNumber().equalsIgnoreCase(
                                    Constants.EDITED_SERIAL_NUMBER))) {
                        Constants.DUPLICATE_SERIAL_ENTRY = true;
                    }
                    Constants.UPDATE_SERIAL_DB = true;
                    Constants.onBrandDesignBool = true;
                    confirmBrandCorrectionForSwap();
                    mAlertDialog.dismiss();
                } else if (Constants.ONSTATE_WOT == true) {
                    getTyreState();
                    Constants.EDITED_SERIAL_NUMBER = mUserInput.getText()
                            .toString().trim();
                    if (!TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
                            && !Constants.tempTyreSerial
                            .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                            && AutoSwapImpl
                            .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)) {
                        Constants.tempTyrePositionNew = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER);
                        if (getTireByPosition(Constants.tempTyrePositionNew)
                                .getTyreState() != TyreState.EMPTY
                                && !TextUtils
                                .isEmpty(Constants.EDITED_SERIAL_NUMBER)
                                && !Constants.tempTyreSerial
                                .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                                && !Constants.DO_LOGICAL_SWAP
                                && !Constants.DO_PHYSICAL_SWAP) {
                            mAlertDialog.dismiss();
                            loadBrandCorrection();
                        } else {
                            Intent defineTireIntent = new Intent(mContext,
                                    DefineTireActivity.class);
                            defineTireIntent.putExtra("SERIAL_NO", mUserInput
                                    .getText().toString());
                            int pos = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            defineTireIntent.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(pos));
                            defineTireIntent.putExtra(mAXLEPRESSURE_POS, pos);
                            startActivityForResult(defineTireIntent, WOT_INTENT);
                        }
                    } else {
                        Intent defineTireIntent = new Intent(mContext,
                                DefineTireActivity.class);
                        defineTireIntent.putExtra("SERIAL_NO", mUserInput
                                .getText().toString());
                        int pos = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        defineTireIntent.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(pos));
                        defineTireIntent.putExtra(mAXLEPRESSURE_POS, pos);
                        startActivityForResult(defineTireIntent, WOT_INTENT);
                    }
                    mAlertDialog.dismiss();
                } else if (Constants.ONSTATE_REGROOVE == true) {
                    Constants.EDITED_SERIAL_NUMBER = mUserInput.getText()
                            .toString().trim();
                    Intent startRegrooveOperation = new Intent(mContext,
                            RegrooveTireOperation.class);
                    int pos = Character.getNumericValue(Constants.SELECTED_TYRE
                            .getPosition().charAt(0)) - 1;
                    startRegrooveOperation.putExtra(mAXLEPRESSURE,
                            mAxleRecommendedPressure.get(pos));
                    startRegrooveOperation.putExtra(mAXLEPRESSURE_POS, pos);
                    startActivityForResult(startRegrooveOperation,
                            VehicleSkeletonFragment.REGROOVE_INTENT);
                    mAlertDialog.dismiss();
                } else { // Mount New Tire
                    String srno = mUserInput.getText().toString().trim();
                    if (isSerialNumberInVehicle(srno)) {
                        mTempNumber = srno;
                        mAlertDialog.dismiss();
                        populateAlretForDuplicateSerialNumber(srno);
                    } else {
                        Intent mountTireIntent = new Intent(mContext,
                                MountTireActivity.class);
                        mountTireIntent.putExtra("SERIAL_NO", mUserInput
                                .getText().toString());
                        int pos = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        mountTireIntent.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(pos));
                        mountTireIntent.putExtra(mAXLEPRESSURE_POS, pos);
                        startActivityForResult(mountTireIntent,
                                VehicleSkeletonFragment.MOUNT_INTENT);
                        mIsTireMenuDisplayed = false;
                        Constants.ONSTATE_MOUNT = false;
                        mAlertDialog.dismiss();
                    }
                }
                Constants.CLICKED_SERIAL_CONFIRM = true;
                if (15 != mDialogRefNo) {
                    mDialogRefNo = 0;
                }
                // Auto-Swap
                if (!TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
                        && !Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP)
                        && !Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP)
                        && !Constants.tempTyreSerial
                        .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                        && AutoSwapImpl
                        .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)) {
                    Constants.tempTyrePositionNew = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER);
                    if (getTireByPosition(Constants.tempTyrePositionNew)
                            .getTyreState() != TyreState.EMPTY
                            && !TextUtils
                            .isEmpty(Constants.EDITED_SERIAL_NUMBER)
                            && !Constants.tempTyreSerial
                            .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                            && !Constants.DO_LOGICAL_SWAP
                            && !Constants.DO_PHYSICAL_SWAP) {

                        if (Constants.tempTyrePositionNew != null
                                && Constants.SELECTED_TYRE != null) {
                            pos2 = Constants.tempTyrePositionNew;
                            pos1 = Constants.SELECTED_TYRE.getPosition();
                            mSwapFinalConfirmationMessageFromDB = mSwapFinalConfirmationMessage;
                            mSwapFinalConfirmationMessageFromDB = String
                                    .format(mSwapFinalConfirmationMessageFromDB,
                                            pos1, pos2);
                            CommonUtils.notifyLong(
                                    mSwapFinalConfirmationMessageFromDB,
                                    mContext);
                        }
                    }
                }
                Constants.OnSerialBool = true;
                Constants.onBrandBoolForJOC = true;

                // BUG ::: 652 :: N/R should be allowed for SAP
                if(mUserInput.getText().toString().equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED)){
                    Constants.EDITED_SERIAL_NUMBER = Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP;
                }
                if(mUserInput.getText().toString().equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA)){
                    Constants.EDITED_SERIAL_NUMBER = Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP;
                }
                if(Constants.EDITED_SERIAL_NUMBER!=null) {
                    LogUtil.TraceInfo(TRACE_INFO, "Mount New tyre Serial Number", "OK - BT - SN : " + Constants.EDITED_SERIAL_NUMBER, false, false, false);
                }
                else
                {
                    LogUtil.TraceInfo(TRACE_INFO, "Mount New tyre Serial Number", "OK - BT - SN : None", false, false, false);
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                alertDialogBuilder.updateInactivityForDialog();
                if (Constants.CLICKED_SERIAL_CONFIRM == false) {
                    // Remove JobItem
					/*
					 * if ((Constants.onDRAGReturnBoolPS == true) ||
					 * Constants.onGOBool == true) {
					 */
                    if (Constants.INSERT_SWAP_DATA == 1) {
                        if (mJobItemList.size() > 0) {
                            // Swap cancellation issue with Inspection
                            SharedPreferences preferences = getActivity().getSharedPreferences(
                                    Constants.GOODYEAR_CONF, 0);
                            if (!TextUtils.isEmpty(preferences.getString(Constants.INSPECTION, ""))) {
                                Constants.INSPECTION_ENABLE = (preferences.getString(Constants.INSPECTION, ""));
                                if (Constants.INSPECTION_ENABLE.equalsIgnoreCase("ON")) {
                                    mJobItemList.remove(mJobItemList
                                            .size() - 1);
                                    if(!Constants.IS_TYRE_INSPECTED) {
                                        mJobItemList.remove(mJobItemList
                                                .size() - 1);
                                    }
                                }else{
                                    mJobItemList.remove(mJobItemList
                                            .size() - 1);
                                }
                            }
                            Constants.INSERT_SWAP_DATA = 0;
                            // Fix:: Cancel Issue
                            resetTyreState();
                        }
                    }
                    Constants.EDITED_SERIAL_NUMBER = "";
                    // Handling inspection status on back press
                    if(Constants.TyreIdListToHandlerBackPress!=null) {
                        for (int x = 0; x < Constants.TyreIdListToHandlerBackPress.size(); x++) {
                            if(Constants.SELECTED_TYRE.getExternalID().equalsIgnoreCase(Constants.TyreIdListToHandlerBackPress.get(x)))
                            {
                                Constants.SELECTED_TYRE.setIsInspected(false);
                                break;
                            }
                        }
                    }
                    updateTireImages();
                    resetBoolValue();
                    resetTyres();
                    // }
                }
                LogUtil.TraceInfo(TRACE_INFO, "Mount New tyre Serial Number", "Cancel - BT", false, false, false);
                mAlertDialog.dismiss();
                mDialogRefNo = 0;
            }
        });
    }

    /**
     * populate Alert For Duplicate Serial-Number
     */
    private void populateAlretForDuplicateSerialNumber(final String serialNumber) {
        mDialogRefNo = 15;
        try {
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(mContext);
            View promptsView = li.inflate(R.layout.dialog_another_tyre, null);

            final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
                    mContext);
            // set prompts.xml to alertdialog builder
            alertDialogBuilder.setView(promptsView);
            final TextView value_AnotherTyre = (TextView) promptsView
                    .findViewById(R.id.txt_choaseAnotherTyre);
            value_AnotherTyre.setText(mDuplicateSerialLabel);
            alertDialogBuilder.setCancelable(false).setPositiveButton(mOKLabel,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // set user activity
                            alertDialogBuilder.updateInactivityForDialog();

                            Intent mountTireIntent = new Intent(mContext,
                                    MountTireActivity.class);
                            mountTireIntent.putExtra("SERIAL_NO", serialNumber);
                            int pos = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            mountTireIntent.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(pos));
                            mountTireIntent.putExtra(mAXLEPRESSURE_POS, pos);
                            startActivityForResult(mountTireIntent,
                                    VehicleSkeletonFragment.MOUNT_INTENT);
                            mIsTireMenuDisplayed = false;
                            Constants.ONSTATE_MOUNT = false;
                            mDialogRefNo = -1;
                            dialog.dismiss();
                        }
                    });
            alertDialogBuilder.setCancelable(false).setNegativeButton(
                    mCancelLabel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // set user activity
                            alertDialogBuilder.updateInactivityForDialog();
                            mDialogRefNo = -1;
                            dialog.dismiss();
                        }
                    });
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();
            // show it
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * loading PopUp for Serial Correction
     */
    private void loadSerialCorrectionDialog() {

        LogUtil.TraceInfo(TRACE_INFO, "Dialog", "Serial Number Correction",false,true,false);
        try {
            if (serialCorrectionDialog != null
                    && serialCorrectionDialog.isShowing()) {
                serialCorrectionDialog.dismiss();
            }
            mDialogRefNo = 2;
            System.out.println("Load Serial Correction::dialogRefNo:: "
                    + mDialogRefNo);
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(mContext);
            final View promptsView = li.inflate(
                    R.layout.dialog_serial_correction, null);

            // update user activity for dialog layout
            LinearLayout rootNode = (LinearLayout) promptsView
                    .findViewById(R.id.layout_root);
            rootNode.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    InactivityUtils.updateActivityOfUser();
                }
            });

            serialCorrectionDialog = new Dialog(mContext);
            serialCorrectionDialog.setCanceledOnTouchOutside(false);
            // remove title bar from dialog
            serialCorrectionDialog
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            serialCorrectionDialog
                    .setOnDismissListener(new OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (Constants.CLICKED_SERIAL_CONFIRM == false) {
                                // Remove JobItem
                                if (Constants.INSERT_SWAP_DATA == 1) {
                                    if ((Constants.onDRAGReturnBoolPS == true)
                                            || Constants.onGOBool == true
                                            || Constants.DO_LOGICAL_SWAP) {
                                        if (mJobItemList.size() > 0) {
                                            // Swap cancellation issue with Inspection
                                            SharedPreferences preferences = getActivity().getSharedPreferences(
                                                    Constants.GOODYEAR_CONF, 0);
                                            if (!TextUtils.isEmpty(preferences.getString(Constants.INSPECTION, ""))) {
                                                Constants.INSPECTION_ENABLE = (preferences.getString(Constants.INSPECTION, ""));
                                                if (Constants.INSPECTION_ENABLE.equalsIgnoreCase("ON")) {
                                                    mJobItemList.remove(mJobItemList
                                                            .size() - 1);
                                                    if(!Constants.IS_TYRE_INSPECTED) {
                                                        mJobItemList.remove(mJobItemList
                                                                .size() - 1);
                                                    }
                                                }else{
                                                    mJobItemList.remove(mJobItemList
                                                            .size() - 1);
                                                }
                                            }
                                            Constants.INSERT_SWAP_DATA = 0;
                                        }
                                        resetTyreState();
                                    }
                                    updateTireImages();
                                }
                                // Removing JobCorrection on cancel
                                if (Constants.JOB_CORRECTION_COUNT_SWAP == 2) {
                                    if (VehicleSkeletonFragment.mJobcorrectionList
                                            .size() >= 2) {
                                        VehicleSkeletonFragment.mJobcorrectionList
                                                .remove(VehicleSkeletonFragment.mJobcorrectionList
                                                        .size() - 1);
                                        VehicleSkeletonFragment.mJobcorrectionList
                                                .remove(VehicleSkeletonFragment.mJobcorrectionList
                                                        .size() - 1);
                                    }
                                    Constants.JOB_CORRECTION_COUNT_SWAP = 0;
                                } else if (Constants.JOB_CORRECTION_COUNT_SWAP == 1) {
                                    if (VehicleSkeletonFragment.mJobcorrectionList
                                            .size() >= 1) {
                                        VehicleSkeletonFragment.mJobcorrectionList
                                                .remove(VehicleSkeletonFragment.mJobcorrectionList
                                                        .size() - 1);
                                    }
                                    Constants.JOB_CORRECTION_COUNT_SWAP = 0;
                                }
                                // Handling inspection status on back press
                                if(Constants.TyreIdListToHandlerBackPress!=null) {
                                    for (int x = 0; x < Constants.TyreIdListToHandlerBackPress.size(); x++) {
                                        if(Constants.SELECTED_TYRE.getExternalID().equalsIgnoreCase(Constants.TyreIdListToHandlerBackPress.get(x)))
                                        {
                                            Constants.SELECTED_TYRE.setIsInspected(false);
                                            break;
                                        }
                                    }
                                }
                                resetBoolValue();
                                resetTyres();
                                System.out
                                        .println("loadSerialCorrectionDialog onDismissListener");
                                serialNumberInEditMode = "";
                                dialogInEditMode = false;
                                mDialogRefNo = -1;
                                dialog.dismiss();
                            }
                        }
                    });
            final EditText userInput = (EditText) promptsView
                    .findViewById(R.id.editTextDialogUserInput);
            userInput.setFilters(new InputFilter[]{new InputFilter.AllCaps(),
                    new InputFilter.LengthFilter(50)});
            userInput.setEnabled(false);
            userInput.setText("");
            userInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    // update activity of user
                    InactivityUtils.updateActivityOfUser();
                    serialNumberInEditMode = userInput.getText().toString();
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            mlbl_TyreSerial = (TextView) promptsView
                    .findViewById(R.id.sc_SerialText);
            mlbl_TyreDesign = (TextView) promptsView
                    .findViewById(R.id.sc_DesignText);
            mlbl_TyrePressure = (TextView) promptsView
                    .findViewById(R.id.sc_PressureText);
            mlbl_TyrePressure_Measurement = (TextView) promptsView
                    .findViewById(R.id.sc_PSIText);
            mlbl_TyreNSK1 = (TextView) promptsView.findViewById(R.id.lblNSK1);
            mlbl_TyreNSK2 = (TextView) promptsView.findViewById(R.id.lblNSK2);
            mlbl_TyreNSK3 = (TextView) promptsView.findViewById(R.id.lblNSK3);
            mlbl_BrandName = (TextView) promptsView
                    .findViewById(R.id.sc_BrandText);
            mValue_TyreSerial = (TextView) promptsView
                    .findViewById(R.id.sc_SerialValue);
            mValue_TyreDesign = (TextView) promptsView
                    .findViewById(R.id.sc_DesignValue);
            mValue_TyrePressure = (TextView) promptsView
                    .findViewById(R.id.sc_PressureValue);
            mValue_TyreNSK1 = (TextView) promptsView
                    .findViewById(R.id.valueNSK1);
            mValue_TyreNSK2 = (TextView) promptsView
                    .findViewById(R.id.valueNSK2);
            mValue_TyreNSK3 = (TextView) promptsView
                    .findViewById(R.id.valueNSK3);
            mValue_BrandName = (TextView) promptsView
                    .findViewById(R.id.sc_Brandvalue);
            mBtnEdit = (Button) promptsView.findViewById(R.id.sc_btnEdit);
            mBtnConfirm = (Button) promptsView.findViewById(R.id.sc_btnConfirm);
            mlbl_SerialConfirmation = (TextView) promptsView
                    .findViewById(R.id.sc_ConfirmText);
            mBtnEdit.setText(mEditLabel);
            mBtnConfirm.setText(mConfirmLabel);
            if (Constants.SELECTED_TYRE != null) {
                Constants.tempTyreSerial = Constants.SELECTED_TYRE
                        .getSerialNumber().trim();
                Constants.tempTyrePosition = Constants.SELECTED_TYRE
                        .getPosition();
            }
            if (Constants.SECOND_SELECTED_TYRE != null
                    && (Constants.SECOND_TYRE_ACTIVE || (Constants.DO_LOGICAL_SWAP
                    && !TextUtils.isEmpty(Constants.SELECTED_TYRE
                    .getBrandName()) && !Boolean
                    .valueOf(Constants.SELECTED_TYRE.isSpare())))) {
                Constants.tempTyreSerial = Constants.SECOND_SELECTED_TYRE
                        .getSerialNumber().trim();
                Constants.tempTyrePosition = Constants.SECOND_SELECTED_TYRE
                        .getPosition();
            }
            try {
                mlbl_TyreSerial.setText(mSerialNumber);
                mlbl_TyreDesign.setText(mDesign);// 137
                mlbl_TyrePressure.setText(mPressure);
                mlbl_TyrePressure_Measurement.setText(CommonUtils
                        .getPressureUnit(getActivity()));
                mlbl_TyreNSK1.setText(mNskOne);
                mlbl_TyreNSK2.setText(mNskTwo);
                mlbl_TyreNSK3.setText(mNskThree);
                mlbl_BrandName.setText(mBrand_label);
                mlbl_SerialConfirmation.setText(mSerialConfirmationLabel);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            if (Constants.ONSTATE_TOR == true
                    || Constants.ONSTATE_DISMOUNT == true
                    || Constants.ONSTATE_WOT == true) {
                mValue_TyreSerial.setText(Constants.SELECTED_TYRE
                        .getSerialNumber());
                mValue_BrandName
                        .setText(Constants.SELECTED_TYRE.getBrandName());
                mValue_TyreDesign.setText(Constants.SELECTED_TYRE
                        .getDesignDetails());
                mValue_TyrePressure.setText(CommonUtils.getPressureValue(
                        getActivity(), Constants.SELECTED_TYRE.getPressure()));
                mValue_TyreNSK1.setText(Constants.SELECTED_TYRE.getNsk());
                mValue_TyreNSK2.setText(Constants.SELECTED_TYRE.getNsk());
                mValue_TyreNSK3.setText(Constants.SELECTED_TYRE.getNsk());
                if (dialogInEditMode) {
                    if (!serialNumberInEditMode.equals("")) {
                        userInput.setText(serialNumberInEditMode);
                    }
                    userInput.setEnabled(true);
                    userInput.setFocusable(true);
                    userInput.setSelection(userInput.getText().length());
                } else {
                    userInput
                            .setText(Constants.SELECTED_TYRE.getSerialNumber());
                }
            } else {
                settingDataForSerialCorrection(userInput);
                Constants.CURRENT_SERIAL_NUMBER = userInput.getText()
                        .toString();
            }
            INITIAL_SERIAL_NUMBER = mValue_TyreSerial.getText().toString()
                    .trim();
            Constants.onBrandDesignBool = false;
            Constants.onBrandBool = false;
            Constants.CLICKED_SERIAL_CONFIRM = false;
            // create alert dialog
            mBtnConfirm.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    try {
                        // update user activity
                        InactivityUtils.updateActivityOfUser();

                        serialNumberInEditMode = ""; // Reseting Value
                        dialogInEditMode = false;
                        Constants.CLICKED_SERIAL_CONFIRM = true;
                        mUserInputSerialVector.add(userInput.getText()
                                .toString());
                        String userInputSerialNumber = userInput.getText()
                                .toString();
                        if (Constants.SELECTED_TYRE == null
                                && Constants.SECOND_SELECTED_TYRE == null) {
                            Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
                            Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
                        }

                        if (checkSerialNumberValidation(userInputSerialNumber)) {
                            mDialogRefNo = -1;
                            Constants.EDITED_SERIAL_NUMBER = userInput
                                    .getText().toString().trim();
                            // AutoSwap
                            if (!Constants.tempTyreSerial
                                    .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                                    && AutoSwapImpl
                                    .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)) {
                                Constants.tempTyrePositionNew = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER);
                                /** Bug 581
                                 * Condition added to check whether the Swapped tire is having the same size details
                                 * as per the selected tire
                                 */
                                if (!AutoSwapImpl.checkIfSizeAllowed(getTireByPosition(Constants.tempTyrePositionNew))){
                                    CommonUtils.notify(mSizeNotAllowed, mContext);
                                    Constants.tempTyrePosition = "";
                                    Constants.EDITED_SERIAL_NUMBER = "";
                                    Constants.tempTyreSerial = "";
                                    return;
                                }
                            }
                            AutoSwapImpl
                                    .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER);
                            if (Constants.ONSTATE_TOR == true) {
                                Constants.onBrandBool = false;
                                if (!userInput
                                        .getText()
                                        .toString()
                                        .equals(Constants.SELECTED_TYRE
                                                .getSerialNumber())
                                        && !(TextUtils
                                        .isEmpty(Constants.SELECTED_TYRE
                                                .getDesignDetails()) || TextUtils
                                        .isEmpty(Constants.SELECTED_TYRE
                                                .getBrandName()))) {
                                    serialCorrectionDialog.dismiss();
                                    mDialogRefNo = -1;
                                    loadBrandCorrection();
                                } else {
                                    serialCorrectionDialog.dismiss();
                                    mDialogRefNo = -1;
                                    if (isSameSerialNumberInVehicle(Constants.SELECTED_TYRE)) {
                                        // Show duplicate serial number popup
                                        showDuplicateSerialNoAlertForDismount();
                                    } else {
                                        Intent startRegrooveOperation = new Intent(
                                                mContext, TurnOnRim.class);
                                        int pos = Character
                                                .getNumericValue(Constants.SELECTED_TYRE
                                                        .getPosition()
                                                        .charAt(0)) - 1;
                                        startRegrooveOperation.putExtra(
                                                mAXLEPRESSURE,
                                                mAxleRecommendedPressure
                                                        .get(pos));
                                        startRegrooveOperation.putExtra(
                                                mAXLEPRESSURE_POS, pos);
                                        startActivityForResult(
                                                startRegrooveOperation,
                                                VehicleSkeletonFragment.TOR_INTENT);
                                        mIsTireMenuDisplayed = false;

                                        Constants.ONSTATE_TOR = false;
                                    }
                                }
                            } else if (Constants.ONSTATE_DISMOUNT == true) {
                                Constants.onBrandBool = false;
                                String srno = userInput.getText().toString();
                                if (!srno.equals(Constants.SELECTED_TYRE
                                        .getSerialNumber())
                                        && !(TextUtils
                                        .isEmpty(Constants.SELECTED_TYRE
                                                .getDesignDetails()) || TextUtils
                                        .isEmpty(Constants.SELECTED_TYRE
                                                .getBrandName()))) {
                                    // Checking for Brand Correction
                                    serialCorrectionDialog.dismiss();
                                    mDialogRefNo = -1;
                                    loadBrandCorrection();
                                } else {
                                    serialCorrectionDialog.dismiss();
                                    mDialogRefNo = -1;
                                    if (isSameSerialNumberInVehicle(Constants.SELECTED_TYRE)) {
                                        // Show duplicate serial number popup
                                        showDuplicateSerialNoAlertForDismount();
                                    } else {
                                        Intent dismountTireIntent = new Intent(
                                                mContext,
                                                DismountTireActivity.class);
                                        startActivityForResult(
                                                dismountTireIntent,
                                                VehicleSkeletonFragment.DISMOUNT_INTENT);
                                        mIsTireMenuDisplayed = false;
                                        Constants.ONSTATE_DISMOUNT = false;
                                    }
                                }
                            } else if (Constants.ONSTATE_WOT) {
                                // Auto-Swap
                                if (!Constants.tempTyreSerial
                                        .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)) {
                                    if (!TextUtils
                                            .isEmpty(Constants.EDITED_SERIAL_NUMBER)
                                            && AutoSwapImpl
                                            .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)
                                            && !Constants.tempTyreSerial
                                            .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                                            && !Constants.DO_LOGICAL_SWAP
                                            && !Constants.DO_PHYSICAL_SWAP) {
                                        loadBrandCorrection();
                                    } else {
                                        getTyreState();
                                        Constants.ONSTATE_WOT = false;
                                        Intent defineTireIntent = new Intent(
                                                mContext,
                                                DefineTireActivity.class);
                                        String serialNo = Constants.EDITED_SERIAL_NUMBER;
                                        if (TextUtils.isEmpty(serialNo)) {
                                            serialNo = Constants.SELECTED_TYRE
                                                    .getSerialNumber();
                                        }
                                        defineTireIntent.putExtra("SERIAL_NO",
                                                serialNo);
                                        int pos = Character
                                                .getNumericValue(Constants.SELECTED_TYRE
                                                        .getPosition()
                                                        .charAt(0)) - 1;
                                        defineTireIntent.putExtra(
                                                mAXLEPRESSURE,
                                                mAxleRecommendedPressure
                                                        .get(pos));
                                        defineTireIntent.putExtra(
                                                mAXLEPRESSURE_POS, pos);
                                        startActivityForResult(
                                                defineTireIntent, WOT_INTENT);
                                    }
                                } else {
                                    getTyreState();
                                    Constants.ONSTATE_WOT = false;
                                    Intent defineTireIntent = new Intent(
                                            mContext, DefineTireActivity.class);
                                    String serialNo = Constants.EDITED_SERIAL_NUMBER;
                                    if (TextUtils.isEmpty(serialNo)) {
                                        serialNo = Constants.SELECTED_TYRE
                                                .getSerialNumber();
                                    }
                                    defineTireIntent.putExtra("SERIAL_NO",
                                            serialNo);
                                    int pos = Character
                                            .getNumericValue(Constants.SELECTED_TYRE
                                                    .getPosition().charAt(0)) - 1;
                                    defineTireIntent.putExtra(mAXLEPRESSURE,
                                            mAxleRecommendedPressure.get(pos));
                                    defineTireIntent.putExtra(
                                            mAXLEPRESSURE_POS, pos);
                                    startActivityForResult(defineTireIntent,
                                            WOT_INTENT);

                                }
                            } else {
                                if (!(userInput.getText().toString()
                                        .equalsIgnoreCase(Constants.CURRENT_SERIAL_NUMBER))) {
                                    newSerialCnfirmationForSwap(userInput);
                                } else {
                                    serialConfirmationForSwap();
                                }
                            }
                            // Auto-Swap
                            if (!TextUtils
                                    .isEmpty(Constants.EDITED_SERIAL_NUMBER)
                                    && !Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP)
                                    && !Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP)
                                    && !Constants.tempTyreSerial
                                    .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                                    && AutoSwapImpl
                                    .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)) {
                                Constants.tempTyrePositionNew = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER);
                                if (getTireByPosition(
                                        Constants.tempTyrePositionNew)
                                        .getTyreState() != TyreState.EMPTY
                                        && !TextUtils
                                        .isEmpty(Constants.EDITED_SERIAL_NUMBER)
                                        && !Constants.tempTyreSerial
                                        .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                                        && !Constants.DO_LOGICAL_SWAP
                                        && !Constants.DO_PHYSICAL_SWAP) {
                                    if (Constants.tempTyrePositionNew != null
                                            && Constants.SELECTED_TYRE != null) {
                                        pos2 = Constants.tempTyrePositionNew;
                                        pos1 = Constants.SELECTED_TYRE
                                                .getPosition();
										/*
										 * CommonUtils.notifyLong(
										 * "The tyre will be swapped from position axle "
										 * + pos1 + " to " + pos2, mContext);
										 */
                                        mSwapFinalConfirmationMessageFromDB = mSwapFinalConfirmationMessage;
                                        mSwapFinalConfirmationMessageFromDB = String
                                                .format(mSwapFinalConfirmationMessageFromDB,
                                                        pos1, pos2);
                                        CommonUtils
                                                .notifyLong(
                                                        mSwapFinalConfirmationMessageFromDB,
                                                        mContext);

                                    }
                                }
                            }
                            // BUG ::: 652 :: N/R should be allowed for SAP
                            if(Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED)){
                                Constants.EDITED_SERIAL_NUMBER = Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP;
                            }
                            if(Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA)){
                                Constants.EDITED_SERIAL_NUMBER = Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP;
                            }
                            if(Constants.EDITED_SERIAL_NUMBER!=null) {
                                LogUtil.TraceInfo(TRACE_INFO, "Serial Number Correction", "Confirm - BT - SN : " + Constants.EDITED_SERIAL_NUMBER,false,false,false);
                            }
                            else
                            {
                                LogUtil.TraceInfo(TRACE_INFO, "Serial Number Correction", "Confirm",false,false,false);
                            }
                            serialCorrectionDialog.cancel();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
            mBtnEdit.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    // update user activity
                    InactivityUtils.updateActivityOfUser();

                    Constants.EDITED_SERIAL_NUMBER = userInput.getText()
                            .toString().trim();
                    userInput.setText("");
                    Constants.EDITED_SERIAL_NUMBER = "";
                    userInput.setEnabled(true);
                    userInput.setFocusable(true);
                    userInput.requestFocus();
                    userInput.setFocusableInTouchMode(true);
                    dialogInEditMode = true;
                    Constants.CLICKED_SERIAL_CONFIRM = false;
                    LogUtil.TraceInfo(TRACE_INFO, "Serial Number Correction", "Edit-BT",false,false,false);
                }
            });
            serialCorrectionDialog.setContentView(promptsView);
            serialCorrectionDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            serialCorrectionDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * returns true if the entered serial is matching Reg-Ex
     *
     * @param entrerdSerial
     * @return
     */
    private boolean checkSerialNumberValidation(String entrerdSerial) {
        if (!TextUtils.isEmpty(INITIAL_SERIAL_NUMBER) && INITIAL_SERIAL_NUMBER.equalsIgnoreCase(entrerdSerial)) {
            return true;
        }
        if (entrerdSerial.equalsIgnoreCase("")) {
            CommonUtils.notify(serialNumberInvaid, mContext);
        } else if (!containsDigit(entrerdSerial)){
            //Condition check for NR and NA
            if (entrerdSerial
                    .equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED)) {
                return true;
             }else if (entrerdSerial
                    .equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA)){
                return true;
            }else{
                if(serialNumberInvaid!=null && entrerdSerial!=null) {
                    LogUtil.TraceInfo(TAG, "Serial Number Correction - Notify", serialNumberInvaid + " SN : "+entrerdSerial,false,false,false);
                }
                else
                {
                    LogUtil.TraceInfo(TAG, "Serial Number Correction - Notify","serial Number Invaid",false,false,false);
                }
                CommonUtils.notify(serialNumberInvaid, mContext);
            }
        } else if (entrerdSerial.trim().length() < Constants.MIN_LENGTH_SERIALNUMBER){
            //Condition check for NR and NA
            if (entrerdSerial
                    .equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED)) {
                return true;
            }else if (entrerdSerial
                    .equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA)){
                return true;
            }else{
                if(serialNumberInvaid!=null) {
                    LogUtil.TraceInfo(TAG, "Serial Number Correction - Notify", serialNumberInvaid,false,false,false);
                }
                else
                {
                    LogUtil.TraceInfo(TAG, "Serial Number Correction - Notify","serial Number Invaid",false,false,false);
                }
                CommonUtils.notify(serialNumberInvaid, mContext);
            }
        }
       else {
            return true;
        }
        return false;
    }

    /**
     * Updating Serial in DB after Job Correction
     */
    public static void updateCorrectedSerialNumberInDB() {
        if(Constants.DO_PHYSICAL_SWAP || Constants.DO_LOGICAL_SWAP) {
            if (!isSerialNumberInVehicle(Constants.EDITED_SERIAL_NUMBER)) {
                Constants.UPDATE_SERIAL_DB = true;
                if (Constants.SECOND_SELECTED_TYRE != null
                        && ((Constants.onDRAGReturnBoolPS && Constants.onDragPSBool == true)
                        || (Constants.onDragBool_LS == true && !Constants.LOGICAL_SWAP_INITIAL)
                        || (Constants.onGOBool == true && !Constants.LOGICAL_SWAP_INITIAL) || (Constants.DO_PHYSICAL_SWAP && Constants.onGOBool))) {
                    Constants.CURRENT_SERIAL_NUMBER = Constants.SECOND_SELECTED_TYRE
                            .getSerialNumber();
                    Constants.TYRE_ID = Constants.SECOND_SELECTED_TYRE
                            .getExternalID();
                } else {
                    Constants.CURRENT_SERIAL_NUMBER = Constants.SELECTED_TYRE
                            .getSerialNumber();
                    Constants.TYRE_ID = Constants.SELECTED_TYRE.getExternalID();
                }
                updateJobCorrection();
                Constants.OnSerialBool = false;
            }
        }
        else{
            Constants.UPDATE_SERIAL_DB = true;
            if (Constants.SECOND_SELECTED_TYRE != null
                    && ((Constants.onDRAGReturnBoolPS && Constants.onDragPSBool == true)
                    || (Constants.onDragBool_LS == true && !Constants.LOGICAL_SWAP_INITIAL)
                    || (Constants.onGOBool == true && !Constants.LOGICAL_SWAP_INITIAL) || (Constants.DO_PHYSICAL_SWAP && Constants.onGOBool))) {
                Constants.CURRENT_SERIAL_NUMBER = Constants.SECOND_SELECTED_TYRE
                        .getSerialNumber();
                Constants.TYRE_ID = Constants.SECOND_SELECTED_TYRE
                        .getExternalID();
            } else {
                Constants.CURRENT_SERIAL_NUMBER = Constants.SELECTED_TYRE
                        .getSerialNumber();
                Constants.TYRE_ID = Constants.SELECTED_TYRE.getExternalID();
            }
            updateJobCorrection();
            Constants.OnSerialBool = false;
        }
    }

    /**
     * Update Brand in DB after JOb Correction
     */
    public static void updateCorrectedBrandInDB() {
        if(Constants.DO_PHYSICAL_SWAP || Constants.DO_LOGICAL_SWAP){
            if (!isSerialNumberInVehicle(Constants.EDITED_SERIAL_NUMBER)) {
                if (Constants.SECOND_SELECTED_TYRE != null
                        && ((Constants.onDRAGReturnBoolPS && Constants.onDragPSBool == true)
                        || (Constants.onDragBool_LS == true && !Constants.LOGICAL_SWAP_INITIAL)
                        || (Constants.onGOBool == true && !Constants.LOGICAL_SWAP_INITIAL) || (Constants.DO_PHYSICAL_SWAP && Constants.onGOBool))) {
                    Constants.CURRENT_SERIAL_NUMBER = Constants.SECOND_SELECTED_TYRE
                            .getSerialNumber();
                    Constants.TYRE_ID = Constants.SECOND_SELECTED_TYRE
                            .getExternalID();
                } else {
                    Constants.CURRENT_SERIAL_NUMBER = Constants.SELECTED_TYRE
                            .getSerialNumber();
                    Constants.TYRE_ID = Constants.SELECTED_TYRE.getExternalID();
                }
                updateJobCorrectionForBrand();
                Constants.onBrandBool = false;
            }
        }
        else{
                if (Constants.SECOND_SELECTED_TYRE != null
                        && ((Constants.onDRAGReturnBoolPS && Constants.onDragPSBool == true)
                        || (Constants.onDragBool_LS == true && !Constants.LOGICAL_SWAP_INITIAL)
                        || (Constants.onGOBool == true && !Constants.LOGICAL_SWAP_INITIAL) || (Constants.DO_PHYSICAL_SWAP && Constants.onGOBool))) {
                    Constants.CURRENT_SERIAL_NUMBER = Constants.SECOND_SELECTED_TYRE
                            .getSerialNumber();
                    Constants.TYRE_ID = Constants.SECOND_SELECTED_TYRE
                            .getExternalID();
                } else {
                    Constants.CURRENT_SERIAL_NUMBER = Constants.SELECTED_TYRE
                            .getSerialNumber();
                    Constants.TYRE_ID = Constants.SELECTED_TYRE.getExternalID();
                }
                updateJobCorrectionForBrand();
                Constants.onBrandBool = false;
        }

    }

    /**
     * Populating Data on Serial Correction PopUp
     */
    private void settingDataForSerialCorrection(EditText userInput) {
        if (Constants.onDRAGReturnBoolPS == true) {
            mValue_TyreSerial.setText(Constants.DES_TYRE.getSerialNumber());
            mValue_BrandName.setText(Constants.DES_TYRE.getBrandName());
            mValue_TyreDesign.setText(Constants.DES_TYRE.getDesignDetails());
            mValue_TyrePressure.setText(Constants.DES_TYRE.getPressure());
            mValue_TyreNSK1.setText(Constants.DES_TYRE.getNsk());
            mValue_TyreNSK2.setText(Constants.DES_TYRE.getNsk2());
            mValue_TyreNSK3.setText(Constants.DES_TYRE.getNsk3());
            userInput.setText(Constants.DES_TYRE.getSerialNumber());
            if (!mEditedUserInput.equalsIgnoreCase("")) {
                userInput.setText(mEditedUserInput);
            }
        } else if (Constants.onDRAGReturnBoolPS == false) {
            if (Constants.ONDRAG
                    && (Constants.onDragPSBool == true || Constants.onBrandBlank == true)) {
                mValue_TyreSerial.setText(Constants.SOURCE_TYRE
                        .getSerialNumber());
                mValue_BrandName.setText(Constants.SOURCE_TYRE.getBrandName());
                mValue_TyreDesign.setText(Constants.SOURCE_TYRE
                        .getDesignDetails());
                mValue_TyrePressure
                        .setText(Constants.SOURCE_TYRE.getPressure());
                mValue_TyreNSK1.setText(Constants.SOURCE_TYRE.getNsk());
                mValue_TyreNSK2.setText(Constants.SOURCE_TYRE.getNsk2());
                mValue_TyreNSK3.setText(Constants.SOURCE_TYRE.getNsk3());
                userInput.setText(Constants.SOURCE_TYRE.getSerialNumber());
                if (!mEditedUserInput.equalsIgnoreCase("")) {
                    mValue_TyreSerial.setText(mEditedUserInput);
                }
            } else {
                if (Constants.onDragBool_LS == true) {
                    mValue_TyreSerial.setText(Constants.DES_TYRE
                            .getSerialNumber());
                    mValue_BrandName.setText(Constants.DES_TYRE.getBrandName());
                    mValue_TyreDesign.setText(Constants.DES_TYRE
                            .getDesignDetails());
                    mValue_TyrePressure.setText(Constants.DES_TYRE
                            .getPressure());
                    mValue_TyreNSK1.setText(Constants.DES_TYRE.getNsk());
                    mValue_TyreNSK2.setText(Constants.DES_TYRE.getNsk2());
                    mValue_TyreNSK3.setText(Constants.DES_TYRE.getNsk3());
                    userInput.setText(Constants.DES_TYRE.getSerialNumber());
                    if (!mEditedUserInput.equalsIgnoreCase("")) {
                        mValue_TyreSerial.setText(mEditedUserInput);
                    }
                } else {
                    if (Constants.ONPRESS
                            && (Constants.onReturnBool == false || Constants.onBrandBlank == true)) {
                        if (Constants.onBrandBlank == false
                                && Constants.DO_LOGICAL_SWAP) {

                            mValue_TyreSerial
                                    .setText(Constants.SECOND_SELECTED_TYRE
                                            .getSerialNumber());
                            mValue_BrandName
                                    .setText(Constants.SECOND_SELECTED_TYRE
                                            .getBrandName());
                            mValue_TyreDesign
                                    .setText(Constants.SECOND_SELECTED_TYRE
                                            .getDesignDetails());
                            mValue_TyrePressure
                                    .setText(Constants.SECOND_SELECTED_TYRE
                                            .getPressure());
                            mValue_TyreNSK1
                                    .setText(Constants.SECOND_SELECTED_TYRE
                                            .getNsk());
                            mValue_TyreNSK2
                                    .setText(Constants.SECOND_SELECTED_TYRE
                                            .getNsk2());
                            mValue_TyreNSK3
                                    .setText(Constants.SECOND_SELECTED_TYRE
                                            .getNsk3());
                            userInput.setText(Constants.SECOND_SELECTED_TYRE
                                    .getSerialNumber());
                            if (!mEditedUserInput.equalsIgnoreCase("")) {
                                mValue_TyreSerial.setText(mEditedUserInput);
                            }
                        } else {
                            mValue_TyreSerial.setText(Constants.SELECTED_TYRE
                                    .getSerialNumber());
                            mValue_BrandName.setText(mSelectedTyre
                                    .getBrandName());
                            mValue_TyreDesign.setText(mSelectedTyre
                                    .getDesignDetails());
                            mValue_TyrePressure.setText(mSelectedTyre
                                    .getPressure());
                            mValue_TyreNSK1.setText(mSelectedTyre.getNsk());
                            mValue_TyreNSK2.setText(mSelectedTyre.getNsk2());
                            mValue_TyreNSK3.setText(mSelectedTyre.getNsk3());
                            userInput.setText(Constants.SELECTED_TYRE
                                    .getSerialNumber());
                            if (!mEditedUserInput.equalsIgnoreCase("")) {
                                mValue_TyreSerial.setText(mEditedUserInput);
                            }
                        }
                    } else if (Constants.onReturnBool == true) {
                        mValue_TyreSerial
                                .setText(Constants.SECOND_SELECTED_TYRE
                                        .getSerialNumber());
                        mValue_BrandName.setText(Constants.SECOND_SELECTED_TYRE
                                .getBrandName());
                        mValue_TyreDesign
                                .setText(Constants.SECOND_SELECTED_TYRE
                                        .getDesignDetails());
                        mValue_TyrePressure
                                .setText(Constants.SECOND_SELECTED_TYRE
                                        .getPressure());
                        mValue_TyreNSK1.setText(Constants.SECOND_SELECTED_TYRE
                                .getNsk());
                        mValue_TyreNSK2.setText(Constants.SECOND_SELECTED_TYRE
                                .getNsk2());
                        mValue_TyreNSK3.setText(Constants.SECOND_SELECTED_TYRE
                                .getNsk3());
                        userInput.setText(Constants.SECOND_SELECTED_TYRE
                                .getSerialNumber());
                        if (!mEditedUserInput.equalsIgnoreCase("")) {
                            mValue_TyreSerial.setText(mEditedUserInput);
                        }
                    }
                }
            }
        }
    }

    /**
     * Loading Brand Correction PopUp
     */
    private void loadBrandCorrection() {
        try {
            LogUtil.TraceInfo(TAG,"Dialog","Brand Correction",false,true,false);
            mDialogRefNo = 3;
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(mContext);
            View promptsView = li.inflate(R.layout.dialog_brand_correction,
                    null);
            final EjobAlertDialog brandCorrectionDialogBuilder = new EjobAlertDialog(
                    mContext);
            // set prompts.xml to alert dialog builder
            brandCorrectionDialogBuilder.setView(promptsView);

            // update user activity for dialog layout
            LinearLayout rootNode = (LinearLayout) promptsView
                    .findViewById(R.id.layout_root);
            rootNode.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    InactivityUtils.updateActivityOfUser();
                }
            });
            mValue_TyreBrand = (TextView) promptsView
                    .findViewById(R.id.bc_BrandValue);
            nValue_TyreBrandDesign = (TextView) promptsView
                    .findViewById(R.id.bc_DesignValue);
            mlvl_TyreBrandConfirm = (TextView) promptsView
                    .findViewById(R.id.bc_confirmText);
            mlbl_TyreBrand = (TextView) promptsView
                    .findViewById(R.id.bc_BrandText);
            mlbl_TyreBrandDesign = (TextView) promptsView
                    .findViewById(R.id.bc_DesignText);
            try {
                //internal bug fixed: rotation issue
                mlbl_TyreBrand.setText(mBrand);
                mlbl_TyreBrandDesign.setText(mDesign);
                mlvl_TyreBrandConfirm.setText(mBrandConfirmLabel);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (Constants.ONSTATE_TOR == true
                    || Constants.ONSTATE_DISMOUNT == true
                    || Constants.ONSTATE_WOT == true) {
                if (!Constants.tempTyreSerial
                        .equalsIgnoreCase(Constants.tempTyrePositionNew)
                        && !Constants.tempTyreSerial
                        .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)) {
                    if (!TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
                            && AutoSwapImpl
                            .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)
                            && !Constants.tempTyreSerial
                            .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                            && !Constants.DO_LOGICAL_SWAP
                            && !Constants.DO_PHYSICAL_SWAP) {
                        mValue_TyreBrand.setText(getTireByPosition(
                                Constants.tempTyrePositionNew).getBrandName());
                        nValue_TyreBrandDesign.setText(getTireByPosition(
                                Constants.tempTyrePositionNew)
                                .getDesignDetails());
                    } else {
                        mValue_TyreBrand.setText(Constants.SELECTED_TYRE
                                .getBrandName());
                        nValue_TyreBrandDesign.setText(Constants.SELECTED_TYRE
                                .getDesignDetails());
                    }
                } else {
                    mValue_TyreBrand.setText(Constants.SELECTED_TYRE
                            .getBrandName());
                    nValue_TyreBrandDesign.setText(Constants.SELECTED_TYRE
                            .getDesignDetails());
                }
            } else if (Constants.ONSTATE_WOT == true) {
                Constants.tempTyrePositionNew = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER);
                mValue_TyreBrand.setText(getTireByPosition(
                        Constants.tempTyrePositionNew).getBrandName());
                nValue_TyreBrandDesign.setText(getTireByPosition(
                        Constants.tempTyrePositionNew).getDesignDetails());
            } else {
                settingDataForBrandCorrection();
            }
            // set dialog message
            brandCorrectionDialogBuilder.setCancelable(false)
                    .setPositiveButton(mConfirmLabel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    // update user activity
                                    brandCorrectionDialogBuilder
                                            .updateInactivityForDialog();
                                    LogUtil.TraceInfo(TAG, "Brand Correction", "Confirm - BT", false, false, false);
                                    mDialogRefNo = -1;
                                    if (Constants.ONSTATE_TOR == true) {

                                        Intent startRegrooveOperation = new Intent(
                                                mContext, TurnOnRim.class);
                                        int pos = Character
                                                .getNumericValue(Constants.SELECTED_TYRE
                                                        .getPosition()
                                                        .charAt(0)) - 1;
                                        startRegrooveOperation.putExtra(
                                                mAXLEPRESSURE,
                                                mAxleRecommendedPressure
                                                        .get(pos));
                                        startRegrooveOperation.putExtra(
                                                mAXLEPRESSURE_POS, pos);
                                        startActivityForResult(
                                                startRegrooveOperation,
                                                VehicleSkeletonFragment.TOR_INTENT);
                                        mIsTireMenuDisplayed = false;

                                        Constants.ONSTATE_TOR = false;

                                    } else if (Constants.ONSTATE_DISMOUNT == true) {

                                        Intent dismountTireIntent = new Intent(
                                                mContext,
                                                DismountTireActivity.class);
                                        startActivityForResult(
                                                dismountTireIntent,
                                                VehicleSkeletonFragment.DISMOUNT_INTENT);
                                        mIsTireMenuDisplayed = false;
                                        Constants.ONSTATE_DISMOUNT = false;
                                    } else if (Constants.ONSTATE_WOT == true) {
                                        getTyreState();
                                        Constants.ONSTATE_WOT = false;
                                        Intent defineTireIntent = new Intent(
                                                mContext,
                                                DefineTireActivity.class);
                                        String serialNo = Constants.EDITED_SERIAL_NUMBER;
                                        if (TextUtils.isEmpty(serialNo)) {
                                            serialNo = Constants.SELECTED_TYRE
                                                    .getSerialNumber();
                                        }
                                        defineTireIntent.putExtra("SERIAL_NO",
                                                serialNo);
                                        int pos = Character
                                                .getNumericValue(Constants.SELECTED_TYRE
                                                        .getPosition()
                                                        .charAt(0)) - 1;
                                        defineTireIntent.putExtra(
                                                mAXLEPRESSURE,
                                                mAxleRecommendedPressure
                                                        .get(pos));
                                        defineTireIntent.putExtra(
                                                mAXLEPRESSURE_POS, pos);
                                        startActivityForResult(
                                                defineTireIntent, WOT_INTENT);

                                    } else {
                                        confirmBrandCorrectionForSwap();
                                    }
                                }
                            });
            brandCorrectionDialogBuilder.setCancelable(false)
                    .setNegativeButton(mEditLabel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    // update user activity
                                    brandCorrectionDialogBuilder
                                            .updateInactivityForDialog();
                                    LogUtil.TraceInfo(TAG, "Brand Correction", "Edit - BT", false, false, false);
                                    mDialogRefNo = -1;
                                    if (Constants.ONSTATE_TOR == true) {

                                        Constants.onBrandBool = true;
                                        Intent startRegrooveOperation = new Intent(
                                                mContext, TurnOnRim.class);
                                        int pos = Character
                                                .getNumericValue(Constants.SELECTED_TYRE
                                                        .getPosition()
                                                        .charAt(0)) - 1;
                                        startRegrooveOperation.putExtra(
                                                mAXLEPRESSURE,
                                                mAxleRecommendedPressure
                                                        .get(pos));
                                        startRegrooveOperation.putExtra(
                                                mAXLEPRESSURE_POS, pos);
                                        startActivityForResult(
                                                startRegrooveOperation,
                                                VehicleSkeletonFragment.TOR_INTENT);
                                        mIsTireMenuDisplayed = false;

                                        Constants.ONSTATE_TOR = false;

                                    } else if (Constants.ONSTATE_DISMOUNT == true) {
                                        Constants.onBrandBool = true;
                                        Intent dismountTireIntent = new Intent(
                                                mContext,
                                                DismountTireActivity.class);
                                        startActivityForResult(
                                                dismountTireIntent,
                                                VehicleSkeletonFragment.DISMOUNT_INTENT);
                                        mIsTireMenuDisplayed = false;
                                        Constants.ONSTATE_DISMOUNT = false;
                                    } else if (Constants.ONSTATE_WOT == true) {
                                        getTyreState();
                                        Constants.onBrandBool = true;
                                        Constants.ONSTATE_WOT = false;
                                        Intent defineTireIntent = new Intent(
                                                mContext,
                                                DefineTireActivity.class);
                                        String serialNo = Constants.EDITED_SERIAL_NUMBER;
                                        if (TextUtils.isEmpty(serialNo)) {
                                            serialNo = Constants.SELECTED_TYRE
                                                    .getSerialNumber();
                                        }
                                        defineTireIntent.putExtra("SERIAL_NO",
                                                serialNo);
                                        int pos = Character
                                                .getNumericValue(Constants.SELECTED_TYRE
                                                        .getPosition()
                                                        .charAt(0)) - 1;
                                        defineTireIntent.putExtra(
                                                mAXLEPRESSURE,
                                                mAxleRecommendedPressure
                                                        .get(pos));
                                        defineTireIntent.putExtra(
                                                mAXLEPRESSURE_POS, pos);
                                        startActivityForResult(
                                                defineTireIntent, WOT_INTENT);

                                    } else {
                                        Constants.onBrandDesignBool = true;
                                        editBrandCorrectionForSwap();
                                    }
                                }
                            });
            // create alert dialog
            AlertDialog alertDialog = brandCorrectionDialogBuilder.create();

            // show it
            alertDialog.show();
            alertDialog.setCanceledOnTouchOutside(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Populating Data on Brand Correction PopUP
     */
    private void settingDataForBrandCorrection() {
        // Auto-Swap
        if (!TextUtils.isEmpty(Constants.tempTyrePositionNew)
                && !TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
                && !Constants.tempTyreSerial
                .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
                && AutoSwapImpl
                .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)
                && !Constants.tempTyreSerial
                .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)) {
            mValue_TyreBrand.setText(getTireByPosition(
                    Constants.tempTyrePositionNew).getBrandName());
            nValue_TyreBrandDesign.setText(getTireByPosition(
                    Constants.tempTyrePositionNew).getDesignDetails());
        } else {
            if (Constants.onDRAGReturnBoolPS == true) {
                mValue_TyreBrand.setText(Constants.DES_TYRE.getBrandName());
                nValue_TyreBrandDesign.setText(Constants.DES_TYRE
                        .getDesignDetails());
            } else if (Constants.onDRAGReturnBoolPS == false) {
                if (Constants.onDragPSBool == true) {

                    mValue_TyreBrand.setText(Constants.SOURCE_TYRE
                            .getBrandName());
                    nValue_TyreBrandDesign.setText(Constants.SOURCE_TYRE
                            .getDesignDetails());
                } else {
                    if (Constants.onDragBool_LS == true) {
                        if (Constants.onBrandBlank) {
                            mValue_TyreBrand.setText(Constants.SOURCE_TYRE
                                    .getBrandName());
                            nValue_TyreBrandDesign
                                    .setText(Constants.SOURCE_TYRE
                                            .getDesignDetails());
                        } else {
                            mValue_TyreBrand.setText(Constants.DES_TYRE
                                    .getBrandName());
                            nValue_TyreBrandDesign.setText(Constants.DES_TYRE
                                    .getDesignDetails());
                        }
                    } else {
                        if (Constants.onReturnBool == false
                                || Constants.onBrandBlank) {
                            mValue_TyreBrand.setText(Constants.SELECTED_TYRE
                                    .getBrandName());
                            nValue_TyreBrandDesign
                                    .setText(Constants.SELECTED_TYRE
                                            .getDesignDetails());
                        } else if (Constants.onReturnBool == true) {
                            mValue_TyreBrand
                                    .setText(Constants.SECOND_SELECTED_TYRE
                                            .getBrandName());
                            nValue_TyreBrandDesign
                                    .setText(Constants.SECOND_SELECTED_TYRE
                                            .getDesignDetails());
                        }
                    }
                }
            }
        }
    }

    /**
     * fetching tire Details for Serial Correction
     *
     * @param userInput
     */
    private void getTyreDetailsForSerialCorrrection(EditText userInput) {
        Cursor mCursor = null;
        try {
            mDbHelper = new DatabaseAdapter(mContext);
            mDbHelper.createDatabase();
            // load Vendor Name
            mCursor = mDbHelper.getDeatilsforSerialValidation(userInput
                    .getText().toString());
            if (CursorUtils.isValidCursor(mCursor)) {
                mTyreRIMSize = mCursor.getInt(mCursor.getColumnIndex("RIM"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CursorUtils.closeCursor(mCursor);
        }
    }

    /**
     * fetching tire serial Number
     */
    private void getTyreSerialNumberForValidation() {
        Cursor mCursor = null;
        try {
            // load Vendor Name
            mTyreSerialFromDB = new ArrayList<String>();
            mCursor = mDbHelper.getTyreSerialNumberForValidation();
            if (CursorUtils.isValidCursor(mCursor)) {
                mCursor.moveToFirst();
                while (!mCursor.isAfterLast()) {
                    mTyreSerialFromDB.add(mCursor.getString(mCursor
                            .getColumnIndex("SerialNumber"))); // add the item
                    mCursor.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns true serial is validated
     *
     * @param enteredSerialNumber
     * @param userInput
     * @return
     */
    private Boolean serialNumberValidation(String enteredSerialNumber,
                                           EditText userInput) {
        try {
            enteredSerialNumber = userInput.getText().toString();
            if (!enteredSerialNumber.equalsIgnoreCase("")) {
                boolean check = true;
                getTyreSerialNumberForValidation();

                Iterator<String> itrate = mTyreSerialFromDB.iterator();
                while (itrate.hasNext()) {
                    String currentTire = itrate.next();
                    if (currentTire.equals(userInput.getText().toString())) {
                        check = false;
                        break;
                    } else {
                        check = true;
                    }
                }
                if (check == true) {
                    return false;
                } else {
                    return true;
                }
            }
            if (tyreInfo.get(3).getRim()
                    .equalsIgnoreCase(String.valueOf(mTyreRIMSize))) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * ChoiceTouchListener will handle touch events on dragable views
     */
    private final class ChoiceTouchListener implements OnTouchListener {
        int pos1x, pos1y, pos2x, pos2y;
        // variable for counting two successive up-down events
        int clickCount = 0;
        // variable for storing the time of first click
        long startTime;

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                pos2x = (int) motionEvent.getX();
                pos2y = (int) motionEvent.getY();
                if (Math.abs(pos2x - pos1x) > 12
                        || Math.abs(pos2y - pos1y) > 12) {
                    ClipData data = ClipData.newPlainText("", "");
                    DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                            view);

                    if (Constants.onReturnBool) {
                        // Do nothing
                    } else if ((!Constants.COMESFROMVIEW && !mIsNotesInEditMode)) {
                        view.startDrag(data, shadowBuilder, view, 0);
                    }
                    view.setVisibility(View.VISIBLE);
                }
                return true;
            } else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                pos1x = (int) motionEvent.getX();
                pos1y = (int) motionEvent.getY();
                t1 = motionEvent.getEventTime();
                return true;
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                try {
                    t2 = motionEvent.getEventTime();
                    clickCount++;
                    if (clickCount == 1) {
                        startTime = System.currentTimeMillis();
                    } else if (clickCount == 2) {
                        long duration = System.currentTimeMillis() - startTime;
                        if (duration <= 500) {
                            if (!Constants.COMESFROMVIEW) {
                                if (mIsNotesInEditMode) {
                                    CommonUtils.notifyLong(mSaveNoteMessage,
                                            mContext);
                                } else {
                                    showTireAction(view);
                                }
                            }
                            clickCount = 0;
                            duration = 0;
                        } else {
                            clickCount = 1;
                            startTime = System.currentTimeMillis();
                        }
                    }
                    pos2x = (int) motionEvent.getX();
                    pos2y = (int) motionEvent.getY();
                    if (Math.abs(pos2x - pos1x) < 12
                            || Math.abs(pos2y - pos1y) < 12) {
                        if (mIsNotesInEditMode) {
                            CommonUtils.notifyLong(mSaveNoteMessage, mContext);
                        } else {
                            showTireDetails(view);
                        }
                    }
                } catch (Exception e) {
                    clickCount = 0;
                    e.printStackTrace();
                }
                return true;

            } else {
                return false;
            }
        }
    }

    /**
     * Class containing the drag listener events
     *
     * @author amitkumar.h
     */
    private class ChoiceDragListener implements OnDragListener {
        @Override
        public boolean onDrag(View v, DragEvent event) {
            if (mIsNotesInEditMode) {
                CommonUtils.notifyLong(mSaveNoteMessage, mContext);
                return false;
            }
            LinearLayout target = (LinearLayout) v;
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;
                case DragEvent.ACTION_DROP:
                    System.out.println("OnDrop onDismissListener");
                    resetBoolValue();
                    Constants.SELECTED_TYRE = null;
                    Constants.SECOND_SELECTED_TYRE = null;
                    Constants.SOURCE_TYRE = null;
                    Constants.DES_TYRE = null;
                    Constants.ONDRAG = true;
                    View view = (View) event.getLocalState();
                    LinearLayout source = (LinearLayout) view.getParent();
                    view.setVisibility(View.VISIBLE);
                    mSourceTyrePosition = (String) source.getTag();
                    mTargetTyrePosition = (String) target.getTag();
                    Constants.SOURCE_TYRE = getTireByPosition(mSourceTyrePosition);
                    Constants.DES_TYRE = getTireByPosition(mTargetTyrePosition);
                    // check swap tires are from diffrerent axle.
                    // for size check.
                    if (CommonUtils.checkDifferentSizeForAxle()) {
                        CommonUtils.notify(mSizeNotAllowed, mContext);
                        resetBoolValue();
                        resetTyres();
                    } else {
                        if (CommonUtils.checkDifferentAxleForSwap()
                                && !Constants.DIFFERENT_SIZE_ALLOWED) {
                            CommonUtils.notifyLong(mSizeNotAllowed, mContext);
                            // for canceling swap (as size dont match)
                            // useless condition is INSERT_SWAP_DATA will not be 1
                            if (Constants.INSERT_SWAP_DATA == 1) {
                                if ((Constants.onDRAGReturnBoolPS == true)
                                        || Constants.onGOBool == true
                                        || Constants.DO_LOGICAL_SWAP) {
                                    if (mJobItemList.size() > 0) {
                                        // remove job item
                                        mJobItemList
                                                .remove(mJobItemList.size() - 1);
                                        Constants.INSERT_SWAP_DATA = 0;
                                    }
                                    resetTyreState();
                                }
                                updateTireImages();
                            }
                            resetTyres();
                            resetBoolValue();
                        } else if (Constants.SOURCE_TYRE == Constants.DES_TYRE) {
                            CommonUtils.notify(CHOOSE_DIFFERENT_TIRE_TO_SWAP,
                                    mContext);
                            resetTyres();
                            resetBoolValue();
                        } else if (Constants.SOURCE_TYRE.getTyreState() == TyreState.EMPTY) {
                            CommonUtils.notify(CHOOSE_DIFFERENT_TIRE_TO_SWAP,
                                    mContext);
                        } else if (Constants.DES_TYRE.getTyreState() == TyreState.EMPTY) {/* Bug 729:Restrict swap on a dismounted tyre*/
                            CommonUtils.notify(mStrNOSWAPONDISMOUNTEDTYRE,
                                    mContext);
                        } else {
                            // useless code (only loadSwapActionDialog required)
                            if (!checkDifferentAxle()) {
                                loadSwapActionDialog();
                            } else {
                                if (Constants.INSERT_SWAP_DATA == 1) {
                                    if (mJobItemList.size() > 0) {
                                        mJobItemList
                                                .remove(mJobItemList.size() - 1);
                                        Constants.INSERT_SWAP_DATA = 0;
                                    }
                                    resetTyreState();
                                    updateTireImages();
                                }
                                CommonUtils.notify(mSizeNotAllowed, mContext);
                                resetBoolValue();
                                resetTyres();
                            }
                        }
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                default:
                    break;
            }
            return true;
        }
    }

    /**
     * Inserting Job in DB after operation
     *
     * @param dbmodel
     */
    public static void insertJobAfterOperation(DBModel dbmodel) {
        // // update job object
        // Job.setStatus("1");// incomplete job
        // EjobTimeLocationFragment.updateJobObject();
        // EjobInformationFragment.updateJobObject();
        // // update DB
        // SaveJob.callUpdateQueries(dbmodel, EjobFormActionActivity.sJobID);
    }

    /**
     * method being called after performing a tire operation
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtil.v("INSIDE", "onActivityResult VehicleSkeleton: requestCode: "
                + requestCode);
        Tyre tyreNow = null;
        if (Constants.SELECTED_TYRE != null) {
            tyreNow = getTireByPosition(Constants.SELECTED_TYRE.getPosition());
        }
        if (resultCode == Activity.RESULT_CANCELED) {
            LogUtil.e("VS", "Activity Reset As Cancelled...");
            if (tyreNow != null) {
                int selecteIcon = TireUtils.getTireIcon(tyreNow, false);
                ImageButton selectedTire = getViewFromUI(tyreNow.getPosition());
                System.out.println("RESULT_CANCELED selectedTire:::: "
                        + selectedTire);
                if (selectedTire != null) {
                    selectedTire.setBackgroundResource(selecteIcon);
                    mLastSelected = null;
                }
            }
            VehicleSkeletonFragment.resetTyres();
            return;
        }
        // CR:: 447:: Condition is true if current opertaion is Inspection
        if (requestCode == VehicleSkeletonFragment.INSPECTION_INTENT) {
            Constants.ONSTATE_INSPECTION = false;
            if (data != null && data.getExtras().getBoolean("inspection")) {
                enableSaveButton();
                //Fix for : bug 630 (If you cancel an inspection on a tyre you are not able to re-inspect the tyre)
                Constants.SELECTED_TYRE.setIsInspected(true);
                Constants.EDITED_SERIAL_NUMBER = "";
            }/*if (!Constants.SELECTED_TYRE.isRetorqued()) {
                    Retorque retorque = new Retorque(tyreNow, mContext);
                    mRetroqueStarted = true;
                    retorque.startRetroqueProcess();
                }*/

            //As per Client Request, state change for non maintained tyre in Inspection is block
            //Constants.SELECTED_TYRE.setTyreState(TyreState.PART_WORN);

        }
        if (requestCode == VehicleSkeletonFragment.REGROOVE_INTENT) {
            Constants.ONSTATE_REGROOVE = false;
            if (data != null && data.getExtras().getBoolean("regroove")) {
                enableSaveButton();
                if (!Constants.SELECTED_TYRE.isRetorqued()) {
                    Retorque retorque = new Retorque(tyreNow, mContext);
                    mRetroqueStarted = true;
                    retorque.startRetroqueProcess();
                }
            }
            Constants.SELECTED_TYRE.setTyreState(TyreState.PART_WORN);
        }
        if (requestCode == VehicleSkeletonFragment.DISMOUNT_INTENT) {
            Constants.ONSTATE_DISMOUNT = false;
            if (data != null && data.hasExtra("dismount")) {
                if (data.getExtras().getBoolean("dismount")) {
                    enableSaveButton();
                    tyreNow = Constants.SELECTED_TYRE;
                    tyreNow.setDismounted(true);
                    tyreNow.setTyreState(TyreState.EMPTY);
                    // Auto-Swap By Amit
                    if (!TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
                            && !TextUtils
                            .isEmpty(Constants.tempTyrePositionNew)
                            && AutoSwapImpl
                            .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)) {
                        Tyre swapTyre = getTireByPosition(Constants.tempTyrePositionNew);
                        if (swapTyre != null
                                && swapTyre.getTyreState() != TyreState.EMPTY) {
                            swapTyre.setLogicalySwaped(true);
                            if (TextUtils.isEmpty(swapTyre.getSerialNumber())) {
                                swapTyre.setTyreState(TyreState.NON_MAINTAINED);
                            }
                            int selecteIcon = TireUtils.getTireIcon(swapTyre,
                                    false);
                            ImageButton selectedTire = getViewFromUI(swapTyre
                                    .getPosition());
                            System.out
                                    .println("selectedTire for DISMOUNT autoSwap:::: "
                                            + selectedTire);
                            if (selectedTire != null) {
                                selectedTire.setBackgroundResource(selecteIcon);
                            }
                            Constants.tempTyrePositionNew = "";
                            Constants.tempTyreSerial = "";
                            Constants.EDITED_SERIAL_NUMBER = "";
                        }
                    }
                }
                insertJobAfterOperation(ejobDbInsert);
            }
        }
        if (requestCode == VehicleSkeletonFragment.MOUNT_INTENT) {
            Constants.ONSTATE_MOUNT = false;
            if (data != null && data.hasExtra("mount")) {
                if (data.getExtras().getBoolean("mount")) {
                    enableSaveButton();
                    mEditedUserInput = "";
                    tyreNow = Constants.SELECTED_TYRE;
                    tyreNow.setDismounted(false);
                    tyreNow.setPWTMounted(false);
                    tyreNow.setTyreState(TyreState.NEW_TIRE);
                    if (!Constants.SELECTED_TYRE.isRetorqued()) {
                        Retorque retorque = new Retorque(tyreNow, mContext);
                        mRetroqueStarted = true;
                        retorque.startRetroqueProcess();
                    }
                    if (data.getExtras().getBoolean("mount")) {
                        tyreNow.setHasMounted(true);
                        tyreNow.setTyreState(TyreState.NEW_TIRE);
                    }
                }
                Constants.EDITED_SERIAL_NUMBER = "";
            }
        }
        if (requestCode == VehicleSkeletonFragment.MOUNT_PWT_INTENT) {
            Constants.ONSTATE_MOUNTPWT = false;
            if (data != null && data.hasExtra("mount_pwt")) {
                if (data.getExtras().getBoolean("mount_pwt")) {
                    enableSaveButton();
                    tyreNow = Constants.SELECTED_TYRE;
                    tyreNow.setDismounted(false);
                    tyreNow.setPWTMounted(true);
                    if (!Constants.SELECTED_TYRE.isRetorqued()) {
                        Retorque retorque = new Retorque(tyreNow, mContext);
                        mRetroqueStarted = true;
                        retorque.startRetroqueProcess();
                    }
                    if (data.getExtras().getBoolean("mount_pwt")) {
                        tyreNow.setPWTMounted(true);
                        tyreNow.setTyreState(TyreState.PART_WORN);
                    }
                    try {
                        int mUsedPWTPosition = 0;
                        ReservedTyre mDeleteReserveTyre = ReservePWTUtils.mReserveTyreListForSelectedTyre.get(mReserve_tyre_selected);
                        if (ReservePWTUtils.mReserveTyreList != null) {

                            for(int x=0;x<ReservePWTUtils.mReserveTyreList.size();x++)
                            {
                                if(mDeleteReserveTyre.getTyreID().equalsIgnoreCase(ReservePWTUtils.mReserveTyreList.get(x).getTyreID()))
                                {
                                    ReservePWTUtils.mReserveTyreList.remove(x);
                                    mUsedPWTPosition = x;
                                    break;
                                }
                            }
                        }
                        if (Constants.Search_TM_ListItems != null) {
                                    Constants.Search_TM_ListItems.remove(mUsedPWTPosition);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        LogUtil.e("TAG", "" + e.getMessage());
                    }
                    mReserve_tyre_selected = 0;
                }
            }
        }
        if (requestCode == VehicleSkeletonFragment.WOT_INTENT) {
            LogUtil.v("INSIDE", "VehicleSkeletonFragment.WOT_INTENT");
            Constants.ONSTATE_WOT = false;
            if (data != null && data.hasExtra("wot")) {
                if (data.getExtras().getBoolean("wot")) {
                    enableSaveButton();
                    mReserve_tyre_selected = 0;
                    tyreNow.setSerialNumber(Constants.SELECTED_TYRE
                            .getSerialNumber());
                    tyreNow.setDesign(Constants.SELECTED_TYRE.getDesign());
                    tyreNow.setDesignDetails(Constants.SELECTED_TYRE
                            .getDesignDetails());
                    tyreNow.setNsk(Constants.NSK_SET_WOT);
                    tyreNow.setNsk2(Constants.NSK_SET_WOT);
                    tyreNow.setNsk3(Constants.NSK_SET_WOT);
                    tyreNow.setPressure(Constants.PRESSURE_SET_WOT);
                    tyreNow.setBrandName(Constants.SELECTED_TYRE.getBrandName());
                    tyreNow.setSize(Constants.SELECTED_TYRE.getSize());
                    tyreNow.setAsp(Constants.SELECTED_TYRE.getAsp());
                    tyreNow.setRim(Constants.SELECTED_TYRE.getRim());
                    tyreNow.setShared("0");
                    tyreNow.setFromJobId(null);
                    tyreNow.setVehicleJob(Constants.contract
                            .getVehicleSapCode());
                    tyreNow.setStatus("0");
                    tyreNow.setSapVenderCodeID(Constants.VENDORSAPCODE);
                    tyreNow.setSapMaterialCodeID(Constants.SELECTED_TYRE
                            .getSapMaterialCodeID());
                    tyreNow.setActive("1");
                    tyreNow.setTyreState(TyreState.PART_WORN);
                    if (!Constants.SELECTED_TYRE.isRetorqued()) {
                        Retorque retorque = new Retorque(tyreNow, mContext);
                        mRetroqueStarted = true;
                        retorque.startRetroqueProcess();
                    }
                    boolean isPutTickMark = true;
                    if (data.hasExtra("puttickmark")) {
                        isPutTickMark = data.getExtras().getBoolean(
                                "puttickmark");
                    }
                    if (!TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
                            && !TextUtils
                            .isEmpty(Constants.tempTyrePositionNew)
                            && AutoSwapImpl
                            .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)) {
                        Tyre swapTyre = getTireByPosition(Constants.tempTyrePositionNew);
                        if (swapTyre != null
                                && swapTyre.getTyreState() != TyreState.EMPTY) {
                            swapTyre.setLogicalySwaped(true);
                            if (TextUtils.isEmpty(swapTyre.getSerialNumber())) {
                                swapTyre.setTyreState(TyreState.NON_MAINTAINED);
                            }
                            int selecteIcon = TireUtils.getTireIcon(swapTyre,
                                    false);
                            ImageButton selectedTire = getViewFromUI(swapTyre
                                    .getPosition());
                            System.out
                                    .println("selectedTire for WOT autoSwap:::: "
                                            + selectedTire);
                            if (selectedTire != null) {
                                selectedTire.setBackgroundResource(selecteIcon);
                            }
                            Constants.tempTyrePositionNew = "";
                            Constants.tempTyreSerial = "";
                            Constants.EDITED_SERIAL_NUMBER = "";
                        }
                    }
                    if (!(isPutTickMark)) {
                        tyreNow.setWorkedOn(true);
                    }
                }
                Constants.NSK_SET_WOT = "";
                Constants.PRESSURE_SET_WOT = "";
            }
        }
        if (requestCode == VehicleSkeletonFragment.TOR_INTENT) {
            try {
                Constants.ONSTATE_TOR = false;
                tyreNow = Constants.SELECTED_TYRE;
                boolean startRegroove = false;
                tyreNow.setTyreState(TyreState.PART_WORN);
                if (data != null && data.getExtras().getBoolean("tor")) {
                    enableSaveButton();
                    if (!TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
                            && !TextUtils
                            .isEmpty(Constants.tempTyrePositionNew)
                            && AutoSwapImpl
                            .checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)) {
                        Tyre swapTyre = getTireByPosition(Constants.tempTyrePositionNew);
                        if (swapTyre != null
                                && swapTyre.getTyreState() != TyreState.EMPTY) {
                            swapTyre.setLogicalySwaped(true);
                            if (TextUtils.isEmpty(swapTyre.getSerialNumber())) {
                                swapTyre.setTyreState(TyreState.NON_MAINTAINED);
                            }
                            int selecteIcon = TireUtils.getTireIcon(swapTyre,
                                    false);
                            ImageButton selectedTire = getViewFromUI(swapTyre
                                    .getPosition());
                            System.out
                                    .println("selectedTire for TOR autoSwap:::: "
                                            + selectedTire);
                            if (selectedTire != null) {
                                selectedTire.setBackgroundResource(selecteIcon);
                            }
                            Constants.tempTyrePositionNew = "";
                            Constants.tempTyreSerial = "";
                            Constants.EDITED_SERIAL_NUMBER = "";
                        }
                    }
                    int axlepos = Character
                            .getNumericValue(Constants.SELECTED_TYRE
                                    .getPosition().charAt(0)) - 1;
                    float recommendedPressure = mAxleRecommendedPressure
                            .get(axlepos);
                    if (recommendedPressure == 0) {
                        PressureCheck pressureCheck = new PressureCheck(
                                tyreNow, mContext);
                        ejobFormParentActivity
                                .pressureDialogStarted(pressureCheck);
                        pressureCheck.updatePressureFromAxleRecommended(
                                recommendedPressure, axlepos);
                        mTorStarted = true;
                    } else {
                        tyreNow.setPressure(String.valueOf(recommendedPressure));
                        startRegroove = true;
                    }
                }
                if (data != null && data.getExtras().getBoolean("regroove")) {
                    if (!Constants.SELECTED_TYRE.isRetorqued()) {
                        if (startRegroove) {
                            Retorque retorque = new Retorque(tyreNow, mContext);
                            mRetroqueStarted = true;
                            retorque.startRetroqueProcess();
                        } else {
                            mDoRetroque = true;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (resultCode == VehicleSkeletonFragment.DEFINE_TIRE_INTENT) {
            LogUtil.v("INSIDE", "VehicleSkeletonFragment.DEFINE_TIRE_INTENT");

            if (data != null && data.hasExtra("define_tire")) {
                if (data.getExtras().getBoolean("define_tire")) {
                    enableSaveButton();
                    String wheelPos = Constants.SELECTED_TYRE.getPosition();
                    int tyreInfoIndex = -1;
                    for (int i = 0; i < tyreInfo.size(); i++) {
                        if (tyreInfo.get(i).getPosition().equals(wheelPos)) {
                            tyreInfoIndex = i;
                            break;
                        }
                    }
                    LogUtil.d("VehicleSkeleton", "Count tyreInfo BEFORE set::"
                            + tyreInfo.size());
                    if (tyreInfoIndex != -1) {
                        tyreInfo.get(tyreInfoIndex).setSerialNumber(
                                Constants.SELECTED_TYRE.getSerialNumber());
                        tyreInfo.get(tyreInfoIndex).setDesign(
                                Constants.SELECTED_TYRE.getDesign());
                        tyreInfo.get(tyreInfoIndex).setDesignDetails(
                                Constants.SELECTED_TYRE.getDesignDetails());
                        tyreInfo.get(tyreInfoIndex).setNsk(
                                Constants.SELECTED_TYRE.getNsk());
                        tyreInfo.get(tyreInfoIndex).setNsk2(
                                Constants.SELECTED_TYRE.getNsk2());
                        tyreInfo.get(tyreInfoIndex).setNsk3(
                                Constants.SELECTED_TYRE.getNsk3());
                        tyreInfo.get(tyreInfoIndex).setPressure(
                                Constants.SELECTED_TYRE.getPressure());
                        tyreInfo.get(tyreInfoIndex).setTyreState(
                                TyreState.PART_WORN);

                        tyreInfo.get(tyreInfoIndex).setBrandName(
                                Constants.SELECTED_TYRE.getBrandName());
                        tyreInfo.get(tyreInfoIndex).setSize(
                                Constants.SELECTED_TYRE.getSize());
                        tyreInfo.get(tyreInfoIndex).setAsp(
                                Constants.SELECTED_TYRE.getAsp());
                        tyreInfo.get(tyreInfoIndex).setRim(
                                Constants.SELECTED_TYRE.getRim());
                        tyreInfo.get(tyreInfoIndex).setShared("0");
                        tyreInfo.get(tyreInfoIndex).setFromJobId(null);
                        tyreInfo.get(tyreInfoIndex).setVehicleJob(
                                Constants.contract.getVehicleSapCode());
                        tyreInfo.get(tyreInfoIndex).setStatus("0");
                        tyreInfo.get(tyreInfoIndex).setSapVenderCodeID(
                                Constants.VENDORSAPCODE);
                        tyreInfo.get(tyreInfoIndex).setSapMaterialCodeID(
                                Constants.SELECTED_TYRE.getSapMaterialCodeID());
                        tyreInfo.get(tyreInfoIndex).setActive("1");
                    }
                    LogUtil.d("VehicleSkeleton", "Count tyreInfo AFTER set::"
                            + tyreInfo.size());
                    try {
                        Constants.ONSTATE_WOT = false;
                        Intent intent = new Intent(mContext, WOTActivity.class);
                        intent.putExtra("SERIAL_NO",
                                Constants.SELECTED_TYRE.getSerialNumber());
                        int pos = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        intent.putExtra(mAXLEPRESSURE_POS, pos);
                        intent.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(pos));
                        intent.putExtra("WHEEL_POS",
                                Constants.SELECTED_TYRE.getPosition());
                        intent.putExtra("FROM_DEFINE_TIRE", true);
                        startActivityForResult(intent, WOT_INTENT);
                        mAlertDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (tyreNow != null
                && (requestCode != 1 && requestCode != 12 && requestCode != 24
                && requestCode != 48
                && requestCode != PHYSICAL_SWAP_DRAG_INITIAL_INTENT && requestCode != PHYSICAL_SWAP_DRAG_FIANL_INTENT)) {
            int selecteIcon = TireUtils.getTireIcon(tyreNow, false);
            ImageButton selectedTire = getViewFromUI(tyreNow.getPosition());
            System.out.println("selectedTire:::: " + selectedTire);
            if (selectedTire != null) {
                selectedTire.setBackgroundResource(selecteIcon);
                mLastSelected = null;
            }
        }
        if (requestCode == 1) {
            if (data != null) {
                try {
                    mEditedUserInput = "";
                    Constants.onReturnBool = true;
                    loadAnotherTyreDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == 12) {
            if (data != null) {
                try {
                    mEditedUserInput = "";
                    Constants.DO_PHYSICAL_SWAP = true;
                    Constants.onReturnBool = false;
                    if (Constants.SECOND_SELECTED_TYRE.getSerialNumber()
                            .equals("")) {
                        loadSerialMountNewDialog();
                    } else {
                        loadSwapFinalConfirmation();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == 24) {
            if (data != null) {
                try {
                    mEditedUserInput = "";
                    Constants.onReturnBool = false;
                    if (Constants.onBrandBlank) {
                        Constants.onBrandBlank = false;
                        Constants.onReturnBool = true;
                        loadAnotherTyreDialog();
                    } else if (Constants.SECOND_SELECTED_TYRE != null) {
                        loadSwapFinalConfirmation();
                    } else {
                        Constants.onReturnBool = true;
                        loadAnotherTyreDialog();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == 48) {
            if (data != null) {
                try {
                    Constants.CHECK_LOGICAL_DONE++;
                    System.out.println("Constants.CHECK_LOGICAL_DONE::: "
                            + Constants.CHECK_LOGICAL_DONE);
                    Constants.onBrandBlank = false;
                    mEditedUserInput = "";
                    Constants.TRIGGER_LOGICAL = false;
                    if (Constants.CHECK_LOGICAL_DONE >= 1) {
                        loadSwapFinalConfirmation();
                    } else if (Constants.DES_TYRE != null) {
                        if (Constants.DES_TYRE.getTyreState() == TyreState.EMPTY) {
                            int jobID = getJobItemIDForThisJobItem();
                            UpdateSwapJobItem.getMyInstance(mContext)
                                    .updateJobItemForEmptyTire(
                                            Constants.DES_TYRE, jobID);
                            loadSwapFinalConfirmation();
                        } else if (!TextUtils.isEmpty(Constants.DES_TYRE
                                .getSerialNumber())
                                && !TextUtils.isEmpty(Constants.DES_TYRE
                                .getBrandName())
                                && Constants.IS_DES_LOGICAL_SWAP_DONE) {
                            Constants.IS_DES_LOGICAL_SWAP_DONE = false;
                            loadSerialCorrectionDialog();// Big Box
                        } else if (TextUtils.isEmpty(Constants.DES_TYRE
                                .getSerialNumber())) {
                            loadSerialMountNewDialog();// Small Box
                        } else if (TextUtils.isEmpty(Constants.DES_TYRE
                                .getBrandName())) {
                            loadSerialCorrectionDialog();// Big Box
                        } else {
                            loadSwapFinalConfirmation();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == PHYSICAL_SWAP_DRAG_INITIAL_INTENT) {
            if (data != null) {
                try {
                    mEditedUserInput = "";
                    if (Constants.DES_TYRE.getSerialNumber().equalsIgnoreCase(
                            "")) {
                        loadSerialMountNewDialog();
                    } else if (Constants.DO_PHYSICAL_SWAP
                            && Constants.DES_TYRE.getTyreState() == TyreState.EMPTY) {
                        int jobID = getJobItemIDForThisJobItem();
                        UpdateSwapJobItem.getMyInstance(mContext)
                                .updateJobItemForEmptyTire(Constants.DES_TYRE,
                                        jobID);
                        loadSwapFinalConfirmation();
                    } else {
                        loadSerialCorrectionDialog();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == PHYSICAL_SWAP_DRAG_FIANL_INTENT) {
            if (data != null) {
                try {
                    enableSaveButton();
                    if (Constants.OnSerialBool == true) {
                        updateCorrectedSerialNumberInDB();
                    }
                    Constants.DO_PHYSICAL_SWAP = true;
                    mEditedUserInput = "";
                    loadSwapFinalConfirmation();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Loading Action Dialog for Swap
     */
    public void loadSwapActionDialog() {
        try {
            clearTextAreasForAction();
            mDialogRefNo = 10;
            LayoutInflater li_action = LayoutInflater.from(mContext);
            final View promptsView = li_action.inflate(
                    R.layout.dialog_swap_action, null);
            final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
                    mContext);
            final Button BtnPhysicalSwap = (Button) promptsView
                    .findViewById(R.id.swapPhysicalSwap);
            BtnPhysicalSwap.setText(mPhysicalSwapLabel);
            if (!Constants.JOBTYPEREGULAR) {
                BtnPhysicalSwap.setVisibility(View.GONE);
            }
            final Button BtnLogicalSwap = (Button) promptsView
                    .findViewById(R.id.swapLogicalSwap);
            BtnLogicalSwap.setText(mLogicalSwapLabel);
            // set prompts.xml to alert dialog builder
            alertDialogBuilder.setView(promptsView);
            // create alert dialog
            mAlertDialog_action = alertDialogBuilder.create();
            resetBoolValue();
            mAlertDialog_action.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode,
                                     KeyEvent event) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            mDialogRefNo = -1;
                            break;
                        default:
                            break;
                    }
                    return false;
                }
            });
            BtnPhysicalSwap.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    // update user's inactivity
                    InactivityUtils.updateActivityOfUser();
                    Constants.TyreIdListToHandlerBackPress = new ArrayList<String>();
                    LogUtil.TraceInfo(TRACE_INFO, "Dialog", "Physical Swap", false, true, false);

                    mDialogRefNo = -1;
                    Constants.onDragPSBool = true;
                    Constants.ONPRESS = false;
                    Constants.ONDRAG = true;
                    Constants.DO_PHYSICAL_SWAP = true;
                    Constants.DO_LOGICAL_SWAP = false;
                    getTyreState();
                    dialogInEditMode = false;
                    mAlertDialog_action.dismiss();
                    if ((Constants.SOURCE_TYRE.getSerialNumber()
                            .equalsIgnoreCase("") && Constants.DES_TYRE
                            .getSerialNumber().equalsIgnoreCase(""))
                            || Constants.SOURCE_TYRE.getTyreState() == TyreState.NON_MAINTAINED) {
                        loadSerialMountNewDialog();
                    } else {
                        loadSerialCorrectionDialog();
                    }
                }
            });
            BtnLogicalSwap.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    // update user's inactivity
                    InactivityUtils.updateActivityOfUser();
                    Constants.TyreIdListToHandlerBackPress = new ArrayList<String>();
                    LogUtil.TraceInfo(TRACE_INFO, "Dialog", "Logical Swap", false, true, false);

                    mDialogRefNo = -1;
                    Constants.DO_LOGICAL_SWAP = true;
                    Constants.DO_PHYSICAL_SWAP = false;
                    Constants.onGOBool = false;
                    Constants.onReturnBool = false;
                    Constants.onDragBool_LS = true;
                    Constants.ONPRESS = false;
                    Constants.ONDRAG = true;
                    Constants.onGOBoolLogical = false;
                    dialogInEditMode = false;
                    Constants.LOGICAL_SWAP_INITIAL = true;
                    getTyreState();
                    mAlertDialog_action.dismiss();
                    if (Constants.SOURCE_TYRE != null
                            && Constants.SOURCE_TYRE.getTyreState() != TyreState.NON_MAINTAINED
                            && (TextUtils.isEmpty(Constants.SOURCE_TYRE
                            .getBrandName()) || TextUtils
                            .isEmpty(Constants.SOURCE_TYRE
                                    .getDesignDetails()))) {
                        Constants.IS_DES_LOGICAL_SWAP_DONE = true;
                        Constants.onBrandBlank = true;
                        Constants.onBrandDesignBlank = true;
                        loadSerialCorrectionDialog();
                    } else if (Constants.SOURCE_TYRE != null
                            && Constants.SOURCE_TYRE.getTyreState() != TyreState.NON_MAINTAINED
                            && "NA".equalsIgnoreCase(Constants.SOURCE_TYRE
                            .getBrandName())
                            || "NA".equalsIgnoreCase(Constants.SOURCE_TYRE
                            .getDesignDetails())) {
                        Constants.onBrandBlank = true;
                        Constants.onBrandDesignBlank = true;
                        Constants.IS_DES_LOGICAL_SWAP_DONE = true;
                        loadSerialCorrectionDialog();
                    } else if (Constants.SOURCE_TYRE.getSerialNumber()
                            .equalsIgnoreCase("")) {
                        Constants.IS_DES_LOGICAL_SWAP_DONE = true;
                        loadSerialMountNewDialog();
                    } else if (Constants.DES_TYRE.getSerialNumber()
                            .equalsIgnoreCase("")) {
                        loadSerialMountNewDialog();
                    } else if (Constants.DO_LOGICAL_SWAP
                            && Constants.DES_TYRE.getTyreState() == TyreState.EMPTY) {
                        int jobID = getJobItemIDForThisJobItem();
                        UpdateSwapJobItem.getMyInstance(mContext)
                                .updateJobItemForEmptyTire(
                                        Constants.SELECTED_TYRE, jobID);
                        UpdateSwapJobItem.getMyInstance(mContext)
                                .updateJobItemForEmptyTire(
                                        Constants.SECOND_SELECTED_TYRE, jobID);

                        loadSwapFinalConfirmation();
                    } else {
                        loadSerialCorrectionDialog();
                    }
                }
            });
            mAlertDialog_action.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Dialog for choosing another tire
     */
    private void loadAnotherTyreDialog() {
        try {
            mDialogRefNo = 9;
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(mContext);
            View promptsView = li.inflate(R.layout.dialog_another_tyre, null);
            final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
                    mContext);

            LinearLayout rootNode = (LinearLayout) promptsView
                    .findViewById(R.id.layout_root);
            rootNode.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    InactivityUtils.updateActivityOfUser();
                }
            });
            alertDialogBuilder.setView(promptsView);
            final TextView value_AnotherTyre = (TextView) promptsView
                    .findViewById(R.id.txt_choaseAnotherTyre);
            value_AnotherTyre.setText(mSwapOperationMessage);
            alertDialogBuilder.setCancelable(false).setPositiveButton(
                    mYesLabel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            mDialogRefNo = -1;
                            alertDialogBuilder.updateInactivityForDialog();
                            Toast.makeText(mContext, mStrCHOOSEANOTHERTYRE,
                                    Toast.LENGTH_SHORT).show();

                        }
                    });
            alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            alertDialogBuilder.updateInactivityForDialog();

                            mDialogRefNo = -1;
                            Constants.onReturnBool = false;
                            if (Constants.INSERT_SWAP_DATA == 1) {
                                if (mJobItemList.size() > 0) {
                                    // Swap cancellation issue with Inspection
                                    SharedPreferences preferences = getActivity().getSharedPreferences(
                                            Constants.GOODYEAR_CONF, 0);
                                    if (!TextUtils.isEmpty(preferences.getString(Constants.INSPECTION, ""))) {
                                        Constants.INSPECTION_ENABLE = (preferences.getString(Constants.INSPECTION, ""));
                                        if (Constants.INSPECTION_ENABLE.equalsIgnoreCase("ON")) {
                                            mJobItemList.remove(mJobItemList
                                                    .size() - 1);
                                            if(!Constants.IS_TYRE_INSPECTED) {
                                                mJobItemList.remove(mJobItemList
                                                        .size() - 1);
                                            }
                                        }else{
                                            mJobItemList.remove(mJobItemList
                                                    .size() - 1);
                                        }
                                    }
                                    Constants.INSERT_SWAP_DATA = 0;
                                }
                                resetTyreState();
                                updateTireImages();
                            }
                            // Removing JobCorrection on cancel
                            if (Constants.JOB_CORRECTION_COUNT_SWAP == 2) {
                                if (VehicleSkeletonFragment.mJobcorrectionList
                                        .size() >= 2) {
                                    VehicleSkeletonFragment.mJobcorrectionList
                                            .remove(VehicleSkeletonFragment.mJobcorrectionList
                                                    .size() - 1);
                                    VehicleSkeletonFragment.mJobcorrectionList
                                            .remove(VehicleSkeletonFragment.mJobcorrectionList
                                                    .size() - 1);
                                }
                                Constants.JOB_CORRECTION_COUNT_SWAP = 0;
                            } else if (Constants.JOB_CORRECTION_COUNT_SWAP == 1) {
                                if (VehicleSkeletonFragment.mJobcorrectionList
                                        .size() >= 1) {
                                    VehicleSkeletonFragment.mJobcorrectionList
                                            .remove(VehicleSkeletonFragment.mJobcorrectionList
                                                    .size() - 1);
                                }
                                Constants.JOB_CORRECTION_COUNT_SWAP = 0;
                            }
                            // Handling inspection status on back press
                            if(Constants.TyreIdListToHandlerBackPress!=null) {
                                for (int x = 0; x < Constants.TyreIdListToHandlerBackPress.size(); x++) {
                                    if(Constants.SELECTED_TYRE.getExternalID().equalsIgnoreCase(Constants.TyreIdListToHandlerBackPress.get(x)))
                                    {
                                        Constants.SELECTED_TYRE.setIsInspected(false);
                                        break;
                                    }
                                }
                            }
                            resetBoolValue();
                            resetTyres();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Dialog for choosing another tire during LS
     */
    private void loadLogicalSwapActionDialog() {
        mDialogRefNo = 18;
        try {
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(mContext);
            View promptsView = li.inflate(R.layout.dialog_another_tyre, null);
            final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
                    mContext);

            // set user activity for
            LinearLayout rootNode = (LinearLayout) promptsView
                    .findViewById(R.id.layout_root);
            rootNode.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    InactivityUtils.updateActivityOfUser();
                }
            });

            alertDialogBuilder.setView(promptsView);
            final TextView value_AnotherTyre = (TextView) promptsView
                    .findViewById(R.id.txt_choaseAnotherTyre);
            value_AnotherTyre.setText(mSwapOperationMessage);
            alertDialogBuilder.setCancelable(false).setPositiveButton(
                    mYesLabel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            alertDialogBuilder.updateInactivityForDialog();
                            if (Constants.ONPRESS == true) {
                                Constants.onReturnBool = true;
                                Constants.onSwapOptionsBool = true;
                                Constants.onDragBool_LS = false;
                            } else if (Constants.ONDRAG == true) {
                                dialogInEditMode = false;
                                loadSerialCorrectionDialog();
                                Constants.onReturnBool = false;
                                Constants.onSwapOptionsBool = false;
                                Constants.onDragBool_LS = true;
                            }
                            Toast.makeText(mContext, mStrCHOOSEANOTHERTYRE,
                                    Toast.LENGTH_SHORT).show();
                            mDialogRefNo = -1;
                        }
                    });
            alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            alertDialogBuilder.updateInactivityForDialog();
                            // Handling inspection status on back press
                            if (Constants.TyreIdListToHandlerBackPress != null) {
                                for (int x = 0; x < Constants.TyreIdListToHandlerBackPress.size(); x++) {
                                    if (Constants.SELECTED_TYRE.getExternalID().equalsIgnoreCase(Constants.TyreIdListToHandlerBackPress.get(x))) {
                                        Constants.SELECTED_TYRE.setIsInspected(false);
                                        break;
                                    }
                                }
                            }
                            resetTyres();
                            resetBoolValue();
                            mDialogRefNo = -1;
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Dialog for final confirmation with entire SWAP Implementation
     */
    private void loadSwapFinalConfirmation() {
        Constants.CHECK_LOGICAL_DONE = -1;
        Constants.IS_DES_LOGICAL_SWAP_DONE = false;
        try {
            enableSaveButton();
            mDialogRefNo = 5;
            LayoutInflater li = LayoutInflater.from(mContext);
            View promptsView = li.inflate(R.layout.dialog_another_tyre, null);
            final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
                    mContext);
            alertDialogBuilder.setView(promptsView);

            // set user activity for
            LinearLayout rootNode = (LinearLayout) promptsView
                    .findViewById(R.id.layout_root);
            rootNode.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    InactivityUtils.updateActivityOfUser();
                }
            });

            final TextView value_AnotherTyre = (TextView) promptsView
                    .findViewById(R.id.txt_choaseAnotherTyre);
            if (Constants.SELECTED_TYRE == null) {
                Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
            }
            if (Constants.SECOND_SELECTED_TYRE == null) {
                Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
            }
            mPosition_one = Constants.SELECTED_TYRE.getPosition();
            mPosition_two = Constants.SECOND_SELECTED_TYRE.getPosition();
            mSwapFinalConfirmationMessageFromDB = mSwapFinalConfirmationMessage;
            mSwapFinalConfirmationMessageFromDB = String.format(
                    mSwapFinalConfirmationMessageFromDB, mPosition_one,
                    mPosition_two);
            value_AnotherTyre.setText(mSwapFinalConfirmationMessageFromDB);
            // set dialog message
            alertDialogBuilder.setCancelable(false).setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // set user activity
                            alertDialogBuilder.updateInactivityForDialog();

                            mDialogRefNo = -1;
                            if (null != Constants.SELECTED_TYRE) {
                                mPosition_one = Constants.SELECTED_TYRE
                                        .getPosition();
                            }
                            if (null != Constants.SECOND_SELECTED_TYRE) {
                                mPosition_two = Constants.SECOND_SELECTED_TYRE
                                        .getPosition();
                            }

                            if (((Constants.triggerMultipleSwap < 3 && Constants.checkSingleSwap == 0) && (Constants.triggerMultipleEdit < 2 || ((Constants.EDITED_SERIAL_NUMBER
                                    .equals(Constants.SELECTED_TYRE
                                            .getSerialNumber())) || (Constants.EDITED_SERIAL_NUMBER
                                    .equals(Constants.SECOND_SELECTED_TYRE
                                            .getSerialNumber())))))
                                    || Constants.DUPLICATE_SERIAL_ENTRY) {

                                updateTickForSwap();
                                AutoSwapImpl.singleSwap(Constants.SELECTED_TYRE
                                                .getPosition(),
                                        Constants.SECOND_SELECTED_TYRE
                                                .getPosition());
                            } else if ((Constants.UPDATE_SERIAL_DB == true
                                    && Constants.triggerMultipleSwap < 4 && Constants.checkSingleSwap < 3)
                                    && (Constants.triggerMultipleEdit < 3 || ((!Constants.EDITED_SERIAL_NUMBER
                                    .equals(Constants.SELECTED_TYRE
                                            .getSerialNumber())) || (!Constants.EDITED_SERIAL_NUMBER
                                    .equals(Constants.SECOND_SELECTED_TYRE
                                            .getSerialNumber()))))) {

                                updateTickForSwap();
                                AutoSwapImpl.singleSwap(Constants.SELECTED_TYRE
                                                .getPosition(),
                                        Constants.SECOND_SELECTED_TYRE
                                                .getPosition());
                            } else {
                                if (Constants.triggerMultipleSwap == 2
                                        || Constants.triggerMultipleSwap == 3
                                        && Constants.DO_PHYSICAL_SWAP == false
                                        && ((!Constants.EDITED_SERIAL_NUMBER
                                        .equals(Constants.SELECTED_TYRE
                                                .getSerialNumber())) || (!Constants.EDITED_SERIAL_NUMBER
                                        .equals(Constants.SECOND_SELECTED_TYRE
                                                .getSerialNumber())))) {
                                    mSecondaryPosition = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER);
                                    mPosition_three = mSecondaryPosition;
                                    updateTickForSwap();
                                    AutoSwapImpl.doubleSwap(
                                            Constants.SELECTED_TYRE
                                                    .getPosition(),
                                            Constants.SECOND_SELECTED_TYRE
                                                    .getPosition(),
                                            mSecondaryPosition);
                                }
                                // Added for DoubleSwap (BCA)
                                else if (!TextUtils
                                        .isEmpty(Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED)
                                        && TextUtils
                                        .isEmpty(Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE)
                                        && (Constants.triggerMultipleSwap == 6
                                        || Constants.triggerMultipleSwap == 5 || Constants.triggerMultipleSwap == 4)
                                        && Constants.triggerMultipleEdit == 1
                                        && Constants.DO_PHYSICAL_SWAP == true
                                        && ((!Constants.EDITED_SERIAL_NUMBER
                                        .equals(Constants.SELECTED_TYRE
                                                .getSerialNumber())) || (!Constants.EDITED_SERIAL_NUMBER
                                        .equals(Constants.SECOND_SELECTED_TYRE
                                                .getSerialNumber())))) {
                                    mSecondaryPosition = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED);
                                    updateTickForSwap();
                                    AutoSwapImpl.doubleSwapPhysical(
                                            Constants.SELECTED_TYRE
                                                    .getPosition(),
                                            Constants.SECOND_SELECTED_TYRE
                                                    .getPosition(),
                                            mSecondaryPosition);
                                }
                                // Added for DoubleSwap (CAB)
                                else if (TextUtils
                                        .isEmpty(Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED)
                                        && !TextUtils
                                        .isEmpty(Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE)
                                        && (Constants.triggerMultipleSwap == 6
                                        || Constants.triggerMultipleSwap == 5 || Constants.triggerMultipleSwap == 4)
                                        && Constants.triggerMultipleEdit == 1
                                        && Constants.DO_PHYSICAL_SWAP == true
                                        && ((!Constants.EDITED_SERIAL_NUMBER
                                        .equals(Constants.SELECTED_TYRE
                                                .getSerialNumber())) || (!Constants.EDITED_SERIAL_NUMBER
                                        .equals(Constants.SECOND_SELECTED_TYRE
                                                .getSerialNumber())))) {
                                    mSecondaryPosition = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE);
                                    updateTickForSwap();
                                    AutoSwapImpl.doubleSwap(
                                            Constants.SELECTED_TYRE
                                                    .getPosition(),
                                            Constants.SECOND_SELECTED_TYRE
                                                    .getPosition(),
                                            mSecondaryPosition);
                                } else {
                                    if ((Constants.triggerMultipleSwap > 2)
                                            && Constants.triggerMultipleEdit == 2
                                            && Constants.DO_PHYSICAL_SWAP == true
                                            && ((!Constants.EDITED_SERIAL_NUMBER
                                            .equals(Constants.SELECTED_TYRE
                                                    .getSerialNumber())) || (!Constants.EDITED_SERIAL_NUMBER
                                            .equals(Constants.SECOND_SELECTED_TYRE
                                                    .getSerialNumber())))) {
                                        mSecondaryPosition = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED);
                                        mFinalPostion = getPosFromSerial(Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE);
                                        mPosition_three = mSecondaryPosition;
                                        mPosition_four = mFinalPostion;
                                        updateTickForSwap();
                                        AutoSwapImpl.tripleSwap(
                                                Constants.SELECTED_TYRE
                                                        .getPosition(),
                                                Constants.SECOND_SELECTED_TYRE
                                                        .getPosition(),
                                                mSecondaryPosition,
                                                mFinalPostion);
                                    }
                                }
                            }
                            if (Constants.DO_PHYSICAL_SWAP == false) {
                                Constants.SELECTED_TYRE.setLogicalySwaped(true);
                                Constants.SECOND_SELECTED_TYRE
                                        .setLogicalySwaped(true);
                                if (Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED) {
                                    Constants.SELECTED_TYRE
                                            .setTyreState(TyreState.PART_WORN);
                                }
                                if (Constants.SECOND_SELECTED_TYRE
                                        .getTyreState() == TyreState.NON_MAINTAINED) {
                                    Constants.SECOND_SELECTED_TYRE
                                            .setTyreState(TyreState.PART_WORN);
                                }
                            } else {
                                Constants.SELECTED_TYRE
                                        .setIsphysicalySwaped(true);
                                Constants.SECOND_SELECTED_TYRE
                                        .setIsphysicalySwaped(true);
                                if (Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED) {
                                    Constants.SELECTED_TYRE
                                            .setTyreState(TyreState.PART_WORN);
                                }
                                if (Constants.SECOND_SELECTED_TYRE
                                        .getTyreState() == TyreState.NON_MAINTAINED) {
                                    Constants.SECOND_SELECTED_TYRE
                                            .setTyreState(TyreState.PART_WORN);
                                }
                            }
                            if (Boolean.parseBoolean(Constants.SELECTED_TYRE
                                    .isSpare())
                                    && Boolean
                                    .parseBoolean(Constants.SECOND_SELECTED_TYRE
                                            .isSpare())) {
                                // do nothing
                            } else if (Boolean
                                    .parseBoolean(Constants.SELECTED_TYRE
                                            .isSpare())) {
                                Constants.SELECTED_TYRE.setIsSpare("false");
                                Constants.SECOND_SELECTED_TYRE
                                        .setIsSpare("true");
                            } else if (Boolean
                                    .parseBoolean(Constants.SECOND_SELECTED_TYRE
                                            .isSpare())) {
                                Constants.SELECTED_TYRE.setIsSpare("true");
                                Constants.SECOND_SELECTED_TYRE
                                        .setIsSpare("false");
                            }
                            if (Constants.SELECTED_TYRE.isDismounted() == true) {
                                Constants.SELECTED_TYRE
                                        .setTyreState(TyreState.EMPTY);
                            } else if (Constants.SECOND_SELECTED_TYRE
                                    .isDismounted() == true) {
                                Constants.SECOND_SELECTED_TYRE
                                        .setTyreState(TyreState.EMPTY);
                            }
                            if (Constants.SELECTED_TYRE.isHasMounted() == true) {
                                Constants.SELECTED_TYRE
                                        .setTyreState(TyreState.NEW_TIRE);
                            } else if (Constants.SECOND_SELECTED_TYRE
                                    .isHasMounted() == true) {
                                Constants.SECOND_SELECTED_TYRE
                                        .setTyreState(TyreState.NEW_TIRE);
                            }
                            updateTickForSwap();
                            updateTireImages();
                            // Retorque for swapped tires
                           // if(Job.getTorqueSettingsSame().equalsIgnoreCase(Retorque.TORQUE_SETTING_SAME_FOR_ALL_AXLE)) {
                                boolean forRetorque = Constants.SELECTED_TYRE
                                        .isRetorqued();
                                Constants.SELECTED_TYRE
                                        .setRetorqued(Constants.SECOND_SELECTED_TYRE
                                                .isRetorqued());
                                Constants.SECOND_SELECTED_TYRE
                                        .setRetorqued(forRetorque);
                           // }
                            // Pressure for Swapped tires
                            if (Constants.SOURCE_TYRE != null
                                    && Constants.DES_TYRE != null) {
                                callPressureDialogForAxle();
                            } else if (Constants.SELECTED_TYRE != null
                                    && Constants.SECOND_SELECTED_TYRE != null) {
                                callPressureDialogForAxle();
                            } else {
                                resetBoolValue();
                                resetTyres();
                            }
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Clearing all the static variables used during SWAP
     */
    public static void resetBoolValue() {
        System.out.println(" Reseting all booleans ");
        mEditedUserInput = "";
        Constants.onDragBool_LS = false;// 1
        Constants.ONDRAG = false;// 2
        Constants.onDragPSBool = false;// 3
        Constants.onBrandBlank = false; // Change on 28-11
        Constants.onDRAGReturnBoolPS = false;// 4
        Constants.onSwapOptionsBool = false;// 5
        Constants.onGOBoolLogical = false;// 6
        Constants.onReturnBool = false;// 7
        Constants.onGOBoolLogical = false;//
        Constants.onGOBool = false;// 9
        Constants.onBrandBool = false;// 10
        Constants.onBrandDesignBool = false;
        Constants.DOUBLESWAP = false;
        Constants.EDITED_SERIAL_NUMBER_SOURCE = "";
        Constants.EDITED_SERIAL_NUMBER_DES = "";
        Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED = "";
        Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE = "";
        Constants.ONLONG_PRESS = false;
        Constants.triggerMultipleSwap = 0;
        Constants.DO_PHYSICAL_SWAP = false;
        Constants.triggerMultipleEdit = 0;
        Constants.CHECK_LOGICAL_SWAP = false;
        Constants.UPDATE_SERIAL_DB = false;
        Constants.checkSingleSwap = 0;
        Constants.CLICKED_SERIAL_CONFIRM = false;
        Constants.DO_LOGICAL_SWAP = false;
        Constants.LOGICAL_SWAP_INITIAL = false;
        Constants.OnSerialBool = false;
        mTorStarted = false;
        mSwapPressureStarted = false;
        Constants.INSERT_SWAP_DATA = 0;
        Constants.TRIGGER_LOGICAL = false;
        Constants.NEW_SWAP_POSITION = "";
        Constants.DUPLICATE_SERIAL_ENTRY = false;
        Constants.EDITED_SERIAL_NUMBER = "";
        Constants.SECOND_TYRE_ACTIVE = false;
        Constants.onBrandDesignBlank = false;
        Constants.onBrandBlank = false;
        Constants.tempTyrePositionNew = "";
        Constants.tempTyreSerial = "";
        Constants.tempTyrePosition = "";
        // For Retorque
        Retorque.currentPosition = 0;
        Constants.CHECK_LOGICAL_DONE = -1;
        Constants.IS_DES_LOGICAL_SWAP_DONE = false;
        PressureSwap.position_swap_tyre = 1;
        Constants.ONSTATE_DISMOUNT = false;
        Constants.ONSTATE_MOUNT = false;
        Constants.ONSTATE_TOR = false;
        Constants.ONSTATE_REGROOVE = false;
        Constants.ONSTATE_WOT = false;
        Constants.ONSTATE_MOUNTPWT = false;
        Constants.JOB_CORRECTION_COUNT_SWAP = 0;
        Constants.ONSTATE_INSPECTION = false;
        Constants.IS_TYRE_INSPECTED = false;
    }

    /**
     * Clearing all the static Tire Objects used during SWAP
     */
    public static void resetTyres() {
        Constants.SELECTED_TYRE = null;
        Constants.SECOND_SELECTED_TYRE = null;
        Constants.SOURCE_TYRE = null;
        Constants.DES_TYRE = null;
        Constants.THIRD_SELECTED_TYRE = null;
        Constants.FOURTH_SELECTED_TYRE = null;
    }

    /**
     * Hiding Soft Key-Board
     */
    protected void hideKeyboard() {
        try {
            InputMethodManager in = (InputMethodManager) mContext
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * create context object on attach of fragment
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        ejobFormParentActivity = (EjobFormActionActivity) activity;
        saveIconHandlerActivity = (ISaveIconEjobFragment) activity;
    }

    /**
     * Retaining Variables for change in Orientation
     */
    @Override
    public void onSaveInstanceState(Bundle newState) {
        super.onSaveInstanceState(newState);
        Constants.TIRE_INFO = tyreInfo;
        Constants.JOB_ITEMLIST = mJobItemList;
        Constants.JOB_TIREIMAGE_ITEMLIST = mTireImageItemList;
		/*
		 * if (VehicleSkeletonFragment.mIsTireMenuDisplayed) {
		 * newState.putBoolean("tiremenu", true); }
		 */
        System.out.println("On Saved Instance State :: " + mDialogRefNo);
        newState.putInt("DIALOG_NUMBER", mDialogRefNo);
        newState.putSerializable("selectedTyre", mSelectedTyre);
        if (mDialogRefNo == 12) {
            mReserve_tyre_selected = mReserve_pwt_spinner
                    .getSelectedItemPosition();
            newState.putInt("reserve_tyre_selected", mReserve_tyre_selected);
        }
        newState.putString("sourceTyrePosition", mSourceTyrePosition);
        newState.putString("targetTyrePosition", mTargetTyrePosition);
        newState.putBoolean("dialogInEditMode", dialogInEditMode);
        newState.putString("serialNumberForDialogEdit", serialNumberInEditMode);
        newState.putString("mArrowState", mArrowState);
        newState.putString("serialNumberForDuplicateAlert", mTempNumber);
        if (mAlertDialog != null && mAlertDialog.isShowing()
                && mUserInput != null) {
            newState.putString("userInput", mUserInput.getText().toString());
        }
        // Added these data for notes handling.
        newState.putBoolean("mIsNotesInEditMode", mIsNotesInEditMode);
        newState.putString("mCurentNote", mCurentNote);
        newState.putString("NoteEditTag", NoteEditTag);
        newState.putInt("itr_position", itr_position);
        newState.putInt("action_type", mJobActionType);
        newState.putBoolean("mIsNoteDialogOrientationChange",
                mIsNoteDialogOrientationChange);
    }

    /**
     * Fetching Position By Serial Number
     *
     * @param serialNo
     * @return
     */
    public static String getPosFromSerial(String serialNo) {
        String position = null;
        for (Tyre swapTire : tyreInfo) {
            // map tire selected to tire array
            if (swapTire.getSerialNumber().equals(serialNo)) {
                position = swapTire.getPosition();
            }
        }
        return position;
    }

    /**
     * Operation after confirming serial number during SWAP
     */
    private void serialConfirmationForSwap() {
        try {
            Constants.CLICKED_SERIAL_CONFIRM = true;
            IsTyreInVehicle(Constants.EDITED_SERIAL_NUMBER);
            if (Constants.ONDRAG == true) {
                if (!Constants.DIFFERENT_SIZE_ALLOWED
                        && (!Constants.onBrandBlank)
                        || (Constants.INSERT_SWAP_DATA > 0 || !(Boolean
                        .parseBoolean(Constants.SOURCE_TYRE.isSpare())))) {
                    Intent loadLS = new Intent(mContext,
                            LogicalSwapActivity.class);

                    int pos = Character.getNumericValue(Constants.SELECTED_TYRE
                            .getPosition().charAt(0)) - 1;
                    loadLS.putExtra(mAXLEPRESSURE,
                            mAxleRecommendedPressure.get(pos));
                    loadLS.putExtra(mAXLEPRESSURE_POS, pos);
                    if (Constants.onDragBool_LS == true) {
                        startActivityForResult(loadLS, 48);
                    } else {
                        if (!Constants.DIFFERENT_SIZE_ALLOWED) {
                            Intent loadPS = new Intent(mContext,
                                    PhysicalSwapActivity.class);
                            int posPS = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            loadPS.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(posPS));
                            loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                            if (Constants.onDRAGReturnBoolPS == true) {
                                startActivityForResult(loadPS,
                                        PHYSICAL_SWAP_DRAG_FIANL_INTENT);
                            } else {
                                if (Constants.onDragPSBool == true) {
                                    startActivityForResult(loadPS,
                                            PHYSICAL_SWAP_DRAG_INITIAL_INTENT);
                                }
                            }
                        } else {
                            Intent loadPS = new Intent(mContext,
                                    PhysicalSwapDifferentSize.class);
                            int posPS = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            loadPS.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(posPS));
                            loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                            if (Constants.onDRAGReturnBoolPS == true) {
                                startActivityForResult(loadPS,
                                        PHYSICAL_SWAP_DRAG_FIANL_INTENT);
                            } else {
                                if (Constants.onDragPSBool == true) {

                                    startActivityForResult(loadPS,
                                            PHYSICAL_SWAP_DRAG_INITIAL_INTENT);
                                }
                            }
                        }

                    }
                } else {
                    Intent loadLS = new Intent(mContext,
                            LogicalSwapDifferentSize.class);
                    int pos = Character.getNumericValue(Constants.SELECTED_TYRE
                            .getPosition().charAt(0)) - 1;
                    loadLS.putExtra(mAXLEPRESSURE,
                            mAxleRecommendedPressure.get(pos));
                    loadLS.putExtra(mAXLEPRESSURE_POS, pos);
                    if (Constants.onDragBool_LS == true) {
                        startActivityForResult(loadLS, 48);
                    } else {
                        if (!Constants.DIFFERENT_SIZE_ALLOWED) {
                            Intent loadPS = new Intent(mContext,
                                    PhysicalSwapActivity.class);
                            int posPS = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            loadPS.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(posPS));
                            loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                            if (Constants.onDRAGReturnBoolPS == true) {
                                startActivityForResult(loadPS,
                                        PHYSICAL_SWAP_DRAG_FIANL_INTENT);
                            } else {
                                if (Constants.onDragPSBool == true) {

                                    startActivityForResult(loadPS,
                                            PHYSICAL_SWAP_DRAG_INITIAL_INTENT);
                                }
                            }
                        } else {
                            Intent loadPS = new Intent(mContext,
                                    PhysicalSwapDifferentSize.class);
                            int posPS = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            loadPS.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(posPS));
                            loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                            if (Constants.onDRAGReturnBoolPS == true) {
                                startActivityForResult(loadPS,
                                        PHYSICAL_SWAP_DRAG_FIANL_INTENT);
                            } else {
                                if (Constants.onDragPSBool == true) {

                                    startActivityForResult(loadPS,
                                            PHYSICAL_SWAP_DRAG_INITIAL_INTENT);
                                }
                            }
                        }
                    }
                }
            } else if (Constants.ONDRAG == false) {
                if (Constants.onSwapOptionsBool == true) {
                    Constants.onGOBool = true;
                    Constants.onGOBoolLogical = true;
                    if (!Constants.DIFFERENT_SIZE_ALLOWED
                            && (!Constants.onBrandBlank || !(Boolean
                            .parseBoolean(Constants.SELECTED_TYRE
                                    .isSpare())))) {
                        Intent loadLS = new Intent(mContext,
                                LogicalSwapActivity.class);
                        int pos = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        loadLS.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(pos));
                        loadLS.putExtra(mAXLEPRESSURE_POS, pos);
                        if (Constants.onGOBoolLogical == true) {
                            startActivityForResult(loadLS, 24);
                        }
                    } else {
                        Intent loadLS = new Intent(mContext,
                                LogicalSwapDifferentSize.class);
                        int pos = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        loadLS.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(pos));
                        loadLS.putExtra(mAXLEPRESSURE_POS, pos);
                        if (Constants.onGOBoolLogical == true) {
                            startActivityForResult(loadLS, 24);
                        }
                    }
                } else {
                    if (!Constants.DIFFERENT_SIZE_ALLOWED) {
                        Intent loadPS = new Intent(mContext,

                                PhysicalSwapActivity.class);
                        int posPS = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        loadPS.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(posPS));
                        loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                        if (Constants.onReturnBool == false) {
                            startActivityForResult(loadPS, 1);
                        } else if (Constants.onReturnBool == true) {
                            startActivityForResult(loadPS, 12);
                        }
                    } else {
                        Intent loadPS = new Intent(mContext,

                                PhysicalSwapDifferentSize.class);
                        int posPS = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        loadPS.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(posPS));
                        loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                        if (Constants.onReturnBool == false) {
                            startActivityForResult(loadPS, 1);
                        } else if (Constants.onReturnBool == true) {
                            startActivityForResult(loadPS, 12);
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Operation after confirming new serial number during SWAP
     *
     * @param userInput
     */
    protected void newSerialCnfirmationForSwap(EditText userInput) {
        try {
            if (Constants.ONDRAG
                    && (Constants.SOURCE_TYRE.getSerialNumber()
                    .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER) || Constants.DES_TYRE
                    .getSerialNumber().equalsIgnoreCase(
                            Constants.EDITED_SERIAL_NUMBER))) {
                Constants.DUPLICATE_SERIAL_ENTRY = true;
            }
            if (Constants.SECOND_SELECTED_TYRE != null
                    && !Constants.ONDRAG
                    && (Constants.SELECTED_TYRE.getSerialNumber()
                    .equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER) || Constants.SECOND_SELECTED_TYRE
                    .getSerialNumber().equalsIgnoreCase(
                            Constants.EDITED_SERIAL_NUMBER))) {
                Constants.DUPLICATE_SERIAL_ENTRY = true;
            }
            Constants.OnSerialBool = true;
            if (Constants.SELECTED_TYRE == null
                    && Constants.SECOND_SELECTED_TYRE == null) {
                Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
                Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
            }
            Constants.CLICKED_SERIAL_CONFIRM = true;
            IsTyreInVehicle(Constants.EDITED_SERIAL_NUMBER);
            Constants.triggerMultipleEdit++;
            if (Constants.onDragBool_LS == true) {
                if (!(userInput.getText().toString()
                        .equalsIgnoreCase(Constants.DES_TYRE.getSerialNumber()) || userInput
                        .getText().toString()
                        .equalsIgnoreCase(Constants.DES_TYRE.getSerialNumber()))) {
                    Constants.EDITED_SERIAL_NUMBER = userInput.getText()
                            .toString();
                    Constants.onSwapOptionsBool = true;
                    loadBrandCorrection();

                    try {
                        getTyreDetailsForSerialCorrrection(userInput);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        serialNumberValidation(Constants.EDITED_SERIAL_NUMBER,
                                userInput);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (Constants.onDRAGReturnBoolPS == true) {
                    if (Constants.DOUBLESWAP == true
                            && (!(userInput.getText().toString()
                            .equalsIgnoreCase(Constants.SOURCE_TYRE
                                    .getSerialNumber())) || (!(userInput
                            .getText().toString()
                            .equalsIgnoreCase(Constants.DES_TYRE
                                    .getSerialNumber()))))) {
                        loadBrandCorrection();

                    } else {
                        loadBrandCorrection();
                    }
                } else if (Constants.onDragPSBool == true) {
                    if (Constants.DOUBLESWAP == true
                            && (!(userInput.getText().toString()
                            .equalsIgnoreCase(Constants.SOURCE_TYRE
                                    .getSerialNumber())) || (!(userInput
                            .getText().toString()
                            .equalsIgnoreCase(Constants.DES_TYRE
                                    .getSerialNumber()))))) {
                        loadBrandCorrection();
                    } else if (!(userInput
                            .getText()
                            .toString()
                            .equalsIgnoreCase(
                                    Constants.SOURCE_TYRE.getSerialNumber()) || userInput
                            .getText()
                            .toString()
                            .equalsIgnoreCase(
                                    Constants.DES_TYRE.getSerialNumber()))) {
                        loadBrandCorrection();
                    }
                } else if (Constants.onDragPSBool == false)

                {
                    if (Constants.onReturnBool == false) {

                        if (!(userInput.getText().toString()
                                .equalsIgnoreCase(Constants.SELECTED_TYRE
                                        .getSerialNumber()))) {
                            loadBrandCorrection();
                        }
                    } else if (Constants.onReturnBool == true) {
                        if (!(userInput.getText().toString()
                                .equalsIgnoreCase(Constants.SECOND_SELECTED_TYRE
                                        .getSerialNumber()))) {

                            loadBrandCorrection();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Operation after confirming brand during SWAP
     */
    protected void confirmBrandCorrectionForSwap() {
        try {
            Constants.checkSingleSwap++;
            if (Constants.SELECTED_TYRE == null
                    && Constants.SECOND_SELECTED_TYRE == null) {
                Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
                Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
            }
            if (Constants.ONDRAG == true) {
                if (!Constants.DIFFERENT_SIZE_ALLOWED
                        && (!Constants.onBrandBlank || !(Boolean
                        .parseBoolean(Constants.SOURCE_TYRE.isSpare())))) {
                    Intent loadLS = new Intent(mContext,
                            LogicalSwapActivity.class);
                    int pos = Character.getNumericValue(Constants.SELECTED_TYRE
                            .getPosition().charAt(0)) - 1;
                    loadLS.putExtra(mAXLEPRESSURE,
                            mAxleRecommendedPressure.get(pos));
                    loadLS.putExtra(mAXLEPRESSURE_POS, pos);
                    if (Constants.onDragBool_LS == true) {
                        startActivityForResult(loadLS, 48);
                    } else {
                        if (!Constants.DIFFERENT_SIZE_ALLOWED) {
                            Intent loadPS = new Intent(mContext,
                                    PhysicalSwapActivity.class);
                            int posPS = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            loadPS.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(posPS));
                            loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                            if (Constants.onDRAGReturnBoolPS == true) {
                                startActivityForResult(loadPS,
                                        PHYSICAL_SWAP_DRAG_FIANL_INTENT);
                            } else {
                                if (Constants.onDragPSBool == true) {

                                    startActivityForResult(loadPS,
                                            PHYSICAL_SWAP_DRAG_INITIAL_INTENT);
                                }
                            }
                        } else {
                            Intent loadPS = new Intent(mContext,
                                    PhysicalSwapDifferentSize.class);
                            int posPS = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            loadPS.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(posPS));
                            loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                            if (Constants.onDRAGReturnBoolPS == true) {
                                startActivityForResult(loadPS,
                                        PHYSICAL_SWAP_DRAG_FIANL_INTENT);
                            } else {
                                if (Constants.onDragPSBool == true) {

                                    startActivityForResult(loadPS,
                                            PHYSICAL_SWAP_DRAG_INITIAL_INTENT);
                                }
                            }
                        }
                    }
                } else {
                    Intent loadLS = new Intent(mContext,
                            LogicalSwapDifferentSize.class);
                    int pos = Character.getNumericValue(Constants.SELECTED_TYRE
                            .getPosition().charAt(0)) - 1;
                    loadLS.putExtra(mAXLEPRESSURE,
                            mAxleRecommendedPressure.get(pos));
                    loadLS.putExtra(mAXLEPRESSURE_POS, pos);
                    if (Constants.onDragBool_LS == true) {
                        startActivityForResult(loadLS, 48);
                    } else {
                        if (!Constants.DIFFERENT_SIZE_ALLOWED) {
                            Intent loadPS = new Intent(mContext,
                                    PhysicalSwapActivity.class);
                            int posPS = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            loadPS.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(posPS));
                            loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                            if (Constants.onDRAGReturnBoolPS == true) {
                                startActivityForResult(loadPS,
                                        PHYSICAL_SWAP_DRAG_FIANL_INTENT);
                            } else {
                                if (Constants.onDragPSBool == true) {

                                    startActivityForResult(loadPS,
                                            PHYSICAL_SWAP_DRAG_INITIAL_INTENT);
                                }
                            }
                        } else {
                            Intent loadPS = new Intent(mContext,
                                    PhysicalSwapDifferentSize.class);
                            int posPS = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            loadPS.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(posPS));
                            loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                            if (Constants.onDRAGReturnBoolPS == true) {
                                startActivityForResult(loadPS,
                                        PHYSICAL_SWAP_DRAG_FIANL_INTENT);
                            } else {
                                if (Constants.onDragPSBool == true) {

                                    startActivityForResult(loadPS,
                                            PHYSICAL_SWAP_DRAG_INITIAL_INTENT);
                                }
                            }
                        }

                    }
                }
            } else if (Constants.ONDRAG == false) {
                if (Constants.onSwapOptionsBool == true) {
                    Constants.onGOBool = true;
                    Constants.onGOBoolLogical = true;

                    if (!Constants.DIFFERENT_SIZE_ALLOWED
                            && (!Constants.onBrandBlank || !(Boolean
                            .parseBoolean(Constants.SELECTED_TYRE
                                    .isSpare())))) {
                        Intent loadLS = new Intent(mContext,

                                LogicalSwapActivity.class);
                        int pos = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        loadLS.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(pos));
                        loadLS.putExtra(mAXLEPRESSURE_POS, pos);
                        if (Constants.onGOBoolLogical == true) {
                            startActivityForResult(loadLS, 24);
                        }
                    } else {
                        Intent loadLS = new Intent(mContext,
                                LogicalSwapDifferentSize.class);
                        int pos = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        loadLS.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(pos));
                        loadLS.putExtra(mAXLEPRESSURE_POS, pos);
                        if (Constants.onGOBoolLogical == true) {
                            startActivityForResult(loadLS, 24);
                        }
                    }
                } else {
                    if (!Constants.DIFFERENT_SIZE_ALLOWED) {
                        Intent loadPS = new Intent(mContext,

                                PhysicalSwapActivity.class);
                        int posPS = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        loadPS.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(posPS));
                        loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                        if (Constants.onReturnBool == false) {
                            startActivityForResult(loadPS, 1);
                        } else if (Constants.onReturnBool == true) {
                            startActivityForResult(loadPS, 12);
                        }
                    } else {
                        Intent loadPS = new Intent(mContext,

                                PhysicalSwapDifferentSize.class);
                        int posPS = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        loadPS.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(posPS));
                        loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                        if (Constants.onReturnBool == false) {
                            startActivityForResult(loadPS, 1);
                        } else if (Constants.onReturnBool == true) {
                            startActivityForResult(loadPS, 12);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Operation after editing brand during SWAP
     */
    protected void editBrandCorrectionForSwap() {
        try {
            Constants.onBrandBool = true;
            if (Constants.SELECTED_TYRE == null
                    && Constants.SECOND_SELECTED_TYRE == null) {
                Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
                Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
            }
            if (Constants.ONDRAG == true) {
                if (!Constants.DIFFERENT_SIZE_ALLOWED
                        && (!Constants.onBrandBlank || (Constants.INSERT_SWAP_DATA > 0 || !(Boolean
                        .parseBoolean(Constants.SOURCE_TYRE.isSpare()))))) {
                    Intent loadLS = new Intent(mContext,
                            LogicalSwapActivity.class);
                    int pos = Character.getNumericValue(Constants.SELECTED_TYRE
                            .getPosition().charAt(0)) - 1;
                    loadLS.putExtra(mAXLEPRESSURE,
                            mAxleRecommendedPressure.get(pos));
                    loadLS.putExtra(mAXLEPRESSURE_POS, pos);
                    if (Constants.onDragBool_LS == true) {
                        startActivityForResult(loadLS, 48);
                    } else {
                        if (!Constants.DIFFERENT_SIZE_ALLOWED) {
                            Intent loadPS = new Intent(mContext,
                                    PhysicalSwapActivity.class);
                            int posPS = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            loadPS.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(posPS));
                            loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                            if (Constants.onDRAGReturnBoolPS == true) {
                                startActivityForResult(loadPS,
                                        PHYSICAL_SWAP_DRAG_FIANL_INTENT);
                            } else {
                                if (Constants.onDragPSBool == true) {

                                    startActivityForResult(loadPS,
                                            PHYSICAL_SWAP_DRAG_INITIAL_INTENT);
                                }
                            }
                        } else {
                            Intent loadPS = new Intent(mContext,
                                    PhysicalSwapDifferentSize.class);
                            int posPS = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            loadPS.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(posPS));
                            loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                            if (Constants.onDRAGReturnBoolPS == true) {
                                startActivityForResult(loadPS,
                                        PHYSICAL_SWAP_DRAG_FIANL_INTENT);
                            } else {
                                if (Constants.onDragPSBool == true) {

                                    startActivityForResult(loadPS,
                                            PHYSICAL_SWAP_DRAG_INITIAL_INTENT);
                                }
                            }
                        }
                    }
                } else {
                    Intent loadLS = new Intent(mContext,
                            LogicalSwapDifferentSize.class);
                    int pos = Character.getNumericValue(Constants.SELECTED_TYRE
                            .getPosition().charAt(0)) - 1;
                    loadLS.putExtra(mAXLEPRESSURE,
                            mAxleRecommendedPressure.get(pos));
                    loadLS.putExtra(mAXLEPRESSURE_POS, pos);
                    if (Constants.onDragBool_LS == true) {
                        startActivityForResult(loadLS, 48);
                    } else {
                        if (!Constants.DIFFERENT_SIZE_ALLOWED) {
                            Intent loadPS = new Intent(mContext,
                                    PhysicalSwapActivity.class);
                            int posPS = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            loadPS.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(posPS));
                            loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                            if (Constants.onDRAGReturnBoolPS == true) {
                                startActivityForResult(loadPS,
                                        PHYSICAL_SWAP_DRAG_FIANL_INTENT);
                            } else {
                                if (Constants.onDragPSBool == true) {

                                    startActivityForResult(loadPS,
                                            PHYSICAL_SWAP_DRAG_INITIAL_INTENT);
                                }
                            }
                        } else {
                            Intent loadPS = new Intent(mContext,
                                    PhysicalSwapDifferentSize.class);
                            int posPS = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            loadPS.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(posPS));
                            loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                            if (Constants.onDRAGReturnBoolPS == true) {
                                startActivityForResult(loadPS,
                                        PHYSICAL_SWAP_DRAG_FIANL_INTENT);
                            } else {
                                if (Constants.onDragPSBool == true) {

                                    startActivityForResult(loadPS,
                                            PHYSICAL_SWAP_DRAG_INITIAL_INTENT);
                                }
                            }
                        }

                    }
                }
            } else if (Constants.ONDRAG == false) {
                if (Constants.onSwapOptionsBool == true) {
                    Constants.onGOBool = true;
                    Constants.onGOBoolLogical = true;
                    if (!Constants.DIFFERENT_SIZE_ALLOWED
                            && (!Constants.onBrandBlank || !(Boolean
                            .parseBoolean(Constants.SELECTED_TYRE
                                    .isSpare())))) {
                        Intent loadLS = new Intent(mContext,
                                LogicalSwapActivity.class);
                        int posPS = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        loadLS.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(posPS));
                        loadLS.putExtra(mAXLEPRESSURE_POS, posPS);
                        if (Constants.onGOBoolLogical == true) {
                            startActivityForResult(loadLS, 24);
                        }
                    } else {
                        Intent loadLS = new Intent(mContext,
                                LogicalSwapDifferentSize.class);
                        int posPS = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        loadLS.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(posPS));
                        loadLS.putExtra(mAXLEPRESSURE_POS, posPS);
                        if (Constants.onGOBoolLogical == true) {
                            startActivityForResult(loadLS, 24);
                        }
                    }

                } else {
                    if (!Constants.DIFFERENT_SIZE_ALLOWED) {
                        Intent loadPS = new Intent(mContext,
                                PhysicalSwapActivity.class);
                        int posPS = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        loadPS.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(posPS));
                        loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                        loadPS.putExtra("SELECTED_SERIAL_NUMBER",
                                Constants.EDITED_SERIAL_NUMBER);
                        loadPS.putExtra("SELECTED_POSITION",
                                mSelectedTyre.getPosition());
                        if (Constants.onReturnBool == false) {
                            startActivityForResult(loadPS, 1);
                        } else if (Constants.onReturnBool == true) {
                            startActivityForResult(loadPS, 12);
                        }
                    } else {
                        Intent loadPS = new Intent(mContext,
                                PhysicalSwapDifferentSize.class);
                        int posPS = Character
                                .getNumericValue(Constants.SELECTED_TYRE
                                        .getPosition().charAt(0)) - 1;
                        loadPS.putExtra(mAXLEPRESSURE,
                                mAxleRecommendedPressure.get(posPS));
                        loadPS.putExtra(mAXLEPRESSURE_POS, posPS);
                        loadPS.putExtra("SELECTED_SERIAL_NUMBER",
                                Constants.EDITED_SERIAL_NUMBER);
                        loadPS.putExtra("SELECTED_POSITION",
                                mSelectedTyre.getPosition());
                        if (Constants.onReturnBool == false) {
                            startActivityForResult(loadPS, 1);
                        } else if (Constants.onReturnBool == true) {
                            startActivityForResult(loadPS, 12);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Calling Pressure Dialog during SWAP
     */
    private void callPressureDialogForAxle() {
        try {
            mSwapPressureStarted = true;
            PressureSwap pressure = new PressureSwap(Constants.SELECTED_TYRE,
                    mContext);
            int axlepos = Character.getNumericValue(Constants.SELECTED_TYRE
                    .getPosition().charAt(0)) - 1;
            float recommendedPressure = mAxleRecommendedPressure.get(axlepos);

            int axlepos2 = Character
                    .getNumericValue(Constants.SECOND_SELECTED_TYRE
                            .getPosition().charAt(0)) - 1;
            float recommendedPressure2 = mAxleRecommendedPressure.get(axlepos2);

            pressure.updatePressureFromAxleRecommended(recommendedPressure,
                    axlepos, false);
            if (recommendedPressure > 0 && recommendedPressure2 > 0) {
                mIsPressureHigh = false;
            }
            if (recommendedPressure > 0) {
                mIsPressureHigh = true;
            }
            PressureSwap pressure2 = new PressureSwap(
                    Constants.SECOND_SELECTED_TYRE, mContext);
            if (recommendedPressure > 0 && recommendedPressure2 == 0) {
                mIsPressureHigh = false;
                pressure2.updatePressureFromAxleRecommended(
                        recommendedPressure2, axlepos2, false);
            }
            float recommendedPressure3 = -1;
            int axlepos3 = -1;
            if (Constants.THIRD_SELECTED_TYRE != null) {
                axlepos3 = Character
                        .getNumericValue(Constants.THIRD_SELECTED_TYRE
                                .getPosition().charAt(0)) - 1;
                recommendedPressure3 = VehicleSkeletonFragment.mAxleRecommendedPressure
                        .get(axlepos3);
            }
            float recommendedPressure4 = -1;
            if (Constants.FOURTH_SELECTED_TYRE != null) {
                int axlepos4 = Character
                        .getNumericValue(Constants.FOURTH_SELECTED_TYRE
                                .getPosition().charAt(0)) - 1;
                recommendedPressure4 = VehicleSkeletonFragment.mAxleRecommendedPressure
                        .get(axlepos4);

            }
            // triple swap
            if (recommendedPressure != 0 && recommendedPressure2 != 0
                    && recommendedPressure3 == 0 && recommendedPressure4 == 0) {
                PressureSwap pressuretriple = new PressureSwap(
                        Constants.THIRD_SELECTED_TYRE, mContext);
                mIsPressureHigh = false;
                pressuretriple.updatePressureFromAxleRecommended(
                        recommendedPressure3, axlepos3, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Constants.onReturnBool = false;
        }
    }

    /**
     * Updating Job Correction Table With New Serial Number
     */
    public static void updateJobCorrection() {
        try {
            JobCorrection jobcor = new JobCorrection();
            jobcor.setmJobID("");
            jobcor.setmTyreID(Constants.TYRE_ID);
            jobcor.setmKeyName("SERIAL_NUM");
            if (null == Constants.CURRENT_SERIAL_NUMBER) {
                jobcor.setmOldValue("");
            } else {
                jobcor.setmOldValue(Constants.CURRENT_SERIAL_NUMBER);
            }
            jobcor.setmNewValue(Constants.EDITED_SERIAL_NUMBER);
            if (mJobcorrectionList != null) {
                //Added a check to avoid unwanted job Correction
                if(!jobcor.getmOldValue().equalsIgnoreCase(jobcor.getmNewValue())) {
                    mJobcorrectionList.add(jobcor);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Updating Job Correction Table with new Material-Code ID for a particular
     * Brand
     */
    public static void updateJobCorrectionForBrand() {
        try {
            JobCorrection jobcor = new JobCorrection();
            jobcor.setmJobID("");
            jobcor.setmTyreID(Constants.TYRE_ID);
            jobcor.setmKeyName("MATERIAL_NUM");
            if (Constants.SECOND_SELECTED_TYRE != null
                    && (Constants.onDRAGReturnBoolPS == true
                    || Constants.onGOBool == true
                    || !Constants.LOGICAL_SWAP_INITIAL || (Constants.DO_LOGICAL_SWAP && Constants.SELECTED_TYRE
                    .getTyreState() == TyreState.PART_WORN))) {
                if (null == Constants.SECOND_SELECTED_TYRE
                        .getSapMaterialCodeID()) {
                    jobcor.setmOldValue("");
                } else {
                    jobcor.setmOldValue(Constants.SECOND_SELECTED_TYRE
                            .getSapMaterialCodeID());
                }
            } else {
                if (null == Constants.SELECTED_TYRE.getSapMaterialCodeID()) {
                    jobcor.setmOldValue("");
                } else {
                    jobcor.setmOldValue(Constants.SELECTED_TYRE
                            .getSapMaterialCodeID());
                }
            }
            jobcor.setmNewValue(Constants.NEW_SAPMATERIAL_CODE);
            //Added a check to avoid unwanted job Correction
            if(!jobcor.getmOldValue().equalsIgnoreCase(jobcor.getmNewValue())) {
                mJobcorrectionList.add(jobcor);
            }
            Constants.onBrandBool = false;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Constants.FULLDESIGN_SELECTED = "";
            Constants.NEW_SAPMATERIAL_CODE = "";
        }
    }

    /**
     * Checking whether the selected tire is there in vehicle
     *
     * @param userEnteredSerialNumber
     * @return
     */
    public boolean IsTyreInVehicle(String userEnteredSerialNumber) {
        if (userEnteredSerialNumber == null) {
            return false;
        }
        //Fix:: 666 (NA/NR will not be a part of Auto-Logical Swap)
        if (userEnteredSerialNumber.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP) ||
                userEnteredSerialNumber.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP)) {
            return false;
        }
        boolean check = true;
        for (Tyre currentTire : tyreInfo) {
            userEnteredSerialNumber = currentTire.getSerialNumber();
            if (userEnteredSerialNumber.equals(Constants.EDITED_SERIAL_NUMBER)) {
                Constants.DOUBLESWAP = true;
                Constants.triggerMultipleSwap++;
                check = false;
                break;
            }
        }
        return !check;
    }

    /**
     * Checking whether the selected serial number is there on vehicle
     *
     * @param isSerialNumberInVehicle
     * @return
     */
    public static boolean isSerialNumberInVehicle(String isSerialNumberInVehicle) {
        boolean check = true;
        //Fix:: 666 (NA/NR will not be a part of Auto-Logical Swap)
        if (isSerialNumberInVehicle.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP) ||
                isSerialNumberInVehicle.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP)) {
            return false;
        }
        for (Tyre currentTire : tyreInfo) {
            String userEnteredSerialNumber = currentTire.getSerialNumber();
            if (userEnteredSerialNumber.equals(isSerialNumberInVehicle)) {
                Constants.DOUBLESWAP = true;
                Constants.triggerMultipleSwap++;
                check = false;
                break;
            }
        }
        return !check;
    }

    /**
     * Returns True if the second tire is on different axle
     *
     * @return
     */
    private boolean checkDifferentAxle() {
        if (Constants.SELECTED_TYRE == null) {
            Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
        }
        if (Constants.SECOND_SELECTED_TYRE == null) {
            Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
        }
        boolean check = false;
        String mFirstAxleNumber = Constants.SELECTED_TYRE.getPosition().charAt(
                0)
                + "";
        String mSecondAxleNumber = Constants.SECOND_SELECTED_TYRE.getPosition()
                .charAt(0) + "";
        if (!mFirstAxleNumber.equalsIgnoreCase(mSecondAxleNumber)) {
            if (!Constants.SELECTED_TYRE.getSize().equalsIgnoreCase(
                    Constants.SECOND_SELECTED_TYRE.getSize())) {
                check = false;
            }
        }
        return check;
    }

    /**
     * Updating Notes into JobItemList
     */
    private void updateNotesInJobList(int i, int actiontype, String notes) {
        if (null != mJobItemList) {
            if (actiontype != 1) {
                mJobItemList.get(i).setNote(notes);
            } else {
                String noteTextToAdd = "";
                String noteTextAlreadyPresent = mJobItemList.get(i).getNote();
                if (!TextUtils.isEmpty(noteTextAlreadyPresent)
                        && noteTextAlreadyPresent.indexOf("|") > 0) {
                    int endPos = noteTextAlreadyPresent.indexOf("|") + 1;
                    noteTextToAdd = noteTextAlreadyPresent.substring(0, endPos);
                }
                mJobItemList.get(i).setNote(noteTextToAdd + notes);
            }
        }
    }

    /**
     * Reset tire state on Cancel
     */
    public static void resetTyreState() {
        Constants.SELECTED_TYRE
                .setSerialNumber(Constants.SELECTED_TYRE_SerialNumber);
        Constants.SELECTED_TYRE.setNsk(Constants.SELECTED_TYRE_setNsk);
        Constants.SELECTED_TYRE.setNsk2(Constants.SELECTED_TYRE_setNsk2);
        Constants.SELECTED_TYRE.setNsk3(Constants.SELECTED_TYRE_setNsk3);
        Constants.SELECTED_TYRE
                .setVehicleJob(Constants.SELECTED_TYRE_setVehicleJob);
        Constants.SELECTED_TYRE.setRimType(Constants.SELECTED_TYRE_setRimType);
        Constants.SELECTED_TYRE
                .setBrandName(Constants.SELECTED_TYRE_setBrandName);
        Constants.SELECTED_TYRE.setSize(Constants.SELECTED_TYRE_setSize);
        Constants.SELECTED_TYRE.setAsp(Constants.SELECTED_TYRE_setAsp);
        Constants.SELECTED_TYRE.setRim(Constants.SELECTED_TYRE_setRim);
        Constants.SELECTED_TYRE.setDesign(Constants.SELECTED_TYRE_setDesign);
        Constants.SELECTED_TYRE
                .setDesignDetails(Constants.SELECTED_TYRE_setDesignDetails);
        int pos = Character.getNumericValue(Constants.SELECTED_TYRE
                .getPosition().charAt(0)) - 1;
        VehicleSkeletonFragment.mAxleRecommendedPressure.set(pos,
                Constants.SELECTED_TYRE_setAxleRecommendedPressure);
        Constants.SELECTED_TYRE.setPressure(String
                .valueOf(Constants.SELECTED_TYRE_setAxleRecommendedPressure));
    }

    /**
     * Getting tire state on Selection to restore data if canceled
     */
    private void getTyreState() {
        Constants.SELECTED_TYRE_SerialNumber = Constants.SELECTED_TYRE
                .getSerialNumber();
        Constants.SELECTED_TYRE_setNsk = Constants.SELECTED_TYRE.getNsk();
        Constants.SELECTED_TYRE_setNsk2 = Constants.SELECTED_TYRE.getNsk2();
        Constants.SELECTED_TYRE_setNsk3 = Constants.SELECTED_TYRE.getNsk3();
        Constants.SELECTED_TYRE_setVehicleJob = Constants.SELECTED_TYRE
                .getVehicleJob();
        Constants.SELECTED_TYRE_setRimType = Constants.SELECTED_TYRE.getNsk3();
        Constants.SELECTED_TYRE_setBrandName = Constants.SELECTED_TYRE
                .getBrandName();
        Constants.SELECTED_TYRE_setSize = Constants.SELECTED_TYRE.getSize();
        Constants.SELECTED_TYRE_setAsp = Constants.SELECTED_TYRE.getAsp();
        Constants.SELECTED_TYRE_setRim = Constants.SELECTED_TYRE.getRim();
        Constants.SELECTED_TYRE_setDesign = Constants.SELECTED_TYRE.getDesign();
        Constants.SELECTED_TYRE_setDesignDetails = Constants.SELECTED_TYRE
                .getDesignDetails();
        int pos = Character.getNumericValue(Constants.SELECTED_TYRE
                .getPosition().charAt(0)) - 1;
        Constants.SELECTED_TYRE_setAxleRecommendedPressure = mAxleRecommendedPressure
                .get(pos);
    }

    /**
     * Showing alert if any changes in note
     */
    private void showAlertForNoteCancle(String previousValue,
                                        String currentValue, int job_indx, int actionType) {
        if (!previousValue.equals(currentValue.trim())) {
            mIsNoteDialogOrientationChange = false;
            createAlertDialog(job_indx, currentValue, actionType);
        } else {
            mIsNotesInEditMode = false;
        }
    }

    /**
     * Showing alert if any changes in note
     */
    private void createAlertDialog(final int job_indx, final String notes,
                                   final int actionType) {
        LayoutInflater li = LayoutInflater.from(mContext);
        View promptsView = li
                .inflate(R.layout.dialog_logout_confirmation, null);
        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(mContext);
        alertDialogBuilder.setView(promptsView);
        TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
        infoTv.setText("Do you want to save the note ?");
        alertDialogBuilder.setCancelable(false).setPositiveButton(mYesLabel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // set user activity
                        alertDialogBuilder.updateInactivityForDialog();

                        updateNotesInJobList(job_indx, actionType, notes);
                        mIsNotesInEditMode = false;
                        dialog.dismiss();
                        mCurentNote = "";
                        NoteEditTag = "";
                    }
                });
        alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // set user activity
                        alertDialogBuilder.updateInactivityForDialog();

                        mIsNotesInEditMode = false;
                        mCurentNote = "";
                        NoteEditTag = "";
                        dialog.dismiss();
                    }
                });
        AlertDialog noteDialog = alertDialogBuilder.create();
        noteDialog.setCanceledOnTouchOutside(false);
        noteDialog.show();
    }

    /**
     * Get Note value from JobItemlist based on index
     */
    private String getNotes(int job_indx, int actionType) {
        if (null != mJobItemList) {
            if (actionType != 1) {
                return mJobItemList.get(job_indx).getNote();
            } else {
                String noteText = mJobItemList.get(job_indx).getNote();
                int startPos = noteText.indexOf("|") > 0 ? noteText
                        .indexOf("|") + 1 : 0;
                return noteText.substring(startPos, noteText.length());
            }
        } else {
            return "";
        }
    }

    /**
     * Returns true if Double Swap is being Performed(Scenario 2)
     *
     * @return
     */
    public static boolean checkForDoubleSwap() {
        if (Constants.SELECTED_TYRE == null) {
            Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
        }
        if (Constants.SECOND_SELECTED_TYRE == null) {
            Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
        }
        if (Constants.SELECTED_TYRE.getSerialNumber().equalsIgnoreCase(
                Constants.SELECTED_TYRE.getSerialNumber())
                && !Constants.SECOND_SELECTED_TYRE.getSerialNumber()
                .equalsIgnoreCase(
                        Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE)) {
            return true;
        }
        return false;
    }

    /**
     * Returns true if Double Swap is being Performed(Scenario 1)
     *
     * @return
     */
    public static boolean checkForDoubleSwapForSenario1() {
        if (Constants.SELECTED_TYRE == null) {
            Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
        }
        if (Constants.SECOND_SELECTED_TYRE == null) {
            Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
        }
        if (Constants.SELECTED_TYRE.getSerialNumber().equalsIgnoreCase(
                Constants.SELECTED_TYRE.getSerialNumber())
                && !Constants.SECOND_SELECTED_TYRE.getSerialNumber()
                .equalsIgnoreCase(
                        Constants.SECOND_SELECTED_TYRE
                                .getSerialNumber())) {
            return true;
        }
        return false;
    }

    /**
     * Check is same serial number existed in vehicle
     */
    public static boolean isSameSerialNumberInVehicle(Tyre tyre) {
        boolean check = true;
        //Fix:: 666 (NA/NR will not be a part of Auto-Logical Swap)
        if (tyre.getSerialNumber().equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP) ||
                tyre.getSerialNumber().equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP)) {
            return false;
        }
        for (Tyre currentTire : tyreInfo) {
            if (currentTire.getPosition().equals(tyre.getPosition())) {
                continue;
            }
            String userEnteredSerialNumber = currentTire.getSerialNumber();
            if (userEnteredSerialNumber.equals(tyre.getSerialNumber())) {
                Constants.DOUBLESWAP = true;
                Constants.triggerMultipleSwap++;
                check = false;
                break;
            }
        }
        return !check;
    }

    /**
     * Show Alert dialog box duplicate serial number allow in dismount operation
     */
    private void showDuplicateSerialNoAlertForDismount() {
        mDialogRefNo = 19;
        LayoutInflater li = LayoutInflater.from(mContext);
        View promptsView = li
                .inflate(R.layout.dialog_logout_confirmation, null);
        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(mContext);
        alertDialogBuilder.setView(promptsView);
        // set user activity for
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });
        TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
        infoTv.setText("This tyre Serial number is not unique, is it OK to have duplicate Serial number in this job? \n");
        alertDialogBuilder.setCancelable(false).setPositiveButton(mYesLabel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // set user activity
                        alertDialogBuilder.updateInactivityForDialog();

                        mDialogRefNo = -1;
                        dialog.dismiss();
                        if (Constants.ONSTATE_DISMOUNT) {
                            Intent dismountTireIntent = new Intent(mContext,
                                    DismountTireActivity.class);
                            startActivityForResult(dismountTireIntent,
                                    VehicleSkeletonFragment.DISMOUNT_INTENT);
                            mIsTireMenuDisplayed = false;
                            Constants.ONSTATE_DISMOUNT = false;
                        } else if (Constants.ONSTATE_TOR) {
                            Intent startRegrooveOperation = new Intent(
                                    mContext, TurnOnRim.class);
                            int pos = Character
                                    .getNumericValue(Constants.SELECTED_TYRE
                                            .getPosition().charAt(0)) - 1;
                            startRegrooveOperation.putExtra(mAXLEPRESSURE,
                                    mAxleRecommendedPressure.get(pos));
                            startRegrooveOperation.putExtra(mAXLEPRESSURE_POS,
                                    pos);
                            startActivityForResult(startRegrooveOperation,
                                    VehicleSkeletonFragment.TOR_INTENT);
                            mIsTireMenuDisplayed = false;
                            Constants.ONSTATE_TOR = false;
                        }
                    }
                });
        alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // set user activity
                        alertDialogBuilder.updateInactivityForDialog();

                        mDialogRefNo = -1;
                        dialog.dismiss();
                    }
                });
        AlertDialog dismountDuplicateSerialAlert = alertDialogBuilder.create();
        dismountDuplicateSerialAlert.setCanceledOnTouchOutside(false);
        dismountDuplicateSerialAlert.show();
    }

    /**
     * Updating Tire Ticks On Swap
     */
    private void updateTickForSwap() {
        Tyre tire;
        if (!TextUtils.isEmpty(mPosition_three)) {
            tire = getTireByPosition(mPosition_three);
            if (tire != null)
                tire.setLogicalySwaped(true);
        }
        if (!TextUtils.isEmpty(mPosition_four)) {
            tire = getTireByPosition(mPosition_four);
            if (tire != null)
                tire.setLogicalySwaped(true);
        }
        if (Constants.DO_PHYSICAL_SWAP) {
            tire = getTireByPosition(mPosition_one);
            if (tire != null)
                tire.setIsphysicalySwaped(true);
            tire = getTireByPosition(mPosition_two);
            if (tire != null)
                tire.setIsphysicalySwaped(true);
        } else {
            tire = getTireByPosition(mPosition_one);
            if (tire != null)
                tire.setLogicalySwaped(true);
            tire = getTireByPosition(mPosition_two);
            if (tire != null)
                tire.setLogicalySwaped(true);
        }
    }

    /*
	 * @see com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#
	 * onActivityBackPress()
	 */
    @Override
    public boolean onActivityBackPress() {
        return false;
    }

    /*
	 * @see
	 * com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#hideKeyBoard
	 * (boolean)
	 */
    @Override
    public void hideKeyBoard(boolean toHide) {
        if (toHide) {
            hideKeyboard();
        }
    }

    public interface PressureDialogCreated {
        public void pressureDialogStarted(PressureCheck pressure);

        public void pressureDialogStartedForSwap(PressureSwap pressureSwap);
    }

    /**
     * Method enabling the save icon as per the user selection on the UI It will
     * always turn the save icon color into red if there will be data to be
     * saved for the local database file
     */
    public void enableSaveButton() {
        if (mSaveJob != null) {
            mSaveJob.setEnabled(true);
            Constants.FINAL_SAVE_STATE = false;
            saveIconHandlerActivity
                    .setSaveIconState(EjobSaveIconStates.ICON_RED);
            mSaveJob.setBackgroundResource(R.drawable.continue_savejob_navigation);
        }
    }

    /**
     * Method disabling the save icon as per the user selection on the UI It
     * will always turn the save icon color into grey if there will not be any
     * data to be saved for the local database file
     */
    public void disableSaveButton() {
        if (mSaveJob != null) {
            mSaveJob.setEnabled(false);
            saveIconHandlerActivity
                    .setSaveIconState(EjobSaveIconStates.ICON_GRAY);
            mSaveJob.setBackgroundResource(R.drawable.unsavejob_navigation);
        }
    }

    /**
     * This method will always turn the save icon color into black if there will
     * not be any data to be saved for the local database file It will navigate
     * user back to the eJoblist
     */
    public void finalSaveButton() {
        if (mSaveJob != null) {
			mSaveJob.setEnabled(true);
            saveIconHandlerActivity
                    .setSaveIconState(EjobSaveIconStates.ICON_BLACK);
            mSaveJob.setBackgroundResource(R.drawable.savejob_navigation);
        }
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.IEjobFragmentVisible#fragmentIsVisibleToUser
	 * ()
	 */
    @Override
    public void fragmentIsVisibleToUser() {
        updateSaveButtonOnUIBasedOnLastFragment();
    }
}
