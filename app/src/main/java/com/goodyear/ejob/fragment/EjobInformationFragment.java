package com.goodyear.ejob.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.adapter.EJobPagerAdapter;
import com.goodyear.ejob.authenticator.Authentication;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.interfaces.IEjobFragmentVisible;
import com.goodyear.ejob.interfaces.IFragmentCommunicate;
import com.goodyear.ejob.interfaces.ISaveIconEjobFragment;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.EjobSaveIconStates;
import com.goodyear.ejob.util.Job;
import com.goodyear.ejob.util.JobBundle;
import com.goodyear.ejob.util.LogUtil;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author shailesh.p
 * 
 */

public class EjobInformationFragment extends Fragment implements
		IFragmentCommunicate, IEjobFragmentVisible {

	public static int sRefNoIncrement;
	public int mCountScreenNotVisibleToUser;

	public TextView mLabelPrimaryODO;
	public EditText sValPrimaryODO;

	public TextView mLabelSecondaryODO;
	public EditText sValSecondaryODO;

	public TextView mLabelCallCenterRefNo;
	public EditText valueCallCenterRefNo;

	public TextView mLabelDriverName;
	public EditText sValueDriverName;

	public TextView mLabelDefectNo;
	public EditText sValueDefectNo;

	public TextView mLabelCallerName;
	public EditText sValueCallerName;

	public TextView mLabelCallerPhone;
	public EditText sValueCallerPhone;

	public TextView mLabelAccount;
	public TextView sValueAccount;

	public TextView mLabelVehId;
	public TextView sValueVehId;

	public TextView mLabelFleetNumber;
	public TextView sValueFleetNumber;

	public TextView mLabelTechID;
	public TextView sValueTechid;

	private TextView mLabelRefNumber;
	public TextView sValueRefNumber;

	public static Button sSaveButton;
	public ImageView mJobTypeIcon;

	public TextView mActivityName;

	private static ISaveIconEjobFragment saveIconHandlerActivity;

	//User Trace logs trace tag
	private static final String TRACE_TAG = "Job Form - 1";

	//Fix - Bug : 702 (restrict users from entering 0 (zero) as a tacho calue)
	//Regular Expression for Zero Entry
	public static final String Regex_For_Zero_Entry = "^[0]+$";


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.ejob_information, container,
				false);

		initalize(rootView);
		loadLabelsFromDB(rootView);

		return rootView;
	}

	/**
	 * @param rootView
	 */
	private void loadLabelsFromDB(View rootView) {
		DatabaseAdapter label = new DatabaseAdapter(getActivity());
		label.createDatabase();
		label.open();

		String activityname = label.getLabel(Constants.EJOB_FORM_ACTIVITY_NAME);
		mActivityName.setText(activityname);

		// header
		String labelFromDB = label.getLabel(Constants.ACCOUNT_LABEL);
		mLabelAccount.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.VEH_ID_LABEL);
		mLabelVehId.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.FLEET_NO);
		mLabelFleetNumber.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.TECHNICIAN_LABEL);
		mLabelTechID.setText(labelFromDB);

		sValueTechid.setText(Authentication.getUserName(Constants.USER));

		if (!Constants.COMESFROMUPDATE && !Constants.COMESFROMVIEW) {
			sValueAccount.setText(Constants.ACCOUNTNAME);
			sValueVehId.setText(Constants.VEHID);
			sValueFleetNumber.setText(Constants.FLEETNUM);
			mLabelRefNumber.setText("#");
			sValueRefNumber.setText(Constants.JOBREFNUMBER);
		}

		// form
		labelFromDB = label.getLabel(Constants.PRIMARY_ODO_LABEL);
		mLabelPrimaryODO.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.SECONDARY_ODO_LABEL);
		mLabelSecondaryODO.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.CALL_CENTER_REF_NUMBER_LABEL);
		mLabelCallCenterRefNo.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.DRIVER_NAME_LABEL);
		mLabelDriverName.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.DEFECT_NUMBER_LABEL);
		mLabelDefectNo.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.CALLER_NAME_LABEL);
		mLabelCallerName.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.CALLER_PHONE_NUMBER_LABEL);
		mLabelCallerPhone.setText(labelFromDB);

		label.close();
	}

	private void initalize(View view) {
		sSaveButton = (Button) view.findViewById(R.id.btn_save);
		sSaveButton.setEnabled(false);
		sSaveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				EjobFormActionActivity.saveJobInfo(view);
			}
		});

		mActivityName = (TextView) view.findViewById(R.id.textView1);
		// header
		mLabelAccount = (TextView) view.findViewById(R.id.lbl_account);
		sValueAccount = (TextView) view.findViewById(R.id.value_account);

		mLabelVehId = (TextView) view.findViewById(R.id.lbl_vehId);
		sValueVehId = (TextView) view.findViewById(R.id.value_vehId);

		mLabelFleetNumber = (TextView) view.findViewById(R.id.lbl_fleetNo);
		sValueFleetNumber = (TextView) view.findViewById(R.id.value_fleetNo);

		mLabelRefNumber = (TextView) view.findViewById(R.id.lbl_jobRefNo);
		sValueRefNumber = (TextView) view.findViewById(R.id.value_jobRefNo);

		// form
		mLabelPrimaryODO = (TextView) view.findViewById(R.id.lbl_primaryODO);
		sValPrimaryODO = (EditText) view.findViewById(R.id.value_primaryODO);
		// length of primary odo length should be less then 12
		/*Jira EJOB-97 : Fix for higher odometer value, length cannot be more than 9*/
		sValPrimaryODO.setFilters(new InputFilter[] {
				new InputFilter.LengthFilter(9), numericFilter });
		sValPrimaryODO.addTextChangedListener(new PrimaryOdometerTextChanged());
		//CommonUtils.setFocus(sValPrimaryODO);
		mLabelSecondaryODO = (TextView) view
				.findViewById(R.id.lbl_secondaryODO);
		sValSecondaryODO = (EditText) view
				.findViewById(R.id.value_secondaryODO);
		// length of primary odo length should be less then 12
		/*Jira EJOB-97 : Fix for higher odometer value, length cannot be more than 9*/
		sValSecondaryODO.setFilters(new InputFilter[] {
				new InputFilter.LengthFilter(9), numericFilter });
		sValSecondaryODO.addTextChangedListener(new EditedText());

		mLabelCallCenterRefNo = (TextView) view
				.findViewById(R.id.lbl_callCenterRefNo);
		valueCallCenterRefNo = (EditText) view
				.findViewById(R.id.value_callCenterRefNo);
		// length of primary odo length should be less then 12
		// valueCallCenterRefNo.setFilters(new InputFilter[] {new
		// InputFilter.LengthFilter(15), alphaNumericFilter });
		valueCallCenterRefNo.setFilters(new InputFilter[] {
				new InputFilter.AllCaps(), new InputFilter.LengthFilter(15),
				alphaNumericFilter });
		// valueCallCenterRefNo.addTextChangedListener(new EditedText());

		mLabelDriverName = (TextView) view.findViewById(R.id.lbl_driverName);
		sValueDriverName = (EditText) view.findViewById(R.id.value_driverName);
		// length of primary odo length should be less then 12
		sValueDriverName.setFilters(new InputFilter[] {
				new InputFilter.AllCaps(), new InputFilter.LengthFilter(25),
				CommonUtils.alphaFilter });
		sValueDriverName.addTextChangedListener(new EditedText());

		mLabelDefectNo = (TextView) view.findViewById(R.id.lbl_defectAuthNo);
		sValueDefectNo = (EditText) view.findViewById(R.id.value_defectAuthNo);
		// length of primary odo length should be less then 12
		// sValueDefectNo.setFilters(new InputFilter[] {new
		// InputFilter.LengthFilter(15), alphaNumericFilter });
		sValueDefectNo.setFilters(new InputFilter[] {
				new InputFilter.AllCaps(), new InputFilter.LengthFilter(15),
				alphaNumericFilter });
		// sValueDefectNo.addTextChangedListener(new EditedText());

		mLabelCallerName = (TextView) view.findViewById(R.id.lbl_callerName);
		sValueCallerName = (EditText) view.findViewById(R.id.value_callerName);
		// length of primary odo length should be less then 12
		sValueCallerName.setFilters(new InputFilter[] {
				new InputFilter.AllCaps(), new InputFilter.LengthFilter(30),
				CommonUtils.alphaFilter });
		sValueCallerName.addTextChangedListener(new EditedText());

		mLabelCallerPhone = (TextView) view
				.findViewById(R.id.lbl_callerPhoneNo);
		sValueCallerPhone = (EditText) view
				.findViewById(R.id.value_callerPhoneNo);
		// length of primary odo length should be less then 12
		sValueCallerPhone.setFilters(new InputFilter[] {
				new InputFilter.LengthFilter(25), numericFilter });
		sValueCallerPhone.addTextChangedListener(new EditedText());
		if (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW) {
			sValPrimaryODO.setText(JobBundle.getmVehicleODO());
			sValSecondaryODO.setText(JobBundle.getmHudometer());
			sValueCallerName.setText(JobBundle.getCallerName());
			sValueCallerPhone.setText(JobBundle.getCallerPhone());
			sValueDriverName.setText(JobBundle.getmDriverName());
			sValueDefectNo.setText(JobBundle.getmDefectAuthNumber());
			valueCallCenterRefNo.setText(JobBundle.getmActionLineN());
			sValueRefNumber.setText(JobBundle.getmRefNO());
			sValueVehId.setText(JobBundle.getmLPlate());
			sValueAccount.setText(JobBundle.getmAccountName());
			sValueFleetNumber.setText(JobBundle.getmFleetNO());

			String traceMsg="";
			try
			{
				if(JobBundle.getmRefNO()!=null)
				{
					traceMsg = traceMsg + " " +JobBundle.getmRefNO()+" ";
				}
				else
				{
					traceMsg = traceMsg + " ";
				}

				if(JobBundle.getmAccountName()!=null)
				{
					traceMsg = traceMsg + "| " +JobBundle.getmAccountName()+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(JobBundle.getmLPlate()!=null)
				{
					traceMsg = traceMsg + "| " +JobBundle.getmLPlate()+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(JobBundle.getmFleetNO()!=null)
				{
					traceMsg = traceMsg + "| " +JobBundle.getmFleetNO()+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(JobBundle.getmVehicleODO()!=null)
				{
					traceMsg = traceMsg + "| " +JobBundle.getmVehicleODO()+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(JobBundle.getmHudometer()!=null)
				{
					traceMsg = traceMsg + "| " +JobBundle.getmHudometer()+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(JobBundle.getmActionLineN()!=null)
				{
					traceMsg = traceMsg + "| " +JobBundle.getmActionLineN()+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(JobBundle.getmDriverName()!=null)
				{
					traceMsg = traceMsg + "| " +JobBundle.getmDriverName()+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(JobBundle.getmDefectAuthNumber()!=null)
				{
					traceMsg = traceMsg + "| " +JobBundle.getmDefectAuthNumber()+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				if(JobBundle.getmActionLineN()!=null)
				{
					traceMsg = traceMsg + "| " +JobBundle.getCallerName()+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}


				if(JobBundle.getmActionLineN()!=null)
				{
					traceMsg = traceMsg + "| " +JobBundle.getCallerPhone()+" ";
				}
				else
				{
					traceMsg = traceMsg + "| ";
				}

				LogUtil.TraceInfo(TRACE_TAG,"Initial Job Info Details : ",traceMsg,true,true,true);
			}catch(Exception e)
			{
				LogUtil.TraceInfo(TRACE_TAG,"Initial Job Info Details : Exception ",e.getMessage(),true,true,true);
			}
		} else {
			if (!TextUtils.isEmpty(Job.getVehicleODO())) {
				sValPrimaryODO.setText(Job.getVehicleODO());
			}
			if (!TextUtils.isEmpty(Job.getDriverName())) {
				sValPrimaryODO.setText(Job.getDriverName());
			}
		}
		mLabelTechID = (TextView) view.findViewById(R.id.lbl_technician);
		sValueTechid = (TextView) view.findViewById(R.id.value_technician);
		// job icon on header
		mJobTypeIcon = (ImageView) view.findViewById(R.id.jobIcon);
		if (!Constants.JOBTYPEREGULAR) {
			mJobTypeIcon.setImageResource(R.drawable.breakdown_job);
		}
		sValPrimaryODO.requestFocus();
		sValPrimaryODO.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				boolean handled = false;
				if (actionId == EditorInfo.IME_ACTION_NEXT) {
					sValSecondaryODO.requestFocus();
					handled = true;
				}
				return handled;
			}
		});

		sValSecondaryODO
				.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						boolean handled = false;
						if (actionId == EditorInfo.IME_ACTION_NEXT) {
							valueCallCenterRefNo.requestFocus();
							handled = true;
						}
						return handled;
					}
				});

		valueCallCenterRefNo
				.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						boolean handled = false;
						if (actionId == EditorInfo.IME_ACTION_NEXT) {
							sValueDriverName.requestFocus();
							handled = true;
						}
						return handled;
					}
				});

		sValueDriverName
				.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						boolean handled = false;
						if (actionId == EditorInfo.IME_ACTION_NEXT) {
							sValueDefectNo.requestFocus();
							handled = true;
						}
						return handled;
					}
				});

		sValueDefectNo.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				boolean handled = false;
				if (actionId == EditorInfo.IME_ACTION_NEXT) {
					sValueCallerName.requestFocus();
					handled = true;
				}
				return handled;
			}
		});

		sValueCallerName
				.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						boolean handled = false;
						if (actionId == EditorInfo.IME_ACTION_NEXT) {
							sValueCallerPhone.requestFocus();
							handled = true;
						}
						return handled;
					}
				});
	}

	private static class EditedText implements TextWatcher {
		@Override
		public void afterTextChanged(Editable arg0) {
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
		}

		@Override
		public void onTextChanged(CharSequence text, int arg1, int arg2,
				int arg3) {
		}
	}

	private class PrimaryOdometerTextChanged implements TextWatcher {
		@Override
		public void afterTextChanged(Editable arg0) {

			//CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
			//Comparing the Last recorded Odometer value with User newly Entered Odometer value.
			//IsOdometerValueIsCorrect=1 means Wrong Odometer Reading
			//IsOdometerValueIsCorrect=0 means Correct or without reading
			if (!TextUtils.isEmpty(sValPrimaryODO.getText().toString().trim())) {
				if(Constants.LastRecordedOdometerValue>Long.parseLong(sValPrimaryODO.getText().toString().trim())) {
					Constants.IsOdometerValueIsCorrect = 1;
					EJobPagerAdapter.count = 1;
					if (EjobFormActionActivity.sViewAdapter != null) {
						EjobFormActionActivity.sViewAdapter.notifyDataSetChanged();
					}

				}
				else
				{
					Constants.IsOdometerValueIsCorrect = 0;
				}
			}
			else
			{
				Constants.IsOdometerValueIsCorrect = 0;
			}
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
									  int arg3) {
		}

		@Override
		public void onTextChanged(CharSequence text, int arg1, int arg2,
								  int arg3) {
			//Fix - Bug : 702 (restrict users from entering 0 (zero) as a tacho calue)
			if (!TextUtils.isEmpty(sValPrimaryODO.getText().toString().trim())) {
				if(sValPrimaryODO.getText().toString().trim().matches(Regex_For_Zero_Entry))
				{
					sValPrimaryODO.setText("");
				}
			}
		}
	}

	// input filter validations
	private static InputFilter alphaNumericFilter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence changedText, int arg1,
				int arg2, Spanned oldText, int arg4, int arg5) {
			for (int k = arg1; k < arg2; k++) {
				if (!Character.isLetterOrDigit(changedText.charAt(k))
						&& !Character.isSpaceChar(changedText.charAt(k))) {
					return "";
				}
			}
			return null;
		}
	};

	private static InputFilter numericFilter = new InputFilter() {
		Pattern mPattern;

		@Override
		public CharSequence filter(CharSequence changedText, int arg1,
				int arg2, Spanned oldText, int arg4, int arg5) {
			mPattern = Pattern.compile("^[\\p{N}]+$|^$");
			Matcher matcher = mPattern
					.matcher(oldText.toString() + changedText);
			if (!matcher.matches()) {
				return "";
			}
			return null;
		}
	};
	/**
	 * Method enabling the save icon as per the user selection on the UI
	 * It will always turn the save icon color into red if there will be data to be saved 
	 * for the local database file
	 */
	public static void enableSaveButton() {
		if (sSaveButton != null) {
			sSaveButton.setEnabled(true);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_RED);
			sSaveButton
					.setBackgroundResource(R.drawable.continue_savejob_navigation);
		}
	}
	/**
	 * Method disabling the save icon as per the user selection on the UI
	 * It will always turn the save icon color into grey if there will not be any 
	 * data to be saved for the local database file
	 */
	public static void disableSaveButton() {
		if (sSaveButton != null) {
			sSaveButton.setEnabled(false);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_GRAY);
			sSaveButton.setBackgroundResource(R.drawable.unsavejob_navigation);
		}
	}
	/**
	 * This method will always turn the save icon color into black 
	 * if there will not be any data to be saved for the local database file
	 * It will navigate user back to the eJoblist
	 */
	public static void finalSaveButton() {
		if (sSaveButton != null) {
			sSaveButton.setEnabled(true);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_BLACK);
			sSaveButton.setBackgroundResource(R.drawable.savejob_navigation);
		}
	}

	public static void updateJobObject() {
		sRefNoIncrement = Constants.JOBREFNUMBERVALUE;
		Job.setJobRefNumber(String.valueOf(sRefNoIncrement));
		Job.setLicenseNumber(Constants.FLEETNUM.toString());
		Job.setSapContractNumber(Constants.SAPCONTRACTNUMBER);
		Job.setExternalID(String.valueOf(UUID.randomUUID()));
		Job.setVehicleID(Constants.VEHID.toString());
		Job.setLicenseNumber(Constants.FLEETNUM.toString());
		Job.setCustomerName(Constants.ACCOUNTNAME.toString());
	}

	private void retainJob() {
		if (sValueCallerPhone != null) {
			Job.setCallerPhone(sValueCallerPhone.getText().toString().trim());
		}

		if (sValueCallerName != null) {
			Job.setCallerName(sValueCallerName.getText().toString().trim());
		}

		if (sValueDefectNo != null) {
			Job.setDefectAuthNumber(sValueDefectNo.getText().toString().trim());
		}

		if (valueCallCenterRefNo != null) {
			Job.setActionLineN(valueCallCenterRefNo.getText().toString().trim());
		}

		if (sValSecondaryODO != null) {
			Job.setHudometer(sValSecondaryODO.getText().toString().trim());
		}

		if (sValPrimaryODO != null) {
			Job.setVehicleODO(sValPrimaryODO.getText().toString().trim());
		}

		if (sValueDriverName != null) {
			Job.setDriverName(sValueDriverName.getText().toString().trim());
		}
	}

	private void retrieveRetainedJob() {
		if (!TextUtils.isEmpty(Job.getVehicleODO()) && sValPrimaryODO != null) {
			sValPrimaryODO.setText(Job.getVehicleODO());
		}
		if (!TextUtils.isEmpty(Job.getDriverName()) && sValueDriverName != null) {
			sValueDriverName.setText(Job.getDriverName());
		}
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (!isVisibleToUser) {
			mCountScreenNotVisibleToUser++;
			if (mCountScreenNotVisibleToUser > 1) {
				retainJob();
				mCountScreenNotVisibleToUser = 0;
			}
		} else {
			retrieveRetainedJob();
			mCountScreenNotVisibleToUser = 1;
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(!Constants.IS_FORM_ELEMENTS_CLEANED){
			retainJob();
		}
	}
	protected void hideKeyboard(Context context) {
		try {
		InputMethodManager in = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(getView().getWindowToken(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#onActivityBackPress()
	 */
	@Override
	public boolean onActivityBackPress() {
		return false;
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#hideKeyBoard(boolean)
	 */
	@Override
	public void hideKeyBoard(boolean toHide) {
		if(toHide){
			hideKeyboard(this.getActivity());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.IEjobFragmentVisible#fragmentIsVisibleToUser
	 * ()
	 */
	@Override
	public void fragmentIsVisibleToUser() {
		updateSaveButtonOnUIBasedOnLastFragment();
	}

	/**
	 * This method is called from view pager listener when this fragment is
	 * selected. It updates the save icon based on the status of save button in
	 * last fragment.
	 */
	private void updateSaveButtonOnUIBasedOnLastFragment() {
		// TODO Auto-generated method stub
		if (saveIconHandlerActivity == null)
			return;
		int saveIconState = saveIconHandlerActivity.getSaveIconState();
		switch (saveIconState) {
		case EjobSaveIconStates.ICON_GRAY:
			disableSaveButton();
			break;
		case EjobSaveIconStates.ICON_BLACK:
			finalSaveButton();
			break;
		case EjobSaveIconStates.ICON_RED:
			enableSaveButton();
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onAttach(android.app.Activity)
	 */
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		saveIconHandlerActivity = (ISaveIconEjobFragment) activity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDetach()
	 */
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		saveIconHandlerActivity = null;
	}
}
