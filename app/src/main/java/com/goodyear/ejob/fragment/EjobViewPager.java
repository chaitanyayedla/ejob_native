package com.goodyear.ejob.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

/**
 * Custom View Pager to get the current displayed fragment.
 * 
 * @author Nilesh.Pawate
 * 
 */
public class EjobViewPager extends ViewPager {

	public EjobViewPager(Context context) {
		super(context);
	}

	public EjobViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * API used to get the current active fragment from view pager.
	 * 
	 * @param fragmentManager
	 *            FragmentManager
	 * @param position
	 *            current displayed position.
	 * @return Fragment, Current displayed fragment.
	 */
	public Fragment getActiveFragment(FragmentManager fragmentManager,
			int position) {
		final String name = makeFragmentName(getId(), position);
		final Fragment fragmentByTag = fragmentManager.findFragmentByTag(name);
		return fragmentByTag;
	}

	private static String makeFragmentName(int viewId, int index) {
		return "android:switcher:" + viewId + ":" + index;
	}
}