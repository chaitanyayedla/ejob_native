package com.goodyear.ejob.fragment;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Locale;
import java.util.UUID;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.interfaces.IFragmentCommunicate;
import com.goodyear.ejob.R;
import com.goodyear.ejob.authenticator.Authentication;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.interfaces.IEjobFragmentVisible;
import com.goodyear.ejob.interfaces.ISaveIconEjobFragment;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.EjobSaveIconStates;
import com.goodyear.ejob.util.JobBundle;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.ServicesEditHelper;

/**
 * @author Good Year
 * 
 */
public class EjobAdditionalServicesFragment extends Fragment implements
		IFragmentCommunicate, IEjobFragmentVisible {

	/** for Additional Service action type is always 5 */
	private static int sActionType = 5;
	/** value of sSapCode */
	private static int sSapCode = 0;
	/** Array of switch buttons for each additional services */
	public static ArrayList<Switch> sSwitchBtns = new ArrayList<Switch>();
	/** Array of service names for each additional services */
	public static ArrayList<TextView> sServiceValues = new ArrayList<TextView>();
	/** value of purchasing organization based on userID and vendorSAPCode */
	private String mPurchasingOrg = "";
	/** value of fixed quantity to be used for each additional service */
	private int mFixedQuantity = 0;
	/** value of service count to be used for each additional service */
	private int mServiceCount = 0;
	/** value for service Id to be used for each additional service */
	private int mServiceID = 0;
	/** value of axle number to be used for each additional service */
	private int mAxleNumber = 0;
	/** database object for DB operation */
	private DatabaseAdapter mDbHelper;
	/** Array of service labels switch buttons for each additional services */
	private ArrayList<TextView> sSserviceLabels = new ArrayList<TextView>();
	/** Get the status of switch button for each additional service */
	private boolean isSwichBtnChecked = false;
	/** Description of the additional service */
	private String mService = "";
	/** quantity of the additional service */
	private String mServiceQuantity = "1";
	/** Array List having sap codes the additional service */
	private ArrayList<Integer> mSapCodeList;
	/** If the job is regular/break down 0=Regular Job and 1=BreakDownJob */
	private int isBreakeDownOnly = 0;
	/** Stores the count of service */
	private int mServiceCounter = 0;
	/** Context of the current class */
	public Context mContext;
	/** Root view of current fragment */
	private View mRootView;
	/** Counts if the screen is loaded for 1st time */
	private int mScreenLoadNum = 0;
	/** flag to be set when save button is clicked */
	private boolean mSaveButtonClicked = false;
	/** Dynamic layout having all additional services */
	private LinearLayout mServiceContainer;

	private LinearLayout mFavServiceContainer;

	/** Layout to be generated for each additional services */
	private LinearLayout mServiceViewLL;
	/** Image of job icon based on Regular/Break down */
	private ImageView mJobIcon;
	/** Image buttons to be generated for each additional service to increment */
	private ImageView mupBtn;
	/** Image buttons to be generated for each additional service to decrement */
	private ImageView mDownBtn;
	/** Save button */
	private Button mSaveJob;
	/** Label for user name/account */
	private TextView mLabelAccount;
	/** Text showing user name/account */
	private TextView mValueAccount;
	/** Label showing selected vehicle Id for which current job is being created */
	private TextView mLabelVehId;
	/** Text showing selected vehicle Id for which current job is being created */
	private TextView mValueVehId;
	/** Text Showing Fleet number */
	private TextView mValueFleetNumber;
	/** Label for reference number */
	private TextView mLabelRefNumber;
	/** Text showing reference number */
	private TextView mValueRefNumber;
	/** TexView showing {@link EjobAdditionalServicesFragment#mFixedQuantity} */
	private TextView mServiceValue;
	private TextView mTitleTextView;
	Bundle savedInstanceState;
	private String yesLabel;
	private String noLabel;
	private TextView mLabelFleetNumber;
	private Hashtable<String, View> originalServicetable;
	private Hashtable<String, View> favouriteServicetable;
	private boolean mMovingFromAdditionalServiceFragment;
	private boolean additionalServiceLoaded;
	private static final String ORIGNAL_SERVICE_TAG = "orignal_service";
	private static final String FAVOURITE_SERVICE_TAG = "favourite_service";
	private static final int SWITCH_UPDATE_WIDGET_ID = 1;
	private static final int DOWN_BUTTON_UPDATE_WIDGET_ID = 2;
	private static final int UP_BUTTON_UPDATE_WIDGET_ID = 3;
	private static final int SERVICE_VALUE_UPDATE_WIDGET_ID = 4;
	private static final int FAVOURITE_STATUS_DB = 1;
	private static final int UNFAVOURITE_STATUS_DB = 0;
	private static ISaveIconEjobFragment saveIconHandlerActivity;
	private LinearLayout favouriteListSeperator;
	private static final String FAVOURITE_SEPARATOR_KEY = "favourite_key";
	private static final String TAG = "AdditionalService";

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mContext = activity;
		mSaveButtonClicked = true;
		saveIconHandlerActivity = (ISaveIconEjobFragment) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.additional_services, container,
				false);
		this.savedInstanceState = savedInstanceState;
		initalize();
		if (Constants.JOB_ITEMLIST != null && Constants.JOB_ITEMLIST.size() > 0) {
			VehicleSkeletonFragment.mJobItemList = Constants.JOB_ITEMLIST;
		}
		loadLabelsFromDB();
		if (sSwitchBtns != null) {
			sSwitchBtns.clear();
		}
		if (sSserviceLabels != null) {
			sSserviceLabels.clear();
		}
		if (sServiceValues != null) {
			sServiceValues.clear();
		}
		loadAdditionalServices(inflater);
		mSaveButtonClicked = true;
		return mRootView;
	}

	/**
	 * Retaining The Services On Page Swipe
	 */
	public void getRetainedService() {
		int count = 0;
		additionalServiceLoaded = false;
		if (VehicleSkeletonFragment.mJobItemList != null
				&& VehicleSkeletonFragment.mJobItemList.size() > 0) {
			for (int i = 0; i < VehicleSkeletonFragment.mJobItemList.size(); i++) {
				JobItem jobItem = VehicleSkeletonFragment.mJobItemList.get(i);
				if (jobItem != null) {
					int actionTypeFromList = -1;
					try {
						actionTypeFromList = Integer.valueOf(jobItem
								.getActionType());
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
					if (actionTypeFromList == sActionType
							&& jobItem.getDeleteStatus() != 1) {
						String sap_code = jobItem.getServiceID();
						String service_counter = jobItem.getServiceCount();
						int sap_code_parse = -1;
						try {
							sap_code_parse = Integer.valueOf(sap_code);
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}

						if (sap_code_parse != -1 && mSapCodeList != null
								&& mSapCodeList.contains(sap_code_parse)) {
							count = mSapCodeList.indexOf(sap_code_parse);
							if (sSwitchBtns != null
									&& sSwitchBtns.size() > count) {
								sSwitchBtns.get(count).setChecked(true);
							}
							if (sServiceValues != null
									&& sServiceValues.size() > count) {
								sServiceValues.get(count).setText(
										service_counter);
							}
							// CR 444
							// update favourite service also if exists
							updateServiceValueAcrossFavouriteAndOriginalService(
									sSwitchBtns.get(count), service_counter,
									favouriteServicetable);
						}
					}
				}
			}
		}
		additionalServiceLoaded = true;
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			mMovingFromAdditionalServiceFragment = true;
			mSaveButtonClicked = false;
			getRetainedService();
			if (Constants.COMESFROMUPDATE) {
				if (!Constants.UPDATE_APPLIED_ON_EDIT) {
					if (Constants.servicesEdit != null
							&& Constants.servicesEdit.size() > 0) {
						for (ServicesEditHelper service : Constants.servicesEdit) {
							try {
								int pos = mSapCodeList.indexOf(Integer
										.parseInt(service.getSapCode()));
								updateServices(pos,
										Integer.parseInt(service.getCount()));
							} catch (NumberFormatException e) {
								e.printStackTrace();
							}
						}
					}
					Constants.UPDATE_APPLIED_ON_EDIT = true;
				}
			}
			CommonUtils.hideKeyboard(mContext, getView().getWindowToken());
		} else {
			if (mMovingFromAdditionalServiceFragment && mScreenLoadNum > 0) {
				mMovingFromAdditionalServiceFragment = false;
				saveAdditionalServices();
			}
			mScreenLoadNum++;
		}
	}

	/**
	 * update the services
	 * 
	 * @param position
	 * @param count
	 */
	private void updateServices(int position, int count) {
		if (sSwitchBtns != null && sSwitchBtns.size() > position) {
			sSwitchBtns.get(position).setChecked(true);
		}
		if (sServiceValues != null && sServiceValues.size() > position) {
			sServiceValues.get(position).setText(String.valueOf(count));
		}
	}

	/**
	 * Get Vendor SAP ID
	 * 
	 * @param vendorName
	 * @return String
	 */
	public String getVendorSAPID(String vendorName) {
		String sapVendorID = null;
		Cursor mCursor = null;
		try {
			mCursor = mDbHelper.getvendorIDByName(vendorName);
			if (CursorUtils.isValidCursor(mCursor)) {
				sapVendorID = mCursor.getString(mCursor
						.getColumnIndex("SAPVendorCodeID"));
			}
			CursorUtils.closeCursor(mCursor);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
		return sapVendorID;
	}

	/**
	 * Loading The Additional Services
	 * 
	 * @param mRootView
	 * @param inflater
	 */
	private void loadAdditionalServices(LayoutInflater inflater) {
		// CR 444
		originalServicetable = new Hashtable<>();
		favouriteServicetable = new Hashtable<>();
		int favourite = 0;
		mDbHelper = new DatabaseAdapter(mContext);
		mDbHelper.createDatabase();
		mDbHelper.open();
		String yesLabel = mDbHelper.getLabel(Constants.YES_LABEL);
		String noLabel = mDbHelper.getLabel(Constants.NO_LABEL);
		Cursor curAddlService = null;
		try {
			String sapVendorID = "";
			if (Constants.SAP_VENDORCODE_ID == null) {
				sapVendorID = getVendorSAPID(Constants.VENDER_NAME);
			} else if (Constants.SAP_VENDORCODE_ID != null
					&& Constants.SAP_VENDORCODE_ID.equals("-1")) {
				sapVendorID = getVendorSAPID(Constants.VENDER_NAME);
			}
			if (!TextUtils.isEmpty(sapVendorID)) {
				Constants.SAP_VENDORCODE_ID = sapVendorID;
			}
			mPurchasingOrg = mDbHelper.getPurchasingOrganization(
					Authentication.getUserName(Constants.USER),
					Constants.SAP_VENDORCODE_ID);
			curAddlService = mDbHelper.getAdditionalServices(mPurchasingOrg,
					isBreakeDownOnly);
			if (CursorUtils.isValidCursor(curAddlService)
					&& curAddlService.moveToFirst()) {
				do {
					sSapCode = curAddlService.getInt(curAddlService
							.getColumnIndex("SAPCode"));
					mService = curAddlService.getString(curAddlService
							.getColumnIndex("Description"));
					mFixedQuantity = curAddlService.getInt(curAddlService
							.getColumnIndex("FixedQuantity"));
					// CR 444: get favourite status for additional services from
					// db
					favourite = curAddlService.getInt(curAddlService
							.getColumnIndex("fav"));
					// inflate each service row and add tag of original list
					View mServiceViewLL = inflater.inflate(
							R.layout.additional_services_row, null);
					TextView serviceLabel = (TextView) mServiceViewLL
							.findViewById(R.id.lbl_service);
					ImageView mFavButton = (ImageView) mServiceViewLL
							.findViewById(R.id.btn_fav);

					final TextView mServiceValue = (TextView) mServiceViewLL
							.findViewById(R.id.value_service);
					final Switch switchBtn = (Switch) mServiceViewLL
							.findViewById(R.id.value_serviceSwitchBtn);
					switchBtn.setTextOn(yesLabel);
					switchBtn.setTextOff(noLabel);
					switchBtn
							.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
								@Override
								public void onCheckedChanged(
										CompoundButton buttonView,
										boolean isChecked) {
									updateFavouriteList(buttonView, isChecked,
											SWITCH_UPDATE_WIDGET_ID, null);
									if (!isChecked) {
										mServiceValue.setText(mServiceQuantity);
									}
									enableSaveButton();
								}
							});
					mServiceValue.setText(mServiceQuantity);
					serviceLabel.setText(mService);
					// CR 444
					// if not fav then set to favorite listener
					// else set unfavorite listener
					if (mFavButton != null && favourite == 0) {
						mFavButton
								.setOnClickListener(new FavouriteButtonListner());
					} else if (mFavButton != null && favourite == 1) {
						mFavButton
								.setOnClickListener(new UnFavouriteButtonListner());
						mFavButton
								.setBackgroundResource(R.drawable.favourite_selected);
					}
					mDownBtn = (ImageView) mServiceViewLL
							.findViewById(R.id.service_downBtn);
					mupBtn = (ImageView) mServiceViewLL
							.findViewById(R.id.service_upBtn);
					mDownBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View bntDown) {
							serviceDecremental(bntDown);
							enableSaveButton();
						}
					});
					mupBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View btnUp) {
							serviceIncremental(btnUp);
							enableSaveButton();
						}
					});
					if (mFixedQuantity == 1) {
						mDownBtn.setVisibility(View.INVISIBLE);
						mServiceValue.setVisibility(View.INVISIBLE);
						mupBtn.setVisibility(View.INVISIBLE);
					}
					mSapCodeList.add(sSapCode);

					// CR : 444
					// if this service is favourite then add it to both the list
					if (favourite != 0) {
						View favouriteRow = infalteService(mService,
								mServiceQuantity, mFixedQuantity,
								switchBtn.isChecked());
						favouriteRow.setTag(FAVOURITE_SERVICE_TAG);
						mFavServiceContainer.addView(favouriteRow);
						favouriteServicetable.put(mService, favouriteRow);
						showFavouriteSeperator();
					}
					// adding service to original list with original service tag
					mServiceContainer.addView(mServiceViewLL);
					mServiceViewLL.setTag(ORIGNAL_SERVICE_TAG);
					originalServicetable.put(mService, mServiceViewLL);
					sSwitchBtns.add(switchBtn);
					sSserviceLabels.add(serviceLabel);
					sServiceValues.add(mServiceValue);
				} while (curAddlService.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(curAddlService);
			if (mDbHelper != null) {
				mDbHelper.close();
			}
		}
	}

	/**
	 * Loading The Labels From Translation Table
	 * 
	 * @param rootView
	 */
	private void loadLabelsFromDB() {
		mDbHelper = new DatabaseAdapter(mContext);
		mDbHelper.createDatabase();
		mDbHelper.open();
		try {
			mTitleTextView.setText(mDbHelper
					.getLabel(Constants.LABEL_ADDITIONAL_SERVICES));
			String labelFromDB = mDbHelper.getLabel(Constants.ACCOUNT_LABEL);
			mLabelAccount.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.VEH_ID_LABEL);
			mLabelVehId.setText(labelFromDB);
			yesLabel = mDbHelper.getLabel(Constants.YES_LABEL);
			noLabel = mDbHelper.getLabel(Constants.NO_LABEL);
			labelFromDB = mDbHelper.getLabel(Constants.FLEET_NO);
			mLabelFleetNumber.setText(labelFromDB);
			if (!Constants.COMESFROMUPDATE && !Constants.COMESFROMVIEW) {
				mValueAccount.setText(Constants.ACCOUNTNAME);
				mValueVehId.setText(Constants.VEHID);
				mValueFleetNumber.setText(Constants.FLEETNUM);
				mLabelRefNumber.setText("#");
				mValueRefNumber.setText(Constants.JOBREFNUMBER);
			} else if (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW) {
				mValueRefNumber.setText(JobBundle.getmRefNO().toString());
				mValueVehId.setText(JobBundle.getmLPlate().toString());
				mValueAccount.setText(JobBundle.getmAccountName());
				mValueFleetNumber.setText(JobBundle.getmFleetNO());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mDbHelper != null) {
				mDbHelper.close();
			}
		}
	}

	/**
	 * Initializing The UI Labels
	 * 
	 * @param view
	 */
	private void initalize() {

		mTitleTextView = (TextView) mRootView.findViewById(R.id.textView1);
		mLabelAccount = (TextView) mRootView.findViewById(R.id.lbl_account);
		mValueAccount = (TextView) mRootView.findViewById(R.id.value_account);
		mLabelVehId = (TextView) mRootView.findViewById(R.id.lbl_vehId);
		mValueVehId = (TextView) mRootView.findViewById(R.id.value_vehId);
		mLabelFleetNumber = (TextView) mRootView.findViewById(R.id.lbl_fleetNo);
		mValueFleetNumber = (TextView) mRootView
				.findViewById(R.id.value_fleetNo);
		mLabelRefNumber = (TextView) mRootView.findViewById(R.id.lbl_jobRefNo);
		mValueRefNumber = (TextView) mRootView
				.findViewById(R.id.value_jobRefNo);
		// CR 444 : initialize service containers
		mServiceContainer = (LinearLayout) mRootView
				.findViewById(R.id.serviceContainer);
		mFavServiceContainer = (LinearLayout) mRootView
				.findViewById(R.id.fav_serviceContainer);
		favouriteListSeperator = (LinearLayout) mRootView
				.findViewById(R.id.linear_favourite_seperator);
		mSapCodeList = new ArrayList<Integer>();
		mJobIcon = (ImageView) mRootView.findViewById(R.id.jobIcon);
		if (!Constants.JOBTYPEREGULAR) {
			mJobIcon.setImageResource(R.drawable.breakdown_job);
			isBreakeDownOnly = 1;
		} else {
			isBreakeDownOnly = 0;
		}
		mSaveJob = (Button) mRootView.findViewById(R.id.btn_save);
		mSaveJob.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				saveAdditionalServices();
				/**
				 * Condition added to disable save button once clicked during Save Job in DB
				 */
				if (saveIconHandlerActivity.getSaveIconState() == EjobSaveIconStates.ICON_BLACK) {
					disableSaveButton();
					EjobFormActionActivity.saveJobInfo(v, true);
				} else if (saveIconHandlerActivity.getSaveIconState() == EjobSaveIconStates.ICON_RED) {
					disableSaveButton();
					EjobFormActionActivity.saveJobInfo(v, false);
					finalSaveButton();
				}
			}
		});
	}

	/**
	 * Additional Services Service Quantity Incremental
	 * 
	 * @param view
	 */
	public void serviceIncremental(View view) {
		mServiceViewLL = (LinearLayout) view.getParent();
		mServiceValue = (TextView) mServiceViewLL
				.findViewById(R.id.value_service);
		Switch btn_switch = (Switch) mServiceViewLL
				.findViewById(R.id.value_serviceSwitchBtn);

		String mServiceValueStr = mServiceValue.getText().toString();
		if (!TextUtils.isEmpty(mServiceValueStr)) {
			try {
				mServiceCounter = Integer.parseInt(mServiceValueStr);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		if (btn_switch.isChecked()) {
			if (mServiceCounter < 99) {
				mServiceCounter = mServiceCounter + 1;
			}
			mServiceValue.setText("" + mServiceCounter);
		}
		// CR 444: update change of value in favourite list
		updateFavouriteList(view, false, SERVICE_VALUE_UPDATE_WIDGET_ID,
				String.valueOf(mServiceCounter));
	}

	/**
	 * Additional Services Service Quantity Decremental
	 * 
	 * @param view
	 */
	public void serviceDecremental(View view) {
		mServiceViewLL = (LinearLayout) view.getParent();
		mServiceValue = (TextView) mServiceViewLL
				.findViewById(R.id.value_service);
		Switch btn_switch = (Switch) mServiceViewLL
				.findViewById(R.id.value_serviceSwitchBtn);

		String mServiceValueStr = mServiceValue.getText().toString();
		if (!TextUtils.isEmpty(mServiceValueStr)) {
			try {
				mServiceCounter = Integer.parseInt(mServiceValueStr);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		if (btn_switch.isChecked()) {
			if (mServiceCounter > 1) {
				mServiceCounter = mServiceCounter - 1;
			}
			mServiceValue.setText(String.valueOf(mServiceCounter));
		}
		// CR 444: update change of value in favourite list
		updateFavouriteList(view, false, SERVICE_VALUE_UPDATE_WIDGET_ID,
				String.valueOf(mServiceCounter));
	}

	/**
	 * Saving The Additional Services Details
	 */
	public boolean saveAdditionalServices() {
		boolean isAnyValueSaved = false;
		LogUtil.i("INSIDE", "saveAdditionalServices  ADDL :: "
				+ mServiceContainer);
		if (mServiceContainer != null) {
			try {
				if (VehicleSkeletonFragment.mJobItemList != null) {
					int noOfLL = mServiceContainer.getChildCount();
					for (int i = 0; i < noOfLL; i++) {
						if (sSserviceLabels != null
								&& sSserviceLabels.size() > i) {
							mService = sSserviceLabels.get(i).getText()
									.toString();
						}
						mServiceCounter = Integer.parseInt(sServiceValues
								.get(i).getText().toString());
						if (sSwitchBtns != null && sSwitchBtns.size() > i) {
							isSwichBtnChecked = sSwitchBtns.get(i).isChecked();
						}

						if (mSapCodeList != null && mSapCodeList.size() > 1) {
							sSapCode = mSapCodeList.get(i);
						}

						mServiceCount = mServiceCounter;
						mServiceID = sSapCode;

						if (isSwichBtnChecked) {
							isAnyValueSaved = true;
							// Saving The Additional Services
							if (!checkAdditionalJOb(mServiceID)) {
								JobItem jobItem = new JobItem();
								jobItem.setActionType(String
										.valueOf(sActionType));
								jobItem.setAxleNumber(String
										.valueOf(mAxleNumber));
								jobItem.setAxleServiceType("0");
								jobItem.setCasingRouteCode("");
								jobItem.setDamageId("");
								jobItem.setExternalId(String.valueOf(UUID
										.randomUUID()));
								jobItem.setGrooveNumber(null);
								jobItem.setJobId("");
								jobItem.setMinNSK("0");
								jobItem.setNote("");
								jobItem.setNskOneAfter("0");
								jobItem.setNskOneBefore("0");
								jobItem.setNskThreeAfter("0");
								jobItem.setNskThreeBefore("0");
								jobItem.setNskTwoAfter("0");
								jobItem.setNskTwoBefore("0");
								jobItem.setOperationID("");
								jobItem.setPressure("0");
								// recommended pressure for axle
								jobItem.setRecInflactionDestination("0");
								// pressure from JSON
								jobItem.setRecInflactionOrignal("0");
								jobItem.setRegrooved("false"); // true false
								jobItem.setReGrooveNumber(null);// alway null
								jobItem.setRegrooveType("0");
								jobItem.setRemovalReasonId("");
								jobItem.setRepairCompany(null);
								jobItem.setRimType("0"); // for tor
								jobItem.setSapCodeTilD("0");// Additional
															// Services
								jobItem.setSequence(String
										.valueOf(VehicleSkeletonFragment.mJobItemList
												.size() + 1));
								jobItem.setServiceCount(String
										.valueOf(mServiceCount));
								jobItem.setServiceID(String.valueOf(mServiceID));
								jobItem.setSwapType("0");// swap
								jobItem.setThreaddepth("0");// tire mgmt
								jobItem.setTorqueSettings("0"); // update from
																// retorque
								jobItem.setTyreID("");
								jobItem.setWorkOrderNumber(null);
								jobItem.setDeleteStatus(-1);
								jobItem.setNote(null);
								VehicleSkeletonFragment.mJobItemList
										.add(jobItem);
							} else {
								updateAdditionalJOb(mServiceID, mServiceCount);
							}
						} else {
							isSwichBtnChecked = false;
							if (checkAdditionalJOb(mServiceID)) {
								removeAdditionalJOb(mServiceID);
							}
						}
					}
					Constants.JOB_ITEMLIST = VehicleSkeletonFragment.mJobItemList;
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (mDbHelper != null) {
					mDbHelper.close();
				}
			}
		}
		return isAnyValueSaved;
	}

	/**
	 * Checking The Additional Services Existance
	 * 
	 * @param serviceID
	 * @return boolean
	 */
	public static boolean checkAdditionalJOb(int serviceID) {
		boolean mcheckAdditionalJob = false;
		if (VehicleSkeletonFragment.mJobItemList != null
				&& VehicleSkeletonFragment.mJobItemList.size() > 0) {
			for (int i = 0; i < VehicleSkeletonFragment.mJobItemList.size(); i++) {
				JobItem jobItem = VehicleSkeletonFragment.mJobItemList.get(i);
				if (jobItem != null) {
					String service_ID = jobItem.getServiceID();
					if (!TextUtils.isEmpty(service_ID)) {
						try {
							if (serviceID == Integer.parseInt(service_ID)
									&& 5 == Integer.parseInt(jobItem
											.getActionType())) {
								mcheckAdditionalJob = true;
							}
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		return mcheckAdditionalJob;
	}

	/**
	 * Updating the existing additional services
	 * 
	 * @param serviceID
	 * @param serviceCountVal
	 */
	public static void updateAdditionalJOb(int serviceID, int serviceCountVal) {
		if (VehicleSkeletonFragment.mJobItemList != null) {
			for (int i = 0; i < VehicleSkeletonFragment.mJobItemList.size(); i++) {
				JobItem jobItem = VehicleSkeletonFragment.mJobItemList.get(i);
				if (jobItem != null) {
					String service_ID = jobItem.getServiceID();
					if (!TextUtils.isEmpty(service_ID)) {
						try {
							if (serviceID == Integer.parseInt(service_ID)
									&& 5 == Integer.parseInt(jobItem
											.getActionType())) {
								jobItem.setServiceCount(String
										.valueOf(serviceCountVal));
								jobItem.setDeleteStatus(-1);
							}
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/**
	 * Updating the existing additional services
	 * 
	 * @param serviceID
	 * 
	 */
	public static void removeAdditionalJOb(int serviceID) {
		if (VehicleSkeletonFragment.mJobItemList != null
				&& VehicleSkeletonFragment.mJobItemList.size() > 0) {
			for (int i = 0; i < VehicleSkeletonFragment.mJobItemList.size(); i++) {
				JobItem jobItem = VehicleSkeletonFragment.mJobItemList.get(i);
				if (jobItem != null) {
					String service_ID = jobItem.getServiceID();
					if (!TextUtils.isEmpty(service_ID)) {
						try {
							if (serviceID == Integer.parseInt(service_ID)
									&& 5 == Integer.parseInt(jobItem
											.getActionType())) {
								jobItem.setDeleteStatus(1);
							}
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// CR 444 : save state of favorite separator
		if(null != favouriteListSeperator){
		outState.putBoolean(FAVOURITE_SEPARATOR_KEY,
				favouriteListSeperator.isShown());
		}
		if (!mSaveButtonClicked) {
			saveAdditionalServices();
			Constants.JOB_ITEMLIST = VehicleSkeletonFragment.mJobItemList;
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onActivityBackPress() {
		LogUtil.i("INSIDE", "onActivityBackPress ADDL SERV");
		System.out.println("INSIDE onActivityBackPress ADDL SERV");
		boolean isSaved = saveAdditionalServices();
		// EjobFormActionActivity.saveJobInfo(null);
		mSaveButtonClicked = true;
		return isSaved;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#hideKeyBoard
	 * (boolean)
	 */
	@Override
	public void hideKeyBoard(boolean toHide) {
	}

	/**
	 * Method enabling the save icon as per the user selection on the UI It will
	 * always turn the save icon color into red if there will be data to be
	 * saved for the local database file
	 */
	public void enableSaveButton() {
		if (mSaveJob != null && additionalServiceLoaded) {
			mSaveJob.setEnabled(true);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_RED);
			mSaveJob.setBackgroundResource(R.drawable.continue_savejob_navigation);
		}
	}

	/**
	 * Method disabling the save icon as per the user selection on the UI It
	 * will always turn the save icon color into grey if there will not be any
	 * data to be saved for the local database file
	 */
	public void disableSaveButton() {
		if (mSaveJob != null) {
			mSaveJob.setEnabled(false);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_GRAY);
			mSaveJob.setBackgroundResource(R.drawable.unsavejob_navigation);
		}
	}

	/**
	 * This method will always turn the save icon color into black if there will
	 * not be any data to be saved for the local database file It will navigate
	 * user back to the eJoblist
	 */
	public void finalSaveButton() {
		if (mSaveJob != null) {
			mSaveJob.setEnabled(true);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_BLACK);
			mSaveJob.setBackgroundResource(R.drawable.savejob_navigation);
		}
	}

	// CR 444 starts
	/**
	 * This class handles the favourite button click for a non favourite
	 * service. this is attached to the UI when the additional service is loaded
	 * in loadAdditionalService()
	 */
	private class FavouriteButtonListner implements OnClickListener {

		@Override
		public void onClick(View selectService) {
			// create favourite for selected service
			View serviceRow = (View) selectService.getParent();
			createFavouriteService(serviceRow);
			showFavouriteSeperator();
		}
	}

	// CR : 444
	/**
	 * Unfavorite listener for additional service This is attached to all the
	 * favourite services
	 */
	private class UnFavouriteButtonListner implements OnClickListener {

		@Override
		public void onClick(View selectedService) {
			String serviceName = getServiceName(selectedService);
			View serviceRow = (View) selectedService.getParent();
			removeFavouriteService(serviceRow, serviceName);
			hideFavouriteSeperator();
		}
	}

	// CR : 444
	/**
	 * This method removes the selected favourite service from favourite
	 * container.
	 * 
	 * @param serviceRow
	 *            service row to be removed from favourite list
	 * @param serviceName
	 *            service name to be removed.
	 */
	private void removeFavouriteService(View serviceRow, String serviceName) {
		boolean isFavourite = isSelectedRowInFavourites(serviceRow);
		if (isFavourite) {
			View originalServiceRow = originalServicetable.get(serviceName);

			// update original row
			ImageView favImage = (ImageView) originalServiceRow
					.findViewById(R.id.btn_fav);
			favImage.setBackgroundResource(R.drawable.favourite_unselected);
			favImage.setOnClickListener(new FavouriteButtonListner());

			// remove favourite row
			removeViewFromParent(serviceRow);
		} else {
			View favouriteServiceRow = favouriteServicetable.get(serviceName);

			// remove favourite row
			removeViewFromParent(favouriteServiceRow);

			// update original row
			ImageView favImage = (ImageView) serviceRow
					.findViewById(R.id.btn_fav);
			favImage.setBackgroundResource(R.drawable.favourite_unselected);
			favImage.setOnClickListener(new FavouriteButtonListner());
		}
		// update service as favourite in database
		updateFavouriteStatusInDB(serviceName, UNFAVOURITE_STATUS_DB);
		// remove view from favourite list
		favouriteServicetable.remove(serviceName);
	}

	// CR : 444
	/**
	 * removes a view from its parent
	 */
	private void removeViewFromParent(View view) {
		if (view != null) {
			ViewGroup viewParent = (ViewGroup) view.getParent();
			viewParent.removeView(view);
		}
	}

	// CR : 444
	/**
	 * This function takes the service row, clones it and adds the clone to the
	 * favourite list. The new service created is a dummy row. When user
	 * interacts with its components it in turn communicates with the original
	 * row.
	 * 
	 * @param serviceRow
	 *            This is the service row to be cloned from original additional
	 *            service.
	 */
	private void createFavouriteService(View serviceRow) {
		ImageView btnFavourite = (ImageView) serviceRow
				.findViewById(R.id.btn_fav);
		btnFavourite.setBackgroundResource(R.drawable.favourite_selected);
		btnFavourite.setOnClickListener(new UnFavouriteButtonListner());

		TextView serviceName = (TextView) serviceRow
				.findViewById(R.id.lbl_service);
		String serviceNameString = serviceName.getText().toString();

		TextView serviceValue = (TextView) serviceRow
				.findViewById(R.id.value_service);
		String serviceValueString = serviceValue.getText().toString();

		Switch serviceSwitch = (Switch) serviceRow
				.findViewById(R.id.value_serviceSwitchBtn);
		// check if selected service is a fixed quantity service
		ImageView btn_down = (ImageView) serviceRow
				.findViewById(R.id.service_downBtn);
		int fixedQuandtity = (btn_down.getVisibility() == ImageView.INVISIBLE) ? 1
				: 0;

		// serviceContainer.removeView(serviceRow);
		View favouriteServiceView = infalteService(serviceNameString,
				serviceValueString, fixedQuandtity, serviceSwitch.isChecked());

		// add view lexicographically to the favourite list
		addViewToFavourite(mFavServiceContainer, favouriteServiceView);
		// add favourite list to favourite table
		favouriteServicetable.put(serviceNameString, favouriteServiceView);
		// update service as favourite in database
		updateFavouriteStatusInDB(serviceNameString, FAVOURITE_STATUS_DB);
	}

	// CR : 444
	/**
	 * updated service as favourite service in db
	 * 
	 * @param serviceName
	 *            service name to be updated
	 * @param status
	 *            service status to be updated
	 */
	private void updateFavouriteStatusInDB(String serviceName, int status) {
		mDbHelper.open();
		mDbHelper.updateFavService(serviceName, status);
		mDbHelper.close();
	}

	// CR : 444
	/**
	 * add the view to the favourite container
	 * 
	 * @param mFavServiceContainer2
	 *            favourite service container
	 * @param favouriteServiceView
	 *            inflated service view
	 */
	private void addViewToFavourite(LinearLayout mFavServiceContainer,
			View favouriteServiceToAdd) {
		LinearLayout serviceToAdd = (LinearLayout) favouriteServiceToAdd;
		String serviceNameToAdd = getServiceName(serviceToAdd.getChildAt(0));
		for (int itrator = 0; itrator <= mFavServiceContainer.getChildCount(); itrator++) {
			LinearLayout favouriteService = (LinearLayout) mFavServiceContainer
					.getChildAt(itrator);
			if (favouriteService == null) {
				mFavServiceContainer.addView(favouriteServiceToAdd, itrator);
				return;
			}
			String favouriteServiceName = getServiceName(favouriteService
					.getChildAt(0));
			if (favouriteServiceName.toUpperCase(Locale.getDefault())
					.compareTo(
							serviceNameToAdd.toUpperCase(Locale.getDefault())) < 0) {
				continue;
			}
			mFavServiceContainer.addView(favouriteServiceToAdd, itrator);
			break;
		}
	}

	// CR : 444
	/**
	 * This service inflates a favourite service from xml
	 * 
	 * @param serviceName
	 *            service name to be assigned to the favourite list
	 * @param serviceQuantity
	 *            service quantity to be set to favourite list
	 * @param mFixedQuantity
	 *            if service is of fixed quantity
	 * @param serviceSelected
	 *            is service switch button selected
	 * @return inflated service row
	 */
	private View infalteService(String serviceName, String serviceQuantity,
			int mFixedQuantity, boolean serviceSelected) {
		LayoutInflater layoutInflator = LayoutInflater.from(mContext);
		View mServiceViewLL = layoutInflator.inflate(
				R.layout.additional_services_row, null);
		mServiceViewLL.setTag(FAVOURITE_SERVICE_TAG);
		TextView serviceLabel = (TextView) mServiceViewLL
				.findViewById(R.id.lbl_service);
		ImageView mFavImage = (ImageView) mServiceViewLL
				.findViewById(R.id.btn_fav);

		final TextView mServiceValue = (TextView) mServiceViewLL
				.findViewById(R.id.value_service);
		final Switch switchBtn = (Switch) mServiceViewLL
				.findViewById(R.id.value_serviceSwitchBtn);
		switchBtn.setTextOn(yesLabel);
		switchBtn.setTextOff(noLabel);
		switchBtn.setChecked(serviceSelected);
		switchBtn
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {

						Hashtable<String, View> serviceTable = getServicetable(buttonView);
						if (serviceTable != null) {
							updateFavouriteList(buttonView, isChecked,
									SWITCH_UPDATE_WIDGET_ID, null);
						}
						if (!isChecked) {
							mServiceValue.setText(mServiceQuantity);
						}
					}
				});
		mServiceValue.setText(serviceQuantity);
		serviceLabel.setText(serviceName);
		if (mFavImage != null) {
			mFavImage.setBackgroundResource(R.drawable.favourite_selected);
			mFavImage.setOnClickListener(new UnFavouriteButtonListner());
		}
		mDownBtn = (ImageView) mServiceViewLL
				.findViewById(R.id.service_downBtn);
		mupBtn = (ImageView) mServiceViewLL.findViewById(R.id.service_upBtn);
		mDownBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View btnDown) {
				updateFavouriteList(btnDown, false,
						DOWN_BUTTON_UPDATE_WIDGET_ID, null);
			}
		});
		mupBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View upBtn) {
				updateFavouriteList(upBtn, false, UP_BUTTON_UPDATE_WIDGET_ID,
						null);
			}
		});

		if (mFixedQuantity == 1) {
			mDownBtn.setVisibility(View.INVISIBLE);
			mServiceValue.setVisibility(View.INVISIBLE);
			mupBtn.setVisibility(View.INVISIBLE);
		}
		return mServiceViewLL;
	}

	// CR : 444
	/**
	 * this method updates the switch on the service. This method is used by
	 * dummy favourite service row to communicate with original service.
	 * 
	 * @param buttonView
	 *            switch button on selected service
	 * @param isChecked
	 *            is service selected
	 * @param serviceTable
	 *            service table
	 */
	private void updateSwitchOnOtherService(CompoundButton buttonView,
			boolean isChecked, Hashtable<String, View> serviceTable) {
		String serviceString = getServiceName(buttonView);
		View view = serviceTable.get(serviceString);
		Switch switchBtn = (Switch) view
				.findViewById(R.id.value_serviceSwitchBtn);
		switchBtn.setChecked(isChecked);
	}

	// CR : 444
	/**
	 * this method performs a increment button click.This method is used by
	 * dummy favourite service row to communicate with original service.
	 * 
	 * @param upButton
	 *            up button on selected service
	 * @param isChecked
	 *            is service selected
	 * @param serviceTable
	 *            service table
	 */
	private void updateUpButtonOnOtherService(ImageView upButton,
			boolean isChecked, Hashtable<String, View> serviceTable) {
		String serviceString = getServiceName(upButton);
		View view = serviceTable.get(serviceString);
		ImageView btnUp = (ImageView) view.findViewById(R.id.service_upBtn);
		btnUp.performClick();
	}

	// CR : 444
	/**
	 * this method performs a decrement button click.This method is used by
	 * dummy favourite service row to communicate with original service.
	 * 
	 * @param downButton
	 *            down button on selected service
	 * @param isChecked
	 *            is service checked
	 * @param serviceTable
	 *            service table
	 */
	private void updateDownButtonOnOtherService(ImageView downButton,
			boolean isChecked, Hashtable<String, View> serviceTable) {
		String serviceString = getServiceName(downButton);
		View view = serviceTable.get(serviceString);
		ImageView btnDown = (ImageView) view.findViewById(R.id.service_downBtn);
		btnDown.performClick();
	}

	// CR : 444
	/**
	 * this method updates service across the favourite or original service i.e.
	 * if the service passed was in Original list then update value in favourite
	 * list and vice-versa.
	 * 
	 * @param siblingView
	 *            selected service view that was updated.
	 * @param serviceValue
	 *            service value to be updated.
	 * @param serviceTable
	 *            service table from which the view to be updated should be
	 *            pulled.
	 */
	private void updateServiceValueAcrossFavouriteAndOriginalService(
			View siblingView, String serviceValue,
			Hashtable<String, View> serviceTable) {
		String serviceString = getServiceName(siblingView);
		View view = serviceTable.get(serviceString);
		if (view != null) {
			TextView textServiceValue = (TextView) view
					.findViewById(R.id.value_service);
			textServiceValue.setText(serviceValue);
		}
	}

	// CR : 444
	/**
	 * This method updates the favourite service with the information from
	 * original service
	 * 
	 * @param view
	 *            selected view
	 * @param isChecked
	 *            is service checked
	 * @param widgetsID
	 *            selected operation
	 * @param serviceValue
	 *            updated service value
	 */
	private void updateFavouriteList(View view, boolean isChecked,
			int widgetsID, String serviceValue) {
		String serviceName = getServiceName(view);
		boolean isFavourite = checkServiceIsFavourite(serviceName);
		if (isFavourite) {
			Hashtable<String, View> serviceTable = getServicetable(view);
			if (serviceTable != null && widgetsID == SWITCH_UPDATE_WIDGET_ID) {
				updateSwitchOnOtherService((CompoundButton) view, isChecked,
						serviceTable);
			} else if (serviceTable != null
					&& widgetsID == UP_BUTTON_UPDATE_WIDGET_ID) {
				updateUpButtonOnOtherService((ImageView) view, isChecked,
						serviceTable);
			} else if (serviceTable != null
					&& widgetsID == DOWN_BUTTON_UPDATE_WIDGET_ID) {
				updateDownButtonOnOtherService((ImageView) view, isChecked,
						serviceTable);
			} else if (serviceTable != null && !TextUtils.isEmpty(serviceValue)
					&& widgetsID == SERVICE_VALUE_UPDATE_WIDGET_ID) {
				updateServiceValueAcrossFavouriteAndOriginalService(view,
						serviceValue, serviceTable);
			}
		}
	}

	// CR : 444
	/**
	 * This method returns the service table based on where the service is
	 * selected i.e. favourite list or original list.
	 * 
	 * @param selectedView
	 *            selected service
	 * @return hashtable with correct list.
	 */
	private Hashtable<String, View> getServicetable(View selectedView) {
		View parentView = (View) selectedView.getParent();
		String tag = (String) parentView.getTag();

		if (FAVOURITE_SERVICE_TAG.equals(tag)) {
			return originalServicetable;
		} else if (ORIGNAL_SERVICE_TAG.equals(tag)) {
			return favouriteServicetable;
		}
		return null;
	}

	// CR : 444
	/**
	 * This method returns if the service is in original list or favourite list.
	 * 
	 * @param selectedView
	 *            selected service
	 * @return true if service is selected in favourite list, false if selected
	 *         from original list
	 */
	private boolean isSelectedRowInFavourites(View selectedView) {
		String tag = (String) selectedView.getTag();

		if (FAVOURITE_SERVICE_TAG.equals(tag)) {
			return true;
		} else if (ORIGNAL_SERVICE_TAG.equals(tag)) {
			return false;
		}
		return false;
	}

	// CR : 444
	/**
	 * This method is used to get the service name by passing any sibling view
	 * of the text containing the service name. see additional_services_row.xml
	 * for more details
	 * 
	 * @param view
	 *            sibling view
	 * @return name of the service
	 */
	private String getServiceName(View view) {
		View parentView = (View) view.getParent();
		TextView serviceName = (TextView) parentView
				.findViewById(R.id.lbl_service);
		return serviceName.getText().toString();
	}

	// CR : 444
	/**
	 * This method checks if the provided service is favourite or not. This
	 * status is stored in DB.
	 * 
	 * @param serviceName
	 *            name of the service to be checked.
	 * @return true if service is set as favourite, false otherwise.
	 */
	private boolean checkServiceIsFavourite(String serviceName) {
		mDbHelper.open();
		int favourite = 0;
		Cursor favouriteStatusCursor = mDbHelper
				.getServiceFavouriteStatus(serviceName);
		if (CursorUtils.isValidCursor(favouriteStatusCursor)
				&& favouriteStatusCursor.moveToFirst()) {
			favourite = favouriteStatusCursor.getInt(favouriteStatusCursor
					.getColumnIndex("fav"));
		}
		CursorUtils.closeCursor(favouriteStatusCursor);
		boolean isFavourite = (favourite == 1) ? true : false;
		mDbHelper.close();
		return isFavourite;
	}

	/**
	 * Use this mehtod to show favourite list seperator when there are favourite
	 * services
	 */
	private void showFavouriteSeperator() {
		if (favouriteServicetable != null && !favouriteServicetable.isEmpty()) {
			favouriteListSeperator.setVisibility(LinearLayout.VISIBLE);
		}
	}

	/**
	 * Use this mehtod to hide favourite list seperator when there are no more
	 * favourite services
	 */
	private void hideFavouriteSeperator() {
		if (favouriteServicetable != null && favouriteServicetable.isEmpty()) {
			favouriteListSeperator.setVisibility(LinearLayout.GONE);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.IEjobFragmentVisible#fragmentIsVisibleToUser
	 * ()
	 */
	@Override
	public void fragmentIsVisibleToUser() {
		updateSaveButtonOnUIBasedOnLastFragment();
	}

	/**
	 * This method is called from view pager listener when this fragment is
	 * selected. It updates the save icon based on the status of save button in
	 * last fragment.
	 */
	private void updateSaveButtonOnUIBasedOnLastFragment() {
		if (saveIconHandlerActivity == null)
			return;
		int saveIconState = saveIconHandlerActivity.getSaveIconState();
		switch (saveIconState) {
		case EjobSaveIconStates.ICON_GRAY:
			disableSaveButton();
			break;
		case EjobSaveIconStates.ICON_BLACK:
			finalSaveButton();
			break;
		case EjobSaveIconStates.ICON_RED:
			enableSaveButton();
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDetach()
	 */
	@Override
	public void onDetach() {
		super.onDetach();
		saveIconHandlerActivity = null;
	}
}