package com.goodyear.ejob.fragment;

import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.interfaces.IFragmentCommunicate;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.interfaces.IEjobFragmentVisible;
import com.goodyear.ejob.interfaces.ISaveIconEjobFragment;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.EjobSaveIconStates;
import com.goodyear.ejob.util.Job;
import com.goodyear.ejob.util.JobBundle;

/**
 * @author amitkumar.h Class handling the history related to the vehicle and job
 *         performed on it It displays the actions recorded on the vehicle
 *         during tire operation
 */
public class EjobVehicleSummaryFragment extends Fragment implements
		IFragmentCommunicate, IEjobFragmentVisible {

	private Context mContext;

	public TextView mLabelAccount;
	public TextView mValueAccount;

	public TextView mLabelVehId;
	public TextView mValueVehId;

	public TextView mLabelFleetNumber;
	public TextView mValueFleetNumber;

	public TextView mLabelRefNumber;
	public TextView mValueRefNumber;

	public Button mSaveButton;
	public ImageView mJobTypeIcon;

	public TextView mLabelPrimaryODO;
	public TextView mValPrimaryODO;

	public TextView mLabelSecondaryODO;
	public TextView mValSecondaryODO;

	public TextView mLabelCallCenterRefNo;
	public TextView mValueCallCenterRefNo;

	public TextView mLabelDriverName;
	public TextView mValueDriverName;

	public TextView mLabelDefectNo;
	public TextView mValueDefectNo;

	public TextView mLabelCallerName;
	public TextView mValueCallerName;

	public TextView mLabelCallerPhone;
	public TextView mValueCallerPhone;

	public TextView mLblTimeRecieved;
	public TextView mValueTimeRecieved;

	public TextView mLblTimeStarted;
	public TextView mValueTimeStarted;

	public TextView mLblTimeOnSite;
	public TextView mValueTimeOnSite;

	public TextView mLblTimeFinished;
	public TextView mValueTimeFinished;

	public TextView mLblLocation;
	public TextView mValueLocation;

	// GPS Location
	public TextView mLblGPSLocation;
	public TextView mLblLat;
	public TextView mValueLat;
	public TextView mLblLong;
	public TextView mValueLong;

	public TextView mLblAddress;
	public TextView mValueAddress;

	public TextView mLblStockLocation;
	public TextView mValueStockLocation;

	public TextView mVehicleSummaryTitle;

	private static ISaveIconEjobFragment saveIconHandlerActivity;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		saveIconHandlerActivity = (ISaveIconEjobFragment) activity;
		mContext = activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.vehicle_summary, container,
				false);
		initalize(rootView);
		loadLabelsFromDB(rootView);
		displayVehicleSummary();
		return rootView;
	}

	/**
	 * Loading The Labels from Translation Tables to handle different languages
	 * 
	 * @param rootView
	 */
	private void loadLabelsFromDB(View rootView) {
		String labelFromDB = "";
		DatabaseAdapter dbHelper = new DatabaseAdapter(mContext);
		dbHelper.createDatabase();
		dbHelper.open();
		try {
			// header
			labelFromDB = dbHelper.getLabel(Constants.VEHICLE_SUMMARY_TITLE);
			mVehicleSummaryTitle.setText(labelFromDB);
			labelFromDB = dbHelper.getLabel(Constants.ACCOUNT_LABEL);
			mLabelAccount.setText(labelFromDB);
			labelFromDB = dbHelper.getLabel(Constants.VEH_ID_LABEL);
			mLabelVehId.setText(labelFromDB);
			// TODO labelFromDB = label.getLabel(Constants.REF_NUMBER_LABEL);
			mLabelRefNumber.setText("#");
			if (!Constants.COMESFROMUPDATE && !Constants.COMESFROMVIEW) {
				mValueVehId.setText(Constants.VEHID);
				mValueAccount.setText(Constants.ACCOUNTNAME);
				mValueFleetNumber.setText(Constants.FLEETNUM);
				mValueRefNumber.setText(Constants.JOBREFNUMBER);
			}
			// form
			labelFromDB = dbHelper.getLabel(Constants.PRIMARY_ODO_LABEL);
			mLabelPrimaryODO.setText(labelFromDB);

			labelFromDB = dbHelper.getLabel(Constants.SECONDARY_ODO_LABEL);
			mLabelSecondaryODO.setText(labelFromDB);

			labelFromDB = dbHelper
					.getLabel(Constants.CALL_CENTER_REF_NUMBER_LABEL);
			mLabelCallCenterRefNo.setText(labelFromDB);

			labelFromDB = dbHelper.getLabel(Constants.DRIVER_NAME_LABEL);
			mLabelDriverName.setText(labelFromDB);

			labelFromDB = dbHelper.getLabel(Constants.DEFECT_NUMBER_LABEL);
			mLabelDefectNo.setText(labelFromDB);

			labelFromDB = dbHelper.getLabel(Constants.CALLER_NAME_LABEL);
			mLabelCallerName.setText(labelFromDB);

			labelFromDB = dbHelper
					.getLabel(Constants.CALLER_PHONE_NUMBER_LABEL);
			mLabelCallerPhone.setText(labelFromDB);

			// Time & Location
			labelFromDB = dbHelper.getLabel(Constants.TIME_RECIEVED_LABEL);
			mLblTimeRecieved.setText(labelFromDB);

			labelFromDB = dbHelper.getLabel(Constants.TIME_STARTED_LABEL);
			mLblTimeStarted.setText(labelFromDB);

			labelFromDB = dbHelper.getLabel(Constants.TIME_ON_SITE_LABEL);
			mLblTimeOnSite.setText(labelFromDB);

			labelFromDB = dbHelper.getLabel(Constants.TIME_FINISHED_LABEL);
			mLblTimeFinished.setText(labelFromDB);

			labelFromDB = dbHelper.getLabel(Constants.LOCATION_LABEL);
			mLblLocation.setText(labelFromDB);

			labelFromDB = dbHelper.getLabel(Constants.STOCK_LOCATION_LABEL);
			mLblStockLocation.setText(labelFromDB);

			mLblGPSLocation.setText(dbHelper
					.getLabel(Constants.GPS_LOCATION_ODO_LABEL));
			mLblAddress.setText(dbHelper.getLabel(Constants.GPS_ADDRESS_LABEL));
			mLblLat.setText(dbHelper.getLabel(Constants.GPS_LAT));
			mLblLong.setText(dbHelper.getLabel(Constants.GPS_LNG));
			labelFromDB = dbHelper.getLabel(Constants.FLEET_NO);
			mLabelFleetNumber.setText(labelFromDB);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dbHelper != null) {
				dbHelper.close();
			}
		}

	}

	/**
	 * Method Initializing The UI Labels to be displayed while loading the page
	 * 
	 * @param view
	 */
	private void initalize(View view) {

		mVehicleSummaryTitle = (TextView) view
				.findViewById(R.id.vehicle_summary_title);
		// header
		mLabelAccount = (TextView) view.findViewById(R.id.lbl_account);
		mValueAccount = (TextView) view.findViewById(R.id.value_account);

		mLabelVehId = (TextView) view.findViewById(R.id.lbl_vehId);
		mValueVehId = (TextView) view.findViewById(R.id.value_vehId);

		mLabelFleetNumber = (TextView) view.findViewById(R.id.lbl_fleetNo);
		mValueFleetNumber = (TextView) view.findViewById(R.id.value_fleetNo);

		mLabelRefNumber = (TextView) view.findViewById(R.id.lbl_jobRefNo);
		mValueRefNumber = (TextView) view.findViewById(R.id.value_jobRefNo);

		if (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW) {
			mValueRefNumber.setText(JobBundle.getmRefNO().toString());
			mValueVehId.setText(JobBundle.getmLPlate().toString());
			mValueAccount.setText(JobBundle.getmAccountName());
			mValueFleetNumber.setText(JobBundle.getmFleetNO());
		}
		// form
		mLabelPrimaryODO = (TextView) view.findViewById(R.id.lbl_primaryODO);
		mValPrimaryODO = (TextView) view.findViewById(R.id.value_primaryODO);

		mLabelSecondaryODO = (TextView) view
				.findViewById(R.id.lbl_secondaryODO);
		mValSecondaryODO = (TextView) view
				.findViewById(R.id.value_secondaryODO);

		mLabelCallCenterRefNo = (TextView) view
				.findViewById(R.id.lbl_callCenterRefNo);
		mValueCallCenterRefNo = (TextView) view
				.findViewById(R.id.value_callCenterRefNo);

		mLabelDriverName = (TextView) view.findViewById(R.id.lbl_driverName);
		mValueDriverName = (TextView) view.findViewById(R.id.value_driverName);

		mLabelDefectNo = (TextView) view.findViewById(R.id.lbl_defectAuthNo);
		mValueDefectNo = (TextView) view.findViewById(R.id.value_defectAuthNo);

		mLabelCallerName = (TextView) view.findViewById(R.id.lbl_callerName);
		mValueCallerName = (TextView) view.findViewById(R.id.value_callerName);

		mLabelCallerPhone = (TextView) view
				.findViewById(R.id.lbl_callerPhoneNo);
		mValueCallerPhone = (TextView) view
				.findViewById(R.id.value_callerPhoneNo);

		// Time & Location (Starts)
		mLblTimeRecieved = (TextView) view.findViewById(R.id.lbl_timeReceived);
		mValueTimeRecieved = (TextView) view
				.findViewById(R.id.value_timeReceived);

		mLblTimeStarted = (TextView) view.findViewById(R.id.lbl_timeStarted);
		mValueTimeStarted = (TextView) view
				.findViewById(R.id.value_timeStarted);

		mLblTimeOnSite = (TextView) view.findViewById(R.id.lbl_timeOnSite);
		mValueTimeOnSite = (TextView) view.findViewById(R.id.value_timeOnSite);

		mLblTimeFinished = (TextView) view.findViewById(R.id.lbl_timeFinished);
		mValueTimeFinished = (TextView) view
				.findViewById(R.id.value_timeFinished);

		mLblLocation = (TextView) view.findViewById(R.id.lbl_location);
		mValueLocation = (TextView) view.findViewById(R.id.value_location);

		mLblGPSLocation = (TextView) view.findViewById(R.id.lbl_GPSlocation);
		mLblLat = (TextView) view.findViewById(R.id.lbl_GPSlocLat);
		mValueLat = (TextView) view.findViewById(R.id.value__GPSlocLat);
		mLblLong = (TextView) view.findViewById(R.id.lbl_GPSlocLong);
		mValueLong = (TextView) view.findViewById(R.id.value__GPSlocLong);
		mLblAddress = (TextView) view.findViewById(R.id.lbl_GPSlocAddress);
		mValueAddress = (TextView) view.findViewById(R.id.value__GPSlocAddress);

		mLblStockLocation = (TextView) view
				.findViewById(R.id.lbl_stockLocation);
		mValueStockLocation = (TextView) view
				.findViewById(R.id.value_stockLocation);
		mSaveButton = (Button) view.findViewById(R.id.btn_save);
		if (Constants.COMESFROMVIEW) {
			mSaveButton.setVisibility(View.INVISIBLE);
		} else {
			mSaveButton.setVisibility(View.VISIBLE);
		}
		mSaveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/**
				 * Condition added to disable save button once clicked during Save Job in DB
				 */
				if (saveIconHandlerActivity.getSaveIconState() == EjobSaveIconStates.ICON_BLACK) {
				disableSaveButton();
				EjobFormActionActivity.saveJobInfo(v, true);
			} else if (saveIconHandlerActivity.getSaveIconState() == EjobSaveIconStates.ICON_RED) {
				disableSaveButton();
				EjobFormActionActivity.saveJobInfo(v, false);
				finalSaveButton();
			}}
		});
		// job icon on header
		mJobTypeIcon = (ImageView) view.findViewById(R.id.jobIcon);
		if (!Constants.JOBTYPEREGULAR) {
			mJobTypeIcon.setImageResource(R.drawable.breakdown_job);
		}
	}

	/**
	 * This methods will display the vehicle summary details on the UI as per
	 * the action performed on it
	 */
	public void displayVehicleSummary() {
		try {

			String primaryODO = Job.getVehicleODO();
			String secondaryODO = Job.getHudometer();
			String callCenterRefNo = Job.getActionLineN();
			String defectNo = Job.getDefectAuthNumber();
			String driverName = Job.getDriverName();
			String callerName = Job.getCallerName();
			String callerPhone = Job.getCallerPhone();
			String timeReceived = "";
			String timeStarted = "";
			String timeOnSite = "";
			String timeFinsihed = "";
			String location = Job.getLocation();
			String latitude = "";
			String longitude = "";
			String address = "";
			String stockLocation = Job.getStockLocation();

			if (!TextUtils.isEmpty(Job.getTimeReceived())) {
				Date receivedDateTimeFromDB = DateTimeUTC.toDate(Long
						.parseLong(Job.getTimeReceived()));

				timeReceived = DateTimeUTC.convertDateToString(
						receivedDateTimeFromDB, mContext)
						+ " "
						+ DateTimeUTC.convertTimeToString(
								receivedDateTimeFromDB, mContext);
			}

			if (!TextUtils.isEmpty(Job.getTimeStarted())) {
				Date startedDateTimeFromDB = DateTimeUTC.toDate(Long
						.parseLong(Job.getTimeStarted()));
				timeStarted = DateTimeUTC.convertDateToString(
						startedDateTimeFromDB, mContext)
						+ " "
						+ DateTimeUTC.convertTimeToString(
								startedDateTimeFromDB, mContext);
			}

			if (!TextUtils.isEmpty(Job.getTimeOnSite())) {
				Date onSiteDateTimeFromDB = DateTimeUTC.toDate(Long
						.parseLong(Job.getTimeOnSite()));
				timeOnSite = DateTimeUTC.convertDateToString(
						onSiteDateTimeFromDB, mContext)
						+ " "
						+ DateTimeUTC.convertTimeToString(onSiteDateTimeFromDB,
								mContext);
			}

			if (!TextUtils.isEmpty(Job.getTimeFinished())) {
				Date finishedDateTimeFromDB = DateTimeUTC.toDate(Long
						.parseLong(Job.getTimeFinished()));
				timeFinsihed = DateTimeUTC.convertDateToString(
						finishedDateTimeFromDB, mContext)
						+ " "
						+ DateTimeUTC.convertTimeToString(
								finishedDateTimeFromDB, mContext);
			}

			if (!TextUtils.isEmpty(Job.getGpsLocation())) {
				String[] splittedGpsLocation = Job.getGpsLocation()
						.split("\\|");
				if (splittedGpsLocation.length == 3) {
					latitude = splittedGpsLocation[0];
					longitude = splittedGpsLocation[1];
					address = splittedGpsLocation[2];
				} else if (splittedGpsLocation.length == 2) {
					latitude = splittedGpsLocation[0];
					longitude = splittedGpsLocation[1];
				}
			}

			mValPrimaryODO.setText(primaryODO);
			mValSecondaryODO.setText(secondaryODO);
			mValueCallCenterRefNo.setText(callCenterRefNo);
			mValueDriverName.setText(driverName);
			mValueDefectNo.setText(defectNo);
			mValueCallerName.setText(callerName);
			mValueCallerPhone.setText(callerPhone);

			// Time & Location
			mValueTimeRecieved.setText(timeReceived);
			mValueTimeStarted.setText(timeStarted);
			mValueTimeOnSite.setText(timeOnSite);
			mValueTimeFinished.setText(timeFinsihed);

			mValueLocation.setText(location);
			mValueLat.setText(latitude);
			mValueLong.setText(longitude);
			mValueAddress.setText(address);
			mValueStockLocation.setText(stockLocation);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			CommonUtils.hideKeyboard(mContext, getView().getWindowToken());
		}
	}

	/**
	 * Method handling the hide/show of virtual Android keyboard
	 * 
	 * @param context
	 */
	protected void hideKeyboard(Context context) {
		try {
			InputMethodManager in = (InputMethodManager) context
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			in.hideSoftInputFromWindow(getView().getWindowToken(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#
	 * onActivityBackPress()
	 */
	@Override
	public boolean onActivityBackPress() {
		return false;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.EjobFormActionActivity.IFragmentCommunicate#hideKeyBoard
	 * (boolean)
	 */
	@Override
	public void hideKeyBoard(boolean toHide) {
		if (toHide) {
			hideKeyboard(this.getActivity());
		}
	}
	/**
	 * Method enabling the save icon as per the user selection on the UI
	 * It will always turn the save icon color into red if there will be data to be saved 
	 * for the local database file
	 */
	public void enableSaveButton() {
		if (mSaveButton != null) {
			mSaveButton.setEnabled(true);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_RED);
			mSaveButton
					.setBackgroundResource(R.drawable.continue_savejob_navigation);
		}
	}
	/**
	 * Method disabling the save icon as per the user selection on the UI
	 * It will always turn the save icon color into grey if there will not be any 
	 * data to be saved for the local database file
	 */
	public void disableSaveButton() {
		if (mSaveButton != null) {
			mSaveButton.setEnabled(false);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_GRAY);
			mSaveButton.setBackgroundResource(R.drawable.unsavejob_navigation);
		}
	}
	/**
	 * This method will always turn the save icon color into black 
	 * if there will not be any data to be saved for the local database file
	 * It will navigate user back to the eJoblist
	 */
	public void finalSaveButton() {
		if (mSaveButton != null) {
			mSaveButton.setEnabled(true);
			saveIconHandlerActivity
					.setSaveIconState(EjobSaveIconStates.ICON_BLACK);
			mSaveButton.setBackgroundResource(R.drawable.savejob_navigation);
		}
	}

	/**
	 * This method is called from view pager listener when this fragment is
	 * selected. It updates the save icon based on the status of save button in
	 * last fragment.
	 */
	private void updateSaveButtonOnUIBasedOnLastFragment() {
		// TODO Auto-generated method stub
		if (saveIconHandlerActivity == null)
			return;
		int saveIconState = saveIconHandlerActivity.getSaveIconState();
		switch (saveIconState) {
		case EjobSaveIconStates.ICON_GRAY:
			disableSaveButton();
			break;
		case EjobSaveIconStates.ICON_BLACK:
			finalSaveButton();
			break;
		case EjobSaveIconStates.ICON_RED:
			enableSaveButton();
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.IEjobFragmentVisible#fragmentIsVisibleToUser
	 * ()
	 */
	@Override
	public void fragmentIsVisibleToUser() {
		updateSaveButtonOnUIBasedOnLastFragment();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDetach()
	 */
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		saveIconHandlerActivity = null;
	}
}
