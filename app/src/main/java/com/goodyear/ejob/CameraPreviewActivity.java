package com.goodyear.ejob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.job.operation.helpers.TireImageHelpers;
import com.goodyear.ejob.job.operation.helpers.TireImageItem;
import com.goodyear.ejob.ui.tyremanagement.TyreSummary;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.LogUtil;

import java.util.ArrayList;

/**
 * @author amitkumar.h
 * @version 1.0
 */
public class CameraPreviewActivity extends Activity implements
		InactivityHandler {

	/**
	 * Minimum pixels to be swiped in order to consider the gesture as swipe
	 */
	private static final int SWIPE_MIN_DISTANCE = 90;
	private int mPos1x;
	/**
	 * Used for common database handler
	 */
	private DatabaseAdapter mDbHelper;
	private Context mContext;
	/**
	 * Flag to indicate whether this activity originated from history
	 */
	private boolean mIsFromHistory = false;
	/**
	 * Used to contain tyre image blob value
	 */
	private ArrayList<byte[]> mByteArray = new ArrayList<>();
	/**
	 * ArrayList that holds the damage details
	 */
	private ArrayList<String> mArrDamageDetails;
	/**
	 * Contains the 3 pages of image and notes
	 */
	private ViewFlipper mViewFlipper;
	/**
	 * Contains the damage area information
	 */
	private TextView mDamageAreaVal;
	/**
	 * * Contains the damage group information
	 */
	private TextView mDamageGroupVal;

	//User Trace logs trace tag
	private static final String TRACE_TAG = "Camera Preview";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.preview_view);
		//User Trace logs
		LogUtil.TraceInfo(TRACE_TAG, "none", TRACE_TAG, false, false,false);
		mContext = CameraPreviewActivity.this;
		ArrayList<String> damageNote = new ArrayList<>();
		// bundle.putStringArrayList("bundleDamageDetails", mArrDamageDetails);
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if (intent.hasExtra("bundleDamageDetails")) {
			mArrDamageDetails = extras
					.getStringArrayList("bundleDamageDetails");
		}
		if (intent.hasExtra("bundleDamageNote")) {
			damageNote = extras.getStringArrayList("bundleDamageNote");
		}
		TextView damageNoteVAL = (TextView) findViewById(R.id.damage_note_val);
		TextView damageNote2VAL = (TextView) findViewById(R.id.damage_note2_val);
		TextView damageNote3VAL = (TextView) findViewById(R.id.damage_note3_val);
		TextView mDamageAreaLbl = (TextView) findViewById(R.id.damage_area_lbl);

		TextView mDamageGroupLbl = (TextView) findViewById(R.id.damage_group_lbl);
		mDamageAreaVal = (TextView) findViewById(R.id.damage_area_val);
		mDamageGroupVal = (TextView) findViewById(R.id.damage_group_val);
		try {
			if (mArrDamageDetails.get(1) != null) {
				mDamageAreaVal.setText(mArrDamageDetails.get(1));
			}
			if (mArrDamageDetails.get(0) != null) {
				mDamageGroupVal.setText(mArrDamageDetails.get(0));
			}
			if (damageNote.get(0) != null) {
				damageNoteVAL.setText(damageNote.get(0));
			}
			if (damageNote.get(1) != null) {
				damageNote2VAL.setText(damageNote.get(1));
			}
			if (damageNote.get(2) != null) {
				damageNote3VAL.setText(damageNote.get(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		mDbHelper = new DatabaseAdapter(mContext);
		mDbHelper.open();
		mDamageAreaLbl.setText(mDbHelper.getLabel(Constants.DAMAGE_AREA_LABEL));
		mDamageGroupLbl.setText(mDbHelper
				.getLabel(Constants.DAMAGE_GROUP_LABEL));
		setTitle(mDbHelper.getLabel(Constants.PHOTO_LABEL));
		mViewFlipper = (ViewFlipper) findViewById(R.id.view_flipper);
		ImageView imageView1 = (ImageView) findViewById(R.id.image_view1);
		ImageView imageView2 = (ImageView) findViewById(R.id.image_view2);
		ImageView imageView3 = (ImageView) findViewById(R.id.image_view3);
		if (intent.hasExtra("FROM_HISTORY")) {
			mIsFromHistory = extras.getBoolean("FROM_HISTORY");
		}
		if (!mIsFromHistory) {
			if (TyreSummary.mCamaraImgFirst != null) {
				imageView1.setImageBitmap(TyreSummary.mCamaraImgFirst);
			}
			imageView1.setVisibility(View.VISIBLE);
			if (TyreSummary.mCamaraImgSecond != null) {
				imageView2.setImageBitmap(TyreSummary.mCamaraImgSecond);
			}
			imageView2.setVisibility(View.VISIBLE);
			if (TyreSummary.mCamaraImgThird != null) {
				imageView3.setImageBitmap(TyreSummary.mCamaraImgThird);
			}
			imageView3.setVisibility(View.VISIBLE);
		} else {
			mByteArray.clear();
			damageNote.clear();
			int jobItemId = extras.getInt("JOBITEMID");
			ArrayList<TireImageItem> tireImages = new ArrayList<>();
			updateDamageDetails(jobItemId);
			Cursor tyreImageCur = mDbHelper
					.getAllValuesFromTireImageTable(String.valueOf(jobItemId));
			// CR 457: as user can save multiple times we need to check images
			// both in db and arraylist

			// if no images are found in tire image table then check in object
			// other wise load from db
			if (!CursorUtils.isValidCursor(tyreImageCur)) {
				tireImages = TireImageHelpers.getTireImageForWithID(
						VehicleSkeletonFragment.mTireImageItemList, jobItemId);
			} else {
				for (boolean hasTorqueSetting = tyreImageCur.moveToFirst(); hasTorqueSetting; hasTorqueSetting = tyreImageCur
						.moveToNext()) {
					TireImageItem tireImage = new TireImageItem();

					tireImage.setJobItemId(String.valueOf(jobItemId));
					tireImage.setDamageDescription(tyreImageCur
							.getString(tyreImageCur
									.getColumnIndex("Description")));
					tireImage.setImageType(tyreImageCur.getString(tyreImageCur
							.getColumnIndex("ImageType")));
					tireImage.setJobItemId(tyreImageCur.getString(tyreImageCur
							.getColumnIndex("JobItemID")));
					tireImage.setTyreImage(tyreImageCur.getBlob(tyreImageCur
							.getColumnIndex("TyreImage")));
					tireImages.add(tireImage);
				}
			}
			for (TireImageItem tireImageItem : tireImages) {
				byte[] byteData = tireImageItem.getTyreImage();

				mByteArray.add(byteData);
				damageNote.add(tireImageItem.getDamageDescription());
			}
			if (mByteArray.size() == 3) {
				displayTireImage(imageView1, 0);
				imageView1.setVisibility(View.VISIBLE);
				String note = damageNote.get(0);
				damageNoteVAL.setText(note);
				displayTireImage(imageView2, 1);
				imageView2.setVisibility(View.VISIBLE);
				damageNote2VAL.setText(damageNote.get(1));
				displayTireImage(imageView3, 2);
				imageView3.setVisibility(View.VISIBLE);
				damageNote3VAL.setText(damageNote.get(2));
			} else if (mByteArray.size() == 2) {
				displayTireImage(imageView1, 0);
				imageView1.setVisibility(View.VISIBLE);
				String note = damageNote.get(0);
				damageNoteVAL.setText(note);
				displayTireImage(imageView2, 1);
				imageView2.setVisibility(View.VISIBLE);
				damageNote2VAL.setText(damageNote.get(1));
			} else if (mByteArray.size() == 1) {
				displayTireImage(imageView1, 0);
				imageView1.setVisibility(View.VISIBLE);
				String note = damageNote.get(0);
				damageNoteVAL.setText(note);
			}
		}
		imageView1.setOnTouchListener(new CustomTouchListener());
		imageView2.setOnTouchListener(new CustomTouchListener());
		imageView3.setOnTouchListener(new CustomTouchListener());
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onBackPressed() {
		LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press", false, false, false);
		super.onBackPressed();
		mDbHelper.close();
	}

	/**
	 * Used to update the job details to the UI
	 * 
	 * @param jobId
	 *            id of the job
	 */
	private void updateDamageDetails(int jobId) {
		String damageId = TireImageHelpers.getDamgeIDForJobItem(jobId);
		if (null != damageId && !TextUtils.isEmpty(damageId)) {
			Cursor mCursor = mDbHelper.getDamageDetails(damageId);
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				String damageGrp = mCursor.getString(mCursor
						.getColumnIndex("DamageGroup"));
				String damageArea = mCursor.getString(mCursor
						.getColumnIndex("RemovalReasonCode"));
				mDamageGroupVal.setText(damageGrp);
				mDamageAreaVal.setText(damageArea);
				CursorUtils.closeCursor(mCursor);
			} else {
				mDamageGroupVal.setText("");
				mDamageAreaVal.setText("");
			}
		} else// Setting Visibility gone if Damage Group and Damage area are
				// Optional
		{
			LinearLayout damagrGroupLL = (LinearLayout) findViewById(R.id.ll_damage_group);
			LinearLayout damagrAreaLL = (LinearLayout) findViewById(R.id.ll_damage_area);
			LinearLayout note1LL = (LinearLayout) findViewById(R.id.ll_damage_note);
			damagrGroupLL.setVisibility(View.GONE);
			damagrAreaLL.setVisibility(View.GONE);
			note1LL.getLayoutParams().height = 130;// Setting The height to 120
													// dp
		}
	}

	/**
	 * Used to detect swipe gesture of navigating to next actions
	 */
	private final class CustomTouchListener implements OnTouchListener {
		@TargetApi(11)
		public boolean onTouch(View view, MotionEvent motionEvent) {
			if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
				return true;
			} else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
				mPos1x = (int) motionEvent.getX();
				return true;
			} else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
				int pos2x = (int) motionEvent.getX();
				if ((pos2x - mPos1x) >= SWIPE_MIN_DISTANCE) {
					mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(
							mContext, R.anim.right_in));
					mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(
							mContext, R.anim.right_out));
					mViewFlipper.showPrevious();
				} else if ((mPos1x - pos2x) >= SWIPE_MIN_DISTANCE) {
					mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(
							mContext, R.anim.left_in));
					mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(
							mContext, R.anim.left_out));
					mViewFlipper.showNext();
				}
				return true;
			} else {
				return false;
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		// set user inactivity handler for this activity
		LogoutHandler.setCurrentActivity(this);
	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		InactivityUtils.logoutFromActivityAboveEjobForm(this);
	}

	/**
	 * Display Tire Image from the corresponding element in the arrayList
	 * 
	 * @param imgView
	 *            : ImageView on the corresponding images are displayed
	 * @param index
	 *            : passing the corresponding image-count
	 */
	private void displayTireImage(ImageView imgView, int index) {
		if (null != mByteArray.get(index)) {
			imgView.setImageBitmap(TyreSummary.getPhoto(mByteArray.get(index)));
		}
	}
}