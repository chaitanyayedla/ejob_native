
package com.goodyear.ejob.inactivity;

import android.content.DialogInterface;

/**
 *
 */
public interface DialogButtonClickListner extends DialogInterface.OnClickListener{

	public void onActivityHandler();

}
