package com.goodyear.ejob.inactivity;

import android.view.View;
import android.view.View.OnClickListener;

/**
 * @author shailesh.p
 * 
 */
public class EjobInactivityClickListner implements OnClickListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		InactivityUtils.updateActivityOfUser();
	}
}
