
package com.goodyear.ejob.inactivity;

import android.app.AlertDialog;
import android.content.Context;

/**
 * @author shailesh.p
 *
 */
public class EjobAlertDialog extends AlertDialog.Builder{

	/**
	 * @param context
	 */
	public EjobAlertDialog(Context context) {
		super(context);
		
	}
	
	public void updateInactivityForDialog(){
		InactivityUtils.updateActivityOfUser();
	}

}
