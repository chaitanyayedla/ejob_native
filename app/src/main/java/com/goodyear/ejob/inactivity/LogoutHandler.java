package com.goodyear.ejob.inactivity;

import com.goodyear.ejob.dbhelpers.DataBaseHelper;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.util.DbBackupUtil;
import com.goodyear.ejob.util.LogUtil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * @author shailesh.p
 * 
 */
public class LogoutHandler {
	private static String TAG = LogoutHandler.class.getSimpleName();
	public static final String INACTIVITY_INTENT = "com.goodyear.ejob.handlers.INACTIVITY";
	public static final String INACTIVITY_88MIN_INTENT = "com.goodyear.ejob.handlers.DBBACKUP";
	private static InactivityHandler mInactivityHandler;
	private BroadcastReceiver broadcastReceiver;
	private Context mContext;

	public static void setCurrentActivity(InactivityHandler inActivityHandler) {
		LogUtil.i(TAG, "Set Inactivity Set:::");
		mInactivityHandler = inActivityHandler;
	}

	public void unregisterReceiver() {
		if (broadcastReceiver != null)
			mContext.unregisterReceiver(broadcastReceiver);
	}

	/**
	 * 
	 */
	public LogoutHandler(Context context) {
		mContext = context;
		IntentFilter intentFilter = new IntentFilter(INACTIVITY_INTENT);
		final IntentFilter intentFilterBackup = new IntentFilter(INACTIVITY_88MIN_INTENT);

		broadcastReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				if (mInactivityHandler != null && intent.getAction() == INACTIVITY_INTENT) {
					LogUtil.i(TAG, "Logout handler called after inactivity:::");
					mInactivityHandler.logUserOutDueToInactivity();
				}
				if(mInactivityHandler != null && intent.getAction() == INACTIVITY_88MIN_INTENT){
					LogUtil.i(TAG, " Inactive for 88 minutes, initiate Db backup");
					DbBackupUtil dbUtil = new DbBackupUtil(context);
					DataBaseHelper dbHelper = new DataBaseHelper(context);

					if(dbHelper.getDatabaseSize() > 15000){
						//LogUtil.d(TAG, " Taking Db backup");
						//dbUtil.backUpFullDB();
					}
				}
			}
		};
		mContext.registerReceiver(broadcastReceiver, intentFilter);
		mContext.registerReceiver(broadcastReceiver, intentFilterBackup);
	}
}
