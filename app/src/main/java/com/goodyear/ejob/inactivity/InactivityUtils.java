package com.goodyear.ejob.inactivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.GoodYear_eJob_MainActivity;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.DBModel;
import com.goodyear.ejob.job.operation.helpers.CleanOperations;
import com.goodyear.ejob.job.operation.helpers.SaveJob;
import com.goodyear.ejob.service.UserInactivityService;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DbBackupUtil;
import com.goodyear.ejob.util.Job;
import com.goodyear.ejob.util.LogUtil;

import java.io.IOException;

/**
 * deals with the in activity of hte
 */
public class InactivityUtils {
	private static String Log_TAG = "InActivity Utils";
	private static final String TAG = InactivityUtils.class.getSimpleName();

	/**
	 * update the count down timer, when a user intracts with the screen use
	 * this method to update the count down timer
	 */
	public static void updateActivityOfUser() {
		UserInactivityService.updateTimer();
	}

	/**
	 * cancels the count down timer, when user logs out of the application then
	 * clear the count down timer
	 */
	public static void stopUserActivityUpdates() {
		UserInactivityService.stopTimer();
	}

	public static void logoutFromActivityAboveEjobForm(Context context) {

		LogUtil.DBLog(Log_TAG, "logout From Activity Above Ejob Form", "Start");
		Activity activity = (Activity) context;
		SharedPreferences sharedPreference;
		Job.setStatus("1");

		DBModel mDb = new DBModel(context);
		mDb.open();
		LogUtil.DBLog(Log_TAG, "logout From Activity Above Ejob Form", "JobID - " + EjobFormActionActivity.sJobID);
		/*Fix EJOB64, Bugtracker 743 : To avoid duplicate jobs, skip inserting data into Job table again when
		autologged out while viewing a completed/incomplete job*/
		if(!Constants.COMESFROMVIEW) {
			SaveJob.callUpdateQueries(mDb, EjobFormActionActivity.sJobID, context);
		} else {
			LogUtil.DBLog(Log_TAG, " Not updating all the tables. ", "Coming from view job");
		}

		LogUtil.DBLog(Log_TAG, "  saved "," jobs");

		DbBackupUtil dbUtil = new DbBackupUtil(context);
		dbUtil.backUpFullDB();

		CleanOperations.cleanOperationsAfterJob();

		DatabaseAdapter db = new DatabaseAdapter(context);
		db.createDatabase();
		db.open();
		try {
			db.copyFinalDB();
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		db.close();

		Intent intent = new Intent(context, GoodYear_eJob_MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
		LogUtil.DBLog(Log_TAG, "logout From Activity Above Ejob Form", "Call Finish");
		activity.finish();
		LogUtil.DBLog(Log_TAG, "logout From Activity Above Ejob Form", "End");
	}

	public static void logoutFromActivityBelowEjobForm(Context context) {
		Activity activity = (Activity) context;

		LogUtil.DBLog(Log_TAG, " Inside logoutFromActivityBelowEjobForm() ", "Start");

		DbBackupUtil dbUtil = new DbBackupUtil(context);
		dbUtil.backUpFullDB();
		/*
		Possible fix to resolve DB crash issue (12Kb DB issue)
		Bug number : EJOB-96,EJOB-103,EJOB-104,EJOB-100
 		*/
		DatabaseAdapter db = new DatabaseAdapter(context);
		db.createDatabase();
		db.open();
		try {
			db.copyFinalDB();
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		db.close();

		LogUtil.DBLog(Log_TAG, " Inside logoutFromActivityBelowEjobForm() end ", ";Launch mainactivity");
		Intent intent = new Intent(context, GoodYear_eJob_MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
		activity.finish();
	}

}
