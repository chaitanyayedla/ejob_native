package com.goodyear.ejob.ui;

import android.app.DatePickerDialog;
import android.content.Context;

/**
 * Created by Rajesh J on 11/3/2014.
 */
public class SafeDatePickerDialog extends DatePickerDialog {

    private boolean isStopped = false;
    public SafeDatePickerDialog(Context context, OnDateSetListener callBack, int year, int monthOfYear,
                                int dayOfMonth) {
        super(context, callBack, year, monthOfYear, dayOfMonth);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isStopped = false;
    }

    @Override
    protected void onStop() {
        isStopped = true;
        super.onStop();
    }

    public boolean isStopped() {
        return isStopped;
    }
}
