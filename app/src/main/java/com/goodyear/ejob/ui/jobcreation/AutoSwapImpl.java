package com.goodyear.ejob.ui.jobcreation;

import java.util.ArrayList;
import java.util.UUID;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.TyreState;

/**
 * @author amitkumar.h
 * Class handling AutoLogical Swap while doing Dismount or TurnOnRim Operations
 * It is a Singleton Class getting called from Vehicle Skeleton 
 */
public class AutoSwapImpl {
	private Context mContext;
	private static AutoSwapImpl sDialogUtils;
	private DatabaseAdapter mDbHelper;

	private AutoSwapImpl(Context context) {
		mContext = context;
	}
	/**
	 * initializing the instance of AutoSwapImp Class
	 * @param context
	 * @return
	 */
	public static AutoSwapImpl getMyInstance(Context context) {
		if (sDialogUtils == null) {
			sDialogUtils = new AutoSwapImpl(context);
		}
		return sDialogUtils;
	}
	/**
	 * Swapping the tire details on doing Dismount or Turn On Rim and updating the corresponding JobItem table
	 * @param mBrandName
	 * @param mDesignDetails
	 * @param nsk1
	 * @param nsk2
	 * @param nsk3
	 * @param minNsk
	 * @param regroove
	 */
	public void callAutoSwapImplementation(String mBrandName,
			String mDesignDetails, String nsk1, String nsk2, String nsk3,
			String minNsk, String regroove) {

		if (!TextUtils.isEmpty(Constants.tempTyrePositionNew)
				&& !Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP)
				&& !Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP)
				&& checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)
				&& VehicleSkeletonFragment.getTireByPosition(
						Constants.tempTyrePositionNew).getTyreState() != TyreState.EMPTY
				&& !Constants.tempTyreSerial
						.equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
				&& !TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)) {
			if (checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)
					&& !Constants.tempTyreSerial
							.equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)) {

				Constants.tempTyrePositionNew = VehicleSkeletonFragment
						.getPosFromSerial(Constants.EDITED_SERIAL_NUMBER);
				if (Constants.tempTyrePositionNew != null
						&& Constants.SELECTED_TYRE != null
						&& !TextUtils.isEmpty(Constants.tempTyrePositionNew)) {
					VehicleSkeletonFragment.pos2 = Constants.tempTyrePositionNew;// B
					VehicleSkeletonFragment.pos1 = Constants.SELECTED_TYRE// A
							.getPosition();
					Tyre mSwappedTire = new Tyre();// B
					Tyre mAutoSwappedTire = VehicleSkeletonFragment// B
							.getTireByPosition(VehicleSkeletonFragment.pos2);

					mSwappedTire.setSerialNumber(Constants.SELECTED_TYRE
							.getSerialNumber());
					mSwappedTire.setAsp(Constants.SELECTED_TYRE.getAsp());
					mSwappedTire.setSize(Constants.SELECTED_TYRE.getSize());
					mSwappedTire.setDesign(Constants.SELECTED_TYRE.getDesign());
					mSwappedTire.setDesignDetails(Constants.SELECTED_TYRE
							.getDesignDetails());
					mSwappedTire.setNsk(Constants.SELECTED_TYRE.getNsk());
					mSwappedTire.setNsk2(Constants.SELECTED_TYRE.getNsk2());
					mSwappedTire.setNsk3(Constants.SELECTED_TYRE.getNsk3());
					mSwappedTire
							.setExternalID(mAutoSwappedTire.getExternalID());
					mSwappedTire.setBrandName(Constants.SELECTED_TYRE
							.getBrandName());
					// Fix: 406
					if (Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED || TextUtils.isEmpty(Constants.SELECTED_TYRE.getSapMaterialCodeID())) {
						mSwappedTire.setSapMaterialCodeID(null);
					} else {
						mSwappedTire
								.setSapMaterialCodeID(Constants.SELECTED_TYRE
										.getSapMaterialCodeID());
					}
					mSwappedTire.setActive(Constants.SELECTED_TYRE.getActive());
					mSwappedTire.setFromJobId(Constants.SELECTED_TYRE
							.getFromJobId());
					mSwappedTire.setOrignalNSK(Constants.SELECTED_TYRE
							.getOrignalNSK());
					mSwappedTire.setPressure(Constants.SELECTED_TYRE
							.getPressure());
					mSwappedTire.setRim(Constants.SELECTED_TYRE.getRim());
					mSwappedTire.setRimType(Constants.SELECTED_TYRE
							.getRimType());
					mSwappedTire.setSapCodeAXID(Constants.SELECTED_TYRE
							.getSapCodeAXID());
					mSwappedTire.setSapCodeTI(Constants.SELECTED_TYRE
							.getSapCodeTI());
					mSwappedTire.setSapCodeVeh(Constants.SELECTED_TYRE
							.getSapCodeVeh());
					// FIX:: 406
					mSwappedTire.setSapCodeWP(mAutoSwappedTire.getSapCodeWP());
					mSwappedTire.setSapContractNumberID(Constants.SELECTED_TYRE
							.getSapContractNumberID());
					// Fix: 406
					if (Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED) {
						mSwappedTire.setSapVenderCodeID(null);
					} else {
						mSwappedTire.setSapVenderCodeID(Constants.SELECTED_TYRE
								.getSapVenderCodeID());
					}
					mSwappedTire.setShared(Constants.SELECTED_TYRE.getShared());
					mSwappedTire.setVehicleJob(Constants.SELECTED_TYRE
							.getVehicleJob());
					mSwappedTire.setVehicleJob(Constants.SELECTED_TYRE
							.getVehicleJob());
					mSwappedTire.setStatus(Constants.SELECTED_TYRE.getStatus());
					mSwappedTire.setReGrooved(Constants.SELECTED_TYRE
							.isReGrooved());// Added regrooved
					mSwappedTire.setPosition(VehicleSkeletonFragment.pos2);
					// End B
					Constants.SELECTED_TYRE.setSerialNumber(mAutoSwappedTire
							.getSerialNumber());
					Constants.SELECTED_TYRE.setAsp(mAutoSwappedTire.getAsp());
					Constants.SELECTED_TYRE.setSize(mAutoSwappedTire.getSize());
					Constants.SELECTED_TYRE.setDesign(mAutoSwappedTire
							.getDesign());
					// Fix :: mAutoSwappedTire .getDesignDetails() Removed this
					// and used mDesignDetails
					Constants.SELECTED_TYRE.setDesignDetails(mDesignDetails);
					Constants.SELECTED_TYRE.setNsk(nsk1);
					Constants.SELECTED_TYRE.setNsk2(nsk2);
					Constants.SELECTED_TYRE.setNsk3(nsk3);
					Constants.SELECTED_TYRE
							.setExternalID(Constants.SELECTED_TYRE
									.getExternalID());
					// Fix :: mAutoSwappedTire .getBrandName() Removed this and
					// used mBrand
					Constants.SELECTED_TYRE.setBrandName(mBrandName);
					
					Constants.SELECTED_TYRE
							.setSapMaterialCodeID(mAutoSwappedTire
									.getSapMaterialCodeID());
					Constants.SELECTED_TYRE.setActive(mAutoSwappedTire
							.getActive());
					Constants.SELECTED_TYRE.setFromJobId(mAutoSwappedTire
							.getFromJobId());
					Constants.SELECTED_TYRE.setOrignalNSK(mAutoSwappedTire
							.getOrignalNSK());
					Constants.SELECTED_TYRE.setPressure(mAutoSwappedTire
							.getPressure());
					Constants.SELECTED_TYRE.setRim(mAutoSwappedTire.getRim());
					Constants.SELECTED_TYRE.setRimType(mAutoSwappedTire
							.getRimType());
					Constants.SELECTED_TYRE.setSapCodeAXID(mAutoSwappedTire
							.getSapCodeAXID());
					Constants.SELECTED_TYRE.setSapCodeTI(mAutoSwappedTire
							.getSapCodeTI());
					Constants.SELECTED_TYRE.setSapCodeVeh(mAutoSwappedTire
							.getSapCodeVeh());
					// FIX:: 406
					Constants.SELECTED_TYRE
							.setSapCodeWP(Constants.SELECTED_TYRE
									.getSapCodeWP());
					Constants.SELECTED_TYRE
							.setSapContractNumberID(mAutoSwappedTire
									.getSapContractNumberID());
					Constants.SELECTED_TYRE.setSapVenderCodeID(mAutoSwappedTire
							.getSapVenderCodeID());
					Constants.SELECTED_TYRE.setShared(mAutoSwappedTire
							.getShared());
					Constants.SELECTED_TYRE.setVehicleJob(mAutoSwappedTire
							.getVehicleJob());
					Constants.SELECTED_TYRE.setVehicleJob(mAutoSwappedTire
							.getVehicleJob());
					Constants.SELECTED_TYRE.setStatus(mAutoSwappedTire
							.getStatus());
					Constants.SELECTED_TYRE
							.setReGrooved(regroove == "True" ? true : false); // Added regrooved
					if (Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED) {
						mSwappedTire.setTyreState(TyreState.NON_MAINTAINED);
					} else {
						mSwappedTire.setTyreState(TyreState.PART_WORN);
					}
					Constants.SELECTED_TYRE
							.setPosition(VehicleSkeletonFragment.pos1);
					// Setting the B tirE into tire Info
					VehicleSkeletonFragment.tyreInfo.set(
							VehicleSkeletonFragment.tyreInfo
									.indexOf(mAutoSwappedTire), mSwappedTire);
					/**
					 * Updating Data for Auto-SwappedData Source tire //B
					 */
					try {
						int len = VehicleSkeletonFragment.mJobItemList.size();
						JobItem jobItem = new JobItem();
						int jobID = getJobItemIDForThisJobItem();
						jobItem.setActionType("3");
						jobItem.setAxleNumber("0");
						jobItem.setAxleServiceType("0");
						jobItem.setCasingRouteCode("");
						jobItem.setDamageId("");
						jobItem.setExternalId(String.valueOf(UUID.randomUUID()));
						jobItem.setGrooveNumber(null);
						jobItem.setJobId(String.valueOf(jobID));
						jobItem.setMinNSK(minNsk);
						jobItem.setNote("");
						jobItem.setNskOneAfter(nsk1);
						jobItem.setNskOneBefore("0");
						jobItem.setNskThreeAfter(nsk3);
						jobItem.setNskThreeBefore("0");
						jobItem.setNskTwoAfter(nsk2);
						jobItem.setNskTwoBefore("0");
						jobItem.setOperationID("");
						jobItem.setRecInflactionOrignal(mSwappedTire
								.getPressure());
						jobItem.setRegrooved(regroove);
						jobItem.setReGrooveNumber(null);
						jobItem.setRegrooveType("0");
						jobItem.setRemovalReasonId("");
						jobItem.setRepairCompany(null);
						jobItem.setRimType("0");
						jobItem.setSapCodeTilD("0");
						jobItem.setSequence(String.valueOf(len + 1));
						jobItem.setServiceCount("0");
						jobItem.setServiceID("0");
						jobItem.setSwapType("4");
						jobItem.setThreaddepth("0");
						jobItem.setTyreID(Constants.SELECTED_TYRE
								.getExternalID());
						jobItem.setWorkOrderNumber(null);
						jobItem.setSwapOriginalPosition(mSwappedTire
								.getPosition());
						jobItem.setPressure(String
								.valueOf(Constants.FORSWAP_BARVALUE));
						jobItem.setTorqueSettings(String
								.valueOf(Constants.RETORQUEVALUE_SAVED));
						jobItem.setRecInflactionDestination(String
								.valueOf(Constants.FORSWAP_BARVALUE));

						jobItem.setSerialNumber(Constants.SELECTED_TYRE
								.getSerialNumber());
						jobItem.setBrandName(mBrandName);
						jobItem.setFullDesignDetails(mDesignDetails);
						VehicleSkeletonFragment.mJobItemList.add(jobItem);

					} catch (Exception e) {
						e.printStackTrace();
					}
					/**
					 * Updating Data for Auto-SwappedData DES tire //A
					 */
					try {
						int len = VehicleSkeletonFragment.mJobItemList.size();
						JobItem jobItem = new JobItem();
						int jobID = getJobItemIDForThisJobItem();
						jobItem.setActionType("3");
						jobItem.setAxleNumber("0");
						jobItem.setAxleServiceType("0");
						jobItem.setCasingRouteCode("");
						jobItem.setDamageId("");
						jobItem.setExternalId(String.valueOf(UUID.randomUUID()));
						jobItem.setGrooveNumber(null);
						jobItem.setJobId(String.valueOf(jobID));
						jobItem.setMinNSK(mSwappedTire.getNsk());
						jobItem.setNote("");
						jobItem.setNskOneAfter(Constants.SELECTED_TYRE.getNsk());
						jobItem.setNskOneBefore("0");
						jobItem.setNskThreeAfter(Constants.SELECTED_TYRE
								.getNsk3());
						jobItem.setNskThreeBefore("0");
						jobItem.setNskTwoAfter(Constants.SELECTED_TYRE
								.getNsk2());
						jobItem.setNskTwoBefore("0");
						jobItem.setOperationID("");
						jobItem.setRecInflactionOrignal(Constants.SELECTED_TYRE
								.getPressure());
						jobItem.setRegrooved("False");
						jobItem.setReGrooveNumber(null);
						jobItem.setRegrooveType("0");
						jobItem.setRemovalReasonId("");
						jobItem.setRepairCompany(null);
						jobItem.setRimType("0");
						jobItem.setSapCodeTilD("0");
						jobItem.setSequence(String.valueOf(len + 1));
						jobItem.setServiceCount("0");
						jobItem.setServiceID("0");
						jobItem.setSwapType("4");
						jobItem.setThreaddepth("0");
						jobItem.setTyreID(mSwappedTire.getExternalID());
						jobItem.setWorkOrderNumber(null);
						jobItem.setSwapOriginalPosition(Constants.SELECTED_TYRE
								.getPosition());
						jobItem.setPressure(String
								.valueOf(Constants.FORSWAP_BARVALUE));
						jobItem.setTorqueSettings(String
								.valueOf(Constants.RETORQUEVALUE_SAVED));
						jobItem.setRecInflactionDestination(String
								.valueOf(Constants.FORSWAP_BARVALUE));
						jobItem.setSerialNumber(mSwappedTire.getSerialNumber());
						jobItem.setBrandName(mSwappedTire.getBrandName());
						jobItem.setFullDesignDetails(mSwappedTire
								.getDesignDetails());
						VehicleSkeletonFragment.mJobItemList.add(jobItem);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else if (!Constants.SELECTED_TYRE.getSerialNumber().equals(
				Constants.EDITED_SERIAL_NUMBER)) {
			Constants.SELECTED_TYRE
					.setSerialNumber(Constants.EDITED_SERIAL_NUMBER);
		}
	}
	/**
	 * @return
	 */
	private int getJobItemIDForThisJobItem() {
		int countJobItemsNotPresent = 0;
		mDbHelper = new DatabaseAdapter(mContext);
		mDbHelper.createDatabase();
		mDbHelper.open();
		int currentJobItemCountInDB = mDbHelper.getJobItemCount();
		Cursor cursor = null;
		try {
			cursor = mDbHelper.getJobItemValues();
			if (!CursorUtils.isValidCursor(cursor)) {
				return countJobItemsNotPresent + 1
						+ VehicleSkeletonFragment.mJobItemList.size();
			}
			for (JobItem jobItem : VehicleSkeletonFragment.mJobItemList) {
				boolean flag = false;
				for (boolean hasItem = cursor.moveToFirst(); hasItem; hasItem = cursor
						.moveToNext()) {
					if (jobItem.getExternalId().equals(
							cursor.getString(cursor
									.getColumnIndex("ExternalID")))) {
						countJobItemsNotPresent++;
						break;
					}
				}
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			mDbHelper.close();
			// CursorUtils.closeCursor(cursor);
		}
		return currentJobItemCountInDB - countJobItemsNotPresent
				+ VehicleSkeletonFragment.mJobItemList.size() +1;
	}
	
	/**
	 * Algorithm for Single Swap Implementation
	 * 
	 * @param pos1
	 * @param pos2
	 */
	public static void singleSwap(String pos1, String pos2) {
		String mNonMaintainedSerial = Constants.SECOND_SELECTED_TYRE
				.getSerialNumber();
		ArrayList<Tyre> tempTyreInfo = new ArrayList<Tyre>();
		int tireListCount = VehicleSkeletonFragment.tyreInfo.size();
		for (int i = 0; i < tireListCount; i++) {
			Tyre temp = VehicleSkeletonFragment.tyreInfo.get(i);
			if (Constants.onDragBool_LS == true
					|| Constants.onDragPSBool == true
					|| Constants.onDRAGReturnBoolPS == true) {
				if (temp.getPosition().startsWith(pos1)) {
					if (Constants.EDITED_SERIAL_NUMBER_SOURCE != null
							&& !Constants.EDITED_SERIAL_NUMBER_SOURCE
									.equals("")) {
						temp.setSerialNumber(Constants.EDITED_SERIAL_NUMBER_SOURCE);
					}
					temp.setPosition(pos2);
				} else if (temp.getPosition().startsWith(pos2)) {
					if (Constants.EDITED_SERIAL_NUMBER_DES != null
							&& !Constants.EDITED_SERIAL_NUMBER_DES.equals("")) {
						temp.setSerialNumber(Constants.EDITED_SERIAL_NUMBER_DES);
					}
					temp.setPosition(pos1);
				}
			} else {
				if (temp.getPosition().startsWith(pos1)) {
					if (Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED != null
							&& !Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED
									.equals("")) {
						temp.setSerialNumber(Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED);
					}

					temp.setPosition(pos2);
				} else if (temp.getPosition().startsWith(pos2)) {
					if (Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE != null
							&& !Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE
									.equals("")) {
						temp.setSerialNumber(Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE);
					}
					temp.setPosition(pos1);
				}
			}
			tempTyreInfo.add(temp);
		}
		VehicleSkeletonFragment.tyreInfo = tempTyreInfo;
		if ((Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED && Constants.SECOND_SELECTED_TYRE
				.getTyreState() != TyreState.NON_MAINTAINED)) {
			Constants.SECOND_SELECTED_TYRE
					.setSerialNumber(mNonMaintainedSerial);
		}
		// Exchanging SAPCODE-WP
		if (Constants.SELECTED_TYRE == null) {
			Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
		}
		if (Constants.SECOND_SELECTED_TYRE == null) {
			Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
		}
		String firstSAPwp = Constants.SELECTED_TYRE.getSapCodeWP();
		String secondSAPwp = Constants.SECOND_SELECTED_TYRE.getSapCodeWP();

		Constants.SELECTED_TYRE.setSapCodeWP(secondSAPwp);
		Constants.SECOND_SELECTED_TYRE.setSapCodeWP(firstSAPwp);
	}

	/**
	 * Algorithm for Double Logical Swap Implementation
	 * 
	 * @param pos1
	 * @param pos2
	 * @param pos3
	 */
	public static void doubleSwap(String pos1, String pos2, String pos3) {
		ArrayList<Tyre> tempTyreInfo = new ArrayList<Tyre>();
		int tireListCount = VehicleSkeletonFragment.tyreInfo.size();
		for (int i = 0; i < tireListCount; i++) {
			String tempPosition1 = pos1;
			String tempPosition2 = pos2;
			String tempPosition3 = pos3;

			Tyre temp = VehicleSkeletonFragment.tyreInfo.get(i);
			if (temp.getPosition().startsWith(pos1)) {

				temp.setPosition(tempPosition2);
			} else if (temp.getPosition().startsWith(pos2)) {
				temp.setPosition(tempPosition3);
			} else if (temp.getPosition().startsWith(pos3)) {
				temp.setPosition(tempPosition1);
			}
			tempTyreInfo.add(temp);
		}
		VehicleSkeletonFragment.tyreInfo = tempTyreInfo;

		try {
			String firstSAPwp = VehicleSkeletonFragment.getTireByPosition(pos1).getSapCodeWP();// A
			String secondSAPwp = VehicleSkeletonFragment.getTireByPosition(pos2).getSapCodeWP();// B
			String thirdSAPwp = VehicleSkeletonFragment.getTireByPosition(pos3).getSapCodeWP();// C

			VehicleSkeletonFragment.getTireByPosition(pos3).setSapCodeWP(firstSAPwp);// C
			VehicleSkeletonFragment.getTireByPosition(pos1).setSapCodeWP(secondSAPwp);// //A
			VehicleSkeletonFragment.getTireByPosition(pos2).setSapCodeWP(thirdSAPwp);// B
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void doubleSwapPhysical(String pos1, String pos2, String pos3) {
		ArrayList<Tyre> tempTyreInfo = new ArrayList<Tyre>();
		int tireListCount = VehicleSkeletonFragment.tyreInfo.size();
		for (int i = 0; i < tireListCount; i++) {
			String tempPosition1 = pos1;
			String tempPosition2 = pos2;
			String tempPosition3 = pos3;
			Tyre temp = VehicleSkeletonFragment.tyreInfo.get(i);
			if (temp.getPosition().startsWith(pos1)) {
				temp.setPosition(tempPosition3);// 3
			} else if (temp.getPosition().startsWith(pos2)) {
				temp.setPosition(tempPosition1);// 1
			} else if (temp.getPosition().startsWith(pos3)) {
				temp.setPosition(tempPosition2);
			}
			tempTyreInfo.add(temp);
		}
		VehicleSkeletonFragment.tyreInfo = tempTyreInfo;

		String firstSAPwp = VehicleSkeletonFragment.getTireByPosition(pos1).getSapCodeWP();// A
		String secondSAPwp = VehicleSkeletonFragment.getTireByPosition(pos2).getSapCodeWP();// B
		String thirdSAPwp = VehicleSkeletonFragment.getTireByPosition(pos3).getSapCodeWP();// C

		VehicleSkeletonFragment.getTireByPosition(pos2).setSapCodeWP(firstSAPwp);// B
		VehicleSkeletonFragment.getTireByPosition(pos3).setSapCodeWP(secondSAPwp);// C
		VehicleSkeletonFragment.getTireByPosition(pos1).setSapCodeWP(thirdSAPwp);// A
	}

	/**
	 * Algorithm for Double Logical Swap Implementation
	 * 
	 * @param pos1
	 * @param pos2
	 * @param pos3
	 * @param pos4
	 */
	public static void tripleSwap(String pos1, String pos2, String pos3, String pos4) {
		ArrayList<Tyre> tempTyreInfo = new ArrayList<Tyre>();
		for (int i = 0; i < VehicleSkeletonFragment.tyreInfo.size(); i++) {
			Tyre temp = VehicleSkeletonFragment.tyreInfo.get(i);
			if (temp.getPosition().startsWith(pos1)) {
				temp.setPosition(pos3);// 4
			} else if (temp.getPosition().startsWith(pos2)) {
				temp.setPosition(pos4);// 3
			} else if (temp.getPosition().startsWith(pos3)) {
				temp.setPosition(pos2);// 1
			} else if (temp.getPosition().startsWith(pos4)) {
				temp.setPosition(pos1);// 2
			}
			tempTyreInfo.add(temp);
		}
		VehicleSkeletonFragment.tyreInfo = tempTyreInfo;

		try {
			String firstSAPwp = VehicleSkeletonFragment.getTireByPosition(pos1).getSapCodeWP();// A
			String secondSAPwp = VehicleSkeletonFragment.getTireByPosition(pos2).getSapCodeWP();// B
			String thirdSAPwp = VehicleSkeletonFragment.getTireByPosition(pos3).getSapCodeWP();// C
			String fourthSAPwp = VehicleSkeletonFragment.getTireByPosition(pos4).getSapCodeWP();// C

			VehicleSkeletonFragment.getTireByPosition(pos4).setSapCodeWP(firstSAPwp);// D
			VehicleSkeletonFragment.getTireByPosition(pos3).setSapCodeWP(secondSAPwp);// C
			VehicleSkeletonFragment.getTireByPosition(pos1).setSapCodeWP(thirdSAPwp);// A
			VehicleSkeletonFragment.getTireByPosition(pos2).setSapCodeWP(fourthSAPwp);// B
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Returns true if Serial already there in Vehicle
	 * 
	 * @param userEnteredSerialNumber
	 * @return
	 */
	public static boolean checkMultipeSwap(String userEnteredSerialNumber) {
		boolean check = true;

		for (Tyre currentTire : VehicleSkeletonFragment.tyreInfo) {
			String currentTireSerial = currentTire.getSerialNumber();
			if (userEnteredSerialNumber.equals(currentTireSerial)) {
				check = false;
				break;
			} else {
				check = true;
			}
		}
		return !check;
	}

	/**
	 * Returns false if serial is not there in Vehicle(DB Update)
	 * 
	 * @param userEnteredSerialNumber
	 * @return
	 */
	public static Boolean checkSerialToUpdateDB(String userEnteredSerialNumber) {
		if (userEnteredSerialNumber == null) {
			return false;
		}
		boolean check = true;
		for (Tyre currentTire : VehicleSkeletonFragment.tyreInfo) {
			String currentTireSerial = currentTire.getSerialNumber();
			if (userEnteredSerialNumber.equals(currentTireSerial)) {
				check = false;
				break;
			} else {
				check = true;
			}
		}
		return !check;
	}
	/** Bug 581
	 * Method added to check whether the Swapped tire is having the same size details
	 * as per the selected tire
	 */
	public static boolean checkIfSizeAllowed(Tyre mTyre){
		boolean check = false;
		String size1 = Constants.SELECTED_TYRE
				.getSize();
		String size2 = Constants.SELECTED_TYRE
				.getAsp();
		String size3 = Constants.SELECTED_TYRE
				.getRim();
		if (TextUtils.isEmpty(size1)) {
			size1 = "0";
		}
		if (TextUtils.isEmpty(size2)) {
			size2 = "0";
		}
		if (TextUtils.isEmpty(size3)) {
			size3 = "0";
		}
		double mDimensionFirstTyre_1_1 = Double
				.valueOf(size1);
		double mDimensionFirstTyre_1_2 = Double
				.valueOf(size2);
		double mDimensionFirstTyre_1_3 = Double
				.valueOf(size3);

		double mDimensionSecondTyre_1_1 = Double
				.valueOf(mTyre.getSize()
						.toString());
		double mDimensionSecondTyre_1_2 = Double
				.valueOf(mTyre.getAsp()
						.toString());
		double mDimensionSecondTyre_1_3 = Double
				.valueOf(mTyre.getRim()
						.toString());

		if (mDimensionFirstTyre_1_1 == mDimensionSecondTyre_1_1
				&& mDimensionFirstTyre_1_2 == mDimensionSecondTyre_1_2
				&& mDimensionFirstTyre_1_3 == mDimensionSecondTyre_1_3) {
			return true;
		}else{
			return false;
		}
	}
}