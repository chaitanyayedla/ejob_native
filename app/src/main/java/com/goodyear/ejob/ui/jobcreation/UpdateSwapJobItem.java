package com.goodyear.ejob.ui.jobcreation;

import java.util.UUID;

import android.content.Context;

import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.InspectionDataHandler;
import com.goodyear.ejob.util.Tyre;

/**
 * @author amitkumar.h
 * 
 */
public class UpdateSwapJobItem {
	private Context mContext;
	private static UpdateSwapJobItem sDialogUtils;

	private UpdateSwapJobItem(Context context) {
		mContext = context;
	}

	public static UpdateSwapJobItem getMyInstance(Context context) {
		if (sDialogUtils == null) {
			sDialogUtils = new UpdateSwapJobItem(context);
		}
		return sDialogUtils;
	}

	public void updateJobItemForDoubleSwap(Tyre sTyre, String mSerialNumber,
			String mBrandName, String mDetailDesign, String mMinNSK,
			String mNSK1, String mNSK2, String mNSK3,int mjobID) {
		try {
			int len = VehicleSkeletonFragment.mJobItemList.size();
			JobItem jobItem = new JobItem();
			jobItem.setActionType("3");
			jobItem.setAxleNumber("0");
			jobItem.setAxleServiceType("0");
			jobItem.setCasingRouteCode("");
			jobItem.setDamageId("");
			jobItem.setExternalId(String.valueOf(UUID.randomUUID()));
			jobItem.setGrooveNumber(null);
			jobItem.setJobId(String.valueOf(mjobID));
			jobItem.setMinNSK(mMinNSK);
			jobItem.setNote("");
			jobItem.setNskOneAfter(mNSK1);
			jobItem.setNskOneBefore("0");
			jobItem.setNskThreeAfter(mNSK3);
			jobItem.setNskThreeBefore("0");
			jobItem.setNskTwoAfter(mNSK2);
			jobItem.setNskTwoBefore("0");
			jobItem.setOperationID("");
			jobItem.setRecInflactionOrignal(sTyre.getPressure());
			jobItem.setRegrooved("False");
			jobItem.setReGrooveNumber(null);
			jobItem.setRegrooveType("0");
			jobItem.setRemovalReasonId("");
			jobItem.setRepairCompany(null);
			jobItem.setRimType("1");
			jobItem.setSapCodeTilD("0");
			jobItem.setSequence("3");
			jobItem.setServiceCount("0");
			jobItem.setServiceID("0");
			jobItem.setSwapType("4");
			jobItem.setThreaddepth("0");
			jobItem.setTyreID(sTyre.getExternalID());
			jobItem.setWorkOrderNumber(null);
			jobItem.setSwapOriginalPosition(sTyre.getPosition());
			jobItem.setTorqueSettings(String
					.valueOf(Constants.RETORQUEVALUE_SAVED));
			jobItem.setRecInflactionDestination(String
					.valueOf(Constants.FORSWAP_BARVALUE));
			jobItem.setSerialNumber(sTyre.getSerialNumber());
			jobItem.setBrandName(mBrandName);
			jobItem.setFullDesignDetails(mDetailDesign);
			jobItem.setDoubleSwap(true);
			Constants.INSERT_SWAP_DATA++;

			VehicleSkeletonFragment.mJobItemList.add(jobItem);
			// Updating Tyre
			sTyre.setBrandName(mBrandName);
			sTyre.setDesignDetails(mDetailDesign);
			sTyre.setNsk(mNSK1);
			sTyre.setNsk2(mNSK2);
			sTyre.setNsk3(mNSK3);
            // Inspection CR:: 447
			//Condition aaded to check and update inspection data for the selected tire
			if(!sTyre.isInspected()) {
				InspectionDataHandler.getMyInstance(mContext).updateJobitemForInspection(sTyre,true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void updateJobItemForEmptyTire(Tyre sTyre, int mjobID) {
		try {
			int len = VehicleSkeletonFragment.mJobItemList.size();
			JobItem jobItem = new JobItem();
			jobItem.setActionType("3");
			jobItem.setAxleNumber("0");
			jobItem.setAxleServiceType("0");
			jobItem.setCasingRouteCode("");
			jobItem.setDamageId("");
			jobItem.setExternalId(String.valueOf(UUID.randomUUID()));
			jobItem.setGrooveNumber(null);
			if(sTyre == Constants.SELECTED_TYRE || Constants.DO_PHYSICAL_SWAP){
			jobItem.setJobId(String.valueOf(mjobID));
			}else{
				jobItem.setJobId(String.valueOf(mjobID+1));
			}
			jobItem.setMinNSK(sTyre.getMinNSK());
			jobItem.setNote("");
			jobItem.setNskOneAfter(sTyre.getNsk());
			jobItem.setNskOneBefore("0");
			jobItem.setNskThreeAfter(sTyre.getNsk3());
			jobItem.setNskThreeBefore("0");
			jobItem.setNskTwoAfter(sTyre.getNsk2());
			jobItem.setNskTwoBefore("0");
			jobItem.setOperationID("");
			jobItem.setRecInflactionOrignal(sTyre.getPressure());
			jobItem.setRegrooved("False");
			jobItem.setReGrooveNumber(null);
			jobItem.setRegrooveType("0");
			jobItem.setRemovalReasonId("");
			jobItem.setRepairCompany(null);
			jobItem.setRimType("1");
			jobItem.setSapCodeTilD("0");
			jobItem.setSequence("3");
			jobItem.setServiceCount("0");
			jobItem.setServiceID("0");
			if(Constants.DO_PHYSICAL_SWAP){
				jobItem.setSwapType("1");
			}else{
				jobItem.setSwapType("4");
			}
			jobItem.setThreaddepth("0");
			jobItem.setTyreID(sTyre.getExternalID());
			jobItem.setWorkOrderNumber(null);
			jobItem.setSwapOriginalPosition(sTyre.getPosition());
			jobItem.setTorqueSettings(String
					.valueOf(Constants.RETORQUEVALUE_SAVED));
			jobItem.setRecInflactionDestination(String
					.valueOf(Constants.FORSWAP_BARVALUE));
			jobItem.setSerialNumber(sTyre.getSerialNumber());
			jobItem.setBrandName(sTyre.getBrandName());
			jobItem.setFullDesignDetails(sTyre.getDesignDetails());
			VehicleSkeletonFragment.mJobItemList.add(jobItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}