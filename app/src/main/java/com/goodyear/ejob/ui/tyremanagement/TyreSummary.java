package com.goodyear.ejob.ui.tyremanagement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goodyear.ejob.CameraPreviewActivity;
import com.goodyear.ejob.GoodYear_eJob_MainActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.service.UserInactivityService;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.ExpandableListAdapter;
import com.goodyear.ejob.util.LogUtil;

/**
 * @author amitkumar.h
 */
public class TyreSummary extends Activity implements InactivityHandler {
	private TextView mVendorName;
	private TextView mCustomerName;
	private LinearLayout mSum_DetailResult;
	private String mSummaryVendorName;
	private String mSummaryCustomerName;
	private TextView mHeaderSerialNum;
	private TextView mHeaderDesign;
	private TextView mLblAction;
	private TextView mLblRepairCom;
	private TextView mLblWorkOrder;
	private TextView mLblOperation;
	private TextView mLblVendor;
	private TextView mLblContract;
	private TextView mLblJobRefNo;
	private TextView mJobReferenceNum;
	private String mSummaryJobRefNum;
	private static DatabaseAdapter mDbHelper;
	ArrayList<HashMap<String, String>> mListItems;
	LinearLayout mSum_ll_savedResultData;
	private HashMap<String, String> mHmap;
	private String mSummary_serial;
	private String mSummary_design;
	private String mSummary_operation;
	private String mSummary_repairCompany;
	private String mSummary_wordOrder;
	private String mSum_action;
	private ExpandableListView mSummaryListView;
	private ExpandableListAdapter mSummaryListAdapter;
	List<String> mListDataHeader;
	HashMap<String, List<String>> mListDataChild;
	HashMap<String, List<String>> mListDataChild1;
	ImageView mSummaryCameraView;
	public static Bitmap mCamaraImgFirst, mCamaraImgSecond, mCamaraImgThird;
	private String mActionString;
	private static Bitmap mCamView;
	private ArrayList<String> mExternalID = new ArrayList<String>();
	ImageView mCamImageInExpandable;
	private int mLastExpandedPosition = -1;
	private static String mDamageNote;
	private static ArrayList<String> mDamageNoteList;
	private LogoutHandler mLogoutHandler;
	Bundle savedInstanceState;
	Cursor mCursor;


	//User Trace logs trace tag
	private static final String TRACE_TAG = "Tyre Summary";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.savedInstanceState = savedInstanceState;
		setContentView(R.layout.tyre_summary);
		//User Trace logs
		LogUtil.TraceInfo(TRACE_TAG, "none", TRACE_TAG, false, false,true);
		try {
			mDbHelper = new DatabaseAdapter(this);
			mDbHelper.open();
			if (mDbHelper.getLabel(100).equalsIgnoreCase("")) {
				setTitle("Summary");
			} else {
				setTitle(mDbHelper.getLabel(100));
			}
		} catch (SQLException e3) {
			e3.printStackTrace();
		}
		mListDataHeader = new ArrayList<String>();
		mListDataChild = new HashMap<String, List<String>>();
		mListDataChild1 = new HashMap<String, List<String>>();
		mSummaryListView = (ExpandableListView) findViewById(R.id.summarylist);
		mSum_ll_savedResultData = (LinearLayout) findViewById(R.id.sum_ll_savedResultData);
		mHeaderDesign = (TextView) findViewById(R.id.sum_design_header);
		mHeaderSerialNum = (TextView) findViewById(R.id.sum_serial_header);
		mLblAction = (TextView) findViewById(R.id.sum_lbl_status);
		mLblRepairCom = (TextView) findViewById(R.id.sum_lbl_RepairCom);
		mLblWorkOrder = (TextView) findViewById(R.id.sum_lbl_OrderNum);
		mLblOperation = (TextView) findViewById(R.id.sum_lbl_Operation);
		mLblVendor = (TextView) findViewById(R.id.sum_lbl_VendoeName);
		mLblContract = (TextView) findViewById(R.id.sum_lbl_ContractName);
		mVendorName = (TextView) findViewById(R.id.sum_value_VendorName);
		mCustomerName = (TextView) findViewById(R.id.sum_value_contractName);
		mJobReferenceNum = (TextView) findViewById(R.id.sum_value_RefNumber);
		mSum_DetailResult = (LinearLayout) findViewById(R.id.sum_ll_detailResultData);
		loadLabelsFromDB();
		getSummary();
		mJobReferenceNum.setText(String.valueOf(Constants.JOBREFNUMBERVALUE));
		try {
			if (savedInstanceState != null) {
				mSummaryVendorName = savedInstanceState
						.getString("vendorNameSaved");
				mSummaryCustomerName = savedInstanceState
						.getString("customerNameSaved");
				mSummaryJobRefNum = savedInstanceState.getString("jobRefSaved");
				mVendorName.setText(mSummaryVendorName);
				mCustomerName.setText(mSummaryCustomerName);
				mJobReferenceNum.setText(mSummaryJobRefNum);
				mLastExpandedPosition = savedInstanceState
						.getInt("mLastExpandedPosition");
			} else {
				Bundle bundle = getIntent().getExtras();
				mSummaryVendorName = bundle.getString("vendorname");
				mSummaryCustomerName = bundle.getString("contractname");
				mVendorName.setText(mSummaryVendorName);
				mCustomerName.setText(mSummaryCustomerName);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		LogoutHandler.setCurrentActivity(this);
	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	/**
	 * Opening DB
	 */
	@Override
	protected void onResume() {
		super.onResume();
		mDbHelper.open();
	}

	/**
	 * closing DB
	 */
	@Override
	protected void onPause() {
		super.onPause();
		mDbHelper.close();
	}

	/**
	 * Loading Labels From DB
	 */
	private void loadLabelsFromDB() {
		try {
			mLblJobRefNo = (TextView) findViewById(R.id.sum_lbl_RefNumber);
			mLblJobRefNo.setText("# ");
			mHeaderDesign.setText("Design");
			String serialNum = "";
			if (mDbHelper.getLabel(418).equalsIgnoreCase("")) {
				serialNum = "Serial Number";
			} else {
				serialNum = mDbHelper.getLabel(418);
			}
			mHeaderSerialNum.setText(serialNum);
			String Action = "";
			if (mDbHelper.getLabel(145).equalsIgnoreCase("")) {
				Action = "Action";
			} else {
				Action = mDbHelper.getLabel(145);
				mLblAction.setText(Action);
			}
			String mRepairCompany = "";
			if (mDbHelper.getLabel(412).equalsIgnoreCase("")) {
				mRepairCompany = "Repair Company";
			} else {
				mRepairCompany = mDbHelper.getLabel(412);
				mLblRepairCom.setText(mRepairCompany);
			}
			String mWorkOrderNum = mDbHelper.getLabel(426);
			mLblWorkOrder.setText(mWorkOrderNum);
			String Operation = mDbHelper.getLabel(390);
			mLblOperation.setText(Operation);
			String vendor = mDbHelper.getLabel(24);
			mLblVendor.setText(vendor);
			String contract = mDbHelper.getLabel(27);
			mLblContract.setText(contract);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Storing data for Orientation Change
	 */
	@Override
	protected void onSaveInstanceState(Bundle newState) {
		super.onSaveInstanceState(newState);
		try {
			newState.putString("vendorNameSaved", mSummaryVendorName);
			newState.putString("customerNameSaved", mSummaryCustomerName);
			newState.putString("jobRefSaved", mJobReferenceNum.getText()
					.toString());
			newState.putInt("mLastExpandedPosition", mLastExpandedPosition);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Getting Data to display TM Summary
	 */
	public void getSummary() {
		int mTemp = 0;
		try {
			mListItems = new ArrayList<HashMap<String, String>>();
			final DatabaseAdapter mDbHelper = new DatabaseAdapter(
					TyreSummary.this);
			mDbHelper.createDatabase();
			mDbHelper.open();
			mCursor = mDbHelper
					.getFullSummaryDetails(Constants.JOBREFNUMBERVALUE);
			System.out.println("mCursor.getCount()--" + mCursor.getCount());
			if (CursorUtils.isValidCursor(mCursor)) {
				mTemp = mCursor.getCount();
				mCursor.moveToFirst();
				while (!mCursor.isAfterLast()) {
					mHmap = new HashMap<String, String>();
					mHmap.put("SerialNumber", mCursor.getString(mCursor
							.getColumnIndex("SerialNumber")));
					mSummary_serial = mHmap.put("SerialNumber", mCursor
							.getString(mCursor.getColumnIndex("SerialNumber")));
					mExternalID.add(mCursor.getString(mCursor
							.getColumnIndex("ExternalID")));
					Log.i("Sandeep", "externalID: " + mExternalID);
					mListDataHeader.add(mDbHelper.getLabel(418)
							+ ": \t\t \t\t\t\t\t\t  "
							+ mCursor.getString(mCursor
									.getColumnIndex("SerialNumber")));
					mHmap.put("FullTireDetails", mCursor.getString(mCursor
							.getColumnIndex("FullTireDetails")));
					mSummary_design = mHmap.put("FullTireDetails", mCursor
							.getString(mCursor
									.getColumnIndex("FullTireDetails")));
					mHmap.put("Description", mCursor.getString(mCursor
							.getColumnIndex("Description")));
					mSummary_operation = mHmap.put("Description", mCursor
							.getString(mCursor.getColumnIndex("Description")));
					mHmap.put("RepairCompany", mCursor.getString(mCursor
							.getColumnIndex("RepairCompany")));
					mSummary_repairCompany = mHmap
							.put("RepairCompany", mCursor.getString(mCursor
									.getColumnIndex("RepairCompany")));
					mHmap.put("WorkOrderNumber", mCursor.getString(mCursor
							.getColumnIndex("WorkOrderNumber")));
					mSummary_wordOrder = mHmap.put("WorkOrderNumber", mCursor
							.getString(mCursor
									.getColumnIndex("WorkOrderNumber")));
					mHmap.put("ActionType", mCursor.getString(mCursor
							.getColumnIndex("ActionType")));
					mSum_action = mHmap.put("ActionType", mCursor
							.getString(mCursor.getColumnIndex("ActionType")));

					String mActionStringDisplayLabel;

					if (mSum_action.equalsIgnoreCase("4")) {
						mActionString = "Re-Groove";
						mActionStringDisplayLabel = mDbHelper.getLabel(200);
					} else if (mSum_action.equalsIgnoreCase("18")) {
						mActionString = "Minor Repair";
						mActionStringDisplayLabel = mDbHelper.getLabel(256);
					} else if (mSum_action.equalsIgnoreCase("19")) {
						mActionString = "Major Repair";
						mActionStringDisplayLabel = mDbHelper.getLabel(257);
					} else {
						mActionString = mCursor.getString(
								mCursor.getColumnIndex("ActionType"))
								.toString();
						mActionStringDisplayLabel=mActionString;
					}
					List<String> top250 = new ArrayList<String>();
					List<String> top250values = new ArrayList<String>();
					top250.add(" " + mDbHelper.getLabel(68)
							+ ":   \t\t\t\t\t\t\t");
					top250values.add(mCursor.getString(mCursor
							.getColumnIndex("FullTireDetails")));
					top250.add(" " + mDbHelper.getLabel(336) + ":\t\t \t\t\t\t");
					top250values.add(mCursor.getString(mCursor
							.getColumnIndex("Description")));
					top250.add(" " + mDbHelper.getLabel(412) + ":\t\t\t");
					top250values.add(mCursor.getString(mCursor
							.getColumnIndex("RepairCompany")));
					top250.add(" " + mDbHelper.getLabel(427) + ":\t\t");
					top250values.add(mCursor.getString(mCursor
							.getColumnIndex("WorkOrderNumber")));

					String mOriginalStatus = mDbHelper.getLabel(542);
					top250.add(mOriginalStatus +":\t\t\t\t");
					top250values.add(mActionStringDisplayLabel);
					String mThreadDepthValue = mCursor.getString(mCursor
							.getColumnIndex("TreadTepth"));
					String mRimTypeValue = mCursor.getString(mCursor
							.getColumnIndex("RegrooveType"));
					if (mActionString.equalsIgnoreCase("Re-Groove")
							&& !mThreadDepthValue.equalsIgnoreCase("0.0")
							&& !mRimTypeValue.equalsIgnoreCase("0")) {
						int mValidateForRegroove = mCursor.getInt(mCursor
								.getColumnIndex("RegrooveType"));
						String mValidateForRegrooveValue = "";
						if (mValidateForRegroove == 1) {
							mValidateForRegrooveValue = "RIB";
							top250.add(" Regrove Type:\t\t\t\t");
							top250values.add(mValidateForRegrooveValue);
						} else if (mValidateForRegroove == 2) {
							mValidateForRegrooveValue = "BLOCK";
							top250.add(" Regrove Type:\t\t\t\t");
							top250values.add(mValidateForRegrooveValue);
						}
						if (String.valueOf(mCursor.getDouble(mCursor
								.getColumnIndex("TreadTepth"))) != "0.0") {
							top250.add(" Thread depth:\t\t\t\t");
							top250values.add(String.valueOf(mCursor
									.getDouble(mCursor
											.getColumnIndex("TreadTepth"))));
						}
					}
					mListDataChild.put(
							mDbHelper.getLabel(418)
									+ ": \t\t \t\t\t\t\t\t  "
									+ mCursor.getString(mCursor
											.getColumnIndex("SerialNumber")),
							top250);
					mListDataChild1.put(
							mDbHelper.getLabel(418)
									+ ": \t\t \t\t\t\t\t\t  "
									+ mCursor.getString(mCursor
											.getColumnIndex("SerialNumber")),
							top250values);
					mSummaryListView = (ExpandableListView) findViewById(R.id.summarylist);
					mSummaryListAdapter = new ExpandableListAdapter(this,
							mListDataHeader, mListDataChild, mListDataChild1);
					mSummaryListView.setAdapter(mSummaryListAdapter);
					mSummaryListAdapter.notifyDataSetChanged();
					mListItems.add(mHmap);
					mCursor.moveToNext();
					mSummaryListView
							.setOnGroupClickListener(new OnGroupClickListener() {
								@Override
								public boolean onGroupClick(
										ExpandableListView parent, View v,
										int groupPosition, long id) {
									return false;
								}
							});
					mSummaryListView
							.setOnGroupExpandListener(new OnGroupExpandListener() {

								@Override
								public void onGroupExpand(int groupPosition) {
									if (mLastExpandedPosition != -1
											&& groupPosition != mLastExpandedPosition) {
										mSummaryListView
												.collapseGroup(mLastExpandedPosition);
									}
									mLastExpandedPosition = groupPosition;
									Constants.SELECTEDGROUPPOSITION = groupPosition;
									mCursor = mDbHelper
											.getCameraImage(mExternalID
													.get(groupPosition));
									mCamaraImgFirst = null;
									mCamaraImgSecond = null;
									mCamaraImgThird = null;
									int mCount = 0;
									mDamageNoteList = new ArrayList<String>();
									if (CursorUtils.isValidCursor(mCursor)) {
										Constants.CAMIMAGEENABLE = true;
										mCursor.moveToFirst();
										while (!mCursor.isAfterLast()) {
											byte[] cameraImageView = mCursor.getBlob(mCursor
													.getColumnIndex("TyreImage"));
											mDamageNote = mCursor.getString(mCursor
													.getColumnIndex("Description"));
											mDamageNoteList.add(mDamageNote);
											mCamView = getPhoto(cameraImageView);
											if (mCamView != null) {
												if (mCount == 0) {
													mCamaraImgFirst = mCamView;
												} else if (mCount == 1) {
													mCamaraImgSecond = mCamView;
												} else if (mCount == 2) {
													mCamaraImgThird = mCamView;
												}
											}
											mCount++;
											mCursor.moveToNext();
										}
										CursorUtils.closeCursor(mCursor);
									} else {
										Constants.CAMIMAGEENABLE = false;
									}
								}
							});
					mSummaryListView
							.setOnGroupCollapseListener(new OnGroupCollapseListener() {
								@Override
								public void onGroupCollapse(int groupPosition) {
								}
							});
					mSummaryListView
							.setOnChildClickListener(new OnChildClickListener() {
								@Override
								public boolean onChildClick(
										ExpandableListView parent, View v,
										int mGroupPosition, int mChildPosition,
										long mId) {
									if (mChildPosition == 4) {
										int index = parent
												.getFlatListPosition(ExpandableListView
														.getPackedPositionForChild(
																mGroupPosition,
																mChildPosition));
										parent.setItemChecked(index, true);
										if (mCamView != null
												&& Constants.CAMERAIMAGECLICKENABLE) {
											Intent photoViewer = new Intent(
													TyreSummary.this,
													CameraPreviewActivity.class);
											Bundle bundle = new Bundle();
											ArrayList<String> arrDamageDetails = mDbHelper
													.getDamageAreaAndGroup(mExternalID
															.get(mGroupPosition));
											bundle.putStringArrayList(
													"bundleDamageDetails",
													arrDamageDetails);
											bundle.putStringArrayList(
													"bundleDamageNote",
													mDamageNoteList);
											photoViewer.putExtras(bundle);
											startActivity(photoViewer);
										}
									}
									return true;
								}
							});
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Loading image From Bitmap
	 * 
	 * @param image
	 * @return
	 */
	public static Bitmap getPhoto(byte[] image) {
		return BitmapFactory.decodeByteArray(image, 0, image.length);
	}
	/* (non-Javadoc)
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press", false, false, false);
		super.onBackPressed();
		if(mCamView != null){
			mCamView = null;
		}
		if(null != mDamageNoteList && mDamageNoteList.size() > 0){
		mDamageNote = "";
		mDamageNoteList.clear();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		Intent intent = new Intent(this, GoodYear_eJob_MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}
}