package com.goodyear.ejob.ui.tyremanagement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.goodyear.ejob.CameraActivity;
import com.goodyear.ejob.GoodYear_eJob_MainActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.DecimalDigitsInputFilter;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.TyreConstants;
import com.goodyear.ejob.util.TyreJobDataBean;
import com.goodyear.ejob.util.Validation;

/**
 * @author amitkumar.h
 * Class handling all the action performed on the selected tire 
 * User can select Work-Order number, Repair Company, operation performed and 
 * can take images in case of Damage/Scrap
 */
public class TyreOperationActivity extends Activity implements
		InactivityHandler {

	/**
	 * Initializing Activity Context
	 */
	final Context mContext = this;
	/**
	 * Variable Declaration
	 */
	private int mPos;
	private String mTyreDesign;
	private String mJobUUID;
	private String mJobItemUUID;
	private String mSelectedSerialNumber;
	private Bundle mBundle;
	private String mTyreStatus;
	private MenuItem mButtonSaved;
	private boolean mImageSavedIconState;
	private String mVendorName;
	private String mCustomerName;
	private String mSelectedDescription;
	private String mSelectedOperationCode;
	private String mSavedrepairCompanyName;
	private String mSavedWorkOrderNumber;
	private String mSavedCurrentStatus;
	private String mSavedThreadDepth;
	private String mRandomJobItemUUID;
	private String mTyreID;
	private String mOperationID;
	private int mSAPContractID;
	private int mSapVendorID;
	private static boolean isRadioBtnChecked = false;
	private String mReGrooveTypeValue = "";
	private String mREGROOVE_IS_REQUIRED = "";
	public static Bitmap mBitmapPhoto1, mBitmapPhoto2, mBitmapPhoto3;
	public static int mBitmapPhoto1Quality, mBitmapPhoto2Quality,
			mBitmapPhoto3Quality;
	TyreJobDataBean mTyre_one;
	private DatabaseAdapter mDbHelper;
	private Bundle mInstanceState;
	public String mRegroove_rib_label, mRegroove_block_label;
	private boolean mOrientationChanged = false;
	public static int sCOUNT_AFTER_IMAGE_BITMAP_ADDED = -1;
	private static String previousActionType = null;
	private ArrayList<String> mOperationList;
	private HashMap<String, String> mOperationCodeList;
	/**
	 * UI Elements Declaration
	 */
	private LinearLayout mLayout_regrooveOption;
	private TextView mLabelSerialNum;
	private TextView mTyre_design;
	private TextView mRegroveType;
	private EditText mThread_depth;
	private TextView mThreadDepth;
	private TextView mTyreSerialNum;
	private Spinner mSpn_ActionOnTyre;
	private EditText mRepairCompanyName;
	private EditText mWorkOrderNumber;
	private TextView mValue_currentstatus;
	private TextView mLbl_CurrentStatus;
	private TextView mLbl_RepairCompany;
	private TextView mLbl_WorkOrderNumber;
	private TextView mLabelDesign;
	private ImageView mBtn_camera_icon;
	private TextView mValue_jobref;
	private RadioGroup rgRegrooveType;
	private RadioButton mRegrooveType;
	private RadioButton mRegrooveTypeYes;
	private RadioButton mRegrooveTypeNo;
	private String mLblMandatoryDamageNotes = "Damage notes is mandatory";;
	private String mLblPhotoIsRequired = "A photo is required";
	private AlertDialog mBackAlertDialog;

	/**
	 * Trace Info Variables
	 */
	private static final String TRACE_INFO = "Tire Status";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tyre_operation_activity);
		LogUtil.TraceInfo(TRACE_INFO, "none", TRACE_INFO, false, false, false);
		mInstanceState = savedInstanceState;
		mDbHelper = new DatabaseAdapter(this);
		mDbHelper.open();
		mBtn_camera_icon = (ImageView) findViewById(R.id.btn_Camere_tm);
		mLabelSerialNum = (TextView) findViewById(R.id.lbl_serialNum);
		mValue_jobref = (TextView) findViewById(R.id.value_jobref);
		mValue_jobref.setText("# " + Constants.JOBREFNUMBERVALUE);
		mLabelDesign = (TextView) findViewById(R.id.lbl_Design);
		mTyreSerialNum = (TextView) findViewById(R.id.value_serialNum);
		mSpn_ActionOnTyre = (Spinner) findViewById(R.id.spinner_action);
		mRepairCompanyName = (EditText) findViewById(R.id.value_repairCompany);
		mWorkOrderNumber = (EditText) findViewById(R.id.value_workOrderNum);
		mRepairCompanyName.requestFocus();
		mRepairCompanyName
				.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						boolean handled = false;
						if (actionId == EditorInfo.IME_ACTION_NEXT) {
							mWorkOrderNumber.requestFocus();
							handled = true;
						}
						return handled;
					}
				});
		mThread_depth = (EditText) findViewById(R.id.value_ThreadDeapth);
		mRepairCompanyName
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(50) });
		mWorkOrderNumber
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(30) });
		mThread_depth
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mTyre_design = (TextView) findViewById(R.id.value_Design);
		mValue_currentstatus = (TextView) findViewById(R.id.value_currentstatus);
		mLayout_regrooveOption = (LinearLayout) findViewById(R.id.ll_regrooveOption);
		mLbl_RepairCompany = (TextView) findViewById(R.id.lbl_repairComapany);
		mLbl_CurrentStatus = (TextView) findViewById(R.id.lbl_currentStatus);
		mLbl_WorkOrderNumber = (TextView) findViewById(R.id.lbl_workOrderNum);
		mRegroveType = (TextView) findViewById(R.id.lbl_regrooveType);
		mThreadDepth = (TextView) findViewById(R.id.lbl_ThreadDeapth);
		mBundle = getIntent().getExtras();
		mSelectedSerialNumber = mBundle.getString("SearchResult");
		mTyreDesign = mBundle.getString("SearchResultDesign");
		mVendorName = mBundle.getString("VendorName");
		mCustomerName = mBundle.getString("CustomerName");
		rgRegrooveType = (RadioGroup) findViewById(R.id.rg_regRooveType);
		mRegrooveTypeYes = (RadioButton) findViewById(R.id.value_regRooveYES);
		mRegrooveTypeNo = (RadioButton) findViewById(R.id.value_regRooveNO);
		mPos = mBundle.getInt("pos");
		mThread_depth
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilter(3,
						2) });
		mThread_depth.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				String strEnteredVal = mThread_depth.getText().toString();
				if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
					Float num = CommonUtils.parseFloat(strEnteredVal);
					if (num <= 30) {
					} else {
						mThread_depth.setText("");
					}
				} else if (strEnteredVal.equals(".")) {
					mThread_depth.setText("");
				}
				if (mThread_depth.getText().toString().equalsIgnoreCase("")) {
					if (mButtonSaved != null) {
						mButtonSaved.setIcon(R.drawable.unsavejob_navigation);
						mButtonSaved.setEnabled(false);
					}
					if (mButtonSaved != null) {
						mButtonSaved.setIcon(R.drawable.unsavejob_navigation);
						mButtonSaved.setEnabled(false);
					}
				} else {
					if (mButtonSaved != null) {
						mButtonSaved.setIcon(R.drawable.savejob_navigation);
						mButtonSaved.setEnabled(true);
					}
					if (mButtonSaved != null) {
						mButtonSaved.setIcon(R.drawable.savejob_navigation);
						mButtonSaved.setEnabled(true);
					}
				}
			}

			@Override
			public void afterTextChanged(Editable arg0) {
				try {
					if (!Validation.verifyNSK(mThread_depth.getText()
							.toString())) {
					} else {

					}
				} catch (Exception e) {
					Log.v("State", e.getMessage());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}
		});
		if (savedInstanceState != null) {
			try {
				mOrientationChanged = true;
				getWindow()
						.setSoftInputMode(
								WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
				mSelectedSerialNumber = savedInstanceState
						.getString("serialNum");
				mTyreDesign = savedInstanceState.getString("design");
				mVendorName = savedInstanceState.getString("vendorName");
				mCustomerName = savedInstanceState.getString("customerName");
				mSapVendorID = savedInstanceState.getInt("sapVendorID");
				mSAPContractID = savedInstanceState.getInt("sAPContractID");
				mTyreID = savedInstanceState.getString("tyreID");
				mSavedrepairCompanyName = savedInstanceState
						.getString("repCompany");
				mSavedWorkOrderNumber = savedInstanceState
						.getString("workOrder");
				mSavedThreadDepth = savedInstanceState.getString("depth");
				mSavedCurrentStatus = savedInstanceState.getString("curStatus");
				if (mButtonSaved != null) {
					mImageSavedIconState = savedInstanceState
							.getBoolean("retainImageSavedIconState");
				}
				mTyreSerialNum.setText(mSelectedSerialNumber);
				mTyre_design.setText(mTyreDesign);
				mValue_currentstatus.setText(mSavedCurrentStatus);
				mRepairCompanyName.setText(mSavedrepairCompanyName);
				mWorkOrderNumber.setText(mSavedWorkOrderNumber);
				mThread_depth.setText(mSavedThreadDepth);
				getStatus();
				generateJobGUID();
				generateJobItemGUID();
				getOperationData();
				loadLabelsFromDB();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			mTyreSerialNum.setText(mSelectedSerialNumber);
			mTyre_design.setText(mTyreDesign);
			try {
				getStatus();
				generateJobGUID();
				generateJobItemGUID();
				try {
					loadLabelsFromDB();

				} catch (Exception e) {
					e.printStackTrace();
				}
				getOperationData();
				getSAPContractID();
				getSAPVendorID();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		restoreDialogState(savedInstanceState);
	}

	@Override
	protected void onStart() {
		super.onStart();
		LogoutHandler.setCurrentActivity(this);
	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	/**
	 * Opening DB
	 */
	@Override
	protected void onResume() {
		super.onResume();
		mDbHelper.open();
	}
	@Override
	public void onBackPressed() {
		if (!areAnyChangesMade()) {
			actionBackPressed();
		} else {
			LogUtil.TraceInfo(TRACE_INFO, "none", "Back Press", false, false, false);
			goBack();
		}
	}

	private boolean areAnyChangesMade() {
		return mRepairCompanyName != null
				&& mRepairCompanyName.getText().toString().isEmpty()
				&& mWorkOrderNumber != null
				&& mWorkOrderNumber.getText().toString().isEmpty()
				&& mSpn_ActionOnTyre != null
				&& mSpn_ActionOnTyre.getSelectedItemPosition() == 0;
	};

	/**
	 * Saving Dialog State
	 * 
	 * @param state
	 */
	private void saveDialogState(Bundle state) {
		state.putBoolean("mBackAlertDialog",
				(mBackAlertDialog != null && mBackAlertDialog.isShowing()));
	}

	/**
	 * Restoring and loading the Dialog State while 
	 * changing the orientation from 
	 * @param state
	 */
	private void restoreDialogState(Bundle state) {
		if (state != null) {
			if (state.getBoolean("mBackAlertDialog")) {
				actionBackPressed();
			}
		}
	}

	/**
	 * Overriding On-Destroy
	 * Closing the opened DB
	 * Clearing all the dialogs
	 */
	@Override
	protected void onDestroy() {
		try {
			if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
				mBackAlertDialog.dismiss();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			mDbHelper.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	/**
	 * Method called when user press the Back button 
	 */
	private void actionBackPressed() {
		try {
			LayoutInflater li = LayoutInflater.from(TyreOperationActivity.this);
			View promptsView = li.inflate(R.layout.dialog_logout_confirmation,
					null);

			final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
					TyreOperationActivity.this);
			alertDialogBuilder.setView(promptsView);

			// update user activity for dialog layout
			LinearLayout rootNode = (LinearLayout) promptsView
					.findViewById(R.id.layout_root);
			rootNode.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					InactivityUtils.updateActivityOfUser();
				}
			});

			TextView infoTv = (TextView) promptsView
					.findViewById(R.id.logout_msg);
			infoTv.setText(mDbHelper.getLabel(241));

			alertDialogBuilder.setCancelable(false).setPositiveButton(
					mDbHelper.getLabel(331),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// update user activity on button click
							alertDialogBuilder.updateInactivityForDialog();
							LogUtil.TraceInfo(TRACE_INFO, "Back Press - Dialog", "BT - Yes", false, false, false);
							goBack();
						}
					});
			alertDialogBuilder.setCancelable(false).setNegativeButton(
					mDbHelper.getLabel(332),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// update user activity on button click
							alertDialogBuilder.updateInactivityForDialog();
							LogUtil.TraceInfo(TRACE_INFO, "Back Press - Dialog", "BT - Cancel", false, false, false);
							dialog.cancel();
						}
					});
			mBackAlertDialog = alertDialogBuilder.create();
			mBackAlertDialog.show();
			mBackAlertDialog.setCanceledOnTouchOutside(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Clearing Variables on BACK press
	 */
	private void goBack() {
		TyreManagementActivity.resetParams();
		Constants.TM_CAMERA_ICON_CLICKED = 0;
		Constants.mBundleSerielNOVec.clear();
		Constants.mBundleDescOperationCodeVec.clear();
		Constants.mBundleOperationCodeVec.clear();
		Constants.mBundleFinalTyreStatusVec.clear();
		if (mBitmapPhoto1 != null)
			Constants.damageImage_TM_LIST_Bitmap1.remove(mBitmapPhoto1);
		if (mBitmapPhoto2 != null)
			Constants.damageImage_TM_LIST_Bitmap2.remove(mBitmapPhoto2);
		if (mBitmapPhoto3 != null)
			Constants.damageImage_TM_LIST_Bitmap3.remove(mBitmapPhoto3);
		if (Constants.DAMAGE_NOTES1 != "")
			Constants.damageNote_TM_LIST1.remove(Constants.DAMAGE_NOTES1);
		if (Constants.DAMAGE_NOTES2 != "")
			Constants.damageNote_TM_LIST2.remove(Constants.DAMAGE_NOTES2);
		if (Constants.DAMAGE_NOTES3 != "")
			Constants.damageNote_TM_LIST3.remove(Constants.DAMAGE_NOTES3);
		Constants.SELECTED_DAMAGE_TYPE_LIST
				.remove(Constants.SELECTED_DAMAGE_TYPE);
		Constants.SELECTED_DAMAGE_AREA_LSIT
				.remove(Constants.SELECTED_DAMAGE_AREA);
		Constants.DAMAGE_NOTES1 = "";
		Constants.DAMAGE_NOTES2 = "";
		Constants.DAMAGE_NOTES3 = "";
		this.finish();
	}

	/**
	 * Generating Random JOB ID
	 */
	private void generateJobItemGUID() {
		try {
			UUID uuid = UUID.randomUUID();
			mJobItemUUID = uuid.toString();
			UUID idTwo = UUID.randomUUID();
			mRandomJobItemUUID = String.valueOf(idTwo);
			System.out.println("Random UUID String = " + mJobItemUUID);
			System.out.println("UUID version       = " + uuid.version());
			System.out.println("UUID variant       = " + uuid.variant());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generating Random JOBITEM ID
	 */
	private void generateJobGUID() {
		try {
			UUID uuid = UUID.randomUUID();
			mJobUUID = uuid.toString();
			System.out.println("Random UUID String = " + mJobUUID);
			System.out.println("UUID version       = " + uuid.version());
			System.out.println("UUID variant       = " + uuid.variant());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Getting Labels From DB
	 */
	private void loadLabelsFromDB() {
		try {
			setTitle(mDbHelper.getLabel(Constants.TIRE_STATUS_LABEL));
			String CurrentStatus = mDbHelper.getLabel(402);// 402
			mLbl_CurrentStatus.setText(CurrentStatus);
			String Major_Repair = mDbHelper.getLabel(257);
			String Minor_Repair = mDbHelper.getLabel(256);
			String Lbl_Regroove = mDbHelper.getLabel(200);
			if (mInstanceState == null) {
				if (mTyreStatus.equals("5")) {
					Constants._mBundleActionTypeVec.add("4");
				} else if (mTyreStatus.equals("3")) {
					Constants._mBundleActionTypeVec.add("19");
				} else if (mTyreStatus.equals("4")) {
					Constants._mBundleActionTypeVec.add("18");
				}
			}
			if (mTyreStatus.equals("5")) {
				mValue_currentstatus.setText(Lbl_Regroove);
				mLayout_regrooveOption.setVisibility(LinearLayout.INVISIBLE);
			} else if (mTyreStatus.equals("3")) {
				mValue_currentstatus.setText(Major_Repair);
			} else if (mTyreStatus.equals("4")) {
				mValue_currentstatus.setText(Minor_Repair);
			}
			mREGROOVE_IS_REQUIRED = mDbHelper
					.getLabel(Constants.REGROOVE_TYPE_IS_REQUIRED);
			String serialNum = mDbHelper.getLabel(418);// 418
			mLabelSerialNum.setText(serialNum);
			String RepairCompany = mDbHelper.getLabel(412);// 412
			mLbl_RepairCompany.setText(RepairCompany);
			String WorkOrderNum = mDbHelper.getLabel(426); // 426
			mLbl_WorkOrderNumber.setText(WorkOrderNum);
			String Regrove_Type = mDbHelper.getLabel(128);
			mRegroveType.setText(Regrove_Type + "*");
			String Depth = mDbHelper.getLabel(346);
			mThreadDepth.setText(Depth + "*");
			String EnterName = mDbHelper.getLabel(406);// 406
			mRepairCompanyName.setHint(EnterName);
			String EnterNum = mDbHelper.getLabel(407);
			mWorkOrderNumber.setHint(EnterNum);
			String EnterDepth = mDbHelper.getLabel(408);// 408
			mThread_depth.setHint(EnterDepth + "*");
			mRegroove_rib_label = mDbHelper
					.getLabel(Constants.REGROOVE_RIB_LABEL);
			mRegrooveTypeYes.setText(mRegroove_rib_label);
			mRegroove_block_label = mDbHelper
					.getLabel(Constants.REGROOVE_BLOCK_LABEL);
			mRegrooveTypeNo.setText(mRegroove_block_label);
			String design = mDbHelper.getLabel(68);
			mLabelDesign.setText(design);
			mLblMandatoryDamageNotes = mDbHelper
					.getLabel(Constants.MANDATORY_DAMAGE_NOTES);
			mLblPhotoIsRequired = mDbHelper
					.getLabel(Constants.PHOTO_IS_REQUIRED);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Getting status of the Tyre Selected
	 */
	public void getStatus() {
		Cursor mCursor = null;
		try {
			mCursor = mDbHelper.getTyreStatus(mSelectedSerialNumber);
			if (CursorUtils.isValidCursor(mCursor)) {
				mTyreStatus = mCursor.getString(mCursor
						.getColumnIndex("Status"));
				TyreConstants.TyreStatus = mTyreStatus;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Getting Tire ID by Serial Number
	 */
	public void getTyreExternalID() {
		Cursor mCursor = null;
		try {
			mCursor = mDbHelper.getTyreID(mSelectedSerialNumber);
			if (CursorUtils.isValidCursor(mCursor)) {
				mTyreID = mCursor.getString(mCursor
						.getColumnIndex("ExternalID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Getting Operation Id By Description
	 */
	public void getTyreOperationID() {
		Cursor mCursor = null;
		try {
			mCursor = mDbHelper.getTyreOperationID(mSelectedDescription);
			if (CursorUtils.isValidCursor(mCursor)) {
				mOperationID = mCursor.getString(mCursor
						.getColumnIndex("ExternalID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Getting Customer ID By Serial Number
	 */
	public void getSAPContractID() {
		Cursor mCursor = null;
		try {
			mCursor = mDbHelper.getTyreSapContractID(mSelectedSerialNumber);
			if (CursorUtils.isValidCursor(mCursor)) {
				mSAPContractID = mCursor.getInt(mCursor
						.getColumnIndex("SAPContractNumberID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Getting vendor ID by Serial Number
	 */
	public void getSAPVendorID() {
		Cursor mCursor = null;
		try {
			mCursor = mDbHelper.getTyreSAPVendorID(mSelectedSerialNumber);
			if (CursorUtils.isValidCursor(mCursor)) {
				mSapVendorID = mCursor.getInt(mCursor
						.getColumnIndex("SAPVendorCodeID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Function to load the spinner data from SQLite database
	 * */
	public void getOperationData() {
		try {
			mOperationList = new ArrayList<String>();
			mOperationCodeList = new HashMap<String, String>();
			Cursor mCursor = mDbHelper.getTyreOperation();
			if (CursorUtils.isValidCursor(mCursor)) {
				for (int i = 0; i < mCursor.getCount(); i++) {
					if (mCursor.moveToPosition(i)) {
						mOperationList.add(mCursor.getString(mCursor
								.getColumnIndex("Description")));
						mOperationCodeList
								.put(mCursor.getString(mCursor
										.getColumnIndex("Description")),
										mCursor.getString(mCursor
												.getColumnIndex("OperationGroup"))
												+ mCursor.getString(mCursor
														.getColumnIndex("OperationCode")));
					}
				}
				CursorUtils.closeCursor(mCursor);
			}
			Collections.sort(mOperationList);
			String mSelectAnAction = mDbHelper.getLabel(171);
			mOperationList.add(0, "-"+mSelectAnAction+"*-");
			mOperationCodeList.put("-"+mSelectAnAction+"*-", "-1");

			ArrayAdapter<String> OperationAdapter = new ArrayAdapter<String>(
					this, android.R.layout.simple_spinner_item, mOperationList);
			OperationAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpn_ActionOnTyre.setAdapter(OperationAdapter);
			mSpn_ActionOnTyre
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							resetCamaraImages();
							mSelectedDescription = mSpn_ActionOnTyre
									.getSelectedItem().toString();
							mSelectedOperationCode = mOperationCodeList
									.get(mSpn_ActionOnTyre.getSelectedItem()
											.toString());
							mBtn_camera_icon.setVisibility(View.VISIBLE);
							if (mSpn_ActionOnTyre.getSelectedItemPosition() == 0) {
								mBtn_camera_icon.setEnabled(false);
								mBtn_camera_icon
										.setImageResource(R.drawable.camera_grey_icon);
								mBtn_camera_icon.setVisibility(View.GONE);
								Constants.CHEKIFPICTAKENORNOT = false;
								if (mButtonSaved != null) {
									mButtonSaved
											.setIcon(R.drawable.unsavejob_navigation);
									mButtonSaved.setEnabled(false);
								}
							} else if (mSelectedOperationCode
									.equalsIgnoreCase("1D")
									|| mSelectedOperationCode
											.equalsIgnoreCase("2D")
									|| mSelectedOperationCode
											.equalsIgnoreCase("3NRR")) {
								mBtn_camera_icon.setEnabled(true);
								mBtn_camera_icon
										.setImageResource(R.drawable.camera_green_icon);
								Constants.CHEKIFPICTAKENORNOT = true;
								Constants.ISBREAKSPOT = false;
								if (mButtonSaved != null) {
									mButtonSaved
											.setIcon(R.drawable.savejob_navigation);
									mButtonSaved.setEnabled(true);
								}
							} else {
								Constants.ISBREAKSPOT = false;
								mBtn_camera_icon.setEnabled(false);
								mBtn_camera_icon
										.setImageResource(R.drawable.camera_grey_icon);
								mBtn_camera_icon.setVisibility(View.GONE);
								Constants.CHEKIFPICTAKENORNOT = false;
								if (mButtonSaved != null) {
									mButtonSaved
											.setIcon(R.drawable.savejob_navigation);
									mButtonSaved.setEnabled(true);
								}
							}
							if (mValue_currentstatus.getText().toString()
									.equalsIgnoreCase("Re-Groove")
									&& mSelectedOperationCode
											.equalsIgnoreCase("3RTMW")
									|| mSelectedOperationCode
											.equalsIgnoreCase("3TBMA")
									|| mSelectedOperationCode
											.equalsIgnoreCase("3TBMI")
									|| mSelectedOperationCode
											.equalsIgnoreCase("3REUS")
									|| mSelectedOperationCode
											.equalsIgnoreCase("1TBRT")
									|| mSelectedOperationCode
											.equalsIgnoreCase("2TBRT")
									|| mSelectedOperationCode
											.equalsIgnoreCase("3NRR")
									|| mSelectedOperationCode
											.equalsIgnoreCase("-1")) {
								mLayout_regrooveOption
										.setVisibility(LinearLayout.INVISIBLE);
								Constants.REGROOVETYPE = true;
							} else if (mValue_currentstatus.getText()
									.toString().equalsIgnoreCase("Re-Groove")
									&& mSelectedOperationCode
											.equalsIgnoreCase("3BGVR")
									|| mSelectedOperationCode
											.equalsIgnoreCase("3BMAR")
									|| mSelectedOperationCode
											.equalsIgnoreCase("3D")) {
								mBtn_camera_icon.setEnabled(false);
								mBtn_camera_icon
										.setImageResource(R.drawable.camera_grey_icon);
								mBtn_camera_icon.setVisibility(View.GONE);
								Constants.CHEKIFPICTAKENORNOT = false;
								Constants.CURRENT_STATUS_TM = mTyreStatus;
								mLayout_regrooveOption
										.setVisibility(LinearLayout.VISIBLE);
								Constants.REGROOVETYPE = false;
							} else if (mValue_currentstatus.getText()
									.toString().equalsIgnoreCase("Re-Groove")
									&& (mSelectedOperationCode
											.equalsIgnoreCase("1D")
											|| mSelectedOperationCode
													.equalsIgnoreCase("2D") || mSelectedOperationCode
												.equalsIgnoreCase("3NRR"))) {
								mBtn_camera_icon.setEnabled(true);
								mBtn_camera_icon
										.setImageResource(R.drawable.camera_green_icon);
								Constants.CHEKIFPICTAKENORNOT = true;
								Constants.CURRENT_STATUS_TM = mTyreStatus;
								mLayout_regrooveOption
										.setVisibility(LinearLayout.VISIBLE);
								Constants.ISBREAKSPOT = false;
							}
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loading Camera Activity for Damage Type
	 * 
	 * @param view
	 */
	public void loadCameraActivity(View view) {
		Constants.CHEKIFPICTAKENORNOT = true;
		Intent loadCamera = new Intent(this, CameraActivity.class);
		loadCamera.putExtra("DAMAGE_MANDATORY", Constants.CHEKIFPICTAKENORNOT);
		loadCamera.putExtra("FROM_TMT", true);
		loadCamera.putExtra("CURNT_IMAGE_COUNT", getImageCount());
		if (Constants.TM_CAMERA_ICON_CLICKED > 0) {
			Constants.TM_CAMERA_ICON_CLICKED_FLAG = true;
		}
		Constants.TM_CAMERA_ICON_CLICKED++;
		startActivity(loadCamera);
	}

	/**
	 * Creating Menu bar
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		MenuItem action_settings = menu.findItem(R.id.action_settings);
		action_settings.setVisible(false);
		MenuItem action_about = menu.findItem(R.id.action_about);
		action_about.setVisible(false);
		MenuItem action_help = menu.findItem(R.id.action_help);
		action_help.setVisible(false);
		MenuItem action_logout = menu.findItem(R.id.action_logout);
		action_logout.setVisible(false);
		MenuItem action_summary = menu.findItem(R.id.action_summary);
		action_summary.setVisible(false);
		return true;
	}

	/**
	 * Creating Menu Item on Action Bar
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.tyreoperation_menu, menu);
		mButtonSaved = menu.findItem(R.id.action_save);
		mButtonSaved.setIcon(R.drawable.unsavejob_navigation);
		mButtonSaved.setEnabled(false);
		mButtonSaved.setEnabled(mImageSavedIconState);
		if (mImageSavedIconState) {
			mButtonSaved.setIcon(R.drawable.savejob_navigation);
		} else {
			mButtonSaved.setIcon(R.drawable.unsavejob_navigation);
		}
		return true;
	}

	/**
	 * Action On Menu Item
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			LogUtil.TraceInfo(TRACE_INFO, "Option", "Settings", false, true, false);
			return true;
		} else if (id == R.id.action_save) {
			LogUtil.TraceInfo(TRACE_INFO, "Option", "Save", false, true, false);
			try {
				try {
					getTyreExternalID();
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					getTyreOperationID();
				} catch (Exception e) {
					e.printStackTrace();
				}
				int checkedRadioButtonId = rgRegrooveType
						.getCheckedRadioButtonId();
				if (mTyreStatus.equalsIgnoreCase("5")
						&& !(mSelectedOperationCode.equalsIgnoreCase("3NRR"))) {
					Constants.SERIAL_NUMBER_TM = mSelectedSerialNumber;
					Constants.IsREGROOVED_TM = true;
					if (checkedRadioButtonId != -1) {
						mRegrooveType = (RadioButton) findViewById(checkedRadioButtonId);
						mReGrooveTypeValue = mRegrooveType.getText().toString()
								.trim();
						if (mReGrooveTypeValue.equals(mRegroove_rib_label)) {
							isRadioBtnChecked = true;
							Constants.REGROOVE_TYPE_TM.add(1);
							String thread_depth_str = mThread_depth.getText()
									.toString();
							if (thread_depth_str != null) {
								if (!thread_depth_str.equals("")) {
									Constants.THREAD_DEPTH_TM.add(CommonUtils
											.parseDouble(thread_depth_str));
								} else {
									Constants.THREAD_DEPTH_TM.add(0.0);
								}
							} else {
								Constants.THREAD_DEPTH_TM.add(0.0);
							}
						} else if (mReGrooveTypeValue
								.equals(mRegroove_block_label)) {
							isRadioBtnChecked = true;
							Constants.REGROOVE_TYPE_TM.add(2);
							Constants.THREAD_DEPTH_TM.add(CommonUtils
									.parseDouble(mThread_depth.getText()
											.toString()));
						} else {
							Constants.REGROOVE_TYPE_TM.add(0);
						}
					}
				} else {
					Constants.REGROOVE_TYPE_TM.add(0);
					Constants.THREAD_DEPTH_TM.add(0.0);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			if (mSelectedDescription.equals("-Select An Action*-")) {
				LogUtil.TraceInfo(TRACE_INFO, "none", "Msg : "+mDbHelper.getLabel(172), false, false, false);
				Toast.makeText(this, mDbHelper.getLabel(172),
						Toast.LENGTH_SHORT).show();
			} else if (mTyreStatus.equals("5")
					&& Constants.REGROOVETYPE == false && !isRadioBtnChecked) {
				LogUtil.TraceInfo(TRACE_INFO, "none", "Msg : "+mREGROOVE_IS_REQUIRED, false, false, false);
				Toast.makeText(this, mREGROOVE_IS_REQUIRED, Toast.LENGTH_SHORT)
						.show();
			} else if (mTyreStatus.equals("5")
					&& Constants.REGROOVETYPE == false
					&& mThread_depth.getText().toString().equals("")) {
				LogUtil.TraceInfo(TRACE_INFO, "none", "Msg : "+mDbHelper.getLabel(408), false, false, false);
				Toast.makeText(this, mDbHelper.getLabel(408),
						Toast.LENGTH_SHORT).show();
			} else if (Constants.CHEKIFPICTAKENORNOT
					&& (Constants.SELECTED_DAMAGE_AREA_LSIT == null || Constants.SELECTED_DAMAGE_TYPE_LIST == null)) {
				LogUtil.TraceInfo(TRACE_INFO, "none", "Msg : "+mLblMandatoryDamageNotes, false, false, false);
				Toast.makeText(this, mLblMandatoryDamageNotes,
						Toast.LENGTH_SHORT).show();
			} else if (Constants.CHEKIFPICTAKENORNOT && mBitmapPhoto1 == null
					&& mBitmapPhoto2 == null && mBitmapPhoto3 == null) {
				LogUtil.TraceInfo(TRACE_INFO, "none", "Msg : "+mLblPhotoIsRequired, false, false, false);
				Toast.makeText(this, mLblPhotoIsRequired, Toast.LENGTH_SHORT)
						.show();

			} else {
				try {
					Intent in = new Intent();
					Constants.FROM_TM_OPERATION = true;
					in.putExtra("firstJobData", mTyre_one);
					in.putExtra("TyreSerialNum", mSelectedSerialNumber);
					in.putExtra("TyreDesign", mTyreDesign);
					in.putExtra("jobUUID", mJobUUID);
					in.putExtra("RandomJobItemUUID", mRandomJobItemUUID);
					in.putExtra("TyreID", mTyreID);
					in.putExtra("OperationID", mOperationID);
					in.putExtra("SAPContractID", mSAPContractID);
					in.putExtra("SAPVendorID", mSapVendorID);
					in.putExtra("TyreVendorName", mVendorName);
					in.putExtra("TyreCustomerName", mCustomerName);
					in.putExtra("jobItemUUID", mJobItemUUID);
					in.putExtra("SelectedDescription", mSelectedDescription);// SelectedDescription
					in.putExtra("currentStatus", mValue_currentstatus.getText()
							.toString());
					in.putExtra("RepairCompany", mRepairCompanyName.getText()
							.toString());
					in.putExtra("WorkOrderNumber", mWorkOrderNumber.getText()
							.toString());
					in.putExtra("ThreadDepth", mThread_depth.getText()
							.toString());
					in.putExtra("actionstatus", "true");
					in.putExtra("pos", mPos);

					//User Trace logs
					try {
						String traceData;
						traceData = mSelectedSerialNumber + " | "
						+ Constants.JOBREFNUMBERVALUE + " | "
						+ mTyreDesign + " | "
						+ mValue_currentstatus.getText().toString() + " | "
						+ mRepairCompanyName.getText().toString() + " | "
						+ mWorkOrderNumber.getText().toString() + " | "
						+ mSpn_ActionOnTyre.getSelectedItem();

						LogUtil.TraceInfo(TRACE_INFO, "Save - BT","Data : " + traceData, false, false, false);
					}
					catch (Exception e)
					{
						LogUtil.TraceInfo(TRACE_INFO, "Save - BT : Exception : ", e.getMessage(), false, true, false);
					}

					setResult(1, in);
					mLayout_regrooveOption
							.setVisibility(LinearLayout.INVISIBLE);
					Constants.TM_CAMERA_ICON_CLICKED = 0;
					Constants.TM_CAMERA_ICON_CLICKED_FLAG = false;
					TyreManagementActivity.resetParams();
					Constants.CheckStatus = true;
					finish();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Hiding keyBaord
	 * 
	 * @param view
	 */
	protected void hideKeyboard(View view) {
		try {
			InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			in.hideSoftInputFromWindow(view.getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Storing data for Orientation Change
	 */
	@Override
	protected void onSaveInstanceState(Bundle newState) {
		super.onSaveInstanceState(newState);
		try {
			newState.putString("serialNum", mSelectedSerialNumber);
			newState.putString("design", mTyreDesign);
			newState.putString("curStatus", mValue_currentstatus.getText()
					.toString());
			newState.putString("insJobUUID", mJobUUID);
			newState.putString("insJobItemUUID", mJobItemUUID);
			newState.putString("repCompany", mRepairCompanyName.getText()
					.toString());
			newState.putString("workOrder", mWorkOrderNumber.getText()
					.toString());
			newState.putString("depth", mThread_depth.getText().toString());
			newState.putString("repCompany", mRepairCompanyName.getText()
					.toString());
			newState.putString("customerName", mCustomerName);
			newState.putString("vendorName", mVendorName);
			newState.putInt("sapVendorID", mSapVendorID);
			newState.putInt("sAPContractID", mSAPContractID);
			newState.putString("tyreID", mTyreID);
			newState.putBoolean("retainImageSavedIconState",
					mButtonSaved.isEnabled());
		} catch (Exception e) {
			e.printStackTrace();
		}
		saveDialogState(newState);
	}

	/**
	 * Clearing BitMap Images
	 */
	private void resetCamaraImages() {
		String curntSelection = mSpn_ActionOnTyre.getSelectedItem().toString();
		if (previousActionType == null) {
			previousActionType = mSpn_ActionOnTyre.getSelectedItem().toString();
		} else if (!curntSelection.equals(previousActionType)
				&& !mOrientationChanged) {
			if (mBitmapPhoto1 != null)
				Constants.damageImage_TM_LIST_Bitmap1.remove(mBitmapPhoto1);
			if (mBitmapPhoto2 != null)
				Constants.damageImage_TM_LIST_Bitmap2.remove(mBitmapPhoto2);
			if (mBitmapPhoto3 != null)
				Constants.damageImage_TM_LIST_Bitmap3.remove(mBitmapPhoto3);
			if (Constants.DAMAGE_NOTES1 != "")
				Constants.damageNote_TM_LIST1.remove(Constants.DAMAGE_NOTES1);
			if (Constants.DAMAGE_NOTES2 != "")
				Constants.damageNote_TM_LIST2.remove(Constants.DAMAGE_NOTES2);
			if (Constants.DAMAGE_NOTES3 != "")
				Constants.damageNote_TM_LIST3.remove(Constants.DAMAGE_NOTES3);
			Constants.SELECTED_DAMAGE_TYPE_LIST
					.remove(Constants.SELECTED_DAMAGE_TYPE);
			Constants.SELECTED_DAMAGE_AREA_LSIT
					.remove(Constants.SELECTED_DAMAGE_AREA);
			Constants.SELECTED_DAMAGE_TYPE = null;
			Constants.SELECTED_DAMAGE_AREA = null;
			Constants.DAMAGE_NOTES1 = "";
			Constants.DAMAGE_NOTES2 = "";
			Constants.DAMAGE_NOTES3 = "";
			mBitmapPhoto1 = null;
			mBitmapPhoto2 = null;
			mBitmapPhoto3 = null;
			if (mThread_depth != null) {
				mThread_depth.setText("");
			}
		}
	}

	/**
	 * Fetching image Count
	 * 
	 * @return
	 */
	private int getImageCount() {
		int curntImgCount = 0;
		if (null != mBitmapPhoto1) {
			curntImgCount++;
		}
		if (null != mBitmapPhoto2) {
			curntImgCount++;
		}
		if (null != mBitmapPhoto3) {
			curntImgCount++;
		}
		return curntImgCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		Intent intent = new Intent(this, GoodYear_eJob_MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}
}