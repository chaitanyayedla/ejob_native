package com.goodyear.ejob.ui.tyremanagement;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.goodyear.ejob.EjobList;
import com.goodyear.ejob.R;
import com.goodyear.ejob.adapter.JobList_Adapter;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.ui.SafeDatePickerDialog;
import com.goodyear.ejob.ui.SafeTimePickerDialog;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.TyreConstants;
import com.goodyear.ejob.util.Validation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TimeZone;
import java.util.UUID;

public class TyreManagementActivity extends Activity implements
		InactivityHandler {

	/**
	 * Adapter class object Declaration
	 */
	private JobList_Adapter mAdapter;
	private MyListAdapter mAdapterTyreActionSaved;
	/**
	 * Declaring variables
	 */
	private int mDamegeCounter;
	private String mBundleJobItemUUID;
	private String mBundleSerialNum;
	private String mBundleDesign;
	private String mBundleTyreVendorName;
	private String mBundleTyreCustomerName;
	private String mBundleactionstatus;
	private Context context;
	private DatabaseAdapter mDbHelper;
	private String mRepairCompanyName;
	private int mSelectedCustomerNamePos;
	private String mSelectedCustomerName;
	private String mSelectedVendorName;
	public String mCustomerID;
	private String mVendorCountryCode;
	private String mWorkOrderNuber;
	private long mJobStartTime;
	private long mJobFinishedTime;
	public long mJobValidityDate;
	public static MenuItem buttonSaved;
	private String mSearchResultSerialNum = null;
	private String mSearchResultDesign = null;
	private boolean mLoadBool;
	private String mBundleTyreID;
	private String mBundleOperationID;
	private int mBundleSAPContractID;
	private int mBundleSAPVendorID;
	private String mBundleCurrentStatus;
	private String mBundleSelectedDescription;
	private String mSapVendorID;
	private String mSavedJobRefNumber;
	private int mGetCount;
	private String mOpertaionCode;
	private int mCurrectStateVal;
	private int mCurrentTyreStatus;
	public String mCurrentTime = "";
	public String mCurrentDate = "";
	public int mSelectedYear, mSelectedMonth, mSelectedDay, mSelectedHour,
			mSelectedMinute;
	private String mEdt_search_str, strSerialNumber;
	private int mNum_item_selected;
	private int mRetainSwipedItem;
	private int mSelectedVendorPos;
	int mMaxSpinner;
	private boolean mDateValidation = false;
	public int mRemovetirevec_pos;
	/**
	 * Declaration and Initialization of ArrayList items
	 */
	public ArrayList<HashMap<String, String>> mSearch_TM_ListItems = new ArrayList<HashMap<String, String>>();
	private ArrayList<String> mVendorArrayList;
	private ArrayList<String> mCustomerArrayList;
	public ArrayList<HashMap<String, String>> mListItems = new ArrayList<HashMap<String, String>>();
	private HashMap<String, String> mHmap = null, mHmapTyreAction = null;
	private Bundle mBundle = new Bundle();
	private ArrayAdapter<String> mDataAdapter;

	private Bundle mInstanceState;
	private AlertDialog mAlert;
	public DialogFragment mNewFragmentDate;
	public DialogFragment mNewFragmentTime;
	boolean mDateTimeDialogVisible;
	public static ArrayList<String> mDamageExternalIds;
	private int mDeleteDialogPos;
	/**
	 * UI Elements Declaration
	 */
	public LinearLayout mll_result;
	private TextView mActionDesignheader;
	private EditText mSearchBox;
	private Spinner mSpinner;
	private Spinner mSpn_VendorName;
	private ImageButton mBtnSearch;
	private ListView mListView;
	private ListView mListViewTyreAction;
	private TextView mSerailheader;
	private TextView mDesignHeader;
	private TextView mLbl_jobRefNo;
	private TextView mValue_jobRefNo;
	private TextView mInput;
	private TextView mInputTime;
	private TextView mActionSerailheader;
	private TextView mResult;
	private TextView mYourActionText;
	private boolean mCheckTireStatus = false;
	private boolean mEnableStatus = false;
	String strTaskDone;
	private String mlblSelectedDateCannotBeLessThanCurrTime = "Please set correct date on the Mobile";
	ArrayList<String> mFinalDataToBeInserted = new ArrayList<String>();
	private AlertDialog mBackAlertDialog;

	private LogoutHandler mLogoutHandler;
	private String mCurrentTimeLabel;
	private String mAlreadyAddedLabel;
	
		/** vendor list with ID to map vendor to id */
	private Hashtable<String, String> mVendorHashWithID;

	/**
	 * Trace Info Variables
	 */
	private static final String TRACE_INFO = "Tire Management";

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		LogUtil.TraceInfo(TRACE_INFO, "none", TRACE_INFO, false, false, true);
		mInstanceState = savedInstanceState;
		mNewFragmentDate = new ReceivedDatePicker(this);
		mNewFragmentTime = new ReceivedOnTimePicker(this);
		try {
			mDbHelper = new DatabaseAdapter(this);
			mDbHelper.open();
			if (mDbHelper.getLabel(424).equalsIgnoreCase("")) {
				setTitle("Tire Management");
			} else {
				setTitle(mDbHelper.getLabel(424));
			}
		} catch (SQLException e3) {
			e3.printStackTrace();
		}
		mDamageExternalIds = new ArrayList<String>();
		context = TyreManagementActivity.this;
		mListView = (ListView) findViewById(R.id.list_view);
		mListView.setDivider(null);
		mListView.setDividerHeight(4);
		mCustomerArrayList = new ArrayList<String>();
		mValue_jobRefNo = (TextView) findViewById(R.id.value_jobRefNo);
		mListViewTyreAction = (ListView) findViewById(R.id.list_view_ActionOnTyre);
		mListViewTyreAction.setDivider(null);
		mListViewTyreAction.setDividerHeight(4);
		getOverflowMenu();
		mSpn_VendorName = (Spinner) findViewById(R.id.spinner_Vendor);
		mSpinner = (Spinner) findViewById(R.id.spinner_costumerName);
		mBtnSearch = (ImageButton) findViewById(R.id.btnSearch);
		mBtnSearch.setEnabled(false);
		mSearchBox = (EditText) findViewById(R.id.edt_search);
		mSpinner.setEnabled(false);
		mSearchBox.setEnabled(false);
		try {
			loadScreenLabelsFromDB();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		mDeleteDialogPos = -1;
		resetDateAlertFlag();
		if (savedInstanceState != null) {
			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			System.out.println("onSaveInstanceState!=null");
			mSelectedCustomerNamePos = savedInstanceState
					.getInt("selectedcustomernamepos");
			mEdt_search_str = savedInstanceState.getString("edtsearchstr");
			mSearchBox.setText(mEdt_search_str);
			mRetainSwipedItem = savedInstanceState.getInt("retainSwipedItem");
			mSelectedVendorPos = savedInstanceState.getInt("selectedVendorPos");
			mSapVendorID = savedInstanceState.getString("sapVendorID");
			try {
				Constants.RELOAD_SCREEN = true;
				mLoadBool = true;
				mSearchResultSerialNum = savedInstanceState
						.getString("serialNum");
				mSearchResultDesign = savedInstanceState.getString("design");
				mSelectedCustomerName = savedInstanceState
						.getString("customerName");// bundlecustomerID
				mBundleSAPContractID = savedInstanceState
						.getInt("customerIdsaved");
				mBundleSAPVendorID = savedInstanceState
						.getInt("bundleSAPVendorID");
				mCustomerID = savedInstanceState.getString("bundlecustomerID");
				mBundleOperationID = savedInstanceState
						.getString("OperationIDSaved");
				mRepairCompanyName = savedInstanceState
						.getString("bundleRepairCompanyName");
				mWorkOrderNuber = savedInstanceState
						.getString("WorkOrderNuberSaved");
				mBundleCurrentStatus = savedInstanceState
						.getString("curStatusSaved");
				mBundleSelectedDescription = savedInstanceState
						.getString("DescriptionSaved");
				mListItems = TyreConstants.getBundle_ListItems();
				if (mListItems != null) {
					mAdapter = new JobList_Adapter(TyreManagementActivity.this,
							mListItems);
					mListView.setAdapter(mAdapter);
					mListView.setVisibility(View.VISIBLE);
				}
				mSearch_TM_ListItems = TyreConstants
						.getBundle_Search_TM_ListItems();
				if (mSearch_TM_ListItems != null) {
					mAdapterTyreActionSaved = new MyListAdapter(
							TyreManagementActivity.this, mSearch_TM_ListItems);
					mListViewTyreAction.setVisibility(View.VISIBLE);
					mListViewTyreAction.setAdapter(mAdapterTyreActionSaved);
				}
				mSavedJobRefNumber = savedInstanceState
						.getString("SavedJobref");
				mValue_jobRefNo.setText(mSavedJobRefNumber);
				mBundleSerialNum = savedInstanceState
						.getString("bundleSerialNumSaved");
				mCurrectStateVal = savedInstanceState
						.getInt("currectStateValSaved");
				mBundleTyreID = savedInstanceState
						.getString("bundleTyreIDSaved");
				mCurrentTyreStatus = savedInstanceState
						.getInt("currentTyreStatusSaved");
				mOpertaionCode = savedInstanceState
						.getString("opertaionCodeSaved");
				mBundleJobItemUUID = savedInstanceState
						.getString("bundleJobItemUUIDSaved");
				mSearchBox.setEnabled(savedInstanceState
						.getBoolean("retainEarachBoxState"));
				if (null != buttonSaved) {
					if (savedInstanceState.getBoolean("retainButtonSaved")) {
						buttonSaved.setEnabled(true);
						buttonSaved.setIcon(R.drawable.savejob_navigation);
					} else {
						buttonSaved.setEnabled(false);
						buttonSaved.setIcon(R.drawable.unsavejob_navigation);
					}
				}
				mDateTimeDialogVisible = mInstanceState
						.getBoolean("isDialogVisible");
				mDeleteDialogPos = mInstanceState.getInt("retainDeleteDialog");
				if (mDateTimeDialogVisible) {
					mCurrentDate = mInstanceState
							.getString("retainCurrentDate");
					mCurrentTime = mInstanceState
							.getString("retainCurrentTime");//
				}
				mDamageExternalIds = mInstanceState
						.getStringArrayList("retainDamageExternalIds");
				mJobStartTime = mInstanceState.getLong("retainJobStartTime");
				mSelectedYear = mInstanceState.getInt("retainSelectedYear");
				mSelectedDay = mInstanceState.getInt("retainSelectedDay");
				mSelectedMonth = mInstanceState.getInt("retainSelectedMonth");
				mSelectedHour = mInstanceState.getInt("retainSelectedHour");
				mSelectedMinute = mInstanceState.getInt("retainSelectedMinute");
			} catch (Exception e) {
				e.printStackTrace();
			}
			mCheckTireStatus = mInstanceState.getBoolean("retainCheckStatus");
			mEnableStatus = mInstanceState.getBoolean("enableStatus");

			if (savedInstanceState.getBoolean("mBackAlertDialog")) {
				actionBackPressed();
			}
		} else {
			System.out.println("outside");
			if (TyreConstants.getBundle_Search_TM_ListItems().size() > 0) {
				TyreConstants.getBundle_Search_TM_ListItems().clear();
			}
			Validation
					.verifySerialNumberSearch(mSearchBox.getText().toString());
			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			mRetainSwipedItem = -1;

			mJobStartTime = DateTimeUTC.getMillisecondsFromUTCdate();
			Calendar c = Calendar.getInstance();
			mSelectedYear = c.get(Calendar.YEAR);
			mSelectedMonth = c.get(Calendar.MONTH) + 1;
			mSelectedDay = c.get(Calendar.DAY_OF_MONTH);
			mSelectedHour = c.get(Calendar.HOUR_OF_DAY);
			mSelectedMinute = c.get(Calendar.MINUTE);
		}
		if (Constants.RELOAD_SCREEN == false) {
			try {
				getJOBRefNumber();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		try {
			loadVendorName();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				hideKeyboard(view);
				mListViewTyreAction.setVisibility(View.VISIBLE);
				Constants._imageVisiblity = false;
				mHmapTyreAction = (HashMap<String, String>) mListView
						.getItemAtPosition(position);
				String Trace_msg="";
				try {
					Trace_msg = mHmapTyreAction.toString();
					LogUtil.TraceInfo(TRACE_INFO,"Selected Tyre Details",Trace_msg,false,true,false);
				}
				catch(Exception e)
				{
					LogUtil.TraceInfo(TRACE_INFO,"Selected Tyre Details - Exception",e.getMessage(),false,true,false);
				}

				mSearchResultSerialNum = mHmapTyreAction.get("licence");
				mSearchResultDesign = mHmapTyreAction.get("cname");
				TyreConstants
						.setSerialNumberSearchResult(mSearchResultSerialNum);
				TyreConstants.setDesignSearchResult(mSearchResultDesign);
				if (Constants.mBundleSerielNOVec
						.contains(mSearchResultSerialNum)) {
					Toast.makeText(getApplicationContext(), strSerialNumber,
							Toast.LENGTH_SHORT).show();
				} else {
					HashMap<String, String> tm_map = new HashMap<String, String>();
					tm_map.put("licence", mSearchResultSerialNum);
					tm_map.put("cname", mSearchResultDesign);
					tm_map.put("vendor", mSelectedVendorName);
					tm_map.put("customername", mSelectedCustomerName);
					tm_map.put("status", "false");
					if (!mSearch_TM_ListItems.contains(tm_map)) {
						mSearch_TM_ListItems.add(tm_map);
					} else {
						Toast.makeText(getApplicationContext(),
								mAlreadyAddedLabel, Toast.LENGTH_SHORT).show();
						LogUtil.TraceInfo(TRACE_INFO, "Work On", "Msg - " + mAlreadyAddedLabel, false, true, false);
					}
					mAdapterTyreActionSaved.refresh(mSearch_TM_ListItems);
					mGetCount = mListViewTyreAction.getCount();
				}
			}
		});
		mListViewTyreAction.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				try {
					TextView txtcust = (TextView) arg1
							.findViewById(R.id.textView1);
					String strSelectedCustomerName = txtcust.getText()
							.toString();
					TextView txt_vendor = (TextView) arg1
							.findViewById(R.id.textView2);
					String strSelectedVendorName = txt_vendor.getText()
							.toString();
					TextView txt_lp = (TextView) arg1
							.findViewById(R.id.TextView06);
					String strsearchResultSerialNum = txt_lp.getText()
							.toString();
					TextView txt_design = (TextView) arg1
							.findViewById(R.id.TextView05);
					String strsearchResultDesign = txt_design.getText()
							.toString();
					if (mSearch_TM_ListItems.get(arg2).get("status")
							.equalsIgnoreCase("false")) {
						Constants.CURRENTDATE = DateTimeUTC
								.getCurrentDate(context);
						Constants.CURRENTIME = DateTimeUTC.getCurrentTime();
						Intent intent = new Intent(TyreManagementActivity.this,
								TyreOperationActivity.class);
						Bundle bundle = new Bundle();
						bundle.putString("SearchResult",
								strsearchResultSerialNum);
						bundle.putString("SearchResultDesign",
								strsearchResultDesign);
						bundle.putString("VendorName", strSelectedVendorName);
						bundle.putString("CustomerName",
								strSelectedCustomerName);
						bundle.putInt("pos", arg2);
						Constants.position = arg2;
						intent.putExtras(bundle);
						Constants.SELECTED_DAMAGE_AREA_LSIT.clear();
						Constants.SELECTED_DAMAGE_TYPE_LIST.clear();
						LogUtil.TraceInfo(TRACE_INFO, "Work On", mSearch_TM_ListItems.get(arg2).toString(), false, true, false);
						startActivityForResult(intent, 1);
					} else {
						LogUtil.TraceInfo(TRACE_INFO,"Work On","Msg - "+strTaskDone,false,true,false);
						Toast.makeText(TyreManagementActivity.this,
								strTaskDone, Toast.LENGTH_SHORT).show();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		mListViewTyreAction
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int arg2, long arg3) {
						LogUtil.TraceInfo(TRACE_INFO,"none","Delete Dialog",false,false,false);
						confirmDeleteDialog(arg2);

						return true;
					}
				});
		mSearchBox.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable arg0) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				try {
					if (!mSearchBox.getText().toString().equals("")) {

						String Trace_msg = "";
						//Managed Tyre list Trace Information based on user preference
						try {
							Trace_msg = "\n\t\t";

							Trace_msg = Trace_msg + "Vendor Name : " + mSpn_VendorName.getSelectedItem().toString()+", ";
							Trace_msg = Trace_msg + "Search By: " + mSpinner.getSelectedItem().toString()+", ";
							Trace_msg = Trace_msg + "Search Key : " + mSearchBox.getText().toString();
							LogUtil.TraceInfo(TRACE_INFO, "On Search", Trace_msg,false,true,false);
						}
						catch (Exception e)
						{
							LogUtil.TraceInfo(TRACE_INFO, "On Search", e.getMessage(),false,true,false);
						}

						searchTyreResultAction();
					} else {
						mListView.setAdapter(null);
						mListItems.clear();

						Constants.getBundle_Retained_Jobitem().clear();
					}

				} catch (Exception e) {
				}
			}
		});
		if (mInstanceState != null) {
			if (mSpn_VendorName.isEnabled()) {
				mNum_item_selected = 0;
				mMaxSpinner = 2;
			} else {
				mNum_item_selected = 1;
				mMaxSpinner = 1;
			}
		} else {
			if (mSpn_VendorName.isEnabled()) {
				mNum_item_selected = 0;
				mMaxSpinner = 1;
			} else {
				mNum_item_selected = 0;
				mMaxSpinner = 0;
			}
		}
		if (mDateTimeDialogVisible) {
			showDateAlert();
		}

		if (mDeleteDialogPos > -1) {
			confirmDeleteDialog(mDeleteDialogPos);
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		LogoutHandler.setCurrentActivity(this);
	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	/**
	 * Getting tires for selected Customer
	 */
	protected void searchTyreResultAction() {
		mBtnSearch.setEnabled(false);
		mListView.setVisibility(View.VISIBLE);
		mListViewTyreAction.setVisibility(View.VISIBLE);

		if (mSelectedCustomerName.equals("-Select Customer Name-")) {
			Log.e("Amit", "selectedCustomerName" + mSelectedCustomerName);
		} else if (mSearchBox.getText().toString().equals("")
				&& mSearchBox.isEnabled()) {
		} else {
			mListItems = new ArrayList<HashMap<String, String>>();
			try {
				if (mListItems != null) {
					if (mListItems.size() > 0) {
						mListView.setVisibility(View.INVISIBLE);
						mListItems.clear();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			Cursor mCursor = mDbHelper.getTyreSearchResult(mSearchBox.getText()
					.toString(), Constants.SAP_CUSTOMER_ID, mSapVendorID);

			if (CursorUtils.isValidCursor(mCursor))// 0 i.e Cursor is empty
			{
				ArrayList<HashMap<String, String>> tyreList = Tyre_List();
				mCursor.moveToFirst();
				while (!mCursor.isAfterLast()) {
					String SerialNumber = mCursor.getString(mCursor
							.getColumnIndex("SerialNumber"));
					for (int i = 0; i < tyreList.size(); i++) {
						if (tyreList.get(i).get("SerialNo")
								.equalsIgnoreCase(SerialNumber)) {
							Cursor c = null;
							c = mDbHelper.CheckTyreId(tyreList.get(i).get(
									"ExternalID"));
							if (c.getCount() <= 0) {
								mHmap = new HashMap<String, String>();
								mHmap.put("licence", mCursor.getString(mCursor
										.getColumnIndex("SerialNumber")));// Serial
								mHmap.put("cname", mCursor.getString(mCursor
										.getColumnIndex("FullTireDetails")));// Design
								mListItems.add(mHmap);
							}
							c.close();
						}
					}
					mCursor.moveToNext();
				}
				CursorUtils.closeCursor(mCursor);
			} else {
				mListItems.removeAll(mListItems);
				String mNoTyreFounfLabel;
				mNoTyreFounfLabel = mDbHelper.getLabel(398);
				Toast toast = Toast.makeText(getApplicationContext(),
						mNoTyreFounfLabel, Toast.LENGTH_SHORT);
				LogUtil.TraceInfo(TRACE_INFO, "On Search - Problem :", mNoTyreFounfLabel,false,true,false);
				toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.show();
			}
			mListView.setVisibility(View.VISIBLE);
			mListViewTyreAction.setVisibility(View.VISIBLE);
			mAdapter = new JobList_Adapter(TyreManagementActivity.this,
					mListItems);
			if(mListItems!=null && mListItems.size()>0)
			{
				LogUtil.TraceInfo(TRACE_INFO, "On Search : Tyre Info", mListItems.toString(),false,true,false);
			}
			mListView.setAdapter(mAdapter);
			TyreConstants.setBundle_ListItems(mListItems);
			mHmapTyreAction = new HashMap<String, String>();
		}
	}

	/**
	 * closing DB
	 */
	@Override
	protected void onPause() {
		super.onPause();
		if (mNewFragmentDate.getDialog() != null) {
			if (mNewFragmentDate.getDialog().isShowing()) {
				mNewFragmentDate.dismiss();
			}
		}
		if (mNewFragmentTime.getDialog() != null) {
			if (mNewFragmentTime.getDialog().isShowing()) {
				mNewFragmentTime.dismiss();
			}
		}
	}

	/**
	 * Opening DB
	 */
	@Override
	public void onResume() {
		super.onResume();

		System.out.println("onResume");
		try {
			mDbHelper.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (mLoadBool) {
			try {
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (Constants.RELOAD_SCREEN == false) {
			if (Constants.JOBREFNUMBER != null
					&& Constants.JOBREFNUMBER.toString().equalsIgnoreCase("-1")) {
				getJOBRefNumber();
			}
		}
		if (Constants.CheckStatus) {
			mSpinner.setEnabled(false);
			mSpn_VendorName.setEnabled(false);
			mSearchBox.setEnabled(false);
		}
	}

	/**
	 * Back press Button
	 */
	@Override
	public void onBackPressed() {
		if (mSpinner != null && mSpinner.getSelectedItemPosition() == 0) {
			goBack();
		} else {
			actionBackPressed();
		}
	}

	/**
	 * action on Back Pressed
	 */
	private void actionBackPressed() {
		try {
			LayoutInflater li = LayoutInflater
					.from(TyreManagementActivity.this);
			View promptsView = li.inflate(R.layout.dialog_logout_confirmation,
					null);
			final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
					TyreManagementActivity.this);
			alertDialogBuilder.setView(promptsView);
			// update user activity for dialog layout
			LinearLayout rootNode = (LinearLayout) promptsView
					.findViewById(R.id.layout_root);
			rootNode.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					InactivityUtils.updateActivityOfUser();
				}
			});
			TextView infoTv = (TextView) promptsView
					.findViewById(R.id.logout_msg);
			infoTv.setText(mDbHelper.getLabel(241));
			alertDialogBuilder.setCancelable(false).setPositiveButton(
					mDbHelper.getLabel(331),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							// update user activity on button click
							alertDialogBuilder.updateInactivityForDialog();
							LogUtil.TraceInfo(TRACE_INFO, "Back Press Dialog", "BT - OK", false, true, false);
							goBack();

						}
					});
			alertDialogBuilder.setCancelable(false).setNegativeButton(
					mDbHelper.getLabel(332),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							// update user activity on button click
							alertDialogBuilder.updateInactivityForDialog();
							LogUtil.TraceInfo(TRACE_INFO, "Back Press Dialog", "BT - Cancel", false, true, false);
							dialog.cancel();
						}
					});
			mBackAlertDialog = alertDialogBuilder.create();
			mBackAlertDialog.show();
			mBackAlertDialog.setCanceledOnTouchOutside(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Clearing variables on Back Press
	 */
	private void goBack() {
		if (mListItems.size() > 0) {
			mListItems.clear();
		}
		resetParams();
		Constants._tireIDVec.clear();
		Constants._tireIDVec_Pos.clear();
		Constants._mBundleActionTypeVec.clear();
		Constants._mBundleOperationIDVec.clear();
		Constants._workOrderVec.clear();
		Constants._repairCompVec.clear();
		Constants.mBundleSerielNOVec.clear();
		Constants.mBundleDescOperationCodeVec.clear();
		Constants.mBundleOperationCodeVec.clear();
		Constants.mBundleFinalTyreStatusVec.clear();
		Constants.FROM_TM_OPERATION = false;
		Constants.damageImage_TM_LIST_Bitmap1.clear();
		Constants.damageImage_TM_LIST_Bitmap2.clear();
		Constants.damageImage_TM_LIST_Bitmap3.clear();
		Constants.damageNote_TM_LIST1.clear();
		Constants.damageNote_TM_LIST2.clear();
		Constants.damageNote_TM_LIST3.clear();
		Constants.SELECTED_DAMAGE_TYPE_LIST.clear();
		Constants.SELECTED_DAMAGE_AREA_LSIT.clear();
		Constants.CheckStatus = false;
		LogUtil.TraceInfo(TRACE_INFO, "none", "Back Key Press",false,false,false);
		this.finish();
	}

	/**
	 * action on delete
	 */
	private void confirmDeleteDialog(final int mPosition) {
		try {
			mDeleteDialogPos = mPosition;

			LayoutInflater li = LayoutInflater
					.from(TyreManagementActivity.this);
			View promptsView = li.inflate(R.layout.dialog_logout_confirmation,
					null);
			final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
					TyreManagementActivity.this);
			alertDialogBuilder.setView(promptsView);
			// update user activity for dialog layout
			LinearLayout rootNode = (LinearLayout) promptsView
					.findViewById(R.id.layout_root);
			rootNode.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					InactivityUtils.updateActivityOfUser();
				}
			});
			TextView infoTv = (TextView) promptsView
					.findViewById(R.id.logout_msg);
			mRetainSwipedItem = mPosition;
			infoTv.setText(mDbHelper.getLabel(339));
			alertDialogBuilder.setCancelable(false).setPositiveButton(
					mDbHelper.getLabel(331),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							// update user activity on button click
							alertDialogBuilder.updateInactivityForDialog();
							try {
								if (mSearch_TM_ListItems.size() != 0) {
									String getLiecenceNO = mSearch_TM_ListItems
											.get(mPosition).get("licence");
									try {
										if (mSearch_TM_ListItems.get(mPosition)
												.get("status")
												.equalsIgnoreCase("true")) {
											mRemovetirevec_pos = -1;
											for (int i = 0; i < Constants._tireIDVec_Pos
													.size(); i++) {
												if (Constants._tireIDVec_Pos
														.get(i) == mPosition) {
													mRemovetirevec_pos = i;
													break;
												}
											}
											if (mRemovetirevec_pos > -1) {
												Constants._tireIDVec
														.remove(mRemovetirevec_pos);
												Constants._tireIDVec_Pos
														.remove(mRemovetirevec_pos);
												Constants._mBundleActionTypeVec
														.remove(mRemovetirevec_pos);
												Constants._mBundleOperationIDVec
														.remove(mRemovetirevec_pos);
												Constants._workOrderVec
														.remove(mRemovetirevec_pos);
												Constants._repairCompVec
														.remove(mRemovetirevec_pos);
												if (Constants.SELECTED_DAMAGE_TYPE_LIST != null
														&& Constants.SELECTED_DAMAGE_TYPE_LIST
																.size() > 0) {
													Constants.SELECTED_DAMAGE_TYPE_LIST
															.remove(mRemovetirevec_pos);
												}
												if (Constants.SELECTED_DAMAGE_AREA_LSIT != null
														&& Constants.SELECTED_DAMAGE_AREA_LSIT
																.size() > 0) {
													Constants.SELECTED_DAMAGE_AREA_LSIT
															.remove(mRemovetirevec_pos);
												}
												if (Constants.damageImage_TM_LIST_Bitmap1 != null) {
													if (Constants.damageImage_TM_LIST_Bitmap1
															.size() > mRemovetirevec_pos) {
														Constants.damageImage_TM_LIST_Bitmap1
																.remove(mRemovetirevec_pos);
														Constants.damageNote_TM_LIST1
																.remove(mRemovetirevec_pos);
														Constants.SELECTED_DAMAGE_TYPE_LIST
																.remove(Constants.SELECTED_DAMAGE_TYPE);
														Constants.SELECTED_DAMAGE_AREA_LSIT
																.remove(Constants.SELECTED_DAMAGE_AREA);
													}
												}

												if (Constants.damageImage_TM_LIST_Bitmap2 != null) {
													if (Constants.damageImage_TM_LIST_Bitmap2
															.size() > mRemovetirevec_pos) {
														Constants.damageImage_TM_LIST_Bitmap2
																.remove(mRemovetirevec_pos);
														Constants.damageNote_TM_LIST2
																.remove(mRemovetirevec_pos);
													}
												}
												if (Constants.damageImage_TM_LIST_Bitmap3 != null) {
													if (Constants.damageImage_TM_LIST_Bitmap3
															.size() > mRemovetirevec_pos) {
														Constants.damageImage_TM_LIST_Bitmap3
																.remove(mRemovetirevec_pos);
														Constants.damageNote_TM_LIST3
																.remove(mRemovetirevec_pos);
													}
												}

												if (mDamageExternalIds != null
														&& mDamageExternalIds
																.size() > 0) {
													mDamageExternalIds
															.remove(mRemovetirevec_pos);
												}
											}
										}
										LogUtil.TraceInfo(TRACE_INFO,"Delete Tyre - BT - Delete",mSearch_TM_ListItems.get(mPosition).toString(),false,true,false);
										mSearch_TM_ListItems.remove(mPosition);
										mAdapterTyreActionSaved
												.refresh(mSearch_TM_ListItems);
										mRetainSwipedItem = -1;
									} catch (Exception e) {
										e.printStackTrace();
									}
									try {
										Constants.mBundleSerielNOVec
												.remove(getLiecenceNO);
									} catch (Exception e) {
										e.printStackTrace();
									}
									mBundleSerialNum = null;

									if (mSearch_TM_ListItems.size() == 0
											|| Constants._tireIDVec.size() == 0) {
										buttonSaved
												.setIcon(R.drawable.unsavejob_navigation);
										buttonSaved.setEnabled(false);
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							mDeleteDialogPos = -1;
						}
					});
			alertDialogBuilder.setCancelable(false).setNegativeButton(
					mDbHelper.getLabel(332),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							// update user activity on button click
							alertDialogBuilder.updateInactivityForDialog();
							LogUtil.TraceInfo(TRACE_INFO,"none", "Delete Tyre - BT - Cancel", false, false, false);
							mDeleteDialogPos = -1;
							dialog.cancel();
						}
					});
			AlertDialog alertDialog = alertDialogBuilder.create();
			alertDialog.show();
			alertDialog.setCanceledOnTouchOutside(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * hiding keyboard
	 * 
	 * @param view
	 */
	protected void hideKeyboard(View view) {
		try {
			InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			in.hideSoftInputFromWindow(view.getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * loading labels from DB
	 */
	private void loadScreenLabelsFromDB() {
		try {
			mLbl_jobRefNo = (TextView) findViewById(R.id.lbl_jobRefNo);
			mLbl_jobRefNo.setText("# ");
			String SearchHint = mDbHelper.getLabel(367);
			mSearchBox.setHint(SearchHint);
			strTaskDone = mDbHelper.getLabel(Constants.TaskDone);
			String Designheaderlabel = mDbHelper.getLabel(68);
			mDesignHeader = (TextView) findViewById(R.id.headerDesign);
			mDesignHeader.setText(Designheaderlabel);
			strSerialNumber = mDbHelper.getLabel(Constants.SerialNumber);
			mResult = (TextView) findViewById(R.id.yourResult);
			if (mDbHelper.getLabel(429).equalsIgnoreCase("")) {
				mResult.setText("Your Search Result");
			} else {
				mResult.setText(mDbHelper.getLabel(429));
			}
			mYourActionText = (TextView) findViewById(R.id.actionTyre_text);
			if (mDbHelper.getLabel(401).equalsIgnoreCase("")) {
				mYourActionText.setText("Action On Tire");
			} else {
				mYourActionText.setText(mDbHelper.getLabel(401));
			}
			mSerailheader = (TextView) findViewById(R.id.headerSerialNum);
			if (mDbHelper.getLabel(418).equalsIgnoreCase("")) {
				mSerailheader.setText("Serial Number");
			} else {
				mSerailheader.setText(mDbHelper.getLabel(418));
			}
			mActionSerailheader = (TextView) findViewById(R.id.header_SerialNumber);
			if (mDbHelper.getLabel(418).equalsIgnoreCase("")) {
				mActionSerailheader.setText("Serial Number");
			} else {
				mActionSerailheader.setText(mDbHelper.getLabel(418));
			}
			mActionDesignheader = (TextView) findViewById(R.id.header_design);

			if (mDbHelper.getLabel(68).equalsIgnoreCase("")) {
				mActionDesignheader.setText("Serial Number");
			} else {
				mActionDesignheader.setText(mDbHelper.getLabel(68));
			}
			mlblSelectedDateCannotBeLessThanCurrTime = mDbHelper
					.getLabel(Constants.SELECTED_DATE_CANNOT_BE_LESS_THAN_CURR);
			mCurrentTimeLabel = mDbHelper.getLabel(Constants.LABEL_DATE_DIALOG_TITLE);
			mAlreadyAddedLabel = mDbHelper.getLabel(Constants.ALLREADY_ADDED);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Get the JOBRefNumber from DB
	 */
	public void getJOBRefNumber() {
		Cursor jobRefCursor = null;
		try {
			jobRefCursor = mDbHelper.getJobReferenceNumber();
			LogUtil.e("TyreMgmt", "jobRefCursor:: " + jobRefCursor);
			if (CursorUtils.isValidCursor(jobRefCursor)) {
				int mJobRefNoFromAccTable = jobRefCursor.getInt(jobRefCursor
						.getColumnIndex("jobRefNumber"));
				if (mJobRefNoFromAccTable > 0) {
					++mJobRefNoFromAccTable;
					Constants.JOBREFNUMBERVALUE = mJobRefNoFromAccTable;
					Constants.JOBREFNUMBER = String
							.valueOf(Constants.JOBREFNUMBERVALUE);
					//Fix for Paritial inspection Job reference Number
					Constants.JobRefNumberForPartialInspectionCheck = Constants.JOBREFNUMBERVALUE;
					mValue_jobRefNo.setText(Constants.JOBREFNUMBER);

					LogUtil.e("TyreMgmt", "Constants.JOBREFNUMBER::"
							+ Constants.JOBREFNUMBER);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			LogUtil.e("LaunchNewJob.getJOBRefNumber", e.getMessage());
		} finally {
			CursorUtils.closeCursor(jobRefCursor);
		}
	}

	/**
	 * loading vender from DB
	 */
	public void loadVendorName() {
		Cursor vendorCursor = null;
		try {
			vendorCursor = mDbHelper.getVendorName();
			mVendorArrayList = new ArrayList<String>();
			mVendorHashWithID = new Hashtable<>();
			if (CursorUtils.isValidCursor(vendorCursor)) {
				vendorCursor.moveToFirst();
				while (!vendorCursor.isAfterLast()) {
					String vendor_city = vendorCursor.getString(vendorCursor
							.getColumnIndex("VendorName"))
							+ ", "
							+ vendorCursor.getString(vendorCursor
									.getColumnIndex("City"));
					mVendorArrayList.add(vendor_city);
					// put vendor_city as key and Id as value to map later
					// when user selects the drop down
					mVendorHashWithID.put(vendor_city, vendorCursor
							.getString(vendorCursor
									.getColumnIndex("VendorName")));
					vendorCursor.moveToNext();
				}
				CursorUtils.closeCursor(vendorCursor);
			}
			if (mVendorArrayList != null) {
				if (mVendorArrayList.size() < 2) {
					mSpn_VendorName.setEnabled(false);
				}
				Collections.sort(mVendorArrayList);
				if (mVendorArrayList.size() > 1) {
					mSpn_VendorName.setClickable(true);
					String mVendorSelectLabel = mDbHelper.getLabel(26)
							.toString();
					if (mVendorSelectLabel != null) {
						mVendorArrayList.add(0, mVendorSelectLabel);
					}
					mSpinner.setEnabled(false);
					mSearchBox.setEnabled(false);
				}
			}
			ArrayAdapter<String> vendorAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mVendorArrayList);
			vendorAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpn_VendorName.setAdapter(vendorAdapter);
			if (mVendorArrayList != null) {
				if (mVendorArrayList.size() < 2) {
					mSpn_VendorName.setClickable(false);
					if (mCheckTireStatus == false) {
						mSpinner.setEnabled(true);

						if (Constants._tireIDVec.size() == 0) {
							mSpinner.setEnabled(true);
						}
					}
					mSpn_VendorName.setEnabled(false);
					mSelectedVendorName = mVendorArrayList.get(0);
					//Bug:: 516 vendoeName fetched from HashTable and condition for ',' check is removed
					mSelectedVendorName = mVendorHashWithID.get(mSelectedVendorName);
					
					/*if (mSelectedVendorName.contains(",")) {
						mSelectedVendorName = mSelectedVendorName.substring(0,
								mSelectedVendorName.indexOf(","));
					}*/
				} else {
					mSpinner.setEnabled(false);
					mSelectedVendorName = mSpn_VendorName.getSelectedItem()
							.toString();
//					if (mSelectedVendorName.contains(",")) {
//						mSelectedVendorName = mSelectedVendorName.substring(0,
//								mSelectedVendorName.indexOf(","));
//					}
					mSelectedVendorName = mVendorHashWithID
							.get(mSelectedVendorName);
				}
			}
			mSpn_VendorName.setSelection(mSelectedVendorPos);
			mSpn_VendorName
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView<?> adapter,
								View v, int i, long lng) {

							System.out.println("mSpn_VendorName");

							if (mCheckTireStatus) {
								mSpinner.setEnabled(true);
							} else {
								mSearch_TM_ListItems
										.removeAll(mSearch_TM_ListItems);
								mAdapterTyreActionSaved = new MyListAdapter(
										TyreManagementActivity.this,
										mSearch_TM_ListItems);
								mListViewTyreAction.setVisibility(View.VISIBLE);
								mListViewTyreAction
										.setAdapter(mAdapterTyreActionSaved);
							}
							//Bug:: 516 vendoeName fetched from HashTable and condition for ',' check is removed
						/*	mSelectedVendorName = mSpn_VendorName
									.getSelectedItem().toString();*/
							/*if (mSelectedVendorName.contains(",")) {
								mSelectedVendorName = mSelectedVendorName
										.substring(0, mSelectedVendorName
												.indexOf(","));
							}*/
							Log.d("selectedVendorPos", "_" + mSelectedVendorPos);
							mSelectedVendorPos = i;
							if (mSelectedVendorName.equalsIgnoreCase(mDbHelper
									.getLabel(26).toString())) {
								mSpinner.setEnabled(false);
								mSearchBox.setEnabled(false);
								if (mListItems.size() > 0) {
									mListView.setVisibility(View.INVISIBLE);
									mListItems.clear();
								}
								mSelectedCustomerNamePos = 0;
								mSpinner.setAdapter(null);
								if (mCustomerArrayList.size() > 0) {
									mCustomerArrayList.clear();
								}
							} else {

								if (mCheckTireStatus == false) {
									mSpinner.setEnabled(true);

									if (Constants._tireIDVec.size() == 0) {
										mSpinner.setEnabled(true);
									}
								}
							}
							try {
								if (mListItems.size() > 0) {
								}
								if (mEdt_search_str != null) {
									mSearchBox.setText(mEdt_search_str);
								} else {
									mSearchBox.setText("");
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							//Bug:: 516 vendoeName fetched from HashTable and condition for ',' check is removed
							/*mSelectedVendorName = mSpn_VendorName
									.getSelectedItem().toString();*/
							/*if (mSelectedVendorName.contains(",")) {
								mSelectedVendorName = mSelectedVendorName
										.substring(0, mSelectedVendorName
												.indexOf(","));
							}*/
							if (mSelectedVendorPos != 0) {
								getVendorCountrycode(mSelectedVendorPos);
							} else if (mSelectedVendorPos == 0
									&& mVendorArrayList.size() < 2) {
								getVendorCountrycode(0);
							}

							if (mEnableStatus) {
								mSpinner.setEnabled(false);
								mSpn_VendorName.setEnabled(false);
								mSearchBox.setEnabled(false);
							}
						}
						@Override
						public void onNothingSelected(AdapterView<?> arg0) {
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(vendorCursor);
		}
	}


	/**
	 * getting vendor country code on position
	 * 
	 * @param position
	 */
	public void getVendorCountrycode(int position) {
		Cursor mCursor = null;
		try {
			//Bug:: 516 vendoeName fetched from HashTable and condition for ',' check is removed
			/*String ss = mVendorArrayList.get(position);
			ss = ss.substring(0, ss.indexOf(","));*/
			mCursor = mDbHelper.getVendorCountryCode(mSelectedVendorName);
			if (CursorUtils.isValidCursor(mCursor)) {
				mVendorCountryCode = mCursor.getString(mCursor
						.getColumnIndex("CountryCode"));
				TyreConstants.CountryCode = mVendorCountryCode;
				getVendorSAPID(mSelectedVendorName);
				CursorUtils.closeCursor(mCursor);
			} else {
				mCustomerArrayList.clear();
				if (mDataAdapter != null) {
					mDataAdapter.notifyDataSetChanged();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * getting vendor SAP ID
	 * 
	 * @param vendorName
	 */
	public void getVendorSAPID(String vendorName) {
		Cursor mCursor = null;
		try {
			mCursor = mDbHelper.getvendorIDByName(vendorName);
			if (CursorUtils.isValidCursor(mCursor)) {
				mCustomerArrayList.clear();
				mSapVendorID = mCursor.getString(mCursor
						.getColumnIndex("SAPVendorCodeID"));
				CursorUtils.closeCursor(mCursor);
				loadCustomerData();
			} else {
				mCustomerArrayList.clear();
				if (mDataAdapter != null) {
					mDataAdapter.notifyDataSetChanged();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Function to load the spinner data from SQLite database
	 * */
	public void loadCustomerData() {
		Cursor mCursor = null;
		try {
			mCustomerArrayList.add(mDbHelper
					.getLabel(Constants.SELECT_CUSTOMER_NAME));
			mCursor = mDbHelper.getCustomerName(TyreConstants.CountryCode,
					mSapVendorID);
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				while (!mCursor.isAfterLast()) {
					mCustomerArrayList.add(mCursor.getString(mCursor
							.getColumnIndex("CustomerName"))); // add the item
					mCursor.moveToNext();
				}
				CursorUtils.closeCursor(mCursor);
			}
			mDataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mCustomerArrayList);
			mDataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpinner.setAdapter(mDataAdapter);
			if (mCustomerArrayList.size() < 2) {
				mSpinner.setEnabled(false);
				Toast.makeText(getApplicationContext(),
						mDbHelper.getLabel(421), Toast.LENGTH_SHORT).show();
			} else {
				if (mCheckTireStatus == false) {
					mSpinner.setEnabled(true);

					if (Constants._tireIDVec.size() == 0) {
						mSpinner.setEnabled(true);
					}

				}
			}
			Log.d("spinner count:", "__" + mSpinner.getCount());
			if (mSpinner.getCount() > mSelectedCustomerNamePos) {
				mSpinner.setSelection(mSelectedCustomerNamePos, true);
			}
			mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> adapter, View v,
						int i, long lng) {

					System.out.println("mSpinner");

					if (mCheckTireStatus) {
						mSpinner.setEnabled(true);
						mCheckTireStatus = false;
					} else {
						mSearch_TM_ListItems.removeAll(mSearch_TM_ListItems);
						mAdapterTyreActionSaved = new MyListAdapter(
								TyreManagementActivity.this,
								mSearch_TM_ListItems);
						mListViewTyreAction.setVisibility(View.VISIBLE);
						mListViewTyreAction.setAdapter(mAdapterTyreActionSaved);
					}

					mSelectedCustomerName = mSpinner.getSelectedItem()
							.toString();
					mSelectedCustomerNamePos = mSpinner
							.getSelectedItemPosition();
					Log.d("spn_VendorName.isEnabled()",
							"_" + mSpn_VendorName.isEnabled());
					try {
						mNum_item_selected++;
						if (mNum_item_selected > mMaxSpinner) {
							Log.d("ListItems.size()", "_" + mListItems.size());
							if (mListItems.size() > 0) {
								mListView.setVisibility(View.INVISIBLE);
								mListItems.clear();
								mSearch_TM_ListItems.clear();
								mAdapterTyreActionSaved.clear();
								mAdapterTyreActionSaved.notifyDataSetChanged();
							}
							Constants.RELOAD_SCREEN = false;
							mEdt_search_str = "";
							mSearchBox.setText("");
						}
						if (mEdt_search_str != null) {
							mSearchBox.setText(mEdt_search_str);
							mEdt_search_str = null;
						} else {
							mSearchBox.setText("");
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					if (mSelectedCustomerName.equals(mDbHelper
							.getLabel(Constants.SELECT_CUSTOMER_NAME))) {
						mSearchBox.setEnabled(false);
						mBtnSearch.setEnabled(false);
						mBtnSearch
								.setBackgroundResource(R.drawable.search_icon_disable);
					} else {
						if (mCheckTireStatus == false) {
							mSearchBox.setEnabled(true);
						}
						mBtnSearch.setEnabled(true);
						mBtnSearch
								.setBackgroundResource(R.drawable.search_icon);
						if (Constants.RELOAD_SCREEN == false) {
							try {
								getCustomerID();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
					if (mEnableStatus) {
						mSpinner.setEnabled(false);
						mSpn_VendorName.setEnabled(false);
						mSearchBox.setEnabled(false);
					}
				}

				@Override
				public void onNothingSelected(AdapterView arg0) {
				}
			});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Fetching customer ID by Customer Name
	 */
	public void getCustomerID() {
		Cursor mCursor = null;
		try {
			mCursor = mDbHelper.getCustomerID(mSelectedCustomerName);
			if (CursorUtils.isValidCursor(mCursor)) {
				mCustomerID = mCursor.getString(mCursor.getColumnIndex("ID")); // add
				Constants.SAP_CUSTOMER_ID = mCustomerID; // the
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Coming back with Result
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		mDbHelper.open();
		if (requestCode == 1) {
			if (data != null) {
				try {
					Constants.RELOAD_REFF = true;
					Constants.GETJOBREFFNO = false;
					mBundle = data.getExtras();
					mBundleSerialNum = mBundle.getString("TyreSerialNum");
					Constants.mBundleSerielNOVec.add(mBundleSerialNum);
					mBundleDesign = mBundle.getString("TyreDesign");
					mBundleTyreID = mBundle.getString("TyreID");
					Constants._tireIDVec.add(mBundleTyreID);
					mBundleOperationID = mBundle.getString("OperationID");
					Constants._mBundleOperationIDVec.add(mBundleOperationID);
					mBundleSAPContractID = mBundle.getInt("SAPContractID");
					mBundleSAPVendorID = mBundle.getInt("SAPVendorID");
					mBundleTyreVendorName = mBundle.getString("TyreVendorName");
					mBundleTyreCustomerName = mBundle
							.getString("TyreCustomerName");
					mBundleactionstatus = mBundle.getString("actionstatus");
					int pos = mBundle.getInt("pos");
					mRepairCompanyName = mBundle.getString("RepairCompany");
					Constants._repairCompVec.add(mRepairCompanyName);
					mBundleSelectedDescription = mBundle
							.getString("SelectedDescription");// SelectedDescription
					Constants.mBundleDescOperationCodeVec
							.add(mBundleSelectedDescription);
					mWorkOrderNuber = mBundle.getString("WorkOrderNumber");
					Constants._workOrderVec.add(mWorkOrderNuber);
					mBundleJobItemUUID = mBundle.getString("jobItemUUID");
					buttonSaved.setIcon(R.drawable.savejob_navigation);
					buttonSaved.setEnabled(true);
					mBundleCurrentStatus = mBundle.getString("currentStatus");
					Constants._mBundleCurrentstatusVec
							.add(mBundleCurrentStatus);
					TextView tex = (TextView) findViewById(R.id.tyre_serialNumber_result);
					mSearch_TM_ListItems.remove(pos);
					HashMap<String, String> tm_map = new HashMap<String, String>();
					tm_map.put("licence", mBundleSerialNum);
					tm_map.put("cname", mBundleDesign);
					tm_map.put("vendor", mBundleTyreVendorName);
					tm_map.put("customername", mBundleTyreCustomerName);
					tm_map.put("status", mBundleactionstatus);
					mSearch_TM_ListItems.add(tm_map);
					if (Constants._tireIDVec_Pos.size() > 0) {
						for (int j = 0; j < Constants._tireIDVec_Pos.size(); j++) {
							Constants._tireIDVec_Pos.set(j,
									Constants._tireIDVec_Pos.get(j) - 1);
						}
					}
					Constants._tireIDVec_Pos
							.add(mSearch_TM_ListItems.size() - 1);
					mAdapterTyreActionSaved.refresh(mSearch_TM_ListItems);
					mGetCount = mListViewTyreAction.getCount();
					getWindow()
							.setSoftInputMode(
									WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
					getDamageExternalID();
					try {
						getOperationCode();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					Constants.FROM_TM_OPERATION = false;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * getting Operation-Code By Operation
	 */
	public void getOperationCode() {
		try {
			mDbHelper.open();
			int oprCodePos;
			for (oprCodePos = 0; oprCodePos < Constants.mBundleDescOperationCodeVec
					.size(); oprCodePos++) {
				Cursor mCursor = mDbHelper
						.getOperationCode(Constants.mBundleDescOperationCodeVec
								.get(oprCodePos));
				if (CursorUtils.isValidCursor(mCursor)) {
					mOpertaionCode = mCursor.getString(mCursor
							.getColumnIndex("OperationCode"));
					Constants.mBundleOperationCodeVec.add(mOpertaionCode);
					CursorUtils.closeCursor(mCursor);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			mDbHelper.close();
		}
	}

	/**
	 * getting Operation code as per operation ID
	 */
	private void checkOperationCode() {
		try {
			for (int finalOperationPos = 0; finalOperationPos < Constants.mBundleOperationCodeVec
					.size(); finalOperationPos++) {
				if (Constants.mBundleOperationCodeVec.get(finalOperationPos)
						.equalsIgnoreCase("BRRE")
						|| Constants.mBundleOperationCodeVec.get(
								finalOperationPos).equalsIgnoreCase("NRR")
						|| Constants.mBundleOperationCodeVec.get(
								finalOperationPos).equalsIgnoreCase("BRRU")
						|| Constants.mBundleOperationCodeVec.get(
								finalOperationPos).equalsIgnoreCase("BGVR")) {
					// R;
					Constants.mBundleFinalTyreStatusVec.add("2");
				} else if (Constants.mBundleOperationCodeVec.get(
						finalOperationPos).equalsIgnoreCase("BRRV")
						|| Constants.mBundleOperationCodeVec.get(
								finalOperationPos).equalsIgnoreCase("D")
						|| Constants.mBundleOperationCodeVec.get(
								finalOperationPos).equalsIgnoreCase("TBRG")
						|| Constants.mBundleOperationCodeVec.get(
								finalOperationPos).equalsIgnoreCase("BRRT")
						|| Constants.mBundleOperationCodeVec.get(
								finalOperationPos).equalsIgnoreCase("TBRT")
						|| Constants.mBundleOperationCodeVec.get(
								finalOperationPos).equalsIgnoreCase("TBMI")) {
					// D;
					Constants.mBundleFinalTyreStatusVec.add("6");
				} else if (Constants.mBundleOperationCodeVec.get(
						finalOperationPos).equalsIgnoreCase("BRRG")
						|| (Constants.mBundleOperationCodeVec
								.get(finalOperationPos)
								.equalsIgnoreCase("REUS"))) {
					// ROV;
					Constants.mBundleFinalTyreStatusVec.add("5");
				} else if (Constants.mBundleOperationCodeVec.get(
						finalOperationPos).equalsIgnoreCase("RTMW")
						|| (Constants.mBundleOperationCodeVec
								.get(finalOperationPos)
								.equalsIgnoreCase("TBMA"))) {
					// MINR;
					Constants.mBundleFinalTyreStatusVec.add("4");
				} else if (Constants.mBundleOperationCodeVec.get(
						finalOperationPos).equalsIgnoreCase("BMAR")) {
					// MAJR;
					Constants.mBundleFinalTyreStatusVec.add("3");
				}

				mCurrentTyreStatus = Integer
						.parseInt(Constants.mBundleFinalTyreStatusVec
								.get(finalOperationPos));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Calling Date Picker
	 */
	private void showDateAlert() {
		final EjobAlertDialog builder = new EjobAlertDialog(context);
		mDateTimeDialogVisible = true;
		builder.setTitle(mCurrentTimeLabel);
		mInput = new TextView(this);
		mInputTime = new TextView(this);
		mInput.setTypeface(Typeface.DEFAULT_BOLD);
		mInputTime.setTypeface(Typeface.DEFAULT_BOLD);
		mInput.setBackgroundResource(R.drawable.picker);
		mInputTime.setBackgroundResource(R.drawable.picker);
		mInput.setPadding(10, 20, 0, 10);
		mInputTime.setPadding(10, 20, 0, 10);

		if (mCurrentDate.equals("")) {
			mCurrentDate = DateTimeUTC.getCurrentDate(context);
		}

		if (mCurrentTime.equals("")) {
			mCurrentTime = DateTimeUTC.getCurrentTime();
		}
		mInput.setText(mCurrentDate);
		mInputTime.setText(mCurrentTime);
		mInput.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// update user activity on button click
				builder.updateInactivityForDialog();
				
				if (mNewFragmentDate.getDialog() != null) {
					if (mNewFragmentDate.getDialog().isShowing()) {
						return;
					}
				}
				Bundle bundle = new Bundle();
				try {
					bundle.putIntArray("bundleSplittedDate", DateTimeUTC
							.convertStringArrayToIntArray(DateTimeUTC
									.getSplittedDate(mInput.getText()
											.toString(), context)));
				} catch (Exception e) {
					e.printStackTrace();
				}
				mNewFragmentDate.setArguments(bundle);
				mNewFragmentDate.show(getFragmentManager(), "datePicker");
			}
		});
		if (mDateValidation == true) {
			LogUtil.TraceInfo(TRACE_INFO, "none", "Msg : "+mlblSelectedDateCannotBeLessThanCurrTime, false, false, false);
			Toast.makeText(getApplicationContext(),
					mlblSelectedDateCannotBeLessThanCurrTime,
					Toast.LENGTH_SHORT).show();
		}
		mInputTime.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// update user activity on button click
				builder.updateInactivityForDialog();

				if (mNewFragmentTime.getDialog() != null) {
					if (mNewFragmentTime.getDialog().isShowing()) {
						return;
					}
				}
				Bundle bundle = new Bundle();
				try {
					bundle.putIntArray("bundleSplittedTime", DateTimeUTC
							.convertStringArrayToIntArray(DateTimeUTC
									.getSplittedTime(mInputTime.getText()
											.toString(), context)));
				} catch (Exception e) {
					e.printStackTrace();
				}
				mNewFragmentTime.setArguments(bundle);
				mNewFragmentTime.show(getFragmentManager(), "timePicker");
			}
		});
		TableLayout.LayoutParams tableParams = new TableLayout.LayoutParams(
				TableLayout.LayoutParams.WRAP_CONTENT,
				TableLayout.LayoutParams.WRAP_CONTENT);
		TableRow.LayoutParams rowParams = new TableRow.LayoutParams(
				TableRow.LayoutParams.WRAP_CONTENT,
				TableRow.LayoutParams.WRAP_CONTENT);
		TableLayout tableLayout = new TableLayout(TyreManagementActivity.this);
		tableLayout.setLayoutParams(tableParams);
		tableLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				builder.updateInactivityForDialog();
			}
		});
		TableRow tableRow = new TableRow(TyreManagementActivity.this);
		tableRow.setLayoutParams(tableParams);
		mInput.setLayoutParams(rowParams);
		tableRow.addView(mInput);
		int leftMargin = 60;
		int topMargin = 0;
		int rightMargin = 0;
		int bottomMargin = 0;
		rowParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
		mInputTime.setLayoutParams(rowParams);
		tableRow.addView(mInputTime);
		tableLayout.addView(tableRow);
		builder.setView(tableLayout);
		builder.setPositiveButton(mDbHelper.getLabel(292),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						// update user activity on button click
						builder.updateInactivityForDialog();

						Constants.GETJOBREFFNO = true;
						Constants.RELOAD_REFF = false;
						Constants.CheckStatus = false;
						mJobFinishedTime = DateTimeUTC.getDateInTicks(mInput,
								mInputTime, context);
						int mDays = 3;// Adding 3 days to find jobValidity Date
						mJobValidityDate = DateTimeUTC
								.getDateInTicksAfterNdays(mInput, mInputTime,
										mDays, context);
						String vJobID = "";
						try {
							vJobID = insertJobData();
							try {
								insertJobItemData(vJobID);

							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								//No Edit mode for Tyre Management
								//Fix : Duplicate serial
								Constants.EDIT_JOB = false;
								mDbHelper
										.updateAccountTable(Constants.JOBREFNUMBERVALUE);
							} catch (Exception e2) {
								e2.printStackTrace();
							}
							try {
								checkOperationCode();
							} catch (Exception e2) {
								e2.printStackTrace();
							}
							try {
								UpdateJOB();
							} catch (Exception e1) {
								e1.printStackTrace();
							}

							try {
								mDbHelper.copyFinalDB();
							} catch (SQLException | IOException e) {
								e.printStackTrace();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							Constants.TM_NOT_ALLOWED = true;
							Intent intent = new Intent(
									TyreManagementActivity.this, EjobList.class);
							intent.putExtra("sentfrom", "TireManagement");
							Bundle bundle = new Bundle();
							bundle.putString("bundleSerialNum",
									mBundleSerialNum);
							bundle.putString("bundlecustomerID", mCustomerID);
							bundle.putString("CustomerName",
									mSelectedCustomerName);
							bundle.putInt("SAPCustomerID", mBundleSAPContractID);
							bundle.putString("bundleOperationID",
									mBundleOperationID);
							bundle.putString("bundleWorkOrderNuber",
									mWorkOrderNuber);
							bundle.putString("bundleRepairCompanyName",
									mRepairCompanyName);
							bundle.putString("bundleResultDesign",
									mSearchResultDesign);
							bundle.putString("curStatus", mBundleCurrentStatus);
							bundle.putString("Description",
									mBundleSelectedDescription);
							intent.putExtras(bundle);
							startActivity(intent);
						} catch (Exception e) {
							e.printStackTrace();
						}
						resetDateAlertFlag();
					}
				});
		builder.setNegativeButton(mDbHelper.getLabel(293),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						// update user activity on button click
						builder.updateInactivityForDialog();

						resetDateAlertFlag();
					}
				});
		mAlert = builder.create();
		mAlert.setCanceledOnTouchOutside(false);
		mAlert.show();
	}

	/**
	 * Inserting Data in JOB table
	 * 
	 * @return
	 */
	public String insertJobData() {
		String vJobID = "";
		Constants.JOB_RESULT_LISTSIZE = mGetCount;
		TimeZone tz = TimeZone.getDefault();
		Date now = new Date();
		int offsetFromUtc = tz.getOffset(now.getTime()) / 1000 / 60;
		try {
			vJobID = String.valueOf(UUID.randomUUID());
			mDbHelper.InsertJob(vJobID, "", mBundleSAPContractID, "",
					Constants.VALIDATE_USER, "2", "0", "", "",
					mJobFinishedTime, "", "0", "0", mJobStartTime,
					mJobStartTime, mJobStartTime, "", "", mVendorCountryCode,
					"0", "", "", "", "0", mSelectedVendorName,
					String.valueOf(mBundleSAPVendorID),
					"00000000-0000-0000-0000-000000000000", "", "0", null, "",
					"5", mValue_jobRefNo.getText().toString(),
					mJobFinishedTime + 259200000, offsetFromUtc, "","0","0");
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return vJobID;
	}

	/**
	 * Inserting Data in JOBITEM table
	 * 
	 * @param pJobID
	 */
	public void insertJobItemData(String pJobID) {
		String jobItmeIDForDB = "";
		try {
			int sequence = 0;
			int i = 0;
			int j;
			int statusT;
			for (statusT = 0; statusT < mSearch_TM_ListItems.size(); statusT++) {
				if (mSearch_TM_ListItems.get(statusT).get("status")
						.equalsIgnoreCase("true")) {
					mFinalDataToBeInserted.add(mSearch_TM_ListItems.get(0).get(
							"status"));
				}
			}
			try {
				mDamegeCounter = 0;
				for (i = 0; i < mFinalDataToBeInserted.size(); i++) {
					jobItmeIDForDB = String.valueOf(UUID.randomUUID());
					String damage_external_id = "";
					if (mDamageExternalIds != null) {
						if (mDamageExternalIds.size() > 0) {
							damage_external_id = mDamageExternalIds.get(i);
						}
					}
					mDbHelper.InsertJobItem(pJobID, 0, jobItmeIDForDB, Integer
							.parseInt(Constants._mBundleActionTypeVec.get(i)),
							0, Constants._tireIDVec.get(i), "",
							damage_external_id, "", "False", 0, "",
							Constants.THREAD_DEPTH_TM.get(i), 0, 0, 0, 0,
							Constants.REGROOVE_TYPE_TM.get(i), 0, 0, 0, 0, 0,
							Constants._workOrderVec.get(i),
							Constants._repairCompVec.get(i), 0, 0, 0, 0, 0,
							++sequence, 0, 0, "",
							Constants._mBundleOperationIDVec.get(i), 0);
					callInsertDamageImage();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Preparing for damage data insertion
	 */
	private void callInsertDamageImage() {
		Cursor mCursor = null;
		try {
			mCursor = mDbHelper.getJobItemIDMAX();
			if (CursorUtils.isValidCursor(mCursor)) {
				String jobID = mCursor.getString(mCursor.getColumnIndex("ID"));
				insertTyreDamageData(jobID);
			}
		} catch (Exception e3) {
			e3.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Getting Damage ID for Damage Type
	 */
	private void getDamageExternalID() {
		Cursor breakSpotCursor = null;
		try {
			mDbHelper.open();
			if (Constants.CHEKIFPICTAKENORNOT == false) {
				mDamageExternalIds.add("");
				Constants.damageImage_TM_LIST_Bitmap1.add(null);
				Constants.damageImage_TM_LIST_Bitmap2.add(null);
				Constants.damageImage_TM_LIST_Bitmap3.add(null);
				Constants.damageNote_TM_LIST1.add("");
				Constants.damageNote_TM_LIST2.add("");
				Constants.damageNote_TM_LIST3.add("");
			} else {
				if (Constants.SELECTED_DAMAGE_AREA_LSIT != null
						&& Constants.SELECTED_DAMAGE_TYPE_LIST != null) {
					if (Constants.SELECTED_DAMAGE_AREA_LSIT.size() > 0
							&& Constants.SELECTED_DAMAGE_TYPE_LIST.size() > 0) {
						if (Constants.SELECTED_DAMAGE_AREA_LSIT.size() == Constants.SELECTED_DAMAGE_TYPE_LIST
								.size()) {
							for (int i = 0; i < Constants.SELECTED_DAMAGE_AREA_LSIT
									.size(); i++) {
								breakSpotCursor = mDbHelper
										.getDamageExternalId(
												Constants.SELECTED_DAMAGE_TYPE_LIST
														.get(i),
												Constants.SELECTED_DAMAGE_AREA_LSIT
														.get(i));
								if (CursorUtils.isValidCursor(breakSpotCursor)) {
									breakSpotCursor.moveToFirst();
									String damage_external_id = breakSpotCursor
											.getString(breakSpotCursor
													.getColumnIndex("ExternalID"));
									mDamageExternalIds.add(damage_external_id);
									CursorUtils.closeCursor(breakSpotCursor);
								}
							}
						}
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			mDbHelper.close();
		}
	}

	/**
	 * getting byte from bitmap
	 * 
	 * @param bitmap
	 * @return
	 */
	public static byte[] getByteFromBitmap(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		int quality = Constants.bitmapQualityMap.get(bitmap);
		bitmap.compress(CompressFormat.JPEG, quality, stream);
		return stream.toByteArray();
	}

	/**
	 * damage image insertion
	 * 
	 * @param jobItmeIDForDB
	 */
	public void insertTyreDamageData(String jobItmeIDForDB) {
		try {
			if (Constants.damageImage_TM_LIST_Bitmap1 != null) {
				if (Constants.damageImage_TM_LIST_Bitmap1.size() != 0
						&& Constants.damageImage_TM_LIST_Bitmap1
								.get(mDamegeCounter) != null) {
					mDbHelper
							.insertTyreDamageData(
									Integer.parseInt(jobItmeIDForDB),
									"0",
									getByteFromBitmap(Constants.damageImage_TM_LIST_Bitmap1
											.get(mDamegeCounter)),
									Constants.damageNote_TM_LIST1
											.get(mDamegeCounter));
				}
			}
			if (Constants.damageImage_TM_LIST_Bitmap2 != null) {
				if (Constants.damageImage_TM_LIST_Bitmap2.size() != 0
						&& Constants.damageImage_TM_LIST_Bitmap2
								.get(mDamegeCounter) != null) {
					mDbHelper
							.insertTyreDamageData(
									Integer.parseInt(jobItmeIDForDB),
									"1",
									getByteFromBitmap(Constants.damageImage_TM_LIST_Bitmap2
											.get(mDamegeCounter)),
									Constants.damageNote_TM_LIST2
											.get(mDamegeCounter));
				}
			}
			if (Constants.damageImage_TM_LIST_Bitmap3 != null) {
				if (Constants.damageImage_TM_LIST_Bitmap3.size() != 0
						&& Constants.damageImage_TM_LIST_Bitmap3
								.get(mDamegeCounter) != null) {
					mDbHelper
							.insertTyreDamageData(
									Integer.parseInt(jobItmeIDForDB),
									"1",
									getByteFromBitmap(Constants.damageImage_TM_LIST_Bitmap3
											.get(mDamegeCounter)),
									Constants.damageNote_TM_LIST3
											.get(mDamegeCounter));
				}
			}
			mDamegeCounter++;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Updating Tire status after TM JOB
	 */
	public void UpdateJOB() {
		try {
			int mBundleSize = 0;
			for (mBundleSize = 0; mBundleSize < Constants.mBundleSerielNOVec
					.size(); mBundleSize++) {
				mDbHelper.updateTyre("Tyre",
						Constants.mBundleSerielNOVec.get(mBundleSize),
						"Status",
						Constants.mBundleFinalTyreStatusVec.get(mBundleSize));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			Constants.REGROOVE_TYPE_TM.clear();
			Constants.THREAD_DEPTH_TM.clear();
			Constants._mBundleActionTypeVec.clear();
			Constants._tireIDVec_Pos.clear();
			Constants._mBundleActionTypeVec
					.removeAll(Constants._mBundleActionTypeVec);
			Constants._tireIDVec_Pos.retainAll(Constants._tireIDVec_Pos);
			Constants._tireIDVec.removeAll(Constants._tireIDVec);
			Constants._tireIDVec.clear();
			Constants.damageImage_TM_LIST_Bitmap1
					.removeAll(Constants.damageImage_TM_LIST_Bitmap1);
			Constants.damageImage_TM_LIST_Bitmap2
					.removeAll(Constants.damageImage_TM_LIST_Bitmap2);
			Constants.damageImage_TM_LIST_Bitmap3
					.removeAll(Constants.damageImage_TM_LIST_Bitmap3);
			Constants.SELECTED_DAMAGE_AREA_LSIT.clear();
			Constants.SELECTED_DAMAGE_TYPE_LIST.clear();
			Constants.mBundleSerielNOVec.clear();
			Constants.mBundleDescOperationCodeVec.clear();
			Constants.mBundleOperationCodeVec.clear();
			Constants.mBundleFinalTyreStatusVec.clear();
			Constants.damageNote_TM_LIST1.clear();
			Constants.damageNote_TM_LIST2.clear();
			Constants.damageNote_TM_LIST3.clear();
			Constants.RELOAD_SCREEN = false;
		}
	}

	/**
	 * Creating Menu-Bar
	 * 
	 * @param menu
	 * @return
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		MenuItem action_settings = menu.findItem(R.id.action_settings);
		action_settings.setVisible(false);
		MenuItem action_about = menu.findItem(R.id.action_about);
		action_about.setVisible(false);
		MenuItem action_help = menu.findItem(R.id.action_help);
		action_help.setVisible(false);
		MenuItem action_logout = menu.findItem(R.id.action_logout);
		action_logout.setVisible(false);
		MenuItem action_summary = menu.findItem(R.id.action_summary);
		action_summary.setVisible(false);
		MenuItem action_summary_back = menu.findItem(R.id.action_save_summary);
		action_summary_back.setVisible(false);
		return true;
	}

	/**
	 * Action Bar menu OnCreate
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.tyremanagement_menu, menu);

		buttonSaved = menu.findItem(R.id.action_save);
		if (Constants.RELOAD_REFF == false) {
			buttonSaved.setIcon(R.drawable.unsavejob_navigation);
			buttonSaved.setEnabled(false);
		} else {
			buttonSaved.setIcon(R.drawable.savejob_navigation);
			buttonSaved.setEnabled(true);
		}
		return true;
	}

	/**
	 * Action on Menu item
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_save) {
			LogUtil.TraceInfo(TRACE_INFO, "Option", "Save", false, true, false);
			if (Constants.RELOAD_REFF) {
				showDateAlert();
			}
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Storing DATA to be retained on Orientation Change
	 */
	@Override
	protected void onSaveInstanceState(Bundle newState) {
		newState.putString("edtsearchstr", mSearchBox.getText().toString());
		newState.putInt("selectedcustomernamepos", mSelectedCustomerNamePos);
		newState.putInt("selectedVendorPos", mSelectedVendorPos);
		super.onSaveInstanceState(newState);
		try {
			newState.putString("serialNum",
					TyreConstants.getSerialNumberSearchResult());
			newState.putString("design", TyreConstants.getDesignSearchResult());
			newState.putString("vendorName", mSelectedVendorName);
			newState.putString("sapVendorID", mSapVendorID);
			newState.putInt("bundleSAPVendorID", mBundleSAPVendorID);
			newState.putInt("customerIdsaved", mBundleSAPContractID);
			newState.putString("customerName", mSelectedCustomerName);
			newState.putString("OperationIDSaved", mBundleOperationID);
			newState.putString("WorkOrderNuberSaved", mWorkOrderNuber);
			newState.putString("bundleRepairCompanyName", mRepairCompanyName);
			newState.putString("curStatusSaved", mBundleCurrentStatus);
			newState.putString("DescriptionSaved", mBundleSelectedDescription);
			newState.putString("bundleJobItemUUIDSaved", mBundleJobItemUUID);
			newState.putString("SavedJobref", mValue_jobRefNo.getText()
					.toString());
			newState.putInt("currectStateValSaved", mCurrectStateVal);
			newState.putString("bundleTyreIDSaved", mBundleTyreID);
			newState.putInt("currentTyreStatusSaved", mCurrentTyreStatus);
			newState.putString("bundleSerialNumSaved", mBundleSerialNum);
			newState.putString("opertaionCodeSaved", mOpertaionCode);
			TyreConstants.setBundle_ListItems(mListItems);
			newState.putInt("retainSwipedItem", mRetainSwipedItem);
			newState.putBoolean("retainEarachBoxState", mSearchBox.isEnabled());

			newState.putBoolean("retainButtonSaved", buttonSaved.isEnabled());
			newState.putBoolean("isDialogVisible", mDateTimeDialogVisible);
			if (mDateTimeDialogVisible) {
				newState.putString("retainCurrentDate", mInput.getText()
						.toString());
				newState.putString("retainCurrentTime", mInputTime.getText()
						.toString());
				mAlert.dismiss();
			}
			newState.putInt("retainDeleteDialog", mDeleteDialogPos);
			newState.putStringArrayList("retainDamageExternalIds",
					mDamageExternalIds);
			newState.putLong("retainJobStartTime", mJobStartTime);
			newState.putInt("retainSelectedYear", mSelectedYear);
			newState.putInt("retainSelectedMonth", mSelectedMonth);
			newState.putInt("retainSelectedDay", mSelectedDay);
			newState.putInt("retainSelectedHour", mSelectedHour);
			newState.putInt("retainSelectedMinute", mSelectedMinute);
			newState.putBoolean("retainCheckStatus", true);
			newState.putBoolean("enableStatus", Constants.CheckStatus);
		} catch (Exception e) {
			e.printStackTrace();
		}
		TyreConstants.setBundle_Search_TM_ListItems(mSearch_TM_ListItems);
		newState.putBoolean("mBackAlertDialog",
				(mBackAlertDialog != null && mBackAlertDialog.isShowing()));
	}

	/**
	 * Overriding On-Destroy
	 */
	@Override
	protected void onDestroy() {
		try {
			if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
				mBackAlertDialog.dismiss();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			mDbHelper.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	/**
	 * 
	 */
	private void getOverflowMenu() {
		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Inner Class for creating DATE TIME picker
	 * 
	 * @author Amitkumar.h
	 */
	@SuppressLint("ValidFragment")
	public class ReceivedDatePicker extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		Context mContext;
		private SafeDatePickerDialog mSafeDatePickerDialog;

		public ReceivedDatePicker(Context cntxt) {
			mContext = cntxt;
		}

		int[] splitteddate = null;

		@Override
		public Dialog onCreateDialog(Bundle savedInstance) {

			splitteddate = getArguments().getIntArray("bundleSplittedDate");

			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			if (splitteddate != null && splitteddate.length >= 3) {
				year = splitteddate[2];
				month = splitteddate[1] - 1;
				day = splitteddate[0];
			}
			mSelectedYear = year;
			mSelectedMonth = month;
			mSelectedDay = day;
			mSafeDatePickerDialog = new SafeDatePickerDialog(getActivity(),
					this, year, month, day);
			return mSafeDatePickerDialog;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			if (mSafeDatePickerDialog.isStopped()) {
				return;
			}

			mSelectedYear = year;
			mSelectedMonth = month + 1;
			mSelectedDay = day;

			if (!mSafeDatePickerDialog.isStopped()) {
				String curDate = DateTimeUTC.splittedDateToString(year,
						(month + 1), day, mContext);
				if (!TextUtils.isEmpty(curDate)) {
					if (mInput != null) {
						mInput.setText(curDate);
					}
				}
			}
		}
	}

	@SuppressLint("ValidFragment")
	public class ReceivedOnTimePicker extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {
		Context mContext;
		private SafeTimePickerDialog mSafeTimePickerDialog;

		public ReceivedOnTimePicker(Context cntxt) {
			mContext = cntxt;
		}

		int[] splittedtime = null;

		@Override
		public Dialog onCreateDialog(Bundle savedInstance) {
			splittedtime = getArguments().getIntArray("bundleSplittedTime");

			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int min = c.get(Calendar.MINUTE);

			if (splittedtime != null && splittedtime.length >= 2) {
				hour = splittedtime[0];
				min = splittedtime[1];
			}
			mSelectedHour = hour;
			mSelectedMinute = min;
			mSafeTimePickerDialog = new SafeTimePickerDialog(getActivity(),
					this, hour, min, DateFormat.is24HourFormat(getActivity()));
			return mSafeTimePickerDialog;
		}

		@Override
		public void onTimeSet(android.widget.TimePicker view, int hourOfDay,
				int minute) {
			if (mSafeTimePickerDialog.isStopped()) {
				return;
			}

			if (mSafeTimePickerDialog.isStopped()) {
				return;
			}
			mSelectedHour = hourOfDay;
			mSelectedMinute = minute;

			String min = checkMinutes(minute);
			String time = hourOfDay + ":" + min;
			if (mInputTime != null) {
				mInputTime.setText(time);
			}
		}
	}

	/**
	 * Returns Calculated Value in Minutes
	 * 
	 * @param min
	 * @return
	 */
	private String checkMinutes(int min) {
		if (min < 10) {
			return "0" + min;
		} else {
			return String.valueOf(min);
		}
	}

	/**
	 * Clearing all objects
	 */
	public static void resetParams() {
		Constants.ISBREAKSPOT = false;
		Constants.SELECTED_DAMAGE_TYPE = null;
		Constants.SELECTED_DAMAGE_AREA = null;
		TyreOperationActivity.mBitmapPhoto1 = null;
		TyreOperationActivity.mBitmapPhoto2 = null;
		TyreOperationActivity.mBitmapPhoto3 = null;
		Constants.ISBREAKSPOT = false;
		Constants.DAMAGE_NOTES1 = "";
		Constants.DAMAGE_NOTES2 = "";
		Constants.DAMAGE_NOTES3 = "";
	}

	/**
	 * Inner Class for List Adapter
	 * 
	 * @author amitkumar.h
	 */
	private class MyListAdapter extends
			com.goodyear.ejob.adapter.ArrayAdapter<HashMap<String, String>> {
		private Context mContext;
		private LayoutInflater l_Inflater;

		public MyListAdapter(Context context,
				ArrayList<HashMap<String, String>> items) {
			super(items);
			mContext = context;
			l_Inflater = LayoutInflater.from(context);
		}

		@Override
		public long getItemId(int position) {
			return getItem(position).hashCode();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			ViewHolder holder = new ViewHolder();
			if (row == null) {
				row = l_Inflater.inflate(R.layout.items_newjob_saved, null);
				holder = new ViewHolder();
				holder.txt_lp = (TextView) row.findViewById(R.id.TextView06);
				holder.txt_cust = (TextView) row.findViewById(R.id.textView1);
				holder.txt_vendor = (TextView) row.findViewById(R.id.textView2);
				holder.txt_customer = (TextView) row
						.findViewById(R.id.TextView05);
				holder.image = (ImageView) row
						.findViewById(R.id.tick_save_back);
				holder.txt_lp.setSelected(true);
				holder.txt_customer.setSelected(true);
				row.setTag(holder);
			} else {
				holder = (ViewHolder) row.getTag();
			}
			try {
				holder.txt_cust.setText(getItem(position).get("customername"));
				holder.txt_vendor.setText(getItem(position).get("vendor"));
				holder.txt_lp.setText(getItem(position).get("licence"));
				holder.txt_customer.setText(getItem(position).get("cname"));
				if (getItem(position).get("status").equalsIgnoreCase("false")) {
					holder.image.setVisibility(View.INVISIBLE);
				} else {
					holder.image.setVisibility(View.VISIBLE);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (position % 2 == 1) {
				row.setBackgroundColor(mContext.getResources().getColor(
						R.color.listview_color_dn)); // color.white
			} else {
				row.setBackgroundColor(mContext.getResources().getColor(
						R.color.listview_color_up)); // color.ltgreay
			}
			return row;
		}

		class ViewHolder {
			TextView txt_customer, txt_ref_no, txt_date, txt_lp;
			TextView txt_cust, txt_vendor;
			ImageView image;
		}
	}

	public void resetDateAlertFlag() {
		mDateTimeDialogVisible = false;
		mCurrentDate = "";
		mCurrentTime = "";
	}

	/**
	 * Fetching Tire ID
	 * 
	 * @return
	 */
	public ArrayList<HashMap<String, String>> Tyre_List() {
		ArrayList<HashMap<String, String>> tyreList = new ArrayList<HashMap<String, String>>();
		Cursor c = null;
		try {
			c = mDbHelper.GetTyre();
			if (CursorUtils.isValidCursor(c)) {

				if (c.moveToFirst()) {
					do {
						HashMap<String, String> tyrelistmap = new HashMap<String, String>();
						String strSerialNo = c.getString(0);
						String strExternalID = c.getString(1);

						tyrelistmap.put("SerialNo", strSerialNo);
						tyrelistmap.put("ExternalID", strExternalID);

						tyreList.add(tyrelistmap);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
		} finally {
			CursorUtils.closeCursor(c);
		}

		return tyreList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		InactivityUtils.logoutFromActivityBelowEjobForm(this);
	}
}