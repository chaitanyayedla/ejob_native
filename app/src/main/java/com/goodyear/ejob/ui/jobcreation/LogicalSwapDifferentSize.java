package com.goodyear.ejob.ui.jobcreation;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.goodyear.ejob.CameraActivity;
import com.goodyear.ejob.NoteActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.blutooth.BluetoothService;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.interfaces.PressureDialogHandler;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.job.operation.helpers.LogicalSwapInfo;
import com.goodyear.ejob.job.operation.helpers.PressureSwapDialog;
import com.goodyear.ejob.job.operation.helpers.TireImageItem;
import com.goodyear.ejob.performance.IOnPerformanceCallback;
import com.goodyear.ejob.performance.PerformanceBaseModel;
import com.goodyear.ejob.performance.ResponseMessagePerformance;
import com.goodyear.ejob.util.CameraUtility;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.InspectionDataHandler;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.TireDesignDetails;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.TyreState;

import java.util.ArrayList;
import java.util.UUID;

/**
 * @author amitkumar.h Class provides mechanism to load design page as per the
 *         selected tire(maintained, non-maintained, empty, spare etc). Moreover
 *         it handles the data updates in different tables(JobItem, Tire,
 *         jobCorrection etc) as per the user-entered tire-data during Logical
 *         Swap Operation with different size allowed .
 */
public class LogicalSwapDifferentSize extends Activity implements
		InactivityHandler, PressureDialogHandler {
	/** Class TAG */
	private static final String TAG = LogicalSwapActivity.class.getSimpleName();
	/** Used for note String */
	private static String sNoteText = "";
	/** use for enabling Radio Button */
	private static boolean sFlag = false;
	private static boolean sIsRadioBtnChecked = false;
	private LogicalSwapInfo mLogicalSwapinfo = new LogicalSwapInfo();
	private boolean mChangeMade;
	/** Used to Change Pressure Settings */
	private boolean mIsSettingsPSI;
	/** Used for Swiper Animation */
	private mSwiper swipeDetector;
	private float mRecommendedpressure;
	/**
	 * String Variable Declaration
	 */
	private String mSelectedBrandName;
	private String mSelectedtyreSize;
	private String mSelectedTyreTASP;
	private String mSelectedtyreTRIM;
	private String mSelectedTyreDesign;
	private String mSelectedTyreDetailedDesign;
	private String mBackPressMessage;
	private String mYESlabelFromDB;
	private String mNOlabelFromDB;
	private DatabaseAdapter mDbHelper;
	private BluetoothService mBTService;
	private BroadcastReceiver mReceiver;
	private int mNSKCounter = 1;
	private Tyre mSelectedTire;
	private boolean mSwiped = false;
	/**
	 * Array-list and Adapter Declaration
	 */
	private ArrayList<String> mbrandArrayList;
	private ArrayList<String> mSizeArrayList;
	private ArrayList<String> mTyreTRIMArrayList;
	private ArrayList<String> mTyreTASPArrayList;
	private ArrayList<String> mDesignArrayList;
	private ArrayList<String> mFullDesignArrayList;
	private ArrayAdapter<String> mSizeDataAdapter, mRimDataAdapter,
			mASPDataAdapter, mDesignDataAdapter, mFullDesignDataAdapter;
	/**
	 * UI Elements Declaration
	 */
	private Switch mRimType;
	private Spinner mBrandSpinner;
	private Spinner mSizeSpinner;
	private Spinner mTyreTRIMSpinner;
	private Spinner mTyreTASPSpinner;
	private Spinner mTyreDesignSpinner;
	private Spinner mTyreFullDetailSpinner;
	private EditText mValueNSK1;
	private EditText mValueNSK2;
	private EditText mValueNSK3;
	private ScrollView mMainScrollBar;
	private TextView mLbl_serialNo_swap;
	private TextView mValue_tyrePosition;
	private TextView mValue_tyreSerialNumber;
	private TextView mRegrooveTypeLabel;
	/**
	 * TextView reference for Brand Label
	 */
	private TextView mBrandLabel;
	private TextView mLabelNSK;
	private TextView mDimensionLabel;
	private TextView mSizeLabel;
	private TextView mAspLabel;
	private TextView mRimLabel;
	private TextView mDesignLabel;
	private TextView mTypeLabel;
	private TextView mDesignTypeLabel;
	private RadioButton mRegrooveTypeYes;
	private RadioButton mRegrooveTypeNo;
	private RadioGroup mGroupRegrooveType;
	private String mSelectedSAPMaterialCodeID;
	private String mSelectedSAPMaterialCodeIDForNM;
	private boolean mIsSizeSpinnerEditable = true;
	private String mBrandForAutoSwap;
	private String mSerialNumberForAutoSwap;
	private String mSizeForAutoSwap;
	private String mRimForAutoSwap;
	private String mAspForAutoSwap;
	private String mDesignForAutoSwap;
	private String mDesignDetailsForAutoSwap;
	private String mNskForAutoSwap;
	private String mNsk2ForAutoSwap;
	private String mNsk3ForAutoSwap;
	/**
	 * ImageView for Camera icon
	 */
	private ImageView mCamera_icon;
	private String external_id = "";
	/**
	 * boolean variable for is Camera enabled
	 */
	private boolean mCameraEnabled = false;
	/**
	 * Final variable for NOTES_INTENT
	 */
	public static final int NOTES_INTENT = 104;
	/**
	 * Default value for Spinner
	 */
	private String mDefaultValueForSpinner = "Please Select";

	/**
	 * IsOrientationChagned, This variable using Spinner manipulation while
	 * orientation
	 */
	private boolean mIsOrientationChanged = false;
	/**
	 * Last Spinner Selection Index, This variable using Spinner manipulation
	 * while orientation
	 */
	private int mLastSpinnerSelection = 0;
	/**
	 * String message from DB for Please Select Brand
	 */
	private String mPleaseSelectBrand = "Select a Brand";
	private AlertDialog mBackPressDialog;
	BTTask task;
	Bundle savedInstanceState;
	private TextView mJobDetailsLabel;
	private String mNotesLabel;
	private String PRESSURE_DIALOG_SAVED_DURING_ROTATION = "pressure_value_saved";
	private String PRESSURE_DIALOG_PRESSURE_SET = "pressure_value";
	private String PRESSURE_DIALOG_CURRENT_TIRE_POS = "pressure_current_tire_pos";
	private boolean mBoolPressureDialogShouldEnable = true;
	private String PRESSURE_SWAP_IS_EDIT_ENABLED = "PRESSURE_SWAP_IS_EDIT_ENABLED";
	private String mREGROOVEREQUIRED = "";

	//User Trace logs trace tag
	private static final String TRACE_TAG = "Logical Swap Diff Size";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.physical_swap);
		LogUtil.TraceInfo(TRACE_TAG, "none", "OP", true, false, true);
		this.savedInstanceState = savedInstanceState;
		Intent intent = getIntent();
		mRecommendedpressure = intent.getExtras().getFloat(
				VehicleSkeletonFragment.mAXLEPRESSURE);
		mGroupRegrooveType = (RadioGroup) findViewById(R.id.rg_regRooveType);
		mRegrooveTypeYes = (RadioButton) findViewById(R.id.value_regRooveYES);
		mRegrooveTypeNo = (RadioButton) findViewById(R.id.value_regRooveNO);
		if (Constants.SELECTED_TYRE == null) {
			Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
			mSelectedTire = Constants.SELECTED_TYRE;
		}
		if (Constants.SECOND_SELECTED_TYRE == null) {
			Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
			mSelectedTire = Constants.SECOND_SELECTED_TYRE;
		}
		try {
			initialize();
		} catch (Exception e4) {
			e4.printStackTrace();
		}
		settingDataForSelectedTire();
		if (savedInstanceState != null) {
			// CR 313
			// collecting swap pressure dialog information if swap dialog was
			// active during rotation
			if (savedInstanceState.getBoolean(
					PRESSURE_DIALOG_SAVED_DURING_ROTATION, false)) {
				String pressureValue = savedInstanceState
						.getString(PRESSURE_DIALOG_PRESSURE_SET);
				int position = savedInstanceState
						.getInt(PRESSURE_DIALOG_CURRENT_TIRE_POS);
				mBoolPressureDialogShouldEnable = savedInstanceState
						.getBoolean(PRESSURE_SWAP_IS_EDIT_ENABLED);
				startPressureDialog(position, pressureValue);
			}
			mNSKCounter = savedInstanceState.getInt("nsk_counter", 0);
			mIsOrientationChanged = true;
			mSelectedBrandName = savedInstanceState.getString("BRAND");
			mSelectedtyreSize = savedInstanceState.getString("SIZE");
			mSelectedTyreTASP = savedInstanceState.getString("ASP");
			mSelectedtyreTRIM = savedInstanceState.getString("RIM");
			mSelectedTyreDesign = savedInstanceState.getString("DESIGN");
			mSelectedTyreDetailedDesign = savedInstanceState
					.getString("FULLDETAIL");
			checkLatestSpinnerSelectionBeforeOrientationChanges();
		}
		mValueNSK1
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mValueNSK2
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mValueNSK3
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mValueNSK1.addTextChangedListener(new nskOneBeforeChanged());
		mValueNSK2.addTextChangedListener(new nskTwoBeforeChanged());
		mValueNSK3.addTextChangedListener(new nskThreeBeforeChanged());
		mDbHelper = new DatabaseAdapter(this);
		mDbHelper.createDatabase();
		mDbHelper.open();
		try {
			loadLabelsFromDB();
		} catch (Exception e3) {
			e3.printStackTrace();
		}
		try {
			mMainScrollBar = (ScrollView) findViewById(R.id.scroll_main);
			swipeDetector = new mSwiper(getApplicationContext(), mMainScrollBar);
			mMainScrollBar.setOnTouchListener(swipeDetector);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		checkAxleSize();
		restoreDialogState(savedInstanceState);
		// AutoSwap
		setDataForAutoSwap();
	}

	/**
	 * 
	 */
	private void settingDataForSelectedTire() {
		if (!Constants.onBrandBlank
				&& Constants.SELECTED_TYRE != null
				&& Constants.SECOND_SELECTED_TYRE != null
				&& !Constants.LOGICAL_SWAP_INITIAL
				|| (Constants.SECOND_SELECTED_TYRE != null && (Constants.SELECTED_TYRE
						.getTyreState() != TyreState.NON_MAINTAINED && Constants.SECOND_SELECTED_TYRE
						.getTyreState() == TyreState.NON_MAINTAINED))
				|| (Constants.SECOND_SELECTED_TYRE != null
						&& (Constants.SELECTED_TYRE.getTyreState() != TyreState.NON_MAINTAINED && Constants.SECOND_SELECTED_TYRE
								.getTyreState() != TyreState.NON_MAINTAINED) && !Constants.onBrandBlank)) {
			mSelectedTire = Constants.SECOND_SELECTED_TYRE;
			if (!Constants.onBrandDesignBool) {
				mSelectedBrandName = Constants.SECOND_SELECTED_TYRE
						.getBrandName();
			}
			mSelectedtyreSize = Constants.SECOND_SELECTED_TYRE.getSize();
			mSelectedTyreTASP = Constants.SECOND_SELECTED_TYRE.getAsp();
			mSelectedtyreTRIM = Constants.SECOND_SELECTED_TYRE.getRim();
			mSelectedTyreDesign = Constants.SECOND_SELECTED_TYRE.getDesign();
			mSelectedTyreDetailedDesign = Constants.SECOND_SELECTED_TYRE
					.getDesignDetails();
			//Fix:: 666 (NA/NR will not be a part of Auto-Logical Swap)
			if (!Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP) &&
					!Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP)) {
				if (AutoSwapImpl.checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)
						&& !mSelectedTire.getSerialNumber().equalsIgnoreCase(
						Constants.EDITED_SERIAL_NUMBER)) {
					Constants.EDITED_SERIAL_NUMBER_DES = Constants.EDITED_SERIAL_NUMBER;
				}
			}
		} else {
			mSelectedTire = Constants.SELECTED_TYRE;
			if (!Constants.onBrandDesignBool) {
				mSelectedBrandName = Constants.SELECTED_TYRE.getBrandName();
			}
			mSelectedtyreSize = Constants.SELECTED_TYRE.getSize();
			mSelectedTyreTASP = Constants.SELECTED_TYRE.getAsp();
			mSelectedtyreTRIM = Constants.SELECTED_TYRE.getRim();
			mSelectedTyreDesign = Constants.SELECTED_TYRE.getDesign();
			mSelectedTyreDetailedDesign = Constants.SELECTED_TYRE
					.getDesignDetails();
		}
		if (mSelectedTire.getSerialNumber().equals(
				Constants.EDITED_SERIAL_NUMBER)) {
			mValue_tyreSerialNumber.setText(mSelectedTire.getSerialNumber());
		} else {
			mValue_tyreSerialNumber.setText(Constants.EDITED_SERIAL_NUMBER);
		}
		mValue_tyrePosition.setText(mSelectedTire.getPosition());
		mValueNSK1.setText(mSelectedTire.getNsk());
		mValueNSK2.setText(mSelectedTire.getNsk2());
		mValueNSK3.setText(mSelectedTire.getNsk3());
	}

	/**
	 * Overriden method storing all the captured data inside bundle object in
	 * order to handle different orientations
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mBrandSpinner.getCount() != 0) {
			outState.putString("BRAND", mBrandSpinner.getSelectedItem()
					.toString());
		}
		if (mSizeSpinner.getCount() != 0) {
			outState.putString("SIZE", mSizeSpinner.getSelectedItem()
					.toString());
		}
		if (mTyreTASPSpinner.getCount() != 0) {
			outState.putString("ASP", mTyreTASPSpinner.getSelectedItem()
					.toString());
		}
		if (mTyreTRIMSpinner.getCount() != 0) {
			outState.putString("RIM", mTyreTRIMSpinner.getSelectedItem()
					.toString());
		}
		// Null check added for Selected Item of design Spinner
		if (null != mTyreDesignSpinner && null != mTyreDesignSpinner.getSelectedItem()) {
			if (mTyreDesignSpinner.getCount() != 0) {
				outState.putString("DESIGN", mTyreDesignSpinner
						.getSelectedItem().toString());
			}
		}
		if (mTyreFullDetailSpinner.getCount() != 0) {
			outState.putString("FULLDETAIL", mTyreFullDetailSpinner
					.getSelectedItem().toString());
		}

		if (mBTService != null) {
			outState.putBoolean("isworkerstopped", mBTService.isWorkerThread());
		}
		outState.putInt("nsk_counter", mNSKCounter);

		// CR 313
		// if pressure dialog is displayed then capture its state for recreating
		if (mPressureSwapDialog != null
				&& mPressureSwapDialog.isPressureDialogDisplayed()) {
			outState.putBoolean(PRESSURE_DIALOG_SAVED_DURING_ROTATION, true);
			outState.putString(PRESSURE_DIALOG_PRESSURE_SET,
					mPressureSwapDialog.getCurrentPressureSetByUser());
			outState.putInt(PRESSURE_DIALOG_CURRENT_TIRE_POS,
					mPressureSwapDialog.getCurrentTirePos());
			outState.putBoolean(PRESSURE_SWAP_IS_EDIT_ENABLED,
					mBoolPressureDialogShouldEnable);
			// dismiss dialog
			mPressureSwapDialog.dismissDialog();
		}
		saveDialogState(outState);
	}

	/**
	 * Overriden method handling application inactivity by updating the
	 * user-interaction with the application
	 */
	@Override
	public void onUserInteraction() {
		// update user activity
		InactivityUtils.updateActivityOfUser();
	}

	@Override
	protected void onStart() {
		// set this activity as logout handler
		LogoutHandler.setCurrentActivity(this);
		// CR 313 : enable pressure dialog textview
		mBoolPressureDialogShouldEnable = true;
		if (mPressureSwapDialog != null)
			mPressureSwapDialog.togglePressureField(true);
		mValueNSK1.setEnabled(true);
		mValueNSK2.setEnabled(true);
		mValueNSK3.setEnabled(true);
		if (mBTService != null) {
			mBTService.stop();
		}
		mBTService = new BluetoothService(this);
		if (savedInstanceState != null) {
			boolean isWorkerThread = savedInstanceState
					.getBoolean("isworkerstopped");
			mBTService.setWorkerThread(isWorkerThread);
		}
		IntentFilter intentFilter = new IntentFilter("BLUETOOTH_SENDER");
		mReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.hasExtra("BT_DATA_NSK")) {
					String nsk_value = intent.getStringExtra("BT_DATA_NSK");
					if (mNSKCounter == 1) {
						mValueNSK1.setText(nsk_value);
						mValueNSK2.setText(nsk_value);
						mValueNSK3.setText(nsk_value);
						mNSKCounter = 2;
					} else if (mNSKCounter == 2) {
						mValueNSK2.setText(nsk_value);
						mNSKCounter = 3;
					} else if (mNSKCounter == 3) {
						mNSKCounter = 1;
						mValueNSK3.setText(nsk_value);
					}
				} else if (intent.hasExtra("BT_DATA_PRE")) {
					String pre_value = intent.getStringExtra("BT_DATA_PRE");
					// CR 313 : Capture pressure from bluetooth device and set
					// this pressure in Pressure Dialog
					if (CommonUtils.getPressureUnit(context).equals(
							Constants.PSI_PRESSURE_UNIT)) {
						String barValue;
						// change PSI to BAR
						barValue = CommonUtils.getPressureValue(context,
								CommonUtils.parseFloat(pre_value));
						pre_value = String.valueOf(barValue);
					}
					if (mPressureSwapDialog != null
							&& mPressureSwapDialog.isPressureDialogDisplayed()) {
						mPressureSwapDialog.setPressureDialogValue(pre_value);
					}
				} else if (intent.hasExtra("BT_STATUS_IS_DISCONNECTED")) {
					boolean isDisconnected = intent.getBooleanExtra(
							"BT_STATUS_IS_DISCONNECTED", false);
					LogUtil.d("Bluetooth in my Operation",
							"Broadcast came to my oeration:: " + isDisconnected);
					// CR 313 : if bluetooth device is disconnected then update
					// the pressure dialog textview
					mBoolPressureDialogShouldEnable = isDisconnected;
					if (mPressureSwapDialog != null)
						mPressureSwapDialog.togglePressureField(isDisconnected);
					mValueNSK1.setEnabled(isDisconnected);
					mValueNSK2.setEnabled(isDisconnected);
					mValueNSK3.setEnabled(isDisconnected);
					if (isDisconnected) {
						mBTService.stop();
						initiateBT();
					} else {
						LogUtil.i("TOR", "######## Makeing stopworker false ");
						mBTService.startReading();
					}
				}
			}
		};
		registerReceiver(mReceiver, intentFilter);

		initiateBT();
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	/**
	 * Method initializing Blue-tooth, checking for connectivity and performing
	 * data transfer while using blue-tooth probes.
	 */
	private void initiateBT() {
		int status = mBTService.getPairedStatus();
		switch (status) {
		case 0:
			Toast.makeText(getApplicationContext(),
					Constants.sLblMultiplePaired, Toast.LENGTH_LONG).show();
			break;
		case 1:
			task = new BTTask();
			task.execute();
			break;
		case 2:
			Toast.makeText(getApplicationContext(),
					Constants.sLblPleasePairTLogik, Toast.LENGTH_LONG).show();
			break;
		case 3:
			Toast.makeText(getApplicationContext(),
					Constants.sLblNoPairedDevices, Toast.LENGTH_LONG).show();
			break;
		}
	}

	/**
	 * class handling blue-tooth functionality in a separate non-UI threads and
	 * performing data transfer while using blue-tooth probes.
	 */
	class BTTask extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... f_url) {
			if (isCancelled()) {
				LogUtil.e("LogicalDiffSize",
						"doInBackground Async Task Cancelled  > ");
				return "";
			}
			try {
				mBTService.connectToDevice(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if (isCancelled()) {
				LogUtil.e("LogicalDiffSize",
						"doInBackground Async Task Cancelled  > ");
				return;
			}
			try {
				if (mBTService.isConnected()) {
					// CR 313: disable pressure dialog 
					mBoolPressureDialogShouldEnable = false;
					if (mPressureSwapDialog != null)
						mPressureSwapDialog.togglePressureField(false);
					mValueNSK1.setEnabled(false);
					mValueNSK2.setEnabled(false);
					mValueNSK3.setEnabled(false);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * On Pause Call
	 */
	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		LogUtil.i("BT",
				"****************** onStop *****************:mBTService: "
						+ mBTService);
		try {
			if (null != mBTService) {
				mBTService.stop();
				if (task != null && !task.isCancelled()) {
					mBTService.asyncCancel(true);
					task.cancel(true);
					task = null;
				}
			}
			if (null != mReceiver) {
				unregisterReceiver(mReceiver);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onStop();
	}

	/**
	 * Loading Labels for UI from the Translation table of the local database
	 * file
	 */
	private void loadLabelsFromDB() {

		try {
			TextView tireDetails = (TextView) findViewById(R.id.lbl_tireDetails);
			if (tireDetails != null) {
				tireDetails.setText(mDbHelper
						.getLabel(Constants.LABEL_TIRE_DETAILS));
			}
			setTitle(mDbHelper.getLabel(Constants.LABEL_LOGICAL_SWAP));
			String labelFromDB = mDbHelper.getLabel(Constants.SERIAL_NUMBER);
			mLbl_serialNo_swap.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.NSK_LABEL);
			mLabelNSK.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.BRAND_LABEL);
			mBrandLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.REGROOV_ACTIVITY_NAME);
			mRegrooveTypeLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.DIMENSION_LABEL);
			mDimensionLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.ASP_LABEL);
			mAspLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.RIM_LABEL);
			mRimLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.DESIGN_LABEL);
			mDesignLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.SIZE_LABEL);
			mSizeLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.TYPE_LABEL);
			mDesignTypeLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.RIM_TYPE_LABEL);
			mTypeLabel.setText(labelFromDB);
			mRimType.setTextOn(mDbHelper.getLabel(Constants.ALLOY));
			mRimType.setTextOff(mDbHelper.getLabel(Constants.STEEL));
			mYESlabelFromDB = mDbHelper.getLabel(Constants.YES_LABEL);
			mRegrooveTypeYes.setText(mYESlabelFromDB);
			mNOlabelFromDB = mDbHelper.getLabel(Constants.NO_LABEL);
			mRegrooveTypeNo.setText(mNOlabelFromDB);
			mBackPressMessage = mDbHelper.getLabel(Constants.CONFIRM_BACK);

			labelFromDB = mDbHelper.getLabel(Constants.JOB_DETAILS);
			mJobDetailsLabel.setText(labelFromDB);
			mPleaseSelectBrand = mDbHelper.getLabel(Constants.SELECT_BRAND);
			labelFromDB = mDbHelper.getLabel(Constants.PLEASE_SELECT_LABEL);
			mDefaultValueForSpinner = labelFromDB;
			mNotesLabel = mDbHelper.getLabel(Constants.NOTES_ACTIVITY_NAME);
			mDbHelper.close();
			mRimType.setOnCheckedChangeListener(new SwitchChangeHandler());
			mREGROOVEREQUIRED = mDbHelper
					.getLabel(Constants.REGROOVE_IS_REQUIRED);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private class SwitchChangeHandler implements OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			mChangeMade = true;
		}
	}

	/**
	 * Method initializing all the UI elements, variables, array-lists and
	 * array-adaptors
	 */
	private void initialize() {
		SharedPreferences preferences = getSharedPreferences(
				Constants.GOODYEAR_CONF, 0);
		if (preferences.getString(Constants.PRESSUREUNIT, "").equals(
				Constants.PSI_PRESSURE_UNIT)) {
			mIsSettingsPSI = true;
		}
		mDesignArrayList = new ArrayList<String>();
		mFullDesignArrayList = new ArrayList<String>();
		mbrandArrayList = new ArrayList<String>();
		mSizeArrayList = new ArrayList<String>();
		mTyreTRIMArrayList = new ArrayList<String>();
		mTyreTASPArrayList = new ArrayList<String>();
		mRimType = (Switch) findViewById(R.id.value_RimType);
		mLbl_serialNo_swap = (TextView) findViewById(R.id.lbl_serialNo_swap);
		mValue_tyreSerialNumber = (TextView) findViewById(R.id.value_serialNo);// lbl_serialNo_swap
		mValue_tyrePosition = (TextView) findViewById(R.id.value_wp);
		mBrandSpinner = (Spinner) findViewById(R.id.spinner_brand);
		mSizeSpinner = (Spinner) findViewById(R.id.spinner_dimenssionSize);
		mTyreTASPSpinner = (Spinner) findViewById(R.id.value_dimenssionASP);
		mTyreTRIMSpinner = (Spinner) findViewById(R.id.value_dimenssionRIM);
		mTyreDesignSpinner = (Spinner) findViewById(R.id.spinner_make);
		mTyreFullDetailSpinner = (Spinner) findViewById(R.id.spinner_type);
		mValueNSK1 = (EditText) findViewById(R.id.value_NSK1);
		mValueNSK2 = (EditText) findViewById(R.id.value_NSK2);
		mValueNSK3 = (EditText) findViewById(R.id.value_NSK3);
		mTyreFullDetailSpinner = (Spinner) findViewById(R.id.spinner_type);// ll_main
		mRegrooveTypeLabel = (TextView) findViewById(R.id.lbl_reGrooved);
		mLabelNSK = (TextView) findViewById(R.id.lbl_NSK);
		mBrandLabel = (TextView) findViewById(R.id.lbl_brand);
		mDimensionLabel = (TextView) findViewById(R.id.lbl_dimenssion);
		mSizeLabel = (TextView) findViewById(R.id.lbl_dimenssionSiz);
		mAspLabel = (TextView) findViewById(R.id.lbl_dimenssionASP);
		mRimLabel = (TextView) findViewById(R.id.lbl_dimenssionRIM);
		mDesignLabel = (TextView) findViewById(R.id.lbl_make);// lbl_type
		mTypeLabel = (TextView) findViewById(R.id.lbl_RimType);
		mDesignTypeLabel = (TextView) findViewById(R.id.lbl_type);
		mJobDetailsLabel = (TextView) findViewById(R.id.lbl_jobDetails);
		// CR:451
		mCamera_icon = (ImageView) findViewById(R.id.camera_icon);
		mCamera_icon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent photoIntent = new Intent(LogicalSwapDifferentSize.this,
						CameraActivity.class);
				photoIntent.putExtra("CURNT_IMAGE_COUNT",
						CameraUtility.getImageCount());
				startActivity(photoIntent);
			}
		});
	}

	/**
	 * Method checking whether the selected tire is present on the axle having
	 * no any defined Dimension(Size, ASP and Rim) or not
	 * 
	 * @return
	 */
	private void checkAxleSize() {
		if (TextUtils.isEmpty(mSelectedTyreDetailedDesign)) {
			TireDesignDetails detail = CommonUtils
					.getDetailsFromTyreInSameAxle(mSelectedTire);
			if (null != detail) {
				mSelectedtyreSize = detail.getSize();
				mSelectedTyreTASP = detail.getAsp();
				mSelectedtyreTRIM = detail.getRim();
				mIsSizeSpinnerEditable = false;
			}
		}
		if (Constants.onBrandBool
				|| Constants.SELECTED_TYRE.getSerialNumber().equalsIgnoreCase(
						"") || TextUtils.isEmpty(mSelectedTyreDetailedDesign)) {
			loadTyreDetailsOnBrandCorrection();
		} else {
			loadTyreDetails();
		}
	}

	/**
	 * Method loading and setting the full tire details for the corresponding
	 * selected tire
	 */
	private void loadTyreDetails() {
		loadSelectedBrand();
		loadSelectedSize();
		loadSelectedASP();
		loadSelectedRIM();
		loadSelectedDesign();
		loadSelectedDesignDetails();
	}

	/**
	 * NSK VAlidation Class
	 * 
	 * @author amitkumar.h
	 * 
	 */
	private class nskOneBeforeChanged implements TextWatcher {
		@Override
		public void afterTextChanged(Editable nskOne) {
			// set before nsk when nsk1 is changed
			mValueNSK2.setText(nskOne.toString());
			mValueNSK3.setText(nskOne.toString());
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mValueNSK1.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {
				} else {
					mValueNSK1.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mValueNSK1.setText("");
			}
		}
	}

	/**
	 * @author amitkumar.h Class handling auto-populate functionality by
	 *         implementing TextWatcher for NSK fields
	 */
	private class nskTwoBeforeChanged implements TextWatcher {
		@Override
		public void afterTextChanged(Editable nskTwo) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mValueNSK2.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {

				} else {
					mValueNSK2.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mValueNSK2.setText("");
			}
		}
	}

	private class nskThreeBeforeChanged implements TextWatcher {
		@Override
		public void afterTextChanged(Editable nskThree) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mValueNSK3.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {

				} else {

					mValueNSK3.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mValueNSK3.setText("");
			}
		}
	}

	/**
	 * Method Getting the tire details from the local db3 file for the selected
	 * tire as per Tire Rules
	 */
	public void getTyreDetailsForSerialCorrrection() {
		Cursor tireDetailsCursor = null;
		try {
			DatabaseAdapter mDbHelper = new DatabaseAdapter(this);
			mDbHelper.createDatabase();
			mDbHelper.open();
			tireDetailsCursor = mDbHelper.getBrandNameForSWAP();
			mbrandArrayList = new ArrayList<String>();
			if (CursorUtils.isValidCursor(tireDetailsCursor)) {
				tireDetailsCursor.moveToFirst();
				while (!tireDetailsCursor.isAfterLast()) {
					mbrandArrayList.add(tireDetailsCursor
							.getString(tireDetailsCursor
									.getColumnIndex("Description"))); // add the
																		// item
					tireDetailsCursor.moveToNext();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(tireDetailsCursor);
		}
	}

	/**
	 * Method loading Brand Name on UI element as per the selected tire
	 */
	private void loadSelectedBrand() {
		mbrandArrayList.clear();
		if (!TextUtils.isEmpty(mSelectedTire.getBrandName())) {
			mbrandArrayList.add(mSelectedTire.getBrandName());
		}
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mbrandArrayList);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mBrandSpinner.setAdapter(dataAdapter);
		mBrandSpinner.setEnabled(false);
	}

	/**
	 * Method loading tire-size on UI element as per the selected tire
	 */
	private void loadSelectedSize() {
		mSizeArrayList.clear();
		if (!TextUtils.isEmpty(mSelectedtyreSize)) {
			mSizeArrayList.add(mSelectedtyreSize);
		}
		mSizeDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mSizeArrayList);
		mSizeDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSizeSpinner.setAdapter(mSizeDataAdapter);
		if (Constants.onBrandDesignBool) {
			mSizeSpinner.setEnabled(true);
		} else {
			mSizeSpinner.setEnabled(false);
		}
	}

	/**
	 * Method loading tire-ASP on UI element as per the selected tire
	 */
	private void loadSelectedASP() {
		mTyreTASPArrayList.clear();
		if (!TextUtils.isEmpty(mSelectedTyreTASP)) {
			mTyreTASPArrayList.add(mSelectedTyreTASP);
		}
		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTASPSpinner.setAdapter(mASPDataAdapter);
		if (Constants.onBrandDesignBool) {
			mTyreTASPSpinner.setEnabled(true);
		} else {
			mTyreTASPSpinner.setEnabled(false);
		}
	}

	/**
	 * Method loading tire-RIM on UI element as per the selected tire
	 */
	private void loadSelectedRIM() {
		mTyreTRIMArrayList.clear();
		if (!TextUtils.isEmpty(mSelectedtyreTRIM)) {
			mTyreTRIMArrayList.add(mSelectedtyreTRIM);
		}
		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTRIMSpinner.setAdapter(mRimDataAdapter);
		if (Constants.onBrandDesignBool) {
			mTyreTRIMSpinner.setEnabled(true);
		} else {
			mTyreTRIMSpinner.setEnabled(false);
		}
	}

	/**
	 * Method loading tire-Design on UI element as per the selected tire
	 */
	private void loadSelectedDesign() {
		mDesignArrayList.clear();
		if (!TextUtils.isEmpty(mSelectedTire.getDesign())) {
			mDesignArrayList.add(mSelectedTire.getDesign());
		}
		mDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mDesignArrayList);
		mDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreDesignSpinner.setAdapter(mDesignDataAdapter);
		mTyreDesignSpinner.setEnabled(false);
	}

	/**
	 * Method loading tire-Design-Details on UI element as per the selected tire
	 */
	private void loadSelectedDesignDetails() {
		mFullDesignArrayList.clear();
		if (!TextUtils.isEmpty(mSelectedTire.getDesignDetails())) {
			mFullDesignArrayList.add(mSelectedTire.getDesignDetails());
		}
		mFullDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mFullDesignArrayList);
		mFullDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreFullDetailSpinner.setAdapter(mFullDesignDataAdapter);
		mTyreFullDetailSpinner.setEnabled(false);
	}

	/**
	 * Method loading tire BrandName from the local DB3 file and populating on
	 * UI element as per the selected tire
	 */
	private void loadTyreDetailsOnBrandCorrection() {
		Cursor mCursor = null;
		try {
			mDbHelper.open();
			mCursor = mDbHelper.getBrandNameForSWAP();
			if (!CursorUtils.isValidCursor(mCursor)) {
				return;
			}
			mCursor.moveToFirst();
			mbrandArrayList.clear();
			mbrandArrayList.add(mPleaseSelectBrand);
			while (!mCursor.isAfterLast()) {
				mbrandArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("Description")));
				mCursor.moveToNext();
			}
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mbrandArrayList);
			dataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mBrandSpinner.setAdapter(dataAdapter);
			mBrandSpinner.setEnabled(true);
			for (int i = 0; i < mbrandArrayList.size(); i++) {
				if (mbrandArrayList.get(i).equals(mSelectedBrandName)) {
					mBrandSpinner.setSelection(i);
					break;
				}
			}
			mBrandSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							mBrandSpinner
									.setOnItemSelectedListener(listenerSelectBrandName);
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
							CommonUtils.notify(Constants.sLblNothingSelected,
									getApplicationContext());
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}

	OnItemSelectedListener listenerSelectBrandName = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			mSelectedBrandName = mBrandSpinner.getSelectedItem().toString();
			if (mSelectedBrandName.equalsIgnoreCase(mPleaseSelectBrand)) {
				mTyreFullDetailSpinner.setEnabled(false);
				mTyreDesignSpinner.setEnabled(false);
			} else {
				mTyreFullDetailSpinner.setEnabled(true);
				mTyreDesignSpinner.setEnabled(true);
			}
			// If source non maintained and size 0 and ""
			if (Constants.SECOND_SELECTED_TYRE != null
					&& (mIsSizeSpinnerEditable
							&& (Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED || Constants.SECOND_SELECTED_TYRE
									.getTyreState() == TyreState.NON_MAINTAINED) && (mSelectedtyreSize
							.equalsIgnoreCase("0") || mSelectedtyreSize
							.equalsIgnoreCase("")))) {
				clearPreviousSelections(2);
				if (0 < mBrandSpinner.getSelectedItemPosition()) {
					getTyreSizes();
				} else {
					clearSpinnersData(2);
				}
			}
			if ((mIsSizeSpinnerEditable
					&& Constants.SELECTED_TYRE.getTyreState() != TyreState.NON_MAINTAINED && mSelectedtyreSize
						.equalsIgnoreCase("0"))
					|| (Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED
							&& Constants.ONLONG_PRESS && (mSelectedtyreSize
							.equalsIgnoreCase("0") || mSelectedtyreSize
							.equalsIgnoreCase("")))) {
				clearPreviousSelections(2);
				if (0 < mBrandSpinner.getSelectedItemPosition()) {
					getTyreSizes();
				} else {
					clearSpinnersData(2);
				}
			}
			if (Constants.SELECTED_TYRE != null
					&& Constants.SECOND_SELECTED_TYRE != null
					&& Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED
					&& Constants.SECOND_SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED
					&& (mSelectedtyreSize.equalsIgnoreCase("0") || mSelectedtyreSize
							.equalsIgnoreCase(""))) {
				clearPreviousSelections(2);
				if (0 < mBrandSpinner.getSelectedItemPosition()) {
					getTyreSizes();
				} else {
					clearSpinnersData(2);
				}
			} else {
				clearPreviousSelections(5);
				if (0 < mBrandSpinner.getSelectedItemPosition()) {
					getTyreSizes();
					getTyreDesign();
				} else {
					clearSpinnersData(5);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView arg0) {
			CommonUtils.notify(Constants.sLblNothingSelected,
					getApplicationContext());
		}
	};

	/**
	 * Method Getting Tire Sizes from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreSizes() {
		Cursor mTyreSizeCursor = null;
		try {
			mDbHelper.open();
			mTyreSizeCursor = mDbHelper
					.getTyreSizeFromBrand(mSelectedBrandName);
			if (CursorUtils.isValidCursor(mTyreSizeCursor)) {
				mTyreSizeCursor.moveToFirst();
				mSizeArrayList.clear();
				while (!mTyreSizeCursor.isAfterLast()) {
					mSizeArrayList
							.add(mTyreSizeCursor.getString(mTyreSizeCursor
									.getColumnIndex("TSize")));
					mTyreSizeCursor.moveToNext();
				}
				loadTyreSize();
			} else {
				clearSpinnersData(2);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mTyreSizeCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Method populating Tire Size after getting from local DB3 file as per the
	 * selected tire-brand
	 */
	private void loadTyreSize() {
		try {
			mSizeDataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mSizeArrayList);
			mSizeDataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSizeSpinner.setAdapter(mSizeDataAdapter);
			if (Constants.contract.getIsDIffSizeAllowedForVehicle().equals(
					"false")) {
				mSizeSpinner.setEnabled(true);
			}
			for (int i = 0; i < mSizeArrayList.size(); i++) {
				if (mSizeArrayList.get(i).equals(mSelectedtyreSize)) {
					mSizeSpinner.setSelection(i);
					break;
				}
			}
			mSizeSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							mSelectedtyreSize = mSizeSpinner.getSelectedItem()
									.toString();
							clearPreviousSelections(3);
							if (0 <= mSizeSpinner.getSelectedItemPosition()) {
								getTyreASP();
							} else {
								clearSpinnersData(3);
							}
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {

						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method Getting Tire ASP from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreASP() {
		Cursor mTyreASPCursor = null;
		try {
			mDbHelper.open();
			mTyreASPCursor = mDbHelper.getTyreASPFromSize(mSelectedtyreSize,
					mSelectedBrandName);
			if (CursorUtils.isValidCursor(mTyreASPCursor)) {
				mTyreASPCursor.moveToFirst();
				mTyreTASPArrayList.clear();
				while (!mTyreASPCursor.isAfterLast()) {
					mTyreTASPArrayList.add(mTyreASPCursor
							.getString(mTyreASPCursor.getColumnIndex("TASP")));
					mTyreASPCursor.moveToNext();
				}
				loadTyreTASP();
			} else {
				clearSpinnersData(3);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mTyreASPCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Method populating Tire ASP after getting from local DB3 file as per the
	 * selected tire-brand
	 */
	private void loadTyreTASP() {

		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTASPSpinner.setAdapter(mASPDataAdapter);
		if (Constants.contract.getIsDIffSizeAllowedForVehicle().equals("false")) {
			mTyreTASPSpinner.setEnabled(true);
		}
		for (int i = 0; i < mTyreTASPArrayList.size(); i++) {
			if (mTyreTASPArrayList.get(i).equals(mSelectedTyreTASP)) {
				mTyreTASPSpinner.setSelection(i);
				break;
			}
		}
		mTyreTASPSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedTyreTASP = mTyreTASPSpinner.getSelectedItem()
								.toString();
						clearPreviousSelections(4);
						if (0 <= mTyreTASPSpinner.getSelectedItemPosition()) {
							getTyreRIM();
						} else {
							clearSpinnersData(4);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
					}
				});
	}

	/**
	 * Method Getting Tire RIM from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreRIM() {
		Cursor mTyreRIMCursor = null;
		try {
			mDbHelper.open();
			mTyreRIMCursor = mDbHelper.getTyreRimFromSize(mSelectedtyreSize,
					mSelectedTyreTASP, mSelectedBrandName);
			if (CursorUtils.isValidCursor(mTyreRIMCursor)) {
				mTyreRIMCursor.moveToFirst();
				mTyreTRIMArrayList.clear();
				while (!mTyreRIMCursor.isAfterLast()) {
					mTyreTRIMArrayList.add(mTyreRIMCursor
							.getString(mTyreRIMCursor.getColumnIndex("TRIM")));
					mTyreRIMCursor.moveToNext();
				}
				loadTyreTRIM();
			} else {
				clearSpinnersData(4);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mTyreRIMCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Method populating Tire RIM after getting from local DB3 file as per the
	 * selected tire-brand
	 */
	private void loadTyreTRIM() {

		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTRIMSpinner.setAdapter(mRimDataAdapter);
		if (Constants.contract.getIsDIffSizeAllowedForVehicle().equals("false")) {
			mTyreTRIMSpinner.setEnabled(true);
		}
		for (int i = 0; i < mTyreTRIMArrayList.size(); i++) {
			if (mTyreTRIMArrayList.get(i).equals(mSelectedtyreTRIM)) {
				mTyreTRIMSpinner.setSelection(i);
				break;
			}
		}
		mTyreTRIMSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedtyreTRIM = mTyreTRIMSpinner.getSelectedItem()
								.toString();
						clearPreviousSelections(5);
						if (0 <= mTyreTRIMSpinner.getSelectedItemPosition()) {
							getTyreDesign();
						} else {
							clearSpinnersData(5);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {

					}
				});
	}

	/**
	 * Method Getting Tire Design from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreDesign() {
		Cursor mDesignCursor = null;
		try {
			mDbHelper.open();
			mDesignCursor = mDbHelper.getTyreDesign(mSelectedtyreSize,
					mSelectedTyreTASP, mSelectedtyreTRIM, mSelectedBrandName);
			if (CursorUtils.isValidCursor(mDesignCursor)) {
				mDesignCursor.moveToFirst();
				mDesignArrayList.clear();
				mDesignArrayList.add(mDefaultValueForSpinner);
				while (!mDesignCursor.isAfterLast()) {
					mDesignArrayList.add(mDesignCursor.getString(mDesignCursor
							.getColumnIndex("Deseign")));
					mDesignCursor.moveToNext();
				}
				loadTyreDesign();
			} else {
				clearSpinnersData(5);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mDesignCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Method populating Tire Design after getting from local DB3 file as per
	 * the selected tire-brand
	 */
	private void loadTyreDesign() {

		mDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mDesignArrayList);
		mDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreDesignSpinner.setAdapter(mDesignDataAdapter);
		for (int i = 0; i < mDesignArrayList.size(); i++) {
			if (mDesignArrayList.get(i).equals(mSelectedTyreDesign)) {
				mTyreDesignSpinner.setSelection(i);
				break;
			}
		}
		mTyreDesignSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedTyreDesign = mTyreDesignSpinner
								.getSelectedItem().toString();
						clearPreviousSelections(6);
						if (0 < mTyreDesignSpinner.getSelectedItemPosition()) {
							getTyreDetailedDesign();// type
						} else {
							clearSpinnersData(6);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
					}
				});
	}

	/**
	 * Method Getting Tire Design-Details from local DB3 file as per the
	 * selected tire-brand
	 */
	public void getTyreDetailedDesign() {
		Cursor mDetailDesignCursor = null;
		try {
			mDbHelper.open();
			mDetailDesignCursor = mDbHelper.getTyreDetailedDesign(
					mSelectedtyreSize, mSelectedTyreTASP, mSelectedtyreTRIM,
					mSelectedTyreDesign, mSelectedBrandName);
			if (CursorUtils.isValidCursor(mDetailDesignCursor)) {
				mDetailDesignCursor.moveToFirst();
				mFullDesignArrayList.clear();
				mFullDesignArrayList.add(mDefaultValueForSpinner);
				while (!mDetailDesignCursor.isAfterLast()) {
					mFullDesignArrayList.add(mDetailDesignCursor
							.getString(mDetailDesignCursor
									.getColumnIndex("FullTireDetails")));
					mDetailDesignCursor.moveToNext();
				}
				loadTyreDetailedDesign();
			} else {
				clearSpinnersData(6);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mDetailDesignCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Method populating Tire Design-Details after getting from local DB3 file
	 * as per the selected tire-brand
	 */
	private void loadTyreDetailedDesign() {

		mFullDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mFullDesignArrayList);
		mFullDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreFullDetailSpinner.setAdapter(mFullDesignDataAdapter);
		for (int i = 0; i < mFullDesignArrayList.size(); i++) {
			if (mFullDesignArrayList.get(i).equals(mSelectedTyreDetailedDesign)) {
				mTyreFullDetailSpinner.setSelection(i);
				break;
			}
		}
		mTyreFullDetailSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedTyreDetailedDesign = mTyreFullDetailSpinner
								.getSelectedItem().toString();
						Constants.FULLDESIGN_SELECTED = mSelectedTyreDetailedDesign;
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
					}
				});
	}

	/**
	 * Method Getting Tire SAPMaterialCodeID from local DB3 file as per the
	 * selected DetailedDesign
	 */
	public void getSAPMaterialCodeIDByDetailedDesign() {
		Cursor mCursor = null;
		try {
			mDbHelper.open();
			mCursor = mDbHelper
					.getSAPMaterialCodeIDByDetailedDesign(Constants.FULLDESIGN_SELECTED);
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				mSelectedSAPMaterialCodeIDForNM = mCursor.getString(
						mCursor.getColumnIndex("ID")).toString();
				// For Creating JOC
				if (Constants.OnSerialBool == true) {
					Constants.JOB_CORRECTION_COUNT_SWAP++;
					VehicleSkeletonFragment.updateCorrectedSerialNumberInDB();
				}
				if (Constants.onBrandBool == true
						|| Constants.onBrandBoolForJOC == true) {
					Constants.NEW_SAPMATERIAL_CODE = mSelectedSAPMaterialCodeIDForNM;
					Constants.JOB_CORRECTION_COUNT_SWAP++;
					VehicleSkeletonFragment.updateCorrectedBrandInDB();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Method Getting Tire SAPMaterialCodeID from local DB3 file as per the
	 * selected DetailedDesign and Tire-Dimensions
	 */
	public void getSAPMaterialCodeID() {

		Cursor mCursor = null;
		try {
			mDbHelper.open();
			mCursor = mDbHelper.getSAPMaterialCodeID(mSelectedtyreSize,
					mSelectedTyreTASP, mSelectedtyreTRIM, mSelectedTyreDesign,
					mSelectedTyreDetailedDesign, mSelectedBrandName);
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				mSelectedSAPMaterialCodeID = mCursor.getString(
						mCursor.getColumnIndex("ID")).toString();
				if (Constants.OnSerialBool == true
						&& (Constants.SELECTED_TYRE.getTyreState() != TyreState.NON_MAINTAINED || Constants.SECOND_SELECTED_TYRE
								.getTyreState() != TyreState.NON_MAINTAINED)) {
					VehicleSkeletonFragment.updateCorrectedSerialNumberInDB();
				}
				if (Constants.onBrandBool == true
						&& (Constants.SELECTED_TYRE.getTyreState() != TyreState.NON_MAINTAINED || Constants.SECOND_SELECTED_TYRE
								.getTyreState() != TyreState.NON_MAINTAINED)) {
					Constants.NEW_SAPMATERIAL_CODE = mSelectedSAPMaterialCodeID;
					Constants.TYRE_ID = Constants.SELECTED_TYRE.getExternalID();
					VehicleSkeletonFragment.updateCorrectedBrandInDB();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Method handling the data-updates for the tables(JobItem, tire,
	 * JobCorrection etc) for the selected tire. It checks all the mandatory
	 * fields then allows user to navigate back to Vehicle Skeleton after
	 * updating the data
	 */
	private void onReturnSkeleton() {

		if (Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED.equalsIgnoreCase("")) {
			Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED = Constants.EDITED_SERIAL_NUMBER_SOURCE;
		}
		if (Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE.equalsIgnoreCase("")) {
			Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE = Constants.EDITED_SERIAL_NUMBER_DES;
		}
		mSwiped = true;
		Constants.NSK1_SWAP = mValueNSK1.getText().toString();
		Constants.NSK2_SWAP = mValueNSK2.getText().toString();
		Constants.NSK3_SWAP = mValueNSK3.getText().toString();
		try {
			mLogicalSwapinfo.setNskOne(mValueNSK1.getText().toString());
			mLogicalSwapinfo.setNskTwo(mValueNSK2.getText().toString());
			mLogicalSwapinfo.setNskThree(mValueNSK3.getText().toString());
			mLogicalSwapinfo.setRegrooved(sFlag);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			if (mRimType.isChecked()) {
				mLogicalSwapinfo.setRimType(2);
			} else {
				mLogicalSwapinfo.setRimType(1);
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		// For Brand ::::: Fix:: SPARE-TYRE
		if (!Constants.onBrandBlank
				&& Constants.SECOND_SELECTED_TYRE != null
				&& ((Constants.SECOND_SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED && Constants.LOGICAL_SWAP_INITIAL == false))
				|| (Constants.LOGICAL_SWAP_INITIAL == false
						&& Constants.SELECTED_TYRE.getTyreState() != TyreState.NON_MAINTAINED && Constants.SECOND_SELECTED_TYRE
						.getTyreState() == TyreState.NON_MAINTAINED)) {
			try {
				getSAPMaterialCodeIDByDetailedDesign();
				Constants.SECOND_SELECTED_TYRE
						.setSapMaterialCodeID(mSelectedSAPMaterialCodeIDForNM);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// BUG ::: 652 :: N/R should be allowed for SAP
			if (Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP) ||
					Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP)) {
				Constants.SECOND_SELECTED_TYRE
						.setSerialNumber(Constants.EDITED_SERIAL_NUMBER);
			}
			if (!Constants.SECOND_SELECTED_TYRE.getSerialNumber().equals(
					Constants.EDITED_SERIAL_NUMBER)) {
				Constants.SECOND_SELECTED_TYRE
						.setSerialNumber(Constants.EDITED_SERIAL_NUMBER);
			}
			Constants.SECOND_SELECTED_TYRE.setNsk(mValueNSK1.getText()
					.toString());
			Constants.SECOND_SELECTED_TYRE.setNsk2(mValueNSK2.getText()
					.toString());
			Constants.SECOND_SELECTED_TYRE.setNsk3(mValueNSK3.getText()
					.toString());
			Constants.SECOND_SELECTED_TYRE.setVehicleJob(Constants.contract
					.getVehicleSapCode());
			Constants.SECOND_SELECTED_TYRE.setBrandName(mBrandSpinner
					.getSelectedItem().toString());
			Constants.SECOND_SELECTED_TYRE.setSize(mSizeSpinner
					.getSelectedItem().toString());
			Constants.SECOND_SELECTED_TYRE.setAsp(mTyreTASPSpinner
					.getSelectedItem().toString());
			Constants.SECOND_SELECTED_TYRE.setRim(mTyreTRIMSpinner
					.getSelectedItem().toString());
			// Null check added for Selected Item of design Spinner
			if (null != mTyreDesignSpinner && null != mTyreDesignSpinner.getSelectedItem()) {
				Constants.SECOND_SELECTED_TYRE.setDesign(mTyreDesignSpinner
						.getSelectedItem().toString());
			} else {
				Constants.SECOND_SELECTED_TYRE.setDesign("");
			}
			Constants.SECOND_SELECTED_TYRE
					.setDesignDetails(mTyreFullDetailSpinner.getSelectedItem()
							.toString());
			if (!Boolean.valueOf(Constants.SELECTED_TYRE.isSpare())
					&& Constants.SELECTED_TYRE.getTyreState() != TyreState.NON_MAINTAINED
					&& Constants.SECOND_SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED) {
				Constants.LOGICAL_SWAP_INITIAL = false;
				updateJobItemForSourceTyre();
				updateJobItemForDestinationTyre();
				// Inspection CR:: 447
				InspectionDataHandler.getMyInstance(getApplicationContext()).updateJobitemForInspection(Constants.SELECTED_TYRE,true);
				InspectionDataHandler.getMyInstance(getApplicationContext()).updateJobitemForInspection(Constants.SECOND_SELECTED_TYRE,true);
			} else {
				updateJobItemForDestinationTyre();
				// Inspection CR:: 447
				InspectionDataHandler.getMyInstance(getApplicationContext()).updateJobitemForInspection(Constants.SECOND_SELECTED_TYRE,true);
			}
			// For Brand ::::: Fix:: SPARE-TYRE
		} else if (Constants.onBrandBlank
				|| (Constants.SELECTED_TYRE != null

				&& ((Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED && Constants.LOGICAL_SWAP_INITIAL == true) || (Constants.LOGICAL_SWAP_INITIAL == true && (Constants.SELECTED_TYRE
						.getTyreState() == TyreState.NON_MAINTAINED && Constants.SECOND_SELECTED_TYRE
						.getTyreState() != TyreState.NON_MAINTAINED))))) {
			try {
				getSAPMaterialCodeIDByDetailedDesign();
				Constants.SELECTED_TYRE
						.setSapMaterialCodeID(mSelectedSAPMaterialCodeIDForNM);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (!Constants.SELECTED_TYRE.getSerialNumber().equals(
					Constants.EDITED_SERIAL_NUMBER)) {
				Constants.SELECTED_TYRE
						.setSerialNumber(Constants.EDITED_SERIAL_NUMBER);
			}
			Constants.SELECTED_TYRE.setNsk(mValueNSK1.getText().toString());
			Constants.SELECTED_TYRE.setNsk2(mValueNSK2.getText().toString());
			Constants.SELECTED_TYRE.setNsk3(mValueNSK3.getText().toString());
			Constants.SELECTED_TYRE.setVehicleJob(Constants.contract
					.getVehicleSapCode());
			Constants.SELECTED_TYRE.setBrandName(mBrandSpinner
					.getSelectedItem().toString());
			Constants.SELECTED_TYRE.setSize(mSizeSpinner.getSelectedItem()
					.toString());
			Constants.SELECTED_TYRE.setAsp(mTyreTASPSpinner.getSelectedItem()
					.toString());
			Constants.SELECTED_TYRE.setRim(mTyreTRIMSpinner.getSelectedItem()
					.toString());
			// Null check added for Selected Item of design Spinner
			if (null != mTyreDesignSpinner && null != mTyreDesignSpinner.getSelectedItem()) {
				Constants.SELECTED_TYRE.setDesign(mTyreDesignSpinner
						.getSelectedItem().toString());
			} else {
				Constants.SELECTED_TYRE.setDesign("");
			}
			Constants.SELECTED_TYRE.setDesignDetails(mTyreFullDetailSpinner
					.getSelectedItem().toString());
			Constants.TRIGGER_LOGICAL = true;
			updateJobItemForSourceTyre();
			// Inspection CR:: 447
			InspectionDataHandler.getMyInstance(getApplicationContext()).updateJobitemForInspection(Constants.SELECTED_TYRE,true);
		} else if (Constants.SECOND_SELECTED_TYRE != null
				&& !Constants.SECOND_SELECTED_TYRE.getSerialNumber().equals("")) {
			try {
				getSAPMaterialCodeID();
				Constants.SECOND_SELECTED_TYRE
						.setSapMaterialCodeID(mSelectedSAPMaterialCodeID);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (Constants.LOGICAL_SWAP_INITIAL == false
					|| (!Constants.SECOND_SELECTED_TYRE.getSerialNumber()
							.equals(Constants.EDITED_SERIAL_NUMBER) && !AutoSwapImpl
							.checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER))) {
				Constants.SECOND_SELECTED_TYRE
						.setSerialNumber(Constants.EDITED_SERIAL_NUMBER);
			}

			Constants.SECOND_SELECTED_TYRE.setNsk(mValueNSK1.getText()
					.toString());
			LogUtil.d("LogicalSwapActivity", "NSK1::"
					+ Constants.SECOND_SELECTED_TYRE.getNsk());
			Constants.SECOND_SELECTED_TYRE.setNsk2(mValueNSK2.getText()
					.toString());
			Constants.SECOND_SELECTED_TYRE.setNsk3(mValueNSK3.getText()
					.toString());
			Constants.SECOND_SELECTED_TYRE.setVehicleJob(Constants.contract
					.getVehicleSapCode());
			Constants.SECOND_SELECTED_TYRE.setBrandName(mBrandSpinner
					.getSelectedItem().toString());
			Constants.SECOND_SELECTED_TYRE.setSize(mSizeSpinner
					.getSelectedItem().toString());
			Constants.SECOND_SELECTED_TYRE.setAsp(mTyreTASPSpinner
					.getSelectedItem().toString());
			Constants.SECOND_SELECTED_TYRE.setRim(mTyreTRIMSpinner
					.getSelectedItem().toString());
			if (mTyreDesignSpinner.getSelectedItem() == null) {
				Constants.SECOND_SELECTED_TYRE.setDesign("");
			} else {
				Constants.SECOND_SELECTED_TYRE.setDesign(mTyreDesignSpinner
						.getSelectedItem().toString());
			}
			Constants.SECOND_SELECTED_TYRE
					.setDesignDetails(mTyreFullDetailSpinner.getSelectedItem()
							.toString());

			if (Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED) {
				updateJobItemForDestinationTyre();
				// Inspection CR:: 447
				InspectionDataHandler.getMyInstance(getApplicationContext()).updateJobitemForInspection(Constants.SECOND_SELECTED_TYRE,true);
			} else {
				Constants.LOGICAL_SWAP_INITIAL = false;
				updateJobItemForSourceTyre();
				updateJobItemForDestinationTyre();
				// Inspection CR:: 447
				InspectionDataHandler.getMyInstance(getApplicationContext()).updateJobitemForInspection(Constants.SELECTED_TYRE,true);
				InspectionDataHandler.getMyInstance(getApplicationContext()).updateJobitemForInspection(Constants.SECOND_SELECTED_TYRE,true);
			}

		}
		try {
			CommonUtils.hideKeyboard(this, getWindow().getDecorView()
					.getRootView().getWindowToken());
		} catch (Exception e) {
			LogUtil.i("Mount PWT on activity return",
					"could not close keyboard");
		}
		updateJobItemForDoubleSwap();
	}

	private String minNSKSetForSource(Tyre tyre) {
		float smallest;
		float a = CommonUtils.parseFloat(tyre.getNsk());
		float b = CommonUtils.parseFloat(tyre.getNsk2());
		float c = CommonUtils.parseFloat(tyre.getNsk3());
		if (a < b && a < c) {
			smallest = a;
		} else if (b < c && b < a) {
			smallest = b;
		} else {
			smallest = c;
		}
		return String.valueOf(smallest);
	}

	/**
	 * Method Updating Data for the selected Source tire in JobItem table with
	 * the corresponding SWAP ActionType
	 */
	private void updateJobItemForSourceTyre() {
		try {
			LogUtil.DBLog(TAG,"Job Item Data","Initialized Insertion for Sourse tire");
			int len = VehicleSkeletonFragment.mJobItemList.size();
			int jobID = getJobItemIDForThisJobItem();
			JobItem jobItem = new JobItem();
			jobItem.setActionType("3");
			jobItem.setAxleNumber("0");
			jobItem.setAxleServiceType("0");
			jobItem.setCasingRouteCode("");
			jobItem.setDamageId("");
			jobItem.setExternalId(String.valueOf(UUID.randomUUID()));
			jobItem.setGrooveNumber(null);
			jobItem.setJobId(String.valueOf(jobID));
			jobItem.setMinNSK(minNSKSetForSource(Constants.SELECTED_TYRE));
			jobItem.setNote(sNoteText);
			jobItem.setNskOneAfter(Constants.SELECTED_TYRE.getNsk());
			jobItem.setNskOneBefore("0");
			jobItem.setNskThreeAfter(Constants.SELECTED_TYRE.getNsk3());
			jobItem.setNskThreeBefore("0");
			jobItem.setNskTwoAfter(Constants.SELECTED_TYRE.getNsk2());
			jobItem.setNskTwoBefore("0");
			jobItem.setOperationID("");
			jobItem.setRecInflactionOrignal(Constants.SELECTED_TYRE
					.getPressure());
			jobItem.setRegrooved("False");
			jobItem.setReGrooveNumber(null);
			jobItem.setRegrooveType("0");
			jobItem.setRemovalReasonId("");
			jobItem.setRepairCompany(null);
			if (mRimType.isChecked()) {
				jobItem.setRimType("0");
			} else {
				jobItem.setRimType("0");
			}
			jobItem.setSapCodeTilD("0");
			jobItem.setSequence(String.valueOf(len + 1));
			jobItem.setServiceCount("0");
			jobItem.setServiceID("0");
			jobItem.setSwapType("4");
			jobItem.setThreaddepth("0");
			jobItem.setTyreID(Constants.SELECTED_TYRE.getExternalID());
			jobItem.setWorkOrderNumber(null);
			jobItem.setSwapOriginalPosition(Constants.SELECTED_TYRE
					.getPosition());
			jobItem.setTorqueSettings(String
					.valueOf(Constants.RETORQUEVALUE_SAVED));
			jobItem.setRecInflactionDestination(String
					.valueOf(Constants.FORSWAP_BARVALUE));
			if (Constants.LOGICAL_SWAP_INITIAL == true
					|| (Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED && Constants.SECOND_SELECTED_TYRE
							.getTyreState() != TyreState.NON_MAINTAINED)) {
				jobItem.setSerialNumber(Constants.EDITED_SERIAL_NUMBER);
				jobItem.setBrandName(mBrandSpinner.getSelectedItem().toString());
				jobItem.setFullDesignDetails(mTyreFullDetailSpinner
						.getSelectedItem().toString());
				updateTireImages(String.valueOf(jobID));
			} else {
				jobItem.setSerialNumber(Constants.SELECTED_TYRE
						.getSerialNumber());
				jobItem.setBrandName(Constants.SELECTED_TYRE.getBrandName());
				jobItem.setFullDesignDetails(Constants.SELECTED_TYRE
						.getDesignDetails());
			}
			Constants.INSERT_SWAP_DATA++;
			// SWAPOriginal Pos For Spare
			{
				if (jobItem.getSwapOriginalPosition().contains("SP1")) {
					jobItem.setSwapOriginalPosition("SP1");
				} else if (jobItem.getSwapOriginalPosition().contains("SP2")) {
					jobItem.setSwapOriginalPosition("SP2");
				} else if (jobItem.getSwapOriginalPosition().contains("SP")) {
					jobItem.setSwapOriginalPosition("SP");
				}
			}
			LogUtil.DBLog(TAG,"Job Item Data","Inserted for Source tire"+jobItem.toString() );
			VehicleSkeletonFragment.mJobItemList.add(jobItem);
			LogUtil.DBLog(TAG,"Job Item Data","Inserted Source Tire Data");
			LogUtil.d(TAG, "writing to jobitem - source");
			sNoteText = "";
		} catch (Exception e) {
			LogUtil.DBLog(TAG,"Job Item Data","Exception--"+e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Method Updating Data for the selected Destination tire in JobItem table
	 * with the corresponding SWAP ActionType
	 */
	private void updateJobItemForDestinationTyre() {
		try {
			LogUtil.DBLog(TAG,"Job Item Data","Initialized Insertion for Destination Tire Data");
			int jobID = getJobItemIDForThisJobItem();
			int len = VehicleSkeletonFragment.mJobItemList.size();
			JobItem jobItem = new JobItem();
			jobItem.setActionType("3");
			jobItem.setAxleNumber("0");
			jobItem.setAxleServiceType("0");
			jobItem.setCasingRouteCode("");
			jobItem.setDamageId("");
			jobItem.setExternalId(String.valueOf(UUID.randomUUID()));//
			jobItem.setGrooveNumber(null);
			jobItem.setJobId(String.valueOf(jobID));
			jobItem.setMinNSK(minNSKSet());
			jobItem.setNote(sNoteText);//
			jobItem.setNskOneAfter(mValueNSK1.getText().toString());
			jobItem.setNskOneBefore("0");
			jobItem.setNskThreeAfter(mValueNSK3.getText().toString());
			jobItem.setNskThreeBefore("0");
			jobItem.setNskTwoAfter(mValueNSK2.getText().toString());
			jobItem.setNskTwoBefore("0");
			jobItem.setOperationID("");
			jobItem.setRecInflactionOrignal(Constants.SECOND_SELECTED_TYRE
					.getPressure());
			if (sFlag) {
				jobItem.setRegrooved("True");
			} else {
				jobItem.setRegrooved("False");
			}
			jobItem.setReGrooveNumber(null);
			jobItem.setRegrooveType("0");
			jobItem.setRemovalReasonId("");
			jobItem.setRepairCompany(null);
			jobItem.setSwapOriginalPosition(Constants.SECOND_SELECTED_TYRE
					.getPosition());
			if (mRimType.isChecked()) {
				jobItem.setRimType("2");
			} else {
				jobItem.setRimType("1");
			}
			jobItem.setSapCodeTilD("0");
			jobItem.setSequence(String.valueOf(len + 1));
			jobItem.setServiceCount("0");
			jobItem.setServiceID("0");
			jobItem.setSwapType("4");
			jobItem.setThreaddepth("0");
			jobItem.setTyreID(Constants.SECOND_SELECTED_TYRE.getExternalID());
			jobItem.setWorkOrderNumber(null);
			jobItem.setTorqueSettings(String
					.valueOf(Constants.RETORQUEVALUE_SAVED));
			jobItem.setRecInflactionDestination(String
					.valueOf(Constants.FORSWAP_BARVALUE));
			// Double Swap
			if (!Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED
					.equalsIgnoreCase("")
					|| !Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE
							.equalsIgnoreCase("")) {
				jobItem.setSerialNumber(Constants.SECOND_SELECTED_TYRE
						.getSerialNumber());
				jobItem.setBrandName(Constants.SECOND_SELECTED_TYRE
						.getBrandName());
				jobItem.setFullDesignDetails(Constants.SECOND_SELECTED_TYRE
						.getDesignDetails());
			} else {
				if ((Constants.SELECTED_TYRE.getTyreState() == TyreState.NON_MAINTAINED && Constants.SECOND_SELECTED_TYRE
						.getTyreState() != TyreState.NON_MAINTAINED)) {
					jobItem.setSerialNumber(Constants.SECOND_SELECTED_TYRE
							.getSerialNumber());
					jobItem.setBrandName(Constants.SECOND_SELECTED_TYRE
							.getBrandName());
					jobItem.setFullDesignDetails(Constants.SECOND_SELECTED_TYRE
							.getDesignDetails());
				} else {
					jobItem.setSerialNumber(Constants.EDITED_SERIAL_NUMBER);
					jobItem.setBrandName(mBrandSpinner.getSelectedItem()
							.toString());
					jobItem.setFullDesignDetails(mTyreFullDetailSpinner
							.getSelectedItem().toString());
				}
			}
			// SWAPOriginal Pos For Spare
			{
				if (jobItem.getSwapOriginalPosition().contains("SP1")) {
					jobItem.setSwapOriginalPosition("SP1");
				} else if (jobItem.getSwapOriginalPosition().contains("SP2")) {
					jobItem.setSwapOriginalPosition("SP2");
				} else if (jobItem.getSwapOriginalPosition().contains("SP")) {
					jobItem.setSwapOriginalPosition("SP");
				}
			}
			LogUtil.DBLog(TAG,"Job Item Data","Inserted for Destination tire"+jobItem.toString() );
			VehicleSkeletonFragment.mJobItemList.add(jobItem);
			LogUtil.DBLog(TAG,"Job Item Data","Inserted Destination Tire Data");
			Log.i(TAG, "writing to jobitem - dest");
			sNoteText = "";
			updateTireImages(String.valueOf(jobID));
		} catch (Exception e) {
			LogUtil.DBLog(TAG,"Job Item Data","Exception--"+e.getMessage());
			System.out.println("Amit" + e);
		}
	}

	/**
	 * Method handling image capture and image data-updates for Tire Image table
	 * It upgrades the quality of captured images as well
	 */
	private void updateTireImages(String id) {
		if (Constants.sBitmapPhoto1 != null) {
			TireImageItem item1 = new TireImageItem();
			item1.setJobItemId(id);
			if (mCameraEnabled)
				item1.setImageType("0");
			else
				item1.setImageType("1");
			item1.setDamageDescription(Constants.DAMAGE_NOTES1);
			item1.setTyreImage(CameraUtility.getByteFromBitmap(
					Constants.sBitmapPhoto1, Constants.sBitmapPhoto1Quality));
			item1.setJobExternalId(external_id);
			VehicleSkeletonFragment.mTireImageItemList.add(item1);
		}
		if (Constants.sBitmapPhoto2 != null) {
			TireImageItem item2 = new TireImageItem();
			item2.setJobItemId(id);
			item2.setImageType("1");
			item2.setDamageDescription(Constants.DAMAGE_NOTES2);
			item2.setTyreImage(CameraUtility.getByteFromBitmap(
					Constants.sBitmapPhoto2, Constants.sBitmapPhoto2Quality));
			item2.setJobExternalId(external_id);
			VehicleSkeletonFragment.mTireImageItemList.add(item2);
		}
		if (Constants.sBitmapPhoto3 != null) {
			TireImageItem item3 = new TireImageItem();
			item3.setJobItemId(id);
			item3.setImageType("1");
			item3.setDamageDescription(Constants.DAMAGE_NOTES3);
			item3.setTyreImage(CameraUtility.getByteFromBitmap(
					Constants.sBitmapPhoto3, Constants.sBitmapPhoto3Quality));
			item3.setJobExternalId(external_id);
			VehicleSkeletonFragment.mTireImageItemList.add(item3);
		}
	}

	/**
	 * Method returning maximum count of Job present in the JobItem table
	 * 
	 * @return: Max Job Count
	 */
	private int getJobItemIDForThisJobItem() {
		int countJobItemsNotPresent = 0;
		mDbHelper.open();
		int currentJobItemCountInDB = mDbHelper.getJobItemCount();
		Cursor cursor = null;
		try {
			cursor = mDbHelper.getJobItemValues();
			if (!CursorUtils.isValidCursor(cursor)) {
				return countJobItemsNotPresent + 1
						+ VehicleSkeletonFragment.mJobItemList.size();
			}
			for (JobItem jobItem : VehicleSkeletonFragment.mJobItemList) {
				boolean flag = false;
				for (boolean hasItem = cursor.moveToFirst(); hasItem; hasItem = cursor
						.moveToNext()) {
					if (jobItem.getExternalId().equals(
							cursor.getString(cursor
									.getColumnIndex("ExternalID")))) {
						countJobItemsNotPresent++;
						break;
					}
				}
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			mDbHelper.close();
			// CursorUtils.closeCursor(cursor);
		}
		return currentJobItemCountInDB - countJobItemsNotPresent
				+ VehicleSkeletonFragment.mJobItemList.size() + 1;
	}

	/**
	 * Method returning minimum value of NSK(thread-depth) among the three
	 * captured NSKs
	 * 
	 * @return: Minimum value of NSK(thread-depth)
	 */
	private String minNSKSet() {
		float smallest;
		float a = CommonUtils.parseFloat(mValueNSK1.getText().toString());
		float b = CommonUtils.parseFloat(mValueNSK2.getText().toString());
		float c = CommonUtils.parseFloat(mValueNSK3.getText().toString());
		if (a < b && a < c) {
			smallest = a;
		} else if (b < c && b < a) {
			smallest = b;
		} else {
			smallest = c;
		}
		return String.valueOf(smallest);
	}

	/**
	 * Method Capturing the dialog state before orientation change
	 */
	private void saveDialogState(Bundle state) {
		state.putBoolean("mBackAlertDialog",
				(mBackPressDialog != null && mBackPressDialog.isShowing()));
		state.putBoolean("mChangeMade", mChangeMade);
	}

	/**
	 * Method Capturing the dialog state after orientation change
	 */
	private void restoreDialogState(Bundle state) {
		if (state != null) {
			mChangeMade = state.getBoolean("mChangeMade");
			if (state.getBoolean("mBackAlertDialog")) {
				actionBackPressed();
			}
		}
	}

	@Override
	protected void onDestroy() {
		try {
			if (mBackPressDialog != null && mBackPressDialog.isShowing()) {
				mBackPressDialog.dismiss();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		actionBackPressed();

	}

	/**
	 * Method handling the action performed where user presses the back button
	 * Alert Box created with two option: YES and NO
	 */
	private void actionBackPressed() {
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", true, true, false);
		try {
			if (mChangeMade) {
				sNoteText = "";
			}
			LayoutInflater li = LayoutInflater
					.from(LogicalSwapDifferentSize.this);
			View promptsView = li.inflate(R.layout.dialog_logout_confirmation,
					null);
			final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
					LogicalSwapDifferentSize.this);
			alertDialogBuilder.setView(promptsView);
			TextView infoTv = (TextView) promptsView
					.findViewById(R.id.logout_msg);
			infoTv.setText(mBackPressMessage);
			// set user activity for
			LinearLayout rootNode = (LinearLayout) promptsView
					.findViewById(R.id.layout_root);
			rootNode.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					InactivityUtils.updateActivityOfUser();
				}
			});
			alertDialogBuilder.setCancelable(false).setPositiveButton(
					mYESlabelFromDB, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							// set user activity
							alertDialogBuilder.updateInactivityForDialog();
							LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", true, true, false);
							if (Constants.INSERT_SWAP_DATA == 1) {
								if (VehicleSkeletonFragment.mJobItemList.size() > 0) {
									// Swap cancellation issue with Inspection
									SharedPreferences preferences = getApplicationContext().getSharedPreferences(
											Constants.GOODYEAR_CONF, 0);
									if (!TextUtils.isEmpty(preferences.getString(Constants.INSPECTION, ""))) {
										Constants.INSPECTION_ENABLE = (preferences.getString(Constants.INSPECTION, ""));
										if (Constants.INSPECTION_ENABLE.equalsIgnoreCase("ON")) {
											VehicleSkeletonFragment.mJobItemList.remove(VehicleSkeletonFragment.mJobItemList
													.size() - 1);
											if(!Constants.IS_TYRE_INSPECTED) {
												VehicleSkeletonFragment.mJobItemList.remove(VehicleSkeletonFragment.mJobItemList
														.size() - 1);
											}
										}else{
											VehicleSkeletonFragment.mJobItemList.remove(VehicleSkeletonFragment.mJobItemList
													.size() - 1);
										}
									}
									Constants.INSERT_SWAP_DATA = 0;
								}
								VehicleSkeletonFragment.resetTyreState();
							}
							// Removing JobCorrection on cancel
							if (Constants.JOB_CORRECTION_COUNT_SWAP == 2) {
								if (VehicleSkeletonFragment.mJobcorrectionList
										.size() >= 2) {
									VehicleSkeletonFragment.mJobcorrectionList
											.remove(VehicleSkeletonFragment.mJobcorrectionList
													.size() - 1);
									VehicleSkeletonFragment.mJobcorrectionList
											.remove(VehicleSkeletonFragment.mJobcorrectionList
													.size() - 1);
								}
								Constants.JOB_CORRECTION_COUNT_SWAP = 0;
							} else if (Constants.JOB_CORRECTION_COUNT_SWAP == 1) {
								if (VehicleSkeletonFragment.mJobcorrectionList
										.size() >= 1) {
									VehicleSkeletonFragment.mJobcorrectionList
											.remove(VehicleSkeletonFragment.mJobcorrectionList
													.size() - 1);
								}
								Constants.JOB_CORRECTION_COUNT_SWAP = 0;
							}
							Constants.JOB_CORRECTION_LIST = VehicleSkeletonFragment.mJobcorrectionList;
							// Handling inspection status on back press
							if(Constants.TyreIdListToHandlerBackPress!=null) {
								for (int x = 0; x < Constants.TyreIdListToHandlerBackPress.size(); x++) {
									if(Constants.SELECTED_TYRE.getExternalID().equalsIgnoreCase(Constants.TyreIdListToHandlerBackPress.get(x)))
									{
										Constants.SELECTED_TYRE.setIsInspected(false);
										break;
									}
								}
							}
							CameraUtility.resetParams();
							VehicleSkeletonFragment.resetBoolValue();
							LogicalSwapDifferentSize.this.finish();
						}
					});
			alertDialogBuilder.setCancelable(false).setNegativeButton(
					mNOlabelFromDB, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							// set user activity
							alertDialogBuilder.updateInactivityForDialog();
							LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", true, true, false);
							dialog.cancel();
						}
					});
			mBackPressDialog = alertDialogBuilder.create();
			mBackPressDialog.show();
			mBackPressDialog.setCanceledOnTouchOutside(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method creating the action-bar menu Items
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.regroove_tire_operation, menu);
		if (mNotesLabel != null) {
			menu.findItem(R.id.action_save).setTitle(mNotesLabel);
		}
		return true;
	}

	/**
	 * Method handling the action performed on Action-bar menu
	 */

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_save) {
			LogUtil.TraceInfo(TRACE_TAG, "Option", "Save", false, true, false);
			Intent startNotesActivity = new Intent(this, NoteActivity.class);
			startNotesActivity.putExtra("sentfrom", "regroove");
			startNotesActivity.putExtra("lastnote", sNoteText);
			startActivityForResult(startNotesActivity,
					LogicalSwapDifferentSize.NOTES_INTENT);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == LogicalSwapDifferentSize.NOTES_INTENT) {
			if (data != null) {
				sNoteText = data.getExtras().getString("mNoteEditText");
			}
		}
	}

	/**
	 * @author amitkumar.h Class providing the animation when user swipes out
	 *         from the activity It also handles the actions performed when use
	 *         is swiping out from the activity after matching all the required
	 *         conditions
	 */
	public class mSwiper implements OnTouchListener, OnClickListener {
		float startX, startY;
		float endX, endY;

		int selectedPositionToDelete;
		ArrayAdapter<String> adapterList;
		ScrollView view;

		private Context ctx;

		public static final float MINIMUM_MOVEMENT_REQUIRED = 100;

		public mSwiper(Context ctx, ScrollView view) {
			this.ctx = ctx;
			this.view = view;
			view.setOnClickListener(this);
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getActionMasked()) {
			case MotionEvent.ACTION_DOWN:
				mSwiped = false;
				startX = event.getX();
				startY = event.getY();
				break;
			case MotionEvent.ACTION_UP:
				break;
			case MotionEvent.ACTION_MOVE:
				endX = event.getX();
				endY = event.getY();
				if (Math.abs(endX - startX) > MINIMUM_MOVEMENT_REQUIRED
						&& !mSwiped) {
					try {
						mSwiped = true;
						Rect rect = new Rect();
						int childCount = view.getChildCount();
						int[] listViewCoords = new int[2];
						view.getLocationOnScreen(listViewCoords);
						int x = (int) event.getRawX() - listViewCoords[0];
						int y = (int) event.getRawY() - listViewCoords[1];
						View child;
						for (int i = 0; i < childCount; i++) {
							child = view.getChildAt(i);
							child.getHitRect(rect);
							if (rect.contains(x, y)) {
								String mNSK1Str = mValueNSK1.getText()
										.toString();
								String mNSK2Str = mValueNSK2.getText()
										.toString();
								String mNSK3Str = mValueNSK3.getText()
										.toString();
								float mNSK1 = 0;
								float mNSK2 = 0;
								float mNSK3 = 0;
								int checkedRadioButtonId = mGroupRegrooveType
										.getCheckedRadioButtonId();
								if (!TextUtils.isEmpty(mNSK1Str)) {
									mNSK1 = CommonUtils.parseFloat(mNSK1Str);
								}
								if (!TextUtils.isEmpty(mNSK2Str)) {
									mNSK2 = CommonUtils.parseFloat(mNSK2Str);
								}
								if (!TextUtils.isEmpty(mNSK3Str)) {
									mNSK3 = CommonUtils.parseFloat(mNSK3Str);
								}
								if (mBrandSpinner.isEnabled()
										&& mBrandSpinner
												.getSelectedItemPosition() == 0) {
									mDbHelper.open();
									String message = mDbHelper
											.getLabel(Constants.SELECT_BRAND);
									LogUtil.TraceInfo(TRACE_TAG,"none","Msg : " +message ,false,false,false);
									CommonUtils.notify(message,
											LogicalSwapDifferentSize.this);
								} else if (mTyreFullDetailSpinner
										.getSelectedItemPosition() < 0
										|| mTyreFullDetailSpinner
												.getSelectedItem()
												.toString()
												.equals(mDefaultValueForSpinner)) {
									mDbHelper.open();
									String message = mDbHelper.getLabel(82);
									LogUtil.TraceInfo(TRACE_TAG,"none","Msg : " +message ,false,false,false);
									CommonUtils.notify(message,
											LogicalSwapDifferentSize.this);
								} else if (mNSK1 <= 0 || mNSK2 <= 0
										|| mNSK3 <= 0) {
									LogUtil.TraceInfo(TRACE_TAG,"none","Msg : " +Constants.sLblEnterNskValues ,false,false,false);
									CommonUtils.notify(
											Constants.sLblEnterNskValues, ctx);
								} else if (checkedRadioButtonId == -1) {
									LogUtil.TraceInfo(TRACE_TAG,"none","Msg : " +mREGROOVEREQUIRED ,false,false,false);
									CommonUtils.notify(mREGROOVEREQUIRED, ctx);
									sIsRadioBtnChecked = false;
								} else {
									startPressureDialog(0, null);
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						mDbHelper.close();
					}
				}
				break;
			}
			return false;
		}

		@Override
		public void onClick(View arg0) {
		}
	}

	/**
	 * When spinner value is null then dependent spinner values refreshing.
	 */
	private void clearSpinnersData(int i) {
		// Don't add break statement, It's sequence of execution
		switch (i) {
		case 1: // Brand Spinner
		case 2: // Size Spinner
			mSizeArrayList.clear();
			if (null != mSizeDataAdapter) {
				mSizeDataAdapter.notifyDataSetChanged();
			}
		case 3: // ASP Spinner
			mTyreTASPArrayList.clear();
			if (null != mASPDataAdapter) {
				mASPDataAdapter.notifyDataSetChanged();
			}
		case 4: // Rim Spinner
			mTyreTRIMArrayList.clear();
			if (null != mRimDataAdapter) {
				mRimDataAdapter.notifyDataSetChanged();
			}
		case 5: // Design Spinner
			mDesignArrayList.clear();
			if (null != mDesignDataAdapter) {
				mDesignDataAdapter.notifyDataSetChanged();
			}
		case 6: // FullDetails Spinner
			mFullDesignArrayList.clear();
			if (mFullDesignDataAdapter != null) {
				mFullDesignDataAdapter.notifyDataSetChanged();
			}
		}
	}

	/**
	 * Method Clearing the Previous Selection Values of the spinners present in
	 * the UI
	 */
	private void clearPreviousSelections(int i) {
		if (mIsOrientationChanged && i < mLastSpinnerSelection) {
			if (i == 6 && mLastSpinnerSelection == 7) {
				mIsOrientationChanged = false;
			}
			return;
		} else if (mIsOrientationChanged) {
			mIsOrientationChanged = false;
		}
		// Don't add break statement, It's sequence of execution
		switch (i) {
		case 1: // Brand Spinner
		case 2: // Size Spinner
			mSelectedtyreSize = "";
		case 3: // ASP Spinner
			mSelectedTyreTASP = "";
		case 4: // Rim Spinner
			mSelectedtyreTRIM = "";
		case 5: // Design Spinner
			mSelectedTyreDesign = "";
		case 6: // FullDetails Spinner
			mSelectedTyreDetailedDesign = "";
		}
	}

	/**
	 * Method Checking the Previous Selected Spinner just before the change in
	 * orientation
	 */
	private void checkLatestSpinnerSelectionBeforeOrientationChanges() {
		if (null == mSelectedBrandName
				|| mPleaseSelectBrand.equals(mSelectedBrandName)) {
			mLastSpinnerSelection = 1;
		} else if (null == mSelectedtyreSize || "".equals(mSelectedtyreSize)) {
			mLastSpinnerSelection = 2;
		} else if (null == mSelectedTyreTASP || "".equals(mSelectedTyreTASP)) {
			mLastSpinnerSelection = 3;
		} else if (null == mSelectedtyreTRIM || "".equals(mSelectedtyreTRIM)) {
			mLastSpinnerSelection = 4;
		} else if (null == mSelectedTyreDesign
				|| mDefaultValueForSpinner.equals(mSelectedTyreDesign)) {
			mLastSpinnerSelection = 5;
		} else if (null == mSelectedTyreDetailedDesign
				|| mDefaultValueForSpinner.equals(mSelectedTyreDetailedDesign)) {
			mLastSpinnerSelection = 6;
		} else {
			mLastSpinnerSelection = 7;
		}
	}

	/**
	 * Method handling the Setting up of data on the UI elements(Spinners,labels
	 * etc) for Auto-Logical Swap
	 */
	public void setDataForAutoSwap() {
		if (!TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
				&& !TextUtils.isEmpty(Constants.tempTyrePositionNew)
				&& AutoSwapImpl
						.checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)
				&& VehicleSkeletonFragment.getTireByPosition(
						Constants.tempTyrePositionNew).getTyreState() != TyreState.EMPTY
				&& !Constants.tempTyreSerial
						.equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
				&& !TextUtils.isEmpty(Constants.tempTyrePositionNew)) {

			Tyre mAutoSwappedTire = VehicleSkeletonFragment
					.getTireByPosition(Constants.tempTyrePositionNew);
			mBrandForAutoSwap = mAutoSwappedTire.getBrandName();
			mSerialNumberForAutoSwap = mAutoSwappedTire.getSerialNumber();
			mSizeForAutoSwap = mAutoSwappedTire.getSize();
			mRimForAutoSwap = mAutoSwappedTire.getRim();
			mAspForAutoSwap = mAutoSwappedTire.getAsp();
			mDesignForAutoSwap = mAutoSwappedTire.getDesign();
			mDesignDetailsForAutoSwap = mAutoSwappedTire.getDesignDetails();
			mNskForAutoSwap = mAutoSwappedTire.getNsk();
			mNsk2ForAutoSwap = mAutoSwappedTire.getNsk2();
			mNsk3ForAutoSwap = mAutoSwappedTire.getNsk3();
			// For NA type Vehicle
			if (TextUtils.isEmpty(mBrandForAutoSwap)) {
				mSelectedBrandName = mBrandForAutoSwap;
				mSelectedTyreDesign = mDesignForAutoSwap;
				mSelectedTyreDetailedDesign = mDesignDetailsForAutoSwap;
			} else {
				mSelectedBrandName = mBrandForAutoSwap;
				mSelectedtyreSize = mSizeForAutoSwap;
				mSelectedtyreTRIM = mRimForAutoSwap;
				mSelectedTyreTASP = mAspForAutoSwap;
				mSelectedTyreDesign = mDesignForAutoSwap;
				mSelectedTyreDetailedDesign = mDesignDetailsForAutoSwap;
			}
			// For Brand ::::: Fix:: SPARE-TYRE
			if (Constants.onBrandDesignBool || Constants.onBrandBlank
					|| TextUtils.isEmpty(mBrandForAutoSwap)) {
				// mIsSizeSpinnerEditable = false;
				mTyreFullDetailSpinner.setEnabled(true);
				mTyreDesignSpinner.setEnabled(true);
				mTyreFullDetailSpinner.setAdapter(null);
				mTyreDesignSpinner.setAdapter(null);
				mSelectedBrandName = "";
				loadTyreDetailsOnBrandCorrection();
			} else {
				loadSelectedBrandForAutoSwap(mBrandForAutoSwap);
				loadSelectedSizeForAutoSwap(mSizeForAutoSwap);
				loadSelectedASPForAutoSwap(mAspForAutoSwap);
				loadSelectedRIMForAutoSwap(mRimForAutoSwap);
				loadSelectedDesignForAutoSwap(mDesignForAutoSwap);
				loadSelectedDesignDetailsForAutoSwap(mDesignDetailsForAutoSwap);

				mValueNSK1.setText(mNskForAutoSwap);
				mValueNSK2.setText(mNsk2ForAutoSwap);
				mValueNSK3.setText(mNsk3ForAutoSwap);
			}
			if (!mSizeForAutoSwap.equalsIgnoreCase("0")) {
				mIsSizeSpinnerEditable = false;
			}
		}
		if (TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED)) {
			Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED = Constants.EDITED_SERIAL_NUMBER_SOURCE;
		}
		if (TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE)) {
			Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE = Constants.EDITED_SERIAL_NUMBER_DES;

		}
		if (!Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE.equalsIgnoreCase("")) {
			String secondaryPosition = VehicleSkeletonFragment
					.getPosFromSerial(Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE);
			Constants.THIRD_SELECTED_TYRE = VehicleSkeletonFragment
					.getTireByPosition(secondaryPosition);
		}
	}

	/**
	 * Method to load tire brand with no brand correction
	 * 
	 * @param mBrandForAutoSwap2
	 */
	private void loadSelectedBrandForAutoSwap(String mBrandForAutoSwap2) {
		mbrandArrayList.clear();
		if (!TextUtils.isEmpty(mBrandForAutoSwap2)) {
			mbrandArrayList.add(mBrandForAutoSwap2);
		}
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mbrandArrayList);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mBrandSpinner.setAdapter(dataAdapter);
		mBrandSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire size with no brand correction
	 * 
	 * @param mSizeForAutoSwap2
	 */
	private void loadSelectedSizeForAutoSwap(String mSizeForAutoSwap2) {
		mSizeArrayList.clear();
		if (!TextUtils.isEmpty(mSizeForAutoSwap2)) {
			mSizeArrayList.add(mSizeForAutoSwap2);
		}
		mSizeDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mSizeArrayList);
		mSizeDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSizeSpinner.setAdapter(mSizeDataAdapter);
		mSizeSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire ASP with no brand correction
	 * 
	 * @param mAspForAutoSwap2
	 */
	private void loadSelectedASPForAutoSwap(String mAspForAutoSwap2) {
		mTyreTASPArrayList.clear();
		if (!TextUtils.isEmpty(mAspForAutoSwap2)) {
			mTyreTASPArrayList.add(mAspForAutoSwap2);
		}
		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTASPSpinner.setAdapter(mASPDataAdapter);
		mTyreTASPSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire RIM with no brand correction
	 * 
	 * @param mRimForAutoSwap2
	 */
	private void loadSelectedRIMForAutoSwap(String mRimForAutoSwap2) {
		mTyreTRIMArrayList.clear();
		if (!TextUtils.isEmpty(mRimForAutoSwap2)) {
			mTyreTRIMArrayList.add(mRimForAutoSwap2);
		}
		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTRIMSpinner.setAdapter(mRimDataAdapter);
		mTyreTRIMSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire design with no brand correction
	 * 
	 * @param mDesignForAutoSwap2
	 */
	private void loadSelectedDesignForAutoSwap(String mDesignForAutoSwap2) {
		mDesignArrayList.clear();
		if (!TextUtils.isEmpty(mDesignForAutoSwap2)) {
			mDesignArrayList.add(mDesignForAutoSwap2);
		}
		mDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mDesignArrayList);
		mDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreDesignSpinner.setAdapter(mDesignDataAdapter);
		mTyreDesignSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire full design details with no brand correction
	 * 
	 * @param mDesignDetailsForAutoSwap2
	 */
	private void loadSelectedDesignDetailsForAutoSwap(
			String mDesignDetailsForAutoSwap2) {
		mFullDesignArrayList.clear();
		if (!TextUtils.isEmpty(mDesignDetailsForAutoSwap2)) {
			mFullDesignArrayList.add(mDesignDetailsForAutoSwap2);
		}
		mFullDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mFullDesignArrayList);
		mFullDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreFullDetailSpinner.setAdapter(mFullDesignDataAdapter);
		mTyreFullDetailSpinner.setEnabled(false);
	}

	/**
	 * Methods handing the data update for Double and Triple Swap after passing
	 * all the required conditions
	 */
	private void updateJobItemForDoubleSwap() {
		if (TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED)) {
			Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED = Constants.EDITED_SERIAL_NUMBER_SOURCE;
		}
		if (TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE)) {
			Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE = Constants.EDITED_SERIAL_NUMBER_DES;
		}
		if (!Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE.equalsIgnoreCase("")) {
			String secondaryPosition = VehicleSkeletonFragment
					.getPosFromSerial(Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE);
			Constants.THIRD_SELECTED_TYRE = VehicleSkeletonFragment
					.getTireByPosition(secondaryPosition);
			int jobID = getJobItemIDForThisJobItem();
			UpdateSwapJobItem.getMyInstance(this).updateJobItemForDoubleSwap(
					Constants.THIRD_SELECTED_TYRE,
					Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE,
					mBrandSpinner.getSelectedItem().toString(),
					mTyreFullDetailSpinner.getSelectedItem().toString(),
					minNSKSet(), mValueNSK1.getText().toString(),
					mValueNSK2.getText().toString(),
					mValueNSK3.getText().toString(), jobID);
		}
	}

	/**
	 * Class to call separate thread which handles all the data-update during
	 * SWAP then it calls Vehicle skeleton if all the conditions are true
	 */
	IOnPerformanceCallback callback = new IOnPerformanceCallback() {
		@Override
		public void updateTables() {
			onReturnSkeleton();
		}

		@Override
		public void showProgressBar(boolean toShow) {
			if (toShow) {
				// Show Progress Bar here
			} else {
				// Hide Progress bar here
			}
		}

		@Override
		public void onPostExecute(ResponseMessagePerformance message) {
			Intent intentReturn = new Intent();
			if (Constants.onGOBool == false) {

				setResult(1, intentReturn);
				Constants.JOB_CORRECTION_LIST = VehicleSkeletonFragment.mJobcorrectionList;
				finish();

			} else if (Constants.onGOBool == true) {
				setResult(12, intentReturn);
				Constants.JOB_CORRECTION_LIST = VehicleSkeletonFragment.mJobcorrectionList;
				finish();
			} else if (Constants.onGOBoolLogical == true) {
				setResult(24, intentReturn);
				Constants.JOB_CORRECTION_LIST = VehicleSkeletonFragment.mJobcorrectionList;
				finish();
			} else if (Constants.onDragBool_LS == true) {
				setResult(48, intentReturn);
				Constants.JOB_CORRECTION_LIST = VehicleSkeletonFragment.mJobcorrectionList;
				finish();
			}
			Constants.LOGICAL_SWAP_INITIAL = false;
			LogicalSwapDifferentSize.this.overridePendingTransition(
					R.anim.slide_left_in, R.anim.slide_left_out);
		}

		@Override
		public String onPreExecute() {
			String messageStr = PerformanceBaseModel.MESSAGE_STR;

			int checkedRadioButtonId = mGroupRegrooveType
					.getCheckedRadioButtonId();
			if (checkedRadioButtonId != -1) {
				if (mRegrooveTypeYes.isChecked()) {
					sFlag = true;
					sIsRadioBtnChecked = true;
				} else if (mRegrooveTypeNo.isChecked()) {
					sFlag = false;
					sIsRadioBtnChecked = true;
				}
			} else {
				messageStr = mREGROOVEREQUIRED;
				sIsRadioBtnChecked = false;
			}
			if (mTyreFullDetailSpinner.getSelectedItem().equals("")) {
				messageStr = "Enter Mandatory Fields";
			}
			return messageStr;
		}

		@Override
		public void onError(ResponseMessagePerformance message) {
			CommonUtils.notify(message.getErrorMessage(),
					getApplicationContext());
		}

		@Override
		public void closePreviousTask() {
		}
	};
	private PressureSwapDialog mPressureSwapDialog;

	/*
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		sNoteText = "";
		InactivityUtils.logoutFromActivityAboveEjobForm(this);
	}

	// CR 313
	/**
	 * This method creates a pressure dialog for the give tire.
	 * 
	 * @param tire
	 *            Tire for which the pressure dialog should be started.
	 * @param pressureSwapPosition
	 *            This defines which tire (tires being swapped) pressure would
	 *            be updated.
	 * @param pressureValue
	 *            Pressure value set by user in the dialog.
	 * @return
	 */
	private boolean createPressureDialogForTire(Tyre tire,
			int pressureSwapPosition, String pressureValue) {
		if (tire != null) {
			int axlePos = Character.getNumericValue(tire.getPosition()
					.charAt(0)) - 1;
			float recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
					.get(axlePos);
			if (recommendedPressure <= 0) {
				if (pressureValue == null) {
					mPressureSwapDialog = new PressureSwapDialog(this,
							pressureSwapPosition, tire);
				} else {
					mPressureSwapDialog = new PressureSwapDialog(this,
							pressureSwapPosition, tire, pressureValue);
				}
				mPressureSwapDialog.showPressureDialog();
				mPressureSwapDialog
						.togglePressureField(mBoolPressureDialogShouldEnable);
				return true;
			}
		}
		return false;
	}

	// CR 313
	@Override
	public void startPressureDialog(int pressureSwapPosition,
			String pressureValue) {
		if (Constants.LOGICAL_SWAP_INITIAL && TextUtils.isEmpty(Constants.SELECTED_TYRE.getDesignDetails())) {
			loadObject();
			return;
		}
		switch (pressureSwapPosition) {
		case FIRST_TIRE:
			// if any tire is null then no further dialogs will be displayed
			if (Constants.SELECTED_TYRE != null) {
				// if pressure dialog is displayed then return else if pressure
				// on this tire is available then move to next tire
				if (createPressureDialogForTire(Constants.SELECTED_TYRE,
						pressureSwapPosition, pressureValue))
					return;
			} else {
				loadObject();
			}
		case SECOND_TIRE:
			if (Constants.SECOND_SELECTED_TYRE != null) {
				pressureSwapPosition = SECOND_TIRE;
				// if pressure dialog is displayed then return else if pressure
				// on this tire is available then move to next tire
				if (createPressureDialogForTire(Constants.SECOND_SELECTED_TYRE,
						pressureSwapPosition, pressureValue))
					return;
			} else {
				loadObject();
				return;
			}
		case THIRD_TIRE:
			if (Constants.THIRD_SELECTED_TYRE != null) {
				pressureSwapPosition = THIRD_TIRE;
				// if pressure dialog is displayed then return else if pressure
				// on this tire is available then move to next tire
				if (createPressureDialogForTire(Constants.THIRD_SELECTED_TYRE,
						pressureSwapPosition, pressureValue))
					return;

			} else if (Constants.FOURTH_SELECTED_TYRE == null) {
				loadObject();
				return;
			}

		case FORTH_TIRE:
			if (Constants.FOURTH_SELECTED_TYRE != null) {
				pressureSwapPosition = FORTH_TIRE;
				// if pressure dialog is displayed then return else if pressure
				// on this tire is available then stop the dialog display
				// process
				if (createPressureDialogForTire(Constants.FOURTH_SELECTED_TYRE,
						pressureSwapPosition, pressureValue))
					return;
			}
			break;
		}
		loadObject();
	}

	// CR 313
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.IPressureDialogHandler#updatePressureOnSave
	 * ()
	 */
	@Override
	public void updatePressureOnSave(int pos, float setPressureValue) {
		switch (pos) {
		case FIRST_TIRE:
			if (Constants.SELECTED_TYRE != null) {
				Constants.SELECTED_TYRE = updateTirePressure(
						Constants.SELECTED_TYRE, pos, setPressureValue);
				startPressureDialog(pos + 1, null);
			}
			break;
		case SECOND_TIRE:
			if (Constants.SECOND_SELECTED_TYRE != null) {
				Constants.SECOND_SELECTED_TYRE = updateTirePressure(
						Constants.SECOND_SELECTED_TYRE, pos, setPressureValue);
				startPressureDialog(pos + 1, null);
			}
			break;
		case THIRD_TIRE:
			if (Constants.THIRD_SELECTED_TYRE != null) {
				Constants.THIRD_SELECTED_TYRE = updateTirePressure(
						Constants.THIRD_SELECTED_TYRE, pos, setPressureValue);
				startPressureDialog(pos + 1, null);
			}
			break;
		case FORTH_TIRE:
			if (Constants.FOURTH_SELECTED_TYRE != null) {
				Constants.FOURTH_SELECTED_TYRE = updateTirePressure(
						Constants.FOURTH_SELECTED_TYRE, pos, setPressureValue);
				startPressureDialog(pos + 1, null);
			}
			break;
		}
	}

	// CR 313
	/**
	 * Update pressure on given tire and axle
	 * 
	 * @param tire
	 *            tire on which pressure needs to be updated
	 * @param pos
	 * @param pressureValue
	 * @return
	 */
	private Tyre updateTirePressure(Tyre tire, int pos, float pressureValue) {
		tire.setPressure(String.valueOf(pressureValue));
		int axlepos = Character.getNumericValue(tire.getPosition().charAt(0)) - 1;
		VehicleSkeletonFragment.mAxleRecommendedPressure.set(axlepos,
				pressureValue);
		return tire;
	}

	// CR 313
	/**
	 * This method is called after dialogs and takes the user out of swap
	 * operation
	 */
	private void loadObject() {
		new PerformanceBaseModel(this, callback, false).perform();
	}
}