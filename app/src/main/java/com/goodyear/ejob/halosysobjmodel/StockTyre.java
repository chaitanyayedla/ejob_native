
package com.goodyear.ejob.halosysobjmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StockTyre {

    @SerializedName("tyreInformation")
    @Expose
    private TyreInformation tyreInformation;

    /**
     * 
     * @return
     *     The tyreInformation
     */
    public TyreInformation getTyreInformation() {
        return tyreInformation;
    }

    /**
     * 
     * @param tyreInformation
     *     The tyreInformation
     */
    public void setTyreInformation(TyreInformation tyreInformation) {
        this.tyreInformation = tyreInformation;
    }

}
