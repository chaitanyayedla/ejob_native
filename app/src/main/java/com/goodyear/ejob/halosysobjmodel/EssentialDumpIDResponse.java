
package com.goodyear.ejob.halosysobjmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EssentialDumpIDResponse {

    @SerializedName("DumpID")
    @Expose
    private Long dumpID;
    @SerializedName("UserID")
    @Expose
    private String userID;
    @SerializedName("GUID")
    @Expose
    private String gUID;
    @SerializedName("CreationTS")
    @Expose
    private Float creationTS;

    /**
     * 
     * @return
     *     The dumpID
     */
    public Long getDumpID() {
        return dumpID;
    }

    /**
     * 
     * @param dumpID
     *     The DumpID
     */
    public void setDumpID(Long dumpID) {
        this.dumpID = dumpID;
    }

    /**
     * 
     * @return
     *     The userID
     */
    public String getUserID() {
        return userID;
    }

    /**
     * 
     * @param userID
     *     The UserID
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * 
     * @return
     *     The gUID
     */
    public String getGUID() {
        return gUID;
    }

    /**
     * 
     * @param gUID
     *     The GUID
     */
    public void setGUID(String gUID) {
        this.gUID = gUID;
    }

    /**
     * 
     * @return
     *     The creationTS
     */
    public Float getCreationTS() {
        return creationTS;
    }

    /**
     * 
     * @param creationTS
     *     The CreationTS
     */
    public void setCreationTS(Float creationTS) {
        this.creationTS = creationTS;
    }

}
