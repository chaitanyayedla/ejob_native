
package com.goodyear.ejob.halosysobjmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class FleetServiceProviderStock {

    @SerializedName("stockDetails")
    @Expose
    private List<StockDetail> stockDetails = new ArrayList<StockDetail>();

    /**
     * 
     * @return
     *     The stockDetails
     */
    public List<StockDetail> getStockDetails() {
        return stockDetails;
    }

    /**
     * 
     * @param stockDetails
     *     The stockDetails
     */
    public void setStockDetails(List<StockDetail> stockDetails) {
        this.stockDetails = stockDetails;
    }

}
