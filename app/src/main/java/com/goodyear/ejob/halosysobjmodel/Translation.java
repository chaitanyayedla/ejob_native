package com.goodyear.ejob.halosysobjmodel;

/**
 * Created by giriprasanth.vp on 10/27/2016.
 */
public class Translation {
        private String LocalizedText;
        private String LanguageCode;
        private String TextCode;
        private String LanguageId;

        public String getLocalizedText ()
        {
            return LocalizedText;
        }

        public void setLocalizedText (String LocalizedText)
        {
            this.LocalizedText = LocalizedText;
        }

        public String getLanguageCode ()
        {
            return LanguageCode;
        }

        public void setLanguageCode (String LanguageCode)
        {
            this.LanguageCode = LanguageCode;
        }

        public String getTextCode ()
        {
            return TextCode;
        }

        public void setTextCode (String TextCode)
        {
            this.TextCode = TextCode;
        }

        public String getLanguageId ()
        {
            return LanguageId;
        }

        public void setLanguageId (String LanguageId)
        {
            this.LanguageId = LanguageId;
        }

        @Override
        public String toString()
        {
            return "Translation [LocalizedText = "+LocalizedText+", LanguageCode = "+LanguageCode+", TextCode = "+TextCode+", LanguageId = "+LanguageId+"]";
        }

}
