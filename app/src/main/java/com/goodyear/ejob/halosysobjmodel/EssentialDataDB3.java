package com.goodyear.ejob.halosysobjmodel;

/**
 * Created by giriprasanth.vp on 11/10/2016.
 */
public class EssentialDataDB3 {

    byte[] Incremental_DB3_data;
    String GUID;
    String DumpID;
    String userId;
    String creationTS;

    public byte[] getIncremental_DB3_data() {
        return Incremental_DB3_data;
    }

    public void setIncremental_DB3_data(byte[] incremental_DB3_data) {
        Incremental_DB3_data = incremental_DB3_data;
    }

    public String getGUID() {
        return GUID;
    }

    public void setGUID(String GUID) {
        this.GUID = GUID;
    }

    public String getDumpID() {
        return DumpID;
    }

    public void setDumpID(String dumpID) {
        DumpID = dumpID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreationTS() {
        return creationTS;
    }

    public void setCreationTS(String creationTS) {
        this.creationTS = creationTS;
    }


}
