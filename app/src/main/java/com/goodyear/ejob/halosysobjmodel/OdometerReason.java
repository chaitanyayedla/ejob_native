package com.goodyear.ejob.halosysobjmodel;

/**
 * Created by pragati.pj Nov 2016.
 */
public class OdometerReason {

    private String ID;
    private String Code;
    private String Reason;
    private String ExternalID;
    private Boolean isavailable;
    private String LastModifiedTS;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getExternalID() {
        return ExternalID;
    }

    public void setExternalID(String externalID) {
        ExternalID = externalID;
    }

    public Boolean getIsavailable() {
        return isavailable;
    }

    public void setIsavailable(Boolean isavailable) {
        this.isavailable = isavailable;
    }

    public String getLastModifiedTS() {
        return LastModifiedTS;
    }

    public void setLastModifiedTS(String lastModifiedTS) {
        LastModifiedTS = lastModifiedTS;
    }

}
