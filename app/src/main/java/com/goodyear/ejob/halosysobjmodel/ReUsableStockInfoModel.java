
package com.goodyear.ejob.halosysobjmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReUsableStockInfoModel {

    @SerializedName("fleetServiceProviderStock")
    @Expose
    private FleetServiceProviderStock fleetServiceProviderStock;

    /**
     * 
     * @return
     *     The fleetServiceProviderStock
     */
    public FleetServiceProviderStock getFleetServiceProviderStock() {
        return fleetServiceProviderStock;
    }

    /**
     * 
     * @param fleetServiceProviderStock
     *     The fleetServiceProviderStock
     */
    public void setFleetServiceProviderStock(FleetServiceProviderStock fleetServiceProviderStock) {
        this.fleetServiceProviderStock = fleetServiceProviderStock;
    }

}
