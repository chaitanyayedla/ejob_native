
package com.goodyear.ejob.halosysobjmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StockInfo {

    @SerializedName("contractNumber")
    @Expose
    private String contractNumber;
    @SerializedName("salesOrganisation")
    @Expose
    private String salesOrganisation;
    @SerializedName("customerNumber")
    @Expose
    private String customerNumber;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("stockTyres")
    @Expose
    private List<StockTyre> stockTyres = new ArrayList<StockTyre>();

    /**
     * 
     * @return
     *     The contractNumber
     */
    public String getContractNumber() {
        return contractNumber;
    }

    /**
     * 
     * @param contractNumber
     *     The contractNumber
     */
    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    /**
     * 
     * @return
     *     The salesOrganisation
     */
    public String getSalesOrganisation() {
        return salesOrganisation;
    }

    /**
     * 
     * @param salesOrganisation
     *     The salesOrganisation
     */
    public void setSalesOrganisation(String salesOrganisation) {
        this.salesOrganisation = salesOrganisation;
    }

    /**
     * 
     * @return
     *     The customerNumber
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    /**
     * 
     * @param customerNumber
     *     The customerNumber
     */
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The stockTyres
     */
    public List<StockTyre> getStockTyres() {
        return stockTyres;
    }

    /**
     * 
     * @param stockTyres
     *     The stockTyres
     */
    public void setStockTyres(List<StockTyre> stockTyres) {
        this.stockTyres = stockTyres;
    }

}
