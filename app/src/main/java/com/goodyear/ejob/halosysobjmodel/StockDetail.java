
package com.goodyear.ejob.halosysobjmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StockDetail {

    @SerializedName("stockInfo")
    @Expose
    private StockInfo stockInfo;

    /**
     * 
     * @return
     *     The stockInfo
     */
    public StockInfo getStockInfo() {
        return stockInfo;
    }

    /**
     * 
     * @param stockInfo
     *     The stockInfo
     */
    public void setStockInfo(StockInfo stockInfo) {
        this.stockInfo = stockInfo;
    }

}
