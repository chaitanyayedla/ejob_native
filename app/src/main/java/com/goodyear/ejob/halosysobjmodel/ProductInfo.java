
package com.goodyear.ejob.halosysobjmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductInfo {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("brand")
    @Expose
    private Brand brand;
    @SerializedName("sizemm")
    @Expose
    private String sizemm;
    @SerializedName("aspectRatio")
    @Expose
    private String aspectRatio;
    @SerializedName("rimDiameter")
    @Expose
    private String rimDiameter;
    @SerializedName("fuelClass")
    @Expose
    private String fuelClass;
    @SerializedName("gripClass")
    @Expose
    private String gripClass;
    @SerializedName("noiseClass")
    @Expose
    private String noiseClass;

    /**
     * 
     * @return
     *     The code
     */
    public String getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The brand
     */
    public Brand getBrand() {
        return brand;
    }

    /**
     * 
     * @param brand
     *     The brand
     */
    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    /**
     * 
     * @return
     *     The sizemm
     */
    public String getSizemm() {
        return sizemm;
    }

    /**
     * 
     * @param sizemm
     *     The sizemm
     */
    public void setSizemm(String sizemm) {
        this.sizemm = sizemm;
    }

    /**
     * 
     * @return
     *     The aspectRatio
     */
    public String getAspectRatio() {
        return aspectRatio;
    }

    /**
     * 
     * @param aspectRatio
     *     The aspectRatio
     */
    public void setAspectRatio(String aspectRatio) {
        this.aspectRatio = aspectRatio;
    }

    /**
     * 
     * @return
     *     The rimDiameter
     */
    public String getRimDiameter() {
        return rimDiameter;
    }

    /**
     * 
     * @param rimDiameter
     *     The rimDiameter
     */
    public void setRimDiameter(String rimDiameter) {
        this.rimDiameter = rimDiameter;
    }

    /**
     * 
     * @return
     *     The fuelClass
     */
    public String getFuelClass() {
        return fuelClass;
    }

    /**
     * 
     * @param fuelClass
     *     The fuelClass
     */
    public void setFuelClass(String fuelClass) {
        this.fuelClass = fuelClass;
    }

    /**
     * 
     * @return
     *     The gripClass
     */
    public String getGripClass() {
        return gripClass;
    }

    /**
     * 
     * @param gripClass
     *     The gripClass
     */
    public void setGripClass(String gripClass) {
        this.gripClass = gripClass;
    }

    /**
     * 
     * @return
     *     The noiseClass
     */
    public String getNoiseClass() {
        return noiseClass;
    }

    /**
     * 
     * @param noiseClass
     *     The noiseClass
     */
    public void setNoiseClass(String noiseClass) {
        this.noiseClass = noiseClass;
    }

}
