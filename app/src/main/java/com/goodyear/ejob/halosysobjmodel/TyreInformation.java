
package com.goodyear.ejob.halosysobjmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TyreInformation {

    @SerializedName("tyreId")
    @Expose
    private String tyreId;
    @SerializedName("serialNumber")
    @Expose
    private String serialNumber;
    @SerializedName("treadDepthMiddle")
    @Expose
    private String treadDepthMiddle;
    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("productInfo")
    @Expose
    private ProductInfo productInfo;

    /**
     * 
     * @return
     *     The tyreId
     */
    public String getTyreId() {
        return tyreId;
    }

    /**
     * 
     * @param tyreId
     *     The tyreId
     */
    public void setTyreId(String tyreId) {
        this.tyreId = tyreId;
    }

    /**
     * 
     * @return
     *     The serialNumber
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * 
     * @param serialNumber
     *     The serialNumber
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * 
     * @return
     *     The treadDepthMiddle
     */
    public String getTreadDepthMiddle() {
        return treadDepthMiddle;
    }

    /**
     * 
     * @param treadDepthMiddle
     *     The treadDepthMiddle
     */
    public void setTreadDepthMiddle(String treadDepthMiddle) {
        this.treadDepthMiddle = treadDepthMiddle;
    }

    /**
     * 
     * @return
     *     The status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The productInfo
     */
    public ProductInfo getProductInfo() {
        return productInfo;
    }

    /**
     * 
     * @param productInfo
     *     The productInfo
     */
    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }

}
