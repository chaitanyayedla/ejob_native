/***/
package com.goodyear.ejob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.CameraActivityFormElements;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.ui.tyremanagement.TyreOperationActivity;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.GYProgressDialog;
import com.goodyear.ejob.util.LogUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Class used to take the photos and store into the bitmap array.
 * 
 * @author amitkumar.h
 * @version 1.0
 */
public class CameraActivity extends Activity implements InactivityHandler {

	/**
	 * File name of original image This file will be temp file and will be
	 * cleaned after its use
	 */
	public static final String ORIGINAL_FILE_NAME = "original_image.jpg";
	/**
	 * Minimum pixels to be swiped in order to consider the gesture as swipe
	 */
	private static final int SWIPE_MIN_DISTANCE = 90;
	/**
	 * Request codes to know for which view the image was requested
	 */
	private static final int requestCode1 = 301, requestCode2 = 302,
			requestCode3 = 303;
	/**
	 * Max image file size in bytes
	 */
	// Changed from 25 to 50
	private static final int IMAGE_MAX_SIZE = 50 * 1024;
	/**
	 * File name of compressed image This file will be temp file and will be
	 * cleaned after its use
	 */
	private static final String COMPRESSED_FILE = "compressed_image.jpg";
	public static int mImageSavedClicked = 0;
	final Context mContext = this;
	private int pos1x;
	private boolean mIsFromTyreMgmnt = false;
	/**
	 * Used for common database handler
	 */
	private DatabaseAdapter mDbHelper;
	/**
	 * ArrayList containing damage type options
	 */
	private ArrayList<String> mDamageTypeList;
	/**
	 * ArrayList containing damage area options
	 */
	private ArrayList<String> mDamageAreaList;
	/**
	 * String that stores selected damage type option
	 */
	private String selectedDamageType;
	/**
	 * String that stores selected damage area option
	 */
	private String selectedDamageArea;
	/**
	 * flag to chack whether is validation required
	 */
	private boolean validationRequired = false;
	private boolean canSaveImage;
	private int mImgCountFromPreviousActivity = 0;
	// VIEWS
	/**
	 * Spinner to select the tyre damage type
	 */
	private Spinner mDamageTypeSpinner;
	/**
	 * Spinner to select the tyre damage area
	 */
	private Spinner mDamageAreaSpinner;
	/**
	 * Contains 3 pages of images and notes
	 */
	private ViewFlipper mViewFlipper;
	/**
	 * EditText that takes notes for each page
	 */
	private EditText mNotesEditText1;
	private EditText mNotesEditText2;
	private EditText mNotesEditText3;
	/**
	 * Tapping on this textview will start camera intent
	 */
	private TextView mPhotoTextPage1;
	private TextView mPhotoTextPage2;
	private TextView mPhotoTextPage3;
	/**
	 * This ImageView will contain the requested Image
	 */
	private ImageView mImageView1;
	private ImageView mImageView2;
	private ImageView mImageView3;
	/**
	 * This will contain the save and discard button
	 */
	private View mButtonLayout1;
	private View mButtonLayout2;
	private View mButtonLayout3;
	/**
	 * This will contain the ImageViews
	 */
	private RelativeLayout mPhotoView1;
	private RelativeLayout mPhotoView2;
	private RelativeLayout mPhotoView3;
	private boolean mTreadBreakspot;
	/**
	 * To save all initial values, for implementing onBack handling
	 */
	private CameraActivityFormElements initialValues;
	private AlertDialog mAlertDialog;
	private boolean mIsInEditMode = false;
	/**
	 * if user selected break spot as removal reason
	 */
	private boolean mUserSelectedBreakSpot;
	/**
	 * if user selected break spot as removal reason
	 */
	private boolean mUserSelectedDamageScrap;
	/**
	 * captures the primary note text every time save button is tapped
	 */
	private String mPrimaryNoteText;
	private TextView mTextMaximumTireNSK;
	private TextView mTextMinimumTireNSK;
	private EditText mEditMaximumTireNSK;
	private EditText mEditMinimumTireNSK;
	private TextView mUnitMax;
	private TextView mUnitMin;
	private TextView mTextBreakSpotInfo;
	private boolean onBackpressed;
	private String mWrongMinimumNSKMessage = "";
	private static final int NSK_VISIBLE = 1;
	private static final int NSK_GONE = 0;
	public static final String BREAK_SPOT_KEY = "breakspot";
	public static final String DAMAGE_SCRAP_KEY = "damagescrap";
	public static final String DAMAGE_SPINNER_DISABLED = "damage_spinner_diabled";
	//User Trace logs trace tag
	private static final String TRACE_TAG = "Camera";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		boolean isDamageSpinnersDisabledBeforeRotation = false;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.photo_view);
		//User Trace logs
		LogUtil.TraceInfo(TRACE_TAG, "none", TRACE_TAG, false, false,false);
		mViewFlipper = (ViewFlipper) findViewById(R.id.view_flipper);

		setUpPageIndicators();
		Constants.IMAGES_PATH = "";
		Constants.IMAGES_PATH = Constants.PROJECT_PATH + Constants.IMAGES_PATH;
		if (getIntent().hasExtra("FROM_TMT")) {
			mIsFromTyreMgmnt = true;
		}
		if (getIntent().hasExtra("CURNT_IMAGE_COUNT")) {
			mImgCountFromPreviousActivity = getIntent().getIntExtra(
					"CURNT_IMAGE_COUNT", 0);
		}
		mDbHelper = new DatabaseAdapter(this);
		mDbHelper.open();
		mDamageTypeList = new ArrayList<>();
		mDamageAreaList = new ArrayList<>();
		mDamageTypeSpinner = (Spinner) findViewById(R.id.spinner_DamageType);
		mDamageAreaSpinner = (Spinner) findViewById(R.id.spinner_DamageArea);
		mNotesEditText1 = (EditText) findViewById(R.id.damage_note1);
		mNotesEditText2 = (EditText) findViewById(R.id.damage_note2);
		mNotesEditText3 = (EditText) findViewById(R.id.damage_note3);
		mNotesEditText1.addTextChangedListener(new NoteTextWatcher());
		mNotesEditText2.addTextChangedListener(new NoteTextWatcher());
		mNotesEditText3.addTextChangedListener(new NoteTextWatcher());
		mTextMaximumTireNSK = (TextView) findViewById(R.id.text_maximum_tire_nsk);
		mTextMinimumTireNSK = (TextView) findViewById(R.id.text_minimum_tire_nsk);
		mEditMaximumTireNSK = (EditText) findViewById(R.id.edit_maximum_tire_nsk);
		mEditMinimumTireNSK = (EditText) findViewById(R.id.edit_minimum_tire_nsk);
		mTextBreakSpotInfo = (TextView) findViewById(R.id.text_break_spot_info);
		mUnitMax = (TextView) findViewById(R.id.text_unit_max);
		mUnitMin = (TextView) findViewById(R.id.text_unit_min);
		if (getIntent().hasExtra("DAMAGE_MANDATORY")) {
			validationRequired = getIntent().getBooleanExtra(
					"DAMAGE_MANDATORY", false);
		}

		if (getIntent().hasExtra(DismountTireActivity.BREAKSPOT_SELECTED)
				&& getIntent().getBooleanExtra(
						DismountTireActivity.BREAKSPOT_SELECTED, false)) {
			mUserSelectedBreakSpot = getIntent().getBooleanExtra(
					DismountTireActivity.BREAKSPOT_SELECTED, false);
			String maxMinValue = getIntent().getStringExtra(BREAK_SPOT_KEY);
			setMinMaxNSKIfAvailable(maxMinValue);
		} else if (getIntent().hasExtra(
				DismountTireActivity.DAMAGESCRAP_SELECTED)
				&& getIntent().getBooleanExtra(
						DismountTireActivity.DAMAGESCRAP_SELECTED, false)) {
			mUserSelectedDamageScrap = true;
			String maxMinValue = getIntent().getStringExtra(DAMAGE_SCRAP_KEY);
			if (!TextUtils.isEmpty(maxMinValue)) {
				setMinMaxNSKIfAvailable(maxMinValue);
				setNSKVisibilty(NSK_VISIBLE);
			}
		}

		// true if user selected break spot
		if (mUserSelectedBreakSpot) {
			setNSKVisibilty(NSK_VISIBLE);
			// set nsk filters
			mEditMaximumTireNSK.addTextChangedListener(new NSKMaxChanged());
			mEditMinimumTireNSK.addTextChangedListener(new NSKMinChanged());
		} else if (mUserSelectedDamageScrap) {
			mEditMaximumTireNSK.addTextChangedListener(new NSKMaxChanged());
			mEditMinimumTireNSK.addTextChangedListener(new NSKMinChanged());
		} else {
			setNSKVisibilty(NSK_GONE);
		}
		// Hiding DamageGroup and DamageArea
		if (!Constants.ISBREAKSPOT && !mUserSelectedDamageScrap
				&& (Constants.PHOTO_CAPTURE_ON_MIN_TD || !validationRequired)) {
			mDamageTypeSpinner.setEnabled(false);
			mDamageAreaSpinner.setEnabled(false);
			mDamageTypeSpinner.setVisibility(View.GONE);
			mDamageAreaSpinner.setVisibility(View.GONE);
			LayoutParams layoutParams = mNotesEditText1.getLayoutParams();
			mNotesEditText1.setLayoutParams(layoutParams);
		} else {
			loadDamageType();
		}
		if (Constants.DAMAGE_NOTES1 != null) {
			String damageNote = Constants.DAMAGE_NOTES1;
			if (!TextUtils.isEmpty(damageNote))
				mNotesEditText1.setText(damageNote);
		}
		if (Constants.DAMAGE_NOTES2 != null) {
			mNotesEditText2.setText(Constants.DAMAGE_NOTES2);
		}
		if (Constants.DAMAGE_NOTES3 != null) {
			mNotesEditText3.setText(Constants.DAMAGE_NOTES3);
		}
		mButtonLayout1 = findViewById(R.id.del_image1);
		mButtonLayout2 = findViewById(R.id.del_image2);
		mButtonLayout3 = findViewById(R.id.del_image3);
		mPhotoView1 = (RelativeLayout) findViewById(R.id.photo_view1);
		mPhotoView2 = (RelativeLayout) findViewById(R.id.photo_view2);
		mPhotoView3 = (RelativeLayout) findViewById(R.id.photo_view3);
		mImageView1 = (ImageView) findViewById(R.id.image_view1);
		mImageView2 = (ImageView) findViewById(R.id.image_view2);
		mImageView3 = (ImageView) findViewById(R.id.image_view3);
		mImageView1.setOnTouchListener(new CustomTouchListener());
		mImageView2.setOnTouchListener(new CustomTouchListener());
		mImageView3.setOnTouchListener(new CustomTouchListener());
		mPhotoTextPage1 = (TextView) findViewById(R.id.photo_text_page1);
		mPhotoTextPage1.setOnTouchListener(new CustomTouchListener());
		mPhotoTextPage2 = (TextView) findViewById(R.id.photo_text_page2);
		mPhotoTextPage2.setOnTouchListener(new CustomTouchListener());
		mPhotoTextPage3 = (TextView) findViewById(R.id.photo_text_page3);
		mPhotoTextPage3.setOnTouchListener(new CustomTouchListener());
		Button saveImage1 = (Button) findViewById(R.id.savebutton1);

		loadLabelsFromDB();
		if (mIsFromTyreMgmnt) {
			if (TyreOperationActivity.mBitmapPhoto1 != null) {
				mImageView1.setImageBitmap(TyreOperationActivity.mBitmapPhoto1);
				mButtonLayout1.setVisibility(View.GONE);
				mPhotoTextPage1.setVisibility(View.GONE);
				mPhotoView1.setVisibility(View.VISIBLE);
			}
			if (TyreOperationActivity.mBitmapPhoto2 != null) {
				mImageView2.setImageBitmap(TyreOperationActivity.mBitmapPhoto2);
				mButtonLayout2.setVisibility(View.GONE);
				mPhotoTextPage2.setVisibility(View.GONE);
				mPhotoView2.setVisibility(View.VISIBLE);
			}
			if (TyreOperationActivity.mBitmapPhoto3 != null) {
				mImageView3.setImageBitmap(TyreOperationActivity.mBitmapPhoto3);
				mButtonLayout3.setVisibility(View.GONE);
				mPhotoTextPage3.setVisibility(View.GONE);
				mPhotoView3.setVisibility(View.VISIBLE);
			}
		} else {
			if (Constants.sBitmapPhoto1 != null) {
				mImageView1.setImageBitmap(Constants.sBitmapPhoto1);
				mButtonLayout1.setVisibility(View.GONE);
				mPhotoTextPage1.setVisibility(View.GONE);
				mPhotoView1.setVisibility(View.VISIBLE);
			}
			if (Constants.sBitmapPhoto2 != null) {
				mImageView2.setImageBitmap(Constants.sBitmapPhoto2);
				mButtonLayout2.setVisibility(View.GONE);
				mPhotoTextPage2.setVisibility(View.GONE);
				mPhotoView2.setVisibility(View.VISIBLE);
			}
			if (Constants.sBitmapPhoto3 != null) {
				mImageView3.setImageBitmap(Constants.sBitmapPhoto3);
				mButtonLayout3.setVisibility(View.GONE);
				mPhotoTextPage3.setVisibility(View.GONE);
				mPhotoView3.setVisibility(View.VISIBLE);
			}
		}
		saveImage1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				saveImage1();
			}
		});
		Button saveImage2 = (Button) findViewById(R.id.savebutton2);
		saveImage2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				saveImage2();
			}
		});
		Button saveImage3 = (Button) findViewById(R.id.savebutton3);
		saveImage3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				saveImage3();
			}
		});
		mButtonLayout1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				discardImage1();
			}
		});
		mButtonLayout2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				discardImage2();
			}
		});
		mButtonLayout3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				discardImage3();
			}
		});
		if (savedInstanceState != null) {
			mViewFlipper.setDisplayedChild(savedInstanceState.getInt(
					"CURRENT_PAGE", 0));
			manageViewsByPage();
			mImageSavedClicked = savedInstanceState
					.getInt("retainImageSavedClicked");
			canSaveImage = savedInstanceState
					.getBoolean("retainImageSavedIconState");
			Constants.mbutton_layout1State = savedInstanceState
					.getInt("retainbutton_layout1State");
			mButtonLayout1.setVisibility(Constants.mbutton_layout1State);
			Constants.mbutton_layout2State = savedInstanceState
					.getInt("retainbutton_layout2State");
			mButtonLayout2.setVisibility(Constants.mbutton_layout2State);
			Constants.mbutton_layout3State = savedInstanceState
					.getInt("retainbutton_layout3State");
			mButtonLayout3.setVisibility(Constants.mbutton_layout3State);
			initialValues = savedInstanceState
					.getParcelable("retainInitialElement");
			if (savedInstanceState.getBoolean("isAlertDialogDisplaying")) {
				actionBackPressed();
			}

			isDamageSpinnersDisabledBeforeRotation = savedInstanceState
					.getBoolean(DAMAGE_SPINNER_DISABLED);
		} else {
			captureInitialState();
		}
		// internal bug fixed (delete image and rotate then dropdown become
		// disabled)
		if (mButtonLayout1.getVisibility() == View.GONE
				&& (savedInstanceState == null || !isDamageSpinnersDisabledBeforeRotation)) {
			disableSpinners();
		}

		// if (mUserSelectedDamageScrap) {
		// setNSKVisibilty(NSK_GONE);
		// }
		if (Constants.sBitmapPhoto1 == null) {
			mEditMaximumTireNSK.setText("");
			mEditMinimumTireNSK.setText("");
		}
	}

	/**
	 * this method sets the visibility of the NSK fields for break spot. it
	 * check if the selected reason
	 * 
	 * @param visibilty
	 *            set 1 to make visible 0 for gone
	 */
	private void setNSKVisibilty(int visibilty) {
		if (visibilty == NSK_VISIBLE
				&& (mUserSelectedBreakSpot || mUserSelectedDamageScrap)) {
			mTextMaximumTireNSK.setVisibility(TextView.VISIBLE);
			mTextMinimumTireNSK.setVisibility(TextView.VISIBLE);
			mEditMaximumTireNSK.setVisibility(EditText.VISIBLE);
			mEditMinimumTireNSK.setVisibility(EditText.VISIBLE);
			mTextBreakSpotInfo.setVisibility(TextView.VISIBLE);
			mUnitMax.setVisibility(TextView.VISIBLE);
			mUnitMin.setVisibility(TextView.VISIBLE);
		} else if (visibilty == NSK_GONE) {
			mTextMaximumTireNSK.setVisibility(TextView.GONE);
			mTextMinimumTireNSK.setVisibility(TextView.GONE);
			mEditMaximumTireNSK.setVisibility(EditText.GONE);
			mEditMinimumTireNSK.setVisibility(EditText.GONE);
			mTextBreakSpotInfo.setVisibility(TextView.GONE);
			mUnitMax.setVisibility(TextView.GONE);
			mUnitMin.setVisibility(TextView.GONE);
		}
	}

	/**
	 * set min max nsk from the user entered note
	 */
	private void setMinMaxNSKIfAvailable(String noteText) {
		if (TextUtils.isEmpty(noteText))
			return;
		int minNSK = 1;
		int maxNSK = 0;
		String[] minMaxNsk = CommonUtils
				.getUserEnteredMaxMinNSKFromNoteWithDelimiter(noteText);
		if (minMaxNsk != null
				&& (mUserSelectedBreakSpot || mUserSelectedDamageScrap)) {
			mEditMaximumTireNSK.setText(minMaxNsk[maxNSK].trim());
			mEditMinimumTireNSK.setText(minMaxNsk[minNSK].trim());
		}
	}

	/**
	 * Disabling Spinner for Damage Area and Group
	 */
	private void disableSpinners() {
		mIsInEditMode = true;
		mDamageAreaSpinner.setEnabled(false);
		mDamageTypeSpinner.setEnabled(false);
		refreshMenu();
	}

	/**
	 * Discarding Functionality for 3rd Image
	 */
	private void discardImage3() {
		if (mIsFromTyreMgmnt) {
			TyreOperationActivity.mBitmapPhoto3 = null;
			mButtonLayout3.setVisibility(View.GONE);
			mPhotoView3.setVisibility(View.GONE);
			mPhotoTextPage3.setVisibility(View.VISIBLE);
		} else {
			Constants.sBitmapPhoto3 = null;
			mButtonLayout3.setVisibility(View.GONE);
			mPhotoView3.setVisibility(View.GONE);
			mPhotoTextPage3.setVisibility(View.VISIBLE);
		}
		mImageView3.setImageDrawable(null);
		refreshMenu();
	}

	/**
	 * Discarding Functionality for 2nd Image
	 */
	private void discardImage2() {
		if (mIsFromTyreMgmnt) {
			TyreOperationActivity.mBitmapPhoto2 = null;
			mButtonLayout2.setVisibility(View.GONE);
			mPhotoView2.setVisibility(View.GONE);
			mPhotoTextPage2.setVisibility(View.VISIBLE);
		} else {
			Constants.sBitmapPhoto2 = null;
			mButtonLayout2.setVisibility(View.GONE);
			mPhotoView2.setVisibility(View.GONE);
			mPhotoTextPage2.setVisibility(View.VISIBLE);
		}
		mImageView2.setImageDrawable(null);
		refreshMenu();
	}

	/**
	 * Discarding Functionality for 1st Image
	 */
	private void discardImage1() {
		if (mIsFromTyreMgmnt) {
			TyreOperationActivity.mBitmapPhoto1 = null;
			mButtonLayout1.setVisibility(View.GONE);
			mPhotoView1.setVisibility(View.GONE);
			mPhotoTextPage1.setVisibility(View.VISIBLE);
		} else {
			Constants.sBitmapPhoto1 = null;
			mButtonLayout1.setVisibility(View.GONE);
			mPhotoView1.setVisibility(View.GONE);
			mPhotoTextPage1.setVisibility(View.VISIBLE);
		}
		mImageView1.setImageDrawable(null);
		refreshMenu();
	}

	/**
	 * Saving Functionality for 3rd Image
	 */
	private void saveImage3() {
		if (mIsFromTyreMgmnt) {
			TyreOperationActivity.mBitmapPhoto3 = ((BitmapDrawable) mImageView3
					.getDrawable()).getBitmap();
			Constants.damageImage_TM_LIST_Bitmap3
					.add(TyreOperationActivity.mBitmapPhoto3);
			Constants.bitmapQualityMap.put(TyreOperationActivity.mBitmapPhoto3,
					TyreOperationActivity.mBitmapPhoto3Quality);
			mButtonLayout3.setVisibility(View.GONE);
		} else {
			Constants.sBitmapPhoto3 = ((BitmapDrawable) mImageView3
					.getDrawable()).getBitmap();
			mButtonLayout3.setVisibility(View.GONE);
		}
		refreshMenu();
	}

	/**
	 * Saving Functionality for 2nd Image
	 */
	private void saveImage2() {
		if (mIsFromTyreMgmnt) {
			TyreOperationActivity.mBitmapPhoto2 = ((BitmapDrawable) mImageView2
					.getDrawable()).getBitmap();
			Constants.damageImage_TM_LIST_Bitmap2
					.add(TyreOperationActivity.mBitmapPhoto2);
			Constants.bitmapQualityMap.put(TyreOperationActivity.mBitmapPhoto2,
					TyreOperationActivity.mBitmapPhoto2Quality);
			mButtonLayout2.setVisibility(View.GONE);
		} else {
			Constants.sBitmapPhoto2 = ((BitmapDrawable) mImageView2
					.getDrawable()).getBitmap();
			mButtonLayout2.setVisibility(View.GONE);
		}
		refreshMenu();
	}

	/**
	 * Saving Functionality for 1st Image
	 */
	private void saveImage1() {
		if (mIsFromTyreMgmnt) {
			mImageSavedClicked = 1;
			TyreOperationActivity.mBitmapPhoto1 = ((BitmapDrawable) mImageView1
					.getDrawable()).getBitmap();
			Constants.damageImage_TM_LIST_Bitmap1
					.add(TyreOperationActivity.mBitmapPhoto1);
			Constants.bitmapQualityMap.put(TyreOperationActivity.mBitmapPhoto1,
					TyreOperationActivity.mBitmapPhoto1Quality);
			mButtonLayout1.setVisibility(View.GONE);
		} else {
			Constants.sBitmapPhoto1 = ((BitmapDrawable) mImageView1
					.getDrawable()).getBitmap();
			mButtonLayout1.setVisibility(View.GONE);
		}
		refreshMenu();
	}

	/**
	 * Retaining Initial State
	 */
	private void captureInitialState() {
		initialValues = new CameraActivityFormElements();
		initialValues.setNotes1(Constants.DAMAGE_NOTES1);
		initialValues.setNotes2(Constants.DAMAGE_NOTES2);
		initialValues.setNotes3(Constants.DAMAGE_NOTES3);
		if (mIsFromTyreMgmnt) {
			initialValues
					.setHasImage1(null != TyreOperationActivity.mBitmapPhoto1);
			initialValues
					.setHasImage2(null != TyreOperationActivity.mBitmapPhoto2);
			initialValues
					.setHasImage3(null != TyreOperationActivity.mBitmapPhoto3);
		} else {
			initialValues.setHasImage1(null != Constants.sBitmapPhoto1);
			initialValues.setHasImage2(null != Constants.sBitmapPhoto2);
			initialValues.setHasImage3(null != Constants.sBitmapPhoto3);
		}
		initialValues.setDamageType(Constants.SELECTED_DAMAGE_TYPE);
		initialValues.setDamageArea(Constants.SELECTED_DAMAGE_AREA);
	}

	/**
	 * Fetches the damage type options from database,saves it to mDamageTypeList
	 * and sets the adapter to mDamageTypeSpinner
	 */
	@SuppressWarnings("rawtypes")
	private void loadDamageType() {
		Cursor damageTypeCursor = null;
		try {
			damageTypeCursor = mDbHelper.getDamageGroup();
			mDamageTypeList.clear();
			if (CursorUtils.isValidCursor(damageTypeCursor)) {
				damageTypeCursor.moveToFirst();
				while (!damageTypeCursor.isAfterLast()) {
					mDamageTypeList.add(damageTypeCursor
							.getString(damageTypeCursor
									.getColumnIndex("DamageGroup")));
					damageTypeCursor.moveToNext();
				}
				CursorUtils.closeCursor(damageTypeCursor);
			}
			Collections.sort(mDamageTypeList);
			mDamageTypeList.add(0, mDbHelper.getLabel(183));
			ArrayAdapter<String> damageTypeAdapter = new ArrayAdapter<>(this,
					android.R.layout.simple_spinner_item, mDamageTypeList);
			damageTypeAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mDamageTypeSpinner.setAdapter(damageTypeAdapter);
			if (Constants.ISBREAKSPOT) {
				Cursor breakSpotCursor = mDbHelper.getBreakSpotDetails();
				if (CursorUtils.isValidCursor(breakSpotCursor)) {
					breakSpotCursor.moveToFirst();
					selectedDamageType = breakSpotCursor
							.getString(breakSpotCursor
									.getColumnIndex("DamageGroup"));
					mDamageTypeSpinner.setEnabled(false);
				}
				CursorUtils.closeCursor(breakSpotCursor);
			} else if (Constants.SELECTED_DAMAGE_TYPE != null) {
				selectedDamageType = Constants.SELECTED_DAMAGE_TYPE;
			}
			for (int i = 0; i < mDamageTypeList.size(); i++) {
				if (mDamageTypeList.get(i).equals(selectedDamageType)) {
					mDamageTypeSpinner.setSelection(i);
				}
			}
			mDamageTypeSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							selectedDamageType = mDamageTypeSpinner
									.getSelectedItem().toString();
							refreshMenu();
							loadDamageArea();
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(damageTypeCursor);
		}
	}

	/**
	 * Overriding ON-DESTROY
	 */
	@Override
	protected void onDestroy() {
		if (mAlertDialog != null && mAlertDialog.isShowing()) {
			mAlertDialog.dismiss();
		}
		if (mDbHelper != null) {
			mDbHelper.close();
		}
		super.onDestroy();
	}

	/**
	 * Fetches the damage area options from database,saves it to mDamageAreaList
	 * and sets the adapter to mDamageAreaSpinner
	 */
	@SuppressWarnings("rawtypes")
	private void loadDamageArea() {
		if (mDamageAreaList != null) {
			mDamageAreaList.clear();
		}
		Cursor damageAreaCursor = null;
		Cursor breakSpotCursor = null;
		try {
			damageAreaCursor = mDbHelper.getDamageArea(selectedDamageType);
			if (CursorUtils.isValidCursor(damageAreaCursor)) {
				damageAreaCursor.moveToFirst();
				while (!damageAreaCursor.isAfterLast()) {
					mDamageAreaList.add(damageAreaCursor
							.getString(damageAreaCursor
									.getColumnIndex("RemovalReasonCode")));
					damageAreaCursor.moveToNext();
				}
			}
			Collections.sort(mDamageAreaList);
			mDamageAreaList.add(0, mDbHelper.getLabel(317));
			ArrayAdapter<String> damageAreaAdapter = new ArrayAdapter<>(this,
					android.R.layout.simple_spinner_item, mDamageAreaList);
			damageAreaAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mDamageAreaSpinner.setAdapter(damageAreaAdapter);
			if (Constants.ISBREAKSPOT) {
				breakSpotCursor = mDbHelper.getBreakSpotDetails();
				if (CursorUtils.isValidCursor(breakSpotCursor)) {
					breakSpotCursor.moveToFirst();
					selectedDamageArea = breakSpotCursor
							.getString(breakSpotCursor
									.getColumnIndex("RemovalReasonCode"));
					mDamageAreaSpinner.setEnabled(false);
				}
			} else if (Constants.SELECTED_DAMAGE_AREA != null) {
				selectedDamageArea = Constants.SELECTED_DAMAGE_AREA;
			}
			for (int i = 0; i < mDamageAreaList.size(); i++) {
				if (mDamageAreaList.get(i).equals(selectedDamageArea)) {
					mDamageAreaSpinner.setSelection(i);
				}
			}
			mDamageAreaSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							selectedDamageArea = mDamageAreaSpinner
									.getSelectedItem().toString();
							refreshMenu();

							//Fix for TD max and Min for breakspot
							final String BREAK_SPOT_DamageCode="BS";
							final String BREAK_SPOT_RemovalReasonCode="Breakspot";


							//Fix for TD max and Min for breakspot (to get Damage code)
							String mDamageCode = mDbHelper
									.getDamageCodeFromDamageGroupAndRemovalReasonCode(
											mDamageTypeSpinner
													.getSelectedItem()
													.toString(),
											mDamageAreaSpinner
													.getSelectedItem()
													.toString());
							//Fix for TD max and Min for breakspot (checking with DamageCode and RemovalReasonCode)
							if (mDamageCode
									.trim()
									.equalsIgnoreCase(
											BREAK_SPOT_DamageCode) &&
									mDamageAreaSpinner
											.getSelectedItem()
											.toString().trim()
									.equalsIgnoreCase(
											BREAK_SPOT_RemovalReasonCode)) {
								mTreadBreakspot = true;
								manageViewsByPage();
							} else {
								mTreadBreakspot = false;
								setNSKVisibilty(NSK_GONE);
							}
							refreshMenu();
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(damageAreaCursor);
			CursorUtils.closeCursor(breakSpotCursor);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			compressInHeadlessFragment(requestCode, resultCode);
		} else if (resultCode == Activity.RESULT_CANCELED) {
			// Do Nothing
		} else {
			LogUtil.i("CameraActivity",
					"The Camera couldn't capture the image. Please try again");
		}
	}

	/**
	 * Handle the bitmap received from tyhe camera after compression
	 * 
	 * @param bitmap
	 *            Compressed bitmap
	 * @param quality
	 *            quality on compressed Bitmap
	 */
	private void onImageReceived(int requestCode, int resultCode,
			Bitmap bitmap, int quality) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		if (mIsFromTyreMgmnt) {
			if (requestCode == requestCode1 && resultCode == RESULT_OK) {
				TyreOperationActivity.mBitmapPhoto1 = bitmap;
				TyreOperationActivity.mBitmapPhoto1Quality = quality;
				mPhotoTextPage1.setVisibility(View.GONE);
				mButtonLayout1.setVisibility(View.VISIBLE);
				mPhotoView1.setVisibility(View.VISIBLE);
				mImageView1.setImageBitmap(TyreOperationActivity.mBitmapPhoto1);
			} else if (requestCode == requestCode2 && resultCode == RESULT_OK) {
				TyreOperationActivity.mBitmapPhoto2 = bitmap;
				TyreOperationActivity.mBitmapPhoto2Quality = quality;
				mPhotoTextPage2.setVisibility(View.GONE);
				mButtonLayout2.setVisibility(View.VISIBLE);
				mPhotoView2.setVisibility(View.VISIBLE);
				mImageView2.setImageBitmap(TyreOperationActivity.mBitmapPhoto2);
			} else if (requestCode == requestCode3 && resultCode == RESULT_OK) {
				TyreOperationActivity.mBitmapPhoto3 = bitmap;
				TyreOperationActivity.mBitmapPhoto3Quality = quality;
				mPhotoTextPage3.setVisibility(View.GONE);
				mButtonLayout3.setVisibility(View.VISIBLE);
				mPhotoView3.setVisibility(View.VISIBLE);
				mImageView3.setImageBitmap(TyreOperationActivity.mBitmapPhoto3);
			}
		} else {
			if (requestCode == requestCode1 && resultCode == RESULT_OK) {
				Constants.sBitmapPhoto1 = bitmap;
				Constants.sBitmapPhoto1Quality = quality;
				mPhotoTextPage1.setVisibility(View.GONE);
				mButtonLayout1.setVisibility(View.VISIBLE);
				mPhotoView1.setVisibility(View.VISIBLE);
				mImageView1.setImageBitmap(Constants.sBitmapPhoto1);
			} else if (requestCode == requestCode2 && resultCode == RESULT_OK) {
				Constants.sBitmapPhoto2 = bitmap;
				Constants.sBitmapPhoto2Quality = quality;
				mPhotoTextPage2.setVisibility(View.GONE);
				mButtonLayout2.setVisibility(View.VISIBLE);
				mPhotoView2.setVisibility(View.VISIBLE);
				mImageView2.setImageBitmap(Constants.sBitmapPhoto2);
			} else if (requestCode == requestCode3 && resultCode == RESULT_OK) {
				Constants.sBitmapPhoto3 = bitmap;
				Constants.sBitmapPhoto3Quality = quality;
				mPhotoTextPage3.setVisibility(View.GONE);
				mButtonLayout3.setVisibility(View.VISIBLE);
				mPhotoView3.setVisibility(View.VISIBLE);
				mImageView3.setImageBitmap(Constants.sBitmapPhoto3);
			}
		}
		refreshMenu();
	}

	/**
	 * Retaining data on Orientation Change
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (null != mViewFlipper) {
			outState.putInt("CURRENT_PAGE", mViewFlipper.getDisplayedChild());
		}
		if (validationRequired) {
			if (null != mDamageTypeSpinner
					&& mDamageTypeSpinner.getSelectedItem() != null) {
				Constants.SELECTED_DAMAGE_TYPE = mDamageTypeSpinner
						.getSelectedItem().toString();
			}
			if (null != mDamageAreaSpinner
					&& mDamageAreaSpinner.getSelectedItem() != null) {
				Constants.SELECTED_DAMAGE_AREA = mDamageAreaSpinner
						.getSelectedItem().toString();
			}
		}
		outState.putInt("retainImageSavedClicked", mImageSavedClicked);
		outState.putBoolean("retainImageSavedIconState", canSaveImage);
		outState.putInt("retainbutton_layout1State",
				mButtonLayout1.getVisibility());
		outState.putInt("retainbutton_layout2State",
				mButtonLayout2.getVisibility());
		outState.putInt("retainbutton_layout3State",
				mButtonLayout3.getVisibility());

		outState.putParcelable("retainInitialElement", initialValues);
		outState.putBoolean("isAlertDialogDisplaying",
				(mAlertDialog != null && mAlertDialog.isShowing()));
		outState.putBoolean(DAMAGE_SPINNER_DISABLED,
				mDamageAreaSpinner.isEnabled());
	}

	/**
	 * Overriding onBackPressed
	 */
	@Override
	public void onBackPressed() {
		onBackpressed = true;
		if (hasChanges()) {
			actionBackPressed();
		} else {
			LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press", false, false, false);
			super.onBackPressed();
			mUserSelectedBreakSpot = false;
			mUserSelectedDamageScrap = false;
			mEditMaximumTireNSK.setText("");
			mEditMinimumTireNSK.setText("");
		}
	}

	private boolean hasChanges() {
		if (null == initialValues) {
			return false;
		}
		boolean hasImage1 = false;
		boolean hasImage2 = false;
		boolean hasImage3 = false;
		if (mIsFromTyreMgmnt) {
			hasImage1 = (null != TyreOperationActivity.mBitmapPhoto1);
			hasImage2 = (null != TyreOperationActivity.mBitmapPhoto2);
			hasImage3 = (null != TyreOperationActivity.mBitmapPhoto3);
		} else {
			hasImage1 = (null != Constants.sBitmapPhoto1);
			hasImage2 = (null != Constants.sBitmapPhoto2);
			hasImage3 = (null != Constants.sBitmapPhoto3);
		}
		// Checking is changes in images, Notes, DamageType, DamageArea
		if (hasImage1 != initialValues.isHasImage1()
				|| hasImage2 != initialValues.isHasImage2()
				|| hasImage3 != initialValues.isHasImage3()) {
			return true;
		} else if (mNotesEditText1 != null
				&& !mNotesEditText1.getText().toString()
						.equals(initialValues.getNotes1())
				|| !mNotesEditText2.getText().toString()
						.equals(initialValues.getNotes2())
				|| !mNotesEditText3.getText().toString()
						.equals(initialValues.getNotes3())) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.note, menu);
		MenuItem buttonSaved = menu.findItem(R.id.action_image_save);

		buttonSaved.setEnabled(canSaveImage);
		buttonSaved.setIcon(canSaveImage ? R.drawable.savejob_navigation
				: R.drawable.unsavejob_navigation);
		return true;
	}

	/**
	 * Used to show the alert dialog which prompts when user tries to exit
	 * without saving
	 */
	private void actionBackPressed() {
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", false, true, false);
		LayoutInflater li = LayoutInflater.from(CameraActivity.this);
		View promptsView = li
				.inflate(R.layout.dialog_logout_confirmation, null);
		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
				CameraActivity.this);
		alertDialogBuilder.setView(promptsView);
		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
		infoTv.setText(mDbHelper.getLabel(241));
		alertDialogBuilder.setCancelable(false).setPositiveButton(

				mDbHelper.getLabel(331), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
						resetCurntChanges();
						CameraActivity.this.finish();
					}
				});
		alertDialogBuilder.setCancelable(false).setNegativeButton(
				mDbHelper.getLabel(332), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);

						dialog.cancel();
					}
				});
		mAlertDialog = alertDialogBuilder.create();
		mAlertDialog.show();
		mAlertDialog.setCanceledOnTouchOutside(false);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_image_save) {
			// get the user text from primary note box
			mPrimaryNoteText = mNotesEditText1.getText().toString().trim();
			if (validationRequired) {
				if (mDamageTypeSpinner.getSelectedItemPosition() == 0) {
					LogUtil.TraceInfo(TRACE_TAG, "Save",mDbHelper.getLabel(428) , false, true, false);
					Toast.makeText(getApplicationContext(),
							mDbHelper.getLabel(428), Toast.LENGTH_SHORT).show();
				} else if (mDamageAreaSpinner.getSelectedItemPosition() == 0) {
					LogUtil.TraceInfo(TRACE_TAG, "Save",mDbHelper.getLabel(428) , false, true, false);
					Toast.makeText(getApplicationContext(),
							mDbHelper.getLabel(428), Toast.LENGTH_SHORT).show();
				} else if (mIsFromTyreMgmnt) {
					if (TyreOperationActivity.mBitmapPhoto1 == null) {
						LogUtil.TraceInfo(TRACE_TAG, "Save",mDbHelper.getLabel(185) , false, true, false);
						Toast.makeText(getApplicationContext(),
								mDbHelper.getLabel(185), Toast.LENGTH_SHORT)
								.show();
					} else {
						finishCameraActivity();
					}
				} else if (!mIsFromTyreMgmnt) {
					if (Constants.sBitmapPhoto1 == null) {
						LogUtil.TraceInfo(TRACE_TAG, "none", mDbHelper.getLabel(185), false, false, false);
						CommonUtils.notify(mDbHelper.getLabel(185),
								getApplicationContext());
					} else if (!checkMaxMinNSK() && mTreadBreakspot) {
						String errorMessage = mDbHelper
								.getLabel(Constants.NSK_WRONG_LABEL);
						LogUtil.TraceInfo(TRACE_TAG, "none", errorMessage, false, false, false);
						CommonUtils.notify(errorMessage,
								getApplicationContext());
					} else {
						addMinMaxNSKTextToNotes();
						finishCameraActivity();
					}
				} else {
					finishCameraActivity();
				}
			} else {
				finishCameraActivity();
			}
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * add Minimum Maximum NSK to the notes text from user input
	 */
	private String addMinMaxNSKTextToNotes() {
		String minMaxNSK = "";
		String tDMaxLabel = mDbHelper.getLabel(Constants.TD_MAX_LABEL);
		String tDMinLabel = mDbHelper.getLabel(Constants.TD_MIN_LABEL);
		if ((mUserSelectedBreakSpot || mUserSelectedDamageScrap)
				&& mPrimaryNoteText != null) {
			String nskMax = mEditMaximumTireNSK.getText().toString().trim();
			String nskMin = mEditMinimumTireNSK.getText().toString().trim();
			minMaxNSK = tDMaxLabel + ": " + nskMax + "mm " + tDMinLabel + ": "
					+ nskMin + "mm |";
		}
		return minMaxNSK;
	}

	/**
	 * Check maximum and minmum NSK is properly added
	 * 
	 * @return true if every thing is fine, else false
	 */
	private boolean checkMaxMinNSK() {
		if (mUserSelectedBreakSpot || mUserSelectedDamageScrap) {
			String valMaxNSK = mEditMaximumTireNSK.getText().toString().trim();
			int lenMaxNSK = valMaxNSK.length();
			String valMinNSK = mEditMinimumTireNSK.getText().toString().trim();
			int lenMinNSK = valMinNSK.length();
			// if max and min lenght not entered
			if (lenMaxNSK <= 0 || lenMinNSK <= 0
					|| Float.parseFloat(valMinNSK) < 0
					|| Float.parseFloat(valMaxNSK) <= 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Used to release camera and bitmap resource
	 */
	private void finishCameraActivity() {
		LogUtil.TraceInfo(TRACE_TAG, "none","Save", false, false, false);
		if (validationRequired) {
			if (null != mDamageTypeSpinner.getSelectedItem()) {
				Constants.SELECTED_DAMAGE_TYPE = mDamageTypeSpinner
						.getSelectedItem().toString();
			}
			if (null != mDamageAreaSpinner.getSelectedItem()) {
				Constants.SELECTED_DAMAGE_AREA = mDamageAreaSpinner
						.getSelectedItem().toString();
			}
		} else {
			Constants.SELECTED_DAMAGE_TYPE = "";
			Constants.SELECTED_DAMAGE_AREA = "";
		}
		if (Constants.TM_CAMERA_ICON_CLICKED == 1) {
			Constants.SELECTED_DAMAGE_TYPE_LIST
					.add(Constants.SELECTED_DAMAGE_TYPE);
			Constants.SELECTED_DAMAGE_AREA_LSIT
					.add(Constants.SELECTED_DAMAGE_AREA);
		}
		if (!mPrimaryNoteText.equals("") && mPrimaryNoteText != null
				&& !Constants.TM_CAMERA_ICON_CLICKED_FLAG) {
			Constants.DAMAGE_NOTES1 = mPrimaryNoteText;
			Constants.damageNote_TM_LIST1.add(Constants.DAMAGE_NOTES1);
		}
		if (!mNotesEditText2.getText().toString().equals("")
				&& mNotesEditText2.getText().toString() != null) {
			Constants.DAMAGE_NOTES2 = mNotesEditText2.getText().toString();
			Constants.damageNote_TM_LIST2.add(Constants.DAMAGE_NOTES2);
		}
		if (!mNotesEditText3.getText().toString().equals("")
				&& mNotesEditText3.getText().toString() != null) {
			Constants.DAMAGE_NOTES3 = mNotesEditText3.getText().toString();
			Constants.damageNote_TM_LIST3.add(Constants.DAMAGE_NOTES3);
		}
		if (mImageView1.getDrawable() != null) {
			saveImage1();
		}
		if (mImageView2.getDrawable() != null) {
			saveImage2();
		}
		if (mImageView3.getDrawable() != null) {
			saveImage3();
		}
		if (mIsFromTyreMgmnt) {
			if (TyreOperationActivity.mBitmapPhoto1 == null) {
				Constants.damageImage_TM_LIST_Bitmap1.add(null);
			}
			if (TyreOperationActivity.mBitmapPhoto2 == null) {
				Constants.damageImage_TM_LIST_Bitmap2.add(null);
			}
			if (TyreOperationActivity.mBitmapPhoto3 == null) {
				Constants.damageImage_TM_LIST_Bitmap3.add(null);
			}
			if (TyreOperationActivity.mBitmapPhoto1 != null
					&& Constants.TM_CAMERA_ICON_CLICKED_FLAG) {
				Constants.damageImage_TM_LIST_Bitmap1.removeAll(Collections
						.singleton(null));
			}
			if (TyreOperationActivity.mBitmapPhoto2 != null
					&& Constants.TM_CAMERA_ICON_CLICKED_FLAG) {
				Constants.damageImage_TM_LIST_Bitmap2.removeAll(Collections
						.singleton(null));
			}
			if (TyreOperationActivity.mBitmapPhoto3 != null
					&& Constants.TM_CAMERA_ICON_CLICKED_FLAG) {
				Constants.damageImage_TM_LIST_Bitmap3.removeAll(Collections
						.singleton(null));
			}
			if (Constants.DAMAGE_NOTES1 == "") {
				Constants.damageNote_TM_LIST1.add("");
			}
			if (Constants.DAMAGE_NOTES2 == "") {
				Constants.damageNote_TM_LIST2.add("");
			}
			if (Constants.DAMAGE_NOTES3 == "") {
				Constants.damageNote_TM_LIST3.add("");
			}
			if (Constants.DAMAGE_NOTES1 != ""
					&& Constants.TM_CAMERA_ICON_CLICKED_FLAG) {
				Constants.damageNote_TM_LIST1.removeAll(Collections
						.singleton(""));
			}
			if (Constants.DAMAGE_NOTES2 != ""
					&& Constants.TM_CAMERA_ICON_CLICKED_FLAG) {
				Constants.damageNote_TM_LIST2.removeAll(Collections
						.singleton(""));
			}
			if (Constants.DAMAGE_NOTES3 != ""
					&& Constants.TM_CAMERA_ICON_CLICKED_FLAG) {
				Constants.damageNote_TM_LIST3.removeAll(Collections
						.singleton(""));
			}

		}

		// check if the current operation is break spot dismount
		// then send the min max nsk values
		if (mUserSelectedBreakSpot) {
			Intent intentReturn = new Intent();
			String minMaxNSK = addMinMaxNSKTextToNotes();
			intentReturn.putExtra(BREAK_SPOT_KEY, minMaxNSK);
			setResult(RESULT_OK, intentReturn);
		}
		if (mUserSelectedDamageScrap && mEditMaximumTireNSK.isShown()) {
			Intent intentReturn = new Intent();
			String minMaxNSK = addMinMaxNSKTextToNotes();
			intentReturn.putExtra(DAMAGE_SCRAP_KEY, minMaxNSK);
			setResult(RESULT_OK, intentReturn);
		}
		onBackpressed = false;
		finish();
	}

	/**
	 * Hides and shows the views depending on current page of ViewFlipper
	 */
	private void manageViewsByPage() {
		int pageNo;
		if (mViewFlipper.getCurrentView().getId() == R.id.page1) {
			pageNo = 0;
		} else if (mViewFlipper.getCurrentView().getId() == R.id.page2) {
			pageNo = 1;
		} else {
			pageNo = 2;
		}
		switch (pageNo) {
		case 0:
			if (validationRequired) {
				mDamageTypeSpinner.setVisibility(View.VISIBLE);
				mDamageAreaSpinner.setVisibility(View.VISIBLE);
			} else {
				mDamageTypeSpinner.setVisibility(View.GONE);
				mDamageAreaSpinner.setVisibility(View.GONE);
			}
			mNotesEditText1.setVisibility(View.VISIBLE);
			mNotesEditText2.setVisibility(View.INVISIBLE);
			mNotesEditText3.setVisibility(View.INVISIBLE);
			// fix :: on damage scrap view flip max min nsk fields appear
			// if damage scrap and not break spot then not visible
			if (mUserSelectedDamageScrap && !mTreadBreakspot) {
				setNSKVisibilty(NSK_GONE);
			} else {
				setNSKVisibilty(NSK_VISIBLE);
			}
			break;
		case 1:
			mDamageTypeSpinner.setVisibility(View.INVISIBLE);
			mDamageAreaSpinner.setVisibility(View.INVISIBLE);
			mNotesEditText1.setVisibility(View.INVISIBLE);
			mNotesEditText2.setVisibility(View.VISIBLE);
			mNotesEditText3.setVisibility(View.INVISIBLE);
			setNSKVisibilty(NSK_GONE);
			break;
		case 2:
			mDamageTypeSpinner.setVisibility(View.INVISIBLE);
			mDamageAreaSpinner.setVisibility(View.INVISIBLE);
			mNotesEditText1.setVisibility(View.INVISIBLE);
			mNotesEditText2.setVisibility(View.INVISIBLE);
			mNotesEditText3.setVisibility(View.VISIBLE);
			setNSKVisibilty(NSK_GONE);
			break;
		}
		updatePageIndicator();
	}

	private void compressInHeadlessFragment(int requestCode, int resultCode) {
		FragmentManager fragmentManager = getFragmentManager();
		CompressorFragment compressorFragment = (CompressorFragment) fragmentManager
				.findFragmentByTag(CompressorFragment.TAG);
		if (compressorFragment != null) {
			fragmentManager.beginTransaction().remove(compressorFragment)
					.commit();
		} else {
			LogUtil.i("CmaeraActivity", "fragmentManager is NOT NULL");
		}
		LogUtil.i("CmaeraActivity", "fragmentManager is NULL");
		Bundle args = new Bundle();
		args.putInt("requestCode", requestCode);
		args.putInt("resultCode", resultCode);
		compressorFragment = CompressorFragment.newInstance(args);
		fragmentManager.beginTransaction()
				.add(compressorFragment, CompressorFragment.TAG).commit();
	}

	/**
	 * Class handling Image Compression
	 * 
	 * @author amitkumar.h
	 * 
	 */
	public static class CompressorFragment extends Fragment {
		public static final String TAG = "CompressorFragment";
		public static final String IMAGE_LOG_TAG = "ImageCompression";
		private File mOriginalImageFile;
		private Dialog mProgressDialog;
		private Bitmap mBitmap;
		private int mQuality;
		private int mRequestCode;
		private int mResultCode;
		private boolean isDialogShown;
		private double mScale = 1.0;
		private Context mContext;

		public CompressorFragment() {
		}

		public static CompressorFragment newInstance(Bundle args) {
			CompressorFragment fragment = new CompressorFragment();
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			mContext = (Context) activity;
			if (isDialogShown) {
				mProgressDialog = new GYProgressDialog(activity);
				mProgressDialog.show();
			}
		}

		@Override
		public void onDetach() {
			if (isDialogShown = (mProgressDialog != null && mProgressDialog
					.isShowing())) {
				mProgressDialog.dismiss();
			}
			super.onDetach();
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setRetainInstance(true);
			Bundle arguments = getArguments();
			mRequestCode = arguments.getInt("requestCode");
			mResultCode = arguments.getInt("resultCode");
			mScale = 1.0;
			mQuality = 80;
			compressInBackground();
		}

		/**
		 * Compresses the image in background using AsyncTask
		 */
		private void compressInBackground() {
			if (mOriginalImageFile == null) {
				LogUtil.i("CameraActivity", "Searching file under :: "
						+ Constants.IMAGES_PATH + ORIGINAL_FILE_NAME);
				mOriginalImageFile = new File(Constants.IMAGES_PATH,
						ORIGINAL_FILE_NAME);
			}

			if (mOriginalImageFile == null) {
				LogUtil.i("CameraActivity", "Image is null");
				return;
			}
			if (!mOriginalImageFile.exists()) {
				LogUtil.i("CameraActivity",
						"Image not captured, Please try again");
				return;
			}

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				new CompresserTask()
						.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			} else {
				new CompresserTask();
			}
		}

		private class CompresserTask extends AsyncTask<Void, Void, Void> {

			@Override
			protected void onPreExecute() {
				LogUtil.i("CameraActivity", "Compress preexecuted::::::::::");
				mProgressDialog = new GYProgressDialog(getActivity());
				mProgressDialog.show();
			}

			@Override
			protected void onPostExecute(Void aVoid) {
				try {
					LogUtil.i("CameraActivity",
							"Compress postexecuted::::::::::");
					if (mProgressDialog != null && mProgressDialog.isShowing()) {
						mProgressDialog.dismiss();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			protected Void doInBackground(Void... voids) {
				LogUtil.i("CameraActivity", "Compress background::::::::::");
				compress();
				return null;
			}
		}

		/**
		 * Compresses the image by scaling it down
		 */
		private void compress() {
			int bitmapSize = (int) mOriginalImageFile.length();
			mBitmap = BitmapFactory.decodeFile(mOriginalImageFile.getPath());
			LogUtil.d(IMAGE_LOG_TAG, "Original size: " + bitmapSize);
			if (isValidSize(bitmapSize)) {
				onCompressionComplete(mBitmap);
			} else {
				scaleImage(bitmapSize);
			}
		}

		private boolean isValidSize(long bitmapSize) {
			return bitmapSize < IMAGE_MAX_SIZE;
		}

		private void scaleImage(long bitmapSize) {
			// crash reported here
			if (mBitmap == null)
				return;
			if (bitmapSize > IMAGE_MAX_SIZE) {
				mScale *= Math.sqrt(((double) bitmapSize) / IMAGE_MAX_SIZE);
				int newWidth = (int) (mBitmap.getWidth() / mScale);
				int newHeight = (int) (mBitmap.getHeight() / mScale);
				mQuality = 80;
				compress(newWidth, newHeight);
			}
		}

		/**
		 * Compresses the image by reducing the quality using recursive
		 * iteration
		 */
		private void compress(int width, int height) {
			Bitmap compressedBitmap = Bitmap.createScaledBitmap(mBitmap, width,
					height, true);
			long compressedSize = saveBitmap(compressedBitmap, COMPRESSED_FILE,
					mQuality);
			if (isValidSize(compressedSize)) {
				LogUtil.d(IMAGE_LOG_TAG, "Compressed to " + compressedSize
						+ " @ " + mQuality + "%");
				onCompressionComplete(compressedBitmap);
			} else {
				mQuality -= 10;
				if (mQuality <= 0) {
					scaleImage(compressedSize);
					return;
				}
				compress(width, height);
			}
		}

		/**
		 * Once compression is complete it notifies activity in UI thread and
		 * clean up temporary images
		 * 
		 * @param finalBitmap
		 *            final im Bitmap after compression
		 */
		private void onCompressionComplete(final Bitmap finalBitmap) {
			final CameraActivity activity = (CameraActivity) getActivity();
			if (activity == null) {
				return;
			}
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					activity.onImageReceived(mRequestCode, mResultCode,
							finalBitmap, mQuality);
					cleanUpImages();
					getFragmentManager().beginTransaction()
							.remove(CompressorFragment.this).commit();
				}
			});
		}

		/**
		 * Since the bitmap size is not equal to image file size only way from
		 * which we can know what is the size of file is to save its as file on
		 * disk and then read its size
		 * 
		 * @param bitmap
		 *            Bitmap to be saved
		 * @param name
		 *            name of the file to be saved as
		 * @param quality
		 *            image compression quality
		 * @return size of the image file
		 */
		public long saveBitmap(Bitmap bitmap, String name, int quality) {
			try {
				File file = new File(Constants.IMAGES_PATH, name);
				FileOutputStream fileOutputStream = new FileOutputStream(file);
				bitmap.compress(Bitmap.CompressFormat.JPEG, quality,
						fileOutputStream);
				fileOutputStream.flush();
				fileOutputStream.close();
				return file.length();
			} catch (java.io.IOException e) {
				e.printStackTrace();
			}
			return 0;
		}

		/**
		 * clean the temporary image files created for compression
		 */
		private void cleanUpImages() {
			File compressedImg = new File(Constants.IMAGES_PATH,
					COMPRESSED_FILE);
			if (compressedImg.exists()) {
				compressedImg.delete();
			}
			File originalImg = new File(Constants.IMAGES_PATH,
					ORIGINAL_FILE_NAME);
			if (originalImg.exists()) {
				originalImg.delete();
			}
		}

	}

	/**
	 * reset the current changes
	 */
	private void resetCurntChanges() {
		if (mIsFromTyreMgmnt) {
			resetTyreOperationChanges();
		} else {
			resetDismountChanges();
		}
	}

	/**
	 * reset the changes made for dismount
	 */
	private void resetDismountChanges() {
		// Don't add break statement, it's sequence of execution
		switch (mImgCountFromPreviousActivity) {
		case 0:
			Constants.DAMAGE_NOTES1 = "";
			Constants.SELECTED_DAMAGE_TYPE = null;
			Constants.SELECTED_DAMAGE_AREA = null;
			Constants.sBitmapPhoto1 = null;
			mEditMaximumTireNSK.setText("");
			mEditMinimumTireNSK.setText("");
		case 1:
			Constants.DAMAGE_NOTES2 = "";
			Constants.sBitmapPhoto2 = null;
			mEditMaximumTireNSK.setText("");
			mEditMinimumTireNSK.setText("");
		case 2:
			Constants.DAMAGE_NOTES3 = "";
			Constants.sBitmapPhoto3 = null;
			mEditMaximumTireNSK.setText("");
			mEditMinimumTireNSK.setText("");
		}
	}

	/**
	 * reset the changes made for TyreOperations
	 */
	private void resetTyreOperationChanges() {
		// Don't add break statement, it's sequence of execution
		switch (mImgCountFromPreviousActivity) {
		case 0:
			if (null != TyreOperationActivity.mBitmapPhoto1) {
				Constants.damageImage_TM_LIST_Bitmap1
						.remove(TyreOperationActivity.mBitmapPhoto1);
			}
			TyreOperationActivity.mBitmapPhoto1 = null;
			Constants.DAMAGE_NOTES1 = "";
		case 1:
			if (null != TyreOperationActivity.mBitmapPhoto2) {
				Constants.damageImage_TM_LIST_Bitmap2
						.remove(TyreOperationActivity.mBitmapPhoto2);
			}
			TyreOperationActivity.mBitmapPhoto2 = null;
			Constants.DAMAGE_NOTES2 = "";
		case 2:
			if (null != TyreOperationActivity.mBitmapPhoto3) {
				Constants.damageImage_TM_LIST_Bitmap3
						.remove(TyreOperationActivity.mBitmapPhoto3);
			}
			TyreOperationActivity.mBitmapPhoto3 = null;
			Constants.DAMAGE_NOTES3 = "";

		}
	}

	/**
	 * This is used to capture tap events for camera request and swipe events
	 * next actions
	 */
	private final class CustomTouchListener implements OnTouchListener {
		@Override
		@TargetApi(11)
		public boolean onTouch(View view, MotionEvent motionEvent) {
			if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
				return true;
			} else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
				pos1x = (int) motionEvent.getX();
				return true;
			} else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
				int pos2x = (int) motionEvent.getX();
				if (Math.abs(pos2x - pos1x) < 5) {
					Intent cameraIntent = new Intent(
							MediaStore.ACTION_IMAGE_CAPTURE);
					File file = new File(Constants.IMAGES_PATH);
					if (!file.exists()) {
						file.mkdir();
					}
					File imageFile = new File(Constants.IMAGES_PATH,
							ORIGINAL_FILE_NAME);
					LogUtil.i("CameraActivity", "save location under :: "
							+ Constants.IMAGES_PATH + ORIGINAL_FILE_NAME);
					cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
							Uri.fromFile(imageFile));
					if (view == mPhotoTextPage1) {
						startActivityForResult(cameraIntent, requestCode1);
					} else if (view == mPhotoTextPage2) {
						startActivityForResult(cameraIntent, requestCode2);
					} else if (view == mPhotoTextPage3) {
						startActivityForResult(cameraIntent, requestCode3);
					}
				} else if ((pos2x - pos1x) >= SWIPE_MIN_DISTANCE) {
					mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(
							mContext, R.anim.right_in));
					mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(
							mContext, R.anim.right_out));
					mViewFlipper.showPrevious();
					manageViewsByPage();
				} else if ((pos1x - pos2x) >= SWIPE_MIN_DISTANCE) {
					mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(
							mContext, R.anim.left_in));
					mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(
							mContext, R.anim.left_out));
					mViewFlipper.showNext();
					manageViewsByPage();
				}
				return true;
			} else {
				return false;
			}
		}
	}

	private void refreshMenu() {
		boolean isImageAvailable = false;
		if (Constants.ISBREAKSPOT) {
			isImageAvailable = (mImageView1 != null && mImageView1
					.getDrawable() != null);
		} else {
			isImageAvailable = ((mImageView1 != null && mImageView1
					.getDrawable() != null)
					|| (mImageView2 != null && mImageView2.getDrawable() != null) || (mImageView3 != null && mImageView3
					.getDrawable() != null));
		}
		boolean isDamageAreaSpinnerValid = mDamageAreaSpinner != null
				&& mDamageAreaSpinner.isEnabled()
				&& mDamageAreaSpinner.getSelectedItemPosition() != 0;
		boolean isDamageTypeValid = mDamageTypeSpinner != null
				&& mDamageTypeSpinner.isEnabled()
				&& mDamageTypeSpinner.getSelectedItemPosition() != 0;
		boolean areSpinnersValid = isDamageAreaSpinnerValid
				&& isDamageTypeValid;
		if (validationRequired && !Constants.ISBREAKSPOT
				&& !mUserSelectedDamageScrap
				&& TextUtils.isEmpty(Constants.THRESHOLD_TD_FOR_CAMERA)) {
			canSaveImage = areSpinnersValid && isImageAvailable;
		} else {
			canSaveImage = isImageAvailable;
		}
		if (mIsInEditMode) {
			canSaveImage |= hasChanges();
		}
		invalidateOptionsMenu();
	}

	/**
	 * Class for handling Image Notes
	 * 
	 * @author amitkumar.h
	 * 
	 */
	private class NoteTextWatcher implements TextWatcher {

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			refreshMenu();
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	}

	private void setUpPageIndicators() {
		LinearLayout parent = (LinearLayout) findViewById(R.id.page_indicator_container);
		for (int i = 0; i < 3; i++) {
			ImageView imageView = new ImageView(this);
			imageView.setImageResource(R.drawable.page_indicator_off);
			imageView.setPadding(8, 8, 8, 8);
			parent.addView(imageView);
		}
		updatePageIndicator();
	}

	private void updatePageIndicator() {
		int currentPage = mViewFlipper.getDisplayedChild();
		LinearLayout parent = (LinearLayout) findViewById(R.id.page_indicator_container);
		for (int i = 0; i < parent.getChildCount(); i++) {
			ImageView indicator = (ImageView) parent.getChildAt(i);
			indicator
					.setImageResource(currentPage == i ? R.drawable.page_indicator_on
							: R.drawable.page_indicator_off);
		}
	}

	private class NSKMaxChanged implements TextWatcher {

		@Override
		public void afterTextChanged(Editable nskOne) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mEditMaximumTireNSK.getText().toString();
			float maxNskVal = CommonUtils.parseFloat(strEnteredVal);
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				if (maxNskVal > 30) {
					mEditMaximumTireNSK.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mEditMaximumTireNSK.setText("");
			}
			float minNskVal = CommonUtils.parseFloat(mEditMinimumTireNSK
					.getText().toString().trim());
			if (maxNskVal <= minNskVal && !onBackpressed
					&& !mEditMaximumTireNSK.getText().toString().equals("")) {
				// TODO: get correct message
				CommonUtils
						.notify(mWrongMinimumNSKMessage, CameraActivity.this);
				mEditMinimumTireNSK.setText("");
			}
		}
	}

	private class NSKMinChanged implements TextWatcher {

		@Override
		public void afterTextChanged(Editable nskOne) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mEditMinimumTireNSK.getText().toString();
			if (strEnteredVal.equals("")) {
				return;
			}
			float minNskVal = CommonUtils.parseFloat(strEnteredVal);
			float maxNskVal = CommonUtils.parseFloat(mEditMaximumTireNSK
					.getText().toString());
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				if (minNskVal > 30) {
					mEditMinimumTireNSK.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mEditMinimumTireNSK.setText("");
			}
			// TODO: change text
			if (minNskVal >= maxNskVal && !onBackpressed
					&& !mEditMaximumTireNSK.getText().toString().equals("")) {
				mEditMinimumTireNSK.setText("");
				LogUtil.TraceInfo(TRACE_TAG, "none", "Msg : Max NSK cannot be less then or equal to Min Nsk", false, false, false);
				CommonUtils.notify(
						"Max NSK cannot be less then or equal to Min Nsk",
						CameraActivity.this);
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		// set user inactivity handler for this activity
		LogoutHandler.setCurrentActivity(this);
	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		InactivityUtils.logoutFromActivityAboveEjobForm(this);
	}

	private void loadLabelsFromDB() {
		try {
			DatabaseAdapter mDbHelperForLabels = new DatabaseAdapter(this);
			mDbHelperForLabels.createDatabase();
			mDbHelperForLabels.open();
			String loadDBLabel = "";
			loadDBLabel = mDbHelperForLabels
					.getLabel(Constants.BREAK_SPOT_INFO_LABEL);
			mTextBreakSpotInfo.setText(loadDBLabel);
			loadDBLabel = mDbHelperForLabels.getLabel(Constants.NOTE_LABEL);
			mNotesEditText1.setHint(loadDBLabel);
			mNotesEditText2.setHint(loadDBLabel);
			mNotesEditText3.setHint(loadDBLabel);
			loadDBLabel = mDbHelperForLabels
					.getLabel(Constants.TAP_TO_TAKE_PICTURE_LABEL);
			mPhotoTextPage1.setText(loadDBLabel + " 1");
			mPhotoTextPage2.setText(loadDBLabel + " 2");
			mPhotoTextPage3.setText(loadDBLabel + " 3");
			setTitle(mDbHelperForLabels.getLabel(Constants.PHOTO_LABEL));
			loadDBLabel = mDbHelperForLabels.getLabel(Constants.TD_MAX_LABEL);
			mTextMaximumTireNSK.setText(loadDBLabel);
			loadDBLabel = mDbHelperForLabels.getLabel(Constants.TD_MIN_LABEL);
			mTextMinimumTireNSK.setText(loadDBLabel);
			mWrongMinimumNSKMessage = mDbHelperForLabels
					.getLabel(Constants.INVALID_NSK_LABEL);
			mDbHelperForLabels.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}