package com.goodyear.ejob.sync;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.FileOperations;
import com.goodyear.ejob.util.SyncTime;

/**
 * @author shailesh.p
 * 
 */
public class PreSync {

	private static int completion = 0;

	/**
	 * create pack file for translation sync
	 * 
	 * @throws IOException
	 */
	public static void createPackFile(Context context) throws IOException {
		long lastSyncTime = getLastSyncTime(context);
		File packFile = new File(Constants.PROJECT_PATH
				+ Constants.PACKFILE_PATH);
		if (packFile.exists()) {
			packFile.delete();
			packFile.createNewFile();
		}
		FileOutputStream fileStream = new FileOutputStream(packFile);
		byte[] head = DataPackage.GetBynaryHead(ActionType.DownloadUpload,
				RequestType.nothing, BodyType.database, Result.unknown,
				EntityType.tyre, completion,
				DateTimeUTC.toDatesTicks(lastSyncTime, context));
		fileStream.write(head, 0, head.length);
		fileStream.close();
	}

	/**
	 * create pack file for auto sync
	 * 
	 * @throws IOException
	 */
	public static void createPackFileForAutoSync(Context context)
			throws IOException {
		long lastSyncTime = getLastSyncTime(context);
		File packFile = new File(Constants.PROJECT_PATH
				+ Constants.PACKFILE_PATH);
		if (packFile.exists()) {
			packFile.delete();
			packFile.createNewFile();
		}
		FileOutputStream fileStream = new FileOutputStream(packFile);
		byte[] head = DataPackage.GetBynaryHead(ActionType.Download,
				RequestType.nothing, BodyType.database, Result.unknown,
				EntityType.tyre, completion,
				DateTimeUTC.toDatesTicks(lastSyncTime, context));
		fileStream.write(head, 0, head.length);
		fileStream.close();
	}

	/**
	 * create pack file for Key value request
	 */
	public static void createPackFileForKeyValueRequest(byte[] loginBody,
			int requestType, int actionType) {
		try {
			File packFile = new File(Constants.PROJECT_PATH
					+ Constants.PACKFILE_PATH);
			if (packFile.exists()) {
				packFile.delete();
				packFile.createNewFile();
			}
			FileOutputStream fileStream = new FileOutputStream(packFile);
			long timeTicks = DateTimeUTC.getMillisecondsFromUTCdateLogin();
			byte[] head = DataPackage.GetBynaryHead(actionType, requestType,
					BodyType.KeyValuePair, Result.unknown, EntityType.tyre,
					completion, timeTicks);

			fileStream.write(head, 0, head.length);
			fileStream.write(loginBody, 0, loginBody.length);
			fileStream.close();

		} catch (IOException e) {

		}
	}

	public static long getLastSyncTime(Context context) {
		DatabaseAdapter dbAdaptor = new DatabaseAdapter(context);
		dbAdaptor.open();
		SyncTime syncTime = dbAdaptor.getSyncTimeInfo();
		dbAdaptor.close();

		return (!TextUtils.isEmpty(syncTime.getValue())) ? Long
				.parseLong(syncTime.getValue()) : 0;
	}

	/**
	 * create pack file for job sync
	 */
	public static void createPackFileUD(Context context) {
		try {
			long lastSyncTime = getLastSyncTime(context);
			File packFile = new File(Constants.PROJECT_PATH
					+ Constants.PACKFILE_PATH);
			if (packFile.exists()) {
				packFile.delete();
				packFile.createNewFile();
			}
			FileOutputStream fileStream = new FileOutputStream(packFile);

			byte[] head = DataPackage.GetBynaryHead(ActionType.DownloadUpload,
					RequestType.nothing, BodyType.database, Result.unknown,
					EntityType.tyre, completion,
					DateTimeUTC.toDatesTicks(lastSyncTime, context));
			fileStream.write(head, 0, head.length);

			fileStream.close();

		} catch (IOException e) {

		}
	}

	/**
	 * compress snapshot file
	 */
	public static void getCompressedSnapshotFile() {
		String snapShot = Constants.PROJECT_PATH + "/temp/snapshot.db";
		String snapShotCom = Constants.PROJECT_PATH
				+ Constants.COMPRESSED_FILEPATH;
		try {
			new File(Constants.PROJECT_PATH + Constants.COMPRESSED_FILEPATH)
					.createNewFile();
		} catch (Exception e) {
			Log.e("PreSync", "could not create compressed file");
		}

		FileOperations.compressGzipFile(snapShot, snapShotCom);
	}
}
