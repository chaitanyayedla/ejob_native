package com.goodyear.ejob.sync;

import java.io.IOException;
import java.net.SocketTimeoutException;

import com.goodyear.ejob.authenticator.Authentication;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.FileOperations;
import com.goodyear.ejob.util.HttpRequest_Ejob;
import com.goodyear.ejob.util.LogUtil;

import android.content.Context;
import android.util.Log;

public class AuthSync {
	private static final String TAG = AuthSync.class.getSimpleName();

	/**
	 * Perform a login sync
	 * 1) create pack file 2) create compressed snapshot and make request 3) De-compress the response body
	 * @param data
	 *            - login and password as key value pair
	 * @return null if
	 */
	public static String loginSync(String... data) {
		try {

			byte[] requestBuffer = createBodyForRequest(data);
			// create the pack file
			PreSync.createPackFileForKeyValueRequest(requestBuffer,
					RequestType.nothing, ActionType.Authentication);

			byte[] compressedResponseBody = HttpRequest_Ejob
					.makeRequestForKeyValueRequest();
			return FileOperations.decompressGzipStream(compressedResponseBody);

		} catch (SocketTimeoutException e) {
			LogUtil.e(TAG, "socket time out");
			LogUtil.pst(e);
			return Authentication.SOCKET_TIMEOUT_EXCEPTION;
		} catch (Exception e) {
			Log.e("Login SYNC", "could not login" + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * start translation SYNC
	 * 
	 * 1) create pack file 2) create compressed snapshot and make request 3) De-compress the response body
	 * 
	 * @throws IOException
	 */
	public static void translationSync(Context context) throws IOException {
		PreSync.createPackFile(context);
		PreSync.getCompressedSnapshotFile();
		byte[] compressedFile = FileOperations.readFile(Constants.PROJECT_PATH
				+ Constants.COMPRESSED_FILEPATH);
		FileOperations.writeFile(Constants.PROJECT_PATH + "/pack.pack",
				compressedFile, true);
		HttpRequest_Ejob.makeRequestToGetDBFile();
	}

	/**
	 * start upload download sync (for job sync)
	 * 1) create pack file 2) create compressed snapshot and make request 3) De-compress the response body
	 * @throws IOException
	 */
	public static void uploadDownloadSync(Context context) throws IOException {
		PreSync.createPackFileUD(context);
		PreSync.getCompressedSnapshotFile();
		FileOperations.copyFile(Constants.PROJECT_PATH
				+ Constants.COMPRESSED_FILEPATH, Constants.PROJECT_PATH
				+ Constants.PACKFILE_PATH);
		HttpRequest_Ejob.makeRequestToGetDBFile();
	}

	/**
	 * start auto sync (sync after long time)
	 * 
	 * 1) create pack file 2) create compressed snapshot and make request 3) Decompress the response body
	 * 
	 * @throws IOException
	 */
	public static void autoSync(Context context) throws IOException {
		PreSync.createPackFileForAutoSync(context);
		PreSync.getCompressedSnapshotFile();
		byte[] compressedFile = FileOperations.readFile(Constants.PROJECT_PATH
				+ Constants.COMPRESSED_FILEPATH);
		FileOperations.writeFile(Constants.PROJECT_PATH + "/pack.pack",
				compressedFile, true);
		HttpRequest_Ejob.makeRequestToGetDBFile();
	}

	/**
	 * takes the key values and create the HTTP body
	 * 
	 * @param data
	 *            key value pair
	 * @return body bytes
	 */
	public static byte[] createBodyForRequest(String... data) {
		// get the body from key value pair
		String body = "";
		byte[] bodyBuffer = null;

		if (data != null && data.length > 0) {
			for (int i = 0; i < data.length; i += 2) {
				body += String.format("%1$s:%2$s\r\n", data[i], data[i + 1]);
			}
		}
		body += "\r\n\r\n";

		if (body.length() > 0) {
			bodyBuffer = body.getBytes();
		}

		return bodyBuffer;
	}

	/**
	 * Method to get Contract Vehicle List
	 * @param data search Keyword
	 * @return Vehicles Details (Json Data)
	 * @throws Exception
	 */
	public static String getContaractVehicleList(String... data)
			throws Exception {
		Constants.jsonContractVal = "";
		byte[] bodyBuffer = createBodyForRequest(data);

		PreSync.createPackFileForKeyValueRequest(bodyBuffer,
				RequestType.GetContractsByLicensePlate, ActionType.Request);

		byte[] compressedResponseBody = HttpRequest_Ejob
				.makeRequestForKeyValueRequest();
		Constants.jsonContractVal = FileOperations
				.decompressGzipStream(compressedResponseBody);
		return Constants.jsonContractVal;
	}

	/**
	 * ethod to get All Vehicle Tires by SAP Code
	 * @param data SAP Code
	 * @return Tires Details (JSON Data)
	 * @throws Exception
	 */
	public static String getAllVehicleTiresBySapCode(String... data)
			throws Exception {

		byte[] bodyBuffer = createBodyForRequest(data);

		PreSync.createPackFileForKeyValueRequest(bodyBuffer,
				RequestType.GetAllVehicleTiresBySapCode, ActionType.Request);

		byte[] compressedResponseBody = HttpRequest_Ejob
				.makeRequestForKeyValueRequest();
		Constants.jsonSAPContractVal = FileOperations
				.decompressGzipStream(compressedResponseBody);
		return Constants.jsonSAPContractVal;
	}
}
