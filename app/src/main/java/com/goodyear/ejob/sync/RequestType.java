package com.goodyear.ejob.sync;

public class RequestType {
	public static int nothing = 0;
	public static int GetContractsByLicensePlate = 1;
	public static int GetAllUnmountedTyreByUserID = 2;
	public static int GetAllVehicleTiresBySapCode = 3;
	public static int GetVendorsByUserIDAndLicensePlate = 4;
	public static int GetAvailableCabFileVersion = 5;
}
