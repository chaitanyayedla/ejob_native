package com.goodyear.ejob.sync;

import java.nio.ByteBuffer;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class ActionTime {
	private static final long TICKS_PER_MILLISECOND = 10000;

	/*
	 * public static byte []ticks = { (byte)128, (byte) 58, (byte) 99, (byte)
	 * 146,
	 * (byte) 202, (byte) 120, (byte) 209, (byte) 8 };
	 */
	/*
	 * public static byte[] timeBynary ={ (byte)128, (byte) 58, (byte) 99,
	 * (byte) 146,
	 * (byte) 202, (byte) 120, (byte) 209, (byte) 8 };
	 */
	public static byte BODY_SIZE = 52;// 78; // 23 for SapCode

	/*
	 * public static byte[] autoTimeBynary = { (byte) 0, (byte) 128, (byte) 153,
	 * (byte) 248, (byte) 171,
	 * (byte) 189, (byte) 198, (byte) 8 };
	 */
	// byte[] bodySize = { 0, 52, 0, 0 };
	public static byte[] timeBynary = { (byte) 0, (byte) 128, (byte) 153,
			(byte) 248, (byte) 171, (byte) 189, (byte) 198, (byte) 8 };
	public static byte[] timeBynaryUD = { (byte) 128, (byte) 143, (byte) 191,
			(byte) 65, (byte) 254, (byte) 147, (byte) 209, (byte) 8 };
	public static byte[] timeBynry = ByteBuffer.allocate(8)
			.putLong(getMillisecondsFromUTCdate()).array();// 635422895821870000L

	// convert time to long ticks
	public static long getMillisecondsFromUTCdate() {
		long elapsedTicks = 0L;
		try {
			// DateTime baseTime = new DateTime(1970,1,1,0,0,0);
			// DateTime baseTime = new DateTime(0001,1,1,0,0,0);//Local Time(US)
			DateTime baseTime = new DateTime(0001, 1, 1, 0, 0, 0,
					DateTimeZone.UTC);
			// DateTime currentDate = DateTime.now();
			DateTime nowInUTC = DateTime.now(DateTimeZone.UTC);//
			elapsedTicks = ((nowInUTC.getMillis() - baseTime.getMillis()) * TICKS_PER_MILLISECOND);
		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Test=>getMillisecondsFromUTCdate) : ");
			e.printStackTrace();
		}
		return elapsedTicks;
	}

}
