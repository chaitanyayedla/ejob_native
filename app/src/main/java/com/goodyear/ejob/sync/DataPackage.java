package com.goodyear.ejob.sync;

import java.nio.ByteBuffer;
import com.goodyear.ejob.util.Constants;

/**
 * Class used for Generate DataPackets
 */
public class DataPackage {

	public static final int HEAD_SIZE = 19;
	private static byte PACK_VERSION = 2;
	public String _fileName;
	public int actionType;
	public int requestType;
	public int bodyType;

	public static byte[] GetBynaryHead(int actionType, int requestType,
			int bodyType, int resultType, int entytyType, int compleation,
			long timeTicks) {
		byte[] head = new byte[HEAD_SIZE];
		byte[] bodySize = { 0, Constants._BODYSIZE, 0, 0 };
		
		byte[] timeBynary = ByteBuffer.allocate(8).putLong(timeTicks).array();
		for (int k = 0; k < timeBynary.length / 2; k++) {
			byte temp = timeBynary[k];
			timeBynary[k] = timeBynary[timeBynary.length - (1 + k)];
			timeBynary[timeBynary.length - (1 + k)] = temp;
		}
		head[0] = PACK_VERSION;
		head[1] = (byte) actionType;
		head[2] = (byte) requestType;
		head[3] = bodySize[0];
		head[4] = bodySize[1];
		head[5] = bodySize[2];
		head[6] = bodySize[3];
		head[7] = timeBynary[0];
		head[8] = timeBynary[1];
		head[9] = timeBynary[2];
		head[10] = timeBynary[3];
		head[11] = timeBynary[4];
		head[12] = timeBynary[5];
		head[13] = timeBynary[6];
		head[14] = timeBynary[7];
		head[15] = (byte) bodyType;
		head[16] = (byte) resultType;
		head[17] = (byte) entytyType;
		head[18] = (byte) compleation;
		return head;
	}

	// Reverse byte array values
	public static byte[] reverse(byte[] array) {
		if (array == null) {
			// return null;
		}
		int i = 0;
		int j = array.length - 1;
		byte tmp;
		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
		return array;
	}
}
