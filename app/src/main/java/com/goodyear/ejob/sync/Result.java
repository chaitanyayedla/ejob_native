package com.goodyear.ejob.sync;

public class Result {

	public static int unknown = 0;
	public static int success = 1;
	public static int error = 2;
	public static int waitingUpload = 3;
	public static int waitingDownload = 4;

}
