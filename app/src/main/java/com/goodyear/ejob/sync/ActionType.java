package com.goodyear.ejob.sync;

public class ActionType {

	public static int unknown = 0;
	public static int DownloadUpload = 1;
	public static int Upload = 2;
	public static int Download = 3;
	public static int Authentication = 4;
	public static int Request = 5;

}
