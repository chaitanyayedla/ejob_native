package com.goodyear.ejob.sync;

public class BodyType {

	public static int database = 0;
	public static int KeyValuePair = 1;
	public static int TextOrJSON = 2;
	public static int bodySize;

	public int getBodySize() {
		return bodySize;
	}

	public void setBodySize(int bodySize) {
		this.bodySize = bodySize;
	}

}
