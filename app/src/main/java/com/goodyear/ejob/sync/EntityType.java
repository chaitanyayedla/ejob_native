package com.goodyear.ejob.sync;

public class EntityType {

	public static int tyre = 0;
	public static int Size = 1;
	public static int RIM = 3;

	public static int ASP = 4;
	public static int Damage = 5;
	public static int Job = 6;
	public static int JobItem = 7;
	public static int OdometerReason = 8;

	public static int RemovalReason = 9;
	public static int TyreBrand = 10;
	public static int TyreModel = 11;
	public static int VehicleSkeleton = 12;
	public static int Vendor = 13;

	public static int Contract = 14;
	public static int Vehicle = 15;
	public static int CasingRoute = 16;
	public static int ServiceMaterial = 17;
	public static int SignatureReason = 18;

	public static int ContractTyrePolicy = 19;
	public static int Translation = 20;
	public static int JobRemovalReason = 21;
	public static int TyreOperation = 22;
	public static int Technician = 23;

	public static int Database = 24;
	public static int PurchasingOrg = 25;
}
