/**
 * Class used for create a new eJob and Sync data with
 * middleware server , JSON Sync and parse values to other screens
 */

package com.goodyear.ejob;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.goodyear.ejob.adapter.JSONListAdapter;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.job.operation.helpers.EditJob;
import com.goodyear.ejob.sync.AuthSync;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.Contract;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.GYProgressDialog;
import com.goodyear.ejob.util.JsonParser;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.TyreConstants;
import com.goodyear.ejob.util.Utility;
import com.goodyear.ejob.util.Validation;
import com.goodyear.ejob.util.VehicleSkeletonInfo;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Hashtable;
import java.util.List;

/**
 * @author johnmiya.s
 * @version 1.0
 */
public class LaunchNewJob extends FragmentActivity implements InactivityHandler {

	private static final String TAG = LaunchNewJob.class.getSimpleName();
	/**
	 * Trace Info Variables
	 */
	private static final String TRACE_INFO = "Launch New Job";
	private String Trace_msg="";
	/** contract list from JSON Sync */
	public ArrayList<HashMap<String, String>> mListFromContractJSONSync = null;
	/** vendor name array */
	private ArrayList<String> mVendorArrayList;
	/** vendor list with ID to map vendor to id */
	private Hashtable<String, String> mVendorHashWithID;
	/** search data */
	private ArrayList<String> mSearchArrayList;
	/** vendor name string */
	private String mVendorName;
	/** spinner list */
	private String mSearchTypeSpinnerValue;
	/** contract string */
	private ArrayList<Contract> mContractInfo;
	/** sap info */
	private ArrayList<Tyre> mTyreSAPInfo;
	/** edit text for search */
	private EditText mEdtSearch;
	/** final String value for Sync */
	private String mFinalPassForSync;
	/** vendor sap code from JSON */
	private String mVendorSAPCode;
	/** list from JSON */
	private ListView mListFromSAPJSONSync;
	/** list values */
	private String[] mFrom;
	/** list labels */
	private int[] mTo;
	/** selected contract string */
	private Contract mContractSelected;
	/** list map from JSON */
	private ArrayList<HashMap<String, String>> mFillMaps;
	/** DataBase model */
	private DatabaseAdapter mDBHelper;
	/** licence no from Data base */
	private ArrayList<String> mLicenseNoArrayList;
	/** SAP Contract number no from Data base */
	private ArrayList<String> mSAPContractNoArrayList;
	/** contract from Translation DB */
	private String mContarctNameFromTranslationTab;
	/** licence plate from Translation DB */
	private String mLicencePlateFromTranslationTab;
	/** Vin from Translation DB */
	private String mVinFromTranslationTab;
	/** chasis from Translation DB */
	private String mChassisFromTranslationTab;
	/** licence from Translation DB */
	private String mLicenseFromTranslationTab;
	/** vehicle from Translation DB */
	private String mVehicleFromTranslationTab;
	/** chasis string from spinner selection */
	private String mSelectChassis;
	/** search string from search edit text */
	private String mSearchStr;
	/** no of records selected */
	private int mNumItemSelected;
	/** no of items selected */
	private int mNumItemSelectedVendor;
	/** progress dialog */
	public GYProgressDialog mGYProgressDialog;
	/** count for clear list */
	private int mMaxCount;
	/**
	 * UI Views
	 */
	/** search spinner */
	private Spinner mSearchTypeSpinner;
	/** vendor name spinner */
	private Spinner mSpn_VendorName;
	/** search result text view */
	private TextView mLbl_searchResult;
	/** job ref text view */
	private TextView mValueJobRefNo;
	/** contract Name text view */
	private TextView mContarctNameFromTranslationTabView;
	/** licence plate txt view */
	private TextView mLicencePlateFromTranslationTabView;
	/** vin txt view */
	private TextView mVinFromTranslationTabView;
	/** chasis txt view */
	private TextView mChassisFromTranslationTabView;
	/** search image view */
	private ImageButton mSearch;
	/** loading image view */
	private ImageView mLoadingImage;
	/** regular job radio */
	private RadioButton mRegularJobRadio;
	/** breack down job radio */
	private RadioButton mBreakDownJobRadio;
	private String mAlreadyAddedWarning;
	private String mYesLabel;
	private String mNoLabel;
	private String mOkLabel;
	private static AuthSyncAsync mAuthAsync;
	private static GetSAPSyncAsync mGetSAPSyncAsync;
	private String mNetworkErrorLabel;
	public static String strDataDownloadingFailed;

    private LogoutHandler mLogoutHandler;

    private String mLicenceSelectedVehicle;
    private String mSAPCodeSelectedVehicle;

	/*Bug665, JIRA EJOB-42:Strings for blacklisted vehicle message*/
	private String mBlacklistedVehicleMessage;
	private String mBlacklistedVehicleLicence;
	private String mBlacklistedVehicleSapCode;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.launch_new_ejob);
		LogUtil.TraceInfo(TRACE_INFO, "none", TRACE_INFO, false, false, true);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		openDB();
		mAlreadyAddedWarning = mDBHelper
				.getLabel(Constants.LABEL_ALREADY_ADDED);
		mYesLabel = mDBHelper.getLabel(Constants.YES_LABEL);
		mOkLabel = mDBHelper.getLabel(Constants.LABEL_OK);
		mNoLabel = mDBHelper.getLabel(Constants.NO_LABEL);
		mBlacklistedVehicleMessage = mDBHelper.getLabel(Constants.BLACKLISTED_VEHICLE_MESSAGE);
		mBlacklistedVehicleLicence = mDBHelper.getLabel(Constants.BLACKLISTED_VEHICLE_LICENCE);
		mBlacklistedVehicleSapCode = mDBHelper.getLabel(Constants.BLACKLISTED_VEHICLE_SAP_VEHICLE_CODE);

		setTitle(mDBHelper.getLabel(Constants.LAUNCH_NEW_JOB_TITLE));
		strDataDownloadingFailed = mDBHelper
				.getLabel(Constants.DataDownloadingFailed);

		screenAllLabelsInitialization();
		mNumItemSelected = 0;
		mNumItemSelectedVendor = 0;
		mMaxCount = 1;
		// handling orientation
		if (savedInstanceState != null) {
			Log.v("INSIDE", "onSaveInstanceState!=null");
			mSearchStr = savedInstanceState.getString("edtsearchstr");
			Log.v("GET edt_search_str", "_" + mSearchStr);
			if (savedInstanceState.getInt("searchTypeSelectedPos") > 0) {
				mMaxCount = 2;
			}
		} else {
			TyreConstants.getBundle_ListItems().clear();
		}
		// check edit field validation
		if (mSearchStr != null) {
			mEdtSearch.setText(mSearchStr);
		}
		Log.v("LaunchNewJOB", "Going to get the JobRefId");
		getJOBRefNumber();
		loadVendorName();
		searchBySAP();

		// figure out how many cherecters entered by user dynamically fetch the
		// count
		mEdtSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable arg0) {
				mSearchStr = mEdtSearch.getText().toString();
				enableSubmitIfReady();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
		});
		// search click view
		mSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				clearList();
				getVendorSAPID();
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(mEdtSearch.getWindowToken(), 0);
				// Bug:: 516 vendoeName fetched from HashTable and condition for
				// ',' check is removed
				/*
				 * if (mVendorName.contains(",")) { mVendorName =
				 * mVendorName.substring(0, mVendorName.indexOf(",")); }
				 */
				if (!mRegularJobRadio.isChecked()
						&& !mBreakDownJobRadio.isChecked()) {
					getWindow()
							.setSoftInputMode(
									WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
					CommonUtils.notify(mDBHelper.getLabel(15).toString(),
							LaunchNewJob.this);
				} else if (mVendorName.toString().equalsIgnoreCase(
						mDBHelper.getLabel(26).toString())) {
					CommonUtils.notify(mDBHelper.getLabel(26).toString(),
							LaunchNewJob.this);
				} else if (mEdtSearch.getText().toString().equalsIgnoreCase("")
						|| mEdtSearch.getText().toString()
								.equalsIgnoreCase(" ")) {
					CommonUtils.notify(mDBHelper.getLabel(18).toString(),
							LaunchNewJob.this);
				} else {


					if (Validation.isNetworkAvailable(getApplicationContext())) {
						try {
							mAuthAsync = new AuthSyncAsync(LaunchNewJob.this);
							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
								mAuthAsync
										.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
							} else {
								mAuthAsync.execute("");
							}
						} catch (Exception e) {
							Log.e("JSON SYNC", "JSON SYNC FAIL:" + e);
						}

					} else {
						getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
						Toast toast = Toast.makeText(getApplicationContext(),
								mNetworkErrorLabel, Toast.LENGTH_SHORT);

						LogUtil.TraceInfo(TRACE_INFO, "On Search - Problem :", mNetworkErrorLabel,false,true,false);

						toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
						toast.show();
						clearList();
					}

					//Vehicle list Trace Information based on user preference
					try {
						Trace_msg = "\n\t\t";
						if(mRegularJobRadio.isChecked())
						{
							Trace_msg = Trace_msg + "Job Type : Regular eJob, ";
						}

						if(mBreakDownJobRadio.isChecked())
						{
							Trace_msg = Trace_msg + "Job Type : BreakDown eJob, ";
						}

						Trace_msg = Trace_msg + "Vendor Name : " + mSpn_VendorName.getSelectedItem().toString()+", ";
						Trace_msg = Trace_msg + "Search By: " + mSearchTypeSpinner.getSelectedItem().toString()+", ";
						Trace_msg = Trace_msg + "Search Key : " + mEdtSearch.getText().toString();
						LogUtil.TraceInfo(TRACE_INFO, "On Search", Trace_msg,false,true,false);
					}
					catch (Exception e)
					{
						LogUtil.TraceInfo(TRACE_INFO, "On Search", e.getMessage(),false,true,false);
					}

				}
				mSearchStr = mEdtSearch.getText().toString();
			}
		});
		mListFromSAPJSONSync = (ListView) findViewById(R.id.listview);
		// handling orientation changes
		if (savedInstanceState != null) {

			mFillMaps = new ArrayList<HashMap<String, String>>();
			mTo = new int[] { R.id.item1, R.id.item2, R.id.item3, R.id.item4 };
			mFrom = new String[] { "Contract Name", "License Plate", "Vin",
					"Chassis" };
			if (!TyreConstants.getBundle_ListItems().isEmpty()) {
				mFillMaps = TyreConstants.getBundle_ListItems();
				setListView();
			}
			try {
				mContractInfo = JsonParser.getContracts(Constants._SAPJSONVAl);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			if (mAuthAsync != null
					&& mAuthAsync.getStatus() != AsyncTask.Status.FINISHED) {
				mAuthAsync.attach(this);
			}
			if (mGetSAPSyncAsync != null
					&& mGetSAPSyncAsync.getStatus() != AsyncTask.Status.FINISHED) {
				mGetSAPSyncAsync.attach(this);
			}

			mListFromSAPJSONSync
					.setOnItemClickListener(new SearchedContractListListner());

		}
		restoreDialogState(savedInstanceState);
	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	@Override
	protected void onStart() {
		super.onStart();
		LogoutHandler.setCurrentActivity(this);
		// mLogoutHandler = new LogoutHandler(getBaseContext(),
		// LaunchNewJob.this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		// mLogoutHandler.unregisterReceiver();
	}

	/**
	 * show progress dialog
	 */
	private void showProgress() {
		try {
			dismissProgress();
			mGYProgressDialog = new GYProgressDialog(LaunchNewJob.this);
			mGYProgressDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Dismiss progress dialog
	 */
	private void dismissProgress() {
		try {
			if (mGYProgressDialog != null && mGYProgressDialog.isShowing()) {
				mGYProgressDialog.dismiss();
				mGYProgressDialog = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Clear search list
	 */
	protected void clearList() {
		try {
			if (mFillMaps != null) {
				mFillMaps.clear();
				ListAdapter adapter = mListFromSAPJSONSync.getAdapter();
				if (adapter instanceof JSONListAdapter) {
					((JSONListAdapter) adapter).notifyDataSetChanged();
				}
				mListFromSAPJSONSync.setAdapter(null);
			}
			if (TyreConstants.getBundle_ListItems() != null) {
				TyreConstants.getBundle_ListItems().clear();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Orientation changes_ list view set
	public void setListView() {
		JSONListAdapter adapter = new JSONListAdapter(this, mFillMaps,
				R.layout.grid_item, mFrom, mTo);
		mListFromSAPJSONSync.setAdapter(adapter);
	}

	/**
	 * initialize all screen lables from xml and values assigned from DB
	 */
	private void screenAllLabelsInitialization() {
		mContarctNameFromTranslationTab = mDBHelper.getLabel(27);
		mLicencePlateFromTranslationTab = mDBHelper.getLabel(294);
		mVinFromTranslationTab = mDBHelper.getLabel(10);
		mChassisFromTranslationTab = mDBHelper.getLabel(11);
		mLicenseFromTranslationTab = mDBHelper.getLabel(28);
		mVehicleFromTranslationTab = mDBHelper.getLabel(29);
		mSelectChassis = mDBHelper.getLabel(11);
		mContarctNameFromTranslationTabView = (TextView) findViewById(R.id.item1);
		mLicencePlateFromTranslationTabView = (TextView) findViewById(R.id.item2);
		mVinFromTranslationTabView = (TextView) findViewById(R.id.item3);
		mChassisFromTranslationTabView = (TextView) findViewById(R.id.item4);
		mContarctNameFromTranslationTabView
				.setText(mContarctNameFromTranslationTab);
		mLicencePlateFromTranslationTabView
				.setText(mLicencePlateFromTranslationTab);
		mVinFromTranslationTabView.setText(mVinFromTranslationTab);
		mChassisFromTranslationTabView.setText(mChassisFromTranslationTab);
		mSearch = (ImageButton) findViewById(R.id.btnSearch);
		mSpn_VendorName = (Spinner) findViewById(R.id.spinner_brand);
		mSearchTypeSpinner = (Spinner) findViewById(R.id.spinner_searchBy);
		mEdtSearch = (EditText) findViewById(R.id.edt_search);
		mEdtSearch.setHint(mDBHelper.getLabel(Constants.LABEL_SEARCH));
		mRegularJobRadio = (RadioButton) findViewById(R.id.value_regularJob);
		mRegularJobRadio.setText(mDBHelper.getLabel(7));
		mBreakDownJobRadio = (RadioButton) findViewById(R.id.value_breakdownJob);
		mBreakDownJobRadio.setText(mDBHelper.getLabel(8));
		mValueJobRefNo = (TextView) findViewById(R.id.launchjob_jobRefNo);
		mLbl_searchResult = (TextView) findViewById(R.id.lbl_searchResult);
		mLbl_searchResult.setText(mDBHelper.getLabel(28));
		mLoadingImage = (ImageView) findViewById(R.id.loading_img);
		mLoadingImage.setVisibility(ImageView.INVISIBLE);
		mNetworkErrorLabel = mDBHelper.getLabel(Constants.NO_NETWORK_TRY_AGAIN);
		Constants.ERROR_NETWORK_SYNC_ERROR = mDBHelper
				.getLabel(Constants.CHECK_INTERNET_CONNECTION_LABEL);
		Constants.ERROR_SERVER_DOWN_ERROR = mDBHelper
				.getLabel(Constants.CANNOT_CONNECT_LABEL);
	}

	// submit button enable/disable
	public void enableSubmitIfReady() {

		//Search key enabled for single character
		//Fix for vehicle search with single char licence number
		boolean isReady = mEdtSearch.getText().toString().length() >= 1;

		if (isReady) {
			mSearch.setVisibility(View.VISIBLE);
		} else {
			mSearch.setVisibility(View.INVISIBLE);
		}
	}

	// get Vendor SAP ID
	private void getVendorSAPID() {
		Cursor mCursor = null;
		try {
			mCursor = mDBHelper.getvendorID(Constants.VENDER_NAME);
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				mVendorSAPCode = mCursor
						.getString(mCursor.getColumnIndex("ID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	// get selected SAP from JSON
	private void searchBySAP() {
		mSearchArrayList = new ArrayList<String>();
		String[] typesOfSearch = { mLicenseFromTranslationTab,
				mVehicleFromTranslationTab, mSelectChassis };
		for (int i = 0; i < typesOfSearch.length; i++) {
			mSearchArrayList.add(typesOfSearch[i]);
		}
		ArrayAdapter<String> vendorAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, typesOfSearch);
		vendorAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSearchTypeSpinner.setAdapter(vendorAdapter);
		mSearchTypeSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						// hideKeyboard(v);
						mNumItemSelected++;
						Log.d("num_item_selected", "_" + mNumItemSelected);
						if (mNumItemSelected > mMaxCount) {// sundeep
							mSearchStr = null;
							clearList();
						}
						Log.v("INSIDE", "onItemSelected");
						if (mSearchStr != null) {
							mEdtSearch.setText(mSearchStr);
						} else {
							mEdtSearch.setText("");
						}
						mSearchTypeSpinnerValue = mSearchTypeSpinner
								.getSelectedItem().toString();
						mLbl_searchResult.setText(mSearchTypeSpinnerValue);
						getFinalStringsToPass(mSearchTypeSpinnerValue);
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
					}
				});
	}

	// get vendor name
	public void loadVendorName() {
		Cursor vendorCursor = null;
		try {
			// load Vendor Name
			vendorCursor = mDBHelper.getVendorNameForEJOB();
			mVendorArrayList = new ArrayList<String>();
			mVendorHashWithID = new Hashtable<>();
			// vendorArrayList.add(check.getLabel(26).toString());// not for one
			// vendor
			if (CursorUtils.isValidCursor(vendorCursor)) {
				vendorCursor.moveToFirst();
				while (!vendorCursor.isAfterLast()) {
					String vendor_city = vendorCursor.getString(vendorCursor
							.getColumnIndex("VendorName"))
							+ ", "
							+ vendorCursor.getString(vendorCursor
									.getColumnIndex("City"));
					mVendorArrayList.add(vendor_city);
					// put vendor_city as key and Id as value to map later
					// when user selects the drop down
					mVendorHashWithID.put(vendor_city, vendorCursor
							.getString(vendorCursor
									.getColumnIndex("VendorName")));
					vendorCursor.moveToNext();
					Collections.sort(mVendorArrayList);
				}
				CursorUtils.closeCursor(vendorCursor);
			}
			if (mVendorArrayList.size() > 1) {
				mVendorArrayList.add(0, mDBHelper.getLabel(26).toString());
				mSearchTypeSpinner.setEnabled(false);
				mEdtSearch.setEnabled(false);
			}
			// Creating adapter for spinner
			ArrayAdapter<String> vendorAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mVendorArrayList);
			vendorAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			// attaching data adapter to spinner
			mSpn_VendorName.setAdapter(vendorAdapter);
			if (mVendorArrayList != null && mVendorArrayList.size() == 0) {
			} else if (mVendorArrayList.size() < 2) {
				mSpn_VendorName.setEnabled(false);
				mSearchTypeSpinner.setEnabled(true);
				mEdtSearch.setEnabled(true);
				mVendorName = mVendorArrayList.get(0);
				// Bug:: 516 vendoeName fetched from HashTable and condition for
				// ',' check is removed
				mVendorName = mVendorHashWithID.get(mVendorName);
				/*
				 * if (mVendorName.contains(",")) { mVendorName =
				 * mVendorName.substring(0, mVendorName.indexOf(",")); }
				 */
				Constants.VENDER_NAME = mVendorName;
				mDBHelper.getvendorID(Constants.VENDER_NAME);
				Cursor mCursor = mDBHelper.getVendorinfo(mVendorName);
				if (CursorUtils.isValidCursor(mCursor)) {
					Constants.VENDORCOUNTRY_ID = mCursor.getString(mCursor
							.getColumnIndex("CountryCode"));
					Constants.VENDORSAPCODE = mCursor.getString(mCursor
							.getColumnIndex("ID"));
					//fixed 139(reopen) the variable was wrongly assigned
					Constants.SAP_VENDORCODE_ID = Constants.VENDORSAPCODE;
					CursorUtils.closeCursor(mCursor);
				}
			} else {
				mSpn_VendorName
						.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView adapter,
									View v, int i, long lng) {
								// hideKeyboard(v);
								mVendorName = mSpn_VendorName.getSelectedItem()
										.toString();
								// if (mVendorName.contains(",")) {
								// mVendorName = mVendorName.substring(0,
								// mVendorName.indexOf(","));
								// }

								Constants.VENDER_NAME = mVendorHashWithID
										.get(mVendorName);
								if (mSpn_VendorName.getSelectedItemPosition() > 0)
									mVendorName = Constants.VENDER_NAME;
								if (mVendorName.equalsIgnoreCase(mDBHelper
										.getLabel(26).toString())) {
									mSearchTypeSpinner.setEnabled(false);
									mEdtSearch.setText("");
									mSearchStr = "";
									mEdtSearch.setEnabled(false);
									clearList();
								} else {
									mSearchTypeSpinner.setEnabled(true);
									mEdtSearch.setEnabled(true);
								}
								mNumItemSelectedVendor++;
								Log.d("num_item_selected_vendor", "_"
										+ mNumItemSelectedVendor);
								if (mNumItemSelectedVendor > 2) {// sundeep
									mSearchStr = null;
									mEdtSearch.setText("");
									clearList();
								}
								Cursor mCursor = mDBHelper
										.getVendorinfo(mVendorName);

								try {
									if (CursorUtils.isValidCursor(mCursor)) {
										Constants.VENDORCOUNTRY_ID = mCursor.getString(mCursor
												.getColumnIndex("CountryCode"));
										Constants.VENDORSAPCODE = mCursor
												.getString(mCursor
														.getColumnIndex("ID"));
										//fixed 139(reopen) the variable was wrongly assigned
										Constants.SAP_VENDORCODE_ID = Constants.VENDORSAPCODE;
									}
								} catch (Exception e) {
									e.printStackTrace();
								} finally {
									CursorUtils.closeCursor(mCursor);
								}
							}

							@Override
							public void onNothingSelected(AdapterView arg0) {
							}
						});
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(vendorCursor);
		}
	}

	// get final value based on user selection to pass JSON for SYNC
	private void getFinalStringsToPass(String finalString) {

		mFinalPassForSync = finalString;

		if (mFinalPassForSync.equalsIgnoreCase(mLicenseFromTranslationTab
				.toString())) {
			mFinalPassForSync = "LPAndRegNoAndTrailerNo";
		} else if (mFinalPassForSync
				.equalsIgnoreCase(mVehicleFromTranslationTab.toString())) {
			mFinalPassForSync = "VehicleIdentificationNo";
		} else if (mFinalPassForSync
				.equalsIgnoreCase(mSelectChassis.toString())) {
			mFinalPassForSync = "ChassisNo";
		} else {
			mFinalPassForSync = "LPAndRegNoAndTrailerNo";
		}
	}

	// Get JSON Sync values and pass to fields
	public void loadJSON(String jsonContract) {

		if (jsonContract.equalsIgnoreCase("null[]")
				|| jsonContract.equalsIgnoreCase(" ")
				|| jsonContract.equalsIgnoreCase("[]")
				|| jsonContract.equalsIgnoreCase("null")
				|| jsonContract.equalsIgnoreCase("")) {

			Toast toast = Toast.makeText(getApplicationContext(), mDBHelper
					.getLabel(21).toString(), Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();
			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			clearList();
		} else {
			try {
				Constants._SAPJSONVAl = jsonContract;
				mListFromSAPJSONSync = (ListView) findViewById(R.id.listview);
				mContractInfo = JsonParser.getContracts(jsonContract);
				mFrom = new String[] { "Contract Name", "License Plate", "Vin",
						"Chassis" };
				mTo = new int[] { R.id.item1, R.id.item2, R.id.item3,
						R.id.item4 };
				mFillMaps = new ArrayList<HashMap<String, String>>();
				if(mContractInfo!=null && mContractInfo.size()>0)
				{
					LogUtil.TraceInfo(TRACE_INFO, "On Search : Contract Info", mContractInfo.toString(),true,true,true);
				}
				for (Contract tire : mContractInfo) {
					HashMap<String, String> map = new HashMap<String, String>();
					byte[] data = Base64.decode(tire.getCustomerName(),
							Base64.DEFAULT);
					String text = new String(data, "UTF-8");
					// System.out.println(text);
					map.put("Contract Name", "" + text);
					map.put("License Plate", tire.getLicenseNum());
					map.put("Vin", tire.getVin());
					map.put("Chassis", tire.getChassisNum());
					mFillMaps.add(map);
				}
				setListView();
				TyreConstants.setBundle_ListItems(mFillMaps);
				mListFromSAPJSONSync
						.setOnItemClickListener(new SearchedContractListListner());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private class SearchedContractListListner implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			mContractSelected = mContractInfo.get(position);

			int pos=position+1;
			//selected vehicle Trace Information
			try {
				Trace_msg = mContractSelected.toString();
                mSAPCodeSelectedVehicle = mContractSelected.getVehicleSapCode();
                mLicenceSelectedVehicle = mContractSelected.getLicenseNum();
				LogUtil.TraceInfo(TRACE_INFO,"Selected Vehicle Details",Trace_msg,false,true,false);
			}
			catch(Exception e)
			{
				LogUtil.TraceInfo(TRACE_INFO,"Selected Vehicle Details - Exception",e.getMessage(),false,true,false);
			}

			//CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
			//Storing Last Recorded Odometer value to constant
			if(!TextUtils.isEmpty(mContractSelected.getVehicleODO())) {
				Constants.LastRecordedOdometerValue = Long.parseLong(mContractSelected.getVehicleODO());
			}
			else
			{
				Constants.LastRecordedOdometerValue = 0L;
			}

			Constants.SAP_CUSTOMER_ID_JOB = mContractSelected.getContractId();
			Constants.contract = mContractSelected;// now all
			// contract
			// values are in
			// Constants.contract
			Log.i(TRACE_INFO,
					"vehicle config is: "
							+ mContractSelected.getVehichleConfiguration());
			Constants.vehicleConfiguration = mContractSelected
					.getVehichleConfiguration();
			byte[] data = Base64.decode(mContractSelected.getCustomerName(),
					Base64.DEFAULT);
			String text = "";
			try {
				text = new String(data, "UTF-8");
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
			Constants.ACCOUNTNAME = text;
			EditJob.sJobItemToDelete = String
					.valueOf(Constants.JOBREFNUMBERVALUE);
			Constants.VEHID = mContractSelected.getLicenseNum();
			Constants.FLEETNUM = mContractSelected.getVin();
			Constants.DIFFERENT_SIZE_ALLOWED = Boolean
					.parseBoolean(mContractSelected
							.getIsDIffSizeAllowedForVehicle());
			try {
				if (mRegularJobRadio.isChecked()) {

					Constants.JOBTYPEREGULAR = true;
				} else if (mBreakDownJobRadio.isChecked()) {

					Constants.JOBTYPEREGULAR = false;
				}
				if (mRegularJobRadio.isChecked()
						|| mBreakDownJobRadio.isChecked()) {

					if (Validation.isNetworkAvailable(getApplicationContext())) {
						try {
							// Constants.CAN_GET_LOCATION = false;
							mGetSAPSyncAsync = new GetSAPSyncAsync(
									LaunchNewJob.this);
							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
								mGetSAPSyncAsync
										.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
							} else {
								mGetSAPSyncAsync.execute("");
							}
						} catch (Exception e) {
							Log.e("JSON SYNC", "JSON SYNC FAIL" + e);
						}
					} else {

						Toast toast = Toast.makeText(getApplicationContext(),
								mNetworkErrorLabel, Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
						toast.show();

					}

				} else {
					CommonUtils.notify(mDBHelper.getLabel(15).toString(),
							getApplicationContext());
					/*
					 * Toast.makeText(getApplicationContext(),
					 * mDBHelper.getLabel(15).toString(),
					 * Toast.LENGTH_SHORT).show(); // no 15
					 */}
			} catch (Exception e) {
				Log.e("JSON SYNC", "JSON SYNC FAIL" + e);
			}
		}

	}

	// onSaveInstanceState
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("edtsearchstr", mSearchStr);
		outState.putBoolean("loading",
				(mGYProgressDialog != null && mGYProgressDialog.isShowing()));
		outState.putInt("searchTypeSelectedPos",
				mSearchTypeSpinner.getSelectedItemPosition());

		super.onSaveInstanceState(outState);
		mFillMaps = TyreConstants.getBundle_ListItems();
		saveDialogState(outState);
	}

	/**
	 * Get the JOBRefNumber from DB
	 */
	public void getJOBRefNumber() {
		Cursor jobRefCursor = null;
		try {
			jobRefCursor = mDBHelper.getJobReferenceNumber();
			Log.v("LaunchNewJOB", "jobRefCursor:: " + jobRefCursor);
			if (CursorUtils.isValidCursor(jobRefCursor)) {
				int mJobRefNoFromAccTable = jobRefCursor.getInt(jobRefCursor
						.getColumnIndex("jobRefNumber"));
				if (mJobRefNoFromAccTable > 0) {
					++mJobRefNoFromAccTable;
					mValueJobRefNo.setText("#"
							+ String.valueOf(mJobRefNoFromAccTable));
					Constants.JOBREFNUMBERVALUE = mJobRefNoFromAccTable;
					LogUtil.e("LaunchNewJob", "Constants.JOBREFNUMBER::"
							+ Constants.JOBREFNUMBER);
					Constants.JOBREFNUMBER = String
							.valueOf(Constants.JOBREFNUMBERVALUE);
					//FIX for Partial Inspection Job Reference number
					Constants.JobRefNumberForPartialInspectionCheck=Constants.JOBREFNUMBERVALUE;
					LogUtil.e("LaunchNewJob", "Constants.JOBREFNUMBER:1:"
							+ Constants.JOBREFNUMBER);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			LogUtil.e("LaunchNewJob.getJOBRefNumber", e.getMessage());
		} finally {
			CursorUtils.closeCursor(jobRefCursor);
		}
	}

	/**
	 * Get the SAP values from JSON
	 * 
	 * @param jsonSAPVehicleVal
	 *            returns JSON string from server
	 */
	public void getSAPValues(String jsonSAPVehicleVal) {
		try {
			String tireJson = jsonSAPVehicleVal;
			Constants.tireJson = tireJson;
			mTyreSAPInfo = JsonParser.getTyreInfo(tireJson);
			List<HashMap<String, String>> fillSAPMaps = new ArrayList<HashMap<String, String>>();
			for (Tyre tire : mTyreSAPInfo) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("Contract Name", "" + tire.getBrandName());
				map.put("License Plate", tire.getDesign());
				map.put("Vin", tire.getDesignDetails());
				map.put("Chassis", tire.getNsk());
				fillSAPMaps.add(map);
				Constants.SAPCONTRACTNUMBER = tire.getSapContractNumberID();
				Constants.SAP_CONTRACTNUMBER_ID = tire.getSapContractNumberID();
				//fixed 139(reopen) the variable was wrongly assigned
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// get GemotryCheckCorrection from JSON
	public boolean getGemotryCheckCorrection() {
		boolean isGemotryCheckCorrection = false;
		int mGeometryCheck = 0;
		int mGeometryCorrection = 0;
		if (Constants.SAP_CONTRACTNUMBER_ID != null
				&& !Constants.SAP_CONTRACTNUMBER_ID.equals("")) {
			DatabaseAdapter dbHelper = new DatabaseAdapter(this);
			dbHelper.createDatabase();
			dbHelper.open();
			Cursor mCursor;
			mCursor = dbHelper.getContaractTyrePolicy(Integer
					.parseInt(Constants.SAP_CONTRACTNUMBER_ID));
			if (CursorUtils.isValidCursor(mCursor)) {
				if (mCursor.moveToNext()) {
					mGeometryCheck = mCursor.getInt(0);
					mGeometryCorrection = mCursor.getInt(1);
				}
				if (mGeometryCheck == 0 && mGeometryCorrection == 0) {
					isGemotryCheckCorrection = true;
				} else {
					isGemotryCheckCorrection = false;
				}
				CursorUtils.closeCursor(mCursor);
			}
			if (dbHelper != null) {
				dbHelper.close();
			}
		}
		return isGemotryCheckCorrection;
	}

	// contract JSON Sync
	public class AuthSyncAsync extends AsyncTask<String, Void, String> {
		private WeakReference<LaunchNewJob> activityReference;

		public AuthSyncAsync(LaunchNewJob launchNewJob) {
			attach(launchNewJob);
		}

		private void attach(LaunchNewJob activity) {
			activityReference = new WeakReference<>(activity);
		}

		private String mConVal = "";

		@Override
		protected void onPreExecute() {
			LaunchNewJob activity = activityReference.get();
			if (activity == null) {
				return;
			}
			activity.mSearch.setVisibility(View.INVISIBLE);

			activity.mSearch.setEnabled(false);
			activity.mSpn_VendorName.setEnabled(false);
			activity.mSearchTypeSpinner.setEnabled(false);
			activity.mEdtSearch.setEnabled(false);
			activity.mRegularJobRadio.setEnabled(false);
			activity.mBreakDownJobRadio.setEnabled(false);
			activity.showProgress();
			mConVal = "";
		}

		@Override
		protected String doInBackground(String... arg0) {
			LaunchNewJob activity = activityReference.get();
			if (activity == null) {
				return null;
			}
			try {
				String[] search = { "SearchStr",
						activity.mEdtSearch.getText().toString(), "SearchType",
						activity.mFinalPassForSync, "VendorSapCode",
						activity.mVendorSAPCode };
				mConVal = AuthSync.getContaractVehicleList(search);
			} catch (Exception e) {
				e.printStackTrace();
				if (!Validation.isNetworkAvailable(activity)
						|| Utility.isProxyConnected(e, activity)) {
					return "7";
				}
				if (e instanceof IOException) {
					return "8";
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			LaunchNewJob activity = activityReference.get();
			if (activity == null) {
				return;
			}
			try {
				if (result != null && result.equals("7")) {
					clearMap(activity, activity.mNetworkErrorLabel);
					return;
				}
				if (result != null && result.equals("8")) {
					clearMap(activity, Constants.ERROR_SERVER_DOWN_ERROR);
					return;
				}
				activity.loadJSON(mConVal);
			} catch (Exception e) {
				clearMap(activity, Constants.ERROR_SERVER_DOWN_ERROR);
			} finally {
				activity.mSearch.setVisibility(View.VISIBLE);
				activity.mSearch.setEnabled(true);
				if (mVendorArrayList.size() < 2)
					activity.mSpn_VendorName.setEnabled(false);
				else
					activity.mSpn_VendorName.setEnabled(true);
				activity.mSearchTypeSpinner.setEnabled(true);
				activity.mEdtSearch.setEnabled(true);
				activity.mRegularJobRadio.setEnabled(true);
				activity.mBreakDownJobRadio.setEnabled(true);
				activity.dismissProgress();
			}
		}

		private void clearMap(LaunchNewJob activity, String error) {
			if (activity.mFillMaps != null) {
				activity.mFillMaps.clear();
				activity.mListFromSAPJSONSync.setAdapter(null);
			}
			CommonUtils.notify(error, activity.getApplicationContext());
		}
	}

	// SAPContract details JSON Sync
	public class GetSAPSyncAsync extends AsyncTask<String, Void, String> {
		private WeakReference<LaunchNewJob> activityReference;

		public GetSAPSyncAsync(LaunchNewJob launchNewJob) {
			attach(launchNewJob);
		}

		private void attach(LaunchNewJob activity) {
			activityReference = new WeakReference<>(activity);
		}

		@Override
		protected void onPreExecute() {
			LaunchNewJob activity = activityReference.get();
			if (activity == null) {
				return;
			}
			activity.showProgress();
			activity.mListFromSAPJSONSync.setEnabled(false);
			activity.mListFromSAPJSONSync.setClickable(false);
			activity.mSearch.setEnabled(false);
			activity.mSpn_VendorName.setEnabled(false);
			activity.mSearchTypeSpinner.setEnabled(false);
			activity.mEdtSearch.setEnabled(false);
			activity.mRegularJobRadio.setEnabled(false);
			activity.mBreakDownJobRadio.setEnabled(false);
		}

		@Override
		protected String doInBackground(String... arg0) {
			LaunchNewJob activity = activityReference.get();
			if (activity == null) {
				return null;
			}
			try {
				String[] sap = { "SapCode",
						activity.mContractSelected.getVehicleSapCode() };
				return AuthSync.getAllVehicleTiresBySapCode(sap);
			} catch (Exception e) {
				e.printStackTrace();
				if (!Validation.isNetworkAvailable(activity)
						|| Utility.isProxyConnected(e, activity)) {
					return "7";
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			LaunchNewJob activity = activityReference.get();
			if (activity == null) {
				return;
			}
			try {
				if (result != null && result.equals("7")) {
					CommonUtils.notify(activity.mNetworkErrorLabel,
							activity.getApplicationContext());
					return;
				}
				activity.getSAPValues(result);
				// LogUtil.e("LauncHNewJob", "activity.mTyreSAPInfo::: "
				// + activity.mTyreSAPInfo);
				// LogUtil.i("*****Launch New Job ******", "Tire Count :: "
				// + activity.mTyreSAPInfo.size());
				// Bug Fix : 224, If Tyre sizes is 0 stop the
				// EjobFormActionActivity
				if ((null != activity.mTyreSAPInfo)
						&& (activity.mTyreSAPInfo.size() > 0)) {
					activity.getJobStatusForLaunch();
					activity.dismissProgress();
					if (activity.checkIsJobAlreadyAdded()) {
						activity.createDialogAlertJobAlredyAdded();
					} else {
                        if (isBlackListedVehicle()) {
							CommonUtils.notifyLong(mBlacklistedVehicleMessage + "\n" + mBlacklistedVehicleLicence + ":" +mLicenceSelectedVehicle + "\n" +
											mBlacklistedVehicleSapCode + ":" + mSAPCodeSelectedVehicle,
									getApplicationContext());
                            return;
                        } else {
                            activity.moveToEjobFormActionActivity();
                        }
					}
				} else {
					CommonUtils.notify(strDataDownloadingFailed, activity);
				}
			} catch (Exception e) {
				e.printStackTrace();
				Log.e("EjobActivity", "cannot open EjobFormActivity class" + e);
			} finally {
				activity.mListFromSAPJSONSync.setEnabled(true);
				activity.mListFromSAPJSONSync.setClickable(true);
				activity.mSearch.setEnabled(true);
				if (mVendorArrayList.size() < 2)
					activity.mSpn_VendorName.setEnabled(false);
				else
					activity.mSpn_VendorName.setEnabled(true);
				activity.mSearchTypeSpinner.setEnabled(true);
				activity.mEdtSearch.setEnabled(true);
				activity.mRegularJobRadio.setEnabled(true);
				activity.mBreakDownJobRadio.setEnabled(true);
				activity.dismissProgress();
			}

		}
	}

	private void moveToEjobFormActionActivity() {
		mListFromSAPJSONSync.setEnabled(false);
		mListFromSAPJSONSync.setClickable(false);

		Constants.JOBTYPEREGULAR = mRegularJobRadio.isChecked();
		Constants.JOBTYPEBREAKDOWN = mBreakDownJobRadio.isChecked();
		Constants.GEMOTRY_CHECK_CORRECTION = getGemotryCheckCorrection();

		Intent startJob = new Intent(LaunchNewJob.this,
				EjobFormActionActivity.class);
		startActivity(startJob);
	}

	private void createDialogAlertJobAlredyAdded() {
		LayoutInflater li = LayoutInflater.from(LaunchNewJob.this);
		View promptsView = li
				.inflate(R.layout.dialog_logout_confirmation, null);
		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
				LaunchNewJob.this);
		alertDialogBuilder.setView(promptsView);
		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
		infoTv.setText(mAlreadyAddedWarning);
		alertDialogBuilder.setCancelable(false).setPositiveButton(mOkLabel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						dialog.dismiss();
                        if (isBlackListedVehicle()) {
                            CommonUtils.notifyLong(mBlacklistedVehicleMessage + "\n" + mBlacklistedVehicleLicence + ":" +mLicenceSelectedVehicle + "\n" +
											mBlacklistedVehicleSapCode + ":" + mSAPCodeSelectedVehicle,
                                    getApplicationContext());
                            return;
                        } else {
                            moveToEjobFormActionActivity();
                        }
					}
				});

		mDuplicateJobAlertDialog = alertDialogBuilder.create();
		mDuplicateJobAlertDialog.show();
		mDuplicateJobAlertDialog.setCanceledOnTouchOutside(false);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (Constants.JOBREFNUMBER.toString().equalsIgnoreCase("-1")) {
			getJOBRefNumber();
		}
		// mValueJobRefNo.setText("#"
		// + Constants.JOBREFNUMBER);
		LogUtil.e("LaunchNewJob OnResume", "Constants.JOBREFNUMBER:1:"
				+ Constants.JOBREFNUMBER);
		openDB();
	}

	@Override
	protected void onPause() {
		super.onPause();
		LogUtil.e("LaunchNewJob OnPause", "Constants.JOBREFNUMBER:1:"
				+ Constants.JOBREFNUMBER);
	}

	private AlertDialog mBackAlertDialog;
	private AlertDialog mDuplicateJobAlertDialog;

	private void saveDialogState(Bundle state) {
		state.putBoolean("mBackAlertDialog",
				(mBackAlertDialog != null && mBackAlertDialog.isShowing()));
		state.putBoolean("mDuplicateAlertDialog",
				(null != mDuplicateJobAlertDialog && mDuplicateJobAlertDialog
						.isShowing()));
	}

	private void restoreDialogState(Bundle state) {
		if (state != null) {
			if (state.getBoolean("loading")) {
				showProgress();
			} else if (state.getBoolean("mBackAlertDialog")) {
				onBackPressed();
			} else if (state.getBoolean("mDuplicateAlertDialog")) {
				createDialogAlertJobAlredyAdded();
			}
		}

	}

	@Override
	protected void onDestroy() {
		try {
			dismissProgress();
			if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
				mBackAlertDialog.dismiss();
			}
			if (null != mDuplicateJobAlertDialog
					&& mDuplicateJobAlertDialog.isShowing()) {
				mDuplicateJobAlertDialog.dismiss();
			}
			closeDB();
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {

		LayoutInflater li = LayoutInflater.from(LaunchNewJob.this);
		View promptsView = li
				.inflate(R.layout.dialog_logout_confirmation, null);

		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
				LaunchNewJob.this);
		alertDialogBuilder.setView(promptsView);

		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		// Setting The Info
		TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
		infoTv.setText(mDBHelper.getLabel(241));

		alertDialogBuilder.setCancelable(false).setPositiveButton(
				mDBHelper.getLabel(Constants.YES_LABEL),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_INFO, "Back Press Dialog", "BT - OK", false, true, false);
						Constants.TM_NOT_ALLOWED = false;
						Intent startJob = new Intent(LaunchNewJob.this,
								EjobList.class);
						startActivity(startJob);
						// finish();
					}
				});

		alertDialogBuilder.setCancelable(false).setNegativeButton(
				mDBHelper.getLabel(Constants.NO_LABEL),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// if user select "No", just cancel this dialog and
						// continue
						// with app
						LogUtil.TraceInfo(TRACE_INFO, "Back Press Dialog", "BT - Cancel", false, true, false);
						dialog.cancel();
					}
				});
		mBackAlertDialog = alertDialogBuilder.create();
		mBackAlertDialog.show();
		mBackAlertDialog.setCanceledOnTouchOutside(false);
	}

	// Job Status
	public void getJobStatusForLaunch() {
		Cursor mCursor = null;
		DatabaseAdapter testAdapter = null;
		try {
			testAdapter = new DatabaseAdapter(this);
			testAdapter.open();
			mCursor = testAdapter.getJobStatus();
			mLicenseNoArrayList = new ArrayList<String>();
			mSAPContractNoArrayList = new ArrayList<String>();
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				while (!mCursor.isAfterLast()) {
					mLicenseNoArrayList.add(mCursor.getString(mCursor
							.getColumnIndex("LicenseNumber"))); // add the item
					mSAPContractNoArrayList.add(mCursor.getString(mCursor
							.getColumnIndex("SAPContractNumber")));
					mCursor.moveToNext();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			if (testAdapter != null) {
				testAdapter.close();
			}
		}
	}

	public boolean checkIsJobAlreadyAdded() {
		try {
			// fixed internal issue 58
			// check if a job with same vehicle exists
			if (mLicenseNoArrayList != null && mLicenseNoArrayList.size() > 0
					&& mSAPContractNoArrayList != null
					&& mSAPContractNoArrayList.size() > 0) {
				for (int itrator = 0; itrator < mLicenseNoArrayList.size(); itrator++) {
					if (mLicenseNoArrayList.get(itrator)
							.equals(Constants.VEHID)
							&& mSAPContractNoArrayList.get(itrator).equals(
									Constants.SAP_CUSTOMER_ID_JOB)) {
						return true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// checking for network connection, and display alert if no network
	public void showAlertWhenNoNetwork() {
		final EjobAlertDialog helpBuilder = new EjobAlertDialog(this);
		LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li
				.inflate(R.layout.dialog_simple_one_textview, null);
		helpBuilder.setView(promptsView);

		// set user activity for
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		final TextView textMessage = (TextView) promptsView
				.findViewById(R.id.text_message);
		final TextView title = (TextView) promptsView
				.findViewById(R.id.text_dialog_title);

		// setting dialog title
		title.setVisibility(TextView.VISIBLE);
		title.setText(Constants.sLblNoNetwork);

		// Setting Dialog Message
		textMessage.setText(Constants.ERROR_NETWORK_SYNC_ERROR);

		helpBuilder.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// update user activity
						helpBuilder.updateInactivityForDialog();
					}
				});

		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();
	}

	// checking for network connection
	public boolean checkNetwork() {
		ConnectivityManager connectivityManager = (ConnectivityManager) this
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	// Database open here
	public void openDB() {
		try {
			mDBHelper = new DatabaseAdapter(this);
			mDBHelper.createDatabase();
			mDBHelper.open();
		} catch (SQLException e4) {
			e4.printStackTrace();
		}
	}

	// DataBase close here
	public void closeDB() {
		try {
			if (mDBHelper != null) {
				mDBHelper.close();
			}
		} catch (SQLException se) {
			LogUtil.d(TAG, "Exception : " + se.getMessage());

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		InactivityUtils.logoutFromActivityBelowEjobForm(this);
	}

    /* Bug 665 : This method will return true if the vehicle or tyre
     * is blacklisted else false is returned.
     * Blacklisted vehicle is the one whose vehicle contract id,
     * vehicle SAP code are not zero or null, has correct tyre number
     * and tyre's SAP information must not be zero or null.
     */
    public static boolean isBlackListedVehicle() {
        Log.d(TRACE_INFO, " Inside isBlackListedVehicle()");
        if (!isVehicleValid()) {//Not valid vehicle
            Log.d(TRACE_INFO, " Is a blacklisted vehicle!");
            return true;
        } else {
            Log.d(TRACE_INFO, " Is a valid(not blacklisted) vehicle");
            return false;
        }
    }

    /* Bug 665 : isVehicleValid() will return true if Vehicle information
     * and tyre information is valid else false is returned
     */
    public static boolean isVehicleValid() {
        Log.d(TRACE_INFO, " Inside isVehicleValid()");
        ArrayList<Contract> contractInfo = new ArrayList<Contract>();
        try {
            contractInfo = JsonParser.getContracts(Constants._SAPJSONVAl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Contract vehicleContract : contractInfo) {
			/* vehicle's validity is checked against contract id and SAP code */
            if (null == vehicleContract.getContractId() || (0 == Integer.parseInt(vehicleContract.getContractId())) || TextUtils.isEmpty(vehicleContract.getContractId())
                    || null == vehicleContract.getVehicleSapCode() || (0 == Integer.parseInt(vehicleContract.getVehicleSapCode())) || TextUtils.isEmpty(vehicleContract.getVehicleSapCode())) {
                Log.d(TRACE_INFO, " Vehicle's contract id or SAP code is invalid");
                return false;
            }
        }
        if (!isTyreInfoValid(Constants._SAPJSONVAl)) {
            return false;
        }
        return true;
    }

    /* Bug 665 : isTyreInfoValid() returns true if the number of tyres in a vehicle and SAP information are valid */
    public static boolean isTyreInfoValid(String sapVehicleJson) {
        Log.d(TRACE_INFO, " Inside isTyreInfoValid()");
        int tyreObjSize = 0;
        int numberOfTyresInVehicle = 0;
        ArrayList<String> tyrePosition = new ArrayList<>();
        ArrayList<String> tyreAxle = new ArrayList<String>();
        ArrayList<Tyre> mTyreSAPInfo = new ArrayList<Tyre>();
        try {
            if (null != Constants.tireJson || (!TextUtils.isEmpty(Constants.tireJson))) {
                mTyreSAPInfo = JsonParser.getTyreInfo(Constants.tireJson);
            } else {
                Log.d(TRACE_INFO, " Tyre JSON object is null");
            }
            tyreObjSize = mTyreSAPInfo.size();
            VehicleSkeletonInfo vehicleContext = new VehicleSkeletonInfo(Constants.vehicleConfiguration);
            numberOfTyresInVehicle = vehicleContext.getNumberOfTyresInVehicle();
            int numberOfAxleInVehicle = vehicleContext.getAxlecount();
            Log.d(TRACE_INFO, " Number of tyres according to vehicle skeleton : " + numberOfTyresInVehicle);
            Log.d(TRACE_INFO, " Number of tyres object in Tyre JSON : " + tyreObjSize);

			/* Tyre is validated against number of tyres received in vehicle skeleton and Tyre information JSON object size */
            if (numberOfTyresInVehicle != tyreObjSize) {
                Log.d(TRACE_INFO, " Number of tyres in the vehicle against vehicle skeleton is invalid");
                return false;
            }

			/* Tyre's SAP information should not be zero or null.
			 * SAPCodeWP and SAPCodeTI cannot be null or zero
			 * whereas for few vehicles SAPVendorCodeID and SAPMaterialCodeID
			 * can be null*/
            for (Tyre tire : mTyreSAPInfo) {
                tyrePosition.add(tire.getPosition());
                if(tire.getPosition().length() > 1) {
                    tyreAxle.add(tire.getPosition().substring(0, 1));
                }
                if (null == tire.getSapCodeTI() || TextUtils.isEmpty(tire.getSapCodeTI())
                        || null == tire.getSapCodeWP() || TextUtils.isEmpty(tire.getSapCodeWP())) {
                    Log.d(TRACE_INFO, " Tyre's one of the SAP code is invalid");
                    return false;
                } else {
                    if (0 == Integer.parseInt(tire.getSapCodeTI()) || 0 == Integer.parseInt(tire.getSapCodeWP())) {
                        Log.d(TRACE_INFO, " Tyre's one of the SAP code is zero");
                        return false;
                    }
                }
            }

            Set<String> set = new HashSet<String>();
            Boolean duplicateTyrePosition = false;
            int trackAxle = 0;
            for (int i = 0; i < tyrePosition.size(); i++) {
                if(set.add(tyrePosition.get(i)) == false){
                    duplicateTyrePosition = true;
                    Log.d(TRACE_INFO, " Duplicate tyre position : "+tyrePosition.get(i));
                    return false;
                }
            }
            for(int j = 0; j < tyreAxle.size(); j++){
                if(set.add(tyreAxle.get(j)) == true){
                    trackAxle++;
                }
            }
            Log.d(TRACE_INFO, "Tyre axle in JSON: "+trackAxle);
            Log.d(TRACE_INFO, "Tyre axle in Vehicle configuration : "+numberOfAxleInVehicle);
            if(trackAxle != numberOfAxleInVehicle) {
                Log.d(TRACE_INFO, "Axle count not matching : "+trackAxle);
                return false;
            }

        } catch (Exception e) {
            Log.d(TRACE_INFO, " Caught exception " + e);
            e.printStackTrace();
        }
        return true;
    }
}
