package com.goodyear.ejob.dbmodel;

public class JobRemovalReason {

	private int jobRemovalReasonID;
	private String reason;

	public int getJobRemovalReasonID() {
		return jobRemovalReasonID;
	}

	public void setJobRemovalReasonID(int jobRemovalReasonID) {
		this.jobRemovalReasonID = jobRemovalReasonID;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
