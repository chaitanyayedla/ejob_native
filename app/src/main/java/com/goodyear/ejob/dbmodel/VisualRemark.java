
package com.goodyear.ejob.dbmodel;

import java.io.Serializable;

/**
 * @author giriprasanth.vp
 * The Purpose of this model is to pull and push the properties of VisualRemark with the help of Single Object.
 */
public class VisualRemark implements Serializable {

	private Integer mVRId;
	private String mType;
	private String mDescription;
	private String mDo;
	
	public String getType() {
		return mType;
	}
	public void setType(String mType) {
		this.mType = mType;
	}
	public String getDescription() {
		return mDescription;
	}
	public void setDescription(String mDescription) {
		this.mDescription = mDescription;
	}
	public String getDo() {
		return mDo;
	}
	public void setDo(String mDo) {
		this.mDo = mDo;
	}
	public Integer getVRId() {
		return mVRId;
	}

	public void setVRId(Integer mVRId) {
		this.mVRId = mVRId;
	}


	@Override
	public String toString() {
		return "\n Visual Remark ID - "+mVRId+"\n Type - "+mType+"\n Description - "+mDescription+"\n Do - "+mDo;
	}
}
