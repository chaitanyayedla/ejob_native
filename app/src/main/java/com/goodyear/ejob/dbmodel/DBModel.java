package com.goodyear.ejob.dbmodel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.goodyear.ejob.dbhelpers.DataBaseHelper;
import com.goodyear.ejob.job.operation.helpers.TireImageItem;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.LogUtil;

/**
 * @author johnmiya.s
 * 
 */
public class DBModel {

	private static Context mContext = null;
	private static SQLiteDatabase mDb;
	private DataBaseHelper mDbHelper;
	private static String Log_TAG = "DBModel";

	public DBModel(Context context) {
		mContext = context;
		mDbHelper = new DataBaseHelper(mContext);
	}

	public DBModel open() throws SQLException {
		try {
			mDbHelper.openDataBase();
			mDb = mDbHelper.getReadableDatabase();
		} catch (SQLException mSQLException) {
			throw mSQLException;
		}
		return this;
	}

	public void close() throws SQLException {
		try {
			mDb.close();
		} catch (SQLException mSQLException) {
			throw mSQLException;
		}
	}

	// Insertion in Job Table
	public static boolean insertJobTable(String ExternalID,
			String LicenseNumber, String SAPContractNumber, String VehicleID,
			String Technitian, String JobType, String OdometerType,
			String GPSLocation, String Location, String TimeFinished,
			String MissingOdometerReasonID, String AgreeStatus,
			String VehicleUnattendedStatus, String TimeOnSite,
			String TimeStarted, String TimeReceived, String CallerPhone,
			String CallerName, String VendorCountryID,
			String ThirtyMinutesRetorqueStatus, String DefectAuthNumber,
			String DriverName, String ActionLineN, String Hudometer,
			String VendorDetail, String VendorSAPCode,
			String SignatureReasonID, String StockLocation, String VehicleODO,
			byte[] SignatureImage, String Note, String Status,
			String jobRefNumber, String JobValidityDate,
			long TimeOffsetMinutes, String JobRemovalReasonID,
			String PreviousOdometerValue,String TorqueSettingsSame) {

		LogUtil.DBLog(Log_TAG,"Insert Job","Start");
		try {
			ContentValues cv = new ContentValues();
			cv.put("ExternalID", ExternalID);
			cv.put("LicenseNumber", LicenseNumber);
			cv.put("SAPContractNumber", SAPContractNumber);
			cv.put("VehicleID", VehicleID);
			cv.put("Technitian", Technitian);
			cv.put("JobType", JobType);
			cv.put("OdometerType", OdometerType);
			cv.put("GPSLocation", GPSLocation);
			cv.put("Location", Location);//
			cv.put("TimeFinished", TimeFinished);//
			cv.put("MissingOdometerReasonID", MissingOdometerReasonID);
			cv.put("AgreeStatus", AgreeStatus);
			cv.put("VehicleUnattendedStatus", VehicleUnattendedStatus);
			cv.put("TimeOnSite", TimeOnSite);//
			cv.put("TimeStarted", TimeStarted);//
			cv.put("TimeReceived", TimeReceived);//
			cv.put("CallerPhone", CallerPhone);//
			cv.put("CallerName", CallerName);//
			cv.put("VendorCountryID", VendorCountryID);
			cv.put("ThirtyMinutesRetorqueStatus", ThirtyMinutesRetorqueStatus);
			cv.put("DefectAuthNumber", DefectAuthNumber);//
			cv.put("DriverName", DriverName);//
			cv.put("ActionLineN", ActionLineN);//
			cv.put("Hudometer", Hudometer);//
			cv.put("VendorDetail", VendorDetail);
			cv.put("VendorSAPCode", VendorSAPCode);
			cv.put("SignatureReasonID", SignatureReasonID);
			cv.put("StockLocation", StockLocation);//
			cv.put("VehicleODO", VehicleODO);//
			cv.put("SignatureImage", SignatureImage);
			cv.put("Note", Note);
			cv.put("Status", Status);
			cv.put("jobRefNumber", jobRefNumber);
			cv.put("JobValidityDate", JobValidityDate);
			cv.put("TimeOffsetMinutes", TimeOffsetMinutes);
			cv.put("JobRemovalReasonID", JobRemovalReasonID);

			//CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
			//To store last recorded odometer Value
			cv.put("PreviousOdometerValue", PreviousOdometerValue);

			//CR - 550 (some vehicles have different torque settings per axle)
			//To store Torque Settings per axle info
			cv.put("TorqueSettingsSame",TorqueSettingsSame);

			LogUtil.DBLog(Log_TAG, "Insert Job", "Job External ID - " + ExternalID);
			LogUtil.DBLog(Log_TAG, "Insert Job", "Job Reference Number - " + jobRefNumber);
			LogUtil.DBLog(Log_TAG, "Insert Job", "VehicleID - " + VehicleID + " - LicenseNumber - " + LicenseNumber + " - SAPContractNumber - " + SAPContractNumber);
			LogUtil.DBLog(Log_TAG, "Insert Job", "Before Insertion in Job Table");
			mDb.insert("Job", null, cv);
			LogUtil.DBLog(Log_TAG, "Insert Job", "After Insertion in Job Table - Success");

			Log.d("SaveJob", "Tyre Management Data Inserted in Job Table");
			LogUtil.DBLog(Log_TAG, "Insert Job", "End : Return - true");
			return true;

		} catch (Exception ex) {
			Log.d("SaveJob", ex.toString());
			LogUtil.DBLog(Log_TAG, "Insert Job", "Exception " + ex.toString());
			return false;
		}
	}

	// Insertion in Job Table
	public static int updateJobTable(String ExternalID, String LicenseNumber,
			String SAPContractNumber, String VehicleID, String Technitian,
			String JobType, String OdometerType, String GPSLocation,
			String Location, String TimeFinished,
			String MissingOdometerReasonID, String AgreeStatus,
			String VehicleUnattendedStatus, String TimeOnSite,
			String TimeStarted, String TimeReceived, String CallerPhone,
			String CallerName, String VendorCountryID,
			String ThirtyMinutesRetorqueStatus, String DefectAuthNumber,
			String DriverName, String ActionLineN, String Hudometer,
			String VendorDetail, String VendorSAPCode,
			String SignatureReasonID, String StockLocation, String VehicleODO,
			byte[] SignatureImage, String Note, String Status,
			String jobRefNumber, String JobValidityDate,
			long TimeOffsetMinutes, String JobRemovalReasonID,
			String PreviousOdometerValue, String TorqueSettingsSame) {

		LogUtil.DBLog(Log_TAG, "Update Job", "Start");
		try {
			ContentValues cv = new ContentValues();
			cv.put("ExternalID", ExternalID);
			cv.put("LicenseNumber", LicenseNumber);
			cv.put("SAPContractNumber", SAPContractNumber);
			cv.put("VehicleID", VehicleID);
			cv.put("Technitian", Technitian);
			cv.put("JobType", JobType);
			cv.put("OdometerType", OdometerType);
			cv.put("GPSLocation", GPSLocation);
			cv.put("Location", Location);//
			cv.put("TimeFinished", TimeFinished);//
			cv.put("MissingOdometerReasonID", MissingOdometerReasonID);
			cv.put("AgreeStatus", AgreeStatus);
			cv.put("VehicleUnattendedStatus", VehicleUnattendedStatus);
			cv.put("TimeOnSite", TimeOnSite);//
			cv.put("TimeStarted", TimeStarted);//
			cv.put("TimeReceived", TimeReceived);//
			cv.put("CallerPhone", CallerPhone);//
			cv.put("CallerName", CallerName);//
			cv.put("VendorCountryID", VendorCountryID);
			cv.put("ThirtyMinutesRetorqueStatus", ThirtyMinutesRetorqueStatus);
			cv.put("DefectAuthNumber", DefectAuthNumber);//
			cv.put("DriverName", DriverName);//
			cv.put("ActionLineN", ActionLineN);//
			cv.put("Hudometer", Hudometer);//
			cv.put("VendorDetail", VendorDetail);
			cv.put("VendorSAPCode", VendorSAPCode);
			cv.put("SignatureReasonID", SignatureReasonID);
			cv.put("StockLocation", StockLocation);//
			cv.put("VehicleODO", VehicleODO);//
			cv.put("SignatureImage", SignatureImage);
			cv.put("Note", Note);
			cv.put("Status", Status);
			cv.put("jobRefNumber", jobRefNumber);
			cv.put("JobValidityDate", JobValidityDate);
			cv.put("TimeOffsetMinutes", TimeOffsetMinutes);
			cv.put("JobRemovalReasonID", JobRemovalReasonID);
			//CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
			//To store last recorded odometer Value
			cv.put("PreviousOdometerValue", PreviousOdometerValue);

			//CR - 550 (some vehicles have different torque settings per axle)
			//To store Torque Settings per axle info
			cv.put("TorqueSettingsSame",TorqueSettingsSame);

			LogUtil.DBLog(Log_TAG, "Update Job", "Job External ID - " + ExternalID);
			LogUtil.DBLog(Log_TAG, "Update Job", "Job Reference Number - " + jobRefNumber);
			LogUtil.DBLog(Log_TAG, "Update Job", "VehicleID - " + VehicleID + " - LicenseNumber - " + LicenseNumber + " - SAPContractNumber - " + SAPContractNumber);
			LogUtil.DBLog(Log_TAG, "Update Job", "Updating Job Data in Job Table");

			return mDb.update("Job", cv, "ExternalID = '" + ExternalID + "' ",
					null);

		} catch (Exception ex) {
			Log.d("SaveJob", ex.toString());
			LogUtil.DBLog(Log_TAG, "Update Job", "Exception : " + ex.toString());
			return 0;
		}
	}

	// InsertJobItem
	//Additional Fields added for CR INSPECTION-447 (VisualDisplayID, Temperature, RectPressure)
	public boolean insertJobItem(String JobID, String SAPCodeTiID,
			String ExternalID, String ActionType, String MinNSK, String TyreID,
			String RemovalReasonID, String DamageID, String CasingRouteCode,
			String Regrooved, String TorqueSetting, String Note,
			String TreadTepth, String Pressure, String GrooveNumber,
			String ReGrooveNumber, String ServiceCount, String RegrooveType,
			String NSK1Before, String NSK1After, String ServiceID,
			String AxleNumber, String AxleServiceType, String WorkOrderNumber,
			String RepairCompany, String NSK2Before, String NSK2After,
			String NSK3Before, String NSK3After, String SwapType,
			String Sequence, String RecInflactionOriginal,
			String RecInflactionDestination, String SWAPOriginalPosition,
			String OperationID, String RimType,
			String VisualCommentID, String temperature, String RectPressure,String InspectionWithOperation,String IsPerfect) {
		try {
			ContentValues cv = new ContentValues();
			cv.put("JobID", JobID);
			cv.put("SAPCodeTiID", SAPCodeTiID);
			cv.put("ExternalID", ExternalID);
			cv.put("ActionType", ActionType);
			cv.put("MinNSK", MinNSK);
			cv.put("TyreID", TyreID);
			cv.put("RemovalReasonID", RemovalReasonID);
			cv.put("DamageID", DamageID);
			cv.put("CasingRouteCode", CasingRouteCode);
			cv.put("Regrooved", Regrooved);
			cv.put("TorqueSetting", TorqueSetting);
			cv.put("Note", Note);
			cv.put("TreadTepth", TreadTepth);
			cv.put("Pressure", Pressure);
			cv.put("GrooveNumber", GrooveNumber);
			cv.put("ReGrooveNumber", ReGrooveNumber);
			cv.put("ServiceCount", ServiceCount);
			cv.put("RegrooveType", RegrooveType);
			cv.put("NSK1Before", NSK1Before);
			cv.put("NSK1After", NSK1After);
			cv.put("ServiceID", ServiceID);
			cv.put("AxleNumber", AxleNumber);
			cv.put("AxleServiceType", AxleServiceType);
			cv.put("WorkOrderNumber", WorkOrderNumber);
			cv.put("RepairCompany", RepairCompany);
			cv.put("NSK2Before", NSK2Before);
			cv.put("NSK2After", NSK2After);
			cv.put("NSK3Before", NSK3Before);
			cv.put("NSK3After", NSK3After);
			cv.put("SwapType", SwapType);
			cv.put("Sequence", Sequence);
			cv.put("RecInflactionOriginal", RecInflactionOriginal);
			cv.put("RecInflactionDestination", RecInflactionDestination);
			cv.put("SWAPOriginalPosition", SWAPOriginalPosition);
			cv.put("OperationID", OperationID);
			cv.put("RimType", RimType);
			cv.put("VisualCommentsID", VisualCommentID);
			cv.put("Temperature" +
					"", temperature);
			cv.put("RectPressure", RectPressure);
			//To Determine Whether Inspection is Done Along With Operation or Not
			cv.put("InspectionWithOperation",InspectionWithOperation);
			//To determine whether a job item can be added to a snapshot DB or not
			cv.put("IsPerfect",IsPerfect);

			LogUtil.DBLog(Log_TAG, "Insert Job Item", "Job ID - " + JobID);
			LogUtil.DBLog(Log_TAG, "Insert Job Item", "External ID - "+ ExternalID +"\tAction Type - " + ActionType);
			LogUtil.DBLog(Log_TAG, "Insert Job Item", "VisualCommentsID - " + VisualCommentID + "\tTemperature - " + temperature + "\tRectPressure - " +RectPressure);
			LogUtil.DBLog(Log_TAG, "Insert Job Item", "Before Insertion in Job Item Table");

			mDb.insert("JobItem", null, cv);
			Log.d("SaveJob", "Data Inserted in JobItem Table");
			LogUtil.DBLog(Log_TAG, "Insert Job Item", "After Insertion in Job Item Table - Success");
			return true;

		} catch (Exception ex) {
			LogUtil.DBLog(Log_TAG, "Insert Job Item", "Exception - " +ex.toString());
			Log.d("SaveJob", ex.toString());
			return false;
		}
	}

	// InsertJobItem
	//Additional Fields added for CR INSPECTION-447 (VisualDisplayID, Temperature, RectPressure)
	public int updateJobItem(String JobID, String SAPCodeTiID,
			String ExternalID, String ActionType, String MinNSK, String TyreID,
			String RemovalReasonID, String DamageID, String CasingRouteCode,
			String Regrooved, String TorqueSetting, String Note,
			String TreadTepth, String Pressure, String GrooveNumber,
			String ReGrooveNumber, String ServiceCount, String RegrooveType,
			String NSK1Before, String NSK1After, String ServiceID,
			String AxleNumber, String AxleServiceType, String WorkOrderNumber,
			String RepairCompany, String NSK2Before, String NSK2After,
			String NSK3Before, String NSK3After, String SwapType,
			String Sequence, String RecInflactionOriginal,
			String RecInflactionDestination, String SWAPOriginalPosition,
			String OperationID, String RimType,
			String VisualCommentID, String temperature, String RectPressure,
							 String InspectionWithOperation, String IsPerfect) {
		try {
			ContentValues cv = new ContentValues();
			cv.put("JobID", JobID);
			cv.put("SAPCodeTiID", SAPCodeTiID);
			cv.put("ExternalID", ExternalID);
			cv.put("ActionType", ActionType);
			cv.put("MinNSK", MinNSK);
			cv.put("TyreID", TyreID);
			cv.put("RemovalReasonID", RemovalReasonID);
			cv.put("DamageID", DamageID);
			cv.put("CasingRouteCode", CasingRouteCode);
			cv.put("Regrooved", Regrooved);
			cv.put("TorqueSetting", TorqueSetting);
			cv.put("Note", Note);
			cv.put("TreadTepth", TreadTepth);
			cv.put("Pressure", Pressure);
			cv.put("GrooveNumber", GrooveNumber);
			cv.put("ReGrooveNumber", ReGrooveNumber);
			cv.put("ServiceCount", ServiceCount);
			cv.put("RegrooveType", RegrooveType);
			cv.put("NSK1Before", NSK1Before);
			cv.put("NSK1After", NSK1After);
			cv.put("ServiceID", ServiceID);
			cv.put("AxleNumber", AxleNumber);
			cv.put("AxleServiceType", AxleServiceType);
			cv.put("WorkOrderNumber", WorkOrderNumber);
			cv.put("RepairCompany", RepairCompany);
			cv.put("NSK2Before", NSK2Before);
			cv.put("NSK2After", NSK2After);
			cv.put("NSK3Before", NSK3Before);
			cv.put("NSK3After", NSK3After);
			cv.put("SwapType", SwapType);
			cv.put("Sequence", Sequence);
			cv.put("RecInflactionOriginal", RecInflactionOriginal);
			cv.put("RecInflactionDestination", RecInflactionDestination);
			cv.put("SWAPOriginalPosition", SWAPOriginalPosition);
			cv.put("OperationID", OperationID);
			cv.put("RimType", RimType);
			cv.put("VisualCommentsID", VisualCommentID);
			cv.put("Temperature", temperature);
			cv.put("RectPressure", RectPressure);
			//To Determine Whether Inspection is Done Along With Operation or Not
			cv.put("InspectionWithOperation",InspectionWithOperation);
			//To determine whether a job item can be added to a snapshot DB or not
			cv.put("IsPerfect",IsPerfect);

			LogUtil.DBLog(Log_TAG, "Insert Job Item", "Job ID - " + JobID);
			LogUtil.DBLog(Log_TAG, "Insert Job Item", "External ID - "+ ExternalID +"\tAction Type - " + ActionType);
			LogUtil.DBLog(Log_TAG, "Insert Job Item", "VisualCommentsID - " + VisualCommentID + "\tTemperature - " + temperature + "\tRectPressure - " +RectPressure);
			LogUtil.DBLog(Log_TAG, "Insert Job Item", "Before Insertion in Job Item Table");

			return mDb.update("JobItem", cv, "ExternalID = '" + ExternalID
					+ "' ", new String[] {});

		} catch (Exception ex) {
			LogUtil.DBLog(Log_TAG, "Update Job Item", "Exception - "+ex.toString());
			Log.d("SaveJob", ex.toString());
			return 0;
		}
	}

	// deleteSingleJobItem
	public void deleteSingleJobItem(String ExternalID) {
		LogUtil.DBLog(Log_TAG, "Delete SINGLE Job Item", "Start - Before Delete");
		LogUtil.DBLog(Log_TAG, "Delete SINGLE Job Item", "External ID - " + ExternalID);
		mDb.delete("JobItem", " ExternalID = '" + ExternalID + "'", null);
		LogUtil.DBLog(Log_TAG, "Delete SINGLE Job Item", "End - After Delete");
	}

	// jobCurrectionInsertion
	public void insertJobCorrectionInsertion(String mJobID, String mTyreID,
			String mKeyName, String mOldValue, String mNewValue, String IsPerfect) {

		LogUtil.DBLog(Log_TAG, "Insert Job Correction", "Start");

		ContentValues cv = new ContentValues();
		cv.put("JobID", mJobID);
		cv.put("TyreID", mTyreID);
		cv.put("KeyName", mKeyName);
		cv.put("OldValue", mOldValue);
		cv.put("NewValue", mNewValue);

		//To determine whether a job correction can be added to a snapshot DB or not
		cv.put("IsPerfect",IsPerfect);

		LogUtil.DBLog(Log_TAG, "Insert Job Correction", "JobID - " + mJobID + "\tIsPerfect - "+IsPerfect);
		LogUtil.DBLog(Log_TAG, "Insert Job Correction", "Before Insertion of JobCorrection Data");
		mDb.insert("JobCorrection", null, cv);
		LogUtil.DBLog(Log_TAG, "Insert Job Correction", "After Insertion of JobCorrection Data");
	}

	// jobCurrectionInsertion
	public int updateJobCorrectionInsertion(String mJobID, String mTyreID,
			String mKeyName, String mOldValue, String mNewValue, String IsPerfect) {
		try {
			LogUtil.DBLog(Log_TAG, "Update Job Correction", "Start");
			ContentValues cv = new ContentValues();
			cv.put("JobID", mJobID);
			cv.put("TyreID", mTyreID);
			cv.put("KeyName", mKeyName);
			cv.put("OldValue", mOldValue);
			cv.put("NewValue", mNewValue);

			//To determine whether a job correction can be added to a snapshot DB or not
			cv.put("IsPerfect",IsPerfect);

			LogUtil.DBLog(Log_TAG, "Update Job Correction", "JobID - " + mJobID + "\tIsPerfect - "+IsPerfect);
			LogUtil.DBLog(Log_TAG, "Update Job Correction", "During Update");

			return mDb.update("JobCorrection", cv, "JobID = '" + mJobID
					+ "' AND TyreID = '" + mTyreID + "' AND KeyName='"
					+ mKeyName + "'", new String[] {});
		} catch (Exception e) {
			LogUtil.DBLog(Log_TAG, "Update Job Correction", "Exception - "+e.getMessage().toString());
			e.printStackTrace();
			return 0;
		}
	}

	// TorqueSetting
	public void insertTorqueSettingTable(String jobId, String wheelPos,
			int isWheelRemoved, String sapCodeWP, String wrenchNumber,
			String noManufacturerTorqueSettingsReason, int torqueSettingsValue,
			int retorqueLabelIssued) {
		LogUtil.DBLog(Log_TAG, "Insert Torque Setting", "Starts");
		try {
			ContentValues cv = new ContentValues();
			cv.put("JobId", jobId);
			cv.put("WheelPosition", wheelPos);
			cv.put("IsWheelRemoved", isWheelRemoved);
			cv.put("SapCodeWP", sapCodeWP);
			cv.put("WrenchNumber", wrenchNumber);
			cv.put("NoManufacturerTorqueSettingReason",
					noManufacturerTorqueSettingsReason);
			cv.put("TorqueSettingValue", torqueSettingsValue);
			cv.put("RetorqueLabelIssued", retorqueLabelIssued);
			LogUtil.DBLog(Log_TAG, "Insert Torque Setting", "Before Insertion -  JobID - " + jobId);
			mDb.insert("TorqueSetting", null, cv);
			LogUtil.DBLog(Log_TAG, "Insert Torque Setting", "After insertion");
		} catch (Exception e) {
			LogUtil.DBLog(Log_TAG, "Insert Torque Setting", "Exception -"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// TorqueSetting
	public int updateTorqueSettingTable(String jobId, String wheelPos,
			int isWheelRemoved, String sapCodeWP, String wrenchNumber,
			String noManufacturerTorqueSettingsReason, int torqueSettingsValue,
			int retorqueLabelIssued) {

		LogUtil.DBLog(Log_TAG, "Update Torque Setting", "Starts");
		try {
			ContentValues cv = new ContentValues();
			cv.put("JobId", jobId);
			cv.put("WheelPosition", wheelPos);
			cv.put("IsWheelRemoved", isWheelRemoved);
			cv.put("SapCodeWP", sapCodeWP);
			cv.put("WrenchNumber", wrenchNumber);
			cv.put("NoManufacturerTorqueSettingReason",
					noManufacturerTorqueSettingsReason);
			cv.put("TorqueSettingValue", torqueSettingsValue);
			cv.put("RetorqueLabelIssued", retorqueLabelIssued);
			LogUtil.DBLog(Log_TAG, "Update Torque Setting", "During Update - JobID - " + jobId);
			return mDb.update("TorqueSetting", cv, "JobId = '" + jobId
					+ "' AND WheelPosition='" + wheelPos + "'", null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtil.DBLog(Log_TAG, "Update Torque Setting",  "Exception -"+e.getMessage());
			e.printStackTrace();
			return 0;
		}

	}

	// Blob mTyreImage
	public void insertTyreImageTable(String mJobItemID, String mImageType,
			byte[] mTyreImage, String mDescription) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("JobItemID", mJobItemID);
			cv.put("ImageType", mImageType);
			cv.put("TyreImage", mTyreImage);
			cv.put("Description", mDescription);
			mDb.insert("TyreImage", null, cv);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtil.DBLog(Log_TAG, "Insert data Tyre Image Table",  "Exception -"+e.getMessage());
			e.printStackTrace();
		}
	}
	// Blob mTyreImage
	public void insertTyreImageTable(TireImageItem imageItem) {
		LogUtil.DBLog(Log_TAG, "Insert Tyre Image", "Starts");
		try {
			updateJobItemIdForTireImage(imageItem);
			ContentValues cv = new ContentValues();
			cv.put("JobItemID", imageItem.getJobItemId());
			cv.put("ImageType", imageItem.getImageType());
			cv.put("TyreImage", imageItem.getTyreImage());
			cv.put("Description", imageItem.getDamageDescription());
			LogUtil.DBLog(Log_TAG, "Insert Tyre Image", "Before Insertion - JOB ITEM ID - "+imageItem.getJobItemId());
			mDb.insert("TyreImage", null, cv);
			LogUtil.DBLog(Log_TAG, "Insert Tyre Image", "After Insertion");
		} catch (Exception e) {
			LogUtil.DBLog(Log_TAG, "Insert Tyre Image",  "Exception -"+e.getMessage());
			e.printStackTrace();
		}
	}

	private void updateJobItemIdForTireImage(TireImageItem imageItem) {
		LogUtil.DBLog(Log_TAG, "update JobItem Id For Tyre Image", "Initialized");
		Cursor cursor = mDb.query("JobItem", new String[]{"ID"}, "ExternalId = ?", new String[]{imageItem
                .getJobExternalId()}, null, null, null);
		if (CursorUtils.isValidCursor(cursor)) {
            cursor.moveToFirst();
			imageItem.setJobItemId(cursor.getString(cursor.getColumnIndex("ID")));
            CursorUtils.closeCursor(cursor);
        }
		LogUtil.DBLog(Log_TAG, "JobItem Id For Tyre Image", "Updated");
	}

	// Blob mTyreImage
	public int updateTyreImageTable(String mJobItemID, String mImageType,
			byte[] mTyreImage, String mDescription) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("JobItemID", mJobItemID);
			cv.put("ImageType", mImageType);
			cv.put("TyreImage", mTyreImage);
			cv.put("Description", mDescription);
			String whrConditions = "JobItemID = '" + mJobItemID
					+ "' and ImageType = '" + mImageType + "'";
			return mDb.update("TyreImage", cv, whrConditions, new String[] {});
		} catch (Exception e) {
			LogUtil.DBLog(Log_TAG, "Update Tyre Image",  "Exception -"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

	public boolean checkIfReservedTyreExists(String mJobID, String mTyreID) {
		LogUtil.DBLog(Log_TAG, "Check the existance of Reserved Tyre", "Initialized");
        boolean isReservedTyreExists = false;
        Cursor cursor = null;
        try {
            String query = "SELECT * FROM ReservedTyre WHERE JobID = '" + mJobID + "' AND TyreID = '" + mTyreID + "'";
            cursor = mDb.rawQuery(query, null);
            if (CursorUtils.isValidCursor(cursor)) {
                isReservedTyreExists = true;
                CursorUtils.closeCursor(cursor);
            }

        } catch (Exception e) {
			LogUtil.DBLog(Log_TAG, "Check the existance of Reserved Tyre", "Exception -"+e.getMessage());
            e.printStackTrace();
        } finally {
            CursorUtils.closeCursor(cursor);
        }
		LogUtil.DBLog(Log_TAG, "Check the existance of Reserved Tyre", "Ends - return exist or not, Result - " +isReservedTyreExists );
        return isReservedTyreExists;
    }

	public void reservedTyreInsertionIfNotExists(String mJobID, String mTyreID) {
		try {
			if (!checkIfReservedTyreExists(mJobID, mTyreID)) {
				ContentValues cv = new ContentValues();
				cv.put("JobID", mJobID);
				cv.put("TyreID", mTyreID);
				mDb.insert("ReservedTyre", null, cv);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtil.DBLog(Log_TAG, "Check the existance of Reserved Tyre",  "Exception -"+e.getMessage());
			e.printStackTrace();
		}
	}

	public void insertReservedTyreInsertion(String mJobID, String mTyreID) {
		try {
			LogUtil.DBLog(Log_TAG, "Reserved Tyre Insertion", "Initialized");
			ContentValues cv = new ContentValues();
			cv.put("JobID", mJobID);
			cv.put("TyreID", mTyreID);
			LogUtil.DBLog(Log_TAG, "Reserved Tyre Insertion", "Before Reserved Tyre Insert - JobID - " + mJobID + " - TyreId - " + mTyreID);
			mDb.insert("ReservedTyre", null, cv);
			LogUtil.DBLog(Log_TAG, "Reserved Tyre Insertion", "After Reserved Tyre Insert - End");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtil.DBLog(Log_TAG, "Reserved Tyre Insertion", "Exception -"+e.getMessage());
			e.printStackTrace();
		}

	}

	public void updateReservedTyreInsertion(String mJobID, String mTyreID) {
		LogUtil.DBLog(Log_TAG, "Update Reserved Tyre", "Start");
		try {
			ContentValues cv = new ContentValues();
			cv.put("JobID", mJobID);
			cv.put("TyreID", mTyreID);
			LogUtil.DBLog(Log_TAG, "Update Reserved Tyre", "Before update Reserved Tyre Info - JobID - " + mJobID + " - TyreId - " + mTyreID);
			mDb.update("ReservedTyre", cv, "", new String[]{});
			LogUtil.DBLog(Log_TAG, "Update Reserved Tyre", "After update Reserved Tyre Info");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtil.DBLog(Log_TAG, "Update Reserved Tyre", "Exception - " + e.getMessage());
			e.printStackTrace();
		}

	}

	public void deleteReservedTyre(String mJobID, String mTyreID) {
		LogUtil.DBLog(Log_TAG, "Delete Reserved Tyre", "Check for Existance of Reserved tyre");
		if (!checkIfReservedTyreExists(mJobID, mTyreID)) {
			mDb.delete("ReservedTyre", "JobID = '" + mJobID
					+ "' AND TyreID = '" + mTyreID + "'", null);
		}
		LogUtil.DBLog(Log_TAG, "Delete Reserved Tyre", "Deleted");
	}

	public void deleteReservedTyres(String mJobID) {
		LogUtil.DBLog(Log_TAG, "Delete Reserved Tyres", "Start to delete - JobID - " + mJobID );
		mDb.delete("ReservedTyre", "JobID = '" + mJobID + "' ", null);
		LogUtil.DBLog(Log_TAG, "Delete Reserved Tyres", "Reserved Tyre Deleted");
	}

	public void insertAccountTable(String accLogin, String accPass,
			String jobReffNO, String passExpDate, int passNotficationType) {
		try {
			ContentValues cv = new ContentValues();
			cv.put("accLogin", accLogin);
			cv.put("accPass", accPass);
			cv.put("jobReffNO", jobReffNO);
			cv.put("passExpDate", passExpDate);
			cv.put("passNotficationType", passNotficationType);
			mDb.insert("Account", null, cv);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void updateAccountTable(String jobRefNum) {
		LogUtil.DBLog(Log_TAG, "Update Account", "Start");
		try {
			ContentValues cv = new ContentValues();

			cv.put("jobRefNumber", jobRefNum);
			LogUtil.DBLog(Log_TAG, "Update Account", "jobRefNum - " + jobRefNum);
			LogUtil.DBLog(Log_TAG, "Update Account", "Before updating Account");
			mDb.update("Account", cv, "ID = '1'", null);
			LogUtil.DBLog(Log_TAG, "Update Account", "After Account updating");
		} catch (Exception e) {
			LogUtil.DBLog(Log_TAG, "Update Account", "Exception - " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void insertVehicleTable(String mSAPCodeVeh,
			String IsDiffSizeAllowedForVehicle, String mLicenseNum,
			String mVehicleConfiguration, String mVin, String mChassisNum,
			String mFleetNum) {
		LogUtil.DBLog(Log_TAG, "Insert Vehicle", "Start");

		try {
			ContentValues cv = new ContentValues();
			cv.put("SAPCodeVeh", mSAPCodeVeh);
			cv.put("IsDiffSizeAllowedForVehicle", IsDiffSizeAllowedForVehicle);
			cv.put("LicenseNum", mLicenseNum);
			cv.put("VehicleConfiguration", mVehicleConfiguration);
			cv.put("Vin", mVin);
			cv.put("ChassisNum", mChassisNum);
			cv.put("FleetNum", mFleetNum);
			LogUtil.DBLog(Log_TAG, "Insert Vehicle", "SAPCodeVeh" + mSAPCodeVeh);
			LogUtil.DBLog(Log_TAG, "Insert Vehicle", "Before Vehicle insertion");
			mDb.insert("Vehicle", null, cv);
			LogUtil.DBLog(Log_TAG, "Insert Vehicle", "After Vehicle inserted");
		} catch (Exception e) {
			LogUtil.DBLog(Log_TAG, "Insert Vehicle", "Exception - " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int updateVehicleTable(String mSAPCodeVeh,
			String IsDiffSizeAllowedForVehicle, String mLicenseNum,
			String mVehicleConfiguration, String mVin, String mChassisNum,
			String mFleetNum) {
		LogUtil.DBLog(Log_TAG, "Update Vehicle", "Start");

		try {
			ContentValues cv = new ContentValues();
			cv.put("SAPCodeVeh", mSAPCodeVeh);
			cv.put("IsDiffSizeAllowedForVehicle", IsDiffSizeAllowedForVehicle);
			cv.put("LicenseNum", mLicenseNum);
			cv.put("VehicleConfiguration", mVehicleConfiguration);
			cv.put("Vin", mVin);
			cv.put("ChassisNum", mChassisNum);
			cv.put("FleetNum", mFleetNum);
			LogUtil.DBLog(Log_TAG, "Update Vehicle", "SAPCodeVehicle - " + mSAPCodeVeh);
			LogUtil.DBLog(Log_TAG, "Update Vehicle", "During Update");
			return mDb.update("Vehicle", cv, "SAPCodeVeh = '" + mSAPCodeVeh
					+ "' ", null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtil.DBLog(Log_TAG, "Update Vehicle", "Exception - " + e.getMessage());
			e.printStackTrace();
			return 0;
		}

	}

	public void deleteTyre(String id) {
		LogUtil.DBLog(Log_TAG, "Delete Tyre", "Start");
		LogUtil.DBLog(Log_TAG, "Delete Tyre", "External Id - " + id);
		mDb.delete("Tyre", " ExternalID = '" + id + "'", null);
		LogUtil.DBLog(Log_TAG, "Delete Tyre", "tyre Deleted");
	}

	public void deleteJobItem(String id) {
		LogUtil.DBLog(Log_TAG, "Delete Job Item", "Start - Before Deleting ");
		LogUtil.DBLog(Log_TAG, "Delete Job Item", "Job ID - " + id);
		mDb.delete("JobItem", " JobID = '" + id + "'", null);
		LogUtil.DBLog(Log_TAG, "Delete Job Item", "After Deletion ");
	}

	public void deleteTorqueSettings(String id) {
		LogUtil.DBLog(Log_TAG,"Delete Torque Settings","Start");
		mDb.delete("TorqueSetting", " JobID = '" + id + "'", null);
		LogUtil.DBLog(Log_TAG, "Delete Torque Settings", "Torque Seleted");
	}

	public void insertTyreTable(String mExternalID, String mPressure,
			String mSerialNumber, String mIsUnmounted, String mIsSpare,
			String mSAPContractNumberID, String mSAPVendorCodeID,
			String mSAPMaterialCodeID, String mPosition, String mSAPCodeTi,
			String mSAPCodeWp, String mOriginalNSK, String SAPCodeAxID,
			String mSAPCodeVeh, String mStatus, String mActive, String mNSK1,
			String mNSK2, String mNSK3, String midVehicleJob, String mShared,
			String mFromJobID,String mIsTyreCorrected, String mTempSAPVendorCodeID ) {
		LogUtil.DBLog(Log_TAG,"Insert Tyre Table","Start");
		try {
			ContentValues cv = new ContentValues();
			cv.put("ExternalID", mExternalID);
			cv.put("Pressure", mPressure);
			cv.put("SerialNumber", mSerialNumber);
			cv.put("IsUnmounted", mIsUnmounted);
			cv.put("IsSpare", mIsSpare);
			cv.put("SAPContractNumberID", mSAPContractNumberID);
			cv.put("SAPVendorCodeID", mSAPVendorCodeID);
			cv.put("SAPMaterialCodeID", mSAPMaterialCodeID);
			cv.put("Position", mPosition);
			cv.put("SAPCodeTi", mSAPCodeTi);
			cv.put("SAPCodeWp", mSAPCodeWp);
			cv.put("OriginalNSK", mOriginalNSK);
			cv.put("SAPCodeAxID", "");
			cv.put("SAPCodeVeh", mSAPCodeVeh);
			cv.put("Status", mStatus);
			cv.put("Active", mActive);
			cv.put("NSK1", mNSK1);
			cv.put("NSK2", mNSK2);
			cv.put("NSK3", mNSK3);
			cv.put("idVehicleJob", midVehicleJob);
			cv.put("Shared", mShared);
			cv.put("FromJobID", mFromJobID);
			//To Determine Whether TyreCorrection is Done for a tyre or not
			cv.put("IsTyreCorrected",mIsTyreCorrected);
			//To maintain Temp SAP Vendor code
			cv.put("TempSAPVendorCodeID",mTempSAPVendorCodeID);
			LogUtil.DBLog(Log_TAG, "Insert Tyre Table", "SerialNumber - "+mSerialNumber);
			LogUtil.DBLog(Log_TAG, "Insert Tyre Table", "Before Insert Tyre into Table");
			mDb.insert("Tyre", null, cv);
			LogUtil.DBLog(Log_TAG, "Insert Tyre Table", "After Tyre Inserted");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtil.DBLog(Log_TAG,"Insert Tyre Table","Exception - " + e.getMessage());
			e.printStackTrace();
		}

	}

	public int updateTyreTable(String mExternalID, String mPressure,
			String mSerialNumber, String mIsUnmounted, String mIsSpare,
			String mSAPContractNumberID, String mSAPVendorCodeID,
			String mSAPMaterialCodeID, String mPosition, String mSAPCodeTi,
			String mSAPCodeWp, String mOriginalNSK, String SAPCodeAxID,
			String mSAPCodeVeh, String mStatus, String mActive, String mNSK1,
			String mNSK2, String mNSK3, String midVehicleJob, String mShared,
			String mFromJobID, String mIsTyreCorrected, String mTempSAPVendorCodeID) {
		LogUtil.DBLog(Log_TAG,"Update Tyre Table","Start");
		try {
			ContentValues cv = new ContentValues();
			cv.put("ExternalID", mExternalID);
			cv.put("SerialNumber", mSerialNumber);
			cv.put("IsUnmounted", mIsUnmounted);
			cv.put("Pressure", mPressure);
			cv.put("IsSpare", mIsSpare);
			cv.put("SAPContractNumberID", mSAPContractNumberID);
			cv.put("SAPVendorCodeID", mSAPVendorCodeID);
			cv.put("SAPMaterialCodeID", mSAPMaterialCodeID);
			cv.put("Position", mPosition);
			cv.put("SAPCodeTi", mSAPCodeTi);
			cv.put("SAPCodeWp", mSAPCodeWp);
			cv.put("OriginalNSK", mOriginalNSK);
			cv.put("SAPCodeAxID", SAPCodeAxID);
			cv.put("SAPCodeVeh", mSAPCodeVeh);
			cv.put("Status", mStatus);
			cv.put("Active", mActive);
			cv.put("NSK1", mNSK1);
			cv.put("NSK2", mNSK2);
			cv.put("NSK3", mNSK3);
			cv.put("idVehicleJob", midVehicleJob);
			cv.put("Shared", mShared);
			cv.put("FromJobID", mFromJobID);
			//To Determine Whether TyreCorrection is Done for a tyre or not
			cv.put("IsTyreCorrected",mIsTyreCorrected);
			//To maintain Temp SAP Vendor code
			cv.put("TempSAPVendorCodeID",mTempSAPVendorCodeID);
			LogUtil.DBLog(Log_TAG, "Update Tyre Table", "SerialNumber - "+mSerialNumber
					+"\tExternal ID - "+mExternalID
					+"\tPosition - "+mPosition
					+"\tSapCode Ti - "+mSAPCodeTi
					+"\tSapCode WP - "+mSAPCodeWp);
			LogUtil.DBLog(Log_TAG,"Update Tyre Table","Tyre Info Updation Processes Started");

			return mDb.update("Tyre", cv,
					"ExternalID = '" + mExternalID + "' ", null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtil.DBLog(Log_TAG,"Update Tyre Table","Exception - " + e.getMessage());

			e.printStackTrace();
			return 0;
		}

	}

}
