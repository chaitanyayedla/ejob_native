package com.goodyear.ejob.dbmodel;

import android.content.ContentValues;

	public class JobCorrection {
	private int mID;
	private String mJobID;
	private String mTyreID;
	private String mKeyName;
	private String mOldValue;
	private String mNewValue;
	private String IsPerfect="1";


	public String getIsPerfect() {
		return IsPerfect;
	}

	public void setIsPerfect(String isPerfect) {
		IsPerfect = isPerfect;
	}



	public int getmID() {
		return mID;
	}
	public void setmID(int mID) {
		this.mID = mID;
	}
/**
	 * @return the mJobID
	 */
	public String getmJobID() {
		return mJobID;
	}
	/**
	 * @param mJobID the mJobID to set
	 */
	public void setmJobID(String mJobID) {
		this.mJobID = mJobID;
	}
	/**
	 * @return the mTyreID
	 */
	public String getmTyreID() {
		return mTyreID;
	}
	/**
	 * @param mTyreID the mTyreID to set
	 */
	public void setmTyreID(String mTyreID) {
		this.mTyreID = mTyreID;
	}
	/**
	 * @return the mKeyName
	 */
	public String getmKeyName() {
		return mKeyName;
	}
	/**
	 * @param mKeyName the mKeyName to set
	 */
	public void setmKeyName(String mKeyName) {
		this.mKeyName = mKeyName;
	}
	/**
	 * @return the mOldValue
	 */
	public String getmOldValue() {
		return mOldValue;
	}
	/**
	 * @param mOldValue the mOldValue to set
	 */
	public void setmOldValue(String mOldValue) {
		this.mOldValue = mOldValue;
	}
	/**
	 * @return the mNewValue
	 */
	public String getmNewValue() {
		return mNewValue;
	}
	/**
	 * @param mNewValue the mNewValue to set
	 */
	public void setmNewValue(String mNewValue) {
		this.mNewValue = mNewValue;
	}
	/*
 *  inserting data in Job Correction Object
 */
	public void jobCurrectionInsertion(String mJobID, String mTyreID,
			String mKeyName, String mOldValue, String mNewValue) {
		ContentValues contentValues = new ContentValues();
		contentValues.put("mJobID", mJobID);
		contentValues.put("mTyreID", mTyreID);
		contentValues.put("mKeyName", mKeyName);
		contentValues.put("mOldValue", mOldValue);
		contentValues.put("mNewValue", mNewValue);
	}
}