package com.goodyear.ejob.dbmodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Good Year
 * 
 * Purpose of this class is to capture the from elements of WOT Screen
 * and save in objects. Later on compare whenever required
 * 
 */
public class WOTFormElements implements Parcelable {
	private boolean mValveFitted;
	private boolean mHpValveCapFitted;
	private boolean mValveFixingSupport;
	private boolean mTireFill;
	private int mBalancing;
	private int mValveExtnFitted;
	private int mRepair;

	private String mNskValue;
	private String mPressureValue;

	/**
	 * @return the valveFitted
	 */
	public boolean isValveFitted() {
		return mValveFitted;
	}

	/**
	 * @param valveFitted the valveFitted to set
	 */
	public void setValveFitted(boolean valveFitted) {
		mValveFitted = valveFitted;
	}

	/**
	 * @return the hpValveCapFitted
	 */
	public boolean isHpValveCapFitted() {
		return mHpValveCapFitted;
	}

	/**
	 * @param hpValveCapFitted the hpValveCapFitted to set
	 */
	public void setHpValveCapFitted(boolean hpValveCapFitted) {
		mHpValveCapFitted = hpValveCapFitted;
	}

	/**
	 * @return the valveFixingSupport
	 */
	public boolean isValveFixingSupport() {
		return mValveFixingSupport;
	}

	/**
	 * @param valveFixingSupport the valveFixingSupport to set
	 */
	public void setValveFixingSupport(boolean valveFixingSupport) {
		mValveFixingSupport = valveFixingSupport;
	}

	/**
	 * @return the tireFill
	 */
	public boolean isTireFill() {
		return mTireFill;
	}

	/**
	 * @param tireFill the tireFill to set
	 */
	public void setTireFill(boolean tireFill) {
		mTireFill = tireFill;
	}

	/**
	 * @return the balancing
	 */
	public int getBalancing() {
		return mBalancing;
	}

	/**
	 * @param balancing the balancing to set
	 */
	public void setBalancing(int balancing) {
		mBalancing = balancing;
	}

	/**
	 * @return the valveExtnFitted
	 */
	public int getValveExtnFitted() {
		return mValveExtnFitted;
	}

	/**
	 * @param valveExtnFitted the valveExtnFitted to set
	 */
	public void setValveExtnFitted(int valveExtnFitted) {
		mValveExtnFitted = valveExtnFitted;
	}

	/**
	 * @return the repair
	 */
	public int getRepair() {
		return mRepair;
	}

	/**
	 * @param repair the repair to set
	 */
	public void setRepair(int repair) {
		mRepair = repair;
	}

	/**
	 * @return the nskValue
	 */
	public String getNskValue() {
		return mNskValue;
	}

	/**
	 * @param nskValue the nskValue to set
	 */
	public void setNskValue(String nskValue) {
		mNskValue = nskValue;
	}

	/**
	 * @return the pressureValue
	 */
	public String getPressureValue() {
		return mPressureValue;
	}

	/**
	 * @param pressureValue the pressureValue to set
	 */
	public void setPressureValue(String pressureValue) {
		mPressureValue = pressureValue;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof WOTFormElements)) {
			return false;
		}

		WOTFormElements that = (WOTFormElements) other;

		// Custom equality check here.
		return (this.mValveFitted == that.mValveFitted)
				&& (this.mHpValveCapFitted == that.mHpValveCapFitted)
				&& (this.mValveFixingSupport == that.mValveFixingSupport)
				&& (this.mTireFill == that.mTireFill)
				&& (this.mBalancing == that.mBalancing)
				&& (this.mValveExtnFitted == that.mValveExtnFitted)
				&& (this.mRepair == that.mRepair);
	}

	public WOTFormElements() {

	}

	public int describeContents() {
		return 0;
	}

	/** save object in parcel */
	public void writeToParcel(Parcel out, int flags) {
		out.writeByte((byte) (mValveFitted ? 1 : 0));
		out.writeByte((byte) (mHpValveCapFitted ? 1 : 0));
		out.writeByte((byte) (mValveFixingSupport ? 1 : 0));
		out.writeByte((byte) (mTireFill ? 1 : 0));
		out.writeInt(mBalancing);
		out.writeInt(mValveExtnFitted);
		out.writeInt(mRepair);
		out.writeString(mNskValue);
		out.writeString(mPressureValue);
	}

	public static final Parcelable.Creator<WOTFormElements> CREATOR = new Parcelable.Creator<WOTFormElements>() {
		public WOTFormElements createFromParcel(Parcel in) {
			return new WOTFormElements(in);
		}

		public WOTFormElements[] newArray(int size) {
			return new WOTFormElements[size];
		}
	};

	/** recreate object from parcel */
	private WOTFormElements(Parcel in) {
		mValveFitted = in.readByte() != 0;
		mHpValveCapFitted = in.readByte() != 0;
		mValveFixingSupport = in.readByte() != 0;
		mTireFill = in.readByte() != 0;
		mBalancing = in.readInt();
		mValveExtnFitted = in.readInt();
		mNskValue = in.readString();
		mPressureValue = in.readString();
	}

}
