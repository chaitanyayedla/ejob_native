package com.goodyear.ejob.dbmodel;

public class CasingRoute {

	private int casingRouteID;
	private int sapCode;
	private String casingRouteDesc;
	private String casingRouteextID;

	public int getCasingRouteID() {
		return casingRouteID;
	}

	public void setCasingRouteID(int casingRouteID) {
		this.casingRouteID = casingRouteID;
	}

	public int getSapCode() {
		return sapCode;
	}

	public void setSapCode(int sapCode) {
		this.sapCode = sapCode;
	}

	public String getCasingRouteDesc() {
		return casingRouteDesc;
	}

	public void setCasingRouteDesc(String casingRouteDesc) {
		this.casingRouteDesc = casingRouteDesc;
	}

	public String getCasingRouteextID() {
		return casingRouteextID;
	}

	public void setCasingRouteextID(String casingRouteextID) {
		this.casingRouteextID = casingRouteextID;
	}

}
