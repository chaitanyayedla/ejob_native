package com.goodyear.ejob.dbmodel;

public class AccountTable {

	private int accountTabID;
	private String accLogin;
	private String accPass;
	private String jobReffNO;
	private String passExpDate;
	private int passNotficationType;

	public int getAccountTabID() {
		return accountTabID;
	}

	public void setAccountTabID(int accountTabID) {
		this.accountTabID = accountTabID;
	}

	public String getAccLogin() {
		return accLogin;
	}

	public void setAccLogin(String accLogin) {
		this.accLogin = accLogin;
	}

	public String getAccPass() {
		return accPass;
	}

	public void setAccPass(String accPass) {
		this.accPass = accPass;
	}

	public String getJobReffNO() {
		return jobReffNO;
	}

	public void setJobReffNO(String jobReffNO) {
		this.jobReffNO = jobReffNO;
	}

	public String getPassExpDate() {
		return passExpDate;
	}

	public void setPassExpDate(String passExpDate) {
		this.passExpDate = passExpDate;
	}

	public int getPassNotficationType() {
		return passNotficationType;
	}

	public void setPassNotficationType(int passNotficationType) {
		this.passNotficationType = passNotficationType;
	}

}
