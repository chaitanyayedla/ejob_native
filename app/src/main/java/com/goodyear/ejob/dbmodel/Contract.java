package com.goodyear.ejob.dbmodel;

public class Contract {

	private int contractID;
	private int ContractType;
	private String ContractStartDate;
	private String ContractEndDate;
	private String SAPCustomerID;
	private String Address;
	private String PostCode;
	private String CustomerName;
	private String CountryCode;
	private String City;
	private int IsEU;
	private int Active;

	public int getContractID() {
		return contractID;
	}

	public void setContractID(int contractID) {
		this.contractID = contractID;
	}

	public int getContractType() {
		return ContractType;
	}

	public void setContractType(int contractType) {
		ContractType = contractType;
	}

	public String getContractStartDate() {
		return ContractStartDate;
	}

	public void setContractStartDate(String contractStartDate) {
		ContractStartDate = contractStartDate;
	}

	public String getContractEndDate() {
		return ContractEndDate;
	}

	public void setContractEndDate(String contractEndDate) {
		ContractEndDate = contractEndDate;
	}

	public String getSAPCustomerID() {
		return SAPCustomerID;
	}

	public void setSAPCustomerID(String sAPCustomerID) {
		SAPCustomerID = sAPCustomerID;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getPostCode() {
		return PostCode;
	}

	public void setPostCode(String postCode) {
		PostCode = postCode;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getCountryCode() {
		return CountryCode;
	}

	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public int getIsEU() {
		return IsEU;
	}

	public void setIsEU(int isEU) {
		IsEU = isEU;
	}

	public int getActive() {
		return Active;
	}

	public void setActive(int active) {
		Active = active;
	}

}
