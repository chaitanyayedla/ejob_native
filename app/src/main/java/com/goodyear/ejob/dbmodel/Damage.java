package com.goodyear.ejob.dbmodel;

public class Damage {

	private int damageID;
	private String ExternalID;
	private String DamageCode;
	private String DamageGroup;
	private String DamageName;
	private String RemovalReasonCode;
	private int Active;

	public int getDamageID() {
		return damageID;
	}

	public void setDamageID(int damageID) {
		this.damageID = damageID;
	}

	public String getExternalID() {
		return ExternalID;
	}

	public void setExternalID(String externalID) {
		ExternalID = externalID;
	}

	public String getDamageCode() {
		return DamageCode;
	}

	public void setDamageCode(String damageCode) {
		DamageCode = damageCode;
	}

	public String getDamageGroup() {
		return DamageGroup;
	}

	public void setDamageGroup(String damageGroup) {
		DamageGroup = damageGroup;
	}

	public String getDamageName() {
		return DamageName;
	}

	public void setDamageName(String damageName) {
		DamageName = damageName;
	}

	public String getRemovalReasonCode() {
		return RemovalReasonCode;
	}

	public void setRemovalReasonCode(String removalReasonCode) {
		RemovalReasonCode = removalReasonCode;
	}

	public int getActive() {
		return Active;
	}

	public void setActive(int active) {
		Active = active;
	}

}
