package com.goodyear.ejob.dbmodel;

import android.content.ContentValues;

public class Job {

	private int ID;
	private String mExternalID;
	private String mLicenseNumber;
	private int SAPContractNumber;
	private int VehicleID;
	private String Technitian;
	private int JobType;
	private int OdometerType;
	private String GPSLocation;
	private String Location;
	private String TimeFinished;
	private String MissingOdometerReasonID;
	private int AgreeStatus;
	private int VehicleUnattendedStatus;
	private String TimeOnSite;
	private String TimeStarted;
	private String TimeReceived;
	private String CallerPhone;
	private String CallerName;
	private String VendorCountryID;
	private int ThirtyMinutesRetorqueStatus;
	private String DefectAuthNumber;
	private String DriverName;
	private String ActionLineN;
	private String Hudometer;
	private String VendorDetail;
	private int VendorSAPCode;
	private String SignatureReasonID;
	private String StockLocation;
	private String VehicleODO;
	private String SignatureImage;
	private String Note;
	private String Status;
	private int jobRefNumber;
	private String JobValidityDate;
	private int TimeOffsetMinutes;
	private int JobRemovalReasonID;

	//CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
	//Last recorded odometer value
	private static String previousOdometerValue = "0";

	public static String getPreviousOdometerValue() {
		return previousOdometerValue;
	}

	public static void setPreviousOdometerValue(String previousOdometerValue) {
		Job.previousOdometerValue = previousOdometerValue;
	}

	//CR - 550 (some vehicles have different torque settings per axle)
	//Torque Settings per axle info
	private static String TorqueSettingsSame = "0";

	public static String getTorqueSettingsSame() {
		return TorqueSettingsSame;
	}

	public static void setTorqueSettingsSame(String torqueSettingsSame) {
		TorqueSettingsSame = torqueSettingsSame;
	}



	public void insertJobStatement(String mExternalID, String mLicenseNumber,
			int mSAPContractNumber, int VehicleID, String mTechnitian,
			int JobType, int OdometerType, String GPSLocation, String Location,
			String TimeFinished, String MissingOdometerReasonID,
			int AgreeStatus, int VehicleUnattendedStatus, String TimeOnSite,
			String TimeStarted, String TimeReceived, String CallerPhone,
			String CallerName, String VendorCountryID,
			int ThirtyMinutesRetorqueStatus, String DefectAuthNumber,
			String DriverName, String ActionLineN, String Hudometer,
			String VendorDetail, int VendorSAPCode, String SignatureReasonID,
			String StockLocation, String VehicleODO, String SignatureImage,
			String Note, String Status, int jobRefNumber,
			String JobValidityDate, int TimeOffsetMinutes,
			int JobRemovalReasonID) {

		ContentValues cv = new ContentValues();
		cv.put("ExternalID", mExternalID);
		cv.put("LicenseNumber", mLicenseNumber);
		cv.put("SAPContractNumber", SAPContractNumber);
		cv.put("VehicleID", VehicleID);
		cv.put("Technitian", Technitian);
		cv.put("JobType", JobType);
		cv.put("OdometerType", OdometerType);
		cv.put("GPSLocation", GPSLocation);
		cv.put("Location", Location);
		cv.put("TimeFinished", TimeFinished);
		cv.put("MissingOdometerReasonID", MissingOdometerReasonID);
		cv.put("AgreeStatus", AgreeStatus);
		cv.put("VehicleUnattendedStatus", VehicleUnattendedStatus);
		cv.put("TimeOnSite", TimeOnSite);
		cv.put("TimeStarted", TimeStarted);
		cv.put("TimeReceived", TimeReceived);
		cv.put("CallerPhone", CallerPhone);
		cv.put("CallerName", CallerName);
		cv.put("VendorCountryID", VendorCountryID);
		cv.put("ThirtyMinutesRetorqueStatus", ThirtyMinutesRetorqueStatus);
		cv.put("DefectAuthNumber", DefectAuthNumber);
		cv.put("DriverName", DriverName);
		cv.put("ActionLineN", ActionLineN);
		cv.put("Hudometer", Hudometer);
		cv.put("VendorDetail", VendorDetail);
		cv.put("VendorSAPCode", VendorSAPCode);
		cv.put("SignatureReasonID", SignatureReasonID);
		cv.put("StockLocation", StockLocation);
		cv.put("VehicleODO", VehicleODO);
		cv.put("SignatureImage", SignatureImage);
		cv.put("Note", Note);
		cv.put("Status", Status);
		cv.put("jobRefNumber", jobRefNumber);
		cv.put("JobValidityDate", JobValidityDate);
		cv.put("TimeOffsetMinutes", TimeOffsetMinutes);
		cv.put("JobRemovalReasonID", JobRemovalReasonID);
		// mDb.insert("Job", null, cv);
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getExternalID() {
		return mExternalID;
	}

	public void setExternalID(String externalID) {
		mExternalID = externalID;
	}

	public String getLicenseNumber() {
		return mLicenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		mLicenseNumber = licenseNumber;
	}

	public int getSAPContractNumber() {
		return SAPContractNumber;
	}

	public void setSAPContractNumber(int sAPContractNumber) {
		SAPContractNumber = sAPContractNumber;
	}

	public int getVehicleID() {
		return VehicleID;
	}

	public void setVehicleID(int vehicleID) {
		VehicleID = vehicleID;
	}

	public String getTechnitian() {
		return Technitian;
	}

	public void setTechnitian(String technitian) {
		Technitian = technitian;
	}

	public int getJobType() {
		return JobType;
	}

	public void setJobType(int jobType) {
		JobType = jobType;
	}

	public int getOdometerType() {
		return OdometerType;
	}

	public void setOdometerType(int odometerType) {
		OdometerType = odometerType;
	}

	public String getGPSLocation() {
		return GPSLocation;
	}

	public void setGPSLocation(String gPSLocation) {
		GPSLocation = gPSLocation;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public String getTimeFinished() {
		return TimeFinished;
	}

	public void setTimeFinished(String timeFinished) {
		TimeFinished = timeFinished;
	}

	public String getMissingOdometerReasonID() {
		return MissingOdometerReasonID;
	}

	public void setMissingOdometerReasonID(String missingOdometerReasonID) {
		MissingOdometerReasonID = missingOdometerReasonID;
	}

	public int getAgreeStatus() {
		return AgreeStatus;
	}

	public void setAgreeStatus(int agreeStatus) {
		AgreeStatus = agreeStatus;
	}

	public int getVehicleUnattendedStatus() {
		return VehicleUnattendedStatus;
	}

	public void setVehicleUnattendedStatus(int vehicleUnattendedStatus) {
		VehicleUnattendedStatus = vehicleUnattendedStatus;
	}

	public String getTimeOnSite() {
		return TimeOnSite;
	}

	public void setTimeOnSite(String timeOnSite) {
		TimeOnSite = timeOnSite;
	}

	public String getTimeStarted() {
		return TimeStarted;
	}

	public void setTimeStarted(String timeStarted) {
		TimeStarted = timeStarted;
	}

	public String getTimeReceived() {
		return TimeReceived;
	}

	public void setTimeReceived(String timeReceived) {
		TimeReceived = timeReceived;
	}

	public String getCallerPhone() {
		return CallerPhone;
	}

	public void setCallerPhone(String callerPhone) {
		CallerPhone = callerPhone;
	}

	public String getCallerName() {
		return CallerName;
	}

	public void setCallerName(String callerName) {
		CallerName = callerName;
	}

	public String getVendorCountryID() {
		return VendorCountryID;
	}

	public void setVendorCountryID(String vendorCountryID) {
		VendorCountryID = vendorCountryID;
	}

	public int getThirtyMinutesRetorqueStatus() {
		return ThirtyMinutesRetorqueStatus;
	}

	public void setThirtyMinutesRetorqueStatus(int thirtyMinutesRetorqueStatus) {
		ThirtyMinutesRetorqueStatus = thirtyMinutesRetorqueStatus;
	}

	public String getDefectAuthNumber() {
		return DefectAuthNumber;
	}

	public void setDefectAuthNumber(String defectAuthNumber) {
		DefectAuthNumber = defectAuthNumber;
	}

	public String getDriverName() {
		return DriverName;
	}

	public void setDriverName(String driverName) {
		DriverName = driverName;
	}

	public String getActionLineN() {
		return ActionLineN;
	}

	public void setActionLineN(String actionLineN) {
		ActionLineN = actionLineN;
	}

	public String getHudometer() {
		return Hudometer;
	}

	public void setHudometer(String hudometer) {
		Hudometer = hudometer;
	}

	public String getVendorDetail() {
		return VendorDetail;
	}

	public void setVendorDetail(String vendorDetail) {
		VendorDetail = vendorDetail;
	}

	public int getVendorSAPCode() {
		return VendorSAPCode;
	}

	public void setVendorSAPCode(int vendorSAPCode) {
		VendorSAPCode = vendorSAPCode;
	}

	public String getSignatureReasonID() {
		return SignatureReasonID;
	}

	public void setSignatureReasonID(String signatureReasonID) {
		SignatureReasonID = signatureReasonID;
	}

	public String getStockLocation() {
		return StockLocation;
	}

	public void setStockLocation(String stockLocation) {
		StockLocation = stockLocation;
	}

	public String getVehicleODO() {
		return VehicleODO;
	}

	public void setVehicleODO(String vehicleODO) {
		VehicleODO = vehicleODO;
	}

	public String getSignatureImage() {
		return SignatureImage;
	}

	public void setSignatureImage(String signatureImage) {
		SignatureImage = signatureImage;
	}

	public String getNote() {
		return Note;
	}

	public void setNote(String note) {
		Note = note;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public int getJobRefNumber() {
		return jobRefNumber;
	}

	public void setJobRefNumber(int jobRefNumber) {
		this.jobRefNumber = jobRefNumber;
	}

	public String getJobValidityDate() {
		return JobValidityDate;
	}

	public void setJobValidityDate(String jobValidityDate) {
		JobValidityDate = jobValidityDate;
	}

	public int getTimeOffsetMinutes() {
		return TimeOffsetMinutes;
	}

	public void setTimeOffsetMinutes(int timeOffsetMinutes) {
		TimeOffsetMinutes = timeOffsetMinutes;
	}

	public int getJobRemovalReasonID() {
		return JobRemovalReasonID;
	}

	public void setJobRemovalReasonID(int jobRemovalReasonID) {
		JobRemovalReasonID = jobRemovalReasonID;
	}

}
