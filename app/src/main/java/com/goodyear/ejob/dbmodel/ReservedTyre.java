package com.goodyear.ejob.dbmodel;


public class ReservedTyre {
	private int ID;
	private String JobID;
	private String TyreID;
	private String TyreSerialNo;

	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}

	/**
	 * @return the jobID
	 */
	public String getJobID() {
		return JobID;
	}

	/**
	 * @param jobID the jobID to set
	 */
	public void setJobID(String jobID) {
		JobID = jobID;
	}

	/**
	 * @return the tyreID
	 */
	public String getTyreID() {
		return TyreID;
	}

	/**
	 * @param tyreID the tyreID to set
	 */
	public void setTyreID(String tyreID) {
		TyreID = tyreID;
	}
	
	/**
	 * @return the tyreSerialNo
	 */
	public String getTyreSerialNo() {
		return TyreSerialNo;
	}

	/**
	 * @param tyreSerialNo the tyreSerialNo to set
	 */
	public void setTyreSerialNo(String tyreSerialNo) {
		TyreSerialNo = tyreSerialNo;
	}

	@Override
	public boolean equals(Object other) {
		
		if (!(other instanceof ReservedTyre)) {
			return false;
		}

		ReservedTyre that = (ReservedTyre) other;

		// Custom equality check here.
		return (this.JobID == that.JobID)
				&& (this.TyreID == that.TyreID);
	}
	
	@Override
	public String toString() {
		return TyreSerialNo;
	}

}
