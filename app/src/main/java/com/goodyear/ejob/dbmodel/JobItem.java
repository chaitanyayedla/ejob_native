package com.goodyear.ejob.dbmodel;

import android.content.ContentValues;
import android.util.Log;

public class JobItem {

    public boolean InsertJobItem(String JobID, int SAPCodeTiID,
                                 String ExternalID, int ActionType, int MinNSK, String TyreID,
                                 String RemovalReasonID, String DamageID, String CasingRouteCode,
                                 String Regrooved, int TorqueSetting, String Note, int TreadTepth,
                                 int Pressure, int GrooveNumber, int ReGrooveNumber,
                                 int ServiceCount, int RegrooveType, int NSK1Before, int NSK1After,
                                 int ServiceID, int AxleNumber, int AxleServiceType,
                                 String WorkOrderNumber, String RepairCompany, int NSK2Before,
                                 int NSK2After, int NSK3Before, int NSK3After, int SwapType,
                                 int Sequence, int RecInflactionOriginal,
                                 int RecInflactionDestination, String SWAPOriginalPosition,
                                 String OperationID, int RimType, String visualDisplayID,
                                 String temperature,
                                 String RectPressure) {
        try {
            ContentValues cv = new ContentValues();
            cv.put("JobID", JobID);
            cv.put("SAPCodeTiID", SAPCodeTiID);
            cv.put("ExternalID", ExternalID);
            cv.put("ActionType", ActionType);
            cv.put("MinNSK", MinNSK);
            cv.put("TyreID", TyreID);
            cv.put("RemovalReasonID", RemovalReasonID);
            cv.put("DamageID", DamageID);
            cv.put("CasingRouteCode", CasingRouteCode);
            cv.put("Regrooved", Regrooved);
            cv.put("TorqueSetting", TorqueSetting);
            cv.put("Note", Note);
            cv.put("TreadTepth", TreadTepth);
            cv.put("Pressure", Pressure);
            cv.put("GrooveNumber", GrooveNumber);
            cv.put("ReGrooveNumber", ReGrooveNumber);
            cv.put("ServiceCount", ServiceCount);
            cv.put("RegrooveType", RegrooveType);
            cv.put("NSK1Before", NSK1Before);
            cv.put("NSK1After", NSK1After);
            cv.put("ServiceID", ServiceID);
            cv.put("AxleNumber", AxleNumber);
            cv.put("AxleServiceType", AxleServiceType);
            cv.put("WorkOrderNumber", WorkOrderNumber);
            cv.put("RepairCompany", RepairCompany);
            cv.put("NSK2Before", NSK2Before);
            cv.put("NSK2After", NSK2After);
            cv.put("NSK3Before", NSK3Before);
            cv.put("NSK3After", NSK3After);
            cv.put("SwapType", SwapType);
            cv.put("Sequence", Sequence);
            cv.put("RecInflactionOriginal", RecInflactionOriginal);
            cv.put("RecInflactionDestination", RecInflactionDestination);
            cv.put("SWAPOriginalPosition", SWAPOriginalPosition);
            cv.put("OperationID", OperationID);
            cv.put("RimType", RimType);
            cv.put("VisualCommentsID", visualDisplayID);
            cv.put("Temperature", temperature);
            cv.put("RectPressure", RectPressure);

            // mDb.insert("JobItem", null, cv);

            Log.d("SaveJob", "Data Inserted in JobItem Table");
            return true;

        } catch (Exception ex) {
            Log.d("SaveJob", ex.toString());
            return false;
        }
    }
}
