package com.goodyear.ejob.dbmodel;

public class ContractTyrePolicy {

	private int ID;
	private String ExternalID;
	private int SAPContractNumber;
	private int BkreakdownCoverage;
	private int Balancing;
	private int GeometryCheck;
	private int GeometryCorrection;
	private int PolicyOnHPValveCap;
	private int PolicyOnValveExtension;
	private int PressureCheck;
	private int PressureCorrection;
	private int Retorquing;
	private int ValveFitting;

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getExternalID() {
		return ExternalID;
	}

	public void setExternalID(String externalID) {
		ExternalID = externalID;
	}

	public int getSAPContractNumber() {
		return SAPContractNumber;
	}

	public void setSAPContractNumber(int sAPContractNumber) {
		SAPContractNumber = sAPContractNumber;
	}

	public int getBkreakdownCoverage() {
		return BkreakdownCoverage;
	}

	public void setBkreakdownCoverage(int bkreakdownCoverage) {
		BkreakdownCoverage = bkreakdownCoverage;
	}

	public int getBalancing() {
		return Balancing;
	}

	public void setBalancing(int balancing) {
		Balancing = balancing;
	}

	public int getGeometryCheck() {
		return GeometryCheck;
	}

	public void setGeometryCheck(int geometryCheck) {
		GeometryCheck = geometryCheck;
	}

	public int getGeometryCorrection() {
		return GeometryCorrection;
	}

	public void setGeometryCorrection(int geometryCorrection) {
		GeometryCorrection = geometryCorrection;
	}

	public int getPolicyOnHPValveCap() {
		return PolicyOnHPValveCap;
	}

	public void setPolicyOnHPValveCap(int policyOnHPValveCap) {
		PolicyOnHPValveCap = policyOnHPValveCap;
	}

	public int getPolicyOnValveExtension() {
		return PolicyOnValveExtension;
	}

	public void setPolicyOnValveExtension(int policyOnValveExtension) {
		PolicyOnValveExtension = policyOnValveExtension;
	}

	public int getPressureCheck() {
		return PressureCheck;
	}

	public void setPressureCheck(int pressureCheck) {
		PressureCheck = pressureCheck;
	}

	public int getPressureCorrection() {
		return PressureCorrection;
	}

	public void setPressureCorrection(int pressureCorrection) {
		PressureCorrection = pressureCorrection;
	}

	public int getRetorquing() {
		return Retorquing;
	}

	public void setRetorquing(int retorquing) {
		Retorquing = retorquing;
	}

	public int getValveFitting() {
		return ValveFitting;
	}

	public void setValveFitting(int valveFitting) {
		ValveFitting = valveFitting;
	}

}
