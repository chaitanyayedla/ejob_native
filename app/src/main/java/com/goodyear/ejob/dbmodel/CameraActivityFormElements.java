
package com.goodyear.ejob.dbmodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *
 */
public class CameraActivityFormElements implements Parcelable {

	private String damageType;
	private String damageArea;
	private String notes1;
	private String notes2;
	private String notes3;
	private boolean hasImage1 = false;
	private boolean hasImage2 = false;
	private boolean hasImage3 = false;
	
	
	
	/**
	 * @return the hasImage1
	 */
	public boolean isHasImage1() {
		return hasImage1;
	}
	/**
	 * @param hasImage1 the hasImage1 to set
	 */
	public void setHasImage1(boolean hasImage1) {
		this.hasImage1 = hasImage1;
	}
	/**
	 * @return the hasImage2
	 */
	public boolean isHasImage2() {
		return hasImage2;
	}
	/**
	 * @param hasImage2 the hasImage2 to set
	 */
	public void setHasImage2(boolean hasImage2) {
		this.hasImage2 = hasImage2;
	}
	/**
	 * @return the hasImage3
	 */
	public boolean isHasImage3() {
		return hasImage3;
	}
	/**
	 * @param hasImage3 the hasImage3 to set
	 */
	public void setHasImage3(boolean hasImage3) {
		this.hasImage3 = hasImage3;
	}
	/**
	 * @return the damageType
	 */
	public String getDamageType() {
		if(null == damageType) {
			return "";
		}
		return damageType.toString();
	}
	/**
	 * @param damageType the damageType to set
	 */
	public void setDamageType(String damageType) {
		this.damageType = damageType;
	}
	/**
	 * @return the damageArea
	 */
	public String getDamageArea() {
		if(null == damageArea) {
			return "";
		}
		return damageArea.toString();
	}
	/**
	 * @param damageArea the damageArea to set
	 */
	public void setDamageArea(String damageArea) {
		this.damageArea = damageArea;
	}
	/**
	 * @return the notes1
	 */
	public String getNotes1() {
		return notes1;
	}
	/**
	 * @param notes1 the notes1 to set
	 */
	public void setNotes1(String notes1) {
		this.notes1 = notes1;
	}
	/**
	 * @return the notes2
	 */
	public String getNotes2() {
		return notes2;
	}
	/**
	 * @param notes2 the notes2 to set
	 */
	public void setNotes2(String notes2) {
		this.notes2 = notes2;
	}
	/**
	 * @return the notes3
	 */
	public String getNotes3() {
		return notes3;
	}
	/**
	 * @param notes3 the notes3 to set
	 */
	public void setNotes3(String notes3) {
		this.notes3 = notes3;
	}
	
	public CameraActivityFormElements() {
		
	}
	
	public int describeContents() {
		return 0;
	}
	
	public void writeToParcel(Parcel out, int flags) {
		out.writeByte((byte) (hasImage1 ? 1 : 0));
		out.writeByte((byte) (hasImage2 ? 1 : 0));
		out.writeByte((byte) (hasImage3 ? 1 : 0));
		out.writeString(notes1);
		out.writeString(notes2);
		out.writeString(notes3);
		out.writeString(damageArea);
		out.writeString(damageType);
	}
	
	public static final Parcelable.Creator<CameraActivityFormElements> CREATOR = new Parcelable.Creator<CameraActivityFormElements>() {
		public CameraActivityFormElements createFromParcel(Parcel in) {
			return new CameraActivityFormElements(in);
		}

		public CameraActivityFormElements[] newArray(int size) {
			return new CameraActivityFormElements[size];
		}
	};
	
	private CameraActivityFormElements(Parcel in) {
		hasImage1 = in.readByte() != 0;
		hasImage2 = in.readByte() != 0;
		hasImage3 = in.readByte() != 0;
		notes1 = in.readString();
		notes2 = in.readString();
		notes3 = in.readString();
		damageArea = in.readString();
		damageType = in.readString();
	}
}
