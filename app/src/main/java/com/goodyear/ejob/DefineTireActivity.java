/**
 * 
 */
package com.goodyear.ejob;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.goodyear.ejob.authenticator.Authentication;
import com.goodyear.ejob.blutooth.BluetoothService;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.job.operation.helpers.PressureCheckListener;
import com.goodyear.ejob.sync.Constant;
import com.goodyear.ejob.ui.jobcreation.AutoSwapImpl;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.TireDesignDetails;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.TyreFormElements;
import com.goodyear.ejob.util.TyreState;

/**
 * @author amitkumar.h
 * @version 1.0
 * Class provides mechanism to load design page as per the selected
 * tire(maintained, non-maintained, empty, spare etc). Moreover it
 * handles the data updates in different tables(JobItem, Tire,
 * jobCorrection etc) as per the user-entered tire-data during
 * DefineTire Operation.
 */
public class DefineTireActivity extends Activity implements InactivityHandler {

	/**
	 * Swipper Object
	 */
	private Swiper mSwipeDetector;
	/**
	 * ArrayList for Brands
	 */
	private ArrayList<String> mBrandArrayList;
	/**
	 * ArrayList for BrandCodes
	 */
	private ArrayList<String> mBrandCodeList;
	/**
	 * Selected Brand Name
	 */
	private String mSelectedBrandName;
	/**
	 * Selected Tyre size
	 */
	private String mSelectedtyreSize;
	/**
	 * Selected Tyre ASP
	 */
	private String mSelectedTyreTASP;
	/**
	 * Selected Tyre RIM
	 */
	private String mSelectedtyreTRIM;
	/**
	 * Selected Tyre Design
	 */
	private String mSelectedTyreDesign;
	/**
	 * Selected tyre details desing
	 */
	private String mSelectedTyreDetailedDesign;
	/**
	 * Size ArrayList
	 */
	private ArrayList<String> mSizeArrayList;
	/**
	 * RIM ArrayList
	 */
	private ArrayList<String> mTyreTRIMArrayList;
	/**
	 * ASP ArrayList
	 */
	private ArrayList<String> mTyreTASPArrayList;
	/**
	 * Spinner reference for Brand
	 */
	private Spinner mBrandSpinner;
	/**
	 * Spinner reference for Size
	 */
	private Spinner mSizeSpinner;
	/**
	 * Spinner reference for RIM
	 */
	private Spinner mTyreTRIMSpinner;
	/**
	 * Spinner reference for ASP
	 */
	private Spinner mTyreTASPSpinner;
	/**
	 * Spinner reference for TyreDesign
	 */
	private Spinner mTyreDesignSpinner;
	/**
	 * Spinner reference for Tyre FullDetails
	 */
	private Spinner mTyreFullDetailSpinner;
	/**
	 * ArrayAdapter for Size
	 */
	private ArrayAdapter<String> mSizeDataAdapter;
	/**
	 * ArrayAdapter for RIM
	 */
	private ArrayAdapter<String> mRimDataAdapter;
	/**
	 * ArrayAdapter for ASP
	 */
	private ArrayAdapter<String> mASPDataAdapter;
	/**
	 * ArrayAdapter for Design
	 */
	private ArrayAdapter<String> mDesignDataAdapter;
	/**
	 * ArrayAdapter for Full Details tyre
	 */
	private ArrayAdapter<String> mFullDesignDataAdapter;
	/**
	 * TextView reference for value tyre position
	 */
	private TextView mValue_tyrePosition;
	/**
	 * TextView reference for value tyre serial number
	 */
	private TextView mValue_tyreSerialNumber;
	/**
	 * ArrayList for Design
	 */
	private ArrayList<String> mDesignArrayList;
	/**
	 * ArrayList for FullDesign Details
	 */
	private ArrayList<String> mFullDesignArrayList;
	/**
	 * EditText reference for valueNSK1
	 */
	private EditText mValueNSK1;
	/**
	 * EditText reference for valueNSK2
	 */
	private EditText mValueNSK2;
	/**
	 * EditText reference for valueNSK3
	 */
	private EditText mValueNSK3;
	/**
	 * ScrollView reference for MainScrollBar
	 */
	private ScrollView mMainScrollBar;
	/**
	 * TextView reference for label NSK
	 */
	private TextView mLabelNSK;
	/**
	 * TextView reference for Dimension label
	 */
	private TextView mDimensionLabel;
	/**
	 * TextView reference for Size label
	 */
	private TextView mSizeLabel;
	/**
	 * TextView reference for ASP label
	 */
	private TextView mASPLabel;
	/**
	 * TextView reference for Rim label
	 */
	private TextView mRimLabel;
	/**
	 * TextView reference for Design label
	 */
	private TextView mDesignLabel;
	/**
	 * TextView reference for Type label
	 */
	private TextView mTypeLabel;
	/**
	 * TextView reference for label serial no
	 */
	private TextView mLbl_serialNo_swap;
	/**
	 * BluetoothService Object
	 */
	private BluetoothService mBTService;
	/**
	 * BroadcastReceiver Object
	 */
	private BroadcastReceiver mReceiver;
	/**
	 * mNSKCounter
	 */
	private int mNSKCounter = 1;
	/**
	 * TextView reference for DesingType
	 */
	private TextView mDesignTypeLabel;
	/**
	 * TextView reference for Selected tyre
	 */
	private Tyre mSelectedTire;
	/**
	 * Switch object from Rim
	 */
	private Switch mRimValue;
	/**
	 * TestAdapter Object
	 */
	private DatabaseAdapter mDbHelper;
	/**
	 * Final variable for NOTES_INTENT
	 */
	public static final int NOTES_INTENT = 104;
	/**
	 * Final variable for WOT_INTENT
	 */
	public static final int WOT_INTENT = 104;
	/**
	 * Final variable for DECIMAL_LENGTH
	 */
	public static final int DECIMAL_LENGTH = 8;
	/**
	 * boolean variable for swiped
	 */
	private boolean mSwiped = false;
	/**
	 * TextView reference for PressureLabel
	 */
	private TextView mPressureLabel;
	/**
	 * TextView reference for PressureUnit
	 */
	private TextView mPressureUnit;
	/**
	 * EditText reference for SetPressure
	 */
	private EditText mSetPressure;
	/**
	 * boolean variable for Pressure measurement in PSI
	 */
	private boolean mIsSettingsPSI;
	/**
	 * SharedPreferences object
	 */
	private SharedPreferences mPreferences;
	/**
	 * Serial Number from previousIntent
	 */
	private String mSerialNoFromIntent;
	/**
	 * Fitter Type
	 */
	private String mFitter_type;
	/**
	 * Recommended Pressure
	 */
	private float mRecommendedPressure;
	/**
	 * Axle position
	 */
	private int mAxlePosition;
	/**
	 * Final Converted Pressure
	 */
	private String mFinalConvertedPressure;
	/**
	 * NSK Default Value
	 */
	// private int mNSKDefaultValue;
	/**
	 * Dismounted Tyre
	 */
	// private Tyre mDismountedTyre;
	/**
	 * Pressure Required
	 */
	private String mPressureRequired;
	/**
	 * Saved NSK value 1
	 */
	// private String mSavedNSK1;
	/**
	 * Saved NSK value 2
	 */
	// private String mSavedNSK2;
	/**
	 * Saved NSK value 3
	 */
	// private String mSavedNSK3;
	/**
	 * Bundle Instance
	 */
	// private Bundle mInstanceState;
	/**
	 * NSK23 set flag
	 */
	// private int mNSK23SetFlag;

	/**
	 * NSK Count
	 */
	// private int mGetnskCount;
	/**
	 * Selected SAP MaterialCode
	 */
	private String mSelectedSAPMaterialCodeID;
	/**
	 * Field in Correct
	 */
	private String fieldIncorrect;
	/**
	 * boolean variable for size Spinner Editable
	 */
	private boolean mIsSizeSpinnerEditable = true;
	/**
	 * Is Vehicle allow Differ sizes tire
	 */
	private boolean isDifferntSizeAllow = false;
	/**
	 * Validation Message for complete operations
	 */
	private String mDesignDetailRequired = "Design Details Required";

	/**
	 * Default value for Spinner
	 */
	private String mDefaultValueForSpinner = "Please Select";
	/**
	 * Last Spinner Selection Index, This variable using Spinner manipulation
	 * while orientation
	 */
	private int mLastSpinnerSelection = 0;
	/**
	 * IsOrientationChagned, This variable using Spinner manipulation while
	 * orientation
	 */
	private boolean mIsOrientationChanged = false;

	/**
	 * Context of the application
	 */
	private Context mContext;

	/**
	 * Axle pressure
	 */
	public static final String AXLEPRESSURE = "axlepressure";

	/**
	 * Axle position
	 */
	public static final String AXLEPRESSURE_POS = "axlepos";
	private static final int DEFINE_TIRE_INTENT_ACTIVITY = 100;
	// For Auto SWAP
	private String mBrandForAutoSwap;
	private String mSerialNumberForAutoSwap;
	private String mSizeForAutoSwap;
	private String mRimForAutoSwap;
	private String mAspForAutoSwap;
	private String mDesignForAutoSwap;
	private String mDesignDetailsForAutoSwap;
	private String mNskForAutoSwap;
	private String mNsk2ForAutoSwap;
	private String mNsk3ForAutoSwap;
	private TyreFormElements mFormElements;
	String strbluetoothconnectionfalied;
	private String mCancelJobLabel;
	private String mNOLabel;
	private String mYESLabel;
	/**
	 * String message from DB for Please Select Brand
	 */
	private String mPleaseSelectBrand = "";
	private TextView mJobDetailsLabel;
	private TextView mBrandLabel;

	//User Trace logs trace tag
	private static final String TRACE_TAG = "Define Tire";

	private TextView mLabelSerialAndBrandHeading;
	private TextView mLabelTireDetailHeading;
	private TextView mLabelJobDetailHeading;

	/**
	 * Switch reference
	 */
	private Switch mRIMValue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_define_tire);
		//User Trace logs
		LogUtil.TraceInfo(TRACE_TAG, "none", "OP", true, false, true);
		mContext = DefineTireActivity.this;
		mPreferences = getSharedPreferences(Constants.GOODYEAR_CONF, 0);
		if (mPreferences.getString(Constants.PRESSUREUNIT, "").equals(
				Constants.PSI_PRESSURE_UNIT)) {
			mIsSettingsPSI = true;
		}
		mSerialNoFromIntent = getIntent().getExtras().getString("SERIAL_NO");
		// BUG ::: 652 :: N/R should be allowed for SAP
		if(Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP)){
			mSerialNoFromIntent = Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP;
		}
		if(Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP)){
			mSerialNoFromIntent = Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP;
		}
		mRecommendedPressure = getIntent().getExtras().getFloat(
				VehicleSkeletonFragment.mAXLEPRESSURE);
		mAxlePosition = getIntent().getExtras().getInt(
				VehicleSkeletonFragment.mAXLEPRESSURE_POS);
		mSelectedTire = Constants.SELECTED_TYRE;
		mDbHelper = new DatabaseAdapter(this);
		mDesignArrayList = new ArrayList<String>();
		mFullDesignArrayList = new ArrayList<String>();
		mBrandArrayList = new ArrayList<String>();
		mBrandCodeList = new ArrayList<String>();
		mSizeArrayList = new ArrayList<String>();
		mTyreTRIMArrayList = new ArrayList<String>();
		mTyreTASPArrayList = new ArrayList<String>();
		mLbl_serialNo_swap = (TextView) findViewById(R.id.lbl_serialNo_swap);
		mValue_tyreSerialNumber = (TextView) findViewById(R.id.value_serialNo);
		mValue_tyrePosition = (TextView) findViewById(R.id.value_wp);
		mBrandSpinner = (Spinner) findViewById(R.id.spinner_brand);
		mSizeSpinner = (Spinner) findViewById(R.id.spinner_dimenssionSize);
		mTyreTASPSpinner = (Spinner) findViewById(R.id.value_dimenssionASP);
		mTyreTRIMSpinner = (Spinner) findViewById(R.id.value_dimenssionRIM);
		mTyreDesignSpinner = (Spinner) findViewById(R.id.spinner_make);
		// mTyreDesignSpinner.setEnabled(false);
		mTyreFullDetailSpinner = (Spinner) findViewById(R.id.spinner_type);
		// mTyreFullDetailSpinner.setEnabled(false);
		mValueNSK1 = (EditText) findViewById(R.id.value_NSK1);
		mValueNSK2 = (EditText) findViewById(R.id.value_NSK2);
		mValueNSK3 = (EditText) findViewById(R.id.value_NSK3);
		mLabelNSK = (TextView) findViewById(R.id.lbl_NSK);
		mDimensionLabel = (TextView) findViewById(R.id.lbl_dimenssion);
		mSizeLabel = (TextView) findViewById(R.id.lbl_dimenssionSiz);
		mASPLabel = (TextView) findViewById(R.id.lbl_dimenssionASP);
		mRimLabel = (TextView) findViewById(R.id.lbl_dimenssionRIM);
		mDesignLabel = (TextView) findViewById(R.id.lbl_make);// lbl_type
		mTypeLabel = (TextView) findViewById(R.id.lbl_RimType);
		mDesignTypeLabel = (TextView) findViewById(R.id.lbl_type);
		TextView wheelPositionLabel = (TextView) findViewById(R.id.lbl_wp);
		wheelPositionLabel.setText(Constants.sLblWheelPos);
		mJobDetailsLabel = (TextView) findViewById(R.id.lbl_jobDetails);
		mBrandLabel = (TextView) findViewById(R.id.lbl_brand);
		// Setting filters and Listeners to NSK values
		mValueNSK1
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mValueNSK2
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mValueNSK3
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mValueNSK1.addTextChangedListener(new NSKOneBeforeChanged());
		mValueNSK2.addTextChangedListener(new nskTwoBeforeChanged());
		mValueNSK3.addTextChangedListener(new nskThreeBeforeChanged());

		mRimValue = (Switch) findViewById(R.id.value_RimType);
		mPressureLabel = (TextView) findViewById(R.id.lbl_pressure);
		mPressureUnit = (TextView) findViewById(R.id.lbl_pressureunit);
		mSetPressure = (EditText) findViewById(R.id.value_pressure);
		setValuetoPressueView();
		mSelectedTire = Constants.SELECTED_TYRE;
		mSelectedSAPMaterialCodeID = Constants.SELECTED_TYRE
				.getSapMaterialCodeID();
		mMainScrollBar = (ScrollView) findViewById(R.id.scroll_main);
		mSwipeDetector = new Swiper(getApplicationContext(), mMainScrollBar);
		mMainScrollBar.setOnTouchListener(mSwipeDetector);
		if (savedInstanceState != null) {
			mIsOrientationChanged = true;
			mNSKCounter = savedInstanceState.getInt("nsk_counter", 0);
			mSelectedBrandName = savedInstanceState.getString("BRAND");
			if (savedInstanceState.containsKey("SIZE")) {
				mSelectedtyreSize = savedInstanceState.getString("SIZE");
				mSelectedTyreTASP = savedInstanceState.getString("ASP");
				mSelectedtyreTRIM = savedInstanceState.getString("RIM");
				mSelectedTyreDesign = savedInstanceState.getString("DESIGN");
				mSelectedTyreDetailedDesign = savedInstanceState
						.getString("FULLDETAIL");
				mValueNSK1.setText(savedInstanceState.getString("SAVEDNSK1"));
				mValueNSK2.setText(savedInstanceState.getString("SAVEDNSK2"));
				mValueNSK3.setText(savedInstanceState.getString("SAVEDNSK3"));

			} else {
				mSelectedtyreSize = "";
			}
			checkLatestSpinnerSelectionBeforeOrientationChanges();
		} else {
			mSelectedtyreSize = mSelectedTire.getSize();
			mSelectedTyreTASP = mSelectedTire.getAsp();
			mSelectedtyreTRIM = mSelectedTire.getRim();
		}

		TireDesignDetails detail = CommonUtils
				.getDetailsFromTyreInSameAxle(mSelectedTire);
		if (null != detail) {
			mSelectedtyreSize = detail.getSize();
			mSelectedTyreTASP = detail.getAsp();
			mSelectedtyreTRIM = detail.getRim();
			mIsSizeSpinnerEditable = false;
		}
		if (!mIsSizeSpinnerEditable) {
			loadSizeDetailsFromAxle();
		}
		if (Constants.contract.getIsDIffSizeAllowedForVehicle().equals("true")) {
			isDifferntSizeAllow = true;
			mSizeSpinner.setEnabled(true);
			mTyreTASPSpinner.setEnabled(true);
			mTyreTRIMSpinner.setEnabled(true);
		}
		mLabelSerialAndBrandHeading = (TextView) findViewById(R.id.lbl_serialNoAndBrand);
		mLabelTireDetailHeading = (TextView) findViewById(R.id.lbl_tireDetails1);
		mLabelJobDetailHeading = (TextView) findViewById(R.id.lbl_jobDetails1);
		mRIMValue = (Switch) findViewById(R.id.value_RimType);
		loadLabelsFromDB();
		mValue_tyreSerialNumber.setText(mSerialNoFromIntent);
		mValue_tyrePosition.setText(mSelectedTire.getPosition());
		getFitterDetails();
		loadTyreDetailsFromOldTyre(0);
		loadRimType();
		restoreDialogState(savedInstanceState);

		// AutoSwap
		setDataForAutoSwap();
		captureInitialState(savedInstanceState);
	}
	/**
	 * Method loading and setting the size details from same axle for the
	 * corresponding selected tire
	 */
	private void loadSizeDetailsFromAxle() {
		loadSelectedSize();
		loadSelectedASP();
		loadSelectedRIM();
	}
	/**
	 * Method loading tire-size on UI element as per the selected tire
	 */
	private void loadSelectedSize() {
		mSizeArrayList.clear();
		mSizeArrayList.add(mSelectedtyreSize);
		mSizeDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mSizeArrayList);
		mSizeDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSizeSpinner.setAdapter(mSizeDataAdapter);
		if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED) {
			mSizeSpinner.setEnabled(true);
		}else{
			mSizeSpinner.setEnabled(false);
		}
	}
	/**
	 * Method loading tire-ASP on UI element as per the selected tire
	 */
	private void loadSelectedASP() {
		mTyreTASPArrayList.clear();
		mTyreTASPArrayList.add(mSelectedTyreTASP);
		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTASPSpinner.setAdapter(mASPDataAdapter);
		if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED) {
			mTyreTASPSpinner.setEnabled(true);
		}else{
			mTyreTASPSpinner.setEnabled(false);
		}
	}
	/**
	 * Method loading tire-RIM on UI element as per the selected tire
	 */
	private void loadSelectedRIM() {
		mTyreTRIMArrayList.clear();
		mTyreTRIMArrayList.add(mSelectedtyreTRIM);
		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTRIMSpinner.setAdapter(mRimDataAdapter);
		if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED) {
			mTyreTRIMSpinner.setEnabled(true);
		}else{
			mTyreTRIMSpinner.setEnabled(false);
		}
	}

	/**
	 * Method to load Rim type based on dismounted tire
	 */
	private void loadRimType() {
		if (Constants.SELECTED_TYRE.getRimType() != null) {
			if (Constants.SELECTED_TYRE.getRimType().equals("2")) {
				mRimValue.setChecked(true);
			} else {
				mRimValue.setChecked(false);
			}
		} else {
			mRimValue.setChecked(false);
		}
	}

	/**
	 * Method to get type of fitter(Internal/External)
	 */
	private void getFitterDetails() {
		Cursor fitterCursor = null;
		try {
			fitterCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				fitterCursor = mDbHelper
						.getFitterTypeOfVendor(Constants.VENDER_NAME);
			}
			if (CursorUtils.isValidCursor(fitterCursor)) {
				fitterCursor.moveToFirst();
				mFitter_type = fitterCursor.getString(fitterCursor
						.getColumnIndex("FitterType"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(fitterCursor);
			if (null != mDbHelper) {
				mDbHelper.close();
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		IntentFilter intentFilter = new IntentFilter("BLUETOOTH_SENDER");
		mReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.hasExtra("BT_DATA_NSK")) {
					String nsk_value = intent.getStringExtra("BT_DATA_NSK");
					if (mNSKCounter == 1) {
						mValueNSK1.setText(nsk_value);
						mValueNSK2.setText(nsk_value);
						mValueNSK3.setText(nsk_value);
						mNSKCounter = 2;
					} else if (mNSKCounter == 2) {
						mValueNSK2.setText(nsk_value);
						mNSKCounter = 3;
					} else if (mNSKCounter == 3) {
						mNSKCounter = 1;
						mValueNSK3.setText(nsk_value);
					}
				} else if (intent.hasExtra("BT_DATA_PRE")) {
					String pre_value = intent.getStringExtra("BT_DATA_PRE");
					if (!mBlockPressureFromBluetooth) {
						if (mIsSettingsPSI) {
							String barValue;
							// change PSI to BAR
							barValue = CommonUtils.getPressureValue(context,
									CommonUtils.parseFloat(pre_value));
							mSetPressure.setText(String.valueOf(barValue));
						} else {
							mSetPressure.setText(pre_value);
						}
					}
				} else if (intent.hasExtra("BT_CONN_STATUS")) {
					CommonUtils.notify(strbluetoothconnectionfalied, mContext);
				} else if (intent.hasExtra("BT_STATUS_IS_DISCONNECTED")) {
					boolean isDisconnected = intent.getBooleanExtra(
							"BT_STATUS_IS_DISCONNECTED", false);
					LogUtil.d("Bluetooth in my Operation",
							"Broadcast came to my oeration:: " + isDisconnected);
					mValueNSK1.setEnabled(isDisconnected);
					mValueNSK2.setEnabled(isDisconnected);
					mValueNSK3.setEnabled(isDisconnected);
					if (!mBlockPressureFromBluetooth)
						mSetPressure.setEnabled(isDisconnected);
				}
			}
		};
		try {
			registerReceiver(mReceiver, intentFilter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mBTService = new BluetoothService(this);
		initiateBT();
	}

	/**
	 * Method to initiate Bluetooth services
	 */
	private void initiateBT() {
		int status = mBTService.getPairedStatus();
		switch (status) {
		case 0:
			Toast.makeText(getApplicationContext(),
					Constants.sLblMultiplePaired, Toast.LENGTH_LONG).show();
			break;
		case 1:
			BTTask task = new BTTask();
			task.execute();
			break;
		case 2:
			Toast.makeText(getApplicationContext(),
					Constants.sLblPleasePairTLogik, Toast.LENGTH_LONG).show();
			break;
		case 3:
			Toast.makeText(getApplicationContext(),
					Constants.sLblNoPairedDevices, Toast.LENGTH_LONG).show();
			break;
		}
	}

	/**
	 * Bluetooth Connection establishment in background
	 */
	class BTTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... f_url) {
			try {
				mBTService.connectToDevice(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				if (mBTService.isConnected()) {
					mValueNSK1.setEnabled(false);
					mValueNSK2.setEnabled(false);
					mValueNSK3.setEnabled(false);
					if (!mBlockPressureFromBluetooth)
						mSetPressure.setEnabled(false);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		try {
			if (null != mReceiver) {
				unregisterReceiver(mReceiver);
			}
			if (null != mBTService) {
				mBTService.stop();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Method to retain data for screen orientation
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (null != mBrandSpinner && mBrandSpinner.getCount() != 0) {
			outState.putString("BRAND", mBrandSpinner.getSelectedItem()
					.toString());
		}
		if (null != mSizeSpinner && mSizeSpinner.getCount() != 0) {
			outState.putString("SIZE", mSizeSpinner.getSelectedItem()
					.toString());
		}
		if (null != mTyreTASPSpinner && mTyreTASPSpinner.getCount() != 0) {
			outState.putString("ASP", mTyreTASPSpinner.getSelectedItem()
					.toString());
		}
		if (null != mTyreTRIMSpinner && mTyreTRIMSpinner.getCount() != 0) {
			outState.putString("RIM", mTyreTRIMSpinner.getSelectedItem()
					.toString());
		}
		if (null != mTyreDesignSpinner && mTyreDesignSpinner.getCount() != 0) {
			outState.putString("DESIGN", mTyreDesignSpinner.getSelectedItem()
					.toString());
		}
		if (null != mTyreFullDetailSpinner
				&& mTyreFullDetailSpinner.getCount() != 0) {
			outState.putString("FULLDETAIL", mTyreFullDetailSpinner
					.getSelectedItem().toString());
		}
		outState.putString("SAVEDNSK1", (mValueNSK1.getText().toString()));
		outState.putString("SAVEDNSK2", (mValueNSK2.getText().toString()));
		outState.putString("SAVEDNSK3", (mValueNSK3.getText().toString()));
		outState.putInt("nsk_counter", mNSKCounter);
		saveDialogState(outState);
		saveInitialState(outState);
		super.onSaveInstanceState(outState);
	}

	/**
	 * Method to load labels based on translation
	 */
	private void loadLabelsFromDB() {
		try {
			if (null == mDbHelper) {
				return;
			}
			mDbHelper.open();
			setTitle(mDbHelper.getLabel(Constants.DEFINE_TIRE_LABEL));
			String labelFromDB = mDbHelper.getLabel(418);
			mLbl_serialNo_swap.setText(labelFromDB);

			strbluetoothconnectionfalied = mDbHelper
					.getLabel(Constants.BLUETOOTH_CONNECTION_FAILED);
			labelFromDB = mDbHelper.getLabel(Constants.NSK_LABEL);
			mLabelNSK.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.PRESSURE_LABEL);
			mPressureLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.DIMENSION_LABEL);
			mDimensionLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.ASP_LABEL);
			mASPLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.RIM_LABEL);
			mRimLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(68);
			mDesignLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.SIZE_LABEL);
			mSizeLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(137);
			mDesignTypeLabel.setText(labelFromDB);

			mPressureRequired = mDbHelper.getLabel(Constants.PRESSURE_REQUIRED);
			mDesignDetailRequired = mDbHelper.getLabel(82);

			labelFromDB = mDbHelper.getLabel(391);
			mTypeLabel.setText(labelFromDB);
			mCancelJobLabel = mDbHelper.getLabel(Constants.CONFIRM_BACK);
			mNOLabel = mDbHelper.getLabel(Constants.NO_LABEL);
			mYESLabel = mDbHelper.getLabel(Constants.YES_LABEL);

			mPleaseSelectBrand = mDbHelper.getLabel(Constants.SELECT_BRAND);
			
			labelFromDB = mDbHelper.getLabel(Constants.JOB_DETAILS);
			mJobDetailsLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.PLEASE_SELECT_LABEL);
			mDefaultValueForSpinner = labelFromDB;
			labelFromDB = mDbHelper.getLabel(Constants.BRAND_LABEL);
			mBrandLabel.setText(labelFromDB);
			String tireDetailsLabel = mDbHelper.getLabel(Constants.LABEL_TIRE_DETAILS);
			((TextView) findViewById(R.id.lbl_tireDetails))
					.setText(tireDetailsLabel);
			fieldIncorrect = "Please fill all the entries";
			// BT PROBE LABELS
			Constants.sLblMultiplePaired = mDbHelper.getLabel(Constants.MULTIPLE_PAIRED_DEVICES_FOUND);
			Constants.sLblPleasePairTLogik =  mDbHelper.getLabel(Constants.PLEASE_PAIR_TLOGIK);
			Constants.sLblNoPairedDevices =  mDbHelper.getLabel(Constants.NO_PAIRED_DEVICES);
	        Constants.sLblCheckPressureValue = mDbHelper.getLabel(Constants.CHECK_PRESSURE_LABEL);

			mRIMValue.setTextOff(mDbHelper.getLabel(Constants.STEEL));
			mRIMValue.setTextOn(mDbHelper.getLabel(Constants.ALLOY));

			if(mLabelSerialAndBrandHeading!=null)
			{
				mLabelSerialAndBrandHeading.setText(mDbHelper.getLabel(Constants.SERIAL_NUMBER) + " & " +mDbHelper.getLabel(Constants.BRAND_LABEL) );
			}
			if(mLabelTireDetailHeading!=null)
			{
				mLabelTireDetailHeading.setText(mDbHelper.getLabel(Constants.LABEL_TIRE_DETAILS));
			}
			if(mLabelJobDetailHeading!=null)
			{
				mLabelJobDetailHeading.setText(mDbHelper.getLabel(Constants.JOB_DETAILS));
			}

			mDbHelper.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * TextWatcher for NSK1
	 */
	private class NSKOneBeforeChanged implements TextWatcher {
		@Override
		public void afterTextChanged(Editable nskOne) {
			mValueNSK2.setText(nskOne.toString());
			mValueNSK3.setText(nskOne.toString());
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mValueNSK1.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {

				} else {
					mValueNSK1.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mValueNSK1.setText("");
			}

		}
	}

	/**
	 * TextWatcher for NSK2
	 */
	private class nskTwoBeforeChanged implements TextWatcher {

		@Override
		public void afterTextChanged(Editable nskTwo) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mValueNSK2.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {
				} else {
					mValueNSK2.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mValueNSK2.setText("");
			}

		}
	}

	/**
	 * TextWatcher for NSK3
	 */
	private class nskThreeBeforeChanged implements TextWatcher {

		@Override
		public void afterTextChanged(Editable nskThree) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mValueNSK3.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {
				} else {
					mValueNSK3.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mValueNSK3.setText("");
			}

		}
	}

	/**
	 * Method to get tire details from old tire
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreDetailsFromOldTyre(final int route) {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				if (mFitter_type.equalsIgnoreCase("I")) {
					String purchasingOrg = mDbHelper.getPurchasingOrganization(
							Authentication.getUserName(Constants.USER),
							Constants.SAP_VENDORCODE_ID);
					String stockLocationCode = "";
					String purchasingPlant = "";
					Cursor curStockLocCode = mDbHelper.getStockLocationCode(
							Authentication.getUserName(Constants.USER),
							Constants.SAP_VENDORCODE_ID, purchasingOrg);
					if (CursorUtils.isValidCursor(curStockLocCode)
							&& curStockLocCode.moveToFirst()) {
						stockLocationCode = curStockLocCode
								.getString(curStockLocCode
										.getColumnIndex("StockLocationCode"));
						purchasingPlant = curStockLocCode
								.getString(curStockLocCode
										.getColumnIndex("PurchasingPlant"));
						mCursor = mDbHelper.getBrandNameForPurPlantStockLoc(
								purchasingPlant, stockLocationCode);
					}
				} else {
					mCursor = mDbHelper.getBrandNameForSWAP();
				}

			}
			mBrandArrayList.clear();
			mBrandArrayList.add(mPleaseSelectBrand);
			mBrandCodeList.clear();
			mBrandCodeList.add("NA");
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				while (!mCursor.isAfterLast()) {
					mBrandArrayList.add(mCursor.getString(mCursor
							.getColumnIndex("Description")));
					mBrandCodeList.add(mCursor.getString(mCursor
							.getColumnIndex("ID")));
					mCursor.moveToNext();
				}
				CursorUtils.closeCursor(mCursor);
			}
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mBrandArrayList);
			dataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mBrandSpinner.setAdapter(dataAdapter);
			mBrandSpinner.setEnabled(true);
			mBrandSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							mBrandSpinner
									.setOnItemSelectedListener(listenerSelectBrandName);
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
							Toast.makeText(getApplicationContext(),
									Constants.sLblNothingSelected,
									Toast.LENGTH_SHORT).show();
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}

	OnItemSelectedListener listenerSelectBrandName = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED){
				mIsSizeSpinnerEditable = true;
			}
			mSelectedBrandName = mBrandSpinner.getSelectedItem().toString();
			if (mSelectedBrandName.equalsIgnoreCase(mPleaseSelectBrand)) {
				mTyreFullDetailSpinner.setEnabled(false);
				mTyreDesignSpinner.setEnabled(false);
			} else {
				mTyreFullDetailSpinner.setEnabled(true);
				mTyreDesignSpinner.setEnabled(true);
			}
			if (isDifferntSizeAllow || mIsSizeSpinnerEditable) {
				clearPreviousSelections(2);
				if (0 < mBrandSpinner.getSelectedItemPosition()) {
					getTyreSizes();
				} else {
					clearSpinnersData(2);
				}
			} else {
				clearPreviousSelections(5);
				if (0 < mBrandSpinner.getSelectedItemPosition()) {
					getTyreDesign();
				} else {
					clearSpinnersData(5);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			Toast.makeText(getApplicationContext(),
					Constants.sLblNothingSelected, Toast.LENGTH_SHORT).show();
		}
	};
	/**
	 * Method Getting Tire Sizes from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreSizes() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				mCursor = mDbHelper.getTyreSizeFromBrand(mSelectedBrandName);
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(2);
				return;
			}
			mCursor.moveToFirst();
			mSizeArrayList.clear();
			// mSizeArrayList.add("");
			while (!mCursor.isAfterLast()) {
				mSizeArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("TSize")));
				mCursor.moveToNext();
			}
			CursorUtils.closeCursor(mCursor);
			loadTyreSize();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method populating Tire Size after getting from local DB3 file as per the
	 * selected tire-brand
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreSize() {
		try {
			mSizeDataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mSizeArrayList);
			mSizeDataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSizeSpinner.setAdapter(mSizeDataAdapter);
			for (int i = 0; i < mSizeArrayList.size(); i++) {
				if (mSizeArrayList.get(i).equals(mSelectedtyreSize)) {
					mSizeSpinner.setSelection(i);
					break;
				}
			}
			mSizeSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							mSelectedtyreSize = mSizeSpinner.getSelectedItem()
									.toString();
							clearPreviousSelections(3);
							if (0 <= mSizeSpinner.getSelectedItemPosition()) {
								getTyreASP();
							} else {
								clearSpinnersData(3);
							}
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
							Toast.makeText(getApplicationContext(),
									Constants.sLblNothingSelected,
									Toast.LENGTH_SHORT).show();
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Method Getting Tire ASP from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreASP() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				mCursor = mDbHelper.getTyreASPFromSize(mSelectedtyreSize,
						mSelectedBrandName);
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(3);
				return;
			}
			mCursor.moveToFirst();
			mTyreTASPArrayList.clear();
			// mTyreTASPArrayList.add("");
			while (!mCursor.isAfterLast()) {
				mTyreTASPArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("TASP")));
				mCursor.moveToNext();
			}
			mCursor.close();
			loadTyreTASP();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method populating Tire ASP after getting from local DB3 file as per the
	 * selected tire-brand
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreTASP() {
		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTASPSpinner.setAdapter(mASPDataAdapter);
		for (int i = 0; i < mTyreTASPArrayList.size(); i++) {
			if (mTyreTASPArrayList.get(i).equals(mSelectedTyreTASP)) {
				mTyreTASPSpinner.setSelection(i);
				break;
			}
		}
		mTyreTASPSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedTyreTASP = mTyreTASPSpinner.getSelectedItem()
								.toString();
						clearPreviousSelections(4);
						if (0 <= mTyreTASPSpinner.getSelectedItemPosition()) {
							getTyreRIM();
						} else {
							clearSpinnersData(4);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
						Toast.makeText(getApplicationContext(),
								Constants.sLblNothingSelected,
								Toast.LENGTH_SHORT).show();
					}
				});
	}
	/**
	 * Method Getting Tire RIM from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreRIM() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				mCursor = mDbHelper.getTyreRimFromSize(mSelectedtyreSize,
						mSelectedTyreTASP, mSelectedBrandName);
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(4);
				return;
			}
			mCursor.moveToFirst();
			mTyreTRIMArrayList.clear();
			// mTyreTRIMArrayList.add("");
			while (!mCursor.isAfterLast()) {
				mTyreTRIMArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("TRIM")));
				mCursor.moveToNext();
			}
			CursorUtils.closeCursor(mCursor);
			loadTyreTRIM();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method populating Tire RIM after getting from local DB3 file as per the
	 * selected tire-brand
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreTRIM() {
		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTRIMSpinner.setAdapter(mRimDataAdapter);
		int size = mTyreTRIMArrayList.size();
		for (int i = 0; i < size; i++) {
			if (mTyreTRIMArrayList.get(i).equals(mSelectedtyreTRIM)) {
				mTyreTRIMSpinner.setSelection(i);
				break;
			}
		}
		mTyreTRIMSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedtyreTRIM = mTyreTRIMSpinner.getSelectedItem()
								.toString();
						clearPreviousSelections(5);
						if (0 <= mTyreTRIMSpinner.getSelectedItemPosition()) {
							getTyreDesign();
						} else {
							clearSpinnersData(5);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
						Toast.makeText(getApplicationContext(),
								Constants.sLblNothingSelected,
								Toast.LENGTH_SHORT).show();
					}
				});
	}
	/**
	 * Method Getting Tire Design from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreDesign() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				mCursor = mDbHelper.getTyreDesign(mSelectedtyreSize,
						mSelectedTyreTASP, mSelectedtyreTRIM,
						mSelectedBrandName);
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(5);
				return;
			}
			mCursor.moveToFirst();
			mDesignArrayList.clear();
			mDesignArrayList.add(mDefaultValueForSpinner);
			while (!mCursor.isAfterLast()) {
				mDesignArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("Deseign")));
				mCursor.moveToNext();
			}
			mCursor.close();
			loadTyreDesign();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method populating Tire Design after getting from local DB3 file as per
	 * the selected tire-brand
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreDesign() {
		mDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mDesignArrayList);
		mDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreDesignSpinner.setAdapter(mDesignDataAdapter);
		for (int i = 0; i < mDesignArrayList.size(); i++) {
			if (mDesignArrayList.get(i).equals(mSelectedTyreDesign)) {
				mTyreDesignSpinner.setSelection(i);
				break;
			}
		}
		mTyreDesignSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedTyreDesign = mTyreDesignSpinner
								.getSelectedItem().toString();
						clearPreviousSelections(6);
						if (0 < mTyreDesignSpinner.getSelectedItemPosition()) {
							getTyreDetailedDesign();// type
						} else {
							clearSpinnersData(6);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
						Toast.makeText(getApplicationContext(),
								Constants.sLblNothingSelected,
								Toast.LENGTH_SHORT).show();
					}
				});
	}
	/**
	 * Method Getting Tire Design-Details from local DB3 file as per the
	 * selected tire-brand
	 */
	public void getTyreDetailedDesign() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				mCursor = mDbHelper.getTyreDetailedDesign(mSelectedtyreSize,
						mSelectedTyreTASP, mSelectedtyreTRIM,
						mSelectedTyreDesign, mSelectedBrandName);
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(6);
				return;
			}
			mCursor.moveToFirst();
			mFullDesignArrayList.clear();
			mFullDesignArrayList.add(mDefaultValueForSpinner);
			while (!mCursor.isAfterLast()) {
				mFullDesignArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("FullTireDetails")));
				mCursor.moveToNext();
			}
			CursorUtils.closeCursor(mCursor);
			loadTyreDetailedDesign();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method populating Tire Design-Details after getting from local DB3 file
	 * as per the selected tire-brand
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreDetailedDesign() {
		mFullDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mFullDesignArrayList);
		mFullDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreFullDetailSpinner.setAdapter(mFullDesignDataAdapter);
		for (int i = 0; i < mFullDesignArrayList.size(); i++) {
			if (mFullDesignArrayList.get(i).equals(mSelectedTyreDetailedDesign)) {
				mTyreFullDetailSpinner.setSelection(i);
				break;
			}
		}
		mTyreFullDetailSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedTyreDetailedDesign = mTyreFullDetailSpinner
								.getSelectedItem().toString();
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
						Toast.makeText(getApplicationContext(),
								Constants.sLblNothingSelected,
								Toast.LENGTH_SHORT).show();
					}
				});
	}
	/**
	 * Method Getting Tire SAPMaterialCodeID from local DB3 file as per the
	 * selected DetailedDesign
	 */
	public void getSAPMaterialCodeID() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				mCursor = mDbHelper.getSAPMaterialCodeID(mSelectedtyreSize,
						mSelectedTyreTASP, mSelectedtyreTRIM,
						mSelectedTyreDesign, mSelectedTyreDetailedDesign,
						mSelectedBrandName);
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				return;
			}
			mCursor.moveToFirst();
			mSelectedSAPMaterialCodeID = mCursor.getString(
					mCursor.getColumnIndex("ID")).toString();
			CursorUtils.closeCursor(mCursor);
			if (TextUtils.isEmpty(Constants.SELECTED_TYRE.getSerialNumber())) {
				if (Constants.onBrandBool == true
						|| Constants.onBrandBoolForJOC == true) {
					Constants.NEW_SAPMATERIAL_CODE = mSelectedSAPMaterialCodeID;
					VehicleSkeletonFragment.updateCorrectedBrandInDB();
				}
				if (!Constants.SELECTED_TYRE.getSerialNumber()
						.equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)) {
					VehicleSkeletonFragment.updateCorrectedSerialNumberInDB();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method handling the data-updates for the tables(JobItem, tire,
	 * JobCorrection etc) for the selected tire. It checks all the mandatory
	 * fields then allows user to navigate back to Vehicle Skeleton after
	 * updating the data
	 */
	private void onReturnSkeleton() {
		// Intent intentReturn = new Intent();

		mFinalConvertedPressure = CommonUtils.getFinalConvertedPressureValue(
				mRecommendedPressure, mSetPressure.getText().toString(),
				mIsSettingsPSI);
		updatePressureValue();
		getSAPMaterialCodeID();
		AutoSwapImpl.getMyInstance(getApplicationContext())
				.callAutoSwapImplementation(
						mBrandSpinner.getSelectedItem().toString(),
						mTyreFullDetailSpinner.getSelectedItem().toString(),
						mValueNSK1.getText().toString(),
						mValueNSK2.getText().toString(),
						mValueNSK3.getText().toString(),
						mValueNSK1.getText().toString(), "False");
		// mDismountedTyre.setPosition(mDismountedTyre.getPosition() + "$");
		// VehicleSkeletonFragment.tyreInfo.add(mDismountedTyre);
		// updateJobItem();
		Constants.SELECTED_TYRE.setSerialNumber(mSerialNoFromIntent);
		Constants.SELECTED_TYRE.setBrandName(mBrandSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setSize(mSizeSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setAsp(mTyreTASPSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setRim(mTyreTRIMSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setDesign(mTyreDesignSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setDesignDetails(mTyreFullDetailSpinner
				.getSelectedItem().toString());
		Constants.SELECTED_TYRE.setNsk(mValueNSK1.getText().toString());
		Constants.SELECTED_TYRE.setNsk2(mValueNSK2.getText().toString());
		Constants.SELECTED_TYRE.setNsk3(mValueNSK3.getText().toString());
		Constants.SELECTED_TYRE.setPressure(mFinalConvertedPressure);
		Constants.SELECTED_TYRE.setShared("0");
		Constants.SELECTED_TYRE.setFromJobId(null);
		Constants.SELECTED_TYRE.setVehicleJob(Constants.contract
				.getVehicleSapCode());
		Constants.SELECTED_TYRE.setStatus("0");
		Constants.SELECTED_TYRE
				.setSapMaterialCodeID(mSelectedSAPMaterialCodeID);

		//User Trace logs
		try {
			String traceData;
			traceData = "\n\t\tTyre Details : " + Constants.SELECTED_TYRE.getBrandName() +
					"| " + Constants.SELECTED_TYRE.getSize() +
					"| " + Constants.SELECTED_TYRE.getAsp() +
					"| " + Constants.SELECTED_TYRE.getRim() +
					"| " + Constants.SELECTED_TYRE.getDesign() +
					"| " + Constants.SELECTED_TYRE.getDesignDetails() +


					"\n\t\tJob Details : " + Constants.SELECTED_TYRE.getNsk() +
					", " + Constants.SELECTED_TYRE.getNsk2() +
					", " + Constants.SELECTED_TYRE.getNsk3() +
					"| " + Constants.SELECTED_TYRE.getPressure();

			traceData = traceData+	"| " + Constants.SELECTED_TYRE.getRimType();
			LogUtil.TraceInfo(TRACE_TAG, "none","Data : " + traceData, false, false, false);
		}
		catch (Exception e)
		{
			LogUtil.TraceInfo(TRACE_TAG, "Data : Exception : ", e.getMessage(), false, true, false);
		}

		Intent intentReturn = new Intent(this, WOTActivity.class);
		intentReturn.putExtra("SERIAL_NO", mSerialNoFromIntent);
		intentReturn.putExtra("WHEEL_POS",
				Constants.SELECTED_TYRE.getPosition());
		String pressure = CommonUtils.getFinalConvertedPressureValue(
				mRecommendedPressure, mSetPressure.getText().toString(),
				mIsSettingsPSI);
		intentReturn.putExtra(VehicleSkeletonFragment.mAXLEPRESSURE,
				CommonUtils.parseFloat(pressure));

		//Fix : Bug 626 (users are reporting that tyre pressures are incorrectly applied to other axles)
		//Fix for Missing Axle Position Information
		intentReturn.putExtra(VehicleSkeletonFragment.mAXLEPRESSURE_POS,mAxlePosition);

		intentReturn.putExtra("define_tire", true);
		// setResult(VehicleSkeletonFragment.DEFINE_TIRE_INTENT, intentReturn);
		startActivityForResult(intentReturn, DEFINE_TIRE_INTENT_ACTIVITY);

		// finish();
		((Activity) mContext).overridePendingTransition(R.anim.slide_left_in,
				R.anim.slide_left_out);
	}

	private AlertDialog mBackAlertDialog;
	private boolean mBlockPressureFromBluetooth;
	/**
	 * Method Capturing the dialog state before orientation change
	 */
	private void saveDialogState(Bundle state) {
		state.putBoolean("mBackAlertDialog",
				(mBackAlertDialog != null && mBackAlertDialog.isShowing()));
	}
	/**
	 * Method Capturing the dialog state after orientation change
	 */
	private void restoreDialogState(Bundle state) {
		if (state != null) {
			if (state.getBoolean("mBackAlertDialog")) {
				actionBackPressed();
			}
		}
	}

	@Override
	protected void onDestroy() {
		try {
			if (null != mDbHelper) {
				mDbHelper.close();
			}
			if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
				mBackAlertDialog.dismiss();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		if (areAnyChangesMade()) {
			actionBackPressed();
		} else {
			LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press", false, false, false);
			goBack();
		}
	};
	/**
	 * Method handling the action performed where user presses the back button
	 * Alert Box created with two option: YES and NO
	 */
	private void actionBackPressed() {
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", false, true, false);
		LayoutInflater li = LayoutInflater.from(mContext);
		View promptsView = li
				.inflate(R.layout.dialog_logout_confirmation, null);

		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(mContext);
		alertDialogBuilder.setView(promptsView);
		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
		infoTv.setText(mCancelJobLabel);
		alertDialogBuilder.setCancelable(false).setPositiveButton(mYESLabel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
						VehicleSkeletonFragment.resetTyreState();
						goBack();
					}
				});
		alertDialogBuilder.setCancelable(false).setNegativeButton(mNOLabel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);
						dialog.cancel();
					}
				});
		mBackAlertDialog = alertDialogBuilder.create();
		mBackAlertDialog.show();
		mBackAlertDialog.setCanceledOnTouchOutside(false);
	}

	private void goBack() {
		((Activity) mContext).finish();
		Constants.ONSTATE_WOT = false;
	}

	/**
	 * check if empty or zero
	 * 
	 * @param value
	 * @return
	 */
	private boolean checkIfEmptyOrZero(String value) {
		if (value.equals("") || CommonUtils.parseFloat(value) == 0.0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * @author amitkumar.h 
	 * Class providing the animation when user swipes out
	 * from the activity It also handles the actions performed when use
	 * is swiping out from the activity after matching all the required
	 * conditions
	 */
	public class Swiper implements OnTouchListener, OnClickListener {
		float startX, startY;
		float endX, endY;
		int selectedPositionToDelete;
		ArrayAdapter<String> adapterList;
		ScrollView view;
		private Context ctx;
		public static final float MINIMUM_MOVEMENT_REQUIRED = 100;

		public Swiper(Context ctx, ScrollView view) {
			this.ctx = ctx;
			this.view = view;
			view.setOnClickListener(this);
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getActionMasked()) {
			case MotionEvent.ACTION_DOWN:
				mSwiped = false;
				startX = event.getX();
				startY = event.getY();
				break;
			case MotionEvent.ACTION_UP:
				break;

			case MotionEvent.ACTION_MOVE:
				endX = event.getX();
				endY = event.getY();
				if (Math.abs(endX - startX) > MINIMUM_MOVEMENT_REQUIRED
						&& !mSwiped) {
					mSwiped = true;
					Rect rect = new Rect();
					int childCount = view.getChildCount();
					int[] listViewCoords = new int[2];
					view.getLocationOnScreen(listViewCoords);
					int x = (int) event.getRawX() - listViewCoords[0];
					int y = (int) event.getRawY() - listViewCoords[1];
					View child;
					for (int i = 0; i < childCount; i++) {
						child = view.getChildAt(i);
						child.getHitRect(rect);
						if (rect.contains(x, y)) {
							if (mTyreDesignSpinner.getSelectedItemPosition() < 0
									|| mTyreDesignSpinner.getSelectedItem()
											.toString()
											.equals(mDefaultValueForSpinner)) {
								CommonUtils.notify(mDesignDetailRequired,
										mContext);
							} else if (mTyreFullDetailSpinner
									.getSelectedItemPosition() < 0
									|| mTyreFullDetailSpinner.getSelectedItem()
											.toString()
											.equals(mDefaultValueForSpinner)) {
								CommonUtils.notify(mDesignDetailRequired,
										mContext);
							} else if (checkIfEmptyOrZero(mValueNSK1.getText()
									.toString())
									|| checkIfEmptyOrZero(mValueNSK2.getText()
											.toString())
									|| checkIfEmptyOrZero(mValueNSK3.getText()
											.toString())) {
								CommonUtils.notify(
										Constants.sLblEnterNskValues,
										getBaseContext());
							} else if (CommonUtils.parseFloat(mSetPressure
									.getText().toString()) == 0.0) {
								CommonUtils.notify(mPressureRequired,
										getBaseContext());
							} else if (mSetPressure.getText().toString()
									.equals("")) {
								CommonUtils.notify(mPressureRequired,
										getBaseContext());
							} else if (!CommonUtils
									.pressureValueValidation(mSetPressure)) {
								CommonUtils.notify(
										Constants.sLblCheckPressureValue,
										getBaseContext());
							} else {
								onReturnSkeleton();
							}
						}
					}
				}
				break;
			}
			return false;
		}

		@Override
		public void onClick(View arg0) {
		}
	}

	/**
	 * When spinner value is null then dependent spinner values refreshing.
	 */
	private void clearSpinnersData(int i) {
		// Don't add break statement, It's sequence of execution
		switch (i) {
		case 1: // Brand Spinner
		case 2: // Size Spinner
			mSizeArrayList.clear();
			if (null != mSizeDataAdapter) {
				mSizeDataAdapter.notifyDataSetChanged();
			}
		case 3: // ASP Spinner
			mTyreTASPArrayList.clear();
			if (null != mASPDataAdapter) {
				mASPDataAdapter.notifyDataSetChanged();
			}
		case 4: // Rim Spinner
			mTyreTRIMArrayList.clear();
			if (null != mRimDataAdapter) {
				mRimDataAdapter.notifyDataSetChanged();
			}
		case 5: // Design Spinner
			mDesignArrayList.clear();
			if (null != mDesignDataAdapter) {
				mDesignDataAdapter.notifyDataSetChanged();
			}
		case 6: // FullDetails Spinner
			mFullDesignArrayList.clear();
			if (mFullDesignDataAdapter != null) {
				mFullDesignDataAdapter.notifyDataSetChanged();
			}
		}
	}
	/**
	 * Method Clearing the Previous Selection Values of the spinners present in
	 * the UI
	 */
	private void clearPreviousSelections(int i) {
		if (mIsOrientationChanged && i < mLastSpinnerSelection) {
			if (i == 6 && mLastSpinnerSelection == 7) {
				mIsOrientationChanged = false;
			}
			return;
		} else if (mIsOrientationChanged) {
			mIsOrientationChanged = false;
		}
		// Don't add break statement, It's sequence of execution
		switch (i) {
		case 1: // Brand Spinner
		case 2: // Size Spinner
			mSelectedtyreSize = "";
		case 3: // ASP Spinner
			mSelectedTyreTASP = "";
		case 4: // Rim Spinner
			mSelectedtyreTRIM = "";
		case 5: // Design Spinner
			mSelectedTyreDesign = "";
		case 6: // FullDetails Spinner
			mSelectedTyreDetailedDesign = "";
		}
	}
	/**
	 * Method Checking the Previous Selected Spinner just before the change in
	 * orientation
	 */
	private void checkLatestSpinnerSelectionBeforeOrientationChanges() {
		if (null == mSelectedBrandName
				|| mPleaseSelectBrand.equals(mSelectedBrandName)) {
			mLastSpinnerSelection = 1;
		} else if (null == mSelectedtyreSize || "".equals(mSelectedtyreSize)) {
			mLastSpinnerSelection = 2;
		} else if (null == mSelectedTyreTASP || "".equals(mSelectedTyreTASP)) {
			mLastSpinnerSelection = 3;
		} else if (null == mSelectedtyreTRIM || "".equals(mSelectedtyreTRIM)) {
			mLastSpinnerSelection = 4;
		} else if (null == mSelectedTyreDesign
				|| mDefaultValueForSpinner.equals(mSelectedTyreDesign)) {
			mLastSpinnerSelection = 5;
		} else if (null == mSelectedTyreDetailedDesign
				|| mDefaultValueForSpinner.equals(mSelectedTyreDetailedDesign)) {
			mLastSpinnerSelection = 6;
		} else {
			mLastSpinnerSelection = 7;
		}
	}

	/**
	 * If axle have pressure apply the value then disable otherwise it's
	 * editable Setting the Pressure
	 */
	private void setValuetoPressueView() {
		if (mRecommendedPressure > 0) {
			mSetPressure.setText(CommonUtils.getPressureValue(mContext,
					mRecommendedPressure));
			mPressureUnit.setText(CommonUtils.getPressureUnit(mContext));
			mSetPressure.setEnabled(false);
			mBlockPressureFromBluetooth = true;
		} else {
			if (mIsSettingsPSI) {
				mSetPressure
						.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
								4, 2) });
			} else {
				mSetPressure
						.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
								3, 2) });
			}
			/*Bug 728 : Adding context to show toast for higher pressure value*/
			mSetPressure.addTextChangedListener(new PressureCheckListener(
					mSetPressure, mIsSettingsPSI, mContext));
			mPressureUnit.setText(CommonUtils.getPressureUnit(mContext));
			mValueNSK3.setOnEditorActionListener(new OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId,
						KeyEvent event) {
					boolean handled = false;
					if (actionId == EditorInfo.IME_ACTION_NEXT) {
						mSetPressure.requestFocus();
						handled = true;
					}
					return handled;
				}
			});
		}
	}

	/**
	 * Method Updating recommended axle pressure
	 */
	private void updatePressureValue() {
		if ((mSetPressure.isEnabled() || !mBlockPressureFromBluetooth)
				&& (null != VehicleSkeletonFragment.mAxleRecommendedPressure)) {
			VehicleSkeletonFragment.mAxleRecommendedPressure.set(mAxlePosition,
					CommonUtils.parseFloat(mFinalConvertedPressure));
		}
	}
	/**
	 * Method handling the Setting up of data on the UI elements(Spinners,labels
	 * etc) for Auto-Logical Swap
	 */
	public void setDataForAutoSwap() {
		if (!TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
				&& !TextUtils.isEmpty(Constants.tempTyrePositionNew)
				&& AutoSwapImpl.checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)
				&& VehicleSkeletonFragment.getTireByPosition(
						Constants.tempTyrePositionNew).getTyreState() != TyreState.EMPTY
				&& !Constants.tempTyreSerial
						.equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
				&& !TextUtils.isEmpty(Constants.tempTyrePositionNew)) {

			Tyre mAutoSwappedTire = VehicleSkeletonFragment
					.getTireByPosition(Constants.tempTyrePositionNew);

			mBrandForAutoSwap = mAutoSwappedTire.getBrandName();
			mSerialNumberForAutoSwap = mAutoSwappedTire.getSerialNumber();
			mSizeForAutoSwap = mAutoSwappedTire.getSize();
			mRimForAutoSwap = mAutoSwappedTire.getRim();
			mAspForAutoSwap = mAutoSwappedTire.getAsp();
			mDesignForAutoSwap = mAutoSwappedTire.getDesign();
			mDesignDetailsForAutoSwap = mAutoSwappedTire.getDesignDetails();
			mNskForAutoSwap = mAutoSwappedTire.getNsk();
			mNsk2ForAutoSwap = mAutoSwappedTire.getNsk2();
			mNsk3ForAutoSwap = mAutoSwappedTire.getNsk3();

			// For SAp material Code Fix:: 315
			if (TextUtils.isEmpty(mBrandForAutoSwap)) {
				mSelectedBrandName = mBrandForAutoSwap;
			} else {
				mSelectedBrandName = mBrandForAutoSwap;
				mSelectedtyreSize = mSizeForAutoSwap;
				mSelectedtyreTRIM = mRimForAutoSwap;
				mSelectedTyreTASP = mAspForAutoSwap;
				mSelectedTyreDesign = mDesignForAutoSwap;
				mSelectedTyreDetailedDesign = mDesignDetailsForAutoSwap;
			}

			// setting values here
			if (Constants.onBrandBool || TextUtils.isEmpty(mBrandForAutoSwap)) {
				//mIsSizeSpinnerEditable = false;
				mTyreFullDetailSpinner.setEnabled(true);
				mTyreDesignSpinner.setEnabled(true);
				mTyreFullDetailSpinner.setAdapter(null);
				mTyreDesignSpinner.setAdapter(null);
				mSelectedBrandName = "";
			} else {
				loadSelectedBrandForAutoSwap(mBrandForAutoSwap);
				loadSelectedSizeForAutoSwap(mSizeForAutoSwap);
				loadSelectedASPForAutoSwap(mAspForAutoSwap);
				loadSelectedRIMForAutoSwap(mRimForAutoSwap);
				loadSelectedDesignForAutoSwap(mDesignForAutoSwap);
				loadSelectedDesignDetailsForAutoSwap(mDesignDetailsForAutoSwap);

				mValueNSK1.setText(mNskForAutoSwap);
				mValueNSK2.setText(mNsk2ForAutoSwap);
				mValueNSK3.setText(mNsk3ForAutoSwap);
			}
			if(!mSizeForAutoSwap.equalsIgnoreCase("0")){
				mIsSizeSpinnerEditable = false;
			}
		}
	}
	/**
	 * Method to load tire brand with no brand correction
	 * @param mBrandForAutoSwap2
	 */
	private void loadSelectedBrandForAutoSwap(String mBrandForAutoSwap2) {
		mBrandArrayList.clear();
		mBrandArrayList.add(mBrandForAutoSwap2);
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mBrandArrayList);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mBrandSpinner.setAdapter(dataAdapter);
		mBrandSpinner.setEnabled(false);
	}
	/**
	 * Method to load tire size with no brand correction
	 * @param mSizeForAutoSwap2
	 */
	private void loadSelectedSizeForAutoSwap(String mSizeForAutoSwap2) {
		mSizeArrayList.clear();
		mSizeArrayList.add(mSizeForAutoSwap2);
		mSizeDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mSizeArrayList);
		mSizeDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSizeSpinner.setAdapter(mSizeDataAdapter);
		mSizeSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire ASP with no brand correction
	 * 
	 * @param mAspForAutoSwap2
	 */
	private void loadSelectedASPForAutoSwap(String mAspForAutoSwap2) {
		mTyreTASPArrayList.clear();
		mTyreTASPArrayList.add(mAspForAutoSwap2);
		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTASPSpinner.setAdapter(mASPDataAdapter);
		mTyreTASPSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire RIM with no brand correction
	 * 
	 * @param mRimForAutoSwap2
	 */
	private void loadSelectedRIMForAutoSwap(String mRimForAutoSwap2) {
		mTyreTRIMArrayList.clear();
		mTyreTRIMArrayList.add(mRimForAutoSwap2);
		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTRIMSpinner.setAdapter(mRimDataAdapter);
		mTyreTRIMSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire design with no brand correction
	 * 
	 * @param mDesignForAutoSwap2
	 */
	private void loadSelectedDesignForAutoSwap(String mDesignForAutoSwap2) {
		mDesignArrayList.clear();
		mDesignArrayList.add(mDesignForAutoSwap2);
		mDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mDesignArrayList);
		mDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreDesignSpinner.setAdapter(mDesignDataAdapter);
		mTyreDesignSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire full design details with no brand correction
	 * 
	 * @param mDesignDetailsForAutoSwap2
	 */
	private void loadSelectedDesignDetailsForAutoSwap(
			String mDesignDetailsForAutoSwap2) {
		mFullDesignArrayList.clear();
		mFullDesignArrayList.add(mDesignDetailsForAutoSwap2);
		mFullDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mFullDesignArrayList);
		mFullDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreFullDetailSpinner.setAdapter(mFullDesignDataAdapter);
		mTyreFullDetailSpinner.setEnabled(false);

	}

	private void captureInitialState(Bundle bundle) {
		if (bundle != null) {
			mFormElements = bundle.getParcelable("physicalSwapForm");
		} else {
			mFormElements = new TyreFormElements();
			mFormElements.setBrand(mBrandSpinner.getSelectedItemPosition());
			mFormElements.setDesign(mTyreDesignSpinner
					.getSelectedItemPosition());
			mFormElements.setType(mTyreFullDetailSpinner
					.getSelectedItemPosition());
			mFormElements.setNsk1(mValueNSK1.getText().toString());
			mFormElements.setNsk2(mValueNSK2.getText().toString());
			mFormElements.setNsk3(mValueNSK3.getText().toString());
			mFormElements.setRimType(mRimValue.isChecked());
		}
	}

	private void saveInitialState(Bundle bundle) {
		bundle.putParcelable("physicalSwapForm", mFormElements);
	}

	private boolean areAnyChangesMade() {
		boolean isSameAsInitial = true;
		isSameAsInitial &= mFormElements.getBrand() == mBrandSpinner
				.getSelectedItemPosition();
		isSameAsInitial &= mFormElements.getDesign() == mTyreDesignSpinner
				.getSelectedItemPosition();
		isSameAsInitial &= mFormElements.getType() == mTyreFullDetailSpinner
				.getSelectedItemPosition();
		isSameAsInitial &= mFormElements.getNsk1().equals(
				mValueNSK1.getText().toString());
		isSameAsInitial &= mFormElements.getNsk2().equals(
				mValueNSK2.getText().toString());
		isSameAsInitial &= mFormElements.getNsk3().equals(
				mValueNSK3.getText().toString());
		isSameAsInitial &= mFormElements.isRimType() == mRimValue.isChecked();
		return !isSameAsInitial;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_CANCELED) {
			return;
		}
		if (requestCode == DEFINE_TIRE_INTENT_ACTIVITY) {
			Intent intentReturn = new Intent();
			boolean putTickMark = false;
			intentReturn.putExtra("puttickmark", putTickMark);
			intentReturn.putExtra("wot", true);
			setResult(VehicleSkeletonFragment.WOT_INTENT, intentReturn);
			Constants.JOB_CORRECTION_LIST = VehicleSkeletonFragment.mJobcorrectionList;
			finish();
			DefineTireActivity.this.overridePendingTransition(
					R.anim.slide_left_in, R.anim.slide_left_out);
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		// set user inactivity handler for this activity
		LogoutHandler.setCurrentActivity(this);
	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		InactivityUtils.logoutFromActivityAboveEjobForm(this);
	}
}
