/**
 * Class used for display all jobs performed by user and
 * Sync happens here
 */

package com.goodyear.ejob;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Messenger;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.internal.view.ActionBarPolicy;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.goodyear.ejob.adapter.EjobListAdapter;
import com.goodyear.ejob.dbhelpers.DataBaseHandler;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.job.operation.helpers.EditJob;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.service.EjobSyncService;
import com.goodyear.ejob.service.MessageHandler;
import com.goodyear.ejob.service.ResponseMessage;
import com.goodyear.ejob.service.SyncBaseModel.SyncCallback;
import com.goodyear.ejob.service.SyncUtils;
import com.goodyear.ejob.service.SyncUtils.SyncOptions;
import com.goodyear.ejob.service.UserInactivityService;
import com.goodyear.ejob.sync.AuthSync;
import com.goodyear.ejob.ui.tyremanagement.TyreManagementActivity;
import com.goodyear.ejob.ui.tyremanagement.TyreSummary;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.FileOperations;
import com.goodyear.ejob.util.GYProgressDialog;
import com.goodyear.ejob.util.InspectionState;
import com.goodyear.ejob.util.JobBundle;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.MapComparator;
import com.goodyear.ejob.util.ObjectSerializer;
import com.goodyear.ejob.util.OfflinePinDialog;
import com.goodyear.ejob.util.Settings;
import com.goodyear.ejob.util.SettingsLoader;
import com.goodyear.ejob.util.SyncTime;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.Validation;

import org.joda.time.DateTime;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class EjobList extends ActionBarActivity implements SyncCallback,
        InactivityHandler {
    /**
     * key for selected items
     */
    public static final String SELECTED_ITEMS = "selected_items";
    /**
     * tire id
     */
    private static String sTyreid;
    /**
     * vendor detail string
     */
    private static String mVendorDetail;
    /**
     * load image view
     */
    private static ImageView sloadingImage;
    /**
     * job list view array
     */
    public static ArrayList<HashMap<String, String>> sListItems = new ArrayList<HashMap<String, String>>();
    /**
     * dialog boolean
     */
    private static boolean isLoading;
    /**
     * language String
     */
    private String mLang;
    /**
     * ref no
     */
    private String mRefNO;
    /**
     * customer name
     */
    private String mCustomerName;
    /**
     * Job list view
     */
    private ListView mLvJoblist;
    /**
     * removal reason
     */
    private ArrayList<String> mRemovalReasonArrayList;
    /**
     * cal Sync time
     */
    private SyncTime mSyncTime;
    /**
     * Alert dialog
     */
    private AlertDialog mAlertDialog;
    /**
     * menu
     */
    private LinearLayout mLinearMenu;
    /**
     * home serial No
     */
    private String mHomeSerialNum;
    /**
     * work order no
     */
    private String mHomeWorkOrderNuber;
    /**
     * repair comp name
     */
    private String mHomeRepairCompanyName;
    /**
     * result design
     */
    private String mHomeResultDesign;
    /**
     * current status
     */
    private String mHomeCurStatus;
    /**
     * description
     */
    private String mHomeDescription;
    /**
     * ejob list adapter
     */
    private EjobListAdapter mAdapter;
    /** reason */
    // private String mNoReason;
    /**
     * db helper
     */
    private DatabaseAdapter mDBHelper;
    /**
     * ref no
     */
    private int mRefnoInt;
    /**
     * context
     */
    private Context context;
    /**
     * bundle data for retain in orientation
     */
    private Bundle bundle = new Bundle();
    /**
     * licence plate txt view
     */
    private TextView mLicencePlate;
    /**
     * customer txt view
     */
    private TextView customerName;
    /**
     * date txt view
     */
    private TextView mDate;
    /**
     * ref no text view
     */
    private TextView refNO;
    /**
     * diff in Sync
     */
    private boolean dayDifferenceInSync;
    /** removal reason ID */
    // private static int mRemovalReasonID;
    /**
     * ref no for delete
     */
    public String mRefNOForDelete;
    /**
     * Job external Id for delete
     */
    public String mJobExternalId;
    /**
     * licence plate lbl
     */
    private String mLicencePlateNOLabel;
    private String strJOB_HAS_BEEN_DELETED;
    private String strAPPLICATIONDB;

    /**
     * cus name lbl
     */
    private String mCusNameLabel;
    /**
     * date lbl
     */
    private String mDateLabel;
    /**
     * ref no lbl
     */
    private String mRefNOLabel;
    /**
     * job status
     */
    private int mJobStatus;
    /**
     * selected item array
     */
    private ArrayList<Integer> mSelectedItems;
    /**
     * progress dialog
     */
    private GYProgressDialog mGYProgressDialog;
    /**
     * TM jobs
     */
    private RelativeLayout rlTireManagementJobs;
    /**
     * incompleate Job
     */
    private RelativeLayout rlIncompleteJobs;
    /**
     * compleate Job
     */
    private RelativeLayout rlCompletedJobs;
    /**
     * delete Job
     */
    private RelativeLayout rlDeletedJobs;
    /**
     * job start time
     */
    private long mStartTime;
    /**
     * licence image
     */
    private ImageView mLinumImage;
    /**
     * customer image view
     */
    private ImageView mCusImage;
    /**
     * date image view
     */
    private ImageView mDateImage;
    /**
     * ref image view
     */
    private ImageView mRefImage;
    /**
     * launch new job activity
     */
    private ImageView mLaunchNewJobActivity;
    /**
     * TM activity
     */
    private ImageView mTireManagementActivity;
    private AlertDialog mLogoutAlertDialog;
    private AlertDialog mLogoutPopUp;
    /**
     * Sorting Icon Image View
     */
    private ImageView mImageViewSorting = null;
    private String mSortingKey = "";
    private AlertDialog mJobLockDialog;
    private String mOkLabel = "";
    private String threeDaysOld = "";

    private String mDialogTitle;
    private String mDialogMsg;
    private AlertDialog mErrorDialog;
    private LogoutHandler mLogoutHandler;
    private String mCancelLabel;
    private String mDeleteReasonLabel;
    private AlertDialog mThreDaysOlderJobAlertDialog;
    private String mNOLabel = "";
    private String mYESLabel = "";
    private String mSyncLabel = "";
    private String mAboutLabel = "";
    private String mHelpLabel = "";
    private String mLogoutLabel = "";
    private String mSettingsLabel = "";
    private String mLogoutConfirmationLabel = "";
    public static final String CHECKED_ITEMS = "checked_list";
    private boolean mDeviceRotated;

    //User Trace logs trace tag
    private static final String TRACE_TAG = "EjobList";

    private TextView mLogoutMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.i("EjobList ", " onCreate()");
        //setting activity(Goodyear_eJob_MainActivity) reference to constants
        Constants.mainActivity = this;
        Intent inactivityService = new Intent(this, UserInactivityService.class);
        startService(inactivityService);
        //Unique Key to store Partial Inspection Status of Each Job
        Constants.PARTIALLY_INSPECTYED = "PARTIALLY_INSPECTYED" + "_" + Constants.VALIDATE_USER + "_" + Constants.JOBREFNUMBER;
        Constants.PARTIALLY_INSPECTED_WITH_OPERATION = "PARTIALLY_INSPECTED_WITH_OPERATION" + "_" + Constants.VALIDATE_USER + "_" + Constants.JOBREFNUMBER;
        setContentView(R.layout.activity_ejob_list);
        openDB();
        ActionBar actionBar = getActionBar();
        actionBar.setCustomView(R.layout.actionbarimages);

        //User Trace logs
        LogUtil.TraceInfo(TRACE_TAG, " none", "Ejob List", true, false, true);

        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
        ActionBarPolicy.get(this).showsOverflowMenuButton();
        display();
        Constants.SINGLESWIPETODELETEEJOBLIST = false;
        TextView titleTxt = (TextView) findViewById(R.id.title_text);
        titleTxt.setText(mDBHelper.getLabel(Constants.LABEL_EJOBS));
        mLogoutMessage = (TextView) findViewById(R.id.logout_msg);
        mLaunchNewJobActivity = (ImageView) findViewById(R.id.actionBarLogo);
        mLaunchNewJobActivity.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Constants.COMESFROMUPDATE = false;
                Constants.COMESFROMVIEW = false;
                Constants.TM_NOT_ALLOWED = false;
                DatabaseAdapter.userValidate(Constants.USER);
                Intent newJob = new Intent(EjobList.this, LaunchNewJob.class);
                startActivity(newJob);
            }
        });
        mTireManagementActivity = (ImageView) findViewById(R.id.actionBarLogo2);
        mTireManagementActivity.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Constants.GETJOBREFFNO = true;
                Constants.RELOAD_SCREEN = false;
                Constants._mBundleActionTypeVec.clear();
                Constants._mBundleOperationIDVec.clear();
                Constants._repairCompVec.clear();
                Constants._workOrderVec.clear();
                Constants._mBundleCurrentstatusVec.clear();
                DatabaseAdapter.userValidate(Constants.USER);
                Constants.RELOAD_REFF = false;
                Constants.TM_NOT_ALLOWED = true;
                Intent intent = new Intent(EjobList.this,
                        TyreManagementActivity.class);
                startActivity(intent);
            }
        });

        mLicencePlateNOLabel = mDBHelper
                .getLabel(Constants.LICENCE_PLATE_FROM_DB);

        strAPPLICATIONDB = mDBHelper.getLabel(Constants.APPLICATIONDB);
        strJOB_HAS_BEEN_DELETED = mDBHelper
                .getLabel(Constants.JOB_HAS_BEEN_DELETED);
        mCusNameLabel = mDBHelper.getLabel(Constants.CUST_NAME_FROM_DB);
        mDateLabel = mDBHelper.getLabel(Constants.DATE_FROM_DB);
        mRefNOLabel = mDBHelper.getLabel(Constants.REF_NUMBER_LABEL);
        mLinumImage = (ImageView) findViewById(R.id.linumimage);
        mCusImage = (ImageView) findViewById(R.id.cusimage);
        mDateImage = (ImageView) findViewById(R.id.dateimage);
        mRefImage = (ImageView) findViewById(R.id.refimage);
        sloadingImage = (ImageView) findViewById(R.id.loading_img);
        sloadingImage.setVisibility(ImageView.INVISIBLE);
        mOkLabel = mDBHelper.getLabel(Constants.LABEL_OK);
        mCancelLabel = mDBHelper.getLabel(Constants.CANCEL_LABEL);
        mDeleteReasonLabel = mDBHelper.getLabel(Constants.DELETE_REASON_LABEL);
        mYESLabel = mDBHelper.getLabel(Constants.YES_LABEL);
        mNOLabel = mDBHelper.getLabel(Constants.NO_LABEL);
        threeDaysOld = mDBHelper.getLabel(5);
        loadGlobalLabels();
        loadFromDB();
        context = this;
        String startedFrom = null;

        /* #ForHalosys , set first time offline pin */
        SharedPreferences sharedPref = getSharedPreferences(Constants.FIRST_TIME_LOGIN_PREFERENCE, getApplicationContext().MODE_PRIVATE);
        Boolean isFirstTime = sharedPref.getBoolean(Constants.FIRST_TIME_LOGIN_KEY + "_" + Constants.VALIDATE_USER, false);
        LogUtil.d(TRACE_TAG , " 1 Constants.VALIDATE_USER : " + Constants.VALIDATE_USER);
        LogUtil.d(TRACE_TAG, " isFirstTime boolean : " + isFirstTime);

        try {
            DatabaseAdapter dbAdapter = new DatabaseAdapter(context);
            dbAdapter.open();
            int isPin = dbAdapter.isOfflinePinSet(Constants.USER);
            LogUtil.d(TRACE_TAG, " ispin :: "+isPin);
            dbAdapter.close();
            if (Validation.isNetworkAvailable(this) && (isFirstTime || isPin == 0)) {
                LogUtil.d(TRACE_TAG, " Db doesn't exist, showing offline pin dialog");
                Intent pinIntent = new Intent(EjobList.this, OfflinePinDialog.class);
                startActivity(pinIntent);
            } else {
                LogUtil.d(TRACE_TAG, " Db exists NOT showing offline pin dialog");
            }
        } catch (Exception e) {
            Log.d(TRACE_TAG, " Exception " + e);
        } finally {
            SharedPreferences.Editor editor = sharedPref.edit();
            LogUtil.d(TRACE_TAG, " 2 Constants.VALIDATE_USER : " + Constants.VALIDATE_USER);
            editor.putBoolean(Constants.FIRST_TIME_LOGIN_KEY + "_" + Constants.VALIDATE_USER, false);
            editor.commit();
        }

        // Land Scape (View Specific Search Result) STARTS
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            try {
                TextView tireManagementLabel = (TextView) findViewById(R.id.textView2);
                TextView incompleteJobsLabel = (TextView) findViewById(R.id.textView3);
                TextView completedJobsLabel = (TextView) findViewById(R.id.textView4);
                TextView deletedJobsLabel = (TextView) findViewById(R.id.textView5);

                tireManagementLabel.setText(mDBHelper
                        .getLabel(Constants.TIRE_MANAGEMENT_LABEL));
                incompleteJobsLabel.setText(mDBHelper
                        .getLabel(Constants.INCOMPLETE_JOBS_LABEL));
                completedJobsLabel.setText(mDBHelper
                        .getLabel(Constants.COMPLETED_JOBS_LABEL));
                deletedJobsLabel.setText(mDBHelper
                        .getLabel(Constants.DELETED_JOBS_LABEL));

                rlTireManagementJobs = (RelativeLayout) findViewById(R.id.RelativeLayout2);
                rlIncompleteJobs = (RelativeLayout) findViewById(R.id.RelativeLayout3);
                rlCompletedJobs = (RelativeLayout) findViewById(R.id.RelativeLayout4);
                rlDeletedJobs = (RelativeLayout) findViewById(R.id.RelativeLayout5);
                rlTireManagementJobs
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mJobStatus = 5;
                                displaySelectedJobs(rlTireManagementJobs,
                                        mJobStatus);
                            }
                        });
                rlIncompleteJobs.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mJobStatus = 1;
                        displaySelectedJobs(rlIncompleteJobs, mJobStatus);
                    }
                });
                rlCompletedJobs.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mJobStatus = 0;
                        displaySelectedJobs(rlCompletedJobs, mJobStatus);
                    }
                });
                rlDeletedJobs.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mJobStatus = 4;
                        displaySelectedJobs(rlDeletedJobs, mJobStatus);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // Land Scape (View Specific Search Result) ENDS
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            if (intent.getExtras() != null) {
                startedFrom = intent.getExtras().getString("sentfrom");
            }
            if (startedFrom != null) {
                if (startedFrom.equals("settings")) {
                    mLang = intent.getExtras().getString("language");
                } else if (!startedFrom.equals("ejobform")) {
                    moveInternalDBToDBFile();
                }
            }
            Settings setting = mDBHelper.getSettingsInfo();
            if (mLang != null && !mLang.equals(setting.getLanguage())) {
                /*
                setting.setLanguage(mLang);
                try {
                    if (Validation.isNetworkAvailable(this)) {
                        if (!isLoading) {
                            isLoading = true;
                            showProgress();
                        }
                        mStartTime = System.currentTimeMillis();
                        Handler messageHandler = new MessageHandler(this);
                        Intent startService = new Intent(context,
                                EjobSyncService.class);
                        startService.putExtra(SyncUtils.SYNC_OPTIONS_KEY,
                                SyncOptions.TRANSLATE_SYNC.getValue());
                        Log.i("EjobList", "Starting Translation Sync::: "
                                + mLang);
                        startService.putExtra(SyncUtils.SYNC_LANG_KEY, mLang);
                        startService.putExtra("MESSENGER", new Messenger(
                                messageHandler));
                        context.startService(startService);
                    } else {
                        CommonUtils
                                .createInformationDialog(
                                        this,
                                        com.goodyear.ejob.util.Constants.ERROR_NO_NETWORK_TO_SYNC);
                    }
                } catch (Exception e) {
                    Log.e("HomeActivity--", "could not update langauge");
                }*/
            }
            // get sync time info from db
            mSyncTime = mDBHelper.getSyncTimeInfo();
            try {
                if (Math.abs(DateTimeUTC
                        .daysBetweenCurDateTimeNgivenDateTime(Long
                                .parseLong(mSyncTime.getValue()))) > 365) {
                    /*
                    LogUtil.i("HomeActivity--", "Download Sync - (365)");
                    LogUtil.i("HomeActivity--", "Difference : " + String.valueOf(Math.abs(DateTimeUTC.
                            daysBetweenCurDateTimeNgivenDateTime(Long.parseLong(mSyncTime.getValue())))));
                    Constants.APP_UPGRADE_STATUS = false;
                    Constants.Force_Download_Sync_STATUS = true;
                    try {
                        if (!isLoading) {
                            isLoading = true;
                            showProgress();
                        }
                        mStartTime = System.currentTimeMillis();

                        Handler messageHandler = new MessageHandler(this);
                        Intent startService = new Intent(context,
                                EjobSyncService.class);
                        startService.putExtra(SyncUtils.SYNC_OPTIONS_KEY,
                                SyncOptions.DOWNLOAD_SYNC.getValue());
                        startService.putExtra("MESSENGER", new Messenger(
                                messageHandler));
                        context.startService(startService);
                    } catch (Exception e) {
                        Log.e("HomeActivity--", "could not Sync");
                    }
                    */
                }
                // if coming from login
                else if (startedFrom != null && startedFrom.equals("login")
                        && Validation.isNetworkAvailable(this)) {
                    /*
                    Date lastSyncDate = DateTimeUTC
                            .convertMillisecondsToUTCDate(Long
                                    .parseLong(mSyncTime.getValue()));
                    Calendar cal1 = Calendar.getInstance();
                    Calendar cal2 = Calendar.getInstance();
                    cal1.setTime(lastSyncDate);
                    cal2.setTime(new Date());
                    dayDifferenceInSync = cal1.get(Calendar.YEAR) == cal2
                            .get(Calendar.YEAR)
                            && cal1.get(Calendar.DAY_OF_YEAR) != cal2
                            .get(Calendar.DAY_OF_YEAR);
                    if (dayDifferenceInSync) {
                        try {
                            if (!isLoading) {
                                isLoading = true;
                                showProgress();
                            }
                            Constants.Force_Download_Sync_STATUS = false;
                            LogUtil.i("HomeActivity--", "initial or Daily sync upload");
                            mStartTime = System.currentTimeMillis();
                            Handler messageHandler = new MessageHandler(this);
                            Intent startService = new Intent(context,
                                    EjobSyncService.class);
                            startService.putExtra(SyncUtils.SYNC_OPTIONS_KEY,
                                    SyncOptions.UPLOAD_SYNC.getValue());
                            startService.putExtra("MESSENGER", new Messenger(
                                    messageHandler));
                            context.startService(startService);
                        } catch (Exception e) {
                            Log.e("HomeActivity--", "could not Sync");
                        }
                    }
                    */
                }
                // ForceSync just after PlayStore Update
                else if (Constants.APP_UPGRADE_STATUS) {/*

                    try {
                        if (!isLoading) {
                            isLoading = true;
                            showProgress();
                        }
                        Constants.Force_Download_Sync_STATUS = false;
                        LogUtil.i("HomeActivity--", "force sync");
                        mStartTime = System.currentTimeMillis();
                        Handler messageHandler = new MessageHandler(this);
                        Intent startService = new Intent(context,
                                EjobSyncService.class);
                        startService.putExtra(SyncUtils.SYNC_OPTIONS_KEY,
                                SyncOptions.UPLOAD_SYNC.getValue());
                        startService.putExtra("MESSENGER", new Messenger(
                                messageHandler));
                        context.startService(startService);
                    } catch (Exception e) {
                        Log.e("HomeActivity--", "could not Sync");
                    }
                */}
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            if (EjobFormActionActivity.bool) {
                System.out.println("Constants.JOBREFNUMBER--"
                        + Constants.JOBREFNUMBER);
                loadRemovalReasonDialog(String.valueOf(Constants.JOBREFNUMBER));
            }

        } else {
            //During Orientation change dialog will be recreated
            //Resetting IS_SHOWING_DIALOG Constants
            if (Constants.APP_UPGRADE_STATUS || Constants.Force_Download_Sync_STATUS) {
                Constants.IS_SHOWING_DIALOG = false;
            }
            isLoading = savedInstanceState.getBoolean("loading");
            if (isLoading) {
                showProgress();
            }

            if (savedInstanceState.containsKey(SELECTED_ITEMS)) {
                mSelectedItems = savedInstanceState
                        .getIntegerArrayList(SELECTED_ITEMS);
            }
            if (savedInstanceState.getBoolean("mLogoutAlertDialog")) {
                eJobLogOut();
            } else if (savedInstanceState.getBoolean("mLogoutPopUp")) {
                logoutPopUp();
            }
            if (savedInstanceState.getBoolean("mAlertDialog")
                    || EjobFormActionActivity.bool) {
                LogUtil.e("EjobList", "Constants.JOBREFNUMBER::"
                        + Constants.JOBREFNUMBER);
                loadRemovalReasonDialog(String.valueOf(Constants.JOBREFNUMBER));
            }
        }
        mLvJoblist = (ListView) findViewById(R.id.lv_joblist);
        mLvJoblist.setDivider(null);
        mLvJoblist.setDividerHeight(0);
        onClickListItems();
        getJobSummaryInfo();
        // getJOBRefNumber();
        getJobList();
            if (EjobFormActionActivity.bool) {
                System.out.println("Constants.JOBREFNUMBER--"
                        + Constants.JOBREFNUMBER);
                // loadRemovalReasonDialog(String.valueOf(Constants.JOBREFNUMBER));
                // EjobFormActionActivity.bool = false;
            }
            if (savedInstanceState != null) {
            int sortingImgId = savedInstanceState.getInt("sorting_img_id",
                    mRefImage.getId());
            mSortingKey = savedInstanceState.getString("sorting_key", "ref_no");
            mImageViewSorting = (ImageView) findViewById(sortingImgId);
            Constants.ASCDESC = !Constants.ASCDESC;
            prepareSort(mImageViewSorting, mSortingKey);
            mAdapter.setCheckedJobList(savedInstanceState
                    .getStringArrayList(CHECKED_ITEMS));
            mDeviceRotated = true;
        }
                if (savedInstanceState != null
                        && savedInstanceState.getBoolean("isLockDialogShown")) {
                    alertDialogueWithOkCancel();
                }
        restoreDialogState(savedInstanceState);
        moveInternalDBToDBFile();
        UpdateJOBStatusAfterSync();
        // Called to refresh the DB
        getJobList();

        // get last APP update time from settings
        SharedPreferences preferences = getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);
        String versionFromSetting = (preferences.getString("LastAppUpgradeDateTime", ""));

        // compare if APP was updated after last sync with server then sync
        // user db with server
        if (mSyncTime != null
                && !(Long.parseLong(versionFromSetting) < Long
                .parseLong(mSyncTime.getValue()))) {
            /*Halosys
            startSyncActivity(false);*/
        }
        /**
         * CR 505
         */
        if (null == Constants.mPWTList && null != retrieveMountPWTTyresFromSharedPref(context)) {
            Constants.mPWTList = new ArrayList<Tyre>();
            Constants.mPWTList = retrieveMountPWTTyresFromSharedPref(context);
        }
    }

    @Override
    public void onUserInteraction() {
        InactivityUtils.updateActivityOfUser();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LogUtil.d("EjobList", "onStart()....");
        LogoutHandler.setCurrentActivity(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void loadGlobalLabels() {
        Constants.sLblPleasePairTLogik = mDBHelper
                .getLabel(Constants.PLEASE_PAIR_TLOGIK);
        Constants.sLblMultiplePaired = mDBHelper
                .getLabel(Constants.MULTIPLE_PAIRED_DEVICES_FOUND);
        Constants.sLblNoPairedDevices = mDBHelper
                .getLabel(Constants.NO_PAIRED_DEVICES);
        Constants.sLblNoNetwork = mDBHelper.getLabel(Constants.NO_NETWORK);
        Constants.sLblWheelPos = mDBHelper.getLabel(Constants.WHEEL_POS);
        Constants.sLblDeviceDontSupportBT = mDBHelper
                .getLabel(Constants.DEVICE_DOES_NOT_SUPPORT_BT);
        Constants.sLblEnterNskValues = mDBHelper
                .getLabel(Constants.ENTER_NSK_VALUES);
        Constants.sLblEnterSerialNumber = mDBHelper
                .getLabel(Constants.PLEASE_ENTER_SERIAL_NUM);
        Constants.sLblCheckPressureValue = mDBHelper
                .getLabel(Constants.CHECK_PRESSURE_VALUE);
        Constants.sLblNothingSelected = mDBHelper
                .getLabel(Constants.NOTHING_SELECTED);
        Constants.sLblNoPairedDevices = mDBHelper
                .getLabel(Constants.NO_PAIRED_DEVICES);
        mSyncLabel = mDBHelper.getLabel(Constants.SYNC_LABEL);
        mAboutLabel = mDBHelper.getLabel(Constants.ABOUT_LABEL);
        mHelpLabel = mDBHelper.getLabel(Constants.HELP_LABEL);
        mLogoutLabel = mDBHelper.getLabel(Constants.LOGOUT_LABEL);
        mSettingsLabel = mDBHelper
                .getLabel(Constants.SETTINGS_ACTIVITY_NAME_LABEL);
        mLogoutConfirmationLabel = mDBHelper
                .getLabel(Constants.LOGOUT_CONFIRMATION_LABEL);
        Constants.sLblCheckPressureValue = mDBHelper
                .getLabel(Constants.CHECK_PRESSURE_LABEL);
        Constants.ERROR_DB_DELETED = mDBHelper
                .getLabel(Constants.DATABASE_FILE_DELETED_LABEL);
        Constants.ERROR_NETWORK_SYNC_ERROR = mDBHelper
                .getLabel(Constants.CHECK_INTERNET_CONNECTION_LABEL);
    }

    /**
     * Show progress
     */
    private void showProgress() {
        if (mGYProgressDialog == null) {
            mGYProgressDialog = new GYProgressDialog(EjobList.this);
        }
        mGYProgressDialog.show();
    }

    /**
     * Dismiss progress
     */
    private void dismissProgress() {

        try {
            if ((this.mGYProgressDialog != null)
                    && this.mGYProgressDialog.isShowing()) {
                mGYProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.mGYProgressDialog = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Constants.FROM_EJOBLIST = false;
        // saveJobsWhenAppKillsForcefully();
        closeDB();
    }

    @Override
    protected void onResume() {
        super.onResume();
        openDB();
        Constants.FROM_EJOBLIST = true;
    }

    /**
     * all screen labels load from DataBase translation table
     */
    private void loadFromDB() {
        try {
            mLicencePlate = (TextView) findViewById(R.id.TextView05);
            customerName = (TextView) findViewById(R.id.TextView03);
            mDate = (TextView) findViewById(R.id.TextView02);
            refNO = (TextView) findViewById(R.id.TextView04);

            mLicencePlate.setText(mLicencePlateNOLabel);
            customerName.setText(mCusNameLabel);
            mDate.setText(mDateLabel);
            refNO.setText(mRefNOLabel);

            mLicencePlate.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    prepareSort(mLinumImage, "licence");
                }
            });
            customerName.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    prepareSort(mCusImage, "cname");
                }
            });
            mDate.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    prepareSort(mDateImage, "date");
                }
            });
            refNO.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    prepareSort(mRefImage, "ref_no");
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Sort list items by user selection
    private void prepareSort(ImageView imageView, String toSort) {
        if (imageView == null) {
            return;
        }
        mLinumImage.setVisibility(View.INVISIBLE);
        mCusImage.setVisibility(View.INVISIBLE);
        mDateImage.setVisibility(View.INVISIBLE);
        mRefImage.setVisibility(View.INVISIBLE);
        imageView.setVisibility(View.VISIBLE);
        mImageViewSorting = imageView;
        mSortingKey = toSort;
        if (Constants.ASCDESC) {
            // imageView.setImageResource(R.drawable.asc);
            imageView.setImageResource(R.drawable.desc);
            Constants.ASCDESC = false;
        } else {
            // imageView.setImageResource(R.drawable.desc);
            imageView.setImageResource(R.drawable.asc);
            Constants.ASCDESC = true;
        }
        Collections.sort(sListItems, new MapComparator(toSort));
        addJobListToAdapterView();
    }

    // get Job Summery info
    private void getJobSummaryInfo() {
        try {
            bundle = getIntent().getExtras();
            if (bundle != null) {
                /*
				 * if(bundle.containsKey("CustomerName")) {
				 * bundle.getString("CustomerName"); }
				 * if(bundle.containsKey("bundlecustomerID")) {
				 * bundle.getString("bundlecustomerID"); }
				 */

                mHomeSerialNum = bundle.getString("bundleSerialNum");
                mHomeWorkOrderNuber = bundle.getString("bundleWorkOrderNuber");
                mHomeRepairCompanyName = bundle
                        .getString("bundleRepairCompanyName");
                mHomeResultDesign = bundle.getString("bundleResultDesign");
                mHomeCurStatus = bundle.getString("curStatus");
                mHomeDescription = bundle.getString("Description");// Description

                mLinearMenu = (LinearLayout) findViewById(R.id.menu_items);

                mLvJoblist = (ListView) findViewById(R.id.lv_joblist);
                mLvJoblist.setDivider(null);
                mLvJoblist.setDividerHeight(0);
            }

        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    // validations for 160 days
    public void startNewJob(View view) {
        Constants.COMESFROMUPDATE = false;
        DatabaseAdapter.userValidate(Constants.USER);
        SyncTime db_syncTime = mDBHelper.getSyncTimeInfo();
        if (Math.abs(DateTimeUTC.daysBetweenCurDateTimeNgivenDateTime(Long
                .parseLong(db_syncTime.getValue()))) < 160) {
            try {
                Constants.TM_NOT_ALLOWED = false;
                Intent newJob = new Intent(this, LaunchNewJob.class);
                startActivity(newJob);
            } catch (Exception e) {
                Log.e("HomeActivity--", "Incorrect sync time" + mSyncTime);

            }
        } else {
            CommonUtils.notify(strAPPLICATIONDB, getApplicationContext());
			/*
			 * Toast.makeText( getApplicationContext(),
			 * "Application DB3 file is older than 160 days. Please Sync Now to get Update!"
			 * , Toast.LENGTH_SHORT).show();
			 */
        }
    }

    // validations 160 days for TireManagement
    public void loadTyreManagement(View view) {

        Constants.GETJOBREFFNO = true;
        Constants.RELOAD_SCREEN = false;
        Constants._mBundleActionTypeVec.clear();
        Constants._mBundleOperationIDVec.clear();
        Constants._repairCompVec.clear();
        Constants._workOrderVec.clear();
        Constants._mBundleCurrentstatusVec.clear();
        DatabaseAdapter.userValidate(Constants.USER);
        SyncTime db_syncTime = mDBHelper.getSyncTimeInfo();
        if (Math.abs(DateTimeUTC.daysBetweenCurDateTimeNgivenDateTime(Long
                .parseLong(db_syncTime.getValue()))) < 160) {
            try {
                Constants.RELOAD_REFF = false;
                Constants.TM_NOT_ALLOWED = true;
                Intent intent = new Intent(EjobList.this,
                        TyreManagementActivity.class);
                startActivity(intent);
            } catch (Exception e) {
                Log.e("HomeActivity--", "Incorrect sync time" + mSyncTime);

            }
        } else {
            CommonUtils.notify(strAPPLICATIONDB, getApplicationContext());
			/*
			 * Toast.makeText( getApplicationContext(),
			 * "Application DB3 file is older than 160 days. Please Sync Now to get Update!"
			 * , Toast.LENGTH_SHORT).show();
			 */
        }
    }

    /**
     * Fetch all joblist
     */
    public void getJobList() {
        sListItems.removeAll(sListItems);
        Cursor mCursor = null;
        try {
            mCursor = mDBHelper.getRefNoFromAccountTable();// get updated
            // jobrefNO
            if (CursorUtils.isValidCursor(mCursor)) {
                mCursor.moveToFirst();
                Constants.JOBREFNUMPASSFROMEJOB = mCursor.getInt(mCursor
                        .getColumnIndex("jobRefNumber"));
                Constants.JOBREFNUMBERVALUE = Constants.JOBREFNUMPASSFROMEJOB;
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            CursorUtils.closeCursor(mCursor);
        }
        Cursor c = null;
        try {
            c = mDBHelper.Job_GetJobList();
            if (CursorUtils.isValidCursor(c)) {

                if (c.moveToFirst()) {
                    do {

                        HashMap<String, String> joblistmap = new HashMap<String, String>();

                        String strlicensenumber = c.getString(0);
                        int sapcontractnumber = c.getInt(1);
                        int status = c.getInt(3);
                        long timefinisshed;
                        if (status == 5) {// For tire management it should show
                            // finish time
                            timefinisshed = c.getLong(c
                                    .getColumnIndex("TimeFinished"));
                        } else {
                            timefinisshed = c.getLong(c
                                    .getColumnIndex("TimeStarted"));
                        }
                        mRefnoInt = c.getInt(4);
                        String jobId = String.valueOf(c.getInt(5));
                        String jobExternalId = c.getString(6);
                        Constants.JOBREFNUMPASSFORNEWJOB = mRefnoInt;
                        DateTime dateTime = DateTimeUTC
                                .convertMillisecondsToUTCDateTime(timefinisshed);

                        String strsapcontractname = GetCustomerName(
                                sapcontractnumber, EjobList.this);
                        String strtimefinisshed = dateTime.toString();// String.valueOf(timefinisshed);

                        String strstatus = String.valueOf(status);
                        Constants.STATUSVALUES = strstatus;
                        String strrefno = String.valueOf(mRefnoInt);

                        joblistmap.put("licence", strlicensenumber);//
                        joblistmap.put("cname", strsapcontractname);//
                        joblistmap.put("date",
                                strtimefinisshed.substring(0, 10));
                        joblistmap.put("timefinished",
                                String.valueOf(timefinisshed));
                        joblistmap.put("status", strstatus);
                        joblistmap.put("ref_no", strrefno);
                        joblistmap.put("id", jobId);
                        joblistmap.put("jobexternalid", jobExternalId);
                        joblistmap.put("check_status", "false");
                        sListItems.add(joblistmap);
                        Collections.sort(sListItems,
                                new MapComparator("ref_no"));
                        mRefImage.setImageResource(R.drawable.asc);
						/*
						 * long currenttime = System.currentTimeMillis(); long
						 * timeInMilliseconds = dateTime.getMillis(); if (status
						 * != 4 && (currenttime - timeInMilliseconds >
						 * 259200000)) {
						 * CommonUtils.notify(mDBHelper.getLabel(5), context); }
						 */

                    } while (c.moveToNext());
                }
            }
        } catch (Exception e) {
        } finally {
            CursorUtils.closeCursor(c);
        }
        addJobListToAdapterView();
    }

    /**
     * add all jobs to adapter and display
     */
    private void addJobListToAdapterView() {
        mAdapter = new EjobListAdapter(EjobList.this, sListItems,
                mDeviceRotated);
        if (mSelectedItems != null) {
            mAdapter.setSelectedItemIds(mSelectedItems);
        }
        LogUtil.TraceInfo(TRACE_TAG, "Job List", "Data : " + sListItems.toString(), false, true, false);
        mLvJoblist.setAdapter(mAdapter);
    }

    // Getting the specific job list i.e. job status wise(STARTS)
    public void getSpecificJobList(int jobStatus) {
        Cursor c = null;
        openDB();
        try {
            sListItems.removeAll(sListItems);

            c = mDBHelper.Job_GetJobList(jobStatus);
            if (CursorUtils.isValidCursor(c)) {
                if (c.moveToFirst()) {
                    do {

                        HashMap<String, String> joblistmap = new HashMap<String, String>();

                        String strlicensenumber = c.getString(0);
                        int sapcontractnumber = c.getInt(1);
                        int status = c.getInt(3);
                        long timefinisshed;
                        if (status == 5) {// For tire management it should show
                            // finish time
                            timefinisshed = c.getLong(c
                                    .getColumnIndex("TimeFinished"));
                        } else {
                            timefinisshed = c.getLong(c
                                    .getColumnIndex("TimeStarted"));
                        }
                        mRefnoInt = c.getInt(4);
                        String jobId = String.valueOf(c.getInt(5));
                        String jobExternalId = c.getString(6);
                        Constants.JOBREFNUMPASSFORNEWJOB = mRefnoInt;
                        DateTime dateTime = DateTimeUTC
                                .convertMillisecondsToUTCDateTime(timefinisshed);

                        String strsapcontractname = GetCustomerName(
                                sapcontractnumber, EjobList.this);
                        String strtimefinisshed = dateTime.toString();// String.valueOf(timefinisshed);

                        String strstatus = String.valueOf(status);
                        Constants.STATUSVALUES = strstatus;
                        String strrefno = String.valueOf(mRefnoInt);

                        joblistmap.put("licence", strlicensenumber);//
                        joblistmap.put("cname", strsapcontractname);//
                        joblistmap.put("date",
                                strtimefinisshed.substring(0, 10));
                        joblistmap.put("timefinished",
                                String.valueOf(timefinisshed));
                        joblistmap.put("status", strstatus);
                        joblistmap.put("ref_no", strrefno);
                        joblistmap.put("id", jobId);
                        joblistmap.put("jobexternalid", jobExternalId);
                        joblistmap.put("check_status", "false");
                        // if(!strstatus.equals("4"))
                        sListItems.add(joblistmap);
                        Collections.sort(sListItems,
                                new MapComparator("ref_no"));
                        mRefImage.setImageResource(R.drawable.asc);
                        // refNO.setTextColor(Color.parseColor("#F78181"));
                    } while (c.moveToNext());
                }
            }
        } catch (Exception e) {
            System.out
                    .println("Opps,Exception in getSpecificJobList(int jobStatus) : "
                            + e);
            e.printStackTrace();
        } finally {
            CursorUtils.closeCursor(c);
        }

        addJobListToAdapterView();
    }

    // Getting the specific job list i.e. job status wise(ENDS)

    // removal reason dialogue
    public void removeDialoguePopUpAtSingleSwipe(int position) {
        if (sListItems.size() != 0) {
            mRefNOForDelete = sListItems.get(position).get("ref_no");
            mJobExternalId = sListItems.get(position).get("jobexternalid");
            EditJob.sJobItemToDelete = mRefNOForDelete;
            loadRemovalReasonDialog(mRefNOForDelete);
            getJobList();
        }
    }

    /**
     * Getting customer name from DB
     */
    public static String GetCustomerName(int number, Context context) {

        String strcustomername = null;
        Cursor c = null;
        DatabaseAdapter mdb = new DatabaseAdapter(context);
        mdb.open();
        try {
            c = mdb.ContractGetName(number);
            if (CursorUtils.isValidCursor(c)) {
                c.moveToFirst();
                int numRows = c.getCount();
                if (numRows > 0) {
                    int indexproductjson = c.getColumnIndex("CustomerName");

                    strcustomername = c.getString(indexproductjson);

                }
            }
        } catch (SQLException e) {
        } finally {
            CursorUtils.closeCursor(c);
            mdb.close();
        }
        return strcustomername;
    }

    // settings click view
    public void settingsLoad(View view) {
        mLinearMenu.setVisibility(LinearLayout.GONE);
        Intent openSettings = new Intent(this, SettingsActivity.class);
        startActivity(openSettings);
    }

    /**
     * Getting external ID from DB
     */
    public static String GetExternalIDandVendorDetail(int Refnumber,
                                                      Context context) {
        String extID = null;
        Cursor c = null;
        DatabaseAdapter db = new DatabaseAdapter(context);

        db.open();
        try {
            c = db.JobextID(Refnumber);
            if (CursorUtils.isValidCursor(c)) {
                c.moveToFirst();
                int numRows = c.getCount();
                if (numRows > 0) {
                    int indexproductjson = c.getColumnIndex("ExternalID");
                    extID = c.getString(indexproductjson);

                    int indexVendorDetail = c.getColumnIndex("VendorDetail");

                    mVendorDetail = c.getString(indexVendorDetail);
                }
            }
        } catch (SQLException e) {
        } finally {
            CursorUtils.closeCursor(c);
            db.close();
        }
        return extID;
    }

    // getting tire ID from DB
    public void getTireActionWorkOrderNumber(String strexternalID,
                                             Context context) {
        Cursor c = null;
        DatabaseAdapter db = new DatabaseAdapter(context);
        db.open();
        try {
            c = db.TyreID_ActionType(strexternalID);
            if (CursorUtils.isValidCursor(c)) {
                c.moveToFirst();
                int numRows = c.getCount();
                if (numRows > 0) {
                    int indexTyreID = c.getColumnIndex("TyreID");
                    sTyreid = c.getString(indexTyreID);
                }
            }
        } catch (SQLException e) {
        } finally {
            CursorUtils.closeCursor(c);
            db.close();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.ejob_list, menu);
        if (mSyncLabel != null) {
            menu.findItem(R.id.action_sync).setTitle(mSyncLabel);
        }
        if (mHelpLabel != null) {
            menu.findItem(R.id.action_help).setTitle(mHelpLabel);
        }
        if (mAboutLabel != null) {
            menu.findItem(R.id.action_about).setTitle(mAboutLabel);
        }
        if (mSettingsLabel != null) {
            menu.findItem(R.id.action_settings).setTitle(mSettingsLabel);
        }
        if (mLogoutLabel != null) {
            menu.findItem(R.id.action_logout).setTitle(mLogoutLabel);
        }
        display();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_sync) {
            //User Trace logs
            LogUtil.TraceInfo(TRACE_TAG, "Option", "Sync", true, true, false);
            SharedPreferences preferences = getSharedPreferences(
                    Constants.GOODYEAR_CONF, 0);
            mDBHelper.open();
            for (int i = 0; i < sListItems.size(); i++) {
                if (sListItems.get(i).get("check_status").equals("true")) {
                    try {
                        //Key to store PARTIALLY_INSPECTED_WITH_OPERATION information
                        String PARTIAL_KEY = "PARTIALLY_INSPECTED_WITH_OPERATION" + "_" + Constants.VALIDATE_USER + "_" + sListItems.get(i).get("ref_no");
                        try {
                            LogUtil.DBLog("EjobList", "onOptionsItemSelected", "PARTIALLY_INSPECTED_WITH_OPERATION key value : " + PARTIAL_KEY);
                        } catch (Exception e) {
                            LogUtil.DBLog("EjobList", "onOptionsItemSelected", "PARTIALLY_INSPECTED_WITH_OPERATION key value : " + e.getMessage());
                        }
                        if (preferences.getInt(PARTIAL_KEY, 8) == InspectionState.ONLY_OPERATION_PARTIAL
                                || preferences.getInt(PARTIAL_KEY, 8) == InspectionState.OPERATION_WITH_INSPECTION_PARTIAL
                                || preferences.getInt(PARTIAL_KEY, 8) == InspectionState.ONLY_INSPECTION_PARTIAL) {
                            mDBHelper.makeJobItemImperfectBasedOnJobReferenceNumber(sListItems.get(i).get("ref_no"));

                            //Set Is perfect value for Job reference
                            mDBHelper.makeJobCorrectionImperfectBasedOnJobReferenceNumber(sListItems.get(i).get("ref_no"));
                        }
                    } catch (Exception e) {
                        LogUtil.DBLog("eJob List", "onOptionsItemSelected", e.getMessage());
                    }

                }

            }

            /**
             * 667 : only the final Swap data should be sent to middleware / SAP
             * 656 : inspection created for the removed tyre
             */
            for (int i = 0; i < sListItems.size(); i++) {
                try {
                    mDBHelper.makeSwapJobItemAndUnmountedTyreInspectionJobItemImperfectBasedOnJobNumber(sListItems.get(i).get("ref_no"));
                } catch (Exception e) {
                    LogUtil.DBLog("eJob List", "onOptionsItemSelected : makeSwapJobItemImperfectBasedOnJobNumber : Exception - ", e.getMessage());
                }
            }

            /**
             * Bug 683: Making previous jobItems of dismounted tyre as imperfect
             * eJob-83 : Change IsPerfect status of unwanted Job correction.
             */
            for (int i = 0; i < sListItems.size(); i++) {
                try {
                    mDBHelper.makeDismountedTyreJobItemImperfect(sListItems.get(i).get("ref_no"));
                    //change IsPerfect status of unwanted Job correction.
                    mDBHelper.makeUnwantedJobCorrectionImperfectBasedOnJobReferenceNumber(sListItems.get(i).get("ref_no"));
                } catch (Exception e) {
                    LogUtil.DBLog("eJob List", "onOptionsItemSelected : makeDismountedTyreJobItemImperfect : Exception - ", e.getMessage());
                }
            }

            /**
             * CR 505 : PWT are not available if a job that uses one has been deleted
             * clearing PWT Tyre List if it is not null or else initialize
             */
            if(Constants.PWTTyreList!=null)
            {
                Constants.PWTTyreList.clear();
            }
            else
            {
                Constants.PWTTyreList = new ArrayList<>();
            }
         /*   *//**
             * addPWTTyreToDeletedJobPWTTyreList Method call moved to delete job module : to add support for immediate availability of PWT tyre after job deleted
             * CR 505 : PWT are not available if a job that uses one has been deleted
             * Filtering Deleted jobs from other job and  Adding deleted job PWT Tyre to PWT Tyre List
             */
            for (int i = 0; i < sListItems.size(); i++) {
                if(sListItems.get(i).get("status").equalsIgnoreCase("4")) {
                    try {
                        mDBHelper.addPWTTyreToDeletedJobPWTTyreList(sListItems.get(i).get("ref_no"));
                    } catch (Exception e) {
                        LogUtil.DBLog("eJob List", "onOptionsItemSelected : addPWTTyreToDeletedJobPWTTyreList : Exception - ", e.getMessage());
                    }
                }
            }

            /**
             * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
             * If material and/or SN is changed when a tyre is removed - this tyre should not be made available immediately
             * Logic to populate IsTyreCorrected value to maintain material and/or SN change info in tyre table
             * Logic to update JobID for dismounted reusable tyre
             */
            for (int i = 0; i < sListItems.size(); i++) {
                try{
                    if(sListItems.get(i).get("status").equalsIgnoreCase("2") ||sListItems.get(i).get("status").equalsIgnoreCase("0"))
                    {
                        //Logic to populate IsTyreCorrected value to maintain material and/or SN change info in tyre table
                        mDBHelper.populateIsTyreCorrectedValueForDismountedTyre(sListItems.get(i).get("ref_no"));
                        //Logic to update JobID for dismounted reusable tyre
                        mDBHelper.updateJobIDForDismountedReusableTyre(sListItems.get(i).get("ref_no"));
                    }
                }catch (Exception e)
                {
                    LogUtil.DBLog("eJob List", "onOptionsItemSelected : populateIsTyreCorrectedValueForDismountedTyre : Exception - ", e.getMessage());
                }
            }

            getCheckedItemsForSync();
            startSyncActivity();
            return true;
        } else if (id == R.id.action_about) {
            //User Trace logs
            LogUtil.TraceInfo(TRACE_TAG, "Option", "About", true, true, false);
            aboutScreen();
            return true;
        } else if (id == R.id.action_logout) {
            //User Trace logs
            LogUtil.TraceInfo(TRACE_TAG, "Option", "Logout", true, true, false);
            eJobLogOut();
            return true;
        } else if (id == R.id.action_help) {
            //User Trace logs
            LogUtil.TraceInfo(TRACE_TAG, "Option", "Help", true, true, false);
            helpScreen();
            return true;
        } else if (id == R.id.action_settings) {
            //User Trace logs
            LogUtil.TraceInfo(TRACE_TAG, "Option", "Settings", true, true, false);
            DatabaseAdapter check = new DatabaseAdapter(this);
            check.createDatabase();
            check.open();
            try {
                // check if translation table contains any record
                if (!check.getLabel(1).equals("")) {
                    Intent openSettings = new Intent(this,
                            SettingsActivity.class);
                    startActivity(openSettings);
                }
            } catch (Exception e) {
                Log.e(TRACE_TAG, " Exception : " + e.getMessage());
                check.close();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startSyncActivity() {
        /*For Halosys
        startSyncActivity(true);*/
    }

    private void startSyncActivity(boolean isSelectedItem) {
        if (Validation.isNetworkAvailable(this)) {
            finish();
            ArrayList<String> checkedItems = mAdapter.getSelectedJobs();
            Intent intent = new Intent(this, SyncActivity.class);

            Bundle bundle = new Bundle();
            bundle.putStringArrayList(CHECKED_ITEMS, checkedItems);
            bundle.putString("sentfrom", "ejobList");
            if (isSelectedItem) {
                bundle.putIntegerArrayList("SelectedItems",
                        mAdapter.getSelectedItemIds());
            }
            intent.putExtras(bundle);

            overridePendingTransition(0, 0);
            /*Halosys
            startActivity(intent);*/
        } else {
            showAlertDialog(Constants.ERROR_NO_NETWORK_TO_SYNC, null);
        }
    }

    /**
     * Help Screen
     */
    private void helpScreen() {
        Intent about = new Intent(this, HelpScreen.class);
        startActivity(about);
    }

    /**
     * About Screen
     */
    private void aboutScreen() {
        Intent about = new Intent(this, AboutScreen.class);
        startActivity(about);

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            setHasOptionsMenu(true);
            View rootView = inflater.inflate(R.layout.activity_ejob_list,
                    container, false);
            return rootView;
        }
    }

    // display three dots (overflow icon)irrespective of device
    public void display() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            // Ignore
        }
    }

    // logout from app
    private void eJobLogOut() {

        String mYes = mDBHelper
                .getLabel(Constants.YES_LABEL);
        String nNo = mDBHelper
                .getLabel(Constants.NO_LABEL);
        LayoutInflater li = LayoutInflater.from(EjobList.this);
        View promptsView = li
                .inflate(R.layout.dialog_logout_confirmation, null);
        mLogoutMessage =(TextView) promptsView.findViewById(R.id.logout_msg);
        if(mLogoutMessage!=null) {
            mLogoutMessage.setText(mDBHelper
                    .getLabel(Constants.LOGOUT_CONFIRMATION_LABEL));
        }

        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
                EjobList.this);
        alertDialogBuilder.setView(promptsView);

        // update user activity for dialog layout
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(mYes,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                // update user activity on button click
                                alertDialogBuilder.updateInactivityForDialog();

                                Constants._CHECKFORLOGINAUTOFILL = true;
                                DatabaseAdapter db = new DatabaseAdapter(
                                        getApplicationContext());
                                db.createDatabase();
                                db.open();
                                try {
                                    db.copyFinalDB();
                                    db.deleteDB();
                                } catch (SQLException | IOException e) {
                                    e.printStackTrace();
                                }
                                db.close();

                                // stop inactivity service
                                InactivityUtils.stopUserActivityUpdates();
                                stopService(new Intent(EjobList.this,
                                        UserInactivityService.class));

                                Intent intent = new Intent(EjobList.this,
                                        GoodYear_eJob_MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        })
                .setNegativeButton(nNo, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // update user activity on button click
                        alertDialogBuilder.updateInactivityForDialog();

                        dialog.cancel();
                    }
                });
        mLogoutAlertDialog = alertDialogBuilder.create();
        mLogoutAlertDialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        ArrayList<String> checkItems = mAdapter.getSelectedJobs();
        outState.putBoolean("loading", isLoading);
        outState.putIntegerArrayList(SELECTED_ITEMS,
                mAdapter.getSelectedItemIds());
        outState.putBoolean("mLogoutAlertDialog",
                (mLogoutAlertDialog != null && mLogoutAlertDialog.isShowing()));
        outState.putBoolean("mLogoutPopUp",
                (mLogoutPopUp != null && mLogoutPopUp.isShowing()));
        outState.putBoolean("mAlertDialog",
                (mAlertDialog != null && mAlertDialog.isShowing()));

        // Default Value(Sorting Button has not triggered)
        if (mImageViewSorting == null && TextUtils.isEmpty(mSortingKey)) {
            mImageViewSorting = mRefImage;
            mSortingKey = "ref_no";
        }
        outState.putInt("sorting_img_id", mImageViewSorting.getId());
        outState.putString("sorting_key", mSortingKey);
        outState.putStringArrayList(CHECKED_ITEMS, checkItems);
        outState.putBoolean("isLockDialogShown",
                (mJobLockDialog != null && mJobLockDialog.isShowing()));
        saveDialogState(outState);
    }

    // UploadDownloadAsync
    public class UploadDownloadAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            if (!isLoading) {
                isLoading = true;
                showProgress();
            }

            DatabaseAdapter copy_temp = new DatabaseAdapter(
                    getApplicationContext());

            try {
                copy_temp.copyFinalDB();
            } catch (SQLException | IOException e1) {
                e1.printStackTrace();
            }
            DataBaseHandler handler = new DataBaseHandler(getBaseContext());
            handler.openDB();
            handler.createTablesInDB();
            handler.updateSession("", "", "EN", "");
            try {
                handler.copyDataBase();
            } catch (IOException e) {
                e.printStackTrace();
            }
            handler.deleteDatabaseAfterLoad();
            handler.closeDB();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AuthSync.uploadDownloadSync(context);
                return params[0];
            } catch (FileNotFoundException e) {
                Log.e("Translation sync", "server not found " + e.getMessage());
                return "error server_down";
            } catch (IOException e) {
                Log.e("Translation sync",
                        "network not working" + e.getMessage());
                e.printStackTrace();
                return "error network";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            DatabaseAdapter copy_temp = new DatabaseAdapter(
                    getApplicationContext());

            super.onPostExecute(result);
            if (result.contains("error")) {
                String errorMessage;
                if (result.contains("server_down")) {
                    errorMessage = Constants.ERROR_SERVER_DOWN_ERROR;
                } else {
                    errorMessage = Constants.ERROR_NETWORK_SYNC_ERROR;
                }
                isLoading = false;
                FileOperations.cleanUp();
                CommonUtils.createInformationDialog(context, errorMessage);
            } else {
                try {
                    // open db, clean it and then reopen
                    copy_temp.open();
                    copy_temp.updateDBTablesAfterSync(Environment
                            .getExternalStorageDirectory()
                            + Constants.PROJECT_PATH
                            + "/"
                            + Constants.USER
                            + "_.db3");
                    copy_temp.copyFinalDB();
                    copy_temp.deleteDB();
                    copy_temp.close();

                    copy_temp.createDatabase();
                    copy_temp.open();
                    copy_temp.updateSyncTime(DateTimeUTC
                            .getMillisecondsFromUTCdate());
                    copy_temp.updateSelectedLanguage(result);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    dismissProgress();

                    copy_temp.close();
                    FileOperations.cleanUp();
                    isLoading = false;
                    Intent refresh = new Intent(getBaseContext(),
                            EjobList.class);
                    startActivity(refresh);
                }
            }
        }
    }

    // Auto Sync
    public class AuthSyncAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            if (!isLoading) {
                isLoading = true;
                showProgress();
            }

            DatabaseAdapter copy_temp = new DatabaseAdapter(
                    getApplicationContext());

            try {
                copy_temp.copyFinalDB();
            } catch (SQLException | IOException e1) {
                e1.printStackTrace();
            }
            copy_temp.close();
            DataBaseHandler handler = new DataBaseHandler(getBaseContext());
            handler.openDB();
            handler.createTablesInDB();
            try {
                handler.copyDataBase();
            } catch (IOException e) {
                e.printStackTrace();
            }
            handler.deleteDatabaseAfterLoad();
            handler.closeDB();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AuthSync.autoSync(context);
            } catch (FileNotFoundException e) {
                Log.e("Translation sync", "server not found " + e.getMessage());
                return "error server_down";
            } catch (IOException e) {
                Log.e("Translation sync",
                        "network not working" + e.getMessage());
                e.printStackTrace();
                return "error network";
            }
            return "auto";
        }

        @Override
        protected void onPostExecute(String result) {

            DatabaseAdapter copy_temp = new DatabaseAdapter(
                    getApplicationContext());

            if (result.contains("error")) {
                String errorMessage;
                if (result.contains("server_down")) {
                    errorMessage = Constants.ERROR_SERVER_DOWN_ERROR;
                } else {
                    errorMessage = Constants.ERROR_NETWORK_SYNC_ERROR;
                }
                isLoading = false;
                FileOperations.cleanUp();

                showAlertWhenNoNetwork(errorMessage);
            } else {
                try {
                    // open db, clean it and then reopen
                    copy_temp.open();
                    copy_temp.copyTempDB();
                    copy_temp.deleteDB();
                    copy_temp.close();
                    // copy the reponse from server
                    copy_temp.copyResponseDB();
                    copy_temp.open();
                    // create db with existing and new data
                    copy_temp.createEmptyTables("");
                    copy_temp.updateDBTables(Environment
                            .getExternalStorageDirectory()
                            + com.goodyear.ejob.util.Constants.PROJECT_PATH
                            + "/"
                            + com.goodyear.ejob.util.Constants.USER
                            + "_temp.db3");
                    FileOperations.removeFile(Environment
                            .getExternalStorageDirectory()
                            + com.goodyear.ejob.util.Constants.PROJECT_PATH
                            + "/"
                            + com.goodyear.ejob.util.Constants.USER
                            + ".db3");
                    copy_temp.copyFinalDB();
                    copy_temp.deleteDB();
                    copy_temp.close();

                    copy_temp.createDatabase();
                    copy_temp.open();
                    copy_temp.updateSyncTime(DateTimeUTC
                            .getMillisecondsFromUTCdate());
                    copy_temp.copyFinalDB();
                    // stop loading screen
                } catch (SQLException | IOException e) {
                    Log.e("EjobActivity", "cannot write db");
                } finally {
                    dismissProgress();
                    Constants.EJOBHEADERVALUESIFDBNULL = false;
                    copy_temp.close();
                    isLoading = false;
                    FileOperations.cleanUp();
                }
            }
        }
    }

    // TransaltionAsync
    public class TransaltionAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            if (!isLoading) {
                isLoading = true;
                showProgress();
            }
            DataBaseHandler handler = new DataBaseHandler(EjobList.this);
            handler.openDB();
            handler.createTablesInDB();
            handler.updateSession("", "", mLang);
            try {
                handler.copyDataBase();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Sandeep", "TranslationError:" + e);
            }
            handler.deleteDatabaseAfterLoad();
            handler.closeDB();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AuthSync.translationSync(context);
                return params[0];
            } catch (FileNotFoundException e) {
                Log.e("Translation sync", "server not found " + e.getMessage());
                return "error server_down";
            } catch (Exception e) {
                Log.e("Translation sync",
                        "network not working" + e.getMessage());
                e.printStackTrace();
                Log.e("Sandeep", "TranslationErrorcatch:" + e);
                return "error network";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            DatabaseAdapter copy_temp = new DatabaseAdapter(
                    getApplicationContext());

            super.onPostExecute(result);

            if (result.contains("error")) {
                String errorMessage;
                if (result.contains("server_down")) {
                    errorMessage = Constants.ERROR_SERVER_DOWN_ERROR;
                } else {
                    errorMessage = Constants.ERROR_NETWORK_SYNC_ERROR;
                }
                isLoading = false;
                FileOperations.cleanUp();
                CommonUtils.createInformationDialog(context, errorMessage);
            } else {
                try {
                    // open db, clean it and then reopen
                    copy_temp.open();
                    copy_temp.updateTranslationDBTables(Environment
                            .getExternalStorageDirectory()
                            + Constants.PROJECT_PATH
                            + "/"
                            + Constants.USER
                            + "_.db3");
                    copy_temp.copyFinalDB();
                    copy_temp.deleteDB();
                    copy_temp.close();

                    copy_temp.createDatabase();
                    copy_temp.open();
                    copy_temp.updateSyncTime(DateTimeUTC
                            .getMillisecondsFromUTCdate());
                    copy_temp.updateSelectedLanguage(result);
                    // update last user in settings
                    HashMap<String, String> langUpdate = new HashMap<>();
                    langUpdate.put("Language",
                            Constants.mapLanguageToSettings(result));
                    SettingsLoader.updateSettings(langUpdate, getBaseContext());

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    dismissProgress();
                    copy_temp.close();
                    FileOperations.cleanUp();
                    isLoading = false;
                    Intent refresh = new Intent(getBaseContext(),
                            EjobList.class);
                    startActivity(refresh);
                }
            }
        }
    }

    // list item click listener
    private void onClickListItems() {
        mLvJoblist.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                // To avoid double click
                if (mJobLockDialog != null) {
                    return;
                }
                //initialization for the inspected tyre
                //Constants.HAS_INSPECTED_TIRE = false;
                Constants.GETSELECTEDLISTPOSITION = Integer.parseInt(sListItems
                        .get(arg2).get("id"));
                TextView txtcust = (TextView) arg1
                        .findViewById(R.id.TextView05);
                mCustomerName = txtcust.getText().toString();
                TextView txtrefno = (TextView) arg1
                        .findViewById(R.id.TextView07);
                mRefNO = txtrefno.getText().toString();
                int intrefno = Integer.valueOf(mRefNO);
                Constants.JOBREFNUMBERVALUE = intrefno;

                //FIX for Partial Inspection Job Reference number
                Constants.JobRefNumberForPartialInspectionCheck = intrefno;
                try {
                    LogUtil.DBLog("Ejob List", "OnclickJob", "JobRefNumberForPartialInspectionCheck : " + Constants.JobRefNumberForPartialInspectionCheck);
                } catch (Exception e) {
                    LogUtil.DBLog("Ejob List", "OnclickJob", "JobRefNumberForPartialInspectionCheck : Exception : " + e.getMessage());
                }

                try {
                    LogUtil.TraceInfo(TRACE_TAG,"none","Selected Job"+sListItems.get(arg2),false,false,false);
                }
                catch (Exception e)
                {
                    LogUtil.TraceInfo(TRACE_TAG,"none","Selected Job : Exception : "+e.getMessage(),false,false,false);
                }

                // FIX this sJobItemToDelete is used for deletion
                EditJob.sJobItemToDelete = mRefNO;
                System.out.println("EditJob.sJobItemToDelete:::: "
                        + EditJob.sJobItemToDelete);

                // Fetching ExternalID and Vendor name
                String strexternalID = GetExternalIDandVendorDetail(intrefno,
                        EjobList.this);
                System.out.println("strexternalID--" + strexternalID);
                // Fetching Tyre ID and Action Type Repair Company Workeorder
                // OPeration ID
                getTireActionWorkOrderNumber(strexternalID, EjobList.this);
                DatabaseAdapter db = new DatabaseAdapter(EjobList.this);
                db.open();
                // Fetching SRNO SAP MATERIAL Status
                Cursor c1 = null;
                try {
                    c1 = db.SerialNo_SAP(sTyreid);
                    if (CursorUtils.isValidCursor(c1)) {
                        c1.moveToFirst();
                        int numRows = c1.getCount();
                        if (numRows > 0) {
                        }
                    }
                } catch (SQLException e) {
                } finally {
                    CursorUtils.closeCursor(c1);
                }
                // get Job values
                Cursor c4 = null;
                try {
                    c4 = db.getAllValuesFromJobTable();
                    if (c4 != null) {
                        c4.moveToFirst();
                        int numRows = c4.getCount();
                        if (numRows > 0) {
                            int mCallerName = c4.getColumnIndex("CallerName");
                            int mCallerPhone = c4.getColumnIndex("CallerPhone");
                            int mDefectAuthNumber = c4
                                    .getColumnIndex("DefectAuthNumber");
                            int mDriverName = c4.getColumnIndex("DriverName");
                            int mVehicleODO = c4.getColumnIndex("VehicleODO");
                            int mLocation = c4.getColumnIndex("Location");
                            int mVehicleId = c4.getColumnIndex("VehicleID");
                            int status = c4.getColumnIndex("Status");
                            int mActionLineN = c4.getColumnIndex("ActionLineN");
                            int mHudometer = c4.getColumnIndex("Hudometer");
                            int mStockLoc = c4.getColumnIndex("StockLocation");
                            int mRefNo = c4.getColumnIndex("jobRefNumber");
                            int mLPlateNO = c4.getColumnIndex("LicenseNumber");
                            int mSAPCode = c4
                                    .getColumnIndex("SAPContractNumber");

                            //CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
                            //To get Index value for Previous odometer
                            int mPreviousOdometerValue = c4.getColumnIndex("PreviousOdometerValue");
                            try {
                                Cursor mLPlateNOCur = db.getSelectedFleetNO(c4
                                        .getString(mLPlateNO));
                                String mLPlateNOVal = mLPlateNOCur
                                        .getString(mLPlateNOCur
                                                .getColumnIndex("Vin"));
                                CursorUtils.closeCursor(mLPlateNOCur);
                                Cursor mAccName = db.getSelectedAccountName(c4
                                        .getString(mSAPCode));
                                String mAccNameVal = mAccName.getString(mAccName
                                        .getColumnIndex("CustomerName"));
                                CursorUtils.closeCursor(mAccName);
                                JobBundle.setmAccountName(mAccNameVal);
                                JobBundle.setmFleetNO(mLPlateNOVal);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            String sts = c4.getString(status);

                            //CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
                            //Storing Last Recorded Odometer Value in Constants
                            if (!TextUtils.isEmpty(c4.getString(mPreviousOdometerValue))) {
                                Constants.LastRecordedOdometerValue = Long.parseLong(c4.getString(mPreviousOdometerValue));
                            } else {
                                Constants.LastRecordedOdometerValue = 0L;
                            }
                            Constants.STATUSONSELECTEDLIST = sts;
                            JobBundle.setCallerName(c4.getString(mCallerName));
                            JobBundle.setCallerPhone(c4.getString(mCallerPhone));
                            JobBundle.setmDefectAuthNumber(c4
                                    .getString(mDefectAuthNumber));
                            JobBundle.setmDriverName(c4.getString(mDriverName));
                            JobBundle.setmVehicleODO(c4.getString(mVehicleODO));
                            JobBundle.setLocation(c4.getString(mLocation));
                            JobBundle.setmActionLineN(c4
                                    .getString(mActionLineN));
                            JobBundle.setmHudometer(c4.getString(mHudometer));
                            JobBundle.setmStockLocation(c4.getString(mStockLoc));
                            JobBundle.setmRefNO(c4.getString(mRefNo));
                            JobBundle.setmLPlate(c4.getString(mLPlateNO));
                            if (c4.getString(mVehicleId) != null) {
                                Constants.VEHICLEIDFORCONFIG = c4
                                        .getString(mVehicleId);
                            }
                        }
                    }
                } catch (SQLException e) {
                } finally {
                    CursorUtils.closeCursor(c4);
                }
                // getting account
                db.close();
                if (!sListItems.get(arg2).get("status").equalsIgnoreCase("5")) {
                    if (sListItems.get(arg2).get("status")
                            .equalsIgnoreCase("2")
                            || sListItems.get(arg2).get("status")
                            .equalsIgnoreCase("0")) {
                        alertDialogueWithOkCancel();
                    } else if (sListItems.get(arg2).get("status")
                            .equalsIgnoreCase("4")) {
                        if (sListItems.get(arg2).get("licence")
                                .equalsIgnoreCase("")) {
                            Constants.COMESFROMVIEW = false;
                            Constants.COMESFROMUPDATE = false;
                            Intent intent = new Intent(EjobList.this,
                                    TyreSummary.class);
                            intent.putExtra("JOBREFNUMBERBUNDLE",
                                    Constants.JOBREFNUMBERVALUE);
                            intent.putExtra("vendorname", mVendorDetail);
                            intent.putExtra("contractname", mCustomerName);
                            intent.putExtra("serialno", mHomeSerialNum);// HomeSerialNum
                            intent.putExtra("Design", mHomeResultDesign);
                            intent.putExtra("Action", mHomeCurStatus);
                            intent.putExtra("RepairCompany",
                                    mHomeRepairCompanyName);
                            intent.putExtra("workorderno", mHomeWorkOrderNuber);
                            intent.putExtra("operation", mHomeDescription);//
                            startActivity(intent);
                        } else {
                            Constants.COMESFROMVIEW = true;
                            Constants.COMESFROMUPDATE = false;
                            EditJob job = new EditJob(EjobList.this);
                            job.loadJobInfoFromDB();
                        }
                    } else if (sListItems.get(arg2).get("status")
                            .equalsIgnoreCase("6")) {// 6,checked TM jobs
                        Constants.COMESFROMVIEW = false;
                        Constants.COMESFROMUPDATE = false;
                        Intent intent = new Intent(EjobList.this,
                                TyreSummary.class);
                        intent.putExtra("JOBREFNUMBERBUNDLE",
                                Constants.JOBREFNUMBERVALUE);
                        intent.putExtra("vendorname", mVendorDetail);
                        intent.putExtra("contractname", mCustomerName);
                        intent.putExtra("serialno", mHomeSerialNum);// HomeSerialNum
                        intent.putExtra("Design", mHomeResultDesign);
                        intent.putExtra("Action", mHomeCurStatus);
                        intent.putExtra("RepairCompany", mHomeRepairCompanyName);
                        intent.putExtra("workorderno", mHomeWorkOrderNuber);
                        intent.putExtra("operation", mHomeDescription);//
                        startActivity(intent);
                    } else {
						/*
						 * Bug Id. 285 & 290 resolved here Show a warning pop-up
						 * if a job's start date is older than 3 days
						 */
                        try {
                            String status = sListItems.get(arg2).get("status");
                            // for job items start date is termed as finish date
                            String startDate = sListItems.get(arg2).get(
                                    "timefinished");
                            if (!TextUtils.isEmpty(startDate)) {
                                DateTime dateTime = DateTimeUTC
                                        .convertMillisecondsToUTCDateTime(Long
                                                .parseLong(startDate));
                                long currenttime = System.currentTimeMillis();
                                long timeInMilliseconds = dateTime.getMillis();
                                if (status.equals("1")
                                        && (currenttime - timeInMilliseconds > 259200000)) {
                                    try {
                                        // CommonUtils.notify(db.getLabel(5),
                                        // context);
                                        dialogForJobOlderThanThreeDays();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Constants.COMESFROMVIEW = false;
                                    Constants.COMESFROMUPDATE = true;
                                    Constants.EDIT_JOB = true;
                                    EditJob job = new EditJob(EjobList.this);
                                    job.loadJobInfoFromDB();
                                }
                            } else {
                                Constants.COMESFROMVIEW = false;
                                Constants.COMESFROMUPDATE = true;
                                Constants.EDIT_JOB = true;
                                EditJob job = new EditJob(EjobList.this);
                                job.loadJobInfoFromDB();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } else {
                    Constants.COMESFROMVIEW = false;
                    Constants.COMESFROMUPDATE = false;
                    Intent intent = new Intent(EjobList.this, TyreSummary.class);
                    intent.putExtra("JOBREFNUMBERBUNDLE",
                            Constants.JOBREFNUMBERVALUE);
                    intent.putExtra("vendorname", mVendorDetail);
                    intent.putExtra("contractname", mCustomerName);
                    intent.putExtra("serialno", mHomeSerialNum);// HomeSerialNum
                    intent.putExtra("Design", mHomeResultDesign);
                    intent.putExtra("Action", mHomeCurStatus);
                    intent.putExtra("RepairCompany", mHomeRepairCompanyName);
                    intent.putExtra("workorderno", mHomeWorkOrderNuber);
                    intent.putExtra("operation", mHomeDescription);//
                    startActivity(intent);
                }
                db.close();
            }
        });
        mLvJoblist.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
                try {
                    LogUtil.TraceInfo(TRACE_TAG, "none", "Delete Job" + sListItems.get(arg2), false, false, false);
                } catch (Exception e) {
                    LogUtil.TraceInfo(TRACE_TAG, "none", "Delete Job : Exception : " + e.getMessage(), false, false, false);
                }

                if (!sListItems.get(arg2).get("status").equals("4")) {
                    removeDialoguePopUpAtSingleSwipe(arg2);
                }
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        logoutPopUp();
    }

    private void saveDialogState(Bundle state) {
        state.putBoolean(
                "mThreDaysOlderJobAlertDialog",
                (mThreDaysOlderJobAlertDialog != null && mThreDaysOlderJobAlertDialog
                        .isShowing()));
        if (mErrorDialog != null && mErrorDialog.isShowing()) {
            state.putBoolean("errorDialogShowing", true);
            state.putString("dialog_msg", mDialogMsg);
            state.putString("dialog_title", mDialogTitle);
        }
    }

    private void restoreDialogState(Bundle state) {
        if (state != null) {
            if (state.getBoolean("mThreDaysOlderJobAlertDialog")) {
                dialogForJobOlderThanThreeDays();
            }
        }

        if (state != null && state.getBoolean("errorDialogShowing")) {
            String msg = state.getString("dialog_msg");
            String title = state.getString("dialog_title");
            showAlertDialog(msg, title);
        }

    }

    private void dialogForJobOlderThanThreeDays() {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li
                .inflate(R.layout.dialog_logout_confirmation, null);
        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(context);
        alertDialogBuilder.setView(promptsView);

        // update user activity for dialog layout
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });

        TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
        infoTv.setText(threeDaysOld);
        alertDialogBuilder.setCancelable(false).setPositiveButton(mOkLabel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // update user activity on button click
                        alertDialogBuilder.updateInactivityForDialog();

                        dialog.dismiss();
                        Constants.COMESFROMVIEW = false;
                        Constants.COMESFROMUPDATE = true;
                        Constants.EDIT_JOB = true;
                        EditJob job = new EditJob(EjobList.this);
                        job.loadJobInfoFromDB();
                    }
                });

        mThreDaysOlderJobAlertDialog = alertDialogBuilder.create();
        mThreDaysOlderJobAlertDialog.show();
        mThreDaysOlderJobAlertDialog.setCanceledOnTouchOutside(false);
    }

    /**
     * log out from Application
     */
    private void logoutPopUp() {
        LayoutInflater li = LayoutInflater.from(EjobList.this);
        View promptsView = li
                .inflate(R.layout.dialog_logout_confirmation, null);
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
        infoTv.setText(mLogoutConfirmationLabel);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });
        LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Logout", false, true, false);

        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
                EjobList.this);
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setCancelable(false).setPositiveButton(mYESLabel,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Constants._CHECKFORLOGINAUTOFILL = true;
                        DatabaseAdapter db = new DatabaseAdapter(
                                getApplicationContext());
                        db.createDatabase();
                        db.open();
                        LogUtil.TraceInfo(TRACE_TAG, "Dialog", "BT - Yes", false, true, false);
                        try {
                            db.copyFinalDB();
                        } catch (SQLException | IOException e) {
                            e.printStackTrace();
                        }
                        db.close();
                        // stop inactivity service
                        InactivityUtils.stopUserActivityUpdates();
                        stopService(new Intent(EjobList.this,
                                UserInactivityService.class));

                        Intent intent = new Intent(EjobList.this,
                                GoodYear_eJob_MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        alertDialogBuilder.updateInactivityForDialog();
                    }
                });

        alertDialogBuilder.setCancelable(false).setNegativeButton(mNOLabel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        alertDialogBuilder.updateInactivityForDialog();
                        LogUtil.TraceInfo(TRACE_TAG, "Logout", "BT - No", false, true, false);
                        dialog.cancel();

                    }
                });
        mLogoutPopUp = alertDialogBuilder.create();
        mLogoutPopUp.show();

    }

    // getCheckedItemsForSync
    private void getCheckedItemsForSync() {
        for (int i = 0; i < sListItems.size(); i++) {
            if (sListItems.get(i).get("check_status").equals("true")) {
                updateJOBTableStatus(sListItems.get(i).get("ref_no"),
                        sListItems.get(i).get("status"));
            }
        }
    }

    // getJobRemovalreason
    public ArrayList<String> selectJobRemovalReason() {
        Cursor mCursor = null;
        DatabaseAdapter testAdapter = null;
        try {
            testAdapter = new DatabaseAdapter(this);
            testAdapter.createDatabase();
            testAdapter.open();
            mCursor = testAdapter.getJOBRemovalReason();
            mRemovalReasonArrayList = new ArrayList<String>();
            if (CursorUtils.isValidCursor(mCursor)) {
                mCursor.moveToFirst();
                while (!mCursor.isAfterLast()) {
                    mRemovalReasonArrayList.add(mCursor.getString(mCursor
                            .getColumnIndex("Reason"))); // add the item
                    mCursor.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CursorUtils.closeCursor(mCursor);
            if (testAdapter != null) {
                testAdapter.close();
            }
        }
        return mRemovalReasonArrayList;
    }

    // getJobRemovalreasonID
    public int getJobRemovalreasonID(String noReason) {
        Cursor mCursor = null;
        DatabaseAdapter mDbHelper = null;
        int removalReasonID = -1;
        try {
            mDbHelper = new DatabaseAdapter(EjobList.this);
            mDbHelper.createDatabase();
            mDbHelper.open();
            mCursor = mDbHelper.getJOBRemovalReasonID(noReason);
            if (CursorUtils.isValidCursor(mCursor)) {
                mCursor.moveToFirst();
                while (!mCursor.isAfterLast()) {
                    removalReasonID = mCursor.getInt(mCursor
                            .getColumnIndex("ID")); // add
                    mCursor.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (mDbHelper != null) {
                mDbHelper.close();
            }
            CursorUtils.closeCursor(mCursor);
        }
        return removalReasonID;
    }

    // loadRemovalReasonDialog
    public void loadRemovalReasonDialog(final String mRefNOForDelateREF) {
        try {
            LayoutInflater li_action = LayoutInflater.from(this);
            final View promptsView = li_action.inflate(
                    R.layout.activity_removal_reason, null);

            final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(this);

            ListView reasonList = (ListView) promptsView
                    .findViewById(R.id.list_removal_reason);

            // update user activity for dialog layout
            LinearLayout rootNode = (LinearLayout) promptsView
                    .findViewById(R.id.layout_root);
            rootNode.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    InactivityUtils.updateActivityOfUser();
                }
            });
            Button cancel = (Button) promptsView
                    .findViewById(R.id.buttonCancel);
            cancel.setText(mCancelLabel);
            selectJobRemovalReason();
            // if (EjobFormActionActivity.bool) {
            // cancel.setVisibility(View.INVISIBLE);
            // cancel.setVisibility(View.GONE);
            // }
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1,
                    mRemovalReasonArrayList);
            reasonList.setAdapter(arrayAdapter);
            alertDialogBuilder.setView(promptsView);
            mAlertDialog = alertDialogBuilder.create();
            mAlertDialog.setMessage(mDeleteReasonLabel);
            mAlertDialog.setCancelable(false);
            reasonList.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int arg2, long arg3) {
                    // update user activity on button click
                    alertDialogBuilder.updateInactivityForDialog();

                    String noReason = (String) arg0.getItemAtPosition(arg2);
                    int removalReasonID = getJobRemovalreasonID(noReason);
                    Log.d("EjobList"," Inside==>reasonList. removalReasonID : "+ removalReasonID);
                    if (removalReasonID >= 0) {
                        LogUtil.TraceInfo(TRACE_TAG,"none","User selected a valid Delete reason",false,false,false);
                        updateJOBTableStatusOnDelete(mRefNOForDelateREF,
                                removalReasonID);
                    }

                    // transalation table
                    CommonUtils.notify(strJOB_HAS_BEEN_DELETED, context);
                    getJobList();
                    EditJob.sJobItemToDelete = "";
                    EjobFormActionActivity.bool = false;
                    mAlertDialog.dismiss();
                }
            });
            cancel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // update user activity on button click
                    alertDialogBuilder.updateInactivityForDialog();

                    EditJob.sJobItemToDelete = "";
                    EjobFormActionActivity.bool = false;
                    mAlertDialog.dismiss();
                }
            });
            // show it
            mAlertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to get Job item List for Partial Inspection for deleted job
     *
     * @param mJobItemCursor job item Cursor
     * @return job item list
     */
    public ArrayList<JobItem> loadJobItemListForPartialInspectionCheck(Cursor mJobItemCursor) {
        ArrayList<JobItem> JOB_ITEM_LIST = new ArrayList<>();

        for (boolean hasItem = mJobItemCursor.moveToFirst(); hasItem; hasItem = mJobItemCursor.moveToNext()) {
            try {
                JobItem jobItem = new JobItem();

                jobItem.setJobId(mJobItemCursor.getString(mJobItemCursor
                        .getColumnIndex("ID")));

                jobItem.setExternalId(mJobItemCursor.getString(mJobItemCursor
                        .getColumnIndex("ExternalID")));

                jobItem.setTyreID(mJobItemCursor.getString(mJobItemCursor
                        .getColumnIndex("TyreID")));

                jobItem.setActionType(mJobItemCursor.getString(mJobItemCursor
                        .getColumnIndex("ActionType")));

                //To Determine Whether Inspection is Done Along With Operation or Not
                jobItem.setInspectedWithOperation(mJobItemCursor.getString(mJobItemCursor
                        .getColumnIndex("InspectionWithOperation")));


                JOB_ITEM_LIST.add(jobItem);
            } catch (Exception e) {
                Log.e("TAG", e.getMessage());
            }
        }
        return JOB_ITEM_LIST;
    }

    /**
     * Method to get tyre list for Partial inspection for deleted job
     *
     * @param mTyreItemCur tyre cursor
     * @return tyre list
     */
    public ArrayList<Tyre> loadTireListforPartialInspectionCheck(Cursor mTyreItemCur) {
        ArrayList<Tyre> TIRE_LIST = new ArrayList<>();
        for (boolean hasItem = mTyreItemCur.moveToFirst(); hasItem; hasItem = mTyreItemCur
                .moveToNext()) {
            try {
                Tyre tyre = new Tyre();

                tyre.setExternalID(mTyreItemCur.getString(mTyreItemCur
                        .getColumnIndex("ExternalID")));

                tyre.setIsSpare(mTyreItemCur.getString(mTyreItemCur
                        .getColumnIndex("IsSpare")));

                tyre.setmIsUnmounted(mTyreItemCur.getString(mTyreItemCur
                        .getColumnIndex("IsUnmounted")));

                TIRE_LIST.add(tyre);
            } catch (Exception e) {
                Log.e("TAG", e.getMessage());
            }


        }
        return TIRE_LIST;
    }


    /**
     * CR :: 447
     * Method checking whether the selected vehicle is partially with operation or fully Inspected for deleted job
     *
     * @return
     */
    public int partiallyInspectedWithOperation(String mRefNO) {

        ArrayList<JobItem> JOB_ITEM_LIST;
        ArrayList<Tyre> TIRE_LIST;

        int mCount = 0;
        int iCount = 0;
        int tyreWithOperation = 0;
        int tyreWithInspection = 0;
        int tyreWithOperationInspection = 0;

        Cursor Jobitems = null;
        Cursor TyreDetails = null;
        Jobitems = mDBHelper.getJobItemValuesByJobRefID(mRefNO);
        TyreDetails = mDBHelper.getTyreValuesByJobRefID(mRefNO);
        JOB_ITEM_LIST = loadJobItemListForPartialInspectionCheck(Jobitems);
        TIRE_LIST = loadTireListforPartialInspectionCheck(TyreDetails);

        for (int i = 0; i < TIRE_LIST.size(); i++) {

            //Checking for Mounted Tyres and non-spare tyres
            if (!TIRE_LIST.get(i).getmIsUnmounted().equalsIgnoreCase("1") && !TIRE_LIST.get(i).getIsSpare().equalsIgnoreCase("1")) {

                mCount++;

                String tyreID = TIRE_LIST.get(i).getExternalID();

                int noOfOperation = 0;
                int noOfOperationWithInspection = 0;
                int noOfInspection = 0;

                //we checking for no Of Operation, no Of Operation With Inspection, no Of Inspection Only
                for (int j = 0; j < JOB_ITEM_LIST.size(); j++) {

                    if (tyreID.equalsIgnoreCase(JOB_ITEM_LIST.get(j).getTyreID())) {
                        if (JOB_ITEM_LIST.get(j).getActionType().equalsIgnoreCase("26")) {
                            noOfInspection++;
                            if (JOB_ITEM_LIST.get(j).getInspectedWithOperation().equalsIgnoreCase("1")) {
                                noOfOperationWithInspection++;
                            }
                        } else {
                            noOfOperation++;
                            if (JOB_ITEM_LIST.get(j).getInspectedWithOperation().equalsIgnoreCase("1")) {
                                noOfOperationWithInspection++;
                            }
                        }
                    }
                }

                //Icount Check
                if (noOfOperationWithInspection > 0 || noOfInspection > 0 || noOfOperation > 0) {
                    iCount++;
                    if (noOfOperation > 0) {
                        tyreWithOperation++;
                    }
                    if (noOfInspection > 0) {
                        tyreWithInspection++;
                    }
                    if (noOfOperationWithInspection > 0) {
                        tyreWithOperationInspection++;
                    }
                }
            }
        }

        //No Operation and No Inspection
        if (iCount == 0) {
            return InspectionState.NO_INSPECTION_NO_OPERATION;
        }

        //all tyre Inspected (Mixed or only Inspection or Only Operation)
        else if (mCount == iCount) {
            if (tyreWithInspection == 0) {
                return InspectionState.ONLY_OPERATION_COMPLETE;
            } else if (tyreWithOperation == 0) {
                return InspectionState.ONLY_INSPECTION_COMPLETE;
            } else if (tyreWithOperation > tyreWithInspection) {
                return InspectionState.OPERATION_WITH_INSPECTION_COMPLETE;
            } else if (tyreWithOperation < tyreWithInspection) {
                return InspectionState.OPERATION_WITH_INSPECTION_COMPLETE;
            } else {
                return InspectionState.OPERATION_WITH_INSPECTION_COMPLETE;
            }
        }
        //Partially Inspected
        else {
            if (tyreWithOperation == 0) {
                return InspectionState.ONLY_INSPECTION_PARTIAL;
            } else if (tyreWithInspection == 0) {
                return InspectionState.ONLY_OPERATION_PARTIAL;
            } else if (tyreWithOperationInspection == tyreWithOperation && tyreWithOperationInspection == tyreWithInspection) {
                return InspectionState.OPERATION_WITH_INSPECTION_PARTIAL;
            } else {
                return InspectionState.OPERATION_WITH_INSPECTION_PARTIAL;
            }

        }
    }

    // update JOB TableStatus
    public void updateJOBTableStatus(String refNo, String status) {
        DatabaseAdapter testAdapter = null;
        try {
            testAdapter = new DatabaseAdapter(this);
            testAdapter.createDatabase();
            testAdapter.open();
            try {
                testAdapter.updateJOBTableWithStatus(refNo, status);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (testAdapter != null) {
                testAdapter.close();
            }
        }
    }

    // update JOBTable Status On Delete
    public void updateJOBTableStatusOnDelete(String refNum, int reasonID) {
        DatabaseAdapter testAdapter = null;
        try {
            // FIX in Edit mode the job should be deleted from the list
            if (!EditJob.sJobItemToDelete.equalsIgnoreCase("")) {
                refNum = EditJob.sJobItemToDelete;
            }
            EditJob.sJobItemToDelete = "";
            EjobFormActionActivity.bool = false;
            System.out.println("sJobItemToDelete::::>"
                    + EditJob.sJobItemToDelete + "< refNum::> " + refNum);
            testAdapter = new DatabaseAdapter(this);
            testAdapter.createDatabase();
            testAdapter.open();
            try {
                testAdapter.updateJOBTableWithStatusOnDelete(refNum, reasonID);
                 //Removed to fix CR 505
				/*testAdapter.releaseReservedTyre(mJobExternalId);*/
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                //As per Harpreet, For deleted Job : we should remove all inspection related information.
                //Partial Inspection check is not needed
//				int PIStatus = partiallyInspectedWithOperation(refNum);
//				if (PIStatus == InspectionState.ONLY_OPERATION_PARTIAL
//						|| PIStatus == InspectionState.OPERATION_WITH_INSPECTION_PARTIAL
//						|| PIStatus == InspectionState.ONLY_INSPECTION_PARTIAL) {
                mDBHelper.makeJobItemImperfectBasedOnJobReferenceNumber(refNum);

                //Set Is perfect value for Job reference
                mDBHelper.makeJobCorrectionImperfectBasedOnJobReferenceNumber(refNum);
                LogUtil.DBLog("EJOB LIST", "updateJOBTableStatusOnDelete", "partial Inspection Check for Deleted Job Success  ");

                //Delete unused Reserved PWT Tire
                mDBHelper.deleteReservedUnusedPWTTireBasedOnJobReferenceNumber(refNum);
                LogUtil.DBLog("EJOB LIST", "updateJOBTableStatusOnDelete", "Delete reserved Tire based on Job reference number ");

                /**
                 * CR 505 : PWT are not available if a job that uses one has been deleted
                 * clearing PWT Tyre List if it is not null or else initialize
                 */
                if(Constants.PWTTyreList!=null)
                {
                    Constants.PWTTyreList.clear();
                }
                else
                {
                    Constants.PWTTyreList = new ArrayList<>();
                }

                /**
                 * addPWTTyreToDeletedJobPWTTyreList Method call moved from Sync option : to add support for immediate availability of PWT tyre after job deleted
                 * CR 505 : PWT are not available if a job that uses one has been deleted
                 * Filtering Deleted jobs from other job and  Adding deleted job PWT Tyre to PWT Tyre List
                 */
                try {
                    mDBHelper.addPWTTyreToDeletedJobPWTTyreList(refNum);
                } catch (Exception e) {
                    LogUtil.DBLog("eJob List", "onOptionsItemSelected : addPWTTyreToDeletedJobPWTTyreList : Exception - ", e.getMessage());
                }

                /**
                 * restorePWTOnDelete Method call moved from Upload Sync Model : to add support for immediate availability of PWT tyre after job deleted
                 * CR 505
                 * Condition handling the method call to restore the deleted PWT data
                 */
                //Retrieve Reserved PWT tyre info from shared pref
                Constants.mPWTList = EjobList.retrieveMountPWTTyresFromSharedPref(context);
                if (null != Constants.mPWTList && Constants.mPWTList.size() != 0) {

                    //create a duplicate entry of PWT Tyre
                    EjobList.restorePWTOnDelete(context,true);
                }


//				}
            } catch (Exception e) {
                e.printStackTrace();
                LogUtil.DBLog("EJOB LIST", "updateJOBTableStatusOnDelete", "partial Inspection Check for Deleted job: Exception : " + e.getMessage());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
           if (testAdapter != null) {
                testAdapter.close();
            }
            EditJob.sJobItemToDelete = "";
            EjobFormActionActivity.bool = false;
        }
    }

    // network checking
    public void showAlertWhenNoNetwork(String error) {
        final EjobAlertDialog helpBuilder = new EjobAlertDialog(this);
        helpBuilder.setTitle(Constants.sLblNoNetwork);
        helpBuilder.setMessage(error);
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li
                .inflate(R.layout.dialog_simple_one_textview, null);
        helpBuilder.setView(promptsView);

        // set user activity for
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });

        final TextView textMessage = (TextView) promptsView
                .findViewById(R.id.text_message);
        final TextView title = (TextView) promptsView
                .findViewById(R.id.text_dialog_title);

        // setting dialog title
        title.setVisibility(TextView.VISIBLE);
        title.setText(Constants.sLblNoNetwork);

        // Setting Dialog Message
        textMessage.setText(error);

        helpBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // update user activity on button click
                        helpBuilder.updateInactivityForDialog();
                        // Disabling the boolean after normal LogOut
                        Constants.IS_SHOWING_DIALOG = false;
                        LogUtil.i("EJOB LIST", "  showAlertWhenNoNetwork()");
                        // Do nothing but close the dialog
                        Intent intent = new Intent(EjobList.this,
                                GoodYear_eJob_MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                });

        // Remember, create doesn't show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();
    }

    // Database open here
    public void openDB() {
        try {
            mDBHelper = new DatabaseAdapter(this);
            mDBHelper.createDatabase();
            mDBHelper.open();
        } catch (SQLException e4) {
            e4.printStackTrace();
        }
    }

    // DataBase close here
    public void closeDB() {
        try {
            if (mDBHelper != null) {
                mDBHelper.close();
            }
        } catch (SQLException se) {
            LogUtil.d("EjobList ", "Exception : " + se.getMessage());
        } catch (Exception e) {
            LogUtil.d("EjobList", "Exception : " + e.getMessage());
        }
    }

    // used for view completed job item
    private void alertDialogueWithOkCancel() {
        LayoutInflater li = LayoutInflater.from(EjobList.this);
        View promptsView = li
                .inflate(R.layout.dialog_logout_confirmation, null);
        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
                EjobList.this);
        String mLabelOk=mDBHelper.getLabel(Constants.LABEL_OK);
        String mLabelCancel = mDBHelper.getLabel(Constants.CANCEL_LABEL);

        // update user activity for dialog layout
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });
        alertDialogBuilder.setView(promptsView);
        TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
        infoTv.setText(mDBHelper.getLabel(4));
        alertDialogBuilder.setCancelable(false).setPositiveButton(mLabelOk,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // update user activity on button click
                        alertDialogBuilder.updateInactivityForDialog();

                        Constants.COMESFROMVIEW = true;
                        Constants.COMESFROMUPDATE = false;
                        EditJob job = new EditJob(EjobList.this);
                        job.loadJobInfoFromDB();
                        mJobLockDialog = null;
                    }
                });
        alertDialogBuilder.setCancelable(false).setNegativeButton(mLabelCancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // update user activity on button click
                        alertDialogBuilder.updateInactivityForDialog();

                        dialog.cancel();
                        mJobLockDialog = null;
                    }
                });
        mJobLockDialog = alertDialogBuilder.create();
        mJobLockDialog.show();
    }

    // get selected job
    private void displaySelectedJobs(RelativeLayout relativeLayout, int status) {
        rlTireManagementJobs.setBackgroundResource(R.color.grey);
        rlIncompleteJobs.setBackgroundResource(R.color.grey);
        rlCompletedJobs.setBackgroundResource(R.color.grey);
        rlDeletedJobs.setBackgroundResource(R.color.grey);
        relativeLayout.setBackgroundResource(R.color.white);
        getSpecificJobList(status);
    }

    @Override
    public void onCanceled(int currentId) {
    }

    @Override
    public void onSyncComplete(ResponseMessage baseModel) {
        isLoading = false;
        dismissProgress();
        Log.i("EjobList", "onSyncComplete ResponseMessage:" + baseModel);
        if (baseModel != null) {
            Log.i("EjobList", "ResponseMessage :" + baseModel.getSyncOption()
                    + " ::Message:: " + baseModel.getMessage());
            long endtime = System.currentTimeMillis();
            Log.i("EjobList", "Final Time :" + +(endtime - mStartTime) / 1000);
            int syncOption = baseModel.getSyncOption();
            // if sync is of type translation or Upload
            if ((syncOption == SyncOptions.TRANSLATE_SYNC.getValue())
                    || syncOption == SyncOptions.UPLOAD_SYNC.getValue()) {
                Log.i("EjobList",
                        "Starting EjobListActivity After Sync/Translation:: ");
                String errorMessage = baseModel.getMessage();
                if (!TextUtils.isEmpty(errorMessage)
                        && errorMessage.contains("error")) {
                    if (errorMessage.contains("server_down")) {
                        errorMessage = Constants.ERROR_SERVER_DOWN_ERROR;
                    } else if (errorMessage.contains("DB_DELETED")) {
                        errorMessage = Constants.ERROR_DB_DELETED;
                    } else {
                        errorMessage = Constants.ERROR_NETWORK_SYNC_ERROR;
                    }
                    CommonUtils.createInformationDialog(context, errorMessage);
                } else {
                    Intent refresh = new Intent(getBaseContext(),
                            EjobList.class);
                    startActivity(refresh);
                }
            }
            // if sync is of type download (auto) sync
            else if (syncOption == SyncOptions.DOWNLOAD_SYNC.getValue()) {
                Constants.Force_Download_Sync_STATUS=true;
                String errorMessage = baseModel.getMessage();
                if (!TextUtils.isEmpty(errorMessage)
                        && errorMessage.contains("error")) {
                    if (errorMessage.contains("server_down")) {
                        errorMessage = Constants.ERROR_SERVER_DOWN_ERROR;
                    } else if (errorMessage.contains("DB_DELETED")) {
                        errorMessage = Constants.ERROR_DB_DELETED;
                    } else {
                        errorMessage = Constants.ERROR_NETWORK_SYNC_ERROR;
                    }
                    showAlertWhenNoNetwork(errorMessage);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdapter.cleanCheckedJobList();
        try {
            LogUtil.d("EjobList", "OnDestroy()");
            if (mGYProgressDialog != null && mGYProgressDialog.isShowing()) {
                LogUtil.d("EjobList", "OnDestroy() Dismissing Progress Dialog");
                mGYProgressDialog.dismiss();
            }

            if (mLogoutAlertDialog != null && mLogoutAlertDialog.isShowing()) {
                LogUtil.d("EjobList",
                        "OnDestroy() Dismissing mLogoutAlertDialog");
                mLogoutAlertDialog.dismiss();
            }
            if (mLogoutPopUp != null && mLogoutPopUp.isShowing()) {
                LogUtil.d("EjobList", "OnDestroy() Dismissing mLogoutPopUp");
                mLogoutPopUp.dismiss();
            }

            dismissErrorDialog();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDB();
        }
    }

    public void showAlertDialog(String errorMsg, String title) {
        dismissProgress();// just to be sure
        mDialogTitle = title;
        mDialogMsg = errorMsg;
        final EjobAlertDialog builder = new EjobAlertDialog(this);

        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li
                .inflate(R.layout.dialog_simple_one_textview, null);
        builder.setView(promptsView);

        // set user activity for
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });

        final TextView textMessage = (TextView) promptsView
                .findViewById(R.id.text_message);
        final TextView dialogTitle = (TextView) promptsView
                .findViewById(R.id.text_dialog_title);

        // setting dialog title
        dialogTitle.setVisibility(TextView.VISIBLE);
        dialogTitle.setText(mDialogTitle);

        // Setting Dialog Message
        textMessage.setText(mDialogMsg);

        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // update user activity on button click
                builder.updateInactivityForDialog();
                LogUtil.i("EjobList", "Forcelogout-condition check");
                LogUtil.i("EjobList", "Constants.APP_UPGRADE_STATUS - " + Constants.APP_UPGRADE_STATUS);
                LogUtil.i("EjobList", "Constants.Force_Download_Sync_STATUS - " + Constants.Force_Download_Sync_STATUS);
                LogUtil.i("EjobList", "Constants.IS_SHOWING_DIALOG - " + Constants.IS_SHOWING_DIALOG);
                if (Constants.APP_UPGRADE_STATUS || Constants.Force_Download_Sync_STATUS) {
                    LogUtil.i("EjobList", "Constants.APP_UPGRADE_STATUS - " + Constants.APP_UPGRADE_STATUS);
                    LogUtil.i("EjobList", "Constants.APP_UPGRADE_STATUS - " + Constants.Force_Download_Sync_STATUS);
                    Constants.IS_SHOWING_DIALOG = false;
                    CommonUtils.forceLogout(context);
                }
                dialog.dismiss();
            }
        });

        if (Constants.APP_UPGRADE_STATUS || Constants.Force_Download_Sync_STATUS) {
            LogUtil.i("EjobList", "show Dialog : Constants.IS_SHOWING_DIALOG - " + Constants.IS_SHOWING_DIALOG);
            if (!Constants.IS_SHOWING_DIALOG) {
                Constants.IS_SHOWING_DIALOG = true;
                LogUtil.i("EjobList", "show dialog : ++ - ");
                mErrorDialog = builder.create();
                mErrorDialog.show();
            }
        } else {
            LogUtil.i("EjobList", "show dialog : + - ");
            mErrorDialog = builder.create();
            mErrorDialog.show();
        }
    }

    private void dismissErrorDialog() {
        LogUtil.i("EjobList", "dismissErrorDialog");
        Constants.IS_SHOWING_DIALOG = false;
        if (mErrorDialog != null && mErrorDialog.isShowing()) {
            mErrorDialog.dismiss();
        }
    }

    private void moveInternalDBToDBFile() {
        DatabaseAdapter db = new DatabaseAdapter(getApplicationContext());
        try {
            db.createDatabase();
            db.open();

            db.copyFinalDB();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
            }
        }

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
     * ()
     */
    @Override
    public void logUserOutDueToInactivity() {
        InactivityUtils.logoutFromActivityBelowEjobForm(this);
    }

    /**
     * Method calling the update query for changing
     * the status for all the Jobs present in Job-list
     * Bug ID:: 487
     */
    public void UpdateJOBStatusAfterSync() {
        DatabaseAdapter dbForUpdateSync = new DatabaseAdapter(context);
        try {
            dbForUpdateSync.open();
            dbForUpdateSync.updateJObStatusAfterSync();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (dbForUpdateSync != null) {
                dbForUpdateSync.close();
            }
        }
    }

    /**
     * Method Restoring deleted PWT data back after sync
     */
    public static void restorePWTOnDelete(Context mContext, Boolean makeDuplicateEntry) {

        DatabaseAdapter dbForUpdateTyre = new DatabaseAdapter(mContext);

        try {
            dbForUpdateTyre.open();
            for (int i = 0; i < Constants.mPWTList.size(); i++) {

                if(Constants.PWTTyreList==null)
                {
                    break;
                }

                for(int j=0;j<Constants.PWTTyreList.size();j++)
                {
                    if(Constants.PWTTyreList.get(j).equalsIgnoreCase(Constants.mPWTList.get(i).getExternalID()))
                    {
                        try
                        {
                            ContentValues PWTTyreValue = new ContentValues();
                            dbForUpdateTyre.initializeSpecialContentValues();

                            dbForUpdateTyre.addValueToContentValues("Pressure", null);

                            if (null != String.valueOf(Constants.mPWTList.get(i).getShared())) {
                                dbForUpdateTyre.addValueToContentValues("Shared", Constants.mPWTList.get(i).getShared());
                            } else {
                                dbForUpdateTyre.addValueToContentValues("Shared", null);
                            }

                            if (null != Constants.mPWTList.get(i).getStatus()) {
                                dbForUpdateTyre.addValueToContentValues("Status", Constants.mPWTList.get(i).getStatus());
                            } else {
                                dbForUpdateTyre.addValueToContentValues("Status", null);
                            }
                            if (null != Constants.mPWTList.get(i).getSapCodeTI()) {
                                dbForUpdateTyre.addValueToContentValues("SAPCodeTi", Constants.mPWTList.get(i).getSapCodeTI());
                            } else {
                                dbForUpdateTyre.addValueToContentValues("SAPCodeTi", null);
                            }
                            if (null != Constants.mPWTList.get(i).getSapMaterialCodeID()) {
                                dbForUpdateTyre.addValueToContentValues("SAPMaterialCodeID", Constants.mPWTList.get(i).getSapMaterialCodeID());
                            } else {
                                dbForUpdateTyre.addValueToContentValues("SAPMaterialCodeID", null);
                            }
                            dbForUpdateTyre.addValueToContentValues("Position", null);

                            dbForUpdateTyre.addValueToContentValues("IsUnmounted", Constants.mPWTList.get(i).getmIsUnmounted());


                            // start
                            if (null != Constants.mPWTList.get(i).getSerialNumber()) {
                                dbForUpdateTyre.addValueToContentValues("SerialNumber", Constants.mPWTList.get(i).getSerialNumber());
                            }

                            if(makeDuplicateEntry)
                            {
                                dbForUpdateTyre.addValueToContentValues("ExternalID", String.valueOf(UUID.randomUUID()));
                            }
                            else {
                                if (null != String.valueOf(Constants.mPWTList.get(i).getExternalID())) {
                                    dbForUpdateTyre.addValueToContentValues("ExternalID", Constants.mPWTList.get(i).getExternalID());
                                } else {
                                    dbForUpdateTyre.addValueToContentValues("ExternalID", null);
                                }
                            }

                            if (null != Constants.mPWTList.get(i).getIsSpare()) {
                                dbForUpdateTyre.addValueToContentValues("IsSpare", Constants.mPWTList.get(i).getIsSpare());
                            } else {
                                dbForUpdateTyre.addValueToContentValues("IsSpare", null);
                            }

                            if (null != Constants.mPWTList.get(i).getSapContractNumberID()) {
                                dbForUpdateTyre.addValueToContentValues("SAPContractNumberID", Constants.mPWTList.get(i).getSapContractNumberID());
                            } else {
                                dbForUpdateTyre.addValueToContentValues("SAPContractNumberID", null);
                            }
                            if (null != Constants.mPWTList.get(i).getSapVenderCodeID()) {
                                dbForUpdateTyre.addValueToContentValues("SAPVendorCodeID", Constants.mPWTList.get(i).getSapVenderCodeID());
                            } else {
                                dbForUpdateTyre.addValueToContentValues("SAPVendorCodeID", null);
                            }

                            if (null != Constants.mPWTList.get(i).getOrignalNSK()) {
                                dbForUpdateTyre.addValueToContentValues("OriginalNSK", Constants.mPWTList.get(i).getOrignalNSK());
                            } else {
                                dbForUpdateTyre.addValueToContentValues("OriginalNSK", null);
                            }

                            if (null != Constants.mPWTList.get(i).getActive()) {
                                dbForUpdateTyre.addValueToContentValues("Active", Constants.mPWTList.get(i).getActive());
                            } else {
                                dbForUpdateTyre.addValueToContentValues("Active", null);
                            }
                            dbForUpdateTyre.addValueToContentValues("NSK1", null);
                            dbForUpdateTyre.addValueToContentValues("NSK2", null);
                            dbForUpdateTyre.addValueToContentValues("NSK3", null);
                            dbForUpdateTyre.addValueToContentValues("SAPCodeAxID", null);
                            dbForUpdateTyre.addValueToContentValues("SAPCodeVeh", null);
                            dbForUpdateTyre.addValueToContentValues("SAPCodeWp", null);
                            dbForUpdateTyre.addValueToContentValues("FromJobID", null);
                            dbForUpdateTyre.addValueToContentValues("idVehicleJob", null);
                            PWTTyreValue = dbForUpdateTyre.getSpecialContentValues();
                            dbForUpdateTyre.insertPWTAfterDeleteJob(PWTTyreValue);
                        }
                        catch (Exception e)
                        {
                            LogUtil.i("EjobList","Exception - " + e.getMessage());
                        }
                    }
                }
            }

            //To remove delete PET info from mPWTList
            for(int i=0;i<Constants.PWTTyreList.size();i++)
            {
                for(int j=0;j<Constants.mPWTList.size();j++)
                    if(Constants.PWTTyreList.get(i).equalsIgnoreCase(Constants.mPWTList.get(j).getExternalID()))
                    {
                        Constants.mPWTList.remove(j);
                        break;
                    }
            }

            //Store PWT information
            MountPWTActivity.storeTyresInSharedPref(Constants.mPWTList, mContext);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (dbForUpdateTyre != null) {
                dbForUpdateTyre.close();
                Constants.mPWTList.clear();
                if(Constants.PWTTyreList==null && Constants.PWTTyreList.size()>0)
                {
                    Constants.PWTTyreList.clear();
                }
            }
        }
    }

    /**
     * Method returning an array of PWT object after fetching from shared preferences
     * @return
     */
    public static ArrayList<Tyre> retrieveMountPWTTyresFromSharedPref(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.VALIDATE_USER, Context.MODE_PRIVATE);
        ArrayList<Tyre> uTyres;
        try {
            uTyres = (ArrayList) ObjectSerializer.deserialize(prefs.getString(Constants.TYRES_PWT, ObjectSerializer.serialize(new ArrayList())));
            return uTyres;
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("1", e.getMessage());
        }
        return new ArrayList<Tyre>();
    }

}