/**
 * Dismount Operations
 */
package com.goodyear.ejob;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.goodyear.ejob.blutooth.BluetoothService;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.job.operation.helpers.TireImageItem;
import com.goodyear.ejob.performance.IOnPerformanceCallback;
import com.goodyear.ejob.performance.PerformanceBaseModel;
import com.goodyear.ejob.performance.ResponseMessagePerformance;
import com.goodyear.ejob.ui.jobcreation.AutoSwapImpl;
import com.goodyear.ejob.util.CameraUtility;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.TireDesignDetails;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.TyreFormElements;
import com.goodyear.ejob.util.TyreState;

/**
 * @author amitkumar.h
 * @version 1.0 Class handling the tire operation called tire removal(Dismount)
 *          It provides mechanism to load design page as per the selected
 *          tire(maintained, non-maintained, empty, spare etc). Moreover it
 *          handles the data updates in different tables(JobItem, Tire,
 *          jobCorrection etc) as per the user-entered tire-data during Dismount
 *          Operation.
 */
public class DismountTireActivity extends Activity implements InactivityHandler {

	/**
	 * Swiper Object
	 */
	private Swiper mSwipeDetector;
	/**
	 * ArrayList for Brands
	 */
	private ArrayList<String> mBrandArrayList;
	/**
	 * Selected Brand Name
	 */
	private String mSelectedBrandName;
	/**
	 * Selected Tyre Size
	 */
	private String mSelectedtyreSize;
	/**
	 * Selected Tyre ASP
	 */
	private String mSelectedTyreTASP;
	/**
	 * Selected Tyre RIM
	 */
	private String mSelectedtyreTRIM;
	/**
	 * Selected Tyre Design
	 */
	private String mSelectedTyreDesign;
	/**
	 * Selected Tyre Detailed Design
	 */
	private String mSelectedTyreDetailedDesign;
	/**
	 * Selected Removal Reason
	 */
	private String mSelectedRemovalReason;
	/**
	 * Arraylist for Size
	 */
	private ArrayList<String> mSizeArrayList;
	/**
	 * ArrayList for RIM
	 */
	private ArrayList<String> mTyreTRIMArrayList;
	/**
	 * ArrayList for ASP
	 */
	private ArrayList<String> mTyreTASPArrayList;
	/**
	 * Spinner for Brands
	 */
	private Spinner mBrandSpinner;
	/**
	 * Spinner for Size
	 */
	private Spinner mSizeSpinner;
	/**
	 * Spinner for RIM
	 */
	private Spinner mTyreTRIMSpinner;
	/**
	 * Spinner for ASP
	 */
	private Spinner mTyreTASPSpinner;
	/**
	 * Spinner for Design
	 */
	private Spinner mTyreDesignSpinner;
	/**
	 * Spinner for Tyre full details
	 */
	private Spinner mTyreFullDetailSpinner;
	/**
	 * Array Adapter for Size
	 */
	private ArrayAdapter<String> mSizeDataAdapter;
	/**
	 * Array Adapter for Rim
	 */
	private ArrayAdapter<String> mRimDataAdapter;
	/**
	 * Array Adapter ASP
	 */
	private ArrayAdapter<String> mASPDataAdapter;
	/**
	 * Array Adapter for Design
	 */
	private ArrayAdapter<String> mDesignDataAdapter;
	/**
	 * Array Adapter for Full design
	 */
	private ArrayAdapter<String> mFullDesignDataAdapter;
	/**
	 * TextView reference for tyre position
	 */
	private TextView mValue_tyrePosition;
	/**
	 * TextView reference for serial number
	 */
	private TextView mValue_tyreSerialNumber;
	/**
	 * ArrayList for Design
	 */
	private ArrayList<String> mDesignArrayList;
	/**
	 * ArrayList for Full Design
	 */
	private ArrayList<String> mFullDesignArrayList;
	/**
	 * EditText for valueNSK1
	 */
	private EditText mValueNSK1;
	/**
	 * EditText for valueNSK2
	 */
	private EditText mValueNSK2;
	/**
	 * EditText for valueNSK3
	 */
	private EditText mValueNSK3;
	/**
	 * ScrollView reference
	 */
	private ScrollView mMainScrollBar;
	/**
	 * TextView reference for RegrooveType
	 */
	private TextView mRegrooveTypeLabel;
	/**
	 * TextView reference for Dimension label
	 */
	private TextView mDimensionLabel;
	/**
	 * TextView reference for Size
	 */
	private TextView mSizeLabel;
	/**
	 * TextView reference for ASP
	 */
	private TextView mASPLabel;
	/**
	 * TextView reference for RIM
	 */
	private TextView mRIMLabel;
	/**
	 * TextView reference for Design
	 */
	private TextView mDesignLabel;
	/**
	 * TextView reference for Type
	 */
	private TextView mTypeLabel;
	/**
	 * TextView reference forlabel serial number swap
	 */
	private TextView mLbl_serialNo_swap;
	/**
	 * Note text variable
	 */
	private String mNoteText = "";
	/**
	 * BluetoothService Object
	 */
	private BluetoothService mBTService;
	/**
	 * BroadcastReceiver Object
	 */
	private BroadcastReceiver mReceiver;
	/**
	 * NSK Counter value
	 */
	private int mNSKCounter = 1;
	/**
	 * TextView Design label
	 */
	private TextView mDesignTypeLabel;
	/**
	 * Spinner for removal reason
	 */
	private Spinner mSpinnerRemovalReason;
	/**
	 * Spinner for CastingRoute
	 */
	private Spinner mSpinnerCasingRoute;
	/**
	 * ArrayList for Removal reason
	 */
	private ArrayList<String> mRemovalReasonList;
	/**
	 * ArrayList for CastingRoute list
	 */
	private ArrayList<String> mCasingRouteList;
	/**
	 * Tyre Object
	 */
	private Tyre mSelectedTire;
	/**
	 * Switch reference
	 */
	private Switch mRIMValue;
	/**
	 * RadioGroup reference for regroove type
	 */
	private RadioGroup mRGRegrooveType;
	/**
	 * Radio button reference for regroove type
	 */
	private RadioButton mRegrooveType;
	/**
	 * Radio button reference for regroove type yes
	 */
	private RadioButton mRegrooveTypeYes;
	/**
	 * Radio button reference for regroove type no
	 */
	private RadioButton mRegrooveTypeNo;
	/**
	 * boolean flag
	 */
	private static boolean sFlag = false;
	/**
	 * boolean flag for is radio button checked or not
	 */
	private static boolean sIsRadioBtnChecked = false;
	/**
	 * regroove tyre value
	 */
	private String mReGrooveTypeValue = "";
	/**
	 * Hasp map for removal reason code
	 */
	private HashMap<String, String> mRemovalReasonCode;
	/**
	 * Arrray list for removal reason external id
	 */
	private ArrayList<String> mRemovalReasonExternalId;
	/**
	 * Hasp map for Casting route code
	 */
	private HashMap<String, String> mCasingRouteCode;
	/**
	 * ImageView for Camera icon
	 */
	private ImageView mCamera_icon;
	/**
	 * boolean variable for is Camera enabled
	 */
	private boolean mCameraEnabled = false;
	/**
	 * this flag is to check if user selected NSk is crossing the threshold NSK
	 */
	private boolean mNSKThresholdCrossed;
	/**
	 * this flag is to check if user selected removal reason is break spot. this
	 * flag will be used in camera activity
	 */
	private boolean mIsSelectedReasonBreakSpot = false;
	/**
	 * TestAdapter Object
	 */
	private DatabaseAdapter mDbHelper;
	/**
	 * Final variable for NOTES_INTENT
	 */
	public static final int NOTES_INTENT = 104;
	/**
	 * Final variable for PHOTO_INTENT
	 */
	public static final int PHOTO_INTENT = 105;
	/**
	 * boolean variable for swiped
	 */
	private boolean mSwiped = false;
	/**
	 * Casing route header
	 */
	private String mCasingRouteHeader;
	/**
	 * Removal reason header
	 */
	private String mRemovalReasonHeader;
	/**
	 * Selected SAP material code id
	 */
	private String mSelectedSAPMaterialCodeID;
	/**
	 * Regroove required
	 */
	private String mRegrooveIsRequired = "";
	/**
	 * Previous Action type
	 */
	private static String sPreviousActionType = null;
	/**
	 * is Size spinners editable
	 */
	private boolean mIsSizeSpinnerEditable = true;
	/**
	 * Yes lable value from Database
	 */
	private String mYESlabelFromDB;
	/**
	 * No lable value from Database
	 */
	private String mNOlabelFromDB;

	/**
	 * Default value for Spinner
	 */
	private String mDefaultValueForSpinner = "Please Select";

	/**
	 * IsOrientationChagned, This variable using Spinner manipulation while
	 * orientation
	 */
	private boolean mIsOrientationChanged = false;
	/**
	 * Last Spinner Selection Index, This variable using Spinner manipulation
	 * while orientation
	 */
	private int mLastSpinnerSelection = 0;
	/**
	 * String message from DB for Please Select Brand
	 */
	private String mPleaseSelectBrand;
	private String mConfirmMsg;
	private String mNoLabel;
	private String mYesLabel;
	private String mNotesLabel;

	private String mBrandForAutoSwap;
	private String mSerialNumberForAutoSwap;
	private String mSizeForAutoSwap;
	private String mRimForAutoSwap;
	private String mAspForAutoSwap;
	private String mDesignForAutoSwap;
	private String mDesignDetailsForAutoSwap;
	private String mNskForAutoSwap;
	private String mNsk2ForAutoSwap;
	private String mNsk3ForAutoSwap;
	private TyreFormElements mFormElements;
	Bundle savedInstanceState;
	BTTask task;
	private AlertDialog mBackAlertDialog;
	/**
	 * set based on removal reason
	 */
	private boolean mIsTireReuable = true;
	public static final String BREAKSPOT_SELECTED = "BREAKSPOT_SELECTED";
	public static final String DAMAGESCRAP_SELECTED = "DAMAGESCRAP_SELECTED";
	private static final int CAMERA_INTENT_CODE = 1;
	private String mbreakSpotMaxMinFromCameraActvity = "";
	protected boolean mIsSelectedReasonDamageScrap = false;
	private TextView mJobDetailsLabel;
	private TextView mNSKLabel;
	private String mPleaseSelectLabel;
	private static final String MIN_MAX_TD_KEY = "minmaxtd";
	private float userEnteredNSK;
	private String selectedItemCode = "";
	private String mNothingSelectedLabel = "-NA-";
	// BluetoothConnectionFragment fragment;
	private static final String TAG = "DismountTireActivity";
	//User Trace logs trace tag
	private static final String TRACE_TAG = "Dismount Tire";

	private TextView mBrandLabel;
	private TextView mJobDetailHeaderLabel;
	private TextView mSerialNoAndBrandLabel;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.savedInstanceState = savedInstanceState;
		setContentView(R.layout.dismount_tire);

		//User Trace logs
		LogUtil.TraceInfo(TRACE_TAG,"none","OP",true,false,true);
		mDbHelper = new DatabaseAdapter(this);
		mDbHelper.open();
		setTitle(mDbHelper.getLabel(Constants.LABEL_DISMOUNT_TYRE));

		SharedPreferences preferences = getSharedPreferences(
				Constants.GOODYEAR_CONF, 0);
		/*
		 * CR:: 455 this condition is checking whether the user has selected
		 * some value for threshold NSK or not, if Yes then photo capture will
		 * be mandatory above that NSK
		 */
		if (Constants.CAMERA_ENABLE_FOR_MIN_TD.equals("ON")
				&& !TextUtils.isEmpty(preferences.getString(
						Constants.MIN_TD_LEVEL_FOR_CAMERA, ""))) {
			Constants.THRESHOLD_TD_FOR_CAMERA = preferences.getString(
					Constants.MIN_TD_LEVEL_FOR_CAMERA, "");
		}
		mDesignArrayList = new ArrayList<String>();
		mFullDesignArrayList = new ArrayList<String>();
		mBrandArrayList = new ArrayList<String>();
		mSizeArrayList = new ArrayList<String>();
		mTyreTRIMArrayList = new ArrayList<String>();
		mTyreTASPArrayList = new ArrayList<String>();
		mCasingRouteList = new ArrayList<String>();
		mRemovalReasonList = new ArrayList<String>();
		mLbl_serialNo_swap = (TextView) findViewById(R.id.lbl_serialNo_swap);
		mValue_tyreSerialNumber = (TextView) findViewById(R.id.value_serialNo);
		mValue_tyrePosition = (TextView) findViewById(R.id.value_wp);
		mBrandSpinner = (Spinner) findViewById(R.id.spinner_brand);
		mSizeSpinner = (Spinner) findViewById(R.id.spinner_dimenssionSize);
		mTyreTASPSpinner = (Spinner) findViewById(R.id.value_dimenssionASP);
		mTyreTRIMSpinner = (Spinner) findViewById(R.id.value_dimenssionRIM);
		mTyreDesignSpinner = (Spinner) findViewById(R.id.spinner_make);
		mTyreFullDetailSpinner = (Spinner) findViewById(R.id.spinner_type);
		mValueNSK1 = (EditText) findViewById(R.id.value_NSK1);
		mValueNSK2 = (EditText) findViewById(R.id.value_NSK2);
		mValueNSK3 = (EditText) findViewById(R.id.value_NSK3);
		mRegrooveTypeLabel = (TextView) findViewById(R.id.lbl_reGrooved);
		mDimensionLabel = (TextView) findViewById(R.id.lbl_dimenssion);
		mJobDetailsLabel = (TextView) findViewById(R.id.lbl_jobDetails);
		mNSKLabel = (TextView) findViewById(R.id.lbl_NSK);
		mCamera_icon = (ImageView) findViewById(R.id.camera_icon);
		mCamera_icon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (mSpinnerRemovalReason.getSelectedItemPosition() != 0) {
					Intent photoIntent = new Intent(DismountTireActivity.this,
							CameraActivity.class);
					// for NSK threshold damage drop down are not required
					
						photoIntent
								.putExtra("DAMAGE_MANDATORY", mCameraEnabled);
					
					photoIntent.putExtra(BREAKSPOT_SELECTED,
							mIsSelectedReasonBreakSpot);
					photoIntent.putExtra(DAMAGESCRAP_SELECTED,
							mIsSelectedReasonDamageScrap);
					photoIntent.putExtra(CameraActivity.BREAK_SPOT_KEY,
							mbreakSpotMaxMinFromCameraActvity);
					photoIntent.putExtra(CameraActivity.DAMAGE_SCRAP_KEY,
							mbreakSpotMaxMinFromCameraActvity);
					photoIntent.putExtra("CURNT_IMAGE_COUNT", getImageCount());
					startActivityForResult(photoIntent, CAMERA_INTENT_CODE);
				}
			}
		});
		mSizeLabel = (TextView) findViewById(R.id.lbl_dimenssionSiz);
		mASPLabel = (TextView) findViewById(R.id.lbl_dimenssionASP);
		mRIMLabel = (TextView) findViewById(R.id.lbl_dimenssionRIM);
		mDesignLabel = (TextView) findViewById(R.id.lbl_make);
		mTypeLabel = (TextView) findViewById(R.id.lbl_RimType);
		mDesignTypeLabel = (TextView) findViewById(R.id.lbl_type);
		mRGRegrooveType = (RadioGroup) findViewById(R.id.rg_regRooveType);
		mRegrooveTypeYes = (RadioButton) findViewById(R.id.value_regRooveYES);
		mRegrooveTypeNo = (RadioButton) findViewById(R.id.value_regRooveNO);
		mBrandLabel = (TextView) findViewById(R.id.lbl_brand);
		mJobDetailHeaderLabel = (TextView) findViewById(R.id.lbl_jobDetailsHeading);
		mSerialNoAndBrandLabel = (TextView) findViewById(R.id.lbl_serialNoAndBrand);
		TextView wheelPositionLabel = (TextView) findViewById(R.id.lbl_wp);
		wheelPositionLabel.setText(Constants.sLblWheelPos);
		mValueNSK1
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mValueNSK2
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mValueNSK3
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mValueNSK1.addTextChangedListener(new nskOneBeforeChanged());
		mValueNSK2.addTextChangedListener(new nskTwoBeforeChanged());
		mValueNSK3.addTextChangedListener(new nskThreeBeforeChanged());
		mSpinnerRemovalReason = (Spinner) findViewById(R.id.spinner_removalReason);
		mSpinnerCasingRoute = (Spinner) findViewById(R.id.spinner_casingRoute);
		mRIMValue = (Switch) findViewById(R.id.value_RimType);
		loadLabelsFromDB();
		mSelectedTire = Constants.SELECTED_TYRE;
		if (TextUtils.isEmpty(Constants.SELECTED_TYRE.getSapMaterialCodeID())) {
			mSelectedSAPMaterialCodeID = "";
		} else {
			mSelectedSAPMaterialCodeID = Constants.SELECTED_TYRE
					.getSapMaterialCodeID();
		}
		mMainScrollBar = (ScrollView) findViewById(R.id.scroll_main);
		mSwipeDetector = new Swiper(getApplicationContext(), mMainScrollBar);
		mMainScrollBar.setOnTouchListener(mSwipeDetector);
		if (mSelectedTire.getSerialNumber().equals(
				Constants.EDITED_SERIAL_NUMBER)) {
			mValue_tyreSerialNumber.setText(mSelectedTire.getSerialNumber());
		} else {
			mValue_tyreSerialNumber.setText(Constants.EDITED_SERIAL_NUMBER);
		}
		mValue_tyrePosition.setText(mSelectedTire.getPosition());
		if (savedInstanceState != null) {
			selectedItemCode = savedInstanceState
					.getString("selectedItemCode");
			mNSKThresholdCrossed = savedInstanceState.getBoolean(
					"mNSKThresholdCrossed");
			mbreakSpotMaxMinFromCameraActvity = savedInstanceState
					.getString(MIN_MAX_TD_KEY);
			mNoteText = savedInstanceState.getString("NOTE_TEXT");
			mNSKCounter = savedInstanceState.getInt("nsk_counter", 0);// NOTE_TEXT
			mIsOrientationChanged = true;
			mSelectedBrandName = savedInstanceState.getString("BRAND");
			mSelectedRemovalReason = savedInstanceState
					.getString("REMOVALREASON");
			sPreviousActionType = mSelectedRemovalReason;
			if (savedInstanceState.containsKey("SIZE")) {
				mSelectedtyreSize = savedInstanceState.getString("SIZE");
				mSelectedTyreTASP = savedInstanceState.getString("ASP");
				mSelectedtyreTRIM = savedInstanceState.getString("RIM");
				mSelectedTyreDesign = savedInstanceState.getString("DESIGN");
				mSelectedTyreDetailedDesign = savedInstanceState
						.getString("FULLDETAIL");
			} else {
				mSelectedtyreSize = "";
			}
			mIsSizeSpinnerEditable = savedInstanceState.getBoolean(
					"mIsSizeSpinnerEditable", true);
			checkLatestSpinnerSelectionBeforeOrientationChanges();
		} else {
			mSelectedtyreSize = mSelectedTire.getSize();
			mSelectedTyreTASP = mSelectedTire.getAsp();
			mSelectedtyreTRIM = mSelectedTire.getRim();
			if (!Constants.onBrandBool) { // If onBrandBool is true spinners
											// should be empty
				mSelectedBrandName = mSelectedTire.getBrandName();
				mSelectedTyreDesign = mSelectedTire.getDesign();
				mSelectedTyreDetailedDesign = mSelectedTire.getDesignDetails();
			}
		}
		if (TextUtils.isEmpty(mSelectedTyreDetailedDesign)
				|| mDefaultValueForSpinner.equals(mSelectedTyreDetailedDesign)) {
			TireDesignDetails detail = CommonUtils
					.getDetailsFromTyreInSameAxle(mSelectedTire);
			if (null != detail) {
				mSelectedtyreSize = detail.getSize();
				mSelectedTyreTASP = detail.getAsp();
				mSelectedtyreTRIM = detail.getRim();
				mIsSizeSpinnerEditable = false;
			}
		}
		if (!mIsSizeSpinnerEditable || Constants.onBrandBool) {
			loadSizeDetailsFromAxle();
			mIsSizeSpinnerEditable = false;
		}
		if (Constants.onBrandBool
				|| Constants.SELECTED_TYRE.getSerialNumber().equalsIgnoreCase(
						"")
				|| TextUtils
						.isEmpty(Constants.SELECTED_TYRE.getDesignDetails())
				|| mDefaultValueForSpinner.equals(mSelectedTyreDetailedDesign)) {
			loadTyreDetailsOnBrandCorrection();
		} else {
			loadTyreDetails();
		}
		loadRemovalReason();
		loadCasingRoute();
		restoreDialogState(savedInstanceState);
		// AutoSwap
		setDataForAutoSwap();
		captureInitialState(savedInstanceState);
	}

	/**
	 * Method loading and setting the full tire details for the corresponding
	 * selected tire
	 */
	private void loadTyreDetails() {
		loadSelectedBrand();
		loadSelectedSize();
		loadSelectedASP();
		loadSelectedRIM();
		loadSelectedDesign();
		loadSelectedDesignDetails();
	}

	/**
	 * Overridden method handling application inactivity by updating the
	 * user-interaction with the application
	 */
	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	/**
	 * Method loading casing route from local database table for the
	 * corresponding selected tire
	 */
	@SuppressWarnings("rawtypes")
	public void loadCasingRoute() {
		Cursor casingRouteCursor = null;
		try {
			casingRouteCursor = null;
			if (null != mDbHelper) {
				casingRouteCursor = mDbHelper.getCasingRoute();
			}
			if (!CursorUtils.isValidCursor(casingRouteCursor)) {
				return;
			}
			mCasingRouteCode = new HashMap<String, String>();
			casingRouteCursor.moveToFirst();
			mCasingRouteList.clear();
			while (!casingRouteCursor.isAfterLast()) {
				mCasingRouteList.add(casingRouteCursor
						.getString(casingRouteCursor
								.getColumnIndex("Description")));
				mCasingRouteCode.put(casingRouteCursor
						.getString(casingRouteCursor
								.getColumnIndex("Description")),
						casingRouteCursor.getString(casingRouteCursor
								.getColumnIndex("SAPCode")));
				casingRouteCursor.moveToNext();
			}
			CursorUtils.closeCursor(casingRouteCursor);
			Collections.sort(mCasingRouteList);
			mCasingRouteList.add(0, mCasingRouteHeader);
			mCasingRouteCode.put(mCasingRouteHeader, "-NA-");
			ArrayAdapter<String> casingRouteAdapter = new ArrayAdapter<String>(
					this, android.R.layout.simple_spinner_item,
					mCasingRouteList);
			casingRouteAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpinnerCasingRoute.setAdapter(casingRouteAdapter);
			mSpinnerCasingRoute
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(casingRouteCursor);
		}
	}

	/**
	 * Method loading removal reason from local database table for the
	 * corresponding selected tire
	 */
	@SuppressWarnings("rawtypes")
	public void loadRemovalReason() {
		Cursor removalReasonCusrsor = null;
		try {
			removalReasonCusrsor = null;
			if (null != mDbHelper) {
				removalReasonCusrsor = mDbHelper.getRemovalReason();
			}
			if (!CursorUtils.isValidCursor(removalReasonCusrsor)) {
				return;
			}
			mRemovalReasonCode = new HashMap<String, String>();
			mRemovalReasonExternalId = new ArrayList<String>();
			removalReasonCusrsor.moveToFirst();
			mRemovalReasonList.clear();
			while (!removalReasonCusrsor.isAfterLast()) {
				mRemovalReasonList.add(removalReasonCusrsor
						.getString(removalReasonCusrsor
								.getColumnIndex("Reason")));
				mRemovalReasonCode.put(removalReasonCusrsor
						.getString(removalReasonCusrsor
								.getColumnIndex("Reason")),
						removalReasonCusrsor.getString(removalReasonCusrsor
								.getColumnIndex("Code")));
				mRemovalReasonExternalId.add(removalReasonCusrsor
						.getString(removalReasonCusrsor
								.getColumnIndex("ExternalID")));
				removalReasonCusrsor.moveToNext();
			}
			CursorUtils.closeCursor(removalReasonCusrsor);
			mRemovalReasonList.add(0, mRemovalReasonHeader);
			mRemovalReasonExternalId.add(0, "-NA-");
			mRemovalReasonCode.put(mRemovalReasonHeader, "-NA-");
			ArrayAdapter<String> removalReasonAdapter = new ArrayAdapter<String>(
					this, android.R.layout.simple_spinner_item,
					mRemovalReasonList);
			removalReasonAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpinnerRemovalReason.setAdapter(removalReasonAdapter);
			if (null != mSelectedRemovalReason)
				for (int i = 0; i < mRemovalReasonList.size(); i++) {
					if (mRemovalReasonList.get(i)
							.equals(mSelectedRemovalReason)) {
						mSpinnerRemovalReason.setSelection(i);
						break;
					}
				}
			mSpinnerRemovalReason
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							resetCamaraImages();
							// reset flag
							mIsSelectedReasonBreakSpot = false;
							mIsSelectedReasonDamageScrap = false;
							Constants.ISBREAKSPOT = false;
							mCamera_icon.setVisibility(View.VISIBLE);
							selectedItemCode = mRemovalReasonCode
									.get(mSpinnerRemovalReason
											.getSelectedItem().toString());
							mIsTireReuable = isTireReusable(selectedItemCode);
							// Start if Condition ::  It will be true when all the condition are matching for mandatory photo
							 if(selectedItemCode.equals("D")
									|| selectedItemCode.equals("MRBS") 
									|| selectedItemCode.equals("O")){
								mCamera_icon.setImageResource(R.drawable.camera_green_icon);
								mCameraEnabled = true;
							}
							 else if (!Constants.THRESHOLD_TD_FOR_CAMERA.equals("0")
										&& userEnteredNSK >= Float.parseFloat(Constants.THRESHOLD_TD_FOR_CAMERA.trim())) 
							{
								mCamera_icon.setImageResource(R.drawable.camera_green_icon);
								mNSKThresholdCrossed = true;
								mCameraEnabled = false;
							}
							// ::  It will be true when the camera icon is green i.e (photo capture is not mandatory)
							else if (!Constants.THRESHOLD_TD_FOR_CAMERA.equals("0")
									&& userEnteredNSK > Float.parseFloat(Constants.THRESHOLD_TD_FOR_CAMERA.trim())) {
								mCamera_icon.setImageResource(R.drawable.camera_green_icon);
								mCameraEnabled = false;
							} 
							// :: It will be true when the camera functionality is disable
							else 
							{
								if (mSpinnerRemovalReason.getSelectedItemPosition() == 0) 
								{
									mCamera_icon.setImageResource(R.drawable.camera_grey_icon);
									mCamera_icon.setVisibility(View.GONE);
									mCameraEnabled = false;
								} 
								else 
								{
									mCamera_icon.setImageResource(R.drawable.camera_icon);
								}
								mCameraEnabled = false;
							}

							if (selectedItemCode.equals("MRBS")
									|| selectedItemCode.equals("O")) {
								Constants.ISBREAKSPOT = true;
								mIsSelectedReasonBreakSpot = true;
							} else if (selectedItemCode.equals("D")) {
								mIsSelectedReasonDamageScrap = true;
							} else {
								Constants.ISBREAKSPOT = false;
								mIsSelectedReasonBreakSpot = false;
								mIsSelectedReasonDamageScrap = false;
							}
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(removalReasonCusrsor);
		}
	}

	/**
	 * checks based on the removal reason if the tire is to come in PWT list or
	 * not (shared field will be set accordingly)
	 * 
	 * @return true if tire is reuable else false
	 */
	private boolean isTireReusable(String code) {
		if (code.equals("D") || code.equals("WO") || code.equals("W")
				|| code.equals("WOS")) {
			return false;
		}
		return true;
	}

	@Override
	protected void onStop() {
		LogUtil.i("BT",
				"****************** onStop *****************:mBTService: "
						+ mBTService);
		try {
			if (null != mBTService) {
				mBTService.stop();
				if (task != null && !task.isCancelled()) {
					mBTService.asyncCancel(true);
					task.cancel(true);
					task = null;
				}
			}
			if (null != mReceiver) {
				unregisterReceiver(mReceiver);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onStop();
	}

	@Override
	protected void onStart() {
		// set this activity as inactivity handler
		LogoutHandler.setCurrentActivity(this);
		mValueNSK1.setEnabled(true);
		mValueNSK2.setEnabled(true);
		mValueNSK3.setEnabled(true);
		if (mBTService != null) {
			mBTService.stop();
		}

		mBTService = new BluetoothService(this);
		if (savedInstanceState != null) {
			boolean isWorkerThread = savedInstanceState
					.getBoolean("isworkerstopped");
			mBTService.setWorkerThread(isWorkerThread);
		}
		IntentFilter intentFilter = new IntentFilter("BLUETOOTH_SENDER");
		mReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.hasExtra("BT_DATA_NSK")) {
					String nsk_value = intent.getStringExtra("BT_DATA_NSK");
					if (mNSKCounter == 1) {
						mValueNSK1.setText(nsk_value);
						mValueNSK2.setText(nsk_value);
						mValueNSK3.setText(nsk_value);
						mNSKCounter = 2;
					} else if (mNSKCounter == 2) {
						mValueNSK2.setText(nsk_value);
						mNSKCounter = 3;
					} else if (mNSKCounter == 3) {
						mNSKCounter = 1;
						mValueNSK3.setText(nsk_value);
					}
				} else if (intent.hasExtra("BT_CONN_STATUS")) {
					CommonUtils.notify(Constants.sLblMultiplePaired,
							DismountTireActivity.this);
				} else if (intent.hasExtra("BT_STATUS_IS_DISCONNECTED")) {
					boolean isDisconnected = intent.getBooleanExtra(
							"BT_STATUS_IS_DISCONNECTED", false);
					LogUtil.i(
							"Bluetooth in my Operation",
							"Broadcast came to my oeration:: " + isDisconnected
									+ " mBTService: "
									+ mBTService.isConnected());
					mValueNSK1.setEnabled(isDisconnected);
					mValueNSK2.setEnabled(isDisconnected);
					mValueNSK3.setEnabled(isDisconnected);
					if (isDisconnected) {
						mBTService.stop();
						initiateBT();
					} else {
						LogUtil.i("TOR", "######## Making stopworker false ");
						mBTService.startReading();
					}
				} else if (intent.hasExtra("BT_MAX_TRY_REACHED")) {
					initiateBT();
				}
			}
		};
		try {
			registerReceiver(mReceiver, intentFilter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		initiateBT();
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	/**
	 * Method initializing Blue-tooth, checking for connectivity and performing
	 * data transfer while using blue-tooth probes.
	 */
	private void initiateBT() {
		int status = mBTService.getPairedStatus();
		switch (status) {
		case 0:
			CommonUtils.notify(Constants.sLblMultiplePaired,
					DismountTireActivity.this);
			break;
		case 1:
			task = new BTTask();
			task.execute();
			break;
		case 2:
			CommonUtils.notify(Constants.sLblPleasePairTLogik,
					DismountTireActivity.this);
			break;
		case 3:
			CommonUtils.notify(Constants.sLblNoPairedDevices,
					DismountTireActivity.this);
			break;
		}
	}

	/**
	 * class handling blue-tooth functionality in a separate non-UI threads and
	 * performing data transfer while using blue-tooth probes.
	 */
	class BTTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... f_url) {
			if (isCancelled()) {
				LogUtil.e("Dismount", "doInBackground Async Task Cancelled  > ");
				return "";
			}
			try {
				mBTService.connectToDevice(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if (isCancelled()) {
				LogUtil.e("Dismount", "onPostExecute Async Task Cancelled  > ");
				return;
			}
			try {
				if (mBTService.isConnected()) {
					mValueNSK1.setEnabled(false);
					mValueNSK2.setEnabled(false);
					mValueNSK3.setEnabled(false);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onDestroy() {
		sPreviousActionType = null;
		try {
			if (null != mDbHelper) {
				mDbHelper.close();
			}
			if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
				mBackAlertDialog.dismiss();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	public void onPause() {
		super.onPause();

	}

	/**
	 * Overridden method storing all the captured data inside bundle object in
	 * order to handle different orientations
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (mBrandSpinner.getCount() != 0) {
			outState.putString("BRAND", mBrandSpinner.getSelectedItem()
					.toString());
		}
		if (null != mSizeSpinner.getSelectedItem()
				&& mSizeSpinner.getCount() != 0)
			outState.putString("SIZE", mSizeSpinner.getSelectedItem()
					.toString());
		if (null != mTyreTASPSpinner.getSelectedItem()
				&& mTyreTASPSpinner.getCount() != 0)
			outState.putString("ASP", mTyreTASPSpinner.getSelectedItem()
					.toString());
		if (null != mTyreTRIMSpinner.getSelectedItem()
				&& mTyreTRIMSpinner.getCount() != 0)
			outState.putString("RIM", mTyreTRIMSpinner.getSelectedItem()
					.toString());
		if (null != mTyreDesignSpinner.getSelectedItem()
				&& mTyreDesignSpinner.getCount() != 0)
			outState.putString("DESIGN", mTyreDesignSpinner.getSelectedItem()
					.toString());
		if (null != mTyreFullDetailSpinner.getSelectedItem()
				&& mTyreFullDetailSpinner.getCount() != 0)
			outState.putString("FULLDETAIL", mTyreFullDetailSpinner
					.getSelectedItem().toString());
		if (mSpinnerRemovalReason.getCount() != 0) {
			outState.putString("REMOVALREASON", mSpinnerRemovalReason
					.getSelectedItem().toString());
		}
		outState.putBoolean("mIsSizeSpinnerEditable", mIsSizeSpinnerEditable);
		outState.putInt("nsk_counter", mNSKCounter);
		// note
		outState.putString("NOTE_TEXT", mNoteText);
		outState.putBoolean("mNSKThresholdCrossed", mNSKThresholdCrossed);
		outState.putString("selectedItemCode", selectedItemCode);
		if (mBTService != null) {
			outState.putBoolean("isworkerstopped", mBTService.isWorkerThread());
		}
		// add MIN MAX TD values to saved instance
		if (mbreakSpotMaxMinFromCameraActvity != null) {
			outState.putString(MIN_MAX_TD_KEY,
					mbreakSpotMaxMinFromCameraActvity);
		}
		saveDialogState(outState);
		saveInitialState(outState);
		super.onSaveInstanceState(outState);
	}

	/**
	 * Loading Labels for UI from the Translation table of the local database
	 * file
	 */
	private void loadLabelsFromDB() {
		try {
			if (null == mDbHelper) {
				return;
			}
			String tireDetailsLabel = mDbHelper
					.getLabel(Constants.LABEL_TIRE_DETAILS);
			TextView tireDetailsHeading = (TextView) findViewById(R.id.lbl_tireDetailsHeading);
			if (tireDetailsHeading != null) {
				tireDetailsHeading.setText(tireDetailsLabel);
			}
			((TextView) findViewById(R.id.lbl_tireDetails))
					.setText(tireDetailsLabel);
			mRIMValue.setTextOff(mDbHelper.getLabel(Constants.STEEL));
			mRIMValue.setTextOn(mDbHelper.getLabel(Constants.ALLOY));
			mConfirmMsg = mDbHelper.getLabel(Constants.CONFIRM_BACK);
			mNoLabel = mDbHelper.getLabel(Constants.NO_LABEL);
			mYesLabel = mDbHelper.getLabel(Constants.YES_LABEL);
			mNotesLabel = mDbHelper.getLabel(Constants.NOTES_ACTIVITY_NAME);
			invalidateOptionsMenu();
			String labelFromDB = mDbHelper.getLabel(418);
			mLbl_serialNo_swap.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(200);
			mRegrooveTypeLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.DIMENSION_LABEL);
			mDimensionLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.ASP_LABEL);
			mASPLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.RIM_LABEL);
			mRIMLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(68);
			mDesignLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.SIZE_LABEL);
			mSizeLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(137);
			mDesignTypeLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(391);
			mTypeLabel.setText(labelFromDB);
			mCasingRouteHeader = mDbHelper.getLabel(60);
			mRemovalReasonHeader = mDbHelper.getLabel(61);
			mYESlabelFromDB = mDbHelper.getLabel(Constants.YES_LABEL);
			mRegrooveTypeYes.setText(mYESlabelFromDB);
			mNOlabelFromDB = mDbHelper.getLabel(Constants.NO_LABEL);
			mRegrooveTypeNo.setText(mNOlabelFromDB);
			mRegrooveIsRequired = mDbHelper
					.getLabel(Constants.REGROOVE_IS_REQUIRED);
			mPleaseSelectBrand = mDbHelper.getLabel(Constants.SELECT_BRAND);
			labelFromDB = mDbHelper.getLabel(Constants.JOB_DETAILS);
			mJobDetailsLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.NSK_LABEL);
			mNSKLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.BRAND_LABEL);
			mBrandLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.JOB_DETAILS);
			if (mJobDetailHeaderLabel != null) {
				mJobDetailHeaderLabel.setText(labelFromDB);
			}

			labelFromDB = mDbHelper.getLabel(Constants.SERIAL_NUMBER) + "&" + mDbHelper.getLabel(Constants.BRAND_LABEL);
			if (mSerialNoAndBrandLabel != null) {
				mSerialNoAndBrandLabel.setText(labelFromDB);
			}
			// BT PROBE LABELS
			Constants.sLblMultiplePaired = mDbHelper
					.getLabel(Constants.MULTIPLE_PAIRED_DEVICES_FOUND);
			Constants.sLblPleasePairTLogik = mDbHelper
					.getLabel(Constants.PLEASE_PAIR_TLOGIK);
			Constants.sLblNoPairedDevices = mDbHelper
					.getLabel(Constants.NO_PAIRED_DEVICES);
			mPleaseSelectLabel = mDbHelper
					.getLabel(Constants.PLEASE_SELECT_LABEL);
			mDefaultValueForSpinner = mPleaseSelectLabel;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * TextWatcher for NSK1
	 */
	private class nskOneBeforeChanged implements TextWatcher {
		@Override
		public void afterTextChanged(Editable nskOne) {
			mValueNSK2.setText(nskOne.toString());
			mValueNSK3.setText(nskOne.toString());
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mValueNSK1.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				userEnteredNSK = CommonUtils.parseFloat(strEnteredVal);
				if (userEnteredNSK <= Constants.MAX_NSK_LEVEL) {
					
				} else {
					mValueNSK1.setText("");
					Constants.PHOTO_CAPTURE_ON_MIN_TD = false;
				}
			} else if (strEnteredVal.equals(".")) {
				mValueNSK1.setText("");
			}

		}
	}

	/**
	 * TextWatcher for NSK2
	 */
	private class nskTwoBeforeChanged implements TextWatcher {

		@Override
		public void afterTextChanged(Editable nskTwo) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mValueNSK2.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= Constants.MAX_NSK_LEVEL) {
					checkCameraStatusOnTextChanged();
				} else {
					mValueNSK2.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mValueNSK2.setText("");
			}
		}
		/**
		 * CR:: 455 Method checking whether the user has
		 * selected some value for threshold NSK or not, if Yes then
		 * photo capture will be mandatory above that NSK
		 */
		private void checkCameraStatusOnTextChanged() {
			// Start if:: It will be true when all the condition are matching for mandatory photo
			if (!Constants.THRESHOLD_TD_FOR_CAMERA.equals("0") 
					&& userEnteredNSK >= Float.parseFloat(Constants.THRESHOLD_TD_FOR_CAMERA.trim())
					&& !TextUtils.isEmpty(selectedItemCode)
					&& !selectedItemCode.equalsIgnoreCase(mNothingSelectedLabel )
					|| mIsSelectedReasonDamageScrap || mIsSelectedReasonBreakSpot)
			{
				mCamera_icon.setVisibility(View.VISIBLE);
				mCamera_icon.setImageResource(R.drawable.camera_green_icon);
				mNSKThresholdCrossed = true;
				Constants.PHOTO_CAPTURE_ON_MIN_TD = true;
			}
			// else if :: It will be true when the camera functionality is disable
			else if(!TextUtils.isEmpty(selectedItemCode) 
					&& selectedItemCode.equalsIgnoreCase(mNothingSelectedLabel))
			{
				mCamera_icon.setVisibility(View.GONE);
				mCameraEnabled = false;
				Constants.PHOTO_CAPTURE_ON_MIN_TD = false;
				mNSKThresholdCrossed = false;
			}
			// else :: It will be true when the camera icon is green i.e (photo capture is not mandatory)
			else
			{
				mCamera_icon.setVisibility(View.VISIBLE);
				mCamera_icon.setImageResource(R.drawable.camera_icon);
				Constants.PHOTO_CAPTURE_ON_MIN_TD = false;
				mCameraEnabled = false;
				mNSKThresholdCrossed = false;
			}
		}
	}
	/**
	 * TextWatcher for NSK3
	 */
	private class nskThreeBeforeChanged implements TextWatcher {

		@Override
		public void afterTextChanged(Editable nskThree) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mValueNSK3.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= Constants.MAX_NSK_LEVEL) {
				} else {
					mValueNSK3.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mValueNSK3.setText("");
			}

		}
	}

	/**
	 * Method loading Brand Name on UI element as per the selected tire
	 */
	private void loadSelectedBrand() {
		mBrandArrayList.clear();
		mBrandArrayList.add(mSelectedTire.getBrandName());
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mBrandArrayList);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mBrandSpinner.setAdapter(dataAdapter);
		mBrandSpinner.setEnabled(false);
	}

	/**
	 * Method loading tire-size on UI element as per the selected tire
	 */
	private void loadSelectedSize() {
		mSizeArrayList.clear();
		mSizeArrayList.add(mSelectedtyreSize);
		mSizeDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mSizeArrayList);
		mSizeDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSizeSpinner.setAdapter(mSizeDataAdapter);
		if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED) {
			mSizeSpinner.setEnabled(true);
		}else{
			mSizeSpinner.setEnabled(false);
		}
	}

	/**
	 * Method loading tire-ASP on UI element as per the selected tire
	 */
	private void loadSelectedASP() {
		mTyreTASPArrayList.clear();
		mTyreTASPArrayList.add(mSelectedTyreTASP);
		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTASPSpinner.setAdapter(mASPDataAdapter);
		if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED) {
			mTyreTASPSpinner.setEnabled(true);
		}else{
			mTyreTASPSpinner.setEnabled(false);
		}
	}

	/**
	 * Method loading tire-RIM on UI element as per the selected tire
	 */
	private void loadSelectedRIM() {
		mTyreTRIMArrayList.clear();
		mTyreTRIMArrayList.add(mSelectedtyreTRIM);
		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTRIMSpinner.setAdapter(mRimDataAdapter);
		if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED) {
			mTyreTRIMSpinner.setEnabled(true);
		}else{
			mTyreTRIMSpinner.setEnabled(false);
		}
	}

	/**
	 * Method loading tire-Design on UI element as per the selected tire
	 */
	private void loadSelectedDesign() {
		mDesignArrayList.clear();
		mDesignArrayList.add(mSelectedTire.getDesign());
		mDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mDesignArrayList);
		mDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreDesignSpinner.setAdapter(mDesignDataAdapter);
		mTyreDesignSpinner.setEnabled(false);
	}

	/**
	 * Method loading tire-Design-Details on UI element as per the selected tire
	 */
	private void loadSelectedDesignDetails() {
		mFullDesignArrayList.clear();
		mFullDesignArrayList.add(mSelectedTire.getDesignDetails());
		mFullDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mFullDesignArrayList);
		mFullDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreFullDetailSpinner.setAdapter(mFullDesignDataAdapter);
		mTyreFullDetailSpinner.setEnabled(false);
	}

	/**
	 * Method loading and setting the size details from same axle for the
	 * corresponding selected tire
	 */
	private void loadSizeDetailsFromAxle() {
		loadSelectedSize();
		loadSelectedASP();
		loadSelectedRIM();
	}

	/**
	 * Method loading tire BrandName from the local DB3 file and populating on
	 * UI element as per the selected tire
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreDetailsOnBrandCorrection() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mCursor = mDbHelper.getBrandNameForSWAP();
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				return;
			}
			mCursor.moveToFirst();
			mBrandArrayList.clear();
			mBrandArrayList.add(mDbHelper.getLabel(Constants.SELECT_BRAND));
			while (!mCursor.isAfterLast()) {
				mBrandArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("Description")));
				mCursor.moveToNext();
			}
			CursorUtils.closeCursor(mCursor);
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mBrandArrayList);
			dataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mBrandSpinner.setAdapter(dataAdapter);
			mBrandSpinner.setEnabled(true);
			for (int i = 0; i < mBrandArrayList.size(); i++) {
				if (mBrandArrayList.get(i).equals(mSelectedBrandName)) {
					mBrandSpinner.setSelection(i);
					break;
				}
			}
			mBrandSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							mBrandSpinner
									.setOnItemSelectedListener(listenerSelectBrandName);
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	OnItemSelectedListener listenerSelectBrandName = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			mSelectedBrandName = mBrandSpinner.getSelectedItem().toString();
			if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED){
				mIsSizeSpinnerEditable = true;
			}
			if (mIsSizeSpinnerEditable) {
				clearPreviousSelections(2);
				if (0 < mBrandSpinner.getSelectedItemPosition()) {
					getTyreSizes();
				} else {
					clearSpinnersData(2);
				}
			} else {
				clearPreviousSelections(5);
				if (0 < mBrandSpinner.getSelectedItemPosition()) {
					getTyreDesign();
				} else {
					clearSpinnersData(5);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {

		}
	};

	/**
	 * Method Getting Tire Sizes from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreSizes() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mCursor = mDbHelper.getTyreSizeFromBrand(mSelectedBrandName);
			}
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				mSizeArrayList.clear();
				while (!mCursor.isAfterLast()) {
					mSizeArrayList.add(mCursor.getString(mCursor
							.getColumnIndex("TSize")));
					mCursor.moveToNext();
				}
				CursorUtils.closeCursor(mCursor);
				loadTyreSize();
			} else {
				clearSpinnersData(2);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Method populating Tire Size after getting from local DB3 file as per the
	 * selected tire-brand
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreSize() {
		try {
			mSizeDataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mSizeArrayList);
			mSizeDataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSizeSpinner.setAdapter(mSizeDataAdapter);
			for (int i = 0; i < mSizeArrayList.size(); i++) {
				if (mSizeArrayList.get(i).equals(mSelectedtyreSize)) {
					mSizeSpinner.setSelection(i);
					break;
				}
			}
			mSizeSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
												   int i, long lng) {
							mSelectedtyreSize = mSizeSpinner.getSelectedItem()
									.toString();
							clearPreviousSelections(3);
							if (0 <= mSizeSpinner.getSelectedItemPosition()) {
								getTyreASP();
							} else {
								clearSpinnersData(3);
							}
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method Getting Tire ASP from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreASP() {
		Cursor mCursor = null;
		try {
			if (null != mDbHelper) {
				mCursor = mDbHelper.getTyreASPFromSize(mSelectedtyreSize,
						mSelectedBrandName);
			}
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				mTyreTASPArrayList.clear();
				while (!mCursor.isAfterLast()) {
					mTyreTASPArrayList.add(mCursor.getString(mCursor
							.getColumnIndex("TASP")));
					mCursor.moveToNext();
				}
				CursorUtils.closeCursor(mCursor);
				loadTyreTASP();
			} else {
				clearSpinnersData(3);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Method populating Tire ASP after getting from local DB3 file as per the
	 * selected tire-brand
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreTASP() {
		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTASPSpinner.setAdapter(mASPDataAdapter);
		for (int i = 0; i < mTyreTASPArrayList.size(); i++) {
			if (mTyreTASPArrayList.get(i).equals(mSelectedTyreTASP)) {
				mTyreTASPSpinner.setSelection(i);
				break;
			}
		}
		mTyreTASPSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedTyreTASP = mTyreTASPSpinner.getSelectedItem()
								.toString();
						clearPreviousSelections(4);
						if (0 <= mTyreTASPSpinner.getSelectedItemPosition()) {
							getTyreRIM();
						} else {
							clearSpinnersData(4);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
					}
				});
	}

	/**
	 * Method Getting Tire RIM from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreRIM() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mCursor = mDbHelper.getTyreRimFromSize(mSelectedtyreSize,
						mSelectedTyreTASP, mSelectedBrandName);
			}
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				mTyreTRIMArrayList.clear();
				while (!mCursor.isAfterLast()) {
					mTyreTRIMArrayList.add(mCursor.getString(mCursor
							.getColumnIndex("TRIM")));
					mCursor.moveToNext();
				}
				CursorUtils.closeCursor(mCursor);
				loadTyreTRIM();
			} else {
				clearSpinnersData(4);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Method populating Tire RIM after getting from local DB3 file as per the
	 * selected tire-brand
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreTRIM() {
		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTRIMSpinner.setAdapter(mRimDataAdapter);

		for (int i = 0; i < mTyreTRIMArrayList.size(); i++) {
			if (mTyreTRIMArrayList.get(i).equals(mSelectedtyreTRIM)) {
				mTyreTRIMSpinner.setSelection(i);
				break;
			}
		}
		mTyreTRIMSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedtyreTRIM = mTyreTRIMSpinner.getSelectedItem()
								.toString();
						clearPreviousSelections(5);
						if (0 <= mTyreTRIMSpinner.getSelectedItemPosition()) {
							getTyreDesign();
						} else {
							clearSpinnersData(5);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
					}
				});
	}

	/**
	 * Method Getting Tire Design from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreDesign() {
		Cursor mCursor = null;
		try {
			if (null != mDbHelper) {
				mCursor = mDbHelper.getTyreDesign(mSelectedtyreSize,
						mSelectedTyreTASP, mSelectedtyreTRIM,
						mSelectedBrandName);
			}
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				mDesignArrayList.clear();
				mDesignArrayList.add(mDefaultValueForSpinner);
				while (!mCursor.isAfterLast()) {
					mDesignArrayList.add(mCursor.getString(mCursor
							.getColumnIndex("Deseign")));
					mCursor.moveToNext();
				}
				CursorUtils.closeCursor(mCursor);
				loadTyreDesign();
			} else {
				clearSpinnersData(5);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Method populating Tire Design after getting from local DB3 file as per
	 * the selected tire-brand
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreDesign() {
		mDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mDesignArrayList);
		mDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreDesignSpinner.setAdapter(mDesignDataAdapter);
		for (int i = 0; i < mDesignArrayList.size(); i++) {
			if (mDesignArrayList.get(i).equals(mSelectedTyreDesign)) {
				mTyreDesignSpinner.setSelection(i);
				break;
			}
		}
		mTyreDesignSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedTyreDesign = mTyreDesignSpinner
								.getSelectedItem().toString();
						clearPreviousSelections(6);
						if (0 < mTyreDesignSpinner.getSelectedItemPosition()) {
							getTyreDetailedDesign();// type
						} else {
							clearSpinnersData(6);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
					}
				});
	}

	/**
	 * Method Getting Tire Design-Details from local DB3 file as per the
	 * selected tire-brand
	 */
	public void getTyreDetailedDesign() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mCursor = mDbHelper.getTyreDetailedDesign(mSelectedtyreSize,
						mSelectedTyreTASP, mSelectedtyreTRIM,
						mSelectedTyreDesign, mSelectedBrandName);
			}
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				mFullDesignArrayList.clear();
				mFullDesignArrayList.add(mDefaultValueForSpinner);
				while (!mCursor.isAfterLast()) {
					mFullDesignArrayList.add(mCursor.getString(mCursor
							.getColumnIndex("FullTireDetails")));
					mCursor.moveToNext();
				}
				CursorUtils.closeCursor(mCursor);
				loadTyreDetailedDesign();
			} else {
				clearSpinnersData(6);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Method populating Tire Design-Details after getting from local DB3 file
	 * as per the selected tire-brand
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreDetailedDesign() {
		mFullDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mFullDesignArrayList);
		mFullDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreFullDetailSpinner.setAdapter(mFullDesignDataAdapter);
		for (int i = 0; i < mFullDesignArrayList.size(); i++) {
			if (mFullDesignArrayList.get(i).equals(mSelectedTyreDetailedDesign)) {
				mTyreFullDetailSpinner.setSelection(i);
				break;
			}
		}
		mTyreFullDetailSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedTyreDetailedDesign = mTyreFullDetailSpinner
								.getSelectedItem().toString();
						Constants.FULLDESIGN_SELECTED = mSelectedTyreDetailedDesign;
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
					}
				});
	}

	/**
	 * Method Getting Tire SAPMaterialCodeID from local DB3 file as per the
	 * selected DetailedDesign
	 */
	public void getSAPMaterialCodeID() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mCursor = mDbHelper.getSAPMaterialCodeID(mSelectedtyreSize,
						mSelectedTyreTASP, mSelectedtyreTRIM,
						mSelectedTyreDesign, mSelectedTyreDetailedDesign,
						mSelectedBrandName);
			}
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				mSelectedSAPMaterialCodeID = mCursor.getString(
						mCursor.getColumnIndex("ID")).toString();
				CursorUtils.closeCursor(mCursor);
				if (Constants.onBrandBool == true
						|| Constants.onBrandBoolForJOC == true) {
					Constants.NEW_SAPMATERIAL_CODE = mSelectedSAPMaterialCodeID;
					VehicleSkeletonFragment.updateCorrectedBrandInDB();
				}
				if (!Constants.SELECTED_TYRE.getSerialNumber()
						.equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)) {
					VehicleSkeletonFragment.updateCorrectedSerialNumberInDB();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Method handling validation of radio-buttons for regroove
	 * 
	 * @return
	 */
	private String validate() {
		String messageStr = PerformanceBaseModel.MESSAGE_STR;
		// Intent intentReturn = new Intent();
		int checkedRadioButtonId = mRGRegrooveType.getCheckedRadioButtonId();
		if (checkedRadioButtonId != -1) {
			mRegrooveType = (RadioButton) findViewById(checkedRadioButtonId);
			mReGrooveTypeValue = mRegrooveType.getText().toString();
			if (mRegrooveTypeYes.isChecked()) {
				sFlag = true;
				sIsRadioBtnChecked = true;
			} else if (mRegrooveTypeNo.isChecked()) {
				sFlag = false;
				sIsRadioBtnChecked = true;
			}
		} else {
			sIsRadioBtnChecked = false;
		}
		if (!sIsRadioBtnChecked) {
			if (null != mDbHelper) {
				messageStr = mDbHelper.getLabel(333);
			}
		}
		return messageStr;
	}

	/**
	 * Method handling the data-updates for the tables(JobItem, tire,
	 * JobCorrection etc) for the selected tire. It checks all the mandatory
	 * fields then allows user to navigate back to Vehicle Skeleton after
	 * updating the data
	 */
	private void onReturnSkeleton() {

		getSAPMaterialCodeID();
		String regrooveStr = sFlag == true ? "True" : "False";
		AutoSwapImpl.getMyInstance(getApplicationContext())
				.callAutoSwapImplementation(
						mBrandSpinner.getSelectedItem().toString(),
						mTyreFullDetailSpinner.getSelectedItem().toString(),
						mValueNSK1.getText().toString(),
						mValueNSK2.getText().toString(),
						mValueNSK3.getText().toString(), minNSKSet(),
						regrooveStr);
		updateJobItem();
		Constants.SELECTED_TYRE.setNsk(mValueNSK1.getText().toString());
		Constants.SELECTED_TYRE.setNsk2(mValueNSK2.getText().toString());
		Constants.SELECTED_TYRE.setNsk3(mValueNSK3.getText().toString());
		if (mIsTireReuable) {
			Constants.SELECTED_TYRE.setShared("1");
		} else {
			Constants.SELECTED_TYRE.setShared("0");
		}
		Constants.SELECTED_TYRE.setVehicleJob(Constants.contract
				.getVehicleSapCode());
		Constants.SELECTED_TYRE.setRimType(getRimType());
		Constants.SELECTED_TYRE.setBrandName(mBrandSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setSize(mSizeSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setAsp(mTyreTASPSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setRim(mTyreTRIMSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setDesign(mTyreDesignSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setDesignDetails(mTyreFullDetailSpinner
				.getSelectedItem().toString());
		Constants.SELECTED_TYRE
				.setSapMaterialCodeID(mSelectedSAPMaterialCodeID);
		String remReasonCode = mRemovalReasonCode.get(mSpinnerRemovalReason
				.getSelectedItem().toString());
		// close keyboard if open
		try {
			CommonUtils.hideKeyboard(this, getWindow().getDecorView()
					.getRootView().getWindowToken());
		} catch (Exception e) {
			LogUtil.i("Mount PWT on activity return",
					"could not close keyboard");
		}
		if (remReasonCode.equals("R"))
			Constants.SELECTED_TYRE.setStatus("5");
		else if (remReasonCode.equals("D") || remReasonCode.equals("WO")
				|| remReasonCode.equals("W") || remReasonCode.equals("WOS"))
			Constants.SELECTED_TYRE.setStatus("6");
		else if (remReasonCode.equals("M") || remReasonCode.equals("WT")
				|| remReasonCode.equals("RMD") || remReasonCode.equals("VLD"))
			Constants.SELECTED_TYRE.setStatus("2");
		else if (remReasonCode.equals("MARE") || remReasonCode.equals("MRBS")
				|| remReasonCode.equals("P") || remReasonCode.equals("O"))
			Constants.SELECTED_TYRE.setStatus("3");
		else if (remReasonCode.equals("MIRE") || remReasonCode.equals("PTR"))
			Constants.SELECTED_TYRE.setStatus("4");

//		/*To set VendorSAPCODEID
//		Bug 738_3 : Dismounted tyre is available in Reserve PWT List before job sync
//		VendorSAPCODEID null check and VendorSAPCODEID empty check added
//		*/
//		if ((null == Constants.SAP_VENDORCODE_ID) || (TextUtils.isEmpty(Constants.SAP_VENDORCODE_ID) || Constants.SAP_VENDORCODE_ID == "") ||
//				(null != Constants.SAP_VENDORCODE_ID
//				&& Constants.SAP_VENDORCODE_ID.equals("-1"))) {
//			String sapVendorID = getVendorSAPID(Constants.VENDER_NAME);
//			if (sapVendorID != null)
//				Constants.SAP_VENDORCODE_ID = sapVendorID;
//		}

		/* Reverting. Back to old code
		 As per Michael, We should not Update the tyre SAPVendorCodeID information
		 */
		if(null != Constants.SAP_VENDORCODE_ID
				&& Constants.SAP_VENDORCODE_ID.equals("-1")) {
			LogUtil.i(TAG,"SapVenderCodeID = -1");
			String sapVendorID = getVendorSAPID(Constants.VENDER_NAME);
			if (sapVendorID != null) {
				Constants.SAP_VENDORCODE_ID = sapVendorID;
				//To avoid null updates,SapVenderCodeID update code moved inside if loop.
				Constants.SELECTED_TYRE.setSapVenderCodeID(Constants.SAP_VENDORCODE_ID);
				LogUtil.i(TAG,"SAPVendorCodeID : "+Constants.SELECTED_TYRE.getSapVenderCodeID());
			}
		}

		/**
		 * eJob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
		 * Maintain temporary SAPVENDERCodeID until SAP updates
		 * Since SAPVENDERCodeID for few tyres are null, so by maintaining temporary SAPVENDERCodeID, will make PWT tyre with SAPVENDERCodeID as null, available for Reserve PWT.
		 * added - null string check case for SAPVendorCodeID.
		 */
		if((null == Constants.SELECTED_TYRE.getSapVenderCodeID()) || (TextUtils.isEmpty(Constants.SELECTED_TYRE.getSapVenderCodeID())
				|| Constants.SELECTED_TYRE.getSapVenderCodeID() == "" || Constants.SELECTED_TYRE.getSapVenderCodeID().equalsIgnoreCase("null")))
		{
			LogUtil.i(TAG,"SapVenderCodeID - null or empty or blank");
			String TempSAPVendorID = getVendorSAPID(Constants.VENDER_NAME);
			Constants.SELECTED_TYRE.setTempSAPVendorCodeID(TempSAPVendorID);
			LogUtil.i(TAG,"TempSAPVendorCodeID : "+Constants.SELECTED_TYRE.getTempSAPVendorCodeID());
		}


		if (!Boolean.valueOf(Constants.SELECTED_TYRE.isSpare())) {
			Constants.SELECTED_TYRE.setIsSpare("0");
		}
		try {
			if (Constants.COMESFROMUPDATE)
				Constants.SELECTED_TYRE.setFromJobId(String
						.valueOf(Constants.GETSELECTEDLISTPOSITION));
			else
				Constants.SELECTED_TYRE.setFromJobId(String.valueOf(mDbHelper
						.getJobCount() + 1));
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		//User Trace logs
		try {
			String traceData;
			traceData = "\n\t\tTyre Details : " + Constants.SELECTED_TYRE.getBrandName() +
					"| " + Constants.SELECTED_TYRE.getSize() +
					"| " + Constants.SELECTED_TYRE.getAsp() +
					"| " + Constants.SELECTED_TYRE.getRim() +
					"| " + Constants.SELECTED_TYRE.getDesign() +
					"| " + Constants.SELECTED_TYRE.getDesignDetails() +


					"\n\t\tJob Details : " + Constants.SELECTED_TYRE.getNsk() +
					", " + Constants.SELECTED_TYRE.getNsk2() +
					", " + Constants.SELECTED_TYRE.getNsk3() +
					"| " + mSpinnerRemovalReason.getSelectedItem().toString() +
					"| " + mSpinnerCasingRoute.getSelectedItem().toString() ;

			if (mRegrooveTypeYes.isChecked())
			{
				traceData = traceData+	"| Regrooved" ;
			}
			else
			{
				traceData = traceData+	"| Not Regrooved";
			}
				traceData = traceData+	"| " + Constants.SELECTED_TYRE.getRimType();
			LogUtil.TraceInfo(TRACE_TAG, "none","Data : " + traceData, false, false, false);
		}
		catch (Exception e)
		{
			LogUtil.TraceInfo(TRACE_TAG, "Data : Exception : ", e.getMessage(), false, true, false);
		}
	}

	/**
	 * Method returning SAP VendorID from the local Database as per the vendor
	 * Selected for the vehicle
	 */
	public String getVendorSAPID(String vendorName) {
		String sapVendorID = null;
		Cursor mCursor = null;
		try {
			if (null == mDbHelper) {
				return sapVendorID;
			}
			mCursor = mDbHelper.getvendorIDByNameFromVendorTable(vendorName);
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				sapVendorID = mCursor.getString(mCursor
						.getColumnIndex("ID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
		return sapVendorID;
	}

	/**
	 * Method returning DamageID from the local Database as per the vendor
	 * Selected for the vehicle
	 */
	private String getDamageExternalID() {
		String damage_external_id = "";
		Cursor breakSpotCursor = null;
		try {
			if (null == mDbHelper) {
				return damage_external_id;
			}
			breakSpotCursor = mDbHelper.getDamageExternalId(
					Constants.SELECTED_DAMAGE_TYPE,
					Constants.SELECTED_DAMAGE_AREA);
			if (CursorUtils.isValidCursor(breakSpotCursor)) {
				breakSpotCursor.moveToFirst();
				damage_external_id = breakSpotCursor.getString(breakSpotCursor
						.getColumnIndex("ExternalID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(breakSpotCursor);
		}
		return damage_external_id;
	}

	/**
	 * Get Rim Type as per the user selection
	 */
	private String getRimType() {
		if (mRIMValue.isChecked())
			return "2";
		else
			return "1";
	}

	/**
	 * Method Updating Data for the selected tire in JobItem table with the
	 * corresponding Dismount ActionType
	 */
	private void updateJobItem() {
		LogUtil.DBLog(TAG,"JobItem Data","Insertion Initialized");
		String external_id = String.valueOf(UUID.randomUUID());
		String external_id_tyre = "";
		if (mSelectedTire.getSerialNumber().equals(
				Constants.EDITED_SERIAL_NUMBER)) {
			external_id_tyre = Constants.SELECTED_TYRE.getExternalID();
		} else {
			if (null != mDbHelper) {
				external_id_tyre = mDbHelper
						.getExternalIdFromSerial(Constants.EDITED_SERIAL_NUMBER);
			}
			if (external_id_tyre.equals(""))
				external_id_tyre = String.valueOf(UUID.randomUUID());
		}
		int jobID = getJobItemIDForThisJobItem();
		int len = VehicleSkeletonFragment.mJobItemList.size();
		JobItem jobItem = new JobItem();
		jobItem.setActionType("1");
		jobItem.setAxleNumber("0");
		jobItem.setAxleServiceType("0");
		jobItem.setCasingRouteCode(mCasingRouteCode.get(mSpinnerCasingRoute
				.getSelectedItem()));
		if (mCameraEnabled) {
			jobItem.setDamageId(getDamageExternalID());
		} else {
			jobItem.setDamageId("");
		}
		jobItem.setExternalId(external_id);
		jobItem.setGrooveNumber(null);
		jobItem.setJobId(String.valueOf(jobID));
		jobItem.setMinNSK(minNSKSet());
		jobItem.setNote(mbreakSpotMaxMinFromCameraActvity + mNoteText);
		jobItem.setNskOneAfter(mValueNSK1.getText().toString());
		jobItem.setNskOneBefore("0");
		jobItem.setNskThreeAfter(mValueNSK3.getText().toString());
		jobItem.setNskThreeBefore("0");
		jobItem.setNskTwoAfter(mValueNSK2.getText().toString());
		jobItem.setNskTwoBefore("0");
		jobItem.setOperationID("");
		jobItem.setPressure("0");
		jobItem.setRecInflactionDestination("0");
		jobItem.setRecInflactionOrignal(Constants.SELECTED_TYRE.getPressure());
		if (sFlag) {
			jobItem.setRegrooved("True");
		} else {
			jobItem.setRegrooved("False");
		}
		jobItem.setReGrooveNumber(null);
		jobItem.setRegrooveType("0");
		jobItem.setRemovalReasonId(mRemovalReasonExternalId
				.get(mSpinnerRemovalReason.getSelectedItemPosition()));
		jobItem.setRepairCompany(null);
		jobItem.setRimType(getRimType());
		jobItem.setSapCodeTilD("0");
		jobItem.setSequence(String.valueOf(len + 1));
		jobItem.setServiceCount("0");
		jobItem.setServiceID("0");
		jobItem.setSwapType("0");
		jobItem.setTorqueSettings("0");
		jobItem.setTyreID(external_id_tyre);
		jobItem.setWorkOrderNumber(null);
		LogUtil.DBLog(TAG, "Job Item Data", "Inserted" + jobItem.toString());
		VehicleSkeletonFragment.mJobItemList.add(jobItem);
		LogUtil.DBLog(TAG, "Job Item Data", "Inserted");
		Constants.SELECTED_TYRE.setExternalID(external_id_tyre);
		jobItem.setThreaddepth("0");
		// Edited serial No. was not coming, hence added
		jobItem.setSerialNumber(Constants.EDITED_SERIAL_NUMBER);
		jobItem.setBrandName(mBrandSpinner.getSelectedItem().toString());
		jobItem.setFullDesignDetails(mTyreFullDetailSpinner.getSelectedItem()
				.toString());
		jobItem.setSwapOriginalPosition(null);
		int maxJobItemId = 0;
		// Cursor jobItemCursor = null;
		// try {
		// if (null != mDbHelper) {
		// jobItemCursor = mDbHelper.getJobItemIDMAX();
		// if (CursorUtils.isValidCursor(jobItemCursor)) {
		// jobItemCursor.moveToFirst();
		// maxJobItemId = jobItemCursor.getInt(jobItemCursor
		// .getColumnIndex("ID"))
		// + VehicleSkeletonFragment.mJobItemList.size() - 1;
		// }
		// }
		// } catch (SQLException e) {
		// e.printStackTrace();
		// } finally {
		// CursorUtils.closeCursor(jobItemCursor);
		// }
		if (Constants.sBitmapPhoto1 != null) {
			TireImageItem item1 = new TireImageItem();
			item1.setJobItemId(String.valueOf(jobID));
			if (mCameraEnabled)
				item1.setImageType("0");
			else
				item1.setImageType("1");
			item1.setDamageDescription(Constants.DAMAGE_NOTES1);
			item1.setTyreImage(getByteFromBitmap(Constants.sBitmapPhoto1,
					Constants.sBitmapPhoto1Quality));
			item1.setJobExternalId(external_id);
			VehicleSkeletonFragment.mTireImageItemList.add(item1);
		}
		if (Constants.sBitmapPhoto2 != null) {
			TireImageItem item2 = new TireImageItem();
			item2.setJobItemId(String.valueOf(jobID));
			item2.setImageType("1");
			item2.setDamageDescription(Constants.DAMAGE_NOTES2);
			item2.setTyreImage(getByteFromBitmap(Constants.sBitmapPhoto2,
					Constants.sBitmapPhoto2Quality));
			item2.setJobExternalId(external_id);
			VehicleSkeletonFragment.mTireImageItemList.add(item2);
		}
		if (Constants.sBitmapPhoto3 != null) {
			TireImageItem item3 = new TireImageItem();
			item3.setJobItemId(String.valueOf(jobID));
			item3.setImageType("1");
			item3.setDamageDescription(Constants.DAMAGE_NOTES3);
			item3.setTyreImage(getByteFromBitmap(Constants.sBitmapPhoto3,
					Constants.sBitmapPhoto3Quality));
			item3.setJobExternalId(external_id);
			VehicleSkeletonFragment.mTireImageItemList.add(item3);
		}
	}

	/**
	 * Method returning maximum count of Job present in the JobItem table
	 * 
	 * @return: Max Job Count
	 */
	private int getJobItemIDForThisJobItem() {
		int countJobItemsNotPresent = 0;
		int currentJobItemCountInDB = mDbHelper.getJobItemCount();
		Cursor cursor = null;
		try {
			cursor = mDbHelper.getJobItemValues();
			if (!CursorUtils.isValidCursor(cursor)) {
				return countJobItemsNotPresent + 1
						+ VehicleSkeletonFragment.mJobItemList.size();
			}
			for (JobItem jobItem : VehicleSkeletonFragment.mJobItemList) {
				boolean flag = false;
				for (boolean hasItem = cursor.moveToFirst(); hasItem; hasItem = cursor
						.moveToNext()) {
					if (jobItem.getExternalId().equals(
							cursor.getString(cursor
									.getColumnIndex("ExternalID")))) {
						countJobItemsNotPresent++;
						break;
					}
				}
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			// CursorUtils.closeCursor(cursor);
		}
		return currentJobItemCountInDB - countJobItemsNotPresent
				+ VehicleSkeletonFragment.mJobItemList.size() + 1;
	}

	/**
	 * Get bitmap image in bytes
	 */
	private byte[] getByteFromBitmap(Bitmap bmp, int quality) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(CompressFormat.JPEG, quality, stream);
		byte[] byteArray = stream.toByteArray();
		return byteArray;
	}

	/**
	 * Method to get minimum NSK of all three
	 */
	private String minNSKSet() {
		float smallest;
		float a = CommonUtils.parseFloat(mValueNSK1.getText().toString());
		float b = CommonUtils.parseFloat(mValueNSK2.getText().toString());
		float c = CommonUtils.parseFloat(mValueNSK3.getText().toString());
		if (a < b && a < c) {
			smallest = a;
		} else if (b < c && b < a) {
			smallest = b;
		} else {
			smallest = c;
		}
		return String.valueOf(smallest);
	}

	@Override
	public void onBackPressed() {
		if (areAnyChangesMade()) {
			actionBackPressed();
		} else {
			LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press", false, false, false);
			goBack();
		}
	};

	/**
	 * Method reseting all Camera related Parameters
	 */
	public void resetParams() {
		Constants.ISBREAKSPOT = false;
		Constants.SELECTED_DAMAGE_TYPE = null;
		Constants.SELECTED_DAMAGE_AREA = null;
		Constants.DAMAGE_NOTES1 = "";
		Constants.DAMAGE_NOTES2 = "";
		Constants.DAMAGE_NOTES3 = "";
		Constants.sBitmapPhoto1 = null;
		Constants.sBitmapPhoto2 = null;
		Constants.sBitmapPhoto3 = null;
	}

	/**
	 * Method Capturing the dialog state before orientation change
	 */
	private void saveDialogState(Bundle state) {
		state.putBoolean("mBackAlertDialog",
				(mBackAlertDialog != null && mBackAlertDialog.isShowing()));
	}

	/**
	 * Method Capturing the dialog state after orientation change
	 */
	private void restoreDialogState(Bundle state) {
		if (state != null) {
			if (state.getBoolean("mBackAlertDialog")) {
				actionBackPressed();
			}
		}
	}

	/**
	 * Method handling the action performed where user presses the back button
	 * Alert Box created with two option: YES and NO
	 */
	private void actionBackPressed() {
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", false, true, false);
		LayoutInflater li = LayoutInflater.from(DismountTireActivity.this);
		View promptsView = li
				.inflate(R.layout.dialog_logout_confirmation, null);
		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
				DismountTireActivity.this);
		alertDialogBuilder.setView(promptsView);

		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});
		TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
		infoTv.setText(mConfirmMsg);
		alertDialogBuilder.setCancelable(false).setPositiveButton(mYesLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
						goBack();
					}
				});
		alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);
						dialog.cancel();
					}
				});
		mBackAlertDialog = alertDialogBuilder.create();
		mBackAlertDialog.show();
		mBackAlertDialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * Method handling the action performed where user presses the back button
	 * without entering any details
	 */
	private void goBack() {
		CameraUtility.resetParams();
		this.finish();
		resetParams();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.regroove_tire_operation, menu);
		if (mNotesLabel != null) {
			menu.findItem(R.id.action_save).setTitle(mNotesLabel);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_save) {
			LogUtil.TraceInfo(TRACE_TAG, "none", "Save", false, false, false);
			Intent startNotesActivity = new Intent(this, NoteActivity.class);
			startNotesActivity.putExtra("sentfrom", "regroove");
			startNotesActivity.putExtra("lastnote", mNoteText);
			startActivityForResult(startNotesActivity,
					DismountTireActivity.NOTES_INTENT);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == DismountTireActivity.NOTES_INTENT) {
			if (data != null) {
				mNoteText = data.getExtras().getString("mNoteEditText");
			}
		}
		if (requestCode == CAMERA_INTENT_CODE && data != null
				&& data.hasExtra(CameraActivity.BREAK_SPOT_KEY)) {
			mbreakSpotMaxMinFromCameraActvity = data.getExtras().getString(
					CameraActivity.BREAK_SPOT_KEY);
		}
		if (requestCode == CAMERA_INTENT_CODE && data != null
				&& data.hasExtra(CameraActivity.DAMAGE_SCRAP_KEY)) {
			mbreakSpotMaxMinFromCameraActvity = data.getExtras().getString(
					CameraActivity.DAMAGE_SCRAP_KEY);
		}
	}

	/**
	 * @author amitkumar.h Class providing the animation when user swipes out
	 *         from the activity It also handles the actions performed when use
	 *         is swiping out from the activity after matching all the required
	 *         conditions
	 */
	public class Swiper implements OnTouchListener, OnClickListener {
		float startX, startY;
		float endX, endY;
		int selectedPositionToDelete;
		ArrayAdapter<String> adapterList;
		ScrollView view;
		private Context ctx;
		public static final float MINIMUM_MOVEMENT_REQUIRED = 100;

		public Swiper(Context ctx, ScrollView view) {
			this.ctx = ctx;
			this.view = view;
			view.setOnClickListener(this);
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getActionMasked()) {
			case MotionEvent.ACTION_DOWN:
				mSwiped = false;
				startX = event.getX();
				startY = event.getY();
				break;
			case MotionEvent.ACTION_UP:
				break;

			case MotionEvent.ACTION_MOVE:
				endX = event.getX();
				endY = event.getY();
				if (Math.abs(endX - startX) > MINIMUM_MOVEMENT_REQUIRED
						&& !mSwiped) {
					mSwiped = true;
					Rect rect = new Rect();
					int childCount = view.getChildCount();
					int[] listViewCoords = new int[2];
					view.getLocationOnScreen(listViewCoords);
					int x = (int) event.getRawX() - listViewCoords[0];
					int y = (int) event.getRawY() - listViewCoords[1];
					View child;
					for (int i = 0; i < childCount; i++) {
						child = view.getChildAt(i);
						child.getHitRect(rect);
						if (rect.contains(x, y)) {
							if (mBrandSpinner.isEnabled()
									&& mBrandSpinner.getSelectedItemPosition() == 0) {
								String message = mDbHelper
										.getLabel(Constants.SELECT_BRAND);
								//User Trace logs
								if(message!=null) {
									LogUtil.TraceInfo(TRACE_TAG, "Brand Validation","Msg : " +message,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Brand Validation","Msg : Check Brand",false,false,false);
								}
								CommonUtils.notify(message,
										DismountTireActivity.this);
							} else if (mTyreFullDetailSpinner
									.getSelectedItemPosition() < 0
									|| mTyreFullDetailSpinner.getSelectedItem()
											.toString()
											.equals(mDefaultValueForSpinner)) {
								String message = mDbHelper.getLabel(82);
								//User Trace logs
								if(message!=null) {
									LogUtil.TraceInfo(TRACE_TAG, "Tyre Validation","Msg : " +message,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Brand Validation","Msg : Check Tyre",false,false,false);
								}
								CommonUtils.notify(message,
										DismountTireActivity.this);
							} else if (!CommonUtils.validateNSKValues(
									mValueNSK1.getText().toString(), mValueNSK2
											.getText().toString(), mValueNSK3
											.getText().toString())) {
								//User Trace logs
								if(Constants.sLblEnterNskValues!=null) {
									LogUtil.TraceInfo(TRACE_TAG, "Nsk Validation","Msg : " +Constants.sLblEnterNskValues,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Brand Validation","Msg : Check NSK",false,false,false);
								}
								CommonUtils.notify(
										Constants.sLblEnterNskValues,
										DismountTireActivity.this);
							} else if (mSpinnerRemovalReason
									.getSelectedItemPosition() == 0) {
								String message = mDbHelper.getLabel(93);
								//User Trace logs
								if(message!=null) {
									LogUtil.TraceInfo(TRACE_TAG, "RemovalReason Validation","Msg : " +message,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Brand Validation","Msg : Check RemovalReason",false,false,false);
								}
								CommonUtils.notify(message,
										DismountTireActivity.this);
							} else if (mSpinnerCasingRoute
									.getSelectedItemPosition() == 0) {
								String message = mDbHelper.getLabel(94);
								//User Trace logs
								if(message!=null) {
									LogUtil.TraceInfo(TRACE_TAG, "CasingRoute Validation","Msg : " +message,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Brand Validation","Msg : Check CasingRoute",false,false,false);
								}
								CommonUtils.notify(message,
										DismountTireActivity.this);
							} else if (mCameraEnabled
									&& Constants.sBitmapPhoto1 == null
									&& Constants.sBitmapPhoto2 == null
									&& Constants.sBitmapPhoto3 == null) {
								String message = mDbHelper.getLabel(185);
								//User Trace logs
								if(message!=null) {
									LogUtil.TraceInfo(TRACE_TAG, "Camera Validation","Msg : " +message,false,false,false);
								}
								CommonUtils.notify(message,
										DismountTireActivity.this);
							} else if (mCameraEnabled
									&& Constants.sBitmapPhoto1 == null) {
								String message = mDbHelper.getLabel(184);
								//User Trace logs
								if(message!=null) {
									LogUtil.TraceInfo(TRACE_TAG, "Camera Validation","Msg : " +message,false,false,false);
								}
								CommonUtils.notify(message,
										DismountTireActivity.this);
							} else if (mCameraEnabled
									&& (Constants.SELECTED_DAMAGE_TYPE == null || Constants.SELECTED_DAMAGE_AREA == null)) {
								String message = mDbHelper.getLabel(428);
								//User Trace logs
								if(message!=null) {
									LogUtil.TraceInfo(TRACE_TAG, "Camera Validation","Msg : " +message,false,false,false);
								}
								CommonUtils.notify(message,
										DismountTireActivity.this);
							}
							else if (mNSKThresholdCrossed
									&& Constants.sBitmapPhoto1 == null) {
								String message = mDbHelper.getLabel(185);
								//User Trace logs
								if(message!=null) {
									LogUtil.TraceInfo(TRACE_TAG, "NSKThresholdCrossed Validation","Msg : " +message,false,false,false);
								}
								CommonUtils.notify(message,
										DismountTireActivity.this);
							}else {

								new PerformanceBaseModel(
										DismountTireActivity.this, callback,
										false).perform();
							}
						}
					}
				}
				break;
			}
			return false;
		}

		@Override
		public void onClick(View arg0) {
		}
	}

	IOnPerformanceCallback callback = new IOnPerformanceCallback() {

		@Override
		public void updateTables() {
			onReturnSkeleton();
		}

		@Override
		public void showProgressBar(boolean toShow) {
			if (toShow) {
				// Show Progress Bar here
			} else {
				// Hide Progress bar here
			}
		}

		@Override
		public void onPostExecute(ResponseMessagePerformance message) {
			Intent intentReturn = new Intent();
			intentReturn.putExtra("dismount", true);
			setResult(VehicleSkeletonFragment.DISMOUNT_INTENT, intentReturn);
			Constants.JOB_CORRECTION_LIST = VehicleSkeletonFragment.mJobcorrectionList;
			Constants.onBrandBool = false;
			finish();
			resetParams();
			DismountTireActivity.this.overridePendingTransition(
					R.anim.slide_left_in, R.anim.slide_left_out);
		}

		@Override
		public String onPreExecute() {
			return validate();
		}

		@Override
		public void onError(ResponseMessagePerformance message) {
			CommonUtils.notify(message.getErrorMessage(),
					DismountTireActivity.this);
		}

		@Override
		public void closePreviousTask() {
			if (task != null && !task.isCancelled()) {
				task.cancel(true);
			}
		}
	};

	/**
	 * Method reseting Camera Images taken during photo capture
	 */
	private void resetCamaraImages() {
		String curntSelection = mSpinnerRemovalReason.getSelectedItem()
				.toString();
		if (sPreviousActionType == null) {
			sPreviousActionType = mSpinnerRemovalReason.getSelectedItem()
					.toString();
		} else if (!curntSelection.equals(sPreviousActionType)) {
			Constants.SELECTED_DAMAGE_TYPE = null;
			Constants.SELECTED_DAMAGE_AREA = null;
			Constants.DAMAGE_NOTES1 = "";
			Constants.DAMAGE_NOTES2 = "";
			Constants.DAMAGE_NOTES3 = "";
			Constants.sBitmapPhoto1 = null;
			Constants.sBitmapPhoto2 = null;
			Constants.sBitmapPhoto3 = null;
			mbreakSpotMaxMinFromCameraActvity = "";
			sPreviousActionType = curntSelection;
		}
	}

	/**
	 * Method returning Current Image Count
	 */
	private int getImageCount() {
		int curntImgCount = 0;
		if (null != Constants.sBitmapPhoto1) {
			curntImgCount++;
		}
		if (null != Constants.sBitmapPhoto2) {
			curntImgCount++;
		}
		if (null != Constants.sBitmapPhoto3) {
			curntImgCount++;
		}
		return curntImgCount;
	}

	/**
	 * When spinner value is null then dependent spinner values refreshing.
	 */
	private void clearSpinnersData(int i) {
		// Don't add break statement, It's sequence of execution
		switch (i) {
		case 1: // Brand Spinner
		case 2: // Size Spinner
			mSizeArrayList.clear();
			if (null != mSizeDataAdapter) {
				mSizeDataAdapter.notifyDataSetChanged();
			}
		case 3: // ASP Spinner
			mTyreTASPArrayList.clear();
			if (null != mASPDataAdapter) {
				mASPDataAdapter.notifyDataSetChanged();
			}
		case 4: // Rim Spinner
			mTyreTRIMArrayList.clear();
			if (null != mRimDataAdapter) {
				mRimDataAdapter.notifyDataSetChanged();
			}
		case 5: // Design Spinner
			mDesignArrayList.clear();
			if (null != mDesignDataAdapter) {
				mDesignDataAdapter.notifyDataSetChanged();
			}
		case 6: // FullDetails Spinner
			mFullDesignArrayList.clear();
			if (mFullDesignDataAdapter != null) {
				mFullDesignDataAdapter.notifyDataSetChanged();
			}
		}
	}

	/**
	 * Method Clearing the Previous Selection Values of the spinners present in
	 * the UI
	 */
	private void clearPreviousSelections(int i) {
		if (mIsOrientationChanged && i < mLastSpinnerSelection) {
			if (i == 6 && mLastSpinnerSelection == 7) {
				mIsOrientationChanged = false;
			}
			return;
		} else if (mIsOrientationChanged) {
			mIsOrientationChanged = false;
		}
		// Don't add break statement, It's sequence of execution
		switch (i) {
		case 1: // Brand Spinner
		case 2: // Size Spinner
			mSelectedtyreSize = "";
		case 3: // ASP Spinner
			mSelectedTyreTASP = "";
		case 4: // Rim Spinner
			mSelectedtyreTRIM = "";
		case 5: // Design Spinner
			mSelectedTyreDesign = "";
		case 6: // FullDetails Spinner
			mSelectedTyreDetailedDesign = "";
		}
	}

	/**
	 * Method Checking the Previous Selected Spinner just before the change in
	 * orientation
	 */
	private void checkLatestSpinnerSelectionBeforeOrientationChanges() {
		if (null == mSelectedBrandName
				|| mPleaseSelectBrand.equals(mSelectedBrandName)) {
			mLastSpinnerSelection = 1;
		} else if (null == mSelectedtyreSize || "".equals(mSelectedtyreSize)) {
			mLastSpinnerSelection = 2;
		} else if (null == mSelectedTyreTASP || "".equals(mSelectedTyreTASP)) {
			mLastSpinnerSelection = 3;
		} else if (null == mSelectedtyreTRIM || "".equals(mSelectedtyreTRIM)) {
			mLastSpinnerSelection = 4;
		} else if (null == mSelectedTyreDesign
				|| mDefaultValueForSpinner.equals(mSelectedTyreDesign)) {
			mLastSpinnerSelection = 5;
		} else if (null == mSelectedTyreDetailedDesign
				|| mDefaultValueForSpinner.equals(mSelectedTyreDetailedDesign)) {
			mLastSpinnerSelection = 6;
		} else {
			mLastSpinnerSelection = 7;
		}
	}

	/**
	 * Method handling the Setting up of data on the UI elements(Spinners,labels
	 * etc) for Auto-Logical Swap
	 */
	public void setDataForAutoSwap() {
		if (!TextUtils.isEmpty(Constants.EDITED_SERIAL_NUMBER)
				&& !TextUtils.isEmpty(Constants.tempTyrePositionNew)
				&& AutoSwapImpl
						.checkMultipeSwap(Constants.EDITED_SERIAL_NUMBER)
				&& VehicleSkeletonFragment.getTireByPosition(
						Constants.tempTyrePositionNew).getTyreState() != TyreState.EMPTY
				&& !Constants.EDITED_SERIAL_NUMBER
						.equalsIgnoreCase(Constants.tempTyreSerial)
				&& !TextUtils.isEmpty(Constants.tempTyrePositionNew)) {

			Tyre mAutoSwappedTire = VehicleSkeletonFragment
					.getTireByPosition(Constants.tempTyrePositionNew);
			mBrandForAutoSwap = mAutoSwappedTire.getBrandName();
			mSerialNumberForAutoSwap = mAutoSwappedTire.getSerialNumber();
			mSizeForAutoSwap = mAutoSwappedTire.getSize();
			mRimForAutoSwap = mAutoSwappedTire.getRim();
			mAspForAutoSwap = mAutoSwappedTire.getAsp();
			mDesignForAutoSwap = mAutoSwappedTire.getDesign();
			mDesignDetailsForAutoSwap = mAutoSwappedTire.getDesignDetails();
			mNskForAutoSwap = mAutoSwappedTire.getNsk();
			mNsk2ForAutoSwap = mAutoSwappedTire.getNsk2();
			mNsk3ForAutoSwap = mAutoSwappedTire.getNsk3();
			// For SAp material Code
			if (TextUtils.isEmpty(mBrandForAutoSwap)) {
				mSelectedBrandName = mBrandForAutoSwap;
			} else {
				mSelectedBrandName = mBrandForAutoSwap;
				mSelectedtyreSize = mSizeForAutoSwap;
				mSelectedtyreTRIM = mRimForAutoSwap;
				mSelectedTyreTASP = mAspForAutoSwap;
				mSelectedTyreDesign = mDesignForAutoSwap;
				mSelectedTyreDetailedDesign = mDesignDetailsForAutoSwap;
			}
			// setting values here
			if (Constants.onBrandBool || TextUtils.isEmpty(mBrandForAutoSwap)) {
				// mIsSizeSpinnerEditable = false;
				mTyreFullDetailSpinner.setEnabled(true);
				mTyreDesignSpinner.setEnabled(true);
				mTyreFullDetailSpinner.setAdapter(null);
				mTyreDesignSpinner.setAdapter(null);
				mSelectedBrandName = "";
				loadTyreDetailsOnBrandCorrection();
			} else {
				loadSelectedBrandForAutoSwap(mBrandForAutoSwap);
				loadSelectedSizeForAutoSwap(mSizeForAutoSwap);
				loadSelectedASPForAutoSwap(mAspForAutoSwap);
				loadSelectedRIMForAutoSwap(mRimForAutoSwap);
				loadSelectedDesignForAutoSwap(mDesignForAutoSwap);
				loadSelectedDesignDetailsForAutoSwap(mDesignDetailsForAutoSwap);

				mValueNSK1.setText(mNskForAutoSwap);
				mValueNSK2.setText(mNsk2ForAutoSwap);
				mValueNSK3.setText(mNsk3ForAutoSwap);
			}
			if (!mSizeForAutoSwap.equalsIgnoreCase("0")) {
				mIsSizeSpinnerEditable = false;
			}
		}
	}

	/**
	 * Method to load tire brand with no brand correction
	 * 
	 * @param mBrandForAutoSwap2
	 */
	private void loadSelectedBrandForAutoSwap(String mBrandForAutoSwap2) {
		mBrandArrayList.clear();
		mBrandArrayList.add(mBrandForAutoSwap2);
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mBrandArrayList);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mBrandSpinner.setAdapter(dataAdapter);
		mBrandSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire size with no brand correction
	 * 
	 * @param mSizeForAutoSwap2
	 */
	private void loadSelectedSizeForAutoSwap(String mSizeForAutoSwap2) {
		mSizeArrayList.clear();
		mSizeArrayList.add(mSizeForAutoSwap2);
		mSizeDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mSizeArrayList);
		mSizeDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSizeSpinner.setAdapter(mSizeDataAdapter);
		mSizeSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire ASP with no brand correction
	 * 
	 * @param mAspForAutoSwap2
	 */
	private void loadSelectedASPForAutoSwap(String mAspForAutoSwap2) {
		mTyreTASPArrayList.clear();
		mTyreTASPArrayList.add(mAspForAutoSwap2);
		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTASPSpinner.setAdapter(mASPDataAdapter);
		mTyreTASPSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire RIM with no brand correction
	 * 
	 * @param mRimForAutoSwap2
	 */
	private void loadSelectedRIMForAutoSwap(String mRimForAutoSwap2) {
		mTyreTRIMArrayList.clear();
		mTyreTRIMArrayList.add(mRimForAutoSwap2);
		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTRIMSpinner.setAdapter(mRimDataAdapter);
		mTyreTRIMSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire design with no brand correction
	 * 
	 * @param mDesignForAutoSwap2
	 */
	private void loadSelectedDesignForAutoSwap(String mDesignForAutoSwap2) {
		mDesignArrayList.clear();
		mDesignArrayList.add(mDesignForAutoSwap2);
		mDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mDesignArrayList);
		mDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreDesignSpinner.setAdapter(mDesignDataAdapter);
		mTyreDesignSpinner.setEnabled(false);
	}

	/**
	 * Method to load tire full design details with no brand correction
	 * 
	 * @param mDesignDetailsForAutoSwap2
	 */
	private void loadSelectedDesignDetailsForAutoSwap(
			String mDesignDetailsForAutoSwap2) {
		mFullDesignArrayList.clear();
		mFullDesignArrayList.add(mDesignDetailsForAutoSwap2);
		mFullDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mFullDesignArrayList);
		mFullDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreFullDetailSpinner.setAdapter(mFullDesignDataAdapter);
		mTyreFullDetailSpinner.setEnabled(false);
	}

	/**
	 * Method to get SAPMaterialCodeID with brand correction
	 */
	public void getSAPMaterialCodeIDForAutoSwap() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mCursor = mDbHelper.getSAPMaterialCodeID(mSelectedtyreSize,
						mSelectedTyreTASP, mSelectedtyreTRIM,
						mSelectedTyreDesign, mSelectedTyreDetailedDesign,
						mSelectedBrandName);
			}
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				mSelectedSAPMaterialCodeID = mCursor.getString(
						mCursor.getColumnIndex("ID")).toString();
				CursorUtils.closeCursor(mCursor);
				if (Constants.onBrandBool == true) {
					Constants.NEW_SAPMATERIAL_CODE = mSelectedSAPMaterialCodeID;
					VehicleSkeletonFragment.updateCorrectedBrandInDB();
				}
				if (!Constants.SELECTED_TYRE.getSerialNumber()
						.equalsIgnoreCase(Constants.EDITED_SERIAL_NUMBER)
						&& Constants.SELECTED_TYRE.getTyreState() != TyreState.NON_MAINTAINED) {
					VehicleSkeletonFragment.updateCorrectedSerialNumberInDB();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
	}

	/**
	 * Method storing state of the tire when user initiates a tire operation
	 * 
	 * @param bundle
	 */
	private void captureInitialState(Bundle bundle) {
		if (bundle != null) {
			mFormElements = bundle.getParcelable("dismountForm");
		} else {
			mFormElements = new TyreFormElements();
			mFormElements.setBrand(mBrandSpinner.getSelectedItemPosition());
			mFormElements.setDesign(mTyreDesignSpinner
					.getSelectedItemPosition());
			mFormElements.setType(mTyreFullDetailSpinner
					.getSelectedItemPosition());
			mFormElements.setNsk1(mValueNSK1.getText().toString());
			mFormElements.setNsk2(mValueNSK2.getText().toString());
			mFormElements.setNsk3(mValueNSK3.getText().toString());
			mFormElements.setRemovalReason(mSpinnerRemovalReason
					.getSelectedItemPosition());
			mFormElements.setCastingRoute(mSpinnerCasingRoute
					.getSelectedItemPosition());
			mFormElements.setRegrooveType(mRGRegrooveType
					.getCheckedRadioButtonId());
			mFormElements.setRimType(mRIMValue.isChecked());
		}
	}

	private void saveInitialState(Bundle bundle) {
		bundle.putParcelable("dismountForm", mFormElements);
	}

	/**
	 * Method Returns true if any change made
	 * 
	 * @return
	 */
	private boolean areAnyChangesMade() {
		boolean isSameAsInitial = true;
		isSameAsInitial &= mFormElements.getBrand() == mBrandSpinner
				.getSelectedItemPosition();
		isSameAsInitial &= mFormElements.getDesign() == mTyreDesignSpinner
				.getSelectedItemPosition();
		isSameAsInitial &= mFormElements.getType() == mTyreFullDetailSpinner
				.getSelectedItemPosition();
		isSameAsInitial &= mFormElements.getNsk1().equals(
				mValueNSK1.getText().toString());
		isSameAsInitial &= mFormElements.getNsk2().equals(
				mValueNSK2.getText().toString());
		isSameAsInitial &= mFormElements.getNsk3().equals(
				mValueNSK3.getText().toString());
		isSameAsInitial &= mFormElements.getRemovalReason() == mSpinnerRemovalReason
				.getSelectedItemPosition();
		isSameAsInitial &= mFormElements.getCastingRoute() == mSpinnerCasingRoute
				.getSelectedItemPosition();
		isSameAsInitial &= mFormElements.getRegrooveType() == mRGRegrooveType
				.getCheckedRadioButtonId();
		isSameAsInitial &= mFormElements.isRimType() == mRIMValue.isChecked();
		return !isSameAsInitial;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		mNoteText = "";
		InactivityUtils.logoutFromActivityAboveEjobForm(this);
	}
}