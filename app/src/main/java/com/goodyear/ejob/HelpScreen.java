package com.goodyear.ejob;

import android.app.Activity;
import android.database.SQLException;
import android.os.Bundle;
import android.webkit.WebView;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.util.Constants;

import java.util.ArrayList;

/**
 * @author johnmiya.s
 * @version 1.0
 */
public class HelpScreen extends Activity implements InactivityHandler {

	/**
	 * Used for common database handler
	 */
	private DatabaseAdapter mDBHelper;
	private String mAboutContent = "";
	private ArrayList<String> mVal2;
	private ArrayList<String> mVal3;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
		openDB();
		int recordCount = getCountForHelpRecords();
		int startsFrom = 830000;
		int endsAt = startsFrom + recordCount;
		for (int i = startsFrom; i < endsAt; i++) {
			mAboutContent = mAboutContent.concat(mDBHelper.getLabel(i));
		}
		get();
		finalStringFetch();
		deletePipeSymbol();
		// set settings activity name
		String helpScreenActivityName = mDBHelper
				.getLabel(Constants.HELP_LABEL);
		setTitle(helpScreenActivityName);

		String mime = "text/html";
		String encoding = "utf-8";
		WebView myWebView = (WebView) this.findViewById(R.id.aboutWebView);
		myWebView.getSettings().setJavaScriptEnabled(true);
		myWebView
				.loadDataWithBaseURL(null, mAboutContent, mime, encoding, null);
	}

	/**
	 * Used to remove '|' from mAboutContent
	 */
	private void deletePipeSymbol() {
		if (mAboutContent.contains("|")) {
			mAboutContent = mAboutContent.replace("|", "");
		}

	}

	private void finalStringFetch() {
		if (mAboutContent.contains("|")) {
			for (int i = 0; i < mVal3.size(); i++) {
				java.util.regex.Matcher.quoteReplacement(mDBHelper
						.getLabel(Integer.parseInt(mVal2.get(i))));
				mAboutContent = mAboutContent.replaceAll(mVal2.get(i),
						mDBHelper.getLabel(Integer.parseInt(mVal2.get(i))));
			}
		}
	}

	/**
	 * Used to open the database connection
	 */
	public void openDB() {
		try {
			mDBHelper = new DatabaseAdapter(this);
			mDBHelper.createDatabase();
			mDBHelper.open();
		} catch (SQLException e4) {
			e4.printStackTrace();
		}
	}

	public void get() {
		ArrayList<Integer> val = new ArrayList<>();
		mVal2 = new ArrayList<>();
		mVal3 = new ArrayList<>();
		for (int i = 0; i < mAboutContent.length(); i++) {
			if (mAboutContent.charAt(i) == '|') {
				val.add(i + 1);
			}
		}
		for (int i = 0; i < val.size();) {
			String middle = "";
			middle = middle.concat(mAboutContent.substring(val.get(i),
					val.get(i + 1)));
			// ss= finalStr.replace(mVal.get(i), mVal.get(i + 1), "123");
			middle = middle.replace("|", "");
			mVal3.add("|" + middle + "|");
			mVal2.add(middle);
			i = i + 2;
		}
	}

	/**
	 * * @return the number of CountAboutRecords from database
	 */
	private int getCountForHelpRecords() {
		return mDBHelper.getCountHelpRecords();
	}

	@Override
	protected void onStart() {
		super.onStart();
		// set user inactivity handler for this activity
		LogoutHandler.setCurrentActivity(this);
	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		InactivityUtils.logoutFromActivityBelowEjobForm(this);
	}
}
