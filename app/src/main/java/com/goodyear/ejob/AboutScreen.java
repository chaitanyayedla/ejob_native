package com.goodyear.ejob;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.database.SQLException;
import android.os.Bundle;
import android.webkit.WebView;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.util.Constants;

import java.util.ArrayList;

/**
 * This activity is used to display About Screen. The content will be loaded
 * from DB.
 * 
 * @version 1.0
 */
public class AboutScreen extends Activity implements InactivityHandler {

	/**
	 * Used for common database handler
	 */
	private DatabaseAdapter mDBHelper;
	/**
	 * Screen content will be stored from DB in this variable
	 */
	private String mAboutContent = "";
	/**
	 * List of Strings.
	 */
	private ArrayList<String> mVal2;
	/**
	 * List of strings
	 */
	private ArrayList<String> mVal3;
	/**
	 * about start index in DB
	 */
	private static final int ABOUT_START_INDEX = 810000;

	@SuppressLint("SetJavaScriptEnabled")
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
		openDB();
		int recordCount = getCountForAboutRecords();
		int endsAt = ABOUT_START_INDEX + recordCount;
		for (int i = ABOUT_START_INDEX; i < endsAt; i++) {
			mAboutContent = mAboutContent.concat(mDBHelper.getLabel(i));
		}
		setUpDataBaseForParsingData();
		finalStringFetch();
		deletePipeSymbol();
		String mime = "text/html";
		String encoding = "utf-8";

		// set settings activity name
		String aboutScreenActivityName = mDBHelper
				.getLabel(Constants.ABOUT_LABEL);
		setTitle(aboutScreenActivityName);

		WebView aboutWebView = (WebView) this.findViewById(R.id.aboutWebView);
		aboutWebView.getSettings().setJavaScriptEnabled(true);
		aboutWebView.loadDataWithBaseURL(null, mAboutContent, mime, encoding,
				null);
	}

	/**
	 * Used to remove '|' from mAboutContent
	 */
	private void deletePipeSymbol() {
		if (mAboutContent.contains("|")) {
			mAboutContent = mAboutContent.replace("|", "");
		}

	}

	/**
	 * Method used to fetch the final Strings from DB.
	 */
	private void finalStringFetch() {
		if (mAboutContent.contains("|")) {
			for (int i = 0; i < mVal3.size(); i++) {
				if (!mVal2.get(i).equals("200")) {
					{
						mAboutContent = mAboutContent.replaceAll(mVal2.get(i),
								mDBHelper.getLabel(Integer.parseInt(mVal2
										.get(i))));
					}
				}
			}
		}
		mAboutContent = mAboutContent
				.replaceAll("200", mDBHelper.getLabel(200));
	}

	/**
	 * Used to open the database connection
	 */
	private void openDB() {
		try {
			mDBHelper = new DatabaseAdapter(this);
			mDBHelper.createDatabase();
			mDBHelper.open();
		} catch (SQLException e4) {
			e4.printStackTrace();
		}
	}

	/**
	 * Method used to add the content to Strings array.
	 */
	private void setUpDataBaseForParsingData() {
		ArrayList<Integer> val = new ArrayList<>();
		mVal2 = new ArrayList<>();
		mVal3 = new ArrayList<>();
		for (int i = 0; i < mAboutContent.length(); i++) {
			if (mAboutContent.charAt(i) == '|') {
				val.add(i + 1);
			}
		}
		for (int i = 0; i < val.size();) {
			String middle = "";
			middle = middle.concat(mAboutContent.substring(val.get(i),
					val.get(i + 1)));
			middle = middle.replace("|", "");
			mVal3.add("|" + middle + "|");
			mVal2.add(middle);
			i = i + 2;
		}
	}

	/**
	 * @return the number of CountAboutRecords from database
	 */
	private int getCountForAboutRecords() {
		return mDBHelper.getCountAboutRecords();
	}

	@Override
	protected void onStart() {
		super.onStart();
		// set user inactivity handler for this activity
		LogoutHandler.setCurrentActivity(this);
	}

	/**
	 * On destroy of this activity this method is called
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mDBHelper != null)
			mDBHelper.close();
	}

	/**
	 * when user interacts with user this mehtod is called
	 */
	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	/**
	 * this method is called from the broadcast receiver when auto log out times
	 * up (see User Inactivty for details)
	 */
	@Override
	public void logUserOutDueToInactivity() {
		InactivityUtils.logoutFromActivityBelowEjobForm(this);
	}

}
