package com.goodyear.ejob.job.operation.helpers;

import java.io.Serializable;

/**
 * @author amitkumar.h
 * 
 */
public class LogicalSwapInfo implements Serializable{
	/**
	 * string variable for getting and setting fields
	 */
	private String mNskOne;
	private String mNskTwo;
	private String mNskThree;
	private int mRimType;
	private boolean mRegrooved;

	/**
	 * @return the nskOne
	 */
	public String getNskOne() {
		return mNskOne;
	}

	/**
	 * @param nskOne the nskOne to set
	 */
	public void setNskOne(String nskOne) {
		this.mNskOne = nskOne;
	}

	/**
	 * @return the nskTwo
	 */
	public String getNskTwo() {
		return mNskTwo;
	}

	/**
	 * @param nskTwo the nskTwo to set
	 */
	public void setNskTwo(String nskTwo) {
		this.mNskTwo = nskTwo;
	}

	/**
	 * @return the nskThree
	 */
	public String getNskThree() {
		return mNskThree;
	}

	/**
	 * @param nskThree the nskThree to set
	 */
	public void setNskThree(String nskThree) {
		this.mNskThree = nskThree;
	}

	/**
	 * @return the rimType
	 */
	public int getRimType() {
		return mRimType;
	}

	/**
	 * @param rimType the rimType to set
	 */
	public void setRimType(int rimType) {
		this.mRimType = rimType;
	}

	/**
	 * @return the regrooved
	 */
	public boolean isRegrooved() {
		return mRegrooved;
	}

	/**
	 * @param regrooved the regrooved to set
	 */
	public void setRegrooved(boolean regrooved) {
		this.mRegrooved = regrooved;
	}

}
