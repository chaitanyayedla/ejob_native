package com.goodyear.ejob.job.operation.helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.DBModel;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.ui.jobcreation.AutoSwapImpl;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.TyreState;
import com.goodyear.ejob.util.VehicleSkeletonInfo;

/**
 * @author shailesh.p
 * 
 */
public class PressureSwap {

	private Tyre selectedTire;
	private Context context;
	private float recommendedPressure;
	private int axlepos, axleposOne, axleposTwo;
	private String save;
	private String cancel;
	private String recInflaction;
	private String pressureUnit;
	private String pressureRecHeading;
	private boolean isSettingsPSI;
	private String errorMessage;
	private float barValue;
	private boolean isSwapSecondTire;
	private static EditText pressureValue;
	private static final String TAG = "PressureSwap";

	public static int axlePosSaved;
	public static float axlePressureSaved;
	public static String pressureSet;
	/**
	 * variable returns true if pressure popup is open
	 */
	public static boolean dialogCreated;
	public static boolean loadSavedPressure;
	AlertDialog alertDialog;
	private boolean mEnableSecondDialogue = true;
	private String mPositionLabel;

	public static int position_swap_tyre = 1;

	//User Trace logs trace tag
	private static final String TRACE_TAG = "Pressure Swap";

	/**
	 * @return the position_swap_tyre
	 */

	public PressureSwap(Tyre tire, Context context) {
		selectedTire = tire;
		this.context = context;

	}

	public static int getAxlePos() {
		return axlePosSaved;
	}

	public static float getAxlePressure() {
		return axlePressureSaved;
	}

	public static String getPressureSet() {
		return pressureValue.getText().toString();
	}

	public static boolean getDialogSet() {
		return dialogCreated;
	}

	public static boolean isDoRetorque() {
		return VehicleSkeletonFragment.mDoRetroque;
	}

	public void updatePressureFromAxleRecommended(float recommendedPressure,
			int axlePosition) {
		updatePressureFromAxleRecommended(recommendedPressure, axlePosition,
				false);
	}

	public void setmPressureValue(String mPressureValu) {
		if (pressureValue != null)
			pressureValue.setText(mPressureValu);
	}

	public void togglePressureField(boolean enable) {
		if (pressureValue != null)
			pressureValue.setEnabled(enable);
	}

	public void toggleSecondDialogueController(boolean enable) {
		this.mEnableSecondDialogue = enable;
	}

	public void dismissDialog() {
		if (alertDialog != null) {
			alertDialog.dismiss();
		}
	}

	public void updatePressureFromAxleRecommended(float recommendedPressure,
			int axlePosition, boolean isSwapSecondTire) {

		// float recommendedPressureOne,recommendedPressureTwo;
		this.recommendedPressure = recommendedPressure;
		this.axlepos = axlePosition;
		this.isSwapSecondTire = isSwapSecondTire;

		getSetPressure();
		getLabelsFromDB();

		if (recommendedPressure == 0) {
			axlePosSaved = axlepos;
			axlePressureSaved = recommendedPressure;
			// if pressure for axle is not set
			// prompt the user for axle pressure
			createPressureDialog();
		} else {
			// if pressure is set for the axle
			// update the tire pressure to
			// tire
			if (Constants.DO_PHYSICAL_SWAP) {

				// For First Tyre
				int axlepos1 = Character
						.getNumericValue(Constants.SELECTED_TYRE.getPosition()
								.charAt(0)) - 1;
				float recommendedPressure1 = VehicleSkeletonFragment.mAxleRecommendedPressure
						.get(axlepos1);
				Constants.SELECTED_TYRE.setPressure(String
						.valueOf(recommendedPressure1));

				// For Second Tyre
				int axlepos2 = Character
						.getNumericValue(Constants.SECOND_SELECTED_TYRE
								.getPosition().charAt(0)) - 1;
				float recommendedPressure2 = VehicleSkeletonFragment.mAxleRecommendedPressure
						.get(axlepos2);
				Constants.SECOND_SELECTED_TYRE.setPressure(String
						.valueOf(recommendedPressure2));

				if (recommendedPressure1 > 0) {
					System.out.println("issue caused:::");
					position_swap_tyre = 2;
				}

				// For Double Swap
				if (Constants.THIRD_SELECTED_TYRE != null) {
					selectedTire = Constants.THIRD_SELECTED_TYRE;
					// position_swap_tyre = 3;
					int axlepos3 = Character
							.getNumericValue(Constants.THIRD_SELECTED_TYRE
									.getPosition().charAt(0)) - 1;
					float recommendedPressure3 = VehicleSkeletonFragment.mAxleRecommendedPressure
							.get(axlepos3);
					Constants.THIRD_SELECTED_TYRE.setPressure(String
							.valueOf(recommendedPressure3));
				}

				// Triple Swap
				if (Constants.FOURTH_SELECTED_TYRE != null) {
					selectedTire = Constants.FOURTH_SELECTED_TYRE;
					// position_swap_tyre = 4;
					axlepos = Character
							.getNumericValue(Constants.FOURTH_SELECTED_TYRE
									.getPosition().charAt(0)) - 1;
					recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
							.get(axlepos);
					Constants.FOURTH_SELECTED_TYRE.setPressure(String
							.valueOf(recommendedPressure));
				}

				if (recommendedPressure != 0 && recommendedPressure2 != 0) {
					if ((Constants.SELECTED_TYRE != null && Constants.SECOND_SELECTED_TYRE != null)
							&& (!Constants.SELECTED_TYRE.isRetorqued() || !Constants.SECOND_SELECTED_TYRE
									.isRetorqued())) {
						Retorque retorque = new Retorque(selectedTire, context);
						VehicleSkeletonFragment.mRetroqueStarted = true;
						position_swap_tyre = 1;
						retorque.startRetroqueProcess();
						VehicleSkeletonFragment.mSwapPressureStarted = false;
					}
				}

			} else if (Constants.DO_LOGICAL_SWAP) {
				// For Spare
				if (Constants.SELECTED_TYRE != null
						&& Constants.SELECTED_TYRE.getPosition().contains("SP")) {
					try {
						VehicleSkeletonInfo vehicleconfg = new VehicleSkeletonInfo(
								Constants.vehicleConfiguration);
						vehicleconfg.getVehicleSkeletonInfo();
						int axelCount = vehicleconfg.getAxlecount();
						if (Constants.SELECTED_TYRE.getPosition().startsWith(
								"SP")) {
							if ((vehicleconfg.getAxlecount() + 1) == axelCount) {
								// Do nothing
							} else {
								++axelCount;
							}
							Constants.SELECTED_TYRE.setPosition((axelCount)
									+ ""
									+ Constants.SELECTED_TYRE.getPosition());
						} else {
							Constants.SELECTED_TYRE
									.setPosition(Constants.SELECTED_TYRE
											.getPosition());
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				int axlepos1_log = Character
						.getNumericValue(Constants.SELECTED_TYRE.getPosition()
								.charAt(0)) - 1;
				float recommendedPressure1_log = VehicleSkeletonFragment.mAxleRecommendedPressure
						.get(axlepos1_log);
				Constants.SELECTED_TYRE.setPressure(String
						.valueOf(recommendedPressure1_log));

				int axlepos2_log = Character
						.getNumericValue(Constants.SECOND_SELECTED_TYRE
								.getPosition().charAt(0)) - 1;
				float recommendedPressure2_log = VehicleSkeletonFragment.mAxleRecommendedPressure
						.get(axlepos2_log);
				Constants.SECOND_SELECTED_TYRE.setPressure(String
						.valueOf(recommendedPressure2_log));

				// For Double Swap
				if (Constants.THIRD_SELECTED_TYRE != null) {
					selectedTire = Constants.THIRD_SELECTED_TYRE;
					int axlepos3_log = Character
							.getNumericValue(Constants.THIRD_SELECTED_TYRE
									.getPosition().charAt(0)) - 1;
					float recommendedPressure3_log = VehicleSkeletonFragment.mAxleRecommendedPressure
							.get(axlepos3_log);
					Constants.THIRD_SELECTED_TYRE.setPressure(String
							.valueOf(recommendedPressure3_log));

				}

				// for setting pressure in history
				int axlepos1 = Character
						.getNumericValue(Constants.SELECTED_TYRE.getPosition()
								.charAt(0)) - 1;
				float recommendedPressure1 = VehicleSkeletonFragment.mAxleRecommendedPressure
						.get(axlepos1);
				int axlepos2 = Character
						.getNumericValue(Constants.SECOND_SELECTED_TYRE
								.getPosition().charAt(0)) - 1;
				float recommendedPressure2 = VehicleSkeletonFragment.mAxleRecommendedPressure
						.get(axlepos2);
				float recommendedPressure3 = -1;
				if (Constants.THIRD_SELECTED_TYRE != null) {
					int axlepos3 = Character
							.getNumericValue(Constants.THIRD_SELECTED_TYRE
									.getPosition().charAt(0)) - 1;
					recommendedPressure3 = VehicleSkeletonFragment.mAxleRecommendedPressure
							.get(axlepos3);
				}
				float recommendedPressure4 = -1;
				if (Constants.FOURTH_SELECTED_TYRE != null) {
					selectedTire = Constants.FOURTH_SELECTED_TYRE;
					// position_swap_tyre = 4;
					int axlepos4 = Character
							.getNumericValue(Constants.FOURTH_SELECTED_TYRE
									.getPosition().charAt(0)) - 1;
					recommendedPressure4 = VehicleSkeletonFragment.mAxleRecommendedPressure
							.get(axlepos4);

				}

				if (recommendedPressure1 != 0 && recommendedPressure2 != 0
						&& recommendedPressure3 != 0
						&& recommendedPressure4 != 0) {
					setJobItemPressure();
					System.out.println("Triple Swap");
				} else if (recommendedPressure1 != 0
						&& recommendedPressure2 != 0
						&& recommendedPressure3 != 0) {
					setJobItemPressure();
					System.out.println("Double Swap");
				} else if (recommendedPressure1 != 0
						&& recommendedPressure2 != 0) {
					setJobItemPressure();
					System.out.println("SingleSwap");
				}

				if (!VehicleSkeletonFragment.mIsPressureHigh) {

					if (recommendedPressure > 0 && recommendedPressure2 == 0) {
					} else {
						System.out.println("reset");
						VehicleSkeletonFragment.resetBoolValue();
						VehicleSkeletonFragment.resetTyres();
					}

				}
			}
			// VehicleSkeletonFragment.mSwapPressureStarted = false;
		}

		// For Spare
		if (Constants.SELECTED_TYRE != null
				&& Constants.SELECTED_TYRE.getPosition().contains("SP")) {
			try {
				VehicleSkeletonInfo vehicleconfg = new VehicleSkeletonInfo(
						Constants.vehicleConfiguration);
				vehicleconfg.getVehicleSkeletonInfo();
				int axelCount = vehicleconfg.getAxlecount();
				if (Constants.SELECTED_TYRE.getPosition().startsWith("SP")) {
					if ((vehicleconfg.getAxlecount() + 1) == axelCount) {
						// Do nothing
					} else {
						++axelCount;
					}
					Constants.SELECTED_TYRE.setPosition((axelCount) + ""
							+ Constants.SELECTED_TYRE.getPosition());
				} else {
					Constants.SELECTED_TYRE.setPosition(Constants.SELECTED_TYRE
							.getPosition());
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// for setting pressure in history
		float recommendedPressure1 = -1;
		if (Constants.SELECTED_TYRE != null) {
			int axlepos1 = Character.getNumericValue(Constants.SELECTED_TYRE
					.getPosition().charAt(0)) - 1;
			recommendedPressure1 = VehicleSkeletonFragment.mAxleRecommendedPressure
					.get(axlepos1);
		}
		float recommendedPressure2 = -1;
		if (Constants.SECOND_SELECTED_TYRE != null) {
			int axlepos2 = Character
					.getNumericValue(Constants.SECOND_SELECTED_TYRE
							.getPosition().charAt(0)) - 1;
			recommendedPressure2 = VehicleSkeletonFragment.mAxleRecommendedPressure
					.get(axlepos2);
		}

		float recommendedPressure3 = -1;
		if (Constants.THIRD_SELECTED_TYRE != null) {
			int axlepos3 = Character
					.getNumericValue(Constants.THIRD_SELECTED_TYRE
							.getPosition().charAt(0)) - 1;
			recommendedPressure3 = VehicleSkeletonFragment.mAxleRecommendedPressure
					.get(axlepos3);
		}
		float recommendedPressure4 = -1;
		if (Constants.FOURTH_SELECTED_TYRE != null) {
			// position_swap_tyre = 4;
			int axlepos4 = Character
					.getNumericValue(Constants.FOURTH_SELECTED_TYRE
							.getPosition().charAt(0)) - 1;
			recommendedPressure4 = VehicleSkeletonFragment.mAxleRecommendedPressure
					.get(axlepos4);

		}

		if (recommendedPressure1 != 0 && recommendedPressure2 != 0
				&& recommendedPressure3 != 0 && recommendedPressure4 != 0) {
			setJobItemPressure();
			System.out.println("Triple Swap");
		} else if (recommendedPressure1 != 0 && recommendedPressure2 != 0
				&& recommendedPressure3 != 0) {
			setJobItemPressure();
			System.out.println("Double Swap");
		} else if (recommendedPressure1 != 0 && recommendedPressure2 != 0) {
			setJobItemPressure();
			System.out.println("SingleSwap");
		}

	}

	/**
	 * 
	 */
	private void getSetPressure() {
		if (CommonUtils.getPressureUnit(context).equals(
				Constants.PSI_PRESSURE_UNIT)) {
			isSettingsPSI = true;
		} else {
			isSettingsPSI = false;
		}
	}

	/**
	 * 
	 */
	public void startCreatePressureDialog() {
		getSetPressure();
		recommendedPressure = axlePressureSaved;
		axlepos = axlePosSaved;
		getLabelsFromDB();
		createPressureDialog();
	}

	public static void savePressureCheck() {

		if (pressureValue != null) {
			String strPressureValue = pressureValue.getText().toString().trim();
			if (!TextUtils.isEmpty(strPressureValue)) {
				axlePressureSaved = CommonUtils.parseFloat(strPressureValue);
			}
		}
	}

	/**
	 * 
	 */
	private void getLabelsFromDB() {
		DatabaseAdapter label = new DatabaseAdapter(context);
		label.createDatabase();
		label.open();

		save = label.getLabel(Constants.SAVE_LABEL);
		cancel = label.getLabel(Constants.CANCEL_LABEL);
		recInflaction = label.getLabel(Constants.REC_INFLACTION);
		mPositionLabel = label.getLabel(Constants.POSITION_LABEL);
		pressureUnit = CommonUtils.getPressureUnit(context);
		pressureRecHeading = label.getLabel(Constants.RECCOMENDED_PRESSURE);
		errorMessage = label.getLabel(Constants.ERROR_MESSAGE);
		Constants.sLblCheckPressureValue = label
				.getLabel(Constants.CHECK_PRESSURE_LABEL);
	}

	public void createPressureDialog() {
		LogUtil.i(TAG, "Created Pressure Dialog");
		dialogCreated = true;
		LogUtil.TraceInfo(TRACE_TAG, "none", "Dialog", true, false, false);

		LayoutInflater inflator = LayoutInflater.from(context);
		View promptsView = inflator.inflate(R.layout.pressure_tor_check, null);

		TextView pressureLabel = (TextView) promptsView
				.findViewById(R.id.pressure_rec_label);
		pressureLabel.setText(recInflaction);
		final TextView pressure_pos_label = (TextView) promptsView
				.findViewById(R.id.pressure_pos_label);
		pressure_pos_label.setText(mPositionLabel);

		// set user activity for
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		// dialogText.setText(message);
		pressureValue = (EditText) promptsView
				.findViewById(R.id.value_rec_pressure);
		pressureValue.setEnabled(mEnableSecondDialogue);

		final TextView position = (TextView) promptsView.findViewById(R.id.value_pos);

		if (selectedTire != null) {
			position.setText(selectedTire.getPosition());
		}

		// if created after orientation change
		if (loadSavedPressure && axlePressureSaved != 0.0) {
			pressureValue.setText(String.valueOf(axlePressureSaved));
		}

		/*Bug 728 : Adding context to show toast for higher pressure value*/
		pressureValue.addTextChangedListener(new PressureCheckListener(
				pressureValue, isSettingsPSI, context));

		if (isSettingsPSI) {
			pressureValue
					.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
							4, 2) });// PSI 150.00

		} else {
			pressureValue
					.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
							3, 2) });// BAR 10.34
		}

		TextView pressureRecTitle = (TextView) promptsView
				.findViewById(R.id.pressure_rec_title);
		pressureRecTitle.setText(pressureRecHeading);

		final TextView pressureUnitLabel = (TextView) promptsView
				.findViewById(R.id.pressure_unit);
		pressureUnitLabel.setText(pressureUnit);

		final EjobAlertDialog dialogBuilder = new EjobAlertDialog(context);

		// set prompts.xml to alertdialog builder
		dialogBuilder.setView(promptsView);

		// add header
		// dialogBuilder.setTitle(pressureRecHeading);

		dialogBuilder.setCancelable(false).setPositiveButton(save,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// set user activity
						dialogBuilder.updateInactivityForDialog();

						LogUtil.TraceInfo(TRACE_TAG, "none", "BT - Save", false, false, false);
						VehicleSkeletonFragment.mSwapPressureStarted = true;
						String pressureSet = pressureValue.getText().toString()
								.trim();
						if (TextUtils.isEmpty(pressureSet)
								|| ".".equals(pressureSet)) {
							LogUtil.TraceInfo(TRACE_TAG, "none", "Msg : "+Constants.sLblCheckPressureValue, false, false, false);
							CommonUtils.notify(
									Constants.sLblCheckPressureValue, context);
							createPressureDialog();
							return;
						}

						if (!CommonUtils.pressureValueValidation(pressureValue)) {
							LogUtil.TraceInfo(TRACE_TAG, "none", "Msg : "+Constants.sLblCheckPressureValue, false, false, false);
							CommonUtils.notify(
									Constants.sLblCheckPressureValue, context);
							createPressureDialog();
							return;
						}

						// on click of apply
						barValue = 0;
						if (isSettingsPSI) {
							// change PSI to BAR
							barValue = CommonUtils.parseFloat(pressureSet) * 0.068947f;
						} else {
							barValue = CommonUtils.parseFloat(pressureSet);
						}

						selectedTire.setPressure(String.valueOf(barValue));
						VehicleSkeletonFragment.mAxleRecommendedPressure.set(
								axlepos, barValue);
						Constants.FORSWAP_BARVALUE = barValue;

						VehicleSkeletonFragment.mTorStarted = false;

						if (Constants.DO_LOGICAL_SWAP) {
							DBModel ejobDbInsert = new DBModel(context);
							ejobDbInsert.open();
							VehicleSkeletonFragment
									.insertJobAfterOperation(ejobDbInsert);
							ejobDbInsert.close();
						}

						if ((Constants.DO_LOGICAL_SWAP || Constants.DO_PHYSICAL_SWAP)
								&& !isSwapSecondTire) {
							// For Spare
							if (Constants.SECOND_SELECTED_TYRE != null
									&& Constants.SECOND_SELECTED_TYRE
											.getPosition().contains("SP")) {
								try {
									VehicleSkeletonInfo vehicleconfg = new VehicleSkeletonInfo(
											Constants.vehicleConfiguration);
									vehicleconfg.getVehicleSkeletonInfo();
									int axelCount = vehicleconfg.getAxlecount();
									if (Constants.SECOND_SELECTED_TYRE
											.getPosition().startsWith("SP")) {
										if ((vehicleconfg.getAxlecount() + 1) == axelCount) {
											// Do nothing
										} else {
											++axelCount;
										}
										Constants.SECOND_SELECTED_TYRE
												.setPosition((axelCount)
														+ ""
														+ Constants.SECOND_SELECTED_TYRE
																.getPosition());
									} else {
										Constants.SECOND_SELECTED_TYRE
												.setPosition(Constants.SECOND_SELECTED_TYRE
														.getPosition());
									}

								} catch (Exception e) {
									e.printStackTrace();
								}
							}

							isSwapSecondTire = true;

							if (Constants.SECOND_SELECTED_TYRE.getPosition() != null) {
								int axlepos = Character
										.getNumericValue(Constants.SECOND_SELECTED_TYRE
												.getPosition().charAt(0)) - 1;
								// for single swap
								float recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
										.get(axlepos);
								selectedTire = Constants.SECOND_SELECTED_TYRE;
								position_swap_tyre = 2;
								if (recommendedPressure == 0) {
									if (Constants.THIRD_SELECTED_TYRE == null
											&& Constants.FOURTH_SELECTED_TYRE == null) {
										updatePressureFromAxleRecommended(
												recommendedPressure, axlepos,
												true);
										setJobItemPressure();
										LogUtil.i(TAG,
												"Setting pressure value for Single Swap");
										return;
									} else {
										updatePressureFromAxleRecommended(
												recommendedPressure, axlepos,
												false);
										LogUtil.i(TAG,
												"Setting pressure value for Double Swap");
										return;
									}
								} else {
									selectedTire.setPressure(String
											.valueOf(recommendedPressure));
								}
							}

							// Double Swap
							if (Constants.THIRD_SELECTED_TYRE != null) {
								selectedTire = Constants.THIRD_SELECTED_TYRE;
								position_swap_tyre = 3;
								int axlepos = Character
										.getNumericValue(Constants.THIRD_SELECTED_TYRE
												.getPosition().charAt(0)) - 1;
								float recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
										.get(axlepos);

								if (recommendedPressure == 0) {
									if (Constants.FOURTH_SELECTED_TYRE == null) {
										updatePressureFromAxleRecommended(
												recommendedPressure, axlepos,
												true);
										setJobItemPressure();
										LogUtil.i(TAG,
												"Setting pressure value for Double Swap(1st Scenario)");
										return;
									} else {
										updatePressureFromAxleRecommended(
												recommendedPressure, axlepos,
												false);
										dialogCreated = false;
										LogUtil.i(TAG,
												"Setting pressure value for Triple Swap");
										return;
									}
								} else {
									Constants.THIRD_SELECTED_TYRE.setPressure(String
											.valueOf(recommendedPressure));
									LogUtil.i(TAG,
											"Setting pressure value for third tire");
								}
								dialogCreated = false;
							}

							// Triple Swap
							if (Constants.FOURTH_SELECTED_TYRE != null) {
								selectedTire = Constants.FOURTH_SELECTED_TYRE;
								position_swap_tyre = 4;
								int axlepos = Character
										.getNumericValue(Constants.FOURTH_SELECTED_TYRE
												.getPosition().charAt(0)) - 1;
								float recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
										.get(axlepos);
								if (recommendedPressure == 0) {
									updatePressureFromAxleRecommended(
											recommendedPressure, axlepos, true);
									setJobItemPressure();
									LogUtil.i(TAG,
											"Setting pressure value for Forth tire");
									dialogCreated = false;
									return;
								} else {
									Constants.FOURTH_SELECTED_TYRE.setPressure(String
											.valueOf(recommendedPressure));
								}
								isSwapSecondTire = true;
							}
							setJobItemPressure();
							dialogCreated = false;
						}
						if (Constants.DO_PHYSICAL_SWAP && isSwapSecondTire) {
							setJobItemPressure();
							int axleposFirstTire = Character
									.getNumericValue(Constants.SELECTED_TYRE
											.getPosition().charAt(0)) - 1;
							int axleposSecondTire = Character
									.getNumericValue(Constants.SECOND_SELECTED_TYRE
											.getPosition().charAt(0)) - 1;
							if ((axleposFirstTire != axleposSecondTire)
									&& (!Constants.SELECTED_TYRE.isRetorqued() || !Constants.SECOND_SELECTED_TYRE
											.isRetorqued())
									|| !VehicleSkeletonFragment.mRetroqueStarted) {
								Retorque retorque = new Retorque(selectedTire,
										context);
								VehicleSkeletonFragment.mRetroqueStarted = true;
								position_swap_tyre = 1;
								retorque.startRetroqueProcess();
								VehicleSkeletonFragment.mSwapPressureStarted = false;
								LogUtil.i(TAG,
										"Setting pressure value for Single Swap(DO_PHYSICAL_SWAP)");
							}
							dialogCreated = false;
						} else if (Constants.DO_LOGICAL_SWAP
								&& isSwapSecondTire) {
							setJobItemPressure();
							VehicleSkeletonFragment.resetBoolValue();
							VehicleSkeletonFragment.resetTyres();
							LogUtil.i(TAG,
									"Setting pressure value for Single Swap(DO_LOGICAL_SWAP)");
							dialogCreated = false;
						}
						setJobItemPressure();
						position_swap_tyre = 1;
						try {
							String Trace_Data = position.getText().toString()+ " | "
									+ pressureValue.getText().toString()
									+ pressureUnitLabel.getText().toString();
							LogUtil.TraceInfo(TRACE_TAG, "none", "Data : " + Trace_Data, false, false, false);
						}
						catch (Exception e)
						{
							LogUtil.TraceInfo(TRACE_TAG, "none", "Data : Exception "+e.getMessage(), false, false, false);
						}
						try {
							// Activity activity = (Activity) context;
							CommonUtils.hideKeyboard(context, alertDialog
									.getWindow().getDecorView().getRootView()
									.getWindowToken());
						} catch (Exception e) {
							LogUtil.i("retorque onacitivityresult",
									"could not close keyboard");
						}
						EjobFormActionActivity parentActivity = (EjobFormActionActivity) context;
						parentActivity.closeConnection();
						LogUtil.i(TAG, "Setting default pressure value");
						dialogCreated = false;
					}
				});

		dialogBuilder.setCancelable(false).setNegativeButton(cancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// set user activity
						dialogBuilder.updateInactivityForDialog();

						LogUtil.TraceInfo(TRACE_TAG, "none", "BT - Cancel", false, false, false);

						if(Constants.SECOND_SELECTED_TYRE.getTyreState() == TyreState.EMPTY){
							
							AutoSwapImpl.singleSwap(Constants.SELECTED_TYRE.getPosition(), Constants.SECOND_SELECTED_TYRE.getPosition());
							if (VehicleSkeletonFragment.mJobItemList.size() > 1) {
								VehicleSkeletonFragment.mJobItemList.remove(VehicleSkeletonFragment.mJobItemList
										.size() - 1);
								VehicleSkeletonFragment.mJobItemList.remove(VehicleSkeletonFragment.mJobItemList
										.size() - 1);
								Constants.INSERT_SWAP_DATA = 0;
							}
							VehicleSkeletonFragment.resetTyreState();
							VehicleSkeletonFragment.updateTireImages();
							VehicleSkeletonFragment.resetTyres();
							VehicleSkeletonFragment.resetBoolValue();
							dialog.dismiss();
							}
						
					}
				});

		// create alert dialog
		alertDialog = dialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public void setJobItemPressure() {
		// Setting pressure in JobItem
		for (int i = 0; i < VehicleSkeletonFragment.mJobItemList.size(); i++) {

			String strTyreID = VehicleSkeletonFragment.mJobItemList.get(i)
					.getTyreID();
			String strPressure = VehicleSkeletonFragment.mJobItemList.get(i)
					.getPressure();

			/*
			 * System.out.println("JobItem ----- strTyreID--outside---"+strTyreID
			 * ); System.out.println("JobItem ----- strPressure---outside--"+
			 * strPressure);
			 */
			for (int j = 0; j < VehicleSkeletonFragment.tyreInfo.size(); j++) {
				String strExternalid = VehicleSkeletonFragment.tyreInfo.get(j)
						.getExternalID();
				if (strTyreID.equalsIgnoreCase(strExternalid)) {

					if (TextUtils.isEmpty(strPressure)
							|| Double.valueOf(strPressure) == 0) {
						/*
						 * System.out.println("JobItem ----- strTyreID--"+strTyreID
						 * );
						 * System.out.println("tyreInfo ----- strExternalid--"
						 * +strExternalid);
						 * System.out.println("JobItem ----- strPressure--"
						 * +strPressure);
						 * System.out.println("tyreInfo ----- strPressure--"
						 * +VehicleSkeletonFragment
						 * .tyreInfo.get(j).getPressure());
						 */
						JobItem jobItem = VehicleSkeletonFragment.mJobItemList
								.get(i);
						jobItem.setRecInflactionDestination(String
								.valueOf(VehicleSkeletonFragment.tyreInfo
										.get(j).getPressure()));
						jobItem.setPressure(VehicleSkeletonFragment.tyreInfo
								.get(j).getPressure());
						VehicleSkeletonFragment.mJobItemList.set(i, jobItem);

						/*
						 * System.out.println("JobItem ----- New strPressure--"+
						 * VehicleSkeletonFragment
						 * .jobItemList.get(i).getPressure());
						 */
					}
				}
			}
		}
	}

}
