package com.goodyear.ejob.job.operation.helpers;

/**
 * @author shailesh.p
 * 
 */
public class TireImageItem {
	private String jobItemId;
	private String imageType;
	private byte[] tireImage;
	private String damageDescription;
	private String jobExternalId;

	/**
	 * @return the jobItemId
	 */
	public String getJobItemId() {
		return jobItemId;
	}

	/**
	 * @param jobItemId the jobItemId to set
	 */
	public void setJobItemId(String jobItemId) {
		this.jobItemId = jobItemId;
	}

	/**
	 * @return the imageType
	 */
	public String getImageType() {
		return imageType;
	}

	/**
	 * @param imageType the imageType to set
	 */
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	/**
	 * @return the Tyre Image
	 */
	public byte[] getTyreImage() {
		return tireImage;
	}

	/**
	 * @param tireImage the tireImage to set
	 */
	public void setTyreImage(byte[] tireImage) {
		this.tireImage = tireImage;
	}

	/**
	 * @return the damageDescription
	 */
	public String getDamageDescription() {
		return damageDescription;
	}

	/**
	 * @param damageDescription the damageDescription to set
	 */
	public void setDamageDescription(String damageDescription) {
		this.damageDescription = damageDescription;
	}

	public String getJobExternalId() {
		return jobExternalId;
	}

	public void setJobExternalId(String jobExternalId) {
		this.jobExternalId = jobExternalId;
	}
}
