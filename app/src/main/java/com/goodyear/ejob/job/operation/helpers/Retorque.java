package com.goodyear.ejob.job.operation.helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.goodyear.ejob.NoteActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.DBModel;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.sync.ActionType;
import com.goodyear.ejob.sync.Constant;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.Job;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.TireActionType;
import com.goodyear.ejob.util.Tyre;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author shailesh.p
 * 
 */
public class Retorque {

	public static boolean startedNotes = false;
	private Tyre selectedTire;
	private Context context;
	public static boolean retorqueDone;
	private static String retorqueValue;
	private static String wrenchNumber = "";
	private String cancel;
	private String mYesLabel;
	private String mNoLabel;
	private static boolean retorqueLabelIssued;
	public static ArrayList<TorqueSettings> torqueSettings;
	private TextView wrenchNumberLabel;
	private TextView retorque_label_issued;

	private String retorqueLessMessage;

	private static TextView retorque_title_label;
	private static EditText retorque_Value;
	private static TextView retorque_label;
	private static EditText wrenchValue;
	private static Switch reTorqueLabel;

	/**
	 * Additional Widgets to display Position
	 */
	private TextView positionLabel;
	private TextView valuePosition;

	public static int currentPosition;
	public static int retorqueValueSaved;
	public static String wrenchNumberSaved;
	public static boolean retorqueLabelIssuedSaved;
	public static boolean loadSavedVaues;

	private static final int RETORQUE_MIN_LIMIT = 10;
	private static final int RETORQUE_MAX_LIMIT = 1000;
	private String torqueSettingsLabel;
	private String mApplyLabel;
	private String mPositionLabel;
	private TextView mNmTxt;
	AlertDialog alertDialog;

	//CR - 550 (some vehicles have different torque settings per axle)
	//Static variable for TORQUE_SETTING Preference
	public static final String TORQUE_SETTING_SAME_FOR_ALL_AXLE = "0";
	public static final String TORQUE_SETTING_DIFFERENT_FOR_ALL_AXLE = "1";
	public static final int Default_Torque_Swap_Type = 0;
	private static boolean UpdateRetorqueArrayForSwap = true;

	//User Trace logs trace tag
	private static final String TRACE_TAG = "Retorque";

	public Retorque(Tyre tire, Context context) {
		selectedTire = Constants.SELECTED_TYRE;
		this.context = context;
		if (Constants.TORQUE_SETTINGS_ARRAY == null) {
			torqueSettings = new ArrayList<>();
		} else {
			torqueSettings = Constants.TORQUE_SETTINGS_ARRAY;
		}
	}

	/**
	 * start re torque process for the tire
	 */
	public void startRetroqueProcess() {
		SharedPreferences preferences = context.getSharedPreferences(
				Constants.GOODYEAR_CONF, 0);
		Constants.TORQUE_ENABLE = (preferences.getString(Constants.TORQUE, ""));
		if(Constants.TORQUE_ENABLE.equalsIgnoreCase("OFF")){
		createWasWheelRemovedDialog();
		}else{
			VehicleSkeletonFragment.resetBoolValue();
			Constants.SECOND_SELECTED_TYRE = null;
			Constants.THIRD_SELECTED_TYRE = null;
			Constants.FOURTH_SELECTED_TYRE = null;
		}
	}
	/**
	 * 
	 */
	private void createWasWheelRemovedDialog() {
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Was Wheel Removed", false, true, false);
		//Setting retorquedone global value
		Retorque.retorqueDone=false;
		//set not to load old values
		Retorque.loadSavedVaues = false;

		currentPosition = 1;
		// get prompts.xml view
		LayoutInflater inflator = LayoutInflater.from(context);
		View promptsView = inflator.inflate(R.layout.dialog_yes_no, null);
		TextView dialogText = (TextView) promptsView
				.findViewById(R.id.dialog_text_message);

		// set user activity for
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		// load message
		loadLabelsFromDBForWheelRemoved(dialogText);

		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);

		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton(mYesLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// update user activity
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Was Wheel Removed", "BT - Yes", false, false, false);
						// recommended settings applied
						if (Constants.SELECTED_TYRE != null
								&& !Constants.SELECTED_TYRE.isRetorqued()) {
							recommendTorqueSettingsAppliedDialog();
						} else {
							updateRetorqueArray(true);

						}
					}
				});
		alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						currentPosition = 0;

						// update user activity
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Was Wheel Removed", "BT - No", false, false, false);

						//Fix : Bug 645 - Doing a TOR create an empty retorque in the mdw
						//updateRetorqueArray(false);
						VehicleSkeletonFragment.resetBoolValue();
						VehicleSkeletonFragment.resetTyres();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	/**
	 * save the state of retorque on orientation change
	 */
	public static void saveStateOfRetorque() {
		// update the re torque to 0 if blank
		String res = retorque_Value.getText().toString();
		res = ("".equals(res)) ? "0" : res;

		// String retorqueToSave =
		// ("".equals(retorque_Value.getText().toString())) ? "0"
		// : retorque_Value.getText().toString();
		retorqueValueSaved = Integer.parseInt(res);
		wrenchNumberSaved = wrenchValue.getText().toString();
		retorqueLabelIssuedSaved = reTorqueLabel.isChecked();
		Constants.RETORQUEVALUE_SAVED = retorqueValueSaved;
		if (Constants.SELECTED_TYRE == null) {
			Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
			Constants.AXLENO = Constants.SELECTED_TYRE.getPosition().substring(
					0, 1);
		} else {
			Constants.AXLENO = Constants.SELECTED_TYRE.getPosition().substring(
					0, 1);
		}
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("Torque", String.valueOf(Constants.RETORQUEVALUE_SAVED));
		hm.put("Axleno", String.valueOf(Constants.AXLENO));
		hm.put("Pressure", String.valueOf(Constants.FORSWAP_BARVALUE));
		VehicleSkeletonFragment.mPressureTorqueAxle_list.add(hm);
	}

	/**
	 * set re torque in edit mode
	 * 
	 * @param wrench
	 *            wrench number
	 * @param torque
	 *            re torque value
	 * @param retorqueLabel
	 *            Retroque Label Issued
	 */
	public static void updateWrenchAndTorqueDetails(String wrench,
			String torque, boolean retorqueLabel) {
		wrenchNumber = wrench;
		retorqueValue = torque;
		retorqueLabelIssued = retorqueLabel;
	}

	/**
	 * 
	 */
	private void recommendTorqueSettingsAppliedDialog() {
		Constants.SwapTypeConstant=0;
		UpdateRetorqueArrayForSwap=true;
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Torque Settings Applied", false, true, false);
		startedNotes = false;
		currentPosition = 2;
		// get prompts.xml view
		LayoutInflater inflator = LayoutInflater.from(context);
		View promptsView = inflator.inflate(R.layout.dialog_yes_no, null);
		TextView dialogText = (TextView) promptsView
				.findViewById(R.id.dialog_text_message);

		// load message
		loadLabelsFromDBForRecommendedSettings(dialogText,selectedTire.getPosition());
		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);

		// set user activity for
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton(mYesLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// set user activity
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Torque Settings Applied", "BT - Yes", false, true, false);
						//CR - 550 (some vehicles have different torque settings per axle)
						//Checking Whether any Torque settings is available or not
						if(isTorqueSettingValueIsEmpty())
						{
								//if Torque setting is not available. Prompting user to Provide Preference for Torque setting
								areTheTorqueSettingsTheSameForEachAxle();
						}
						else
						{
							//if already torque setting exist. based on user preference, We Update or Prompt user to provide torque info
							if(!Job.getTorqueSettingsSame().equalsIgnoreCase(TORQUE_SETTING_SAME_FOR_ALL_AXLE)) {

								//Based on Axle Torque info, We Update or Prompt user to enter Torque info
								if (!IsTorqueSettingIsAppliedForGivenAxle(selectedTire.getPosition())) {
									torqueSettingsDialog(Default_Torque_Swap_Type);
								} else {
									currentPosition = 0;
									updateRetorqueArray(true);
								}
							}
							else
							{
								currentPosition = 0;
								updateRetorqueArray(true);
							}
								// VehicleSkeletonFragment.resetBoolValue();
						}
					}
				});

		alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// set user activity
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Torque Settings Applied", "BT - No", false, true, false);
						startedNotes = true;
						// s
						Intent openNotes = new Intent(context,
								NoteActivity.class);

						openNotes.putExtra("sentfrom", "retorque");
						openNotes.putExtra(Constants.WHEELPOS,
								selectedTire.getPosition());
						// need to change external id##########
						openNotes.putExtra(Constants.JOBID,
								selectedTire.getExternalID());
						openNotes.putExtra(Constants.ISWHEELREMOVED, 0);
						openNotes.putExtra(Constants.SAPCODEWP,
								selectedTire.getSapCodeWP());
						openNotes.putExtra(Constants.SwapType, Default_Torque_Swap_Type);
						currentPosition = 0;
						context.startActivity(openNotes);
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	/**
	 * Method For recommend Torque Settings Applied Dialog for Swap Operation
	 * @param tyre tyre
	 * @param swapNumber Double,triple or $ tyre Swap
	 */
	private void recommendTorqueSettingsAppliedDialogForSwap(Tyre tyre,final int swapNumber) {
		Constants.SwapTypeConstant=swapNumber;
		UpdateRetorqueArrayForSwap=false;

		if(Constants.SwapTypeConstant >0)
		{
			//retorque again started for swapped tyre
			//Orientation change related fix
			VehicleSkeletonFragment.mRetroqueStarted = true;
			retorqueDone = false;
			//set not to load old values
			Retorque.loadSavedVaues = false;
		}
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Torque Settings Applied - Swap", false, true, false);

		//Load Tyre Information based On type of Swap
		if(tyre==null)
		{
			switch (swapNumber) {
				case 2:
					tyre = Constants.SECOND_SELECTED_TYRE;
					break;
				case 3:
					tyre = Constants.THIRD_SELECTED_TYRE;
					break;
				case 4:
					tyre = Constants.FOURTH_SELECTED_TYRE;
					break;
				default:
					tyre = Constants.SELECTED_TYRE;

			}
		}
		startedNotes = false;
		currentPosition = 2;
		// get prompts.xml view
		LayoutInflater inflator = LayoutInflater.from(context);
		View promptsView = inflator.inflate(R.layout.dialog_yes_no, null);
		TextView dialogText = (TextView) promptsView
				.findViewById(R.id.dialog_text_message);

		// load message
		loadLabelsFromDBForRecommendedSettings(dialogText,tyre.getPosition());
		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);

		// set user activity for
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		final Tyre t = tyre;

		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton(mYesLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// set user activity
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Torque Settings Applied", "BT - Yes", false, true, false);
						//CR - 550 (some vehicles have different torque settings per axle)
						//Checking Whether any Torque settings is available or not
						if(isTorqueSettingValueIsEmpty())
						{
							//if Torque setting is not available. Prompting user to Provide Preference for Torque setting
							areTheTorqueSettingsTheSameForEachAxle();
						}
						else
						{
							//if already torque setting exist. based on user preference, We Update or Prompt user to provide torque info
							if(!Job.getTorqueSettingsSame().equalsIgnoreCase(TORQUE_SETTING_SAME_FOR_ALL_AXLE)) {


								//Based on Axle Torque info, We Update or Prompt user to enter Torque info
								if (!IsTorqueSettingIsAppliedForGivenAxle(t.getPosition())) {
									torqueSettingsDialog(swapNumber);

								} else {
									currentPosition = 0;
									SwapOperationDone(swapNumber);
									//updateRetorqueArray(true);
								}
							}
							else
							{
								currentPosition = 0;
								SwapOperationDone(swapNumber);
								//updateRetorqueArray(true);
							}
							// VehicleSkeletonFragment.resetBoolValue();
						}
					}
				});

		alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// set user activity
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Torque Settings Applied", "BT - No", false, true, false);
						startedNotes = true;
						// s
						Intent openNotes = new Intent(context,
								NoteActivity.class);

						openNotes.putExtra("sentfrom", "retorque");
						openNotes.putExtra(Constants.WHEELPOS,
								t.getPosition());
						// need to change external id##########
						openNotes.putExtra(Constants.JOBID,
								t.getExternalID());
						openNotes.putExtra(Constants.ISWHEELREMOVED, 0);
						openNotes.putExtra(Constants.SAPCODEWP,
								t.getSapCodeWP());
						openNotes.putExtra(Constants.SwapType, swapNumber);
						currentPosition = 0;
						context.startActivity(openNotes);
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	/**
	 * Based On Operation, recommend Torque Settings Applied Dialog will be called
	 */
	public void reCreateRecommendTorqueSettingsAppliedDialog() {
		if (startedNotes) {
			//Coming from Notes
			if (NoteActivity.isNotesSaved) {
				if(!Constants.DO_PHYSICAL_SWAP)
				{
					onDone();
				}
				//For Physical Swap (Multiple Pop up for Different Axle Tyres)
				else
				{
					startedNotes = false;

					//Fix for Retorque Notes related Issues
					if(Constants.SwapTypeConstant==0 && Constants.SECOND_SELECTED_TYRE!=null)
					{
						Constants.SwapTypeConstant =Constants.SwapTypeConstant+2;
						recommendTorqueSettingsAppliedDialogForSwap(null,Constants.SwapTypeConstant);
					}
					else if(Constants.SwapTypeConstant==2 && Constants.THIRD_SELECTED_TYRE!=null)
					{
						Constants.SwapTypeConstant =Constants.SwapTypeConstant+1;
						recommendTorqueSettingsAppliedDialogForSwap(null,Constants.SwapTypeConstant);
					}
					else if(Constants.SwapTypeConstant==3 && Constants.FOURTH_SELECTED_TYRE!=null)
					{
						Constants.SwapTypeConstant =Constants.SwapTypeConstant+1;
						recommendTorqueSettingsAppliedDialogForSwap(null,Constants.SwapTypeConstant);
					}
					else
					{
						onDone(Constants.SwapTypeConstant);
					}
				}
			} else {
				if(Constants.SwapTypeConstant==0) {
					recommendTorqueSettingsAppliedDialog();
				}
				else{
					recommendTorqueSettingsAppliedDialogForSwap(null,Constants.SwapTypeConstant);
				}
			}
		}
	}

	private void onDone() {
		selectedTire.setRetorqued(true);
		startedNotes = false;
		VehicleSkeletonFragment.resetBoolValue();
		VehicleSkeletonFragment.resetTyres();
	}

	/**
	 * Final Method Called to reset Booleans
	 * @param sNumber
	 */
	private void onDone(int sNumber)
	{
		//Fix for Retorque notes related issues
		selectedTire.setRetorqued(true);
		switch(sNumber)
		{
			case 4:
				Constants.FOURTH_SELECTED_TYRE.setRetorqued(true);
			case 3:
				Constants.THIRD_SELECTED_TYRE.setRetorqued(true);
			case 2:
				Constants.SECOND_SELECTED_TYRE.setRetorqued(true);
			case 1:
				selectedTire.setRetorqued(true);
			default:
				selectedTire.setRetorqued(true);
		}
		startedNotes = false;
		VehicleSkeletonFragment.resetBoolValue();
		VehicleSkeletonFragment.resetTyres();
	}

	/**
	 * start recommended settings after orientation change
	 */
	public void startRecommendTorqueSettingsAppliedDialog() {
		if(Constants.SwapTypeConstant==0) {
			recommendTorqueSettingsAppliedDialog();
		}
		else
		{
			recommendTorqueSettingsAppliedDialogForSwap(null,Constants.SwapTypeConstant);
		}
	}

	/**
	 * start wheel settings after orientation change
	 */
	public void startWasWheelRemoved() {
		createWasWheelRemovedDialog();
	}

	/**
	 * start torque settings dialog after orientation change
	 */
	public void startTorqueSettingsDialog() {
		if(Constants.SwapTypeConstant==0) {
			torqueSettingsDialog(0);
		}
		else
		{
			torqueSettingsDialog(Constants.SwapTypeConstant);
		}
	}

	/**
	 * start Are The Torque Settings The Same For Each Axle after orientation change
	 */
	public void startAreTheTorqueSettingsTheSameForEachAxle()
	{
		areTheTorqueSettingsTheSameForEachAxle();
	}

	/**
	 * 
	 */
	private void torqueSettingsDialog(final int swapType) {
		Constants.SwapTypeConstant = swapType;

		Tyre tyre=null;
		//Load Tyre Information based On type of Swap
		if(tyre==null)
		{
			switch (swapType) {
				case 2:
					tyre = Constants.SECOND_SELECTED_TYRE;
					break;
				case 3:
					tyre = Constants.THIRD_SELECTED_TYRE;
					break;
				case 4:
					tyre = Constants.FOURTH_SELECTED_TYRE;
					break;
				default:
					tyre = Constants.SELECTED_TYRE;

			}
		}

		String position= tyre.getPosition();

		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Torque Settings", false, true, false);
		currentPosition = 3;
		// get prompts.xml view
		LayoutInflater inflator = LayoutInflater.from(context);
		View promptsView = inflator.inflate(R.layout.retorque_dialog, null);

		// set user activity for
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		retorque_title_label = (TextView) promptsView
				.findViewById(R.id.retorque_title);
		retorque_Value = (EditText) promptsView
				.findViewById(R.id.value_retorque);
		//Position related widget initialization
		positionLabel = (TextView)promptsView.findViewById(R.id.position_label);
		valuePosition = (TextView)promptsView.findViewById(R.id.value_position);
		//Setting position
		valuePosition.setText(position);
		retorque_Value.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				String retorqueValStr = s.toString();
				if (!TextUtils.isEmpty(retorqueValStr)) {
					int retorqueVal = Integer.parseInt(retorqueValStr);
					if (retorqueVal > RETORQUE_MAX_LIMIT) {
						retorque_Value.setText("");
						CommonUtils.notify(retorqueLessMessage, context);
					}
				}
			}
		});
		retorque_Value.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String retorqueValStr = retorque_Value.getText().toString()
							.trim();
					if (!TextUtils.isEmpty(retorqueValStr)) {
						int retorqueVal = Integer.parseInt(retorqueValStr);
						if (retorqueVal < RETORQUE_MIN_LIMIT) {
							retorque_Value.setText("");
							CommonUtils.notify(retorqueLessMessage, context);
						}
					}
				}
			}
		});

		retorque_label = (TextView) promptsView
				.findViewById(R.id.retorque_label);
		mNmTxt = (TextView) promptsView.findViewById(R.id.retorque_unit);

		wrenchValue = (EditText) promptsView.findViewById(R.id.value_wrenchnum);

		reTorqueLabel = (Switch) promptsView
				.findViewById(R.id.issued_label_switch);
		wrenchNumberLabel = (TextView) promptsView
				.findViewById(R.id.wrench_number_label);
		retorque_label_issued = (TextView) promptsView
				.findViewById(R.id.label_issued);

		// load ui labels
		loadLabelsFromDBForRetorqueChange();

		// if loading after orientation change
		if (loadSavedVaues) {
			wrenchValue.setText(wrenchNumberSaved);
			// if saved value is 0 then set value as blank
			String retorqueValue = (retorqueValueSaved == 0) ? "" : String
					.valueOf(retorqueValueSaved);
			retorque_Value.setText(retorqueValue);
			reTorqueLabel.setChecked(retorqueLabelIssuedSaved);
		}

		retorque_title_label.setText(torqueSettingsLabel);

		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(context);

		// set prompts.xml to alert dialog builder
		alertDialogBuilder.setView(promptsView);

		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton(mApplyLabel,
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						// set user activity
						alertDialogBuilder.updateInactivityForDialog();

						String retorqueValueStr = retorque_Value.getText()
								.toString();
						if (!TextUtils.isEmpty(retorqueValueStr)) {
							int retorqueValueParsed = CommonUtils
									.parseInt(retorqueValueStr);
							if (retorqueValueParsed >= RETORQUE_MIN_LIMIT
									&& retorqueValueParsed <= RETORQUE_MAX_LIMIT) {
								retorqueValue = retorque_Value.getText()
										.toString();
								wrenchNumber = wrenchValue.getText().toString();
								if (reTorqueLabel.isChecked())
									retorqueLabelIssued = true;
								else
									retorqueLabelIssued = false;

								if(UpdateRetorqueArrayForSwap) {
									// set re torque set for tire
									selectedTire.setRetorqued(true);
								}
								retorqueDone = true;

								// set re torque finished
								VehicleSkeletonFragment.mRetroqueStarted = false;
								currentPosition = 0;

								try {
									String Trace_Data = retorque_Value.getText().toString() + mNmTxt.getText().toString() + " | "
											+ wrenchValue.getText().toString() + " | "
											+ reTorqueLabel.isChecked();
									LogUtil.TraceInfo(TRACE_TAG, "Torque Settings", "BT - Apply" + " : Data : " + Trace_Data, false, false, false);
								}
								catch(Exception e)
								{
									LogUtil.TraceInfo(TRACE_TAG, "Torque Settings", "BT - Apply" + " : Exception : " +e.getMessage(), false, false, false);
								}

								if(UpdateRetorqueArrayForSwap) {
									updateRetorqueArray(true);
								}


								try {
									// Activity activity = (Activity) context;
									CommonUtils.hideKeyboard(context,
											alertDialog.getWindow()
													.getDecorView()
													.getRootView()
													.getWindowToken());
								} catch (Exception e) {
									LogUtil.i("retorque onacitivityresult",
											"could not close keyboard");
								}

								/*
								 * VehicleSkeletonFragment.resetBoolValue();
								 * VehicleSkeletonFragment.resetTyres();
								 */
							} else {
								LogUtil.TraceInfo(TRACE_TAG, "Torque Settings", "BT - Apply : Msg : "+retorqueLessMessage, false, false, false);
								CommonUtils
										.notify(retorqueLessMessage, context);
								torqueSettingsDialog(swapType);
							}
						} else {
							LogUtil.TraceInfo(TRACE_TAG, "Torque Settings", "BT - Apply : Data : Is not Correct", false, false, false);
							torqueSettingsDialog(swapType);
						}
						//Based On Swap Type will set Value
						//swapType - 0 (No swap Operation) , swapType>0 (Swap Operation)
						if(swapType>0) {
							SwapOperationDone(swapType);
						}
					}


				});
		alertDialogBuilder.setCancelable(false).setNegativeButton(cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// set user activity
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Cancel", false, false, false);
						currentPosition = 0;
						//Based On Swap Type Will Close Dialog or Move to Further Operation
						//swapType - 0 (No swap Operation) , swapType>0 (Swap Operation)
						if(swapType>0)
						{
							//SwapOperationDone(swapType);
							startRecommendTorqueSettingsAppliedDialog();
						}
						else {
							startRetroqueProcess();
						}
					}
				});

		// create alert dialog
		alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	private InputFilter alphaNumericFilter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end,
				Spanned dest, int dstart, int dend) {
			boolean keepOriginal = true;
			StringBuilder sb = new StringBuilder(end - start);
			for (int i = start; i < end; i++) {
				char c = source.charAt(i);
				if (isCharAllowed(c)) // put your condition here
					sb.append(c);
				else
					keepOriginal = false;
			}
			if (keepOriginal)
				return null;
			else {
				if (source instanceof Spanned) {
					SpannableString sp = new SpannableString(sb);
					TextUtils.copySpansFrom((Spanned) source, start,
							sb.length(), null, sp, 0);
					return sp;
				} else {
					return sb;
				}
			}
		}

		private boolean isCharAllowed(char c) {
			return Character.isLetterOrDigit(c) || Character.isSpaceChar(c);
		}
	};

	/**
	 * update re torque array (to be written to DB)
	 */
	private void updateRetorqueArray(boolean wasWheelRemoved) {
		// torque setting to added to array-list
		TorqueSettings torqueSet = new TorqueSettings();
		if (selectedTire == null) {
			selectedTire = Constants.SELECTED_TYRE;
		}
		if (wasWheelRemoved) {

			selectedTire.setRetorqued(true);
			torqueSet.setWheelPos(selectedTire.getPosition());
			if (TextUtils.isEmpty(retorqueValue)) {
				torqueSet.setTorqueSettingsValue(0);
			} else {
				torqueSet.setTorqueSettingsValue(Integer
						.parseInt(retorqueValue));
			}
			torqueSet.setSapCodeWP(selectedTire.getSapCodeWP());
			torqueSet.setWrenchNumber(Retorque.wrenchNumber);
			torqueSet.setJobId(selectedTire.getExternalID());

			//Bug 645 : Doing a TOR create an empty re-torque in the mdw
			//if no reason is there, then the Reason should be empty.
			torqueSet.setNoManufacturerTorqueSettingsReason("");

			// wheel removed
			torqueSet.setIsWheelRemoved(1);

			if (Retorque.retorqueLabelIssued)
				torqueSet.setRetorqueLabelIssued(1);
			else
				torqueSet.setRetorqueLabelIssued(0);
		} else {
			// TODO fix me
			torqueSet.setSapCodeWP(selectedTire.getSapCodeWP());
			torqueSet.setWheelPos(selectedTire.getPosition());
			torqueSet.setWrenchNumber("");
			torqueSet.setNoManufacturerTorqueSettingsReason("");

			// wheel not removed
			torqueSet.setIsWheelRemoved(0);
		}

		updateRetorqueForJobItem(selectedTire.getExternalID(), retorqueValue, Default_Torque_Swap_Type);

		/*
		 * int lastJobAddedPosition = VehicleSkeletonFragment.jobItemList.size()
		 * - 1; if (lastJobAddedPosition != -1) { JobItem jobItem =
		 * VehicleSkeletonFragment.jobItemList .get(lastJobAddedPosition); if
		 * (TextUtils.isEmpty(retorqueValue)){ jobItem.setTorqueSettings("0");
		 * }else{ jobItem.setTorqueSettings(retorqueValue); }
		 * 
		 * VehicleSkeletonFragment.jobItemList.set(lastJobAddedPosition,
		 * jobItem); }
		 */

		torqueSettings.add(torqueSet);
		//Updating Torque setting value to Constants Torque Setting Array
		Constants.TORQUE_SETTINGS_ARRAY = torqueSettings;

		// For Physical Swap
		if (wasWheelRemoved && Constants.DO_PHYSICAL_SWAP)
		{
			//Show Recommend Torque Settings Applied Dialog for Second Tyre (Swap)
			recommendTorqueSettingsAppliedDialogForSwap(Constants.SECOND_SELECTED_TYRE,2);
		}

		DBModel ejobDbInsert = new DBModel(context);
		ejobDbInsert.open();
		VehicleSkeletonFragment.insertJobAfterOperation(ejobDbInsert);
		ejobDbInsert.close();

		//Constants.TORQUE_SETTINGS_ARRAY = torqueSettings;
	}

	/**
	 * Method to pop up multiple recommend Torque Settings Applied Dialog for Retroque along with Swap Operation
	 * @param SwapNumber
	 */
	private void SwapOperationDone(int SwapNumber)
	{
		{
			int lastSwap=0;
			//2 tyre Swap Check
			if(Constants.SECOND_SELECTED_TYRE!=null && SwapNumber==2)
			{
				addRetorqueForMultiSwap(2);
				if(Constants.THIRD_SELECTED_TYRE == null)
				{
					lastSwap=1;
				}
				else
				{
					recommendTorqueSettingsAppliedDialogForSwap(Constants.THIRD_SELECTED_TYRE,3);
				}
			}
			//3 tyre Swap Check
			else if (Constants.THIRD_SELECTED_TYRE != null && SwapNumber==3) {
				addRetorqueForMultiSwap(3);
				if(Constants.FOURTH_SELECTED_TYRE == null)
				{
					lastSwap=1;
				}
				else
				{
					recommendTorqueSettingsAppliedDialogForSwap(Constants.FOURTH_SELECTED_TYRE,4);
				}
			}
			//4 Tyre Swap Check
			else if (Constants.FOURTH_SELECTED_TYRE != null && SwapNumber==4) {
				addRetorqueForMultiSwap(4);
				lastSwap=1;
			}

//			int lastJobAddedPosition2 = VehicleSkeletonFragment.mJobItemList
//					.size() - 2;
//			if (lastJobAddedPosition2 != -2) {
//				JobItem jobItem = VehicleSkeletonFragment.mJobItemList
//						.get(lastJobAddedPosition2);
//				if (TextUtils.isEmpty(retorqueValue))
//					jobItem.setTorqueSettings("0");
//				else
//					jobItem.setTorqueSettings(retorqueValue);
//				VehicleSkeletonFragment.mJobItemList.set(lastJobAddedPosition2,
//						jobItem);
//			}

			if(lastSwap==1) {
				VehicleSkeletonFragment.resetBoolValue();
				VehicleSkeletonFragment.resetTyres();
			}
		}
	}

	/**
	 * add Re-torque for multi swap
	 * 
	 * @param swapType
	 */
	private void addRetorqueForMultiSwap(int swapType) {
		Tyre currentTire = null;
		if(2 == swapType)
		{
			currentTire = Constants.SECOND_SELECTED_TYRE;
			Constants.SECOND_SELECTED_TYRE.setRetorqued(true);
		}
		else if (3 == swapType) {
			currentTire = Constants.THIRD_SELECTED_TYRE;
			Constants.THIRD_SELECTED_TYRE.setRetorqued(true);
		} else if (4 == swapType) {
			currentTire = Constants.FOURTH_SELECTED_TYRE;
			Constants.FOURTH_SELECTED_TYRE.setRetorqued(true);
		}

		//Creating New Object for Retorque
		TorqueSettings torqueSetForSwap = new TorqueSettings();
		if (TextUtils.isEmpty(retorqueValue)) {
			torqueSetForSwap.setTorqueSettingsValue(0);
		} else {
			torqueSetForSwap.setTorqueSettingsValue(Integer
					.parseInt(retorqueValue));
		}
		torqueSetForSwap.setWheelPos(currentTire.getPosition());
		torqueSetForSwap.setSapCodeWP(currentTire.getSapCodeWP());
		torqueSetForSwap.setJobId(currentTire.getExternalID());
		torqueSetForSwap.setWrenchNumber(Retorque.wrenchNumber);
		torqueSetForSwap.setIsWheelRemoved(1);
		if (Retorque.retorqueLabelIssued)
			torqueSetForSwap.setRetorqueLabelIssued(1);
		else
			torqueSetForSwap.setRetorqueLabelIssued(0);

		// Update Re torque for JobItem
		updateRetorqueForJobItem(currentTire.getExternalID(), retorqueValue, swapType);

		torqueSettings.add(torqueSetForSwap);
		//Updating Torque setting value to Constants Torque Setting Array
		Constants.TORQUE_SETTINGS_ARRAY = torqueSettings;
	}

	/**
	 * Method to Update Re torque for JobItem
	 * @param selectedTyreExternalId
	 * @param retorqueValue
	 */
	private void updateRetorqueForJobItem(String selectedTyreExternalId,
			String retorqueValue,int swapType) {
		if (VehicleSkeletonFragment.mJobItemList != null
				&& VehicleSkeletonFragment.mJobItemList.size() > 0) {

			int lastJobAddedPosition;
			JobItem lastJobItem=null;
			String actionTypeLastJobItem="";
			//Loop count variable
			int count=1;

			//Loop will check for Swap Operation related job item
			do {
				//Loop will Avoid Inspection Related Job Item's
				do {
					lastJobAddedPosition = VehicleSkeletonFragment.mJobItemList
							.size() - count;
					if (lastJobAddedPosition != -1) {
						lastJobItem = VehicleSkeletonFragment.mJobItemList
								.get(lastJobAddedPosition);
						actionTypeLastJobItem = lastJobItem.getActionType();
						count++;
					}
				} while (lastJobItem.getActionType().equalsIgnoreCase(TireActionType.INSPECTION));
				swapType--;
			}while(swapType>0);

			if (lastJobAddedPosition != -1) {
				if (TireActionType.ACTIONTYPE_WOT
						.contains(actionTypeLastJobItem)) {
					for (int i = 0; i < VehicleSkeletonFragment.mJobItemList
							.size(); i++) {
						JobItem jobItem = VehicleSkeletonFragment.mJobItemList
								.get(i);
						if (selectedTyreExternalId.equals(jobItem.getTyreID())
								&& TireActionType.ACTIONTYPE_WOT
										.contains(jobItem.getActionType())) {
							if (TextUtils.isEmpty(retorqueValue)) {
								jobItem.setTorqueSettings("0");
							} else {
								jobItem.setTorqueSettings(retorqueValue);
							}
							VehicleSkeletonFragment.mJobItemList
									.set(i, jobItem);
						}
					}
				} else {
					if (TextUtils.isEmpty(retorqueValue)) {
						lastJobItem.setTorqueSettings("0");
					} else {
						lastJobItem.setTorqueSettings(retorqueValue);
					}
					VehicleSkeletonFragment.mJobItemList.set(
							lastJobAddedPosition, lastJobItem);
				}
			}
		}
	}

	private void loadLabelsFromDBForRetorqueChange() {

		DatabaseAdapter label = new DatabaseAdapter(context);
		label.createDatabase();
		label.open();

		/*retorqueLessMessage = "Retorque value should be between "
				+ RETORQUE_MIN_LIMIT + " & " + RETORQUE_MAX_LIMIT;*/
		retorqueLessMessage = label
				.getLabel(Constants.INVALID_RETORQUE__MESSAGE_LABEL);
		retorqueLessMessage = String.format(retorqueLessMessage, RETORQUE_MIN_LIMIT, RETORQUE_MAX_LIMIT);
		String labelFromDB = label
				.getLabel(Constants.RETORQUE_LABEL_ISSUED_LABEL);
		retorque_label_issued.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.NM_RETORQUE_UNIT);
		mNmTxt.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.RETORQUE_VALUE_APPLIED);
		retorque_label.setText(labelFromDB);

		labelFromDB = label.getLabel(Constants.WRENCH_NUMBER_LABEL);
		wrenchNumberLabel.setText(labelFromDB);

		torqueSettingsLabel = label.getLabel(Constants.LABEL_TORQUE_SETTINGS);
		cancel = label.getLabel(Constants.CANCEL_LABEL);
		mYesLabel = label.getLabel(Constants.YES_LABEL);
		mNoLabel = label.getLabel(Constants.NO_LABEL);
		mApplyLabel = label.getLabel(Constants.APPLY_LABEL);
		//Label for Position
		mPositionLabel=label.getLabel(Constants.POSITION_LABEL);
		positionLabel.setText(mPositionLabel);
		label.close();
	}

	private void loadLabelsFromDBForWheelRemoved(TextView dialogTextlabel) {

		DatabaseAdapter label = new DatabaseAdapter(context);
		label.createDatabase();
		label.open();

		String labelFromDB = label
				.getLabel(Constants.WAS_THE_WHEEL_REMOVED_LABEL);
		dialogTextlabel.setText(labelFromDB);

		mYesLabel = label.getLabel(Constants.YES_LABEL);
		mNoLabel = label.getLabel(Constants.NO_LABEL);

		label.close();
	}

	private void loadLabelsFromDBForRecommendedSettings(TextView dialogTextLabel,String position) {

		DatabaseAdapter label = new DatabaseAdapter(context);
		label.createDatabase();
		label.open();

		String labelFromDB = label
				.getLabel(Constants.WAS_THE_RECOMMENDED_SETTINGS_APPLIED_LABEL);
		String labelPosition = label.getLabel(Constants.POSITION_LABEL);
		//Label with Position
		labelFromDB = labelFromDB + "\n" +labelPosition + " : " +position;
		dialogTextLabel.setText(labelFromDB);

		mYesLabel = label.getLabel(Constants.YES_LABEL);
		mNoLabel = label.getLabel(Constants.NO_LABEL);

		label.close();
	}

	/**
	 * CR - 550 (some vehilces have different torque settings per axle)
	 * Method to find whether torque setting is applied to the axle, for the given tyre position.
	 * @param selectedWheelPosition selected tyre position
	 * @return boolean value
	 * True - if torque settings applied on that axle
	 * False - if torque settings not applied on that axle
	 */
	private Boolean IsTorqueSettingIsAppliedForGivenAxle(String selectedWheelPosition) {
		//checking for null
		if(Constants.TORQUE_SETTINGS_ARRAY!=null)
		{
			//Comparing Tyre Axle number with Pre added TorqueSetting Tyre Axle number
			//Based on the match. Torque setting info is updated or added newly
			for (TorqueSettings torque : Constants.TORQUE_SETTINGS_ARRAY) {
				if (torque.getIsWheelRemoved() == 0 ||
						!selectedWheelPosition.substring(0,2).equalsIgnoreCase(torque.getWheelPos().substring(0,2))) {
					continue;
				}
				Tyre tire = VehicleSkeletonFragment.getTireByPosition(torque.getWheelPos());
				if (null != tire) {
					tire.setRetorqued(true);
					if (TextUtils.isEmpty(torque
							.getNoManufacturerTorqueSettingsReason())) {

						boolean retroqueLabel;
						if (torque.getRetorqueLabelIssued() == 1) {
							retroqueLabel = true;
						} else {
							retroqueLabel = false;
						}

						//Updating Torque Setting Variables.
						Retorque.retorqueDone = true;
						Retorque.updateWrenchAndTorqueDetails(
								torque.getWrenchNumber(),
								String.valueOf(torque.getTorqueSettingsValue()),
								retroqueLabel);
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * CR - 550 (some vehilces have different torque settings per axle)
	 * Method to find whether torque setting Value is there or not
	 * @return true or false
	 */
	private Boolean isTorqueSettingValueIsEmpty()
	{
		//checking for null
		if(Constants.TORQUE_SETTINGS_ARRAY!=null && Constants.TORQUE_SETTINGS_ARRAY.size()>0)
		{
			//Fix to resolve torque settings related to 'recommended wheel torque not applied (Reason)'
			int manufacCount=0;
			for (TorqueSettings torque : Constants.TORQUE_SETTINGS_ARRAY)
			{
				if(torque.getNoManufacturerTorqueSettingsReason()!=null && !torque.getNoManufacturerTorqueSettingsReason().trim().equalsIgnoreCase(""))
				{
					manufacCount++;
				}
			}
			//if all torque settings are with 'recommended wheel torque not applied (Reason)' type
			//then, TorqueSettingValue is empty
			if(manufacCount==Constants.TORQUE_SETTINGS_ARRAY.size())
			{
				return true;
			}
			//Checking atleast a wheel is removed (not Dismount) during Operation or not
			//If wheel is removed - return false
			for (TorqueSettings torque : Constants.TORQUE_SETTINGS_ARRAY) {
				if (torque.getIsWheelRemoved() == 0) {
					continue;
				}
				return false;
			}
		}
		return true;
	}

	/**
	 * CR - 550 (some vehilces have different torque settings per axle)
	 * Method to load Dialog Torque Settings The Same For Each Axle
	 */
	private void areTheTorqueSettingsTheSameForEachAxle()
	{
		// Dialog Number (Orientation Change)
		currentPosition = 4;
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Torque Settings The Same", false, true, false);
		LayoutInflater inflator = LayoutInflater.from(context);
		View promptsView = inflator.inflate(R.layout.dialog_yes_no, null);
		TextView dialogText = (TextView) promptsView
				.findViewById(R.id.dialog_text_message);

		// load message
		loadLabelsFromDBForAreTheTorqueSettingsTheSameForEachAxle(dialogText);
		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);

		// set user activity for
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton(mYesLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// set user activity
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Torque Settings The Same", "BT - Yes", false, false, false);
						//Method call to show Torque Setting Dialog
						//Sending swap tyre info
						torqueSettingsDialog(Constants.SwapTypeConstant);
						//Setting User Defined Preference for Torque Setting
						Job.setTorqueSettingsSame(TORQUE_SETTING_SAME_FOR_ALL_AXLE);

					}
				});

		alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// set user activity
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Torque Settings The Same", "BT - No", false, false, false);
						//Method call to show Torque Setting Dialog
						//Sending swap tyre info
						torqueSettingsDialog(Constants.SwapTypeConstant);
						//Setting User Defined Preference for Torque Setting
						Job.setTorqueSettingsSame(TORQUE_SETTING_DIFFERENT_FOR_ALL_AXLE);

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	/**
	 * CR - 550 (some vehilces have different torque settings per axle)
	 * Method to load lables for Torque Settings The Same For Each Axle
	 * @param dialogTextLabel dialog text view
	 */
	private void loadLabelsFromDBForAreTheTorqueSettingsTheSameForEachAxle(TextView dialogTextLabel) {

		DatabaseAdapter label = new DatabaseAdapter(context);
		label.createDatabase();
		label.open();

//		String labelFromDB = label
//				.getLabel();

		String labelFromDB = label.getLabel(Constants.Label_Are_the_torque_setting_same);
		dialogTextLabel.setText(labelFromDB);

		mYesLabel = label.getLabel(Constants.YES_LABEL);
		mNoLabel = label.getLabel(Constants.NO_LABEL);

		label.close();
	}


}
