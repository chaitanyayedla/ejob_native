package com.goodyear.ejob.job.operation.helpers;

import com.goodyear.ejob.util.Constants;

/**
 * @author shailesh.p
 * 
 */
public class JobItem {
	private String jobId;
	private String sapCodeTilD;
	private String ExternalId;
	private String ActionType;
	private String minNSK;
	private String tyreID;
	private String removalReasonId;
	private String damageId;
	private String casingRouteCode;
	private String regrooved;
	private String torqueSettings;
	private String note;
	private String threaddepth;
	private String pressure;
	private String grooveNumber;
	private String reGrooveNumber;
	private String serviceCount;
	private String regrooveType;
	private String nskOneBefore;
	private String nskTwoBefore;
	private String nskThreeBefore;
	private String nskOneAfter;
	private String nskTwoAfter;
	private String nskThreeAfter;
	private String serviceID;
	private String axleNumber;
	private String axleServiceType;
	private String workOrderNumber;
	private String repairCompany;
	private String swapType;
	private String sequence;
	private String recInflactionOrignal;
	private String recInflactionDestination;
	private String operationID;
	private String rimType;
	private String swapOriginalPosition;
	private String serialNumber = "";
	private String brandName = "";
	private String fullDesignDetails = "";
	private boolean isDoubleSwap = false;
	//Added for inspection
	private String VisualDisplaysID = "";
	private String Temperature = "0";

	//Setting Default Value as 99.99 for rect pressure
	private String RectPressure = Constants.DEFAULT_INSPECTION_PRESSURE_VALUE;

	public String getIsPerfect() {
		return IsPerfect;
	}

	public void setIsPerfect(String isPerfect) {
		IsPerfect = isPerfect;
	}

	//To determine whether a job item can be added to a snapshot DB or not
	private String IsPerfect="1";

	//Added to Determine Whether Inspection is Done Along With Operation or Not
	private String InspectedWithOperation = "";
	public String getInspectedWithOperation() {
		return this.InspectedWithOperation;
	}

	public void setInspectedWithOperation(String inspectedWithOperation) {
		this.InspectedWithOperation = inspectedWithOperation;
	}



	public String getVisualDisplayID() {
		return VisualDisplaysID;
	}

	public void setVisualDisplayID(String visualDisplayID) {
		this.VisualDisplaysID = visualDisplayID;
	}

	public String getTemperature() {
		return Temperature;
	}

	public void setTemperature(String temperature) {
		this.Temperature = temperature;
	}

	public String getRectPressure() {
		return RectPressure;
	}

	public void setRectPressure(String rectPressure) {
		RectPressure = rectPressure;
	}

	/**
	 * @return the isDoubleSwap
	 */
	public boolean isDoubleSwap() {
		return isDoubleSwap;
	}

	/**
	 * @param isDoubleSwap the isDoubleSwap to set
	 */
	public void setDoubleSwap(boolean isDoubleSwap) {
		this.isDoubleSwap = isDoubleSwap;
	}

	/**
	 * @return the mBrandName
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param mBrandName the mBrandName to set
	 */
	public void setBrandName(String BrandName) {
		this.brandName = BrandName;
	}

	/**
	 * @return the mFullDesignDetails
	 */
	public String getFullDesignDetails() {
		return fullDesignDetails;
	}

	/**
	 * @param mFullDesignDetails the mFullDesignDetails to set
	 */
	public void setFullDesignDetails(String FullDesignDetails) {
		this.fullDesignDetails = FullDesignDetails;
	}

	/*purpose is to check if the item should be deleted 
	 * from db on SaveJob.java in udpateJobItem()
	 *  -1: no operation, 1: indicates delete operation
	 */
	private int deleteStatus = -1; 
	

	/**
	 * @return the swapOriginalPosition
	 */
	public String getSwapOriginalPosition() {
		return swapOriginalPosition;
	}

	/**
	 * @return the serialNumber
	 */
	public String getSerialNumber() {
		return serialNumber;
	}

	/**
	 * @param serialNumber the serialNumber to set
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	/**
	 * @param swapOriginalPosition the swapOriginalPosition to set
	 */
	public void setSwapOriginalPosition(String swapOriginalPosition) {
		this.swapOriginalPosition = swapOriginalPosition;
	}

	/**
	 * @return the jobId
	 */
	public String getJobId() {
		return jobId;
	}

	/**
	 * @param jobId the jobId to set
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	/**
	 * @return the sapCodeTilD
	 */
	public String getSapCodeTilD() {
		return sapCodeTilD;
	}

	/**
	 * @param sapCodeTilD the sapCodeTilD to set
	 */
	public void setSapCodeTilD(String sapCodeTilD) {
		this.sapCodeTilD = sapCodeTilD;
	}

	/**
	 * @return the externalId
	 */
	public String getExternalId() {
		return ExternalId;
	}

	/**
	 * @param externalId the externalId to set
	 */
	public void setExternalId(String externalId) {
		ExternalId = externalId;
	}

	/**
	 * @return the actionType
	 */
	public String getActionType() {
		return ActionType;
	}

	/**
	 * @param actionType the actionType to set
	 */
	public void setActionType(String actionType) {
		ActionType = actionType;
	}

	/**
	 * @return the minNSK
	 */
	public String getMinNSK() {
		return minNSK;
	}

	/**
	 * @param minNSK the minNSK to set
	 */
	public void setMinNSK(String minNSK) {
		this.minNSK = minNSK;
	}

	/**
	 * @return the tyreID
	 */
	public String getTyreID() {
		return tyreID;
	}

	/**
	 * @param tyreID the tyreID to set
	 */
	public void setTyreID(String tyreID) {
		this.tyreID = tyreID;
	}

	/**
	 * @return the removalReasonId
	 */
	public String getRemovalReasonId() {
		return removalReasonId;
	}

	/**
	 * @param removalReasonId the removalReasonId to set
	 */
	public void setRemovalReasonId(String removalReasonId) {
		this.removalReasonId = removalReasonId;
	}

	/**
	 * @return the damageId
	 */
	public String getDamageId() {
		return damageId;
	}

	/**
	 * @param damageId the damageId to set
	 */
	public void setDamageId(String damageId) {
		this.damageId = damageId;
	}

	/**
	 * @return the casingRouteCode
	 */
	public String getCasingRouteCode() {
		return casingRouteCode;
	}

	/**
	 * @param casingRouteCode the casingRouteCode to set
	 */
	public void setCasingRouteCode(String casingRouteCode) {
		this.casingRouteCode = casingRouteCode;
	}

	/**
	 * @return the regrooved
	 */
	public String getRegrooved() {
		return regrooved;
	}

	/**
	 * @param regrooved the regrooved to set
	 */
	public void setRegrooved(String regrooved) {
		this.regrooved = regrooved;
	}

	/**
	 * @return the torqueSettings
	 */
	public String getTorqueSettings() {
		return torqueSettings;
	}

	/**
	 * @param torqueSettings the torqueSettings to set
	 */
	public void setTorqueSettings(String torqueSettings) {
		this.torqueSettings = torqueSettings;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the threaddepth
	 */
	public String getThreaddepth() {
		return threaddepth;
	}

	/**
	 * @param threaddepth the threaddepth to set
	 */
	public void setThreaddepth(String threaddepth) {
		this.threaddepth = threaddepth;
	}

	/**
	 * @return the pressure
	 */
	public String getPressure() {
		return pressure;
	}

	/**
	 * @param pressure the pressure to set
	 */
	public void setPressure(String pressure) {
		this.pressure = pressure;
	}

	/**
	 * @return the grooveNumber
	 */
	public String getGrooveNumber() {
		return grooveNumber;
	}

	/**
	 * @param grooveNumber the grooveNumber to set
	 */
	public void setGrooveNumber(String grooveNumber) {
		this.grooveNumber = grooveNumber;
	}

	/**
	 * @return the reGrooveNumber
	 */
	public String getReGrooveNumber() {
		return reGrooveNumber;
	}

	/**
	 * @param reGrooveNumber the reGrooveNumber to set
	 */
	public void setReGrooveNumber(String reGrooveNumber) {
		this.reGrooveNumber = reGrooveNumber;
	}

	/**
	 * @return the serviceCount
	 */
	public String getServiceCount() {
		return serviceCount;
	}

	/**
	 * @param serviceCount the serviceCount to set
	 */
	public void setServiceCount(String serviceCount) {
		this.serviceCount = serviceCount;
	}

	/**
	 * @return the regrooveType
	 */
	public String getRegrooveType() {
		return regrooveType;
	}

	/**
	 * @param regrooveType the regrooveType to set
	 */
	public void setRegrooveType(String regrooveType) {
		this.regrooveType = regrooveType;
	}

	/**
	 * @return the nskOneBefore
	 */
	public String getNskOneBefore() {
		return nskOneBefore;
	}

	/**
	 * @param nskOneBefore the nskOneBefore to set
	 */
	public void setNskOneBefore(String nskOneBefore) {
		this.nskOneBefore = nskOneBefore;
	}

	/**
	 * @return the nskTwoBefore
	 */
	public String getNskTwoBefore() {
		return nskTwoBefore;
	}

	/**
	 * @param nskTwoBefore the nskTwoBefore to set
	 */
	public void setNskTwoBefore(String nskTwoBefore) {
		this.nskTwoBefore = nskTwoBefore;
	}

	/**
	 * @return the nskThreeBefore
	 */
	public String getNskThreeBefore() {
		return nskThreeBefore;
	}

	/**
	 * @param nskThreeBefore the nskThreeBefore to set
	 */
	public void setNskThreeBefore(String nskThreeBefore) {
		this.nskThreeBefore = nskThreeBefore;
	}

	/**
	 * @return the nskOneAfter
	 */
	public String getNskOneAfter() {
		return nskOneAfter;
	}

	/**
	 * @param nskOneAfter the nskOneAfter to set
	 */
	public void setNskOneAfter(String nskOneAfter) {
		this.nskOneAfter = nskOneAfter;
	}

	/**
	 * @return the nskTwoAfter
	 */
	public String getNskTwoAfter() {
		return nskTwoAfter;
	}

	/**
	 * @param nskTwoAfter the nskTwoAfter to set
	 */
	public void setNskTwoAfter(String nskTwoAfter) {
		this.nskTwoAfter = nskTwoAfter;
	}

	/**
	 * @return the nskThreeAfter
	 */
	public String getNskThreeAfter() {
		return nskThreeAfter;
	}

	/**
	 * @param nskThreeAfter the nskThreeAfter to set
	 */
	public void setNskThreeAfter(String nskThreeAfter) {
		this.nskThreeAfter = nskThreeAfter;
	}

	/**
	 * @return the serviceID
	 */
	public String getServiceID() {
		return serviceID;
	}

	/**
	 * @param serviceID the serviceID to set
	 */
	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	/**
	 * @return the axleNumber
	 */
	public String getAxleNumber() {
		return axleNumber;
	}

	/**
	 * @param axleNumber the axleNumber to set
	 */
	public void setAxleNumber(String axleNumber) {
		this.axleNumber = axleNumber;
	}

	/**
	 * @return the axleServiceType
	 */
	public String getAxleServiceType() {
		return axleServiceType;
	}

	/**
	 * @param axleServiceType the axleServiceType to set
	 */
	public void setAxleServiceType(String axleServiceType) {
		this.axleServiceType = axleServiceType;
	}

	/**
	 * @return the workOrderNumber
	 */
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}

	/**
	 * @param workOrderNumber the workOrderNumber to set
	 */
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}

	/**
	 * @return the repairCompany
	 */
	public String getRepairCompany() {
		return repairCompany;
	}

	/**
	 * @param repairCompany the repairCompany to set
	 */
	public void setRepairCompany(String repairCompany) {
		this.repairCompany = repairCompany;
	}

	/**
	 * @return the swapType
	 */
	public String getSwapType() {
		return swapType;
	}

	/**
	 * @param swapType the swapType to set
	 */
	public void setSwapType(String swapType) {
		this.swapType = swapType;
	}

	/**
	 * @return the sequence
	 */
	public String getSequence() {
		return sequence;
	}

	/**
	 * @param sequence the sequence to set
	 */
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	/**
	 * @return the recInflactionOrignal
	 */
	public String getRecInflactionOrignal() {
		return recInflactionOrignal;
	}

	/**
	 * @param recInflactionOrignal the recInflactionOrignal to set
	 */
	public void setRecInflactionOrignal(String recInflactionOrignal) {
		this.recInflactionOrignal = recInflactionOrignal;
	}

	/**
	 * @return the recInflactionDestination
	 */
	public String getRecInflactionDestination() {
		return recInflactionDestination;
	}

	/**
	 * @param recInflactionDestination the recInflactionDestination to set
	 */
	public void setRecInflactionDestination(String recInflactionDestination) {
		this.recInflactionDestination = recInflactionDestination;
	}

	/**
	 * @return the operationID
	 */
	public String getOperationID() {
		return operationID;
	}

	/**
	 * @param operationID the operationID to set
	 */
	public void setOperationID(String operationID) {
		this.operationID = operationID;
	}

	/**
	 * @return the rimType
	 */
	public String getRimType() {
		return rimType;
	}

	/**
	 * @param rimType the rimType to set
	 */
	public void setRimType(String rimType) {
		this.rimType = rimType;
	}

	/**
	 * @return the deleteStatus
	 */
	public int getDeleteStatus() {
		return deleteStatus;
	}

	/**
	 * @param deleteStatus the deleteStatus to set
	 */
	public void setDeleteStatus(int deleteStatus) {
		this.deleteStatus = deleteStatus;
	}
	
	@Override
	public String toString() {
		return "\n JobItem : "+"\n Job Id : "+ jobId + "\n Sap Code Ti lD : " + sapCodeTilD +
				"\n External Id : "+ ExternalId+"\n ActionType : "+ActionType + "\n Minimum NSK : " + minNSK+
				"\n Tyre ID : "+ tyreID + "\n Removal Reason Id"+removalReasonId+
				"\n Damage Id : "+damageId+"\n casingRouteCode : "+casingRouteCode+
				"\n Regrooved : "+regrooved+"\n TorqueSettings : "+torqueSettings+
				"\n note : "+note+"\n Thread depth : "+threaddepth+"\n Pressure : "+pressure+
				"\n Groove Number : "+grooveNumber+"\n Re-Groove Number : "+reGrooveNumber+"\n ServiceCount : "+serviceCount+
				"\n Regroove Type : "+regrooveType+"\n NskOneBefore : "+nskOneBefore+"\n NskTwoBefore : "+nskTwoBefore+
				"\n NskThreeBefore : "+nskThreeBefore+"\n NskOneAfter : "+nskOneAfter+"\n NskTwoAfter : "+nskTwoAfter+"\n NskThreeAfter : "+nskThreeAfter+
				"\n Service ID : "+serviceID+"\n Axle Number : "+axleNumber+"\n Axle Service Type : "+axleServiceType+
				"\n Work Order Number : "+workOrderNumber+"\n Repair Company : "+repairCompany+"\n Swap Type : "+swapType+
				"\n Sequence : "+sequence+"\n Rec Inflaction Orignal : "+recInflactionOrignal+
				"\n Rec Inflaction Destination : "+recInflactionDestination+"\n Operation ID : "+operationID+
				"\n RimType : "+rimType+"\n Swap Original Position : "+swapOriginalPosition+
				"\n Serial Number : "+serialNumber+"\n Brand Name : "+brandName+"\n Full Design Details : "+fullDesignDetails+
				"\n Is DoubleSwap : "+isDoubleSwap + "\n VisualDisplaysID : "+ VisualDisplaysID+"\n Temperature : "+Temperature +
				"\n Is RectPressure : "+RectPressure+"\n" ;
	}

}
