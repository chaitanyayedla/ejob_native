package com.goodyear.ejob.job.operation.helpers;

import android.content.Context;
import android.database.SQLException;
import android.text.TextUtils;
import android.util.Log;

import com.goodyear.ejob.authenticator.Authentication;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.DBModel;
import com.goodyear.ejob.dbmodel.JobCorrection;
import com.goodyear.ejob.dbmodel.ReservedTyre;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.Job;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.ReservePWTUtils;
import com.goodyear.ejob.util.Tyre;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

/**
 * @author munna.kumar
 * 
 */
public class SaveJob {
	private static String jobID;
	private static DBModel db;
	private static String Log_TAG="SaveJob";
	private static DatabaseAdapter mDbHelper;



	/**
	 * Method Calling DataBase update
	 */
	public static void callUpdateQueries(DBModel dbToSave, String jobIdCurrent,
			Context context) {
		LogUtil.DBLog(Log_TAG,"DataBase Insert/Update Query","Initialized");
		db = dbToSave;
		jobID = jobIdCurrent;
		LogUtil.DBLog(Log_TAG,"DataBase Insert/Update Query","JobID - "+jobID);
		db.open();
		mDbHelper = new DatabaseAdapter(context);
		udpateJob(context);
		// FIX::Removed UpdateAccount from here as we only need to update the
		// account at insert Time
		// updateAccount();
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Tyre - Initailized");
		udpateTyre();
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Tyre - Updated");
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Job Item - Initailized");
		udpateJobItem();
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Job Item - Updated");
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Job Correction - Initailized");
		udpateJobCorrection();
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Job Correction - Updated");
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Reserved Tyre - Initailized");
		udpateReservedTyre();
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Reserved Tyre - Updated");
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Torque Setting - Initailized");
		udpateTorqueSettings();
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Torque Setting - Updated");
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Tyre image - Initailized");
		udpateTyreImage();
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Tyre image- Updated");
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Vehicle - Initailized");
		updateVehicle();
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Update Vehicle - Updated");
		db.close();
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Move Internal DB To DB File - Initailized");
		moveInternalDBToDBFile(context);
		LogUtil.DBLog(Log_TAG, "DataBase Insert/Update Query", "Move Internal DB To DB File - Updated");
	}

	/**
	 * 
	 */
	private static void udpateTyreImage() {
		LogUtil.DBLog(Log_TAG, "Update Tyre Image", "Initailized");
		if (VehicleSkeletonFragment.mTireImageItemList != null
				&& VehicleSkeletonFragment.mTireImageItemList.size() > 0) {
			insertTyreImage();
			//Fix CR 457 (need to change data source from this arraylist to DB
			VehicleSkeletonFragment.mTireImageItemList.clear();
		}
		if (Constants.JOB_TIREIMAGE_ITEMLIST != null
				&& Constants.JOB_TIREIMAGE_ITEMLIST.size() > 0) {
			//Fix CR 457 (need to change data source from this arraylist to DB
			Constants.JOB_TIREIMAGE_ITEMLIST.clear();
		}
		LogUtil.DBLog(Log_TAG, "Update Tyre Image", "Updated");
	}

	/**
	 * 
	 */
	private static void udpateTorqueSettings() {
		LogUtil.DBLog(Log_TAG, "Update Torque Setting", "Initailized");
		if (Retorque.torqueSettings != null) {
			LogUtil.DBLog(Log_TAG, "Update Torque Setting", "Initialize Retorqu value Iteration");
			for (TorqueSettings torque : Retorque.torqueSettings) {
				// db.deleteTorqueSettings(jobID);
				LogUtil.DBLog(Log_TAG, "Update Torque Setting", "Update Torque or In failure case - Insert torque data");
				if (db.updateTorqueSettingTable(jobID, torque.getWheelPos(),
						torque.getIsWheelRemoved(), torque.getSapCodeWP(),
						torque.getWrenchNumber(),
						torque.getNoManufacturerTorqueSettingsReason(),
						torque.getTorqueSettingsValue(),
						torque.getRetorqueLabelIssued()) == 0) {
					LogUtil.DBLog(Log_TAG, "Update Torque Setting", "Insert Torque Setting - Initailized");
					db.insertTorqueSettingTable(jobID, torque.getWheelPos(),
							torque.getIsWheelRemoved(), torque.getSapCodeWP(),
							torque.getWrenchNumber(),
							torque.getNoManufacturerTorqueSettingsReason(),
							torque.getTorqueSettingsValue(),
							torque.getRetorqueLabelIssued());
					LogUtil.DBLog(Log_TAG, "Update Torque Setting", "Insert Torque Setting - Updated");
				}
			}
		}
		LogUtil.DBLog(Log_TAG, "Update Torque Setting", "Updated");
	}

	/**
	 * 
	 */
	private static void udpateJobItem() {

		LogUtil.DBLog(Log_TAG, "Update Job Item", "Initailized");
		if (VehicleSkeletonFragment.mJobItemList == null) {
			return;
			/*
			 * TODO App was crashing on double tapping on the save button on
			 * Ejob form fragments. This is a temporary fix. Owner of this code
			 * has to make a permanent solution to it.
			 */
		}
		for (Iterator<JobItem> iterator = VehicleSkeletonFragment.mJobItemList
				.iterator(); iterator.hasNext();) {
			JobItem jobItem = iterator.next();
//// TODO: 05-11-2015  
			if (jobItem.getDeleteStatus() == 1) {
				if (jobItem.getExternalId() != null
						&& !jobItem.getExternalId().equals("")) {
					db.deleteSingleJobItem(jobItem.getExternalId());
					iterator.remove();
				}
			} else {
				String posCount = String
						.valueOf(VehicleSkeletonFragment.mJobItemList
								.indexOf(jobItem) + 1);
				jobItem.setSequence(posCount);
				String nskOneAfter = CommonUtils.getParsedNSKValue(jobItem
						.getNskOneAfter());
				String nskTwoAfter = CommonUtils.getParsedNSKValue(jobItem
						.getNskTwoAfter());
				String nskThreeAfter = CommonUtils.getParsedNSKValue(jobItem
						.getNskThreeAfter());
				// For Spare
				if (VehicleSkeletonFragment.mJobItemList.size() > 0
						&& jobItem.getActionType().equalsIgnoreCase("3")) {
					if (jobItem.getSwapOriginalPosition().contains("SP1")) {
						jobItem.setSwapOriginalPosition("SP1");
					} else if (jobItem.getSwapOriginalPosition()
							.contains("SP2")) {
						jobItem.setSwapOriginalPosition("SP2");
					} else if (jobItem.getSwapOriginalPosition().contains("SP")) {
						jobItem.setSwapOriginalPosition("SP");
					}
					//// TODO: 05-11-2015  
				}
				LogUtil.DBLog(Log_TAG, "Update Job Item", "Update Job or In failure case - insert job ");
				//Additional Fields added for CR INSPECTION-447 (VisualDisplayID, Temperature, RectPressure)
				if (db.updateJobItem(jobID, jobItem.getSapCodeTilD(),
						jobItem.getExternalId(), jobItem.getActionType(),
						jobItem.getMinNSK(), jobItem.getTyreID(),
						jobItem.getRemovalReasonId(), jobItem.getDamageId(),
						jobItem.getCasingRouteCode(), jobItem.getRegrooved(),
						jobItem.getTorqueSettings(), jobItem.getNote(),
						jobItem.getThreaddepth(), jobItem.getPressure(),
						jobItem.getGrooveNumber(), jobItem.getReGrooveNumber(),
						jobItem.getServiceCount(), jobItem.getRegrooveType(),
						jobItem.getNskOneBefore(), nskOneAfter,
						jobItem.getServiceID(), jobItem.getAxleNumber(),
						jobItem.getAxleServiceType(),
						jobItem.getWorkOrderNumber(),
						jobItem.getRepairCompany(), jobItem.getNskTwoBefore(),
						nskTwoAfter, jobItem.getNskThreeBefore(),
						nskThreeAfter, jobItem.getSwapType(),
						jobItem.getSequence(),
						jobItem.getRecInflactionOrignal(),
						jobItem.getRecInflactionDestination(),
						jobItem.getSwapOriginalPosition(),
						jobItem.getOperationID(), jobItem.getRimType(),jobItem.getVisualDisplayID(),
						jobItem.getTemperature(),jobItem.getRectPressure(),
						jobItem.getInspectedWithOperation(),jobItem.getIsPerfect()) == 0) {

						LogUtil.DBLog(Log_TAG, "Update Job Item", "Insert Job Item - Initailized");
					//Additional Fields added for CR INSPECTION-447 (VisualDisplayID, Temperature, RectPressure)
					db.insertJobItem(jobID, jobItem.getSapCodeTilD(),
							jobItem.getExternalId(), jobItem.getActionType(),
							jobItem.getMinNSK(), jobItem.getTyreID(),
							jobItem.getRemovalReasonId(),
							jobItem.getDamageId(),
							jobItem.getCasingRouteCode(),
							jobItem.getRegrooved(),
							jobItem.getTorqueSettings(), jobItem.getNote(),
							jobItem.getThreaddepth(), jobItem.getPressure(),
							jobItem.getGrooveNumber(),
							jobItem.getReGrooveNumber(),
							jobItem.getServiceCount(),
							jobItem.getRegrooveType(),
							jobItem.getNskOneBefore(), nskOneAfter,
							jobItem.getServiceID(), jobItem.getAxleNumber(),
							jobItem.getAxleServiceType(),
							jobItem.getWorkOrderNumber(),
							jobItem.getRepairCompany(),
							jobItem.getNskTwoBefore(), nskTwoAfter,
							jobItem.getNskThreeBefore(), nskThreeAfter,
							jobItem.getSwapType(), jobItem.getSequence(),
							jobItem.getRecInflactionOrignal(),
							jobItem.getRecInflactionDestination(),
							jobItem.getSwapOriginalPosition(),
							jobItem.getOperationID(), jobItem.getRimType(),
							jobItem.getVisualDisplayID(),
							jobItem.getTemperature(),
							jobItem.getRectPressure(),jobItem.getInspectedWithOperation(),
							jobItem.getIsPerfect());

							LogUtil.DBLog(Log_TAG, "Update Job Item", "Insert Job Item - Updated");
				}
			}

		}
		LogUtil.DBLog(Log_TAG, "Update Job Item", "Updated");
	}

	/**
	 * deleting the reserved tyre from the table based on job id and re-insert
	 * the selected ones
	 */
	private static void udpateReservedTyre() {
		LogUtil.DBLog(Log_TAG,"Update Reserved Tyre","Initailized");
		LogUtil.DBLog(Log_TAG,"Update Reserved Tyre","Delete Reserved tyre : JobID - " + jobID);
		db.deleteReservedTyres(jobID);
		if (Job.getStatus() != "0") {
			// if job is completed no need to reserve tyre
			LogUtil.DBLog(Log_TAG,"Update Reserved Tyre","Insert Reserved Tyre - Initailized");
			insertReservedTyre();
			LogUtil.DBLog(Log_TAG, "Update Reserved Tyre", "Insert Reserved Tyre - Updated");
		}
		LogUtil.DBLog(Log_TAG,"Update Reserved Tyre","Updated");
	}

	/**
	 * 
	 */
	private static void udpateJobCorrection() {
		LogUtil.DBLog(Log_TAG,"Update Job Correction","Initailized");
		if (VehicleSkeletonFragment.mJobcorrectionList == null) {
			LogUtil.DBLog(Log_TAG,"Update Job Correction","Job List - Null");
			return;
		}
		LogUtil.DBLog(Log_TAG,"Update Job Correction","Looping");
		for (JobCorrection jobcorrection : VehicleSkeletonFragment.mJobcorrectionList) {
			if (db.updateJobCorrectionInsertion(jobID,
					jobcorrection.getmTyreID(), jobcorrection.getmKeyName(),
					jobcorrection.getmOldValue(), jobcorrection.getmNewValue(), jobcorrection.getIsPerfect()) == 0) {
				db.insertJobCorrectionInsertion(jobID,
						jobcorrection.getmTyreID(),
						jobcorrection.getmKeyName(),
						jobcorrection.getmOldValue(),
						jobcorrection.getmNewValue(),
						jobcorrection.getIsPerfect());
			}
		}
		LogUtil.DBLog(Log_TAG,"Update Job Correction","Updated");
	}

	/**
	 * 
	 */
	private static void udpateTyre() {
		LogUtil.DBLog(Log_TAG,"Update Tyre","Initailized");
		if (VehicleSkeletonFragment.tyreInfo == null) {
			return;
			/*
			 * TODO App was crashing on double tapping on the save button on
			 * Ejob form fragments. This is a temporary fix. Owner of this code
			 * has to make a permanent solution to it.
			 */
		}
		for (Tyre tyre : VehicleSkeletonFragment.tyreInfo) {
			String dismounted;
			String spare;
			String position = null;
			String shared;
			if (tyre.isDismounted()) {
				dismounted = "1";
			} else {
				dismounted = "0";
			}

			if (Boolean.parseBoolean(tyre.isSpare())) {
				spare = "1";
				// For Spare
				if (tyre.getPosition().contains("SP1")) {
					position = "SP1";
				} else if (tyre.getPosition().contains("SP2")) {
					position = "SP2";
				} else if (tyre.getPosition().contains("SP")) {
					position = "SP";
				}
			} else {
				if (tyre.getPosition() != null
						&& tyre.getPosition().endsWith("$")) {
					position = tyre.getPosition().substring(0,
							tyre.getPosition().length() - 1);
				} else {
					position = tyre.getPosition();
				}
				spare = "0";
			}
			try {
				if (Integer.parseInt(tyre.getSapVenderCodeID()) == -1) {
					tyre.setSapVenderCodeID("");
				}
				//// TODO: 05-11-2015  
			} catch (Exception e) {
				tyre.setSapVenderCodeID("");
			}

			// FIX :: 506
			if (TextUtils.isEmpty(tyre.getSapVenderCodeID())) {
				tyre.setSapVenderCodeID(null);
			}
			if (TextUtils.isEmpty(tyre.getSapMaterialCodeID())) {
				tyre.setSapMaterialCodeID(null);
			}
			if (TextUtils.isEmpty(tyre.getSapContractNumberID())) {
				tyre.setSapContractNumberID(null);
			}
			if (TextUtils.isEmpty(tyre.getSapContractNumberID())) {
				tyre.setSapContractNumberID(null);
			}

			if (TextUtils.isEmpty(tyre.getSapCodeAXID())) {
				tyre.setSapCodeAXID(null);
			}
			/**
			 * Condition added to fetch missing SAPCODEID from DB
			 * and set it back to the tire worked on
			 */
			if (TextUtils.isEmpty(tyre.getSapCodeTI())) {
				LogUtil.i("SaveJob","SapCodeWith Error");
				tyre.setSapCodeTI(null);
				if(!tyre.isHasMounted())
				{
					if(tyre.getExternalID() != null)
					{
						try {
							mDbHelper.open();
							//get sap code using External ID
							tyre.setSapCodeTI(mDbHelper.getSAPCodeTIIdFromExternalID(tyre.getExternalID()));
							
						} catch (Exception e) {
							LogUtil.i("SaveJob","getSAPCodeTIIdFromExternalID() : "+e.getMessage());
							e.printStackTrace();
							tyre.setSapCodeTI(null);
						}
						finally {
							if(null != mDbHelper) {
								mDbHelper.close();
							}
						}
					}
					
				}
			}
			/**
			 * Condition added to fetch missing SAPCODEWP from DB
			 * and set it back to the tire worked on
			 */
			if (TextUtils.isEmpty(tyre.getSapCodeWP())) {
				tyre.setSapCodeWP(null);
				if(!tyre.isHasMounted())
				{
					if(tyre.getExternalID() != null)
					{
						try {
							mDbHelper.open();
							//get sap code using External ID
							tyre.setSapCodeWP(mDbHelper.getSAPCodeWPExternalID(tyre.getExternalID()));
							
						} catch (Exception e) {
							LogUtil.i("SaveJob","getSAPCodeWPExternalID() : "+e.getMessage());
							e.printStackTrace();
							tyre.setSapCodeWP(null);
						}
						finally {
							if(null != mDbHelper) {
								mDbHelper.close();
							}
						}
					}
				}
			}
			if (TextUtils.isEmpty(tyre.getSapCodeVeh())) {
				tyre.setSapCodeVeh(null);
			}
			if (tyre.getShared() == null) {
				shared = "0";
			} else {
				shared = tyre.getShared();
			}
			LogUtil.DBLog(Log_TAG,"Update Tyre","tyre serial number - " + tyre.getSerialNumber()
			+"\tAction type - " +tyre.getActionType()
			+"\tExternal ID - "+tyre.getExternalID()
			+"\tPosition - "+tyre.getPosition()
			+"\tSapCode Ti - "+tyre.getSapCodeTI()
			+"\tSapCode WP - "+tyre.getSapCodeWP());
			LogUtil.DBLog(Log_TAG,"Update Tyre","Checking for Tyre Info Update if (True)--Update-- else --Insert--");
			if (db.updateTyreTable(tyre.getExternalID(), tyre.getPressure(),
					tyre.getSerialNumber(), dismounted, spare,
					tyre.getSapContractNumberID(), tyre.getSapVenderCodeID(),
					tyre.getSapMaterialCodeID(), position, tyre.getSapCodeTI(),
					tyre.getSapCodeWP(), tyre.getOrignalNSK(),
					tyre.getSapCodeAXID(), tyre.getSapCodeVeh(),
					tyre.getStatus(), tyre.getActive(), tyre.getNsk(),
					tyre.getNsk2(), tyre.getNsk3(), tyre.getVehicleJob(),
					shared, tyre.getFromJobId(),tyre.getIsTyreCorrected(),tyre.getTempSAPVendorCodeID()) == 0) {
				LogUtil.DBLog(Log_TAG,"Update Tyre","Insert Tyre info - Initailized");
				db.insertTyreTable(tyre.getExternalID(), tyre.getPressure(),
						tyre.getSerialNumber(), dismounted, spare,
						tyre.getSapContractNumberID(),
						tyre.getSapVenderCodeID(), tyre.getSapMaterialCodeID(),
						position, tyre.getSapCodeTI(), tyre.getSapCodeWP(),
						tyre.getOrignalNSK(), tyre.getSapCodeAXID(),
						tyre.getSapCodeVeh(), tyre.getStatus(),
						tyre.getActive(), tyre.getNsk(), tyre.getNsk2(),
						tyre.getNsk3(), tyre.getVehicleJob(), shared,
						tyre.getFromJobId(), tyre.getIsTyreCorrected(), tyre.getTempSAPVendorCodeID());
				LogUtil.DBLog(Log_TAG, "Update Tyre", "Insert Tyre info - Updated");
			}
		}
		LogUtil.DBLog(Log_TAG,"Update Tyre","Updated");
	}

	/**
	 * Method updating the Job details in the local database file
	 */
	private static void udpateJob(Context context) {
		LogUtil.DBLog(Log_TAG,"Update Job","Initailized");
		String jobType;

		if (Constants.JOBTYPEREGULAR) {
			jobType = "0";
		} else {
			jobType = "1";
		}
		Log.i("SaveJob ",
				"Updating vehicle id is: "
						+ Constants.contract.getVehicleSapCode());

		TimeZone tz = TimeZone.getDefault();
		Date now = new Date();
		int offsetFromUtc = tz.getOffset(now.getTime()) / 1000 / 60;

		/*
		 * If a user save job other than signature form, the signature must be
		 * cleared. So here we're checking if the job is incomplete (i.e, 1)
		 * then clear the signature
		 */
		if (Job.getStatus().equals("1")) {
			Job.setSignatureImage(null);
		}
		// Bug 484
		if(TextUtils.isEmpty(Constants.VENDER_NAME)){
			DatabaseAdapter db = new DatabaseAdapter(context);
			try {
				db.createDatabase();
				db.open();
			Constants.VENDER_NAME = db.getVendorNameFromVendor(Constants.VENDORSAPCODE);
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
		LogUtil.DBLog(Log_TAG,"Update Job","update job Update if (True)--Update-- else --Insert-- ");
		if (DBModel.updateJobTable(jobID, Constants.contract.getLicenseNum(),
				Constants.contract.getContractId(),
				Constants.contract.getVehicleSapCode(),
				Authentication.getUserName(Constants.USER), jobType, "0",
				Job.getGpsLocation(), Job.getLocation(), Job.getTimeFinished(),
				Job.getMissingOdometerReasonID(), Job.getAgreeStatus(),
				Job.getVehicleUnattendedStatus(), Job.getTimeOnSite(),
				Job.getTimeStarted(), Job.getTimeReceived(),
				Job.getCallerPhone(), Job.getCallerName(),
				Constants.VENDORCOUNTRY_ID,
				Job.getThirtyMinutesRetorqueStatus(),
				Job.getDefectAuthNumber(), Job.getDriverName(),
				Job.getActionLineN(), Job.getHudometer(),
				Constants.VENDER_NAME, Constants.VENDORSAPCODE,
				Job.getSignatureReasonID(), Job.getStockLocation(),
				Job.getVehicleODO(), Job.getSignatureImage(),
				/**
				 * CR:: 469 Inserting variable holding full details related to
				 * the application installed and the device details
				 */
				Constants.FULL_DEVICE_DETAILS, Job.getStatus(),
				String.valueOf(Constants.JOBREFNUMBERVALUE),
				Job.getJobValidityDate(), offsetFromUtc, null,

				//CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
				//CR - 550 (some vehicles have different torque settings per axle)
				//added support for Last recorded odometer value and Torque Settings per axle info

				Constants.contract.getVehicleODO(),Job.getTorqueSettingsSame()) == 0) {
			LogUtil.DBLog(Log_TAG, "Update Job", "Before Insert job");
			insertJob(context);
			LogUtil.DBLog(Log_TAG, "Update Job", "After Insert job");
			// FIX:: Adding the jobReference No here.
			updateAccount();
			LogUtil.DBLog(Log_TAG, "Update Job", "Updated");
		}
	}

	/**
	 * 
	 */
	private static void updateVehicle() {
		LogUtil.DBLog(Log_TAG,"Update Vehicle","Initailized");
		LogUtil.DBLog(Log_TAG,"Update Vehicle","update Vehicle Update if (True)--Update-- else --Insert-- ");
		if (db.updateVehicleTable(Constants.contract.getVehicleSapCode(),
				Constants.contract.getIsDIffSizeAllowedForVehicle(),
				Constants.contract.getLicenseNum(),
				Constants.contract.getVehichleConfiguration(),
				Constants.contract.getVin(),
				Constants.contract.getChassisNum(), null) == 0) {
			LogUtil.DBLog(Log_TAG,"Update Vehicle","Insert Vehicle - Initailized");
			insertVehicle();
			LogUtil.DBLog(Log_TAG, "Update Vehicle", "Insert Vehicle - Updated");
		}
		LogUtil.DBLog(Log_TAG,"Update Vehicle","Updated");
	}

	/**
	 * 
	 */
	private static void insertVehicle() {
		LogUtil.DBLog(Log_TAG,"Insert Vehicle","Initailized");
		db.insertVehicleTable(Constants.contract.getVehicleSapCode(),
				Constants.contract.getIsDIffSizeAllowedForVehicle(),
				Constants.contract.getLicenseNum(),
				Constants.contract.getVehichleConfiguration(),
				Constants.contract.getVin(),
				Constants.contract.getChassisNum(), null);
		LogUtil.DBLog(Log_TAG, "Insert Vehicle", "Updated");
	}

	/**
	 * 
	 */
	private static void updateAccount() {
		LogUtil.DBLog(Log_TAG, "Update Account", "Initailized");
		LogUtil.DBLog(Log_TAG, "Update Account", "JobReference Number - " + Constants.JOBREFNUMBER);
		LogUtil.i("JobReference Number", Constants.JOBREFNUMBER + "");
		db.updateAccountTable((String) Constants.JOBREFNUMBER);
		LogUtil.DBLog(Log_TAG, "Update Account", "Updated");
	}

	/**
	 * 
	 */
	private static void insertTyreImage() {
		LogUtil.DBLog(Log_TAG, "Insert Tyre Image", "Initailized");
		LogUtil.DBLog(Log_TAG, "Insert Tyre Image", "Looping");
		for (TireImageItem tyreImage : VehicleSkeletonFragment.mTireImageItemList) {
			db.insertTyreImageTable(tyreImage);
		}
		LogUtil.DBLog(Log_TAG, "Insert Tyre Image", "Updated");
	}

	private static void insertReservedTyre() {
		LogUtil.DBLog(Log_TAG, "Insert Reserved tyre", "Initailized");
		if (ReservePWTUtils.mReserveTyreList != null) {
			LogUtil.DBLog(Log_TAG, "Insert Reserved tyre", "List Contains Value");
			LogUtil.DBLog(Log_TAG, "Insert Reserved tyre", "Looping");
			for (ReservedTyre reserve : ReservePWTUtils.mReserveTyreList) {
				db.insertReservedTyreInsertion(jobID, reserve.getTyreID());
			}
		}
		LogUtil.DBLog(Log_TAG, "Insert Reserved tyre", "Updated");
	}

	/**
	 * Method inserting the Job details in the local database file
	 */
	private static void insertJob(Context context) {
		LogUtil.DBLog(Log_TAG, "Insert Job", "Initailized");
		String jobType;
		if (Constants.JOBTYPEREGULAR) {
			jobType = "0";
		} else {
			jobType = "1";
		}
		// Bug 484
		if(TextUtils.isEmpty(Constants.VENDER_NAME)){
			DatabaseAdapter db = new DatabaseAdapter(context);
			try {
				db.createDatabase();
				db.open();
			Constants.VENDER_NAME = db.getVendorNameFromVendor(Constants.VENDORSAPCODE);
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
		TimeZone tz = TimeZone.getDefault();
		Date now = new Date();
		LogUtil.DBLog(Log_TAG, "Insert Job", "TimeZone - " + tz);
		int offsetFromUtc = tz.getOffset(now.getTime()) / 1000 / 60;
		DBModel.insertJobTable(
				jobID,
				Constants.contract.getLicenseNum(),
				Constants.contract.getContractId(),
				Constants.contract.getVehicleSapCode(),
				Authentication.getUserName(Constants.USER),
				jobType,
				"0",
				Job.getGpsLocation(),
				Job.getLocation(),
				Job.getTimeFinished(),//
				Job.getMissingOdometerReasonID(),
				Job.getAgreeStatus(),
				Job.getVehicleUnattendedStatus(),
				Job.getTimeOnSite(),
				Job.getTimeStarted(),
				Job.getTimeReceived(),//
				Job.getCallerPhone(), Job.getCallerName(),
				Constants.VENDORCOUNTRY_ID,
				Job.getThirtyMinutesRetorqueStatus(),
				Job.getDefectAuthNumber(), Job.getDriverName(),
				Job.getActionLineN(), Job.getHudometer(),
				Constants.VENDER_NAME, Constants.VENDORSAPCODE,
				Job.getSignatureReasonID(), Job.getStockLocation(),
				/**
				 * CR:: 469 Inserting variable holding full details related to
				 * the application installed and the device details
				 */
				Job.getVehicleODO(), Job.getSignatureImage(),
				Constants.FULL_DEVICE_DETAILS, Job.getStatus(),
				String.valueOf(Constants.JOBREFNUMBERVALUE),
				Job.getJobValidityDate(), offsetFromUtc, null,

				//CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
				//CR - 550 (some vehicles have different torque settings per axle)
				//added support for Last recorded odometer value and Torque Settings per axle info

				Constants.contract.getVehicleODO(),Job.getTorqueSettingsSame());
		LogUtil.e("SaveJob", "Constants.JOBREFNUMBERVALUE ::"
				+ Constants.JOBREFNUMBERVALUE);
		LogUtil.DBLog(Log_TAG, "Insert Job", "Updated");
	}

	private static void moveInternalDBToDBFile(Context context) {
		LogUtil.DBLog(Log_TAG, "Move Internal DB To DB File", "Initailized");
		DatabaseAdapter db = new DatabaseAdapter(context);
		try {
			LogUtil.DBLog(Log_TAG, "Move Internal DB To DB File", "Transfer Process - Initailized");
			db.createDatabase();
			db.open();

			db.copyFinalDB();
		} catch (SQLException | IOException e) {
			LogUtil.DBLog(Log_TAG, "Move Internal DB To DB File", "Exception");
			e.printStackTrace();
		} finally {
			if (db != null) {
				LogUtil.DBLog(Log_TAG, "Move Internal DB To DB File", "Final DB Close");
				db.close();
			}
		}
		LogUtil.DBLog(Log_TAG, "Move Internal DB To DB File", "Done");
	}
}
