package com.goodyear.ejob.job.operation.helpers;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.text.TextUtils;
import android.util.Log;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.EjobList;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.JobCorrection;
import com.goodyear.ejob.fragment.EjobTimeLocationFragment;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.Contract;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.Job;
import com.goodyear.ejob.util.JobBundle;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.ReservePWTUtils;
import com.goodyear.ejob.util.ServicesEditHelper;
import com.goodyear.ejob.util.TireActionType;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.TyreState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * This class is used to start and unfinished saved job. When user selects a job
 * from {@link EjobList} screen this class is used to load all the data from
 * database for the selected job.
 */
public class EditJob {
	private Context mContext;
	private String mJobId;
	private Cursor mJobItemCur;
	private DatabaseAdapter mDBHelper;
	private Cursor mTorqueSettingsCur;
	private Cursor mJobCorrectionCur;
	private Cursor mTyreImageCur;
	private ArrayList<String> mGetTyreIdFromJobItem;
	private String mActionTypeStr;
	private int mPosToAdd;
	public static String sJobItemToDelete = "";
	private List<String> mActionType = Arrays.asList("25", "9", "10", "12",
			"13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23");
	public static String KEY_COMES_FROM_EDIT = "comes_from_edit";

	/**
	 * constructor for edit job
	 * 
	 * @param context
	 */
	public EditJob(Context context) {
		this.mContext = context;
	}

	/**
	 * Get job item cursor (job operations) using the Job ID of the selected
	 * Job.
	 */
	public void getJobsFromDBCursors() {
		mJobItemCur = mDBHelper.getAllValuesFromJobItemTable(mJobId);
	}

	/**
	 * update job item object from db for current selected job
	 */
	public void loadJobItemFromDB() {
		if (mJobItemCur == null) {
			return;
		}

		for (boolean hasItem = mJobItemCur.moveToFirst(); hasItem; hasItem = mJobItemCur
				.moveToNext()) {
			try {
				JobItem jobItem = new JobItem();

				jobItem.setJobId(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("ID")));

				jobItem.setRimType(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("RimType")));

				jobItem.setSapCodeTilD(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("SAPCodeTiID")));

				jobItem.setExternalId(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("ExternalID")));

				jobItem.setActionType(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("ActionType")));

				jobItem.setMinNSK(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("MinNSK")));

				jobItem.setTyreID(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("TyreID")));

				jobItem.setRemovalReasonId(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("RemovalReasonID")));

				jobItem.setDamageId(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("DamageID")));

				jobItem.setCasingRouteCode(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("CasingRouteCode")));

				jobItem.setRegrooved(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("Regrooved")));

				jobItem.setTorqueSettings(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("TorqueSetting")));

				jobItem.setNote(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("Note")));

				jobItem.setThreaddepth(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("TreadTepth")));

				jobItem.setPressure(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("Pressure")));

				jobItem.setGrooveNumber(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("GrooveNumber")));

				jobItem.setReGrooveNumber(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("RegrooveNumber")));

				jobItem.setServiceCount(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("ServiceCount")));

				jobItem.setRegrooveType(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("RegrooveType")));

				jobItem.setNskOneBefore(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("NSK1Before")));

				jobItem.setNskOneAfter(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("NSK1After")));

				jobItem.setServiceID(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("ServiceID")));

				jobItem.setAxleNumber(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("AxleNumber")));

				jobItem.setAxleServiceType(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("AxleServiceType")));

				jobItem.setWorkOrderNumber(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("WorkOrderNumber")));

				jobItem.setRepairCompany(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("RepairCompany")));

				jobItem.setNskTwoBefore(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("NSK2Before")));

				jobItem.setNskTwoAfter(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("NSK2After")));

				jobItem.setNskThreeBefore(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("NSK3Before")));

				jobItem.setNskThreeAfter(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("NSK3After")));

				jobItem.setSwapType(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("SwapType")));

				jobItem.setSequence(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("Sequence")));

				jobItem.setRecInflactionOrignal(mJobItemCur
						.getString(mJobItemCur
								.getColumnIndex("RecInflactionOriginal")));

				jobItem.setRecInflactionDestination(mJobItemCur
						.getString(mJobItemCur
								.getColumnIndex("RecInflactionDestination")));

				jobItem.setSwapOriginalPosition(mJobItemCur
						.getString(mJobItemCur
								.getColumnIndex("SWAPOriginalPosition")));

				jobItem.setOperationID(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("OperationID")));

				//CR-447 (Inspection) added support for 3 new columns in JobItem Table
				jobItem.setRectPressure(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("RectPressure")));

				jobItem.setTemperature(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("Temperature")));

				jobItem.setVisualDisplayID(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("VisualCommentsID")));
				//To Determine Whether Inspection is Done Along With Operation or Not
				jobItem.setInspectedWithOperation(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("InspectionWithOperation")));
				//To determine whether a job item can be added to a snapshot DB or not
				jobItem.setIsPerfect(mJobItemCur.getString(mJobItemCur
						.getColumnIndex("IsPerfect")));

				Constants.JOB_ITEMLIST.add(jobItem);

				loadTyreImage(jobItem.getJobId());
				updateTyreImage(jobItem.getJobId());

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public int checkIfPositionIsAdded(String tyrePosition, String tyreId) {
		int tierPos = -1;
		if (Constants.TIRE_INFO != null && Constants.TIRE_INFO.size() > 0) {
			for (int i = 0; i < Constants.TIRE_INFO.size(); i++) {
				String fromTyreInfo = Constants.TIRE_INFO.get(i).getPosition();
				if (tyrePosition.equalsIgnoreCase(fromTyreInfo)) {
					tierPos = i;
					return tierPos;
				}
			}
		}
		return tierPos;
	}

	/**
	 * Check is WOT or PWT before the dismount
	 * 
	 * @param tire
	 * @return
	 */
	private boolean isPWTorWOT(String tyrePosition) {
		if (Constants.TIRE_INFO != null && Constants.TIRE_INFO.size() > 0) {
			for (int i = 0; i < Constants.TIRE_INFO.size(); i++) {
				String posTier = Constants.TIRE_INFO.get(i).getPosition();

				if (tyrePosition.equalsIgnoreCase(posTier)) {
					String actionType = Constants.TIRE_INFO.get(i)
							.getActionType();
					if (!TextUtils.isEmpty(actionType)
							&& mActionType.contains(actionType)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * @param actionType2
	 * @param tire
	 */
	private boolean checkAddedPositionIsFromPreviousJob(String tyreId,
			String tyrePosition) {
		if (Constants.TIRE_INFO != null && Constants.TIRE_INFO.size() > 0) {
			for (int i = 0; i < Constants.TIRE_INFO.size(); i++) {
				String TierInfotierPosition = Constants.TIRE_INFO.get(i)
						.getPosition();
				if (tyrePosition.equalsIgnoreCase(TierInfotierPosition)) {
					String infoTierId = Constants.TIRE_INFO.get(i)
							.getExternalID();
					if (!tyreId.equalsIgnoreCase(infoTierId)
							&& !mGetTyreIdFromJobItem.contains(tyreId)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * API to check to SET/ADD the tier in swap operation.
	 * 
	 * @param tyreId
	 * @param tyrePosition
	 * @return
	 */
	private boolean checkSwapedTierPosition(String tyreId, String tyrePosition) {
		if (Constants.TIRE_INFO != null && Constants.TIRE_INFO.size() > 0) {
			for (int i = 0; i < Constants.TIRE_INFO.size(); i++) {
				String TierInfotierPosition = Constants.TIRE_INFO.get(i)
						.getPosition();
				if (tyrePosition.equalsIgnoreCase(TierInfotierPosition)) {
					String infoTierId = Constants.TIRE_INFO.get(i)
							.getExternalID();
					if (!tyreId.equalsIgnoreCase(infoTierId)
							&& mGetTyreIdFromJobItem.contains(tyreId)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * load job and tire info
	 */
	public void loadJobInfoFromDB() {
		mDBHelper = new DatabaseAdapter(mContext);
		mDBHelper.open();
		Constants.JOB_ITEMLIST = new ArrayList<>();
		Constants.servicesEdit = new ArrayList<>();
		Constants.TIRE_INFO = new ArrayList<>();
		Constants.JOB_TIREIMAGE_ITEMLIST = new ArrayList<>();
		Constants.JOB_CORRECTION_LIST = new ArrayList<>();
		Constants.TORQUE_SETTINGS_ARRAY = new ArrayList<>();
		Constants.contract = new Contract();
		// get Job values
		Cursor jobCursor = null;
		Cursor vehicleCur = null;
		try {
			// get selected job
			jobCursor = mDBHelper.getAllValuesFromJobTable();
			// get selected vehicle for job
			vehicleCur = mDBHelper.getAllValuesFromVehicleTable();
			// check if vehicle is found
			if (vehicleCur != null && vehicleCur.moveToFirst()) {
				// check
				Constants.vehicleConfiguration = vehicleCur
						.getString(vehicleCur
								.getColumnIndex("VehicleConfiguration"));
				Constants.contract
						.setIsDIffSizeAllowedForVehicle(vehicleCur.getString(vehicleCur
								.getColumnIndex("IsDiffSizeAllowedForVehicle")));

				Constants.DIFFERENT_SIZE_ALLOWED = Boolean
						.parseBoolean(vehicleCur.getString(vehicleCur
								.getColumnIndex("IsDiffSizeAllowedForVehicle")));

				Constants.contract.setLicenseNum(vehicleCur
						.getString(vehicleCur.getColumnIndex("LicenseNum")));
				Constants.contract.setVehichleConfiguration(vehicleCur
						.getString(vehicleCur
								.getColumnIndex("VehicleConfiguration")));
				Constants.contract.setVin(vehicleCur.getString(vehicleCur
						.getColumnIndex("Vin")));
				Constants.contract.setChassisNum(vehicleCur
						.getString(vehicleCur.getColumnIndex("ChassisNum")));
				// get sap code for vehicle
				String sapCode = vehicleCur.getString(vehicleCur
						.getColumnIndex("SAPCodeVeh"));
				Constants.contract
						.setIsDIffSizeAllowedForVehicle(vehicleCur.getString(vehicleCur
								.getColumnIndex("IsDiffSizeAllowedForVehicle")));
				// get all the tires for the vehicle
				Cursor tireInfoFromDB = mDBHelper
						.getAllValuesFromTyreTable(sapCode);
				jobCursor.moveToFirst();
				String jobId = jobCursor.getString(jobCursor
						.getColumnIndex("ExternalID"));
				// Constants.jobIdEdit = jobId;
				mGetTyreIdFromJobItem = mDBHelper.getTyreIdFromJobItem(jobId);

				boolean toContinue = true;
				boolean directAdd = false;
				mActionTypeStr = "";
				if (tireInfoFromDB != null) {
					// for all the tires found add to tire info object
					for (boolean hasItem = tireInfoFromDB.moveToFirst(); hasItem; hasItem = tireInfoFromDB
							.moveToNext()) {
						toContinue = false;
						String materialCode = tireInfoFromDB
								.getString(tireInfoFromDB
										.getColumnIndex("SAPMaterialCodeID"));
						// get current tire's model info from DB
						// based on material code
						Cursor tireModelInfo = mDBHelper
								.getTireModelInfo(materialCode);
						Tyre tire = new Tyre();
						mPosToAdd = -1;
						String tyrePosition = tireInfoFromDB
								.getString(tireInfoFromDB
										.getColumnIndex("Position"));
						String tyreId = tireInfoFromDB.getString(tireInfoFromDB
								.getColumnIndex("ExternalID"));
						mActionTypeStr = mDBHelper
								.getTyreIdActionTypeFromJobItem(tyreId, jobId);
						tire.setActionType(mActionTypeStr);

						//Inspection Status Check for a Tyre and Setting value
						tire.setIsInspected(mDBHelper.getInspectionStatusFromJobItem(tyreId, jobId));
						int positionAdded = checkIfPositionIsAdded(
								tyrePosition, tyreId);
						LogUtil.d("EditJob", "<< isPositionAdded >>>>>>>>>>>"
								+ positionAdded + "tyrePosition: "
								+ tyrePosition);
						if (positionAdded == -1) {
							// break;
							boolean isPreviousTyreAtJobItem = mDBHelper
									.isTyrePresentInJobItem(tyreId);
							if (isPreviousTyreAtJobItem
									&& !mGetTyreIdFromJobItem.contains(tyreId)) {
								toContinue = false;
							} else {
								toContinue = true;
							}
						} else {

							if (!mGetTyreIdFromJobItem.contains(tyreId)) {
								boolean isTyreAtJobItem = mDBHelper
										.isTyrePresentInJobItem(tyreId);
								LogUtil.i("EditJob", "isTyreAtJobItem >>>>> "
										+ isTyreAtJobItem);
								if (!isTyreAtJobItem) {
									int posInTierInfo = checkIfPositionIsAdded(
											tyrePosition, tyreId);
									LogUtil.i("EditJob",
											"Pervious Tier Position: "
													+ posInTierInfo);
									if (posInTierInfo >= 0) {
										String tierIdPerviousAdded = Constants.TIRE_INFO
												.get(posInTierInfo)
												.getExternalID();
										LogUtil.i("EditJob",
												"Pervious Tier Id ::: "
														+ tierIdPerviousAdded);
										boolean isPreviousTyreAtJobItem = mDBHelper
												.isTyrePresentInJobItem(tierIdPerviousAdded);
										LogUtil.i(
												"EditJob",
												"isPreviousTyreAtJobItem::"
														+ isPreviousTyreAtJobItem);
										String isUnmounted = Constants.TIRE_INFO
												.get(posInTierInfo)
												.getmIsUnmounted();
										if ((!mGetTyreIdFromJobItem
												.contains(tierIdPerviousAdded) && isPreviousTyreAtJobItem)
												|| isUnmounted.equals("1")) {
											LogUtil.i(
													"EditJob",
													"Now Replacing the Tier>:: "
															+ isPreviousTyreAtJobItem);
											// actionType = db
											// .getTyreIdActionTypeFromJobItem(
											// tyreId, jobId);
											mPosToAdd = positionAdded;
											toContinue = true;
										}
									} else {
										toContinue = false;
									}
								}
							} else {
								// actionType =
								// db.getTyreIdActionTypeFromJobItem(tyreId,
								// jobId);
								directAdd = true;
								mPosToAdd = positionAdded;
								toContinue = true;
							}
						}
						LogUtil.d("EditJob", "toContinue::::: " + toContinue);
						if (toContinue) {

							String isSpare;
							if (tireInfoFromDB.getString(
									tireInfoFromDB.getColumnIndex("IsSpare"))
									.equals("1")) {
								isSpare = "true";
							} else {
								isSpare = "false";
							}
							tire.setSerialNumber(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("SerialNumber")));
							tire.setIsSpare(isSpare);
							tire.setSapCodeTI(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("SAPCodeTi")));
							tire.setSapMaterialCodeID(materialCode);
							tire.setSapContractNumberID(tireInfoFromDB.getString(tireInfoFromDB
									.getColumnIndex("SAPContractNumberID")));
							tire.setPosition(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("Position")));
							tire.setSapCodeWP(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("SAPCodeWp")));
							tire.setShared(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("Shared")));
							tire.setmIsUnmounted(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("IsUnmounted")));
							tire.setNsk(tireInfoFromDB.getString(tireInfoFromDB
									.getColumnIndex("NSK1")));
							tire.setNsk2(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("NSK2")));
							tire.setNsk3(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("NSK3")));
							tire.setOrignalNSK(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("OriginalNSK")));
							tire.setStatus(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("Status")));
							tire.setExternalID(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("ExternalID")));
							tire.setPressure(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("Pressure")));

							/*To retain VendorSAPCODEID information from DB for Tyre
							Bug 738_3 : Dismounted tyre is available in Reserve PWT List before job sync
							*/
							tire.setSapVenderCodeID(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("SAPVendorCodeID")));

							/*To retain FromJobId information for Dismount tyre
							Bug 738_3 : Dismounted tyre is available in Reserve PWT List before job sync
							*/
							tire.setFromJobId(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("FromJobID")));

							/**
                             * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
							 * Maintain temporary SAPVENDERCodeID until SAP updates
							 * Since SAPVENDERCodeID for few tyres are null, so by maintaining temporary SAPVENDERCodeID, will make PWT tyre with SAPVENDERCodeID as null, available for Reserve PWT.
							 * To retain TempSAPVendorCodeID Info
							 */
							tire.setTempSAPVendorCodeID(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("TempSAPVendorCodeID")));

							/**
                             * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
							 * If material and/or SN is changed when a tyre is removed - this tyre should not be made available immediately
							 * To retain material and/or SN change info
							 */
							tire.setIsTyreCorrected(tireInfoFromDB
									.getString(tireInfoFromDB
											.getColumnIndex("IsTyreCorrected")));



							tire.setVehicleJob(sapCode);
							tire.setTyreState(TyreState.PART_WORN);
							// For non maintained
							if (tire.getSerialNumber().equalsIgnoreCase("")) {
								tire.setTyreState(TyreState.NON_MAINTAINED);
							}
							if ("1".equalsIgnoreCase(tire.getmIsUnmounted())) {
								tire.setTyreState(TyreState.EMPTY);
							}
							try {
								if (tireModelInfo != null
										&& tireModelInfo.moveToFirst()) {
									tire.setBrandName(mDBHelper.getBrandNameForID(tireModelInfo.getString(tireModelInfo
											.getColumnIndex("IDTireBrand"))));
									String mSize = tireModelInfo
											.getString(tireModelInfo
													.getColumnIndex("FSize"));
									if (TextUtils.isEmpty(mSize)) {
										mSize = "";
									}
									tire.setSize(mSize);
									tire.setAsp(tireModelInfo.getString(tireModelInfo
											.getColumnIndex("AspectRatio")));
									tire.setRim(tireModelInfo.getString(tireModelInfo
											.getColumnIndex("Diameter")));
									tire.setDesign(tireModelInfo
											.getString(tireModelInfo
													.getColumnIndex("Deseign")));
									tire.setDesignDetails(tireModelInfo.getString(tireModelInfo
											.getColumnIndex("FullTireDetails")));
								} else {
									tire.setSize("0");
									tire.setAsp("0");
									tire.setRim("0");
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (mPosToAdd != -1) {
								LogUtil.d("EditJob", "actionType:: "
										+ mActionTypeStr);
								if (mActionTypeStr.equalsIgnoreCase("2")) { // Mount
									Constants.TIRE_INFO.add(tire);
									// actionType.equalsIgnoreCase("25")
									// || actionType.equalsIgnoreCase("9")
									// || actionType.equalsIgnoreCase("10")
									// || actionType.equalsIgnoreCase("12")
									// || actionType.equalsIgnoreCase("13")
									// || actionType.equalsIgnoreCase("14")
									// || actionType.equalsIgnoreCase("15")
									// || actionType.equalsIgnoreCase("16")
									// || actionType.equalsIgnoreCase("17")
									// || actionType.equalsIgnoreCase("18")
									// || actionType.equalsIgnoreCase("19")
									// || actionType.equalsIgnoreCase("20")
									// || actionType.equalsIgnoreCase("21")
									// || actionType.equalsIgnoreCase("22")
									// || actionType.equalsIgnoreCase("23")
								} else if (mActionType.contains(mActionTypeStr)) { // WOT
									boolean isFromPreviousJob = checkAddedPositionIsFromPreviousJob(
											tyreId, tyrePosition);
									if (isFromPreviousJob) {
										Constants.TIRE_INFO
												.set(mPosToAdd, tire);
									} else {
										Constants.TIRE_INFO.add(tire);
									}
								} else if (mActionTypeStr.equalsIgnoreCase("1")) { // Dismount
									// Dismount Case
									boolean isPWTorWOT = isPWTorWOT(tyrePosition); // Filter
																					// WOT
									System.out.println("isPWTorWOT:: "
											+ isPWTorWOT);
									if (isPWTorWOT) {
										Constants.TIRE_INFO.add(tire);
									} else {
										Constants.TIRE_INFO.add(tire);
										// Constants.TIRE_INFO.set(mPosToAdd,
										// tire);
									}

								} else if (mActionTypeStr.equalsIgnoreCase("3")) {
									String tierIdPerviousAdded = Constants.TIRE_INFO
											.get(mPosToAdd).getExternalID();
									boolean isSwap = checkSwapedTierPosition(
											tyreId, tyrePosition);
									if (isSwap && !directAdd) {
										Constants.TIRE_INFO.add(tire);
										directAdd = false;
									} else if (mGetTyreIdFromJobItem
											.contains(tierIdPerviousAdded)) {
										Constants.TIRE_INFO.add(tire);
									} else {
										Constants.TIRE_INFO
												.set(mPosToAdd, tire);
									}
								}

								//Fix
								//Bug 648 : eJob summary, tyre position history not showing all items, tyre history goes missing
								else if(mActionTypeStr.equalsIgnoreCase("26"))
								{
									boolean isFromPreviousJob = checkAddedPositionIsFromPreviousJob(
											tyreId, tyrePosition);
									if (isFromPreviousJob) {
										Constants.TIRE_INFO
												.set(mPosToAdd, tire);
									} else {
										Constants.TIRE_INFO.add(tire);
									}
								}
								else {
									// Constants.TIRE_INFO.add(tire);
									Constants.TIRE_INFO.set(mPosToAdd, tire);
								}
								mActionTypeStr = "";
								mPosToAdd = -1;
							} else {
								Constants.TIRE_INFO.add(tire);
							}
							LogUtil.e("EditJob",
									"Constants.TIRE_INFO Size::::: "
											+ Constants.TIRE_INFO.size());
						}
					}
				}
			}
			if (jobCursor != null) {
				jobCursor.moveToFirst();
				int numRows = jobCursor.getCount();
				if (numRows > 0) {
					int mCallerName = jobCursor.getColumnIndex("CallerName");
					int mCallerPhone = jobCursor.getColumnIndex("CallerPhone");
					int mDefectAuthNumber = jobCursor
							.getColumnIndex("DefectAuthNumber");
					int mDriverName = jobCursor.getColumnIndex("DriverName");
					int mVehicleODO = jobCursor.getColumnIndex("VehicleODO");
					int mHudoMeter = jobCursor.getColumnIndex("Hudometer");
					int mLocation = jobCursor.getColumnIndex("Location");
					int mActionLineN = jobCursor.getColumnIndex("ActionLineN");
					int mGpsLocation = jobCursor.getColumnIndex("GPSLocation");
					int mStockLocation = jobCursor
							.getColumnIndex("StockLocation");

					//CR - 548 : Bring the latest SAP tacho rwading to the application and compare to that entered by the user
					//Getting Column index for Previous Odometer Value
					int mPreviousOdometerValue = jobCursor
							.getColumnIndex("PreviousOdometerValue");

					//CR - 550 (some vehicles have different torque settings per axle)
					//Getting Column index for Torque Settings for axle
					int mTorqueSettingsSame = jobCursor
							.getColumnIndex("TorqueSettingsSame");

					// sJobItemToDelete = jobCursor.getString(jobCursor
					// .getColumnIndex("jobRefNumber"));
					mJobId = jobCursor.getString(jobCursor
							.getColumnIndex("ExternalID"));
					Constants.jobIdEdit = mJobId;

					Constants.contract.setLicenseNum(jobCursor
							.getString(jobCursor
									.getColumnIndex("LicenseNumber")));
					Constants.contract.setContractId(jobCursor
							.getString(jobCursor
									.getColumnIndex("SAPContractNumber")));
					Constants.SAP_CUSTOMER_ID_JOB = Constants.contract
							.getContractId();
					Constants.contract
							.setContractType(jobCursor.getString(jobCursor
									.getColumnIndex("ContractType")));
					Constants.contract.setVehicleSapCode(jobCursor
							.getString(jobCursor.getColumnIndex("VehicleID")));
					Constants.VENDORCOUNTRY_ID = jobCursor.getString(jobCursor
							.getColumnIndex("VendorCountryID"));
					Constants.VENDORCOUNTRY_ID = jobCursor.getString(jobCursor
							.getColumnIndex("VendorCountryID"));
					Constants.VENDER_NAME = jobCursor.getString(jobCursor
							.getColumnIndex("VendorDetail"));
					Constants.SAPCONTRACTNUMBER = jobCursor.getString(jobCursor
							.getColumnIndex("VendorSAPCode"));
					Constants.VENDORSAPCODE = jobCursor.getString(jobCursor
							.getColumnIndex("VendorSAPCode"));
					Constants.JOBREFNUMBERVALUE = jobCursor.getInt(jobCursor
							.getColumnIndex("jobRefNumber"));

					long receivedDate = jobCursor.getLong(jobCursor
							.getColumnIndex("TimeReceived"));
					long startedDate = jobCursor.getLong(jobCursor
							.getColumnIndex("TimeStarted"));
					long onSiteDate = jobCursor.getLong(jobCursor
							.getColumnIndex("TimeOnSite"));
					long finishedDate = jobCursor.getLong(jobCursor
							.getColumnIndex("TimeFinished"));

					int status = jobCursor.getInt(jobCursor
							.getColumnIndex("Status"));

					/*
					 * The below is being commented and now we're showing dialog
					 * instead of toast to resolve Bug Id. 285.
					 */
					// FIX: Issue no 290 Added check here to show the toast
					/*
					 * DateTime dateTime = DateTimeUTC
					 * .convertMillisecondsToUTCDateTime(startedDate); long
					 * currenttime = System.currentTimeMillis(); long
					 * timeInMilliseconds = dateTime.getMillis(); if (status ==
					 * 1 && (currenttime - timeInMilliseconds > 259200000)) {
					 * try { CommonUtils.notify(db.getLabel(5), context); }
					 * catch (Exception e) { e.printStackTrace(); } }
					 */

					Date receivedDateTimeFromDB = DateTimeUTC
							.toDate(receivedDate);
					Date startedDateTimeFromDB = DateTimeUTC
							.toDate(startedDate);
					Date onSiteDateTimeFromDB = DateTimeUTC.toDate(onSiteDate);
					Date finishedDateTimeFromDB = DateTimeUTC
							.toDate(finishedDate);

					EjobTimeLocationFragment.sDateRecievedSaved = DateTimeUTC
							.convertDateToString(receivedDateTimeFromDB,
									mContext);
					EjobTimeLocationFragment.sTimeRecievedSaved = DateTimeUTC
							.convertTimeToString(receivedDateTimeFromDB,
									mContext);

					EjobTimeLocationFragment.sDateStartedSaved = DateTimeUTC
							.convertDateToString(startedDateTimeFromDB,
									mContext);
					EjobTimeLocationFragment.sTimeStartedSaved = DateTimeUTC
							.convertTimeToString(startedDateTimeFromDB,
									mContext);

					EjobTimeLocationFragment.sDateOnSiteSaved = DateTimeUTC
							.convertDateToString(onSiteDateTimeFromDB, mContext);
					EjobTimeLocationFragment.sTimeOnSiteSaved = DateTimeUTC
							.convertTimeToString(onSiteDateTimeFromDB, mContext);

					EjobTimeLocationFragment.sDateFinishedSaved = DateTimeUTC
							.convertDateToString(finishedDateTimeFromDB,
									mContext);
					EjobTimeLocationFragment.sTimeFinishedSaved = DateTimeUTC
							.convertTimeToString(finishedDateTimeFromDB,
									mContext);

					Constants.TIME_LOCATION_SET = true;
					if (jobCursor
							.getString(jobCursor.getColumnIndex("JobType"))
							.equals("0")) {
						Constants.JOBTYPEREGULAR = true;
					} else {
						Constants.JOBTYPEREGULAR = false;
					}

					Constants.SAP_CONTRACTNUMBER_ID = jobCursor
							.getString(jobCursor
									.getColumnIndex("SAPContractNumber"));
					// TestAdapter.updateEJobForm(valueCallerName.getText().toString());
					JobBundle.setCallerName(jobCursor.getString(mCallerName));
					JobBundle.setCallerPhone(jobCursor.getString(mCallerPhone));
					JobBundle.setmDefectAuthNumber(jobCursor
							.getString(mDefectAuthNumber));
					JobBundle.setmDriverName(jobCursor.getString(mDriverName));
					JobBundle.setmVehicleODO(jobCursor.getString(mVehicleODO));
					JobBundle.setLocation(jobCursor.getString(mLocation));
					JobBundle
							.setmActionLineN(jobCursor.getString(mActionLineN));
					Log.e("Jaani",
							"mCallerName" + jobCursor.getString(mCallerName)
									+ "mCallerPhone"
									+ jobCursor.getString(mCallerPhone)
									+ "mDefectAuthNumber"
									+ jobCursor.getString(mDefectAuthNumber)
									+ "mDriverName"
									+ jobCursor.getString(mDriverName));

					Job.setActionLineN(jobCursor.getString(mActionLineN));
					Job.setVehicleODO(jobCursor.getString(mVehicleODO));
					Job.setHudometer(jobCursor.getString(mHudoMeter));
					Job.setDriverName(jobCursor.getString(mDriverName));
					Job.setDefectAuthNumber(jobCursor
							.getString(mDefectAuthNumber));
					Job.setCallerName(jobCursor.getString(mCallerName));
					Job.setCallerPhone(jobCursor.getString(mCallerPhone));
					Job.setLocation(jobCursor.getString(mLocation));
					Job.setTimeStarted(String.valueOf(startedDate));
					Job.setTimeFinished(String.valueOf(finishedDate));
					Job.setTimeOnSite(String.valueOf(onSiteDate));
					Job.setTimeReceived(String.valueOf(receivedDate));
					Job.setGpsLocation(jobCursor.getString(mGpsLocation));
					Job.setStockLocation(jobCursor.getString(mStockLocation));

					//CR - 548 : Bring the latest SAP tacho rwading to the application and compare to that entered by the user
					//To set Last Recorded Odometer Value
					Constants.contract.setVehicleODO(jobCursor.getString(mPreviousOdometerValue));
					Job.setPreviousOdometerValue(jobCursor.getString(mPreviousOdometerValue));

					//CR - 550 (some vehicles have different torque settings per axle)
					//To set Torque Settings for axle value
					Job.setTorqueSettingsSame(jobCursor.getString(mTorqueSettingsSame));
				}
			}

			getJobsFromDBCursors();

			loadJobItemFromDB();
			updateTireInfoFromJobItem();

			loadTorqueSettings();
			updateRetorque();
			//
			loadJobCorrection();
			updateJobCorrection();

			loadReserveTyreDetailsFromDB();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (mDBHelper != null) {
				mDBHelper.close();
			}
			if (jobCursor != null) {
				jobCursor.close();
			}
		}

		Intent intent = new Intent(mContext, EjobFormActionActivity.class);
		intent.putExtra(KEY_COMES_FROM_EDIT, true);
		mContext.startActivity(intent);
	}

	public void loadReserveTyreDetailsFromDB() {
		String mJobID = String.valueOf(Constants.GETSELECTEDLISTPOSITION);
		if (!TextUtils.isEmpty(mJobID)) {
			if (mDBHelper != null) {
				Cursor cur = mDBHelper.getReserveTyreDetails();
				if (cur != null && cur.getCount() > 0) {
					for (int i = 0; i < cur.getCount(); i++) {
						if (cur.moveToPosition(i)) {
							HashMap<String, String> mHmap = new HashMap<String, String>();

							mHmap.put("licence", cur.getString(cur
									.getColumnIndex("SerialNumber")));// Serial

							mHmap.put("cname", cur.getString(cur
									.getColumnIndex("FullTireDetails")));// Design

							mHmap.put("TYRE_ID",
									cur.getString(cur.getColumnIndex("TyreID")));

							mHmap.put("status", "false");

							Constants.Search_TM_ListItems.add(mHmap);
						}
					}
				}
			}
			loadmReserveTyreList();
		}
	}

	/**
	 * 
	 */
	private void loadTyreImage(String jobItemId) {
		mTyreImageCur = mDBHelper.getAllValuesFromTireImageTable(jobItemId);
	}

	/**
	 * 
	 */
	private void updateTyreImage(String jobItemId) {
		if (mTyreImageCur == null) {
			return;
		}
		for (boolean hasTorqueSetting = mTyreImageCur.moveToFirst(); hasTorqueSetting; hasTorqueSetting = mTyreImageCur
				.moveToNext()) {
			TireImageItem tireImage = new TireImageItem();

			tireImage.setJobItemId(jobItemId);
			tireImage.setDamageDescription(mTyreImageCur
					.getString(mTyreImageCur.getColumnIndex("Description")));
			tireImage.setImageType(mTyreImageCur.getString(mTyreImageCur
					.getColumnIndex("ImageType")));
			tireImage.setJobItemId(mTyreImageCur.getString(mTyreImageCur
					.getColumnIndex("JobItemID")));
			tireImage.setTyreImage(mTyreImageCur.getBlob(mTyreImageCur
					.getColumnIndex("TyreImage")));

			Constants.JOB_TIREIMAGE_ITEMLIST.add(tireImage);
		}
	}

	/**
	 * 
	 */
	private void updateJobCorrection() {
		if (mJobCorrectionCur == null) {
			return;
		}

		for (boolean hasTorqueSetting = mJobCorrectionCur.moveToFirst(); hasTorqueSetting; hasTorqueSetting = mJobCorrectionCur
				.moveToNext()) {
			JobCorrection jobCorrection = new JobCorrection();

			jobCorrection.setmJobID(mJobCorrectionCur
					.getString(mJobCorrectionCur.getColumnIndex("JobID")));
			jobCorrection.setmTyreID(mJobCorrectionCur
					.getString(mJobCorrectionCur.getColumnIndex("TyreID")));
			jobCorrection.setmKeyName(mJobCorrectionCur
					.getString(mJobCorrectionCur.getColumnIndex("KeyName")));
			jobCorrection.setmOldValue(mJobCorrectionCur
					.getString(mJobCorrectionCur.getColumnIndex("OldValue")));
			jobCorrection.setmNewValue(mJobCorrectionCur
					.getString(mJobCorrectionCur.getColumnIndex("NewValue")));

			//To determine whether a job correction can be added to a snapshot DB or not
			jobCorrection.setIsPerfect(mJobCorrectionCur
					.getString(mJobCorrectionCur.getColumnIndex("IsPerfect")));
			//Added a check to avoid unwanted job Correction
			if(!jobCorrection.getmOldValue().equalsIgnoreCase(jobCorrection.getmNewValue())) {
				Constants.JOB_CORRECTION_LIST.add(jobCorrection);
			}
		}
	}

	/**
	 * 
	 */
	private void loadJobCorrection() {
		mJobCorrectionCur = mDBHelper
				.getAllValuesFromJobCorrectionTable(mJobId);
	}

	/**
	 * 
	 */
	private void loadTorqueSettings() {
		mTorqueSettingsCur = mDBHelper.getAllValuesFromTorqueSettings(mJobId);
	}

	private void updateRetorque() {
		if (mTorqueSettingsCur == null) {
			return;
		}
		try {
			Constants.retorqueUpdateID = new ArrayList<>();
			for (boolean hasTorqueSetting = mTorqueSettingsCur.moveToFirst(); hasTorqueSetting; hasTorqueSetting = mTorqueSettingsCur
					.moveToNext()) {
				TorqueSettings torque = new TorqueSettings();
				torque.setJobId(mTorqueSettingsCur.getString(mTorqueSettingsCur
						.getColumnIndex("JobID")));
				torque.setIsWheelRemoved(mTorqueSettingsCur
						.getInt(mTorqueSettingsCur
								.getColumnIndex("IsWheelRemoved")));
				torque.setSapCodeWP(mTorqueSettingsCur
						.getString(mTorqueSettingsCur
								.getColumnIndex("SAPCodeWP")));
				torque.setWrenchNumber(mTorqueSettingsCur
						.getString(mTorqueSettingsCur
								.getColumnIndex("WrenchNumber")));
				torque.setNoManufacturerTorqueSettingsReason(mTorqueSettingsCur.getString(mTorqueSettingsCur
						.getColumnIndex("NoManufacturerTorqueSettingReason")));
				torque.setTorqueSettingsValue(mTorqueSettingsCur
						.getInt(mTorqueSettingsCur
								.getColumnIndex("TorqueSettingValue")));
				torque.setRetorqueLabelIssued(mTorqueSettingsCur
						.getInt(mTorqueSettingsCur
								.getColumnIndex("RetorqueLabelIssued")));
				torque.setWheelPos(mTorqueSettingsCur
						.getString(mTorqueSettingsCur
								.getColumnIndex("WheelPosition")));
				Constants.TORQUE_SETTINGS_ARRAY.add(torque);
				Constants.retorqueUpdateID.add(mTorqueSettingsCur
						.getInt(mTorqueSettingsCur.getColumnIndex("ID")));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * update tire info from job item
	 */
	private void updateTireInfoFromJobItem() {
		for (JobItem jobItem : Constants.JOB_ITEMLIST) {
			if (jobItem.getTyreID() != "") {
				for (Tyre t : Constants.TIRE_INFO) {
					if (t.getExternalID().equals(jobItem.getTyreID())) {
						switch (jobItem.getActionType()) {
						case TireActionType.MOUNTPW:
							t.setTyreState(TyreState.PART_WORN);
							t.setPWTMounted(true);
							break;
						case TireActionType.MOUNTNEW:
							t.setTyreState(TyreState.NEW_TIRE);
							t.setHasMounted(true);
							break;
						case TireActionType.REGROOVE:
							t.setTyreState(TyreState.PART_WORN);
							t.setReGrooved(true);
							break;
						case TireActionType.REMOVE:
							t.setTyreState(TyreState.EMPTY);
							t.setDismounted(true);
							break;
						case TireActionType.SWAP:
							if ("1".equals(jobItem.getSwapType())) {
								t.setIsphysicalySwaped(true);
							} else {
								t.setLogicalySwaped(true);
							}
							t.setTyreState(TyreState.PART_WORN);
							if (t.isDismounted()) {
								t.setTyreState(TyreState.EMPTY);
							}
							if (t.isHasMounted()) {
								t.setTyreState(TyreState.NEW_TIRE);
							}
							break;
						case TireActionType.TOR:
							t.setTyreState(TyreState.PART_WORN);
							t.setHasTurnedOnRim(true);
							break;
						case TireActionType.SERVICEMATERIAL:
							ServicesEditHelper service = new ServicesEditHelper();
							service.setCount(jobItem.getServiceCount());
							service.setSapCode(jobItem.getServiceID());
							Constants.servicesEdit.add(service);
							break;
						case TireActionType.AXLESERVICE:
							break;
						case TireActionType.VALVEFITTED:
						case TireActionType.HIGHTPRESSUREVALUECAPFITTED:
						case TireActionType.VALUEFIXINGSUPPORT:
						case TireActionType.DYNAMICBALANCING:
						case TireActionType.STATICBALANCING:
						case TireActionType.RIGIDVALVEEXTENSINFITTED:
						case TireActionType.FLEXIBLEVALVEEXTENSINFITTED:
						case TireActionType.PUNCTUREDREPAIRREPAIRED:
						case TireActionType.MINORREPAIR:
						case TireActionType.MAJORREPAIR:
						case TireActionType.TYREREPAIREANDREINFORCEMENT:
						case TireActionType.BREAKSPOTREPAIR:
						case TireActionType.MAJORREPAIRWITHVULCANISATION:
						case TireActionType.TIREFILL:
							t.setTyreState(TyreState.PART_WORN);
							t.setWorkedOn(true);
							break;
						default:
							break;
						}
					}
				}
				if (jobItem.getActionType().equals(
						TireActionType.SERVICEMATERIAL)) {
					ServicesEditHelper service = new ServicesEditHelper();
					service.setCount(jobItem.getServiceCount());
					service.setSapCode(jobItem.getServiceID());
					Constants.servicesEdit.add(service);
				}
			}
		}
	}

	private static String checkMinutes(int min) {
		if (min < 10) {
			return "0" + min;
		} else {
			return String.valueOf(min);
		}
	}

	private static String checkHours(int hour) {
		if (hour < 10) {
			return "0" + hour;
		} else {
			return String.valueOf(hour);
		}
	}

	private void loadmReserveTyreList()
	{
		ReservePWTUtils pwtUtils = new ReservePWTUtils();
		pwtUtils.updateReserveTyre(mContext);
	}


}
