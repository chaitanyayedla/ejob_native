package com.goodyear.ejob.job.operation.helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.DBModel;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.VehicleSkeletonInfo;

/**
 * @author shailesh.p
 * 
 */
public class PressureCheck {

	private Tyre selectedTire;
	private Context context;
	private float recommendedPressure;
	private int axlepos, axleposOne, axleposTwo;
	private String save;
	private String cancel;
	private String recInflaction;
	private String pressureUnit;
	private String pressureRecHeading;
	private boolean isSettingsPSI;
	private String errorMessage;
	private float barValue;
	private boolean isSwapSecondTire;
	private static EditText pressureValue;

	public static int axlePosSaved;
	public static float axlePressureSaved;
	public static String pressureSet;
	public static boolean dialogCreated;
	public static boolean loadSavedPressure;
	AlertDialog alertDialog;
	//User Trace logs trace tag
	private static final String TRACE_TAG = "Pressure Check";
	public PressureCheck(Tyre tire, Context context) {
		selectedTire = tire;
		this.context = context;

	}

	public static int getAxlePos() {
		return axlePosSaved;
	}

	public static float getAxlePressure() {
		return axlePressureSaved;
	}

	public static String getPressureSet() {
		return pressureValue.getText().toString();
	}

	public static boolean getDialogSet() {
		return dialogCreated;
	}

	public static boolean isDoRetorque() {
		return VehicleSkeletonFragment.mDoRetroque;
	}

	public void updatePressureFromAxleRecommended(float recommendedPressure,
			int axlePosition) {
		updatePressureFromAxleRecommended(recommendedPressure, axlePosition,
				false);
	}

	/**
	 * @param mPressureValue
	 *            the mPressureValue to set
	 */
	public void setmPressureValue(String mPressureValue) {
		PressureCheck.pressureValue.setText(mPressureValue);
	}

	public void togglePressureField(boolean enable) {
		PressureCheck.pressureValue.setEnabled(enable);
	}

	public void updatePressureFromAxleRecommended(float recommendedPressure,
			int axlePosition, boolean isSwapSecondTire) {

		System.out.println("updatePressureFromAxleRecommended--");
		System.out.println("recommendedPressure--" + recommendedPressure);
		System.out.println("axlePosition--" + axlePosition);

		float recommendedPressureOne, recommendedPressureTwo;
		this.recommendedPressure = recommendedPressure;
		this.axlepos = axlePosition;
		this.isSwapSecondTire = isSwapSecondTire;

		getSetPressure();
		getLabelsFromDB();

		if (recommendedPressure == 0) {
			axlePosSaved = axlepos;
			axlePressureSaved = recommendedPressure;
			// if pressure for axle is not set
			// prompt the user for axle pressure
			createPressureDialog();
		} else {

			// if pressure is set for the axle
			// update the tire pressure to
			// tire

			if (Constants.DO_PHYSICAL_SWAP) {
				for (int i = 0; i < 2; i++) {
					if (i == 0) {
						axlepos = Character
								.getNumericValue(Constants.SELECTED_TYRE
										.getPosition().charAt(0)) - 1;
						recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
								.get(axlepos);
						Constants.SELECTED_TYRE.setPressure(String
								.valueOf(recommendedPressure));
						// For Double Swap
						if (Constants.THIRD_SELECTED_TYRE != null) {
							axlepos = Character
									.getNumericValue(Constants.THIRD_SELECTED_TYRE
											.getPosition().charAt(0)) - 1;
							recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
									.get(axlepos);
							Constants.THIRD_SELECTED_TYRE.setPressure(String
									.valueOf(recommendedPressure));

						}
						// Triple Swap
						if (Constants.FOURTH_SELECTED_TYRE != null) {
							axlepos = Character
									.getNumericValue(Constants.FOURTH_SELECTED_TYRE
											.getPosition().charAt(0)) - 1;
							recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
									.get(axlepos);
							Constants.FOURTH_SELECTED_TYRE.setPressure(String
									.valueOf(recommendedPressure));
						}
					} else if (i == 1) {
						axlepos = Character
								.getNumericValue(Constants.SECOND_SELECTED_TYRE
										.getPosition().charAt(0)) - 1;
						recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
								.get(axlepos);
						Constants.SECOND_SELECTED_TYRE.setPressure(String
								.valueOf(recommendedPressure));
						// Double SWAP
						if (Constants.THIRD_SELECTED_TYRE != null) {
							axlepos = Character
									.getNumericValue(Constants.THIRD_SELECTED_TYRE
											.getPosition().charAt(0)) - 1;
							recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
									.get(axlepos);
							Constants.THIRD_SELECTED_TYRE.setPressure(String
									.valueOf(recommendedPressure));
						}
						// Triple Swap
						if (Constants.FOURTH_SELECTED_TYRE != null) {
							axlepos = Character
									.getNumericValue(Constants.FOURTH_SELECTED_TYRE
											.getPosition().charAt(0)) - 1;
							recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
									.get(axlepos);
							Constants.FOURTH_SELECTED_TYRE.setPressure(String
									.valueOf(recommendedPressure));
						}
					}
				}
				int jobItemSize = VehicleSkeletonFragment.mJobItemList.size();
				if (jobItemSize >= 2) {
					int lastJobAddedPosition2 = jobItemSize - 2;
					// if (lastJobAddedPosition2 >= 2) {
					JobItem jobItem2 = VehicleSkeletonFragment.mJobItemList
							.get(lastJobAddedPosition2);
					jobItem2.setRecInflactionDestination(String
							.valueOf(recommendedPressure));
					jobItem2.setPressure(String.valueOf(recommendedPressure));
					VehicleSkeletonFragment.mJobItemList.set(
							lastJobAddedPosition2, jobItem2);

					int lastJobAddedPosition1 = jobItemSize - 1;
					/* if (lastJobAddedPosition1 >= 1) { */
					JobItem jobItem1 = VehicleSkeletonFragment.mJobItemList
							.get(lastJobAddedPosition1);
					jobItem1.setRecInflactionDestination(String
							.valueOf(recommendedPressure));
					jobItem1.setPressure(String.valueOf(recommendedPressure));
					VehicleSkeletonFragment.mJobItemList.set(
							lastJobAddedPosition1, jobItem1);

				}
				if (recommendedPressure != 0) {
					Retorque retorque = new Retorque(selectedTire, context);
					VehicleSkeletonFragment.mRetroqueStarted = true;
					retorque.startRetroqueProcess();
				}

			} else if (Constants.DO_LOGICAL_SWAP) {

				for (int i = 0; i < 2; i++) {
					if (i == 0) {
						axlepos = Character
								.getNumericValue(Constants.SELECTED_TYRE
										.getPosition().charAt(0)) - 1;
						recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
								.get(axlepos);
						Constants.SELECTED_TYRE.setPressure(String
								.valueOf(recommendedPressure));
					} else if (i == 1) {
						axlepos = Character
								.getNumericValue(Constants.SECOND_SELECTED_TYRE
										.getPosition().charAt(0)) - 1;
						recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
								.get(axlepos);
						Constants.SECOND_SELECTED_TYRE.setPressure(String
								.valueOf(recommendedPressure));
					}
				}

				int jobItemSize = VehicleSkeletonFragment.mJobItemList.size();
				if (jobItemSize >= 2) {
					int lastJobAddedPosition2 = jobItemSize - 2;
					// if (lastJobAddedPosition2 >= 2) {
					JobItem jobItem2 = VehicleSkeletonFragment.mJobItemList
							.get(lastJobAddedPosition2);
					jobItem2.setRecInflactionDestination(String
							.valueOf(recommendedPressure));
					jobItem2.setPressure(String.valueOf(recommendedPressure));
					VehicleSkeletonFragment.mJobItemList.set(
							lastJobAddedPosition2, jobItem2);

					int lastJobAddedPosition1 = jobItemSize - 1;
					/* if (lastJobAddedPosition1 >= 1) { */
					JobItem jobItem1 = VehicleSkeletonFragment.mJobItemList
							.get(lastJobAddedPosition1);
					jobItem1.setRecInflactionDestination(String
							.valueOf(recommendedPressure));
					jobItem1.setPressure(String.valueOf(recommendedPressure));
					VehicleSkeletonFragment.mJobItemList.set(
							lastJobAddedPosition1, jobItem1);
				}

				if (!VehicleSkeletonFragment.mIsPressureHigh) {
					VehicleSkeletonFragment.resetBoolValue();
					VehicleSkeletonFragment.resetTyres();
				}
			}
		}
	}

	/**
	 * 
	 */
	private void getSetPressure() {
		if (CommonUtils.getPressureUnit(context).equals(
				Constants.PSI_PRESSURE_UNIT))
			isSettingsPSI = true;
		else
			isSettingsPSI = false;
	}

	/**
	 * 
	 */
	public void startCreatePressureDialog() {
		getSetPressure();
		recommendedPressure = axlePressureSaved;
		axlepos = axlePosSaved;
		getLabelsFromDB();
		createPressureDialog();
	}

	public static void savePressureCheck() {
		if (pressureValue != null) {
			String strPressureValue = pressureValue.getText().toString().trim();
			if (!TextUtils.isEmpty(strPressureValue)) {
				axlePressureSaved = CommonUtils.parseFloat(strPressureValue);
			}
		}

	}

	/**
	 * 
	 */
	private void getLabelsFromDB() {
		DatabaseAdapter label = new DatabaseAdapter(context);
		label.createDatabase();
		label.open();

		save = label.getLabel(Constants.SAVE_LABEL);
		cancel = label.getLabel(Constants.CANCEL_LABEL);
		recInflaction = label.getLabel(Constants.REC_INFLACTION);
		pressureUnit = CommonUtils.getPressureUnit(context);
		pressureRecHeading = label.getLabel(Constants.RECCOMENDED_PRESSURE);
		errorMessage = label.getLabel(Constants.ERROR_MESSAGE);
		Constants.sLblCheckPressureValue = label.getLabel(Constants.CHECK_PRESSURE_LABEL);
		label.close();
	}

	public void createPressureDialog() {
		dialogCreated = true;

		LayoutInflater inflator = LayoutInflater.from(context);
		View promptsView = inflator.inflate(R.layout.pressure_tor_check, null);
		LogUtil.TraceInfo(TRACE_TAG, "none", "Dialog", true, false, false);
		TextView pressureLabel = (TextView) promptsView
				.findViewById(R.id.pressure_rec_label);
		pressureLabel.setText(recInflaction);

		// dialogText.setText(message);o
		pressureValue = (EditText) promptsView
				.findViewById(R.id.value_rec_pressure);

		// set user activity for
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		final TextView position = (TextView) promptsView.findViewById(R.id.value_pos);
		if (selectedTire != null) {
			position.setText(selectedTire.getPosition());
		}
		// if its not a physical swap or logical swap
		// hide position
		if (!Constants.DO_PHYSICAL_SWAP && !Constants.DO_LOGICAL_SWAP) {
			LinearLayout postionHolder = (LinearLayout) promptsView
					.findViewById(R.id.position_container);
			postionHolder.setVisibility(LinearLayout.GONE);
		}
		// if created after orientation change
		if (loadSavedPressure && axlePressureSaved != 0.0)
			pressureValue.setText(String.valueOf(axlePressureSaved));

		/*Bug 728 : Adding context to show toast for higher pressure value*/
		pressureValue.addTextChangedListener(new PressureCheckListener(
				pressureValue, isSettingsPSI, context));

		if (isSettingsPSI) {
			pressureValue
					.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
							4, 2) });// PSI 150.00

		} else {
			pressureValue
					.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
							3, 2) });// BAR 10.34
		}

		TextView pressureRecTitle = (TextView) promptsView
				.findViewById(R.id.pressure_rec_title);
		pressureRecTitle.setText(pressureRecHeading);

		final TextView pressureUnitLabel = (TextView) promptsView
				.findViewById(R.id.pressure_unit);
		pressureUnitLabel.setText(pressureUnit);

		final EjobAlertDialog dialogBuilder = new EjobAlertDialog(context);

		// set prompts.xml to alertdialog builder
		dialogBuilder.setView(promptsView);

		// add header
		// dialogBuilder.setTitle(pressureRecHeading);

		dialogBuilder.setCancelable(false).setPositiveButton(save,
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "none", "BT - Save", false, false, false);
						String pressureSet = pressureValue.getText().toString()
								.trim();
						if (TextUtils.isEmpty(pressureSet)
								|| ".".equals(pressureSet)) {
							LogUtil.TraceInfo(TRACE_TAG, "none", "Msg : "+Constants.sLblCheckPressureValue, false, false, false);
							CommonUtils.notify(
									Constants.sLblCheckPressureValue, context);
							createPressureDialog();
							return;
						}

						if (!CommonUtils.pressureValueValidation(pressureValue)) {
							LogUtil.TraceInfo(TRACE_TAG, "none", "Msg : "+Constants.sLblCheckPressureValue, false, false, false);
							CommonUtils.notify(
									Constants.sLblCheckPressureValue, context);
							createPressureDialog();
							return;
						}

						// on click of apply
						barValue = 0;
						if (isSettingsPSI) {
							// change PSI to BAR
							barValue = CommonUtils.parseFloat(pressureSet) * 0.068947f;
						} else {
							barValue = CommonUtils.parseFloat(pressureSet);
						}
						selectedTire.setPressure(String.valueOf(barValue));
						VehicleSkeletonFragment.mAxleRecommendedPressure.set(
								axlepos, barValue);
						Constants.FORSWAP_BARVALUE = barValue;

						VehicleSkeletonFragment.mTorStarted = false;

						int lastJobAddedPosition = VehicleSkeletonFragment.mJobItemList
								.size() - 1;
						if (lastJobAddedPosition != -1) {
							JobItem jobItem = VehicleSkeletonFragment.mJobItemList
									.get(lastJobAddedPosition);

							jobItem.setRecInflactionDestination(String
									.valueOf(barValue));
							jobItem.setPressure(String.valueOf(barValue));
							VehicleSkeletonFragment.mJobItemList.set(
									lastJobAddedPosition, jobItem);
						}

						if (Constants.DO_LOGICAL_SWAP) {
							DBModel ejobDbInsert = new DBModel(context);
							ejobDbInsert.open();
							VehicleSkeletonFragment
									.insertJobAfterOperation(ejobDbInsert);
							ejobDbInsert.close();
						}

						if (VehicleSkeletonFragment.mDoRetroque
								&& !Constants.DO_LOGICAL_SWAP
								&& !Constants.DO_PHYSICAL_SWAP) {
							Retorque retorque = new Retorque(
									Constants.SELECTED_TYRE, context);
							VehicleSkeletonFragment.mRetroqueStarted = true;
							retorque.startRetroqueProcess();
						}

						if ((Constants.DO_LOGICAL_SWAP || Constants.DO_PHYSICAL_SWAP)
								&& !isSwapSecondTire) {

							// For Spare
							if (Constants.SECOND_SELECTED_TYRE.getPosition()
									.contains("SP")) {
								try {

									VehicleSkeletonInfo vehicleconfg = new VehicleSkeletonInfo(
											Constants.vehicleConfiguration);
									vehicleconfg.getVehicleSkeletonInfo();
									int axelCount = vehicleconfg.getAxlecount();
									if (Constants.SECOND_SELECTED_TYRE
											.getPosition().startsWith("SP")) {
										if ((vehicleconfg.getAxlecount() + 1) == axelCount) {
											// Do nothing
										} else {
											++axelCount;
										}

										Constants.SECOND_SELECTED_TYRE
												.setPosition((axelCount)
														+ ""
														+ Constants.SECOND_SELECTED_TYRE
																.getPosition());
									} else {
										Constants.SECOND_SELECTED_TYRE
												.setPosition(Constants.SECOND_SELECTED_TYRE
														.getPosition());
									}

								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							isSwapSecondTire = true;
							VehicleSkeletonFragment.mDoRetroque = true;

							if (Constants.SECOND_SELECTED_TYRE.getPosition() != null) {
								axlepos = Character
										.getNumericValue(Constants.SECOND_SELECTED_TYRE
												.getPosition().charAt(0)) - 1;

							}
							// for single swap
							recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
									.get(axlepos);
							selectedTire = Constants.SECOND_SELECTED_TYRE;

							if (recommendedPressure == 0) {
								updatePressureFromAxleRecommended(
										recommendedPressure, axlepos, true);
							} else {
								Constants.SECOND_SELECTED_TYRE
										.setPressure(String
												.valueOf(recommendedPressure));
							}

							// Double Swap
							if (Constants.THIRD_SELECTED_TYRE != null) {
								int axlepos = Character
										.getNumericValue(Constants.THIRD_SELECTED_TYRE
												.getPosition().charAt(0)) - 1;
								float recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
										.get(axlepos);
								if (recommendedPressure == 0) {
									updatePressureFromAxleRecommended(
											recommendedPressure, axlepos, true);
								} else {
									Constants.THIRD_SELECTED_TYRE.setPressure(String
											.valueOf(recommendedPressure));
								}

							}

							// Triple Swap
							if (Constants.FOURTH_SELECTED_TYRE != null) {
								int axlepos = Character
										.getNumericValue(Constants.FOURTH_SELECTED_TYRE
												.getPosition().charAt(0)) - 1;
								float recommendedPressure = VehicleSkeletonFragment.mAxleRecommendedPressure
										.get(axlepos);
								if (recommendedPressure == 0) {
									updatePressureFromAxleRecommended(
											recommendedPressure, axlepos, true);
								} else {
									Constants.FOURTH_SELECTED_TYRE.setPressure(String
											.valueOf(recommendedPressure));
								}
							}
						} else if (Constants.DO_PHYSICAL_SWAP
								&& isSwapSecondTire) {
							Retorque retorque = new Retorque(
									Constants.SELECTED_TYRE, context);
							VehicleSkeletonFragment.mRetroqueStarted = true;
							retorque.startRetroqueProcess();
						} else if (Constants.DO_LOGICAL_SWAP
								&& isSwapSecondTire) {
							VehicleSkeletonFragment.resetBoolValue();
							VehicleSkeletonFragment.resetTyres();
						}
						dialogCreated = false;

						try {
							String Trace_Data = position.getText().toString()+ " | "
									+ pressureValue.getText().toString()
									+ pressureUnitLabel.getText().toString();
							LogUtil.TraceInfo(TRACE_TAG, "none", "Data : " + Trace_Data, false, false, false);
						}
						catch (Exception e)
						{
							LogUtil.TraceInfo(TRACE_TAG, "none", "Data : Exception "+e.getMessage(), false, false, false);
						}

						try {
							// Activity activity = (Activity) context;
							CommonUtils.hideKeyboard(context, alertDialog
									.getWindow().getDecorView().getRootView()
									.getWindowToken());
						} catch (Exception e) {
							LogUtil.i("retorque onacitivityresult",
									"could not close keyboard");
						}

						EjobFormActionActivity parentActivity = (EjobFormActionActivity) context;
						parentActivity.closeConnection();
					}

				});

		dialogBuilder.setCancelable(false).setNegativeButton(cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						LogUtil.TraceInfo(TRACE_TAG, "none", "BT - Cancel", false, false, false);
						dialogBuilder.updateInactivityForDialog();
						createPressureDialog();
					}
				});

		// create alert dialog
		alertDialog = dialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
