package com.goodyear.ejob.job.operation.helpers;

import java.io.Serializable;

/**
 * @author shailesh.p
 * 
 */
public class TurnOnRimHelper implements Serializable{
	private String nskOne;
	private String nskTwo;
	private String nskThree;
	private int rimType;
	private boolean regrooved;

	/**
	 * @return the nskOne
	 */
	public String getNskOne() {
		return nskOne;
	}

	/**
	 * @param nskOne the nskOne to set
	 */
	public void setNskOne(String nskOne) {
		this.nskOne = nskOne;
	}

	/**
	 * @return the nskTwo
	 */
	public String getNskTwo() {
		return nskTwo;
	}

	/**
	 * @param nskTwo the nskTwo to set
	 */
	public void setNskTwo(String nskTwo) {
		this.nskTwo = nskTwo;
	}

	/**
	 * @return the nskThree
	 */
	public String getNskThree() {
		return nskThree;
	}

	/**
	 * @param nskThree the nskThree to set
	 */
	public void setNskThree(String nskThree) {
		this.nskThree = nskThree;
	}

	/**
	 * @return the rimType
	 */
	public int getRimType() {
		return rimType;
	}

	/**
	 * @param rimType the rimType to set
	 */
	public void setRimType(int rimType) {
		this.rimType = rimType;
	}

	/**
	 * @return the regrooved
	 */
	public boolean isRegrooved() {
		return regrooved;
	}

	/**
	 * @param regrooved the regrooved to set
	 */
	public void setRegrooved(boolean regrooved) {
		this.regrooved = regrooved;
	}
}
