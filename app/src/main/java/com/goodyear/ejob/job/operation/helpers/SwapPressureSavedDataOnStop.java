
package com.goodyear.ejob.job.operation.helpers;
/**
 * @author amitkumar.h
 *
 */
public class SwapPressureSavedDataOnStop {
	private boolean mIsDialogSet;
	private int mAxlePosition;
	private float mPressure;
	private String mPressureSet;
	/**
	 * @return the mIsDialogSet
	 */
	public boolean ismIsDialogSet() {
		return mIsDialogSet;
	}
	/**
	 * @param mIsDialogSet the mIsDialogSet to set
	 */
	public void setmIsDialogSet(boolean mIsDialogSet) {
		this.mIsDialogSet = mIsDialogSet;
	}
	/**
	 * @return the mAxlePosition
	 */
	public int getmAxlePosition() {
		return mAxlePosition;
	}
	/**
	 * @param mAxlePosition the mAxlePosition to set
	 */
	public void setmAxlePosition(int mAxlePosition) {
		this.mAxlePosition = mAxlePosition;
	}
	/**
	 * @return the mPressure
	 */
	public float getmPressure() {
		return mPressure;
	}
	/**
	 * @param mPressure the mPressure to set
	 */
	public void setmPressure(float mPressure) {
		this.mPressure = mPressure;
	}
	/**
	 * @return the mPressureSet
	 */
	public String getmPressureSet() {
		return mPressureSet;
	}
	/**
	 * @param mPressureSet the mPressureSet to set
	 */
	public void setmPressureSet(String mPressureSet) {
		this.mPressureSet = mPressureSet;
	}
	/**
	 * @return the mDoRetorqueAfter
	 */
	public boolean ismDoRetorqueAfter() {
		return mDoRetorqueAfter;
	}
	/**
	 * @param mDoRetorqueAfter the mDoRetorqueAfter to set
	 */
	public void setmDoRetorqueAfter(boolean mDoRetorqueAfter) {
		this.mDoRetorqueAfter = mDoRetorqueAfter;
	}
	/**
	 * @return the mPressureSwapPos
	 */
	public int getmPressureSwapPos() {
		return mPressureSwapPos;
	}
	/**
	 * @param mPressureSwapPos the mPressureSwapPos to set
	 */
	public void setmPressureSwapPos(int mPressureSwapPos) {
		this.mPressureSwapPos = mPressureSwapPos;
	}
	private boolean mDoRetorqueAfter;
	private int mPressureSwapPos;
}
