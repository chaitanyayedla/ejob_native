package com.goodyear.ejob.job.operation.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goodyear.ejob.R;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.interfaces.PressureDialogHandler;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.Tyre;

/**
 * This class is used to create a pressure dialog for swap. All swaps must
 * implement {@link PressureDialogHandler}, and the
 * {@link PressureDialogHandler} reference is passed in constructor of
 * {@link PressureSwapDialog}. This reference is then used to
 */
public class PressureSwapDialog {

	private AlertDialog mAlertDialog;
	private PressureDialogHandler currentSaveHandler;
	private int currentTireForSwapPos;
	private String pressureValue;
	private Context mContext;
	private Tyre mSelectedTire;
	private String save;
	private String cancel;
	private String recInflaction;
	private String pressureUnit;
	private String pressureRecHeading;
	private String errorMessage;
	private EditText mEditPressureVal;
	private boolean mIsSettingsPSI;
	private boolean mEditPressureValEnable;
	private String positionLab;
	private String LabelHintEnterValue;

	//User Trace logs trace tag
	private static final String TRACE_TAG = "Pressure Swap Dialog";

	/**
	 * constructor for {@link PressureSwapDialog}
	 * 
	 * @param handler
	 *            Pressure dialog handler, all {@link Activity} that need to
	 *            display Pressure dialog must implement
	 *            {@link PressureDialogHandler}.
	 * @param currentTireForSwap
	 *            This states for which tire (in the tires being swaped)
	 *            pressure dialog should be started.
	 * @param selectedTire
	 *            Tire for which pressure should be captured.
	 */
	public PressureSwapDialog(PressureDialogHandler handler,
			int currentTireForSwap, Tyre selectedTire) {
		currentSaveHandler = handler;
		currentTireForSwapPos = currentTireForSwap;
		mContext = (Context) handler;
		mSelectedTire = selectedTire;

		getLabelsFromDB();
		getSetPressureUnit();
	}

	/**
	 * constructor for {@link PressureSwapDialog}
	 * 
	 * @param handler
	 *            Pressure dialog handler, all {@link Activity} that need to
	 *            display Pressure dialog must implement
	 *            {@link PressureDialogHandler}.
	 * @param currentTireForSwap
	 *            This states for which tire (in the tires being swaped)
	 *            pressure dialog should be started.
	 * @param selectedTire
	 *            Tire for which pressure should be captured.
	 * @param pressureValue
	 *            Any default value to set in dialog {@link TextView}.
	 */
	public PressureSwapDialog(PressureDialogHandler handler,
			int currentTireForSwap, Tyre selectedTire, String pressureValue) {
		currentSaveHandler = handler;
		currentTireForSwapPos = currentTireForSwap;
		this.pressureValue = pressureValue;
		mContext = (Context) handler;
		mSelectedTire = selectedTire;

		getLabelsFromDB();
		getSetPressureUnit();
	}

	/**
	 * Use this method to check if pressure dialog is displayed to user.
	 * 
	 * @return true if dialog is showing else false.
	 */
	public boolean isPressureDialogDisplayed() {
		if (mAlertDialog != null) {
			return mAlertDialog.isShowing();
		}
		return false;
	}

	/**
	 * Use this method to set pressure value in dialog pressure {@link TextView}
	 * .
	 * 
	 * @param pressureValue
	 *            value to be set.
	 * @return false if dialog instance is null or dialog is not showing. true
	 *         if success.
	 */
	public boolean setPressureDialogValue(String pressureValue) {
		if (mAlertDialog != null && mEditPressureVal != null
				&& mAlertDialog.isShowing()) {
			mEditPressureVal.setText(pressureValue);
			return true;
		}
		return false;
	}

	/**
	 * Use this method to enable/disable dialog pressure {@link TextView}.
	 * 
	 * @param enable
	 *            pass true to enable, false to disable
	 */
	public void togglePressureField(boolean enable) {
		if (mAlertDialog != null && mEditPressureVal != null
				&& mAlertDialog.isShowing()) {
			mEditPressureVal.setEnabled(enable);
			mEditPressureValEnable = enable;
		}
	}

	/**
	 * Use this method to get pressure value set by user.
	 * 
	 * @return current pressure set by user, null if dialog not displayed
	 */
	public String getCurrentPressureSetByUser() {
		String pressure = null;
		if (mAlertDialog != null && mAlertDialog.isShowing()) {
			pressure = mEditPressureVal.getText().toString().trim();
		}
		return pressure;
	}

	/**
	 * Use this method to get which tire's pressure is currently displayed in
	 * the dialog.
	 * 
	 * @return Current tire position whose pressure is being captured
	 */
	public int getCurrentTirePos() {
		int pos = -1;
		if (mAlertDialog != null && mAlertDialog.isShowing()) {
			pos = currentTireForSwapPos;
		}
		return pos;
	}

	private void getLabelsFromDB() {
		DatabaseAdapter dataBase = new DatabaseAdapter(mContext);
		dataBase.open();

		save = dataBase.getLabel(Constants.SAVE_LABEL);
		cancel = dataBase.getLabel(Constants.CANCEL_LABEL);
		recInflaction = dataBase.getLabel(Constants.REC_INFLACTION);
		positionLab = dataBase.getLabel(Constants.POSITION_LABEL);
		pressureUnit = CommonUtils.getPressureUnit(mContext);
		pressureRecHeading = dataBase.getLabel(Constants.RECCOMENDED_PRESSURE);
		errorMessage = dataBase.getLabel(Constants.ERROR_MESSAGE);
		Constants.sLblCheckPressureValue = dataBase
				.getLabel(Constants.CHECK_PRESSURE_LABEL);
		LabelHintEnterValue = dataBase.getLabel(Constants.Label_Enter_Value);

		dataBase.close();
	}

	/**
	 * get current pressure unit preference of user
	 */
	private void getSetPressureUnit() {
		if (CommonUtils.getPressureUnit(mContext).equals(
				Constants.PSI_PRESSURE_UNIT)) {
			mIsSettingsPSI = true;
		} else {
			mIsSettingsPSI = false;
		}
	}

	/**
	 * Use this method to dismiss currently displayed dialog
	 */
	public void dismissDialog() {
		if (mAlertDialog != null && mAlertDialog.isShowing()) {
			currentSaveHandler = null;
			mAlertDialog.dismiss();
		}
	}

	/**
	 * This method creates and displays the dialog with information passed in
	 * the constructor.
	 */
	public void showPressureDialog() {

		LayoutInflater inflator = LayoutInflater.from(mContext);

		View promptsView = inflator.inflate(R.layout.pressure_tor_check, null);

		LogUtil.TraceInfo(TRACE_TAG, "none", "Dialog", true, false, false);

		TextView positionLabel = (TextView) promptsView
				.findViewById(R.id.pressure_pos_label);
		positionLabel.setText(positionLab);

		TextView pressureLabel = (TextView) promptsView
				.findViewById(R.id.pressure_rec_label);
		pressureLabel.setText(recInflaction);

		// set user activity for
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		// dialogText.setText(message);
		mEditPressureVal = (EditText) promptsView
				.findViewById(R.id.value_rec_pressure);
		mEditPressureVal.setHint(LabelHintEnterValue);
		final TextView position = (TextView) promptsView.findViewById(R.id.value_pos);

		if (mSelectedTire != null) {
			position.setText(mSelectedTire.getPosition());
		}

		if (mEditPressureVal != null && pressureValue != null) {
			mEditPressureVal.setText(pressureValue);
		}
		/*Bug 728 : Adding context to show toast for higher pressure value*/
		mEditPressureVal.addTextChangedListener(new PressureCheckListener(
				mEditPressureVal, mIsSettingsPSI, mContext));

		if (mIsSettingsPSI) {
			mEditPressureVal
					.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
							4, 2) });// PSI 150.00

		} else {
			mEditPressureVal
					.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
							3, 2) });// BAR 10.34
		}

		TextView pressureRecTitle = (TextView) promptsView
				.findViewById(R.id.pressure_rec_title);
		pressureRecTitle.setText(pressureRecHeading);

		final TextView pressureUnitLabel = (TextView) promptsView
				.findViewById(R.id.pressure_unit);
		pressureUnitLabel.setText(pressureUnit);

		final EjobAlertDialog dialogBuilder = new EjobAlertDialog(mContext);

		// set prompts.xml to alertdialog builder
		dialogBuilder.setView(promptsView);

		// save button
		dialogBuilder.setCancelable(false).setPositiveButton(save,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// set user activity
						dialogBuilder.updateInactivityForDialog();

						LogUtil.TraceInfo(TRACE_TAG, "none", "BT - Save", false, false, false);
						// get pressure set on textview
						String pressureSet = mEditPressureVal.getText()
								.toString().trim();
						// if pressure set is empty or null then recreate
						// pressure dialog
						if (TextUtils.isEmpty(pressureSet)
								|| ".".equals(pressureSet)
								|| !CommonUtils
										.pressureValueValidation(mEditPressureVal)) {
							LogUtil.TraceInfo(TRACE_TAG, "none", "Msg : "+Constants.sLblCheckPressureValue, false, false, false);
							CommonUtils.notify(
									Constants.sLblCheckPressureValue, mContext);
							showPressureDialog();
							mEditPressureVal.setEnabled(mEditPressureValEnable);
							return;
						}

						float barValue = 0;
						if (mIsSettingsPSI) {
							// change PSI to BAR
							barValue = CommonUtils.parseFloat(pressureSet) * 0.068947f;
						} else {
							barValue = CommonUtils.parseFloat(pressureSet);
						}

						try {
							String Trace_Data = position.getText().toString()+ " | "
									+ mEditPressureVal.getText().toString()
									+ pressureUnitLabel.getText().toString();
							LogUtil.TraceInfo(TRACE_TAG, "none", "Data : " + Trace_Data, false, false, false);
						}
						catch (Exception e)
						{
							LogUtil.TraceInfo(TRACE_TAG, "none", "Data : Exception "+e.getMessage(), false, false, false);
						}

						dialog.dismiss();
						mEditPressureValEnable = false;
						// call handler method and pass user pressure settings
						// with the current tire position
						currentSaveHandler.updatePressureOnSave(
								currentTireForSwapPos, barValue);
						currentSaveHandler = null;
					}
				});

		// cancel button :: CR 313 requirement - the dialog will be canceled and
		// user will on swap screen itself
		dialogBuilder.setCancelable(false).setNegativeButton(cancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// set user activity
						dialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "none", "BT - Cancel", false, false, false);
						dialog.dismiss();
						mEditPressureValEnable = false;
						currentSaveHandler = null;
					}
				});

		// create alert dialog
		mAlertDialog = dialogBuilder.create();

		// show it
		mAlertDialog.show();

	}
}
