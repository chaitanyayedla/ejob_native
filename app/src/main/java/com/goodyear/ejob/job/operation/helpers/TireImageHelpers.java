package com.goodyear.ejob.job.operation.helpers;

import java.util.ArrayList;

import android.database.Cursor;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;

/**
 * Helper class for tire images captured
 */
public class TireImageHelpers {

	public static boolean checkIfImageExistsWithID(
			ArrayList<TireImageItem> mTireImageItemList, int id,
			DatabaseAdapter mDBAdaptor) {
		// CR 457: check if images are available in db if not then check object
		// if images available in DB
		Cursor imageCursor = mDBAdaptor.getImagesForJobItemId(id);
		if (imageCursor != null && imageCursor.getCount() > 0) {
			return true;
		}
		for (int itrator = 0; itrator < mTireImageItemList.size(); itrator++) {
			if (mTireImageItemList.get(itrator).getJobItemId()
					.equals(String.valueOf(id))) {
				return true;
			}
		}
		return false;
	}

	public static ArrayList<TireImageItem> getTireImageForWithID(
			ArrayList<TireImageItem> mTireImageItemList, int id) {
		ArrayList<TireImageItem> tireImageResult = new ArrayList<>();
		for (int itrator = 0; itrator < mTireImageItemList.size(); itrator++) {
			if (mTireImageItemList.get(itrator).getJobItemId()
					.equals(String.valueOf(id))) {
				tireImageResult.add(mTireImageItemList.get(itrator));
			}
		}
		return tireImageResult;
	}

	public static String getDamgeIDForJobItem(int ID) {
		String idToCheck = String.valueOf(ID);
		String damageID = null;
		for (JobItem jItem : VehicleSkeletonFragment.mJobItemList) {
			if (jItem.getJobId().equals(idToCheck)) {
				damageID = jItem.getDamageId();
				break;
			}
		}
		return damageID;
	}
}
