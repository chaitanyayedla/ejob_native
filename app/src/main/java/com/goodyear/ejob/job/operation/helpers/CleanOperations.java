package com.goodyear.ejob.job.operation.helpers;

import com.goodyear.ejob.fragment.EjobAdditionalServicesFragment;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.util.CameraUtility;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.Job;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.ReservePWTUtils;

/**
 * This class is used to perform clean after an open job is saved.
 */
public class CleanOperations {

	/**
	 * This method is called to clean up the static objects that are used in job
	 * operations.
	 */
	public static void cleanOperationsAfterJob() {
		LogUtil.DBLog("CleanOperations ", "cleanOperationsAfterJob "," start");
		// remove config data
		VehicleSkeletonFragment.mAxleRecommendedPressure = null;
		Retorque.retorqueDone = false;
		Constants.TIRE_INFO = null;
		VehicleSkeletonFragment.tyreInfo = null;
		VehicleSkeletonFragment.mJobItemList = null;
		VehicleSkeletonFragment.mTireImageItemList = null;
		VehicleSkeletonFragment.mJobcorrectionList = null;
		Constants.JOB_ITEMLIST = null;
		Constants.JOB_CORRECTION_LIST = null;
		Constants.vehicleConfiguration = null;
		Constants.JOB_TIREIMAGE_ITEMLIST = null;
		Constants.UPDATE_APPLIED_ON_EDIT = false;
		Retorque.torqueSettings = null;
		Constants.TORQUE_SETTINGS_ARRAY = null;
		Job.clearJobObject();
		EjobAdditionalServicesFragment.sSwitchBtns.clear();
		EjobAdditionalServicesFragment.sServiceValues.clear();
		VehicleSkeletonFragment.mReserveTyreList.clear();
		ReservePWTUtils.mReserveTyreList.clear();
		VehicleSkeletonFragment.resetBoolValue();
		Constants.Search_TM_ListItems.clear();
		Constants.jobIdEdit = null;
		Constants.IS_FORM_ELEMENTS_CLEANED = true;
		Constants.IS_SIGANTURE_DONE = false;
		CameraUtility.resetParams();
	}
}
