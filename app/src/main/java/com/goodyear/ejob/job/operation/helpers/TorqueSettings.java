package com.goodyear.ejob.job.operation.helpers;

/**
 * @author shailesh.p
 * 
 */
public class TorqueSettings {
	private String jobId;
	private String wheelPos;
	private int isWheelRemoved;
	private String sapCodeWP;
	private String wrenchNumber;
	private String noManufacturerTorqueSettingsReason;
	private int torqueSettingsValue;
	private int retorqueLabelIssued;

	/**
	 * @return the jobId
	 */
	public String getJobId() {
		return jobId;
	}

	/**
	 * @param jobId the jobId to set
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	/**
	 * @return the wheelPos
	 */
	public String getWheelPos() {
		return wheelPos;
	}

	/**
	 * @param wheelPos the wheelPos to set
	 */
	public void setWheelPos(String wheelPos) {
		this.wheelPos = wheelPos;
	}

	/**
	 * @return the isWheelRemoved
	 */
	public int getIsWheelRemoved() {
		return isWheelRemoved;
	}

	/**
	 * @param isWheelRemoved the isWheelRemoved to set
	 */
	public void setIsWheelRemoved(int isWheelRemoved) {
		this.isWheelRemoved = isWheelRemoved;
	}

	/**
	 * @return the sapCodeWP
	 */
	public String getSapCodeWP() {
		return sapCodeWP;
	}

	/**
	 * @param sapCodeWP the sapCodeWP to set
	 */
	public void setSapCodeWP(String sapCodeWP) {
		this.sapCodeWP = sapCodeWP;
	}

	/**
	 * @return the wrenchNumber
	 */
	public String getWrenchNumber() {
		return wrenchNumber;
	}

	/**
	 * @param wrenchNumber the wrenchNumber to set
	 */
	public void setWrenchNumber(String wrenchNumber) {
		this.wrenchNumber = wrenchNumber;
	}

	/**
	 * @return the noManufacturerTorqueSettingsReason
	 */
	public String getNoManufacturerTorqueSettingsReason() {
		return noManufacturerTorqueSettingsReason;
	}

	/**
	 * @param noManufacturerTorqueSettingsReason the
	 *        noManufacturerTorqueSettingsReason to set
	 */
	public void setNoManufacturerTorqueSettingsReason(
			String noManufacturerTorqueSettingsReason) {
		this.noManufacturerTorqueSettingsReason = noManufacturerTorqueSettingsReason;
	}

	/**
	 * @return the torqueSettingsValue
	 */
	public int getTorqueSettingsValue() {
		return torqueSettingsValue;
	}

	/**
	 * @param torqueSettingsValue the torqueSettingsValue to set
	 */
	public void setTorqueSettingsValue(int torqueSettingsValue) {
		this.torqueSettingsValue = torqueSettingsValue;
	}

	/**
	 * @return the retorqueLabelIssued
	 */
	public int getRetorqueLabelIssued() {
		return retorqueLabelIssued;
	}

	/**
	 * @param retorqueLabelIssued the retorqueLabelIssued to set
	 */
	public void setRetorqueLabelIssued(int retorqueLabelIssued) {
		this.retorqueLabelIssued = retorqueLabelIssued;
	}
}
