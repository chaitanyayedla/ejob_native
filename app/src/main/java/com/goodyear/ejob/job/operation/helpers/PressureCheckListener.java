
package com.goodyear.ejob.job.operation.helpers;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Switch;

import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;

/**
 * 
 *
 */
public class PressureCheckListener implements TextWatcher {

	private EditText mEditTxtVw;
	private boolean mIsSettingsPSI;
	private Switch mRecValueAdjust;
	private boolean mRecEnable;
	private static Context mContext ;

	public PressureCheckListener(EditText view, boolean isSettingsPSI, Context context) {
		mEditTxtVw = view;
		mIsSettingsPSI = isSettingsPSI;
		mRecEnable=false;
		this.mContext = context;

	}

	/**
	 * Added constructor to control Rec Adjust Switch
	 * @param view view
	 * @param isSettingsPSI pressure setting
	 * @param mRecValueAdjust switch control
	 */
	public PressureCheckListener(EditText view, boolean isSettingsPSI, Switch mRecValueAdjust, Context context) {
		mEditTxtVw = view;
		mIsSettingsPSI = isSettingsPSI;
		this.mRecValueAdjust=mRecValueAdjust;
		mRecEnable=true;
		this.mContext = context;
	}
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		
	}

	
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		String strEnteredVal = mEditTxtVw.getText().toString();
		if (null == strEnteredVal || "".equals(strEnteredVal)) {
			//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
			if(mRecEnable) {
				mRecValueAdjust.setEnabled(false);
			}
			return;
		}
		
		boolean isDecimalAllow = mIsSettingsPSI ? false : true;
		
		if(strEnteredVal.startsWith(".") && !isDecimalAllow) {
			mEditTxtVw.setText("");
			//Rec Adjust can be enabled only for proper pressure value
			//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
			if(mRecEnable) {
				mRecValueAdjust.setEnabled(false);
			}
			return;
		}
		if(strEnteredVal.startsWith(".") && strEnteredVal.length() <= 1) {
			return;
		}
		Double num = Double.valueOf(0);
		try {
			num = CommonUtils.parseDouble(strEnteredVal);
		} catch(NumberFormatException ex) {
			
		}
		if(mIsSettingsPSI) {
			if(strEnteredVal.contains(".")) {
				mEditTxtVw.setText("");
			} else if(num < Constants.PSI_MIN_VALUE) {

				//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
				if(mRecEnable) {
					mRecValueAdjust.setEnabled(false);
				}
				return;
			} else if(num > Constants.PSI_MAX_VALUE || num < Constants.PSI_MIN_VALUE) {

				//Rec Adjust can be enabled only for proper pressure value
				//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
				if(mRecEnable) {
					mRecValueAdjust.setEnabled(false);
				}
				mEditTxtVw.setText("");
				/*Bug 728 : Adding a toast to notify user when higher value of pressure is entered*/
				try {
					CommonUtils.notify(Constants.sLblCheckPressureValue, mContext);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if(num <= Constants.PSI_MAX_VALUE && num >= Constants.PSI_MIN_VALUE)
			{
				//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
				if(mRecEnable) {
					mRecValueAdjust.setEnabled(true);
				}
			}
		} else { // BAR
			if (num > Constants.BAR_MAX_VALUE || num < Constants.BAR_MIN_VALUE) {
				//Rec Adjust can be enabled only for proper pressure value
				//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
				if(mRecEnable) {
					mRecValueAdjust.setEnabled(false);
				}
				mEditTxtVw.setText("");
				/*Bug 728 : Adding a toast to notify user when higher value of pressure is entered*/
				try {
					CommonUtils.notify(Constants.sLblCheckPressureValue, mContext);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if(num <= Constants.BAR_MAX_VALUE && num >= Constants.BAR_MIN_VALUE)
			{
				//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
				if(mRecEnable) {
					mRecValueAdjust.setEnabled(true);
				}
			}
		}
	}

	@Override
	public void afterTextChanged(Editable s) {
		
	}
}
