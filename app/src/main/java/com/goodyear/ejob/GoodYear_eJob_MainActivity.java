package com.goodyear.ejob;

import android.app.AlertDialog;
import android.app.Notification;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.SQLException;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.app.FragmentActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Layout;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/*import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.SimpleShowcaseEventListener;
import com.github.amlcurran.showcaseview.targets.ViewTarget;*/
import com.goodyear.ejob.authenticator.Authentication;
import com.goodyear.ejob.authenticator.HybrisAuthentication;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.halosys.HalosysCallStatus;
import com.goodyear.ejob.halosys.HalosysServiceCall;
import com.goodyear.ejob.halosys.HalosysStatusModel;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.interfaces.AlertDialogHandler;
import com.goodyear.ejob.playstoreupdate.ServerConnection;
import com.goodyear.ejob.service.ClearLogService;
import com.goodyear.ejob.util.Account;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.ConstantHashForDefaultValues;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CustomShowcaseView;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.FileOperations;
import com.goodyear.ejob.util.GYProgressDialog;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.PasswordExpiryNotificationType;
import com.goodyear.ejob.util.SettingsLoader;
import com.goodyear.ejob.util.SharedPreferenceUtil;
import com.goodyear.ejob.util.Utility;
import com.goodyear.ejob.util.Validation;

import org.apache.http.util.TextUtils;
import org.joda.time.DateTime;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import static android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN;
import static android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE;

/**
 * Login Activity
 *
 * @author amitkumar.h
 */
public class GoodYear_eJob_MainActivity extends FragmentActivity implements
        AlertDialogHandler {
    private static final String TAG = GoodYear_eJob_MainActivity.class
            .getSimpleName();

    public static final String SHOW_KEYBOARD = "show_keyboard";
    private EditText mUserNameEdit;
    private EditText mPasswordEdit;
    private EditText mPinEdit;
    private LinearLayout mErrorBoxLinear;
    private LinearLayout mLoginContainerLinear;
    private TextView mErrorMessageText;
    private Button mLoginBtn;

    private String userName = null;
    private String mPassword = null;
    private String mUserUrlIdentifier = null;
    /*#ForHalosys*/
    private String mOfflinePin = null;

    private boolean mIsDBexists;
    private boolean mIsNetworkAvailable;
    private Account mAccount;

    private ImageView mLoadingImage;
    public GYProgressDialog mGYProgressDialog;

    private static AsynCall mAuthAsyncTask;
    private AlertDialog mCrashAlertDialog;
    private boolean mLoginClicked = false;
    JSONObject subjson;
    JSONObject subJsonObj;
    ArrayList<String> dataList;
    AlertDialog alertDialog;
    private static final String SERVER_TIME_OFFSET = "ServerTimeOffset";
    private static final String DEFAULT_OFFSET_TIME = "5";

    /**
     * Trace Info Variables
     */
    private static final String TRACE_INFO = "eJob Main";
    private String Trace_msg = "";
    /**
     * Label displaying the current app version
     */
    private TextView mVesionLabel;
    /**
     * Variable displaying the current app version
     */
    private static String mAppVesion = "";

    /**
     * Variable which stores database path during Login
     */
    private static String mLoginTimeDatabasePath = null;

    /*#ForHalosys */
    private boolean userDbExists;
    private String tempUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Constants.PROJECT_PATH = getExternalFilesDir(null).getAbsolutePath();
        LogUtil.i(TAG, " inside onCreate ");
        userDbExists = false;

        checkIfDBFileExistsFromPreviousVersions();
        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER)
                    && intentAction != null
                    && intentAction.equals(Intent.ACTION_MAIN)) {
                finish();
                return;
            }
        }
        setContentView(R.layout.activity_goodyear_ejob__main);

        ConstantHashForDefaultValues.getWiringLabel();

        // set the edit box for login and username
        if (mAuthAsyncTask != null) {
            mAuthAsyncTask.setActivity(this);
        }
        mUserNameEdit = (EditText) findViewById(R.id.username);

        /*#ForHalosys; Changed length from 12 to 50 and removed all caps filter*/
        mUserNameEdit.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(50)});
        mPasswordEdit = (EditText) findViewById(R.id.password);
        mPinEdit = (EditText) findViewById(R.id.pin);
        mPinEdit.setTypeface(Typeface.DEFAULT_BOLD);

         /*#ForHalosys*/
        if(mUserNameEdit.getText() != null || !TextUtils.isEmpty(mUserNameEdit.getText()) ||
                mUserNameEdit.getText().toString().trim() != ""){
            tempUsername = mUserNameEdit.getText().toString().trim();
        }
        if (mPasswordEdit.getText() != null) {
            mPasswordEdit.getText().clear();
        }
        if (mPinEdit.getText() != null) {
            mPinEdit.getText().clear();
        }

        try {
            userDbExists = checkUserDbExists(tempUsername);
        }catch (Exception e){
            LogUtil.d(TAG," Exception "+e);
        }

        if (!Validation.isNetworkAvailable(this) && userDbExists) {
            LogUtil.d(TAG, ": Network not available; userDbExists = "+userDbExists);
            mPasswordEdit.setVisibility(View.GONE);
            mPinEdit.setVisibility(View.VISIBLE);
        } else {
            LogUtil.d(TAG, ": Network available; userDbExists = "+userDbExists);
            mPinEdit.setVisibility(View.GONE);
            mPasswordEdit.setVisibility(View.VISIBLE);
        }
        mUserNameEdit.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mUserNameEdit.requestFocus();
                Log.i(TAG, " on touch mUserNameEdit ");
                if (!Validation.isNetworkAvailable(getApplicationContext()) && userDbExists
                        && isPinSet(mUserNameEdit.getText().toString().trim())) {
                    LogUtil.d(TAG, " : Network not available, pin is set; userDbExists = "+userDbExists);
                    mPasswordEdit.setVisibility(View.GONE);
                    mPinEdit.setVisibility(View.VISIBLE);
                } else {
                    LogUtil.d(TAG, " : Network available, pin is not set ; userDbExists = "+userDbExists);
                    mPasswordEdit.setVisibility(View.VISIBLE);
                    mPinEdit.setVisibility(View.GONE);
                }
                return false;
            }
        });
        mPasswordEdit.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mPasswordEdit.requestFocus();
                try {
                    userDbExists = checkUserDbExists(mUserNameEdit.getText().toString().trim());
                }catch (Exception e){
                    LogUtil.d(TAG," Exception "+e);
                }
                mPasswordEdit.requestFocus();
                Log.i(TAG, " on touch mPasswordEdit ");
                if ((mPasswordEdit.getVisibility() == View.VISIBLE) && !Validation.isNetworkAvailable(getApplicationContext())
                        && userDbExists && isPinSet(mUserNameEdit.getText().toString().trim())) {
                    LogUtil.d(TAG, " : Network not available, pin is set ; userDbExists = "+userDbExists);
                    mPasswordEdit.setVisibility(View.GONE);
                    mPinEdit.setVisibility(View.VISIBLE);
                } else {
                    LogUtil.d(TAG, " : Network available, pin is not set; userDbExists = "+userDbExists);
                    mPinEdit.setVisibility(View.GONE);
                    mPasswordEdit.setVisibility(View.VISIBLE);
                }
                return false;
            }

        });
        mPinEdit.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mPinEdit.requestFocus();
                try {
                    userDbExists = checkUserDbExists(mUserNameEdit.getText().toString().trim());
                }catch (Exception e){
                    LogUtil.d(TAG," Exception "+e);
                }
                mPinEdit.requestFocus();
                Log.i(TAG, " on touch mPinEdit ");
                if ((mPinEdit.getVisibility() == View.VISIBLE) && !Validation.isNetworkAvailable(getApplicationContext())
                        && userDbExists && isPinSet(mUserNameEdit.getText().toString().trim())) {
                    LogUtil.d(TAG, " : Network not available, pin is set; userDbExists = "+userDbExists);
                    mPasswordEdit.setVisibility(View.GONE);
                    mPinEdit.setVisibility(View.VISIBLE);
                } else {
                    LogUtil.d(TAG, " : Network available, pin is not set ; userDbExists = "+userDbExists);
                    mPinEdit.setVisibility(View.GONE);
                    mPasswordEdit.setVisibility(View.VISIBLE);
                }
                return false;
            }

        });
        TextView textHelp = (TextView) findViewById(R.id.help);
        textHelp.setMovementMethod(LinkMovementMethod.getInstance());

        // make the ui elements transparent
        mLoginContainerLinear = (LinearLayout) findViewById(R.id.login_item_container);
        mLoginContainerLinear.getBackground().setAlpha(125);

        mLoginBtn = (Button) findViewById(R.id.loginbtn);

        // make transparent
        mPasswordEdit.getBackground().setAlpha(125);
        mUserNameEdit.getBackground().setAlpha(125);
        mLoginBtn.getBackground().setAlpha(125);

        // make the error box invisible
        mErrorBoxLinear = (LinearLayout) findViewById(R.id.errorboxcontainer);
        mErrorBoxLinear.setVisibility(LinearLayout.INVISIBLE);

        mErrorMessageText = (TextView) findViewById(R.id.errortext);

        // loading image will be invisible
        mLoadingImage = (ImageView) findViewById(R.id.loading_img);
        mLoadingImage.setVisibility(ImageView.INVISIBLE);

        // label for version number
        mVesionLabel = (TextView) findViewById(R.id.txt_version);
        try {
            PackageInfo pinfo;
            pinfo = getPackageManager().getPackageInfo(
                    getPackageName(), 0);
            mAppVesion = pinfo.versionName;
            mVesionLabel.setText(mAppVesion);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        // no action bar
        getActionBar().hide();

        getWindow().setSoftInputMode(SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (null != savedInstanceState) {
            if (savedInstanceState.getBoolean("loading")) {
                showProgress();
            }

            if (savedInstanceState.getBoolean(SHOW_KEYBOARD)) {
                getWindow().setSoftInputMode(SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }

            if (savedInstanceState.getBoolean("login_clicked")) {
                mLoginClicked = true;
            }

            String errorMsgTxt = savedInstanceState.getString("ErrorMessage");
            if (!TextUtils.isEmpty(errorMsgTxt)) {
                mErrorMessageText.setText(errorMsgTxt);
                mErrorBoxLinear.setVisibility(View.VISIBLE);
                mErrorMessageText.setVisibility(View.VISIBLE);
            }
        }
        // load settings in to shared preferences
        try {
            SettingsLoader.loadSettings(this);
        } catch (Exception e1) {
            CommonUtils.log("login error - "
                    + Constants.ERROR_SETTING_NOT_LOADED);
            Log.e("login error - ", Constants.ERROR_SETTING_NOT_LOADED);
            e1.printStackTrace();
        }

        SharedPreferences preferences = getSharedPreferences(Constants.GOODYEAR_CONF, 0);
        mUserNameEdit.setText(preferences.getString("USER", ""));
        /*#ForHalosys, commented for Sprint1*/
        //checkUpdatesForTranslationSync();

        // start service to clear logs
        Intent clearLogService = new Intent(this, ClearLogService.class);
        startService(clearLogService);

        // PlayStore Update Check
        /* #ForHalosys commenting upgrade check until last release */
        /*if (Validation.isNetworkAvailable(GoodYear_eJob_MainActivity.this)) {
            UpdateCheckAsyncTask updateCheckTask = new UpdateCheckAsyncTask();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                updateCheckTask
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                updateCheckTask.execute("");
            }
        }*/

        /*if(preferences.getBoolean(Constants.FIRST_LOGIN, true)) {
            preferences.edit().putBoolean(Constants.FIRST_LOGIN, false).commit();
            final ViewTarget target = new ViewTarget(R.id.hack_pass, this);
            ShowcaseView view = new ShowcaseView.Builder(this)
                    .setTarget(target)
                    .setContentTitle("Password/PIN")
                    .setContentText("Please enter password while online.\nElse provide offline pin to login")
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setShowcaseDrawer(new CustomShowcaseView(getResources()))
                    .setShowcaseEventListener(new SimpleShowcaseEventListener() {
                        @Override
                        public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                            super.onShowcaseViewDidHide(showcaseView);
                        }
                    })
                    .build();
            view.setDetailTextAlignment(Layout.Alignment.ALIGN_CENTER);
            view.setTitleTextAlignment(Layout.Alignment.ALIGN_CENTER);
            view.forceTextPosition(ShowcaseView.BELOW_SHOWCASE);
        }*/
    }

    /**
     * before starting the check if DB on previous path exists. if it does then
     * move it to new path.
     */
    private void checkIfDBFileExistsFromPreviousVersions() {
        try {
            // check file in phone memory
            String oldPath = Environment.getExternalStorageDirectory()
                    .getAbsoluteFile() + "/com.goodyear.ejob";
            FileOperations.checkAndMoveAllFilesIfExists(oldPath,
                    Constants.PROJECT_PATH);
            FileOperations.clearDir(new File(oldPath));

            // check file at SD card
            String externalStorage = System.getenv("SECONDARY_STORAGE");
            String sdCardList[] = externalStorage.split(":");
            externalStorage = sdCardList[0] + "/com.goodyear.ejob";
            FileOperations.checkAndMoveAllFilesIfExists(externalStorage,
                    Constants.PROJECT_PATH);
            FileOperations.clearDir(new File(externalStorage));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see android.support.v4.app.FragmentActivity#onResume()
     */
    @Override
    protected void onResume() {
        super.onResume();
        LogUtil.d(TAG, " inside onResume ");

       /*#ForHalosys*/
        if (mPasswordEdit.getText() != null) {
            mPasswordEdit.getText().clear();
        }
        if (mPinEdit.getText() != null) {
            mPinEdit.getText().clear();
        }
         /*#ForHalosys*/
        if(mUserNameEdit.getText().toString() != null || !TextUtils.isEmpty(mUserNameEdit.getText().toString()) ||
                mUserNameEdit.getText().toString().trim() != ""){
            tempUsername = mUserNameEdit.getText().toString().trim();
        }
        try{
            userDbExists = checkUserDbExists(tempUsername);
        }catch (Exception e){
            LogUtil.d(TAG, " Exception "+e);
        }

        if (!Validation.isNetworkAvailable(this) && userDbExists) {
            LogUtil.d(TAG, " : Network not available; userDbExists = "+userDbExists);
            mPasswordEdit.setVisibility(View.GONE);
            mPinEdit.setVisibility(View.VISIBLE);
        } else {
            LogUtil.d(TAG, " : Network available ; userDbExists = "+userDbExists);
                    mPinEdit.setVisibility(View.GONE);
            mPasswordEdit.setVisibility(View.VISIBLE);
        }
        mUserNameEdit.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mUserNameEdit.requestFocus();
                Log.i(TAG, " on touch mUserNameEdit ");
                if (!Validation.isNetworkAvailable(getApplicationContext()) && userDbExists
                        && isPinSet(mUserNameEdit.getText().toString().trim())) {
                    LogUtil.d(TAG, " : Network not available, pin is set; userDbExists = "+userDbExists);
                    mPasswordEdit.setVisibility(View.GONE);
                    mPinEdit.setVisibility(View.VISIBLE);
                } else {
                    LogUtil.d(TAG, " : Network available, pin is not set; userDbExists = "+userDbExists);
                    mPasswordEdit.setVisibility(View.VISIBLE);
                    mPinEdit.setVisibility(View.GONE);
                }
                return false;
            }
        });
        mPasswordEdit.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mPasswordEdit.requestFocus();
                try {
                    userDbExists = checkUserDbExists(mUserNameEdit.getText().toString().trim());
                }catch (Exception e){
                    LogUtil.d(TAG," Exception "+e);
                }
                Log.i(TAG, " on touch mPasswordEdit");
                if ((mPasswordEdit.getVisibility() == View.VISIBLE) && !Validation.isNetworkAvailable(getApplicationContext())
                        && userDbExists && isPinSet(mUserNameEdit.getText().toString().trim())) {
                    LogUtil.d(TAG, " : Network not available, pin is set; userDbExists = "+userDbExists);
                    mPasswordEdit.setVisibility(View.GONE);
                    mPinEdit.setVisibility(View.VISIBLE);
                } else {
                    LogUtil.d(TAG, " : Network available, pin is not set; userDbExists = "+userDbExists);
                    mPinEdit.setVisibility(View.GONE);
                    mPasswordEdit.setVisibility(View.VISIBLE);
                }
                return false;
            }

        });
        mPinEdit.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mPinEdit.requestFocus();
                try {
                    userDbExists = checkUserDbExists(mUserNameEdit.getText().toString().trim());
                } catch (Exception e) {
                    LogUtil.d(TAG, " Exception " + e);
                }
                Log.i(TAG, " on touch mPinEdit ");
                if ((mPinEdit.getVisibility() == View.VISIBLE) && !Validation.isNetworkAvailable(getApplicationContext())
                        && userDbExists && isPinSet(mUserNameEdit.getText().toString().trim())) {
                    LogUtil.d(TAG, " : Network not available, pin is set; userDbExists = " + userDbExists);
                    mPasswordEdit.setVisibility(View.GONE);
                    mPinEdit.setVisibility(View.VISIBLE);
                } else {
                    LogUtil.d(TAG, " : Network available, pin is not set; userDbExists = " + userDbExists);
                    mPinEdit.setVisibility(View.GONE);
                    mPasswordEdit.setVisibility(View.VISIBLE);
                }
                return false;
            }

        });

        if (!mLoginClicked) {
            Intent i = getIntent();
            if (i.getExtras() != null) {
                String value = i.getExtras().getString("CRASH_STATUS");
                if (!TextUtils.isEmpty(value)) {
                    createDialogOnCrash();
                }
            }
        }
    }

    /**
     * login process start
     *
     * @param view
     */
    public void login(View view) {

        // save data when app crash in any screen and display all
        // records...Johnmiya
        mLoginClicked = true;
        // SharedPreferences preferences = getSharedPreferences(
        // Constants.GOODYEAR_CONF, 0);
        // saveJobsWhenAppKillsForcefully(preferences.getString("USER", null));

        // make error box disappear after next login try
        mErrorBoxLinear.setVisibility(LinearLayout.INVISIBLE);

        userName = mUserNameEdit.getText().toString();
        mPassword = mPasswordEdit.getText().toString();
        mOfflinePin = mPinEdit.getText().toString();

        // user name should be upper case
        /*#ForHalosys; removed toUppercase()*/
        userName = userName.trim();

        // to check the url selected
        mUserUrlIdentifier = userName;

        // set the loged in user.
        Constants.USER = userName;
        Constants.VALIDATE_USER = Constants.USER;

        //To Delete Log files based on the Last modified Date
        try {
            ArrayList<File> logFolders = new ArrayList<>();
            logFolders.add(new File(Constants.PROJECT_PATH + "/logs"));
            logFolders.add(new File(Constants.PROJECT_PATH + "/DBLogs"));
            logFolders.add(new File(Constants.PROJECT_PATH + "/TraceInfo"));

            if (FileOperations.calculateMultipleFolderSize(logFolders) > Constants.LogSizeLimit) {
                //To Delete files based on Last modified Date... if size is more than the limit
                FileOperations.deleteFilesFromMultipleFolderToFitForGivenSize(logFolders, Constants.LogSizeLimit);
            }
        } catch (Exception e) {
            LogUtil.i(TAG, "deleteLog - msg : " + e.getMessage());
        }

        //Login Trace Information
        try {
            Trace_msg = "\n\t\tUsername : " + Constants.USER + " , ";
            Trace_msg = Trace_msg + CommonUtils.getDeviceInfo(this);
            LogUtil.TraceInfo(TRACE_INFO, "Login", Trace_msg, true, true, true);
        } catch (Exception e) {
            //LogUtil.TraceInfo(TRACE_INFO, "Login", e.getMessage());
        }

        // verify and prevalidate
        try {
            if (canVerifyUserAndLogin()) {

                /* #ForHalosys, commenting for Sprint1*/
                // start next activity
                /*Intent startEjobList = new Intent(this, SyncActivity.class);
                startEjobList.putExtra("sentfrom", "login");
                Constants.SAVEEXTDBWHENAPPSAVE = false;
                startActivity(startEjobList);*/

                launchEJobLandingPage();

            } else {
                // loadingScreen.setVisibility(RelativeLayout.INVISIBLE);
            }
        } catch (Exception e) {
            CommonUtils.log("Login" + e.toString());
            Log.e("Login", e.toString());
        }

        /*if(mPasswordEdit.getText() != null || !TextUtils.isEmpty(mPasswordEdit.getText())) {
            mPasswordEdit.getText().clear();
        }
        if(mPinEdit.getText() != null || !TextUtils.isEmpty(mPinEdit.getText())) {
            mPinEdit.getText().clear();
        }*/
        LinearLayout mainLayout;
        mainLayout = (LinearLayout) findViewById(R.id.login_item_container);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
    }

    /**
     * verify user and start the login process
     *
     * @return true if verified , false if not
     * @throws Exception
     */
    public boolean canVerifyUserAndLogin() throws Exception {

        SharedPreferences sharedPref = getSharedPreferences(Constants.FIRST_TIME_LOGIN_PREFERENCE, getApplicationContext().MODE_PRIVATE);
        // check if db exists
        mIsDBexists = dbExists();


        if (mIsDBexists) {
            //db size
            try {
                SharedPreferences sharedPreference = getSharedPreferences(Constants.AUTO_LOGOUT_PREFERENCE, getApplicationContext().MODE_PRIVATE);
                Boolean isAutoLogout = sharedPref.getBoolean(Constants.AUTO_LOGOUT_KEY + "_" + Constants.VALIDATE_USER, false);
                LogUtil.d(TAG, " isAutoLogout boolean : " + isAutoLogout);
                File tempFile = new File(mLoginTimeDatabasePath);
                LogUtil.i(TAG, "DB-SIZE : " + String.valueOf(tempFile.length()));
                if (isAutoLogout) {
                    LogUtil.d(TAG, " Suspect a Db crash, recovering Db file");
                    recoverFullDb();
                }
                /*SharedPreferences.Editor sharedEdit = sharedPref.edit();
                sharedEdit.putBoolean(Constants.AUTO_LOGOUT_KEY + "_" + Constants.VALIDATE_USER, false);
                sharedEdit.commit();*/
                /*Display free memory space just for information*/
                StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
                long bytesAvailableExt = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
                long megaAvailableExt = bytesAvailableExt / (1024 * 1024);

                StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
                long bytesAvailableInt = (long) statFs.getBlockSize() * (long) statFs.getAvailableBlocks();
                long megaAvailableInt = bytesAvailableInt / (1024 * 1024);

                LogUtil.d(TAG, " External:Available free space in MB : " + megaAvailableExt);
                LogUtil.d(TAG, " Internal:Available free space in MB : " + megaAvailableInt);
            } catch (Exception e) {
                LogUtil.i(TAG, " Exception : " + e.getMessage());
            }
        } else if (!mIsDBexists) {
            LogUtil.d(TAG, " DB wont exist, adding in shared pref");
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(Constants.FIRST_TIME_LOGIN_KEY + "_" + Constants.VALIDATE_USER, true);
            editor.commit();
        }

        // check if network available
        mIsNetworkAvailable = Validation.isNetworkAvailable(this);

  /*      if (Constants.USER.equalsIgnoreCase("")
                || mPassword.equalsIgnoreCase("")) {
            setErrorMessage(Constants.PASSWORD_EMPTY);
            return false;
        }*/

        if (!mIsDBexists && !mIsNetworkAvailable) {
            setErrorMessage(Constants.ERROR_NODB_NONETWORK);
            return false;
        }

        if (mIsNetworkAvailable) {

            try {
                LogUtil.i(TAG, "Network Available; DB-SIZE : " + String.valueOf(new File(mLoginTimeDatabasePath).length()));
            } catch (Exception e) {
                LogUtil.e(TAG, "Exception " + e);
            }
            //getting the db exists status and storing to constant IsDBExistBeforeLogin
            if (!mIsDBexists) {
                Constants.IsDBExistBeforeLogin = false;
                Constants.APP_UPGRADE_STATUS = false;
                LogUtil.i(TAG, "Constants.APP_UPGRADE_STATUS = false");
            } else {
                Constants.IsDBExistBeforeLogin = true;
            }
            // if found in environment
            /*#ForHalosys , commenting identifyEnvironment()*/
            //identifyEnvironment();
           /* if (!TextUtils.isEmpty(Constants._URL)
                    && !Constants._URL.equals("not found")) {*/

            LogUtil.i(TAG, "mAuthAsyncTask");
            String serverTime = Constants._URL + "/ServerTime";

            HalosysServiceCall.initializeSession(this);

            mAuthAsyncTask = new AsynCall(this);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                mAuthAsyncTask.executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                mAuthAsyncTask.execute();
            }
            return false;
            /*} else {
                setErrorMessage(Constants.ERROR_NO_SERVERULR);
                // this situation should not come
                CommonUtils.notify(Constants.ERROR_NO_SERVERULR, this);
                Log.e("login --", Constants.ERROR_NO_SERVERULR);
                return false;
            }*/
        } else {

            LogUtil.i(TAG, "no wifi");
            //getting the db exists status and storing to constant IsDBExistBeforeLogin
            if (!mIsDBexists) {
                Constants.IsDBExistBeforeLogin = false;
                LogUtil.i(TAG, "Constants.IsDBExistBeforeLogin = false");
            } else {
                Constants.IsDBExistBeforeLogin = true;
            }

            int result = canVerifyUserNameAndPassword();
            if (result == 0) {
                setErrorMessage(Constants.PASSWORD_INCORRECT);
                return false;
            } else if (result == 2) {
                setErrorMessage(Constants.ERROR_CONNECTION_WITH_SERVER);
                return false;
            }

            File tempFile = new File(mLoginTimeDatabasePath);
            LogUtil.i(TAG, "DB-SIZE : " + String.valueOf(tempFile.length()));
            /*boolean isdateExpired = DateTimeUTC.isDateOlderThanToday(mAccount
                    .getPassExpirationDate());
            // check if password expired
            if (isdateExpired) {
                setErrorMessage(Constants.ERROR_PASSWORD_EXPIRED);
                return false;
            } else {
                // check and notify when password will expire
                passwordExpiryMessage();
            }
            mAccount.saveAccountToDB(this);
            */
        }
        return true;
    }

    /**
     * Notify user of password expiration.
     *
     * @return was the dialog displayed
     */
    private boolean passwordExpiryMessage() {
        try {
            LogUtil.i(TAG, " passwordExpiryMessage() ; DB-SIZE : " + String.valueOf(new File(mLoginTimeDatabasePath).length()));
        } catch (Exception e) {
            LogUtil.e(TAG, " Exception : " + e);
        }
        boolean dialogDisplayed = false;
        if (null == mAccount) {
            return dialogDisplayed;
        }
        long daydifference = DateTimeUTC
                .daysBetweenCurDateTimeNgivenDateTime(mAccount
                        .getPassExpirationDate());
        if ((14 == daydifference)
                && (mAccount.getPassNotificationType() != PasswordExpiryNotificationType.TwoWeeks)) {
            dialogDisplayed = true;
            mAccount.setPassNotificationType(PasswordExpiryNotificationType.TwoWeeks);
            CommonUtils.createInformationDialog(
                    GoodYear_eJob_MainActivity.this,
                    Constants.PASSWORD_EXPIRE_14_DAYS, this);
        }
        if ((10 == daydifference)
                && (mAccount.getPassNotificationType() != PasswordExpiryNotificationType.TenDays)) {
            dialogDisplayed = true;
            mAccount.setPassNotificationType(PasswordExpiryNotificationType.TenDays);
            CommonUtils.createInformationDialog(
                    GoodYear_eJob_MainActivity.this,
                    Constants.PASSWORD_EXPIRE_10_DAYS, this);
        }
        if ((7 == daydifference)
                && (mAccount.getPassNotificationType() != PasswordExpiryNotificationType.OneWeek)) {
            dialogDisplayed = true;
            mAccount.setPassNotificationType(PasswordExpiryNotificationType.OneWeek);
            CommonUtils.createInformationDialog(
                    GoodYear_eJob_MainActivity.this,
                    Constants.PASSWORD_EXPIRE_7_DAYS, this);
        }
        if ((5 == daydifference)
                && (mAccount.getPassNotificationType() != PasswordExpiryNotificationType.FiveDays)) {
            dialogDisplayed = true;
            mAccount.setPassNotificationType(PasswordExpiryNotificationType.FiveDays);
            CommonUtils.createInformationDialog(
                    GoodYear_eJob_MainActivity.this,
                    Constants.PASSWORD_EXPIRE_5_DAYS, this);
        }
        if ((3 == daydifference)
                && (mAccount.getPassNotificationType() != PasswordExpiryNotificationType.ThreeDays)) {
            dialogDisplayed = true;
            mAccount.setPassNotificationType(PasswordExpiryNotificationType.ThreeDays);
            CommonUtils.createInformationDialog(
                    GoodYear_eJob_MainActivity.this,
                    Constants.PASSWORD_EXPIRE_3_DAYS, this);
        }
        if ((2 == daydifference)
                && (mAccount.getPassNotificationType() != PasswordExpiryNotificationType.TwoDays)) {
            dialogDisplayed = true;
            mAccount.setPassNotificationType(PasswordExpiryNotificationType.TwoDays);
            CommonUtils.createInformationDialog(
                    GoodYear_eJob_MainActivity.this,
                    Constants.PASSWORD_EXPIRE_2_DAYS, this);
        }
        if ((1 == daydifference)
                && (mAccount.getPassNotificationType() != PasswordExpiryNotificationType.OneDay)) {
            dialogDisplayed = true;
            mAccount.setPassNotificationType(PasswordExpiryNotificationType.OneDay);
            CommonUtils.createInformationDialog(
                    GoodYear_eJob_MainActivity.this,
                    Constants.PASSWORD_EXPIRE_1_DAYS, this);
        }
        return dialogDisplayed;
    }

    /**
     * when user is verified from server the login credentials should be updated
     * in the db
     *
     * @param localAccount
     * @return was the login credentials updated
     * @throws UnsupportedEncodingException encryption/decryption
     */
    private boolean canUpdateAcountRecord(Account localAccount)
            throws Exception {

        LogUtil.i(TAG, " Inside canUpdateAcountRecord ");
        if (null != localAccount) {
            localAccount.setPassword(mAccount.getPassword());

            LogUtil.i(TAG, "canUpdateAcountRecord() - localAccount pass exp - " + localAccount.getPassExpirationDate());
            LogUtil.i(TAG, "canUpdateAcountRecord() - mAccount pass exp - " + mAccount.getPassExpirationDate());

            localAccount
                    .setPassExpirationDate(mAccount.getPassExpirationDate());
            mAccount = localAccount;
            return true;
        } else {
            return false;
        }
    }

    /**
     * identify the environment to use based on user name D = dev, C =
     * consolidation
     */
    public void identifyEnvironment() {
        LogUtil.i(TAG, "Inside identifyEnvironment ; DB-SIZE : " + String.valueOf(new File(mLoginTimeDatabasePath).length()));
        // check which environment to use
        SharedPreferences preferences = getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);

        if (mUserUrlIdentifier.endsWith("D")) {
            Constants._URL = preferences
                    .getString("DevServiceURL", "not found");
        } else if (mUserUrlIdentifier.endsWith("C")) {
            Constants._URL = preferences.getString("ConsServiceURL",
                    "not found");
        } else {
            Constants._URL = preferences.getString("ProdServiceURL",
                    "not found");
        }
    }

    /**
     * update user name as last logged in saved settings after successful login
     */
    private void updateLastUserNameInSettings() {
        LogUtil.i(TAG, "Inside updateLastUserNameInSettings; Constants.USER : " + Constants.USER.toString());
        // update last user in settings
        HashMap<String, String> unameUpdate = new HashMap<>();
        unameUpdate.put("USER", Constants.USER);
        SettingsLoader.updateSettings(unameUpdate, this);
    }

    /**
     * Verify User name and Password
     *
     * @return true if verified, false if not
     * @throws Exception
     */
    public int canVerifyUserNameAndPassword() throws Exception {

        File tempFile = new File(mLoginTimeDatabasePath);
        LogUtil.i(TAG, "in canVerifyUserNameAndPassword; DB-SIZE : " + String.valueOf(tempFile.length()));

        // Validate the user name and password correct order
        if (/*Validation.verifyUserName(Authentication
                .getUserName(Constants.USER))
                && Validation.verifyPassword(mPassword)*/true) {
            // if validated verify the password and user name

            /*#ForHalosys , #ToDo create new column pin and use here to authenticate offline*/
            if (!mIsNetworkAvailable) {
                /*Object dbResponse = Authentication.verifyUserFromDB(
                        Constants.USER, mOfflinePin, this);*/
                Object dbResponse = HybrisAuthentication.verifyUserFromDB(
                        Constants.USER, mOfflinePin, this);
                // if error message returned
                if (dbResponse instanceof String) {
                    mPinEdit.getText().clear();
                    if(mIsNetworkAvailable){
                        return 0;//username/password wrong
                    }else {
                        return 2;//no network
                    }
                }
                // else account message returned
                mAccount = (Account) dbResponse;
                checkIfNoUserFound();
                updateLastUserNameInSettings();
                mPinEdit.getText().clear();
                return 1;
            } else {

                // Object serverResponse = Authentication.verifyUserFromServer(Constants.USER, mPassword, this);
                Object serverResponse = HybrisAuthentication.verifyUserFromHybrisServer(Constants.USER, mPassword, this);
                LogUtil.i(TAG, "canVerifyUserNameAndPassword() - serverResponse - " + serverResponse);
                LogUtil.i(TAG, "canVerifyUserNameAndPassword() - serverResponse(tostring) - " + serverResponse.toString());

             /*   // if error message returned
                if (serverResponse instanceof String) {
                    // setErrorMessage((String) dbResponse);
                    if (serverResponse == Constants.ERROR_CONNECTION_WITH_SERVER)
                        return 2;
                    else
                        return 0;
                } */

                //Error Information
                if(serverResponse instanceof HalosysStatusModel)
                {
                    switch (((HalosysStatusModel) serverResponse).getsCode())
                    {
                        case HalosysCallStatus.CODE_DENIED_ACCESS_TO_RESOURCE :
                            return 2;
                        case HalosysCallStatus.CODE_FAILURE:
                            return 2;
                        case HalosysCallStatus.CODE_TOKEN_NULL:
                            return 2;
                        case HalosysCallStatus.CODE_UNKNOWN_ERROR:
                            return 2;
                        default:
                            return 2;
                    }

                }

                LogUtil.i(TAG, "before Account update; DB-SIZE : " + String.valueOf(tempFile.length()));
                // else account message returned
                mAccount = (Account) serverResponse;

                LogUtil.i(TAG, "canVerifyUserNameAndPassword() - mAccount password exp - " + mAccount.getPassExpirationDate());

                checkIfNoUserFound();

                LogUtil.i(TAG, "after checkIfNoUserFound; DB-SIZE : " + String.valueOf(tempFile.length()));

                updateLastUserNameInSettings();
                LogUtil.i(TAG, "after updateLastUserNameInSettings; DB-SIZE : " + String.valueOf(tempFile.length()));

                if (!mIsDBexists) {
                    DatabaseAdapter db = new DatabaseAdapter(this);
                    DatabaseAdapter.createNewUserDB();
                    db.close();
                }
                LogUtil.i(TAG, "after create new user db; DB-SIZE : " + String.valueOf(tempFile.length()));

                // search the account in db
                Account localAccount = Account.getAccountFromDB(this);
                LogUtil.i(TAG, "after search the account in db; DB-SIZE : " + String.valueOf(tempFile.length()));

                if (null == localAccount) {
                    DatabaseAdapter execute = new DatabaseAdapter(this);
                    execute.deleteDB();
                    execute.close();
                    execute.createDatabase();
                    execute.open();
                    //CR-447 DB upgrade
                    execute.doUpgrade(this);
                    /*Old type */
                    /*execute.insertAccount(
                            Authentication.getUserName(Constants.USER),
                            mPassword, mAccount.getPassExpirationDate(), "0",
                            PasswordExpiryNotificationType.None);*/

                    execute.insertAccountWithPin(
                            Authentication.getUserName(Constants.USER), "0",
                            PasswordExpiryNotificationType.None);
                    execute.getValues();
                    execute.copyFinalDB();
                    execute.close();
                    try {
                        LogUtil.i(TAG, "inside  if (null == localAccount) ; DB-SIZE : " + String.valueOf(tempFile.length()));
                    } catch (Exception e) {
                        LogUtil.e(TAG, " Exception " + e);
                    }

                    /*Translation block commented. Right now getting info from database
                    try
                    {
                        String mLangCode="EN";
                        Object mTranslationTes=HalosysServiceCall.getTranslationBasedOnLangCode(this,mLangCode);
                    }
                    catch(Exception e)
                    {
                        LogUtil.i(TAG, "Update Translation : Exception " + e.getMessage());
                    }*/
                }
                // module with user name
                else {
                  /*  canUpdateAcountRecord(localAccount);
                    try {
                        LogUtil.i(TAG, "after canUpdateAcountRecord ; DB-SIZE : " + String.valueOf(tempFile.length()));
                    } catch (Exception e) {
                        LogUtil.e(TAG, " Exception " + e);
                    }*/
                }
                return 1;
            }
        } else {
            return 0;
        }
    }

    /**
     * set error message in the error box
     *
     * @param message
     */
    public void setErrorMessage(String message) {
        mPasswordEdit.setText("");
        mErrorMessageText.setText(message);
        mErrorBoxLinear.setVisibility(LinearLayout.VISIBLE);
    }

    /**
     * to check if account returned is null or not (if null then no user found
     * by that userid)
     *
     * @throws Exception no user found
     */
    public void checkIfNoUserFound() throws Exception {
        if (null == mAccount) {
            setErrorMessage(Constants.ERROR_NO_USER_FOUND);
        }
    }

    /**
     * check if DB exists
     *
     * @return true if DB exists, false if not
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public boolean dbExists() throws Exception {
        LogUtil.d(TAG, " dbExists!.... Constants.USER : "+Constants.USER.toString());
        SharedPreferences preferences = getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);
        if (preferences.getString(Constants.DATABASE_LOCATION, "not found")
                .equals(Constants.EXTERNAL_DB_LOCATION)) {
            String sdCardPath = System.getenv("SECONDARY_STORAGE");
            try {
                StatFs stat = new StatFs(sdCardPath);
                // if sd card is not available
                if (0 == stat.getAvailableBlocks()) {
                    return canChangeSDCardLocationInSettings();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return canChangeSDCardLocationInSettings();
            }

            String sdCardList[] = sdCardPath.split(":");
            String path = sdCardList[0]
                    + Constants.PROJECT_PATH_FOR_BELOW_KITKAT;

            FileOperations.moveFile(Constants.PROJECT_PATH + "/"
                    + Constants.USER + ".db3", path, Constants.USER + ".db3");
            /*Variable to stores database path during Login*/
            mLoginTimeDatabasePath = path + "/" + Constants.USER + ".db3";

            Log.d(TAG," 1 db exists : "+fileExist(path + "/" + Constants.USER + ".db3"));
            return fileExist(path + "/" + Constants.USER + ".db3");
        }
        // phone memory
        else {
            String path = Constants.PROJECT_PATH + "/" + Constants.USER
                    + ".db3";
            /*Variable to stores database path during Login*/
            mLoginTimeDatabasePath = path;
            Log.d(TAG," 2 db exists : "+fileExist(path));
            return fileExist(path);
        }
    }

    /**
     * change SD card location in settings xml and shared preferences
     *
     * @return true if DB location can be changed false if not
     */
    public boolean canChangeSDCardLocationInSettings() {
        // if sd card is not available
        HashMap<String, String> dbLocationUpdate = new HashMap<>();
        dbLocationUpdate.put("DatabaseLoc", "Phone Memory");
        SettingsLoader.updateSettings(dbLocationUpdate, this);

        // return phone memory path
        String path = Constants.PROJECT_PATH + "/" + Constants.USER + ".db3";
        return fileExist(path);
    }

    /**
     * check if given file exists in phone
     *
     * @param path Path to file
     * @return true if exist otherwise false
     */
    private boolean fileExist(String path) {
        File tempFile = new File(path);
        System.out.println(tempFile.getAbsolutePath());
        return tempFile.exists();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("loading", isProgressShowing());
        outState.putBoolean("login_clicked", mLoginClicked);
        if (mErrorBoxLinear.getVisibility() == View.VISIBLE) {
            outState.putString("ErrorMessage", mErrorMessageText.getText()
                    .toString());
        }
        boolean isFocused = mUserNameEdit.isFocused()
                || mPasswordEdit.isFocused();
        outState.putBoolean(SHOW_KEYBOARD, isFocused);
    }

    private boolean isProgressShowing() {
        return (mGYProgressDialog != null && mGYProgressDialog.isShowing());
    }

    private void showProgress() {
        dismissProgress();
        mGYProgressDialog = new GYProgressDialog(this);
        mGYProgressDialog.show();
    }

    private void dismissProgress() {
        if (mGYProgressDialog != null && mGYProgressDialog.isShowing()) {
            mGYProgressDialog.dismiss();
        }
        mGYProgressDialog = null;
    }

    /**
     * to make async call to the server
     *
     * @author shailesh.p
     */
    private static class AsynCall extends AsyncTask<String, Void, String> {

        private WeakReference<GoodYear_eJob_MainActivity> mActivityWeakReference;

        public AsynCall(GoodYear_eJob_MainActivity activity) {
            mActivityWeakReference = new WeakReference<>(activity);
        }

        @Override
        protected void onPreExecute() {
            GoodYear_eJob_MainActivity activity = getActivity();
            activity.showProgress();
            activity.mPasswordEdit.setEnabled(false);
            activity.mUserNameEdit.setEnabled(false);
        }

        @Override
        protected void onPostExecute(String code) {

            getActivity().onAuthPostExecute(code);

        }

        /**
         * Returns 0 if Phone date does not match with server, returns 1 if
         * password expired returns 2 if success, 3 if password incorrect
         *
         * @return code for status of login
         */
        @Override
        protected String doInBackground(String... url) {
//            String serverUrl = url[0];
//            String serverTime = null;
//            HttpClient client = new DefaultHttpClient();
//            HttpResponse response;
//            HttpGet request = new HttpGet(serverUrl);
//            BufferedReader rd = null;

            try {
//                /* #ForHalosys , commented the following code*/
//                Log.d(TAG, " onCreate; initialize session");
//                Authentication.attemptApplicationVerification(this.getActivity(), getActivity().userName, getActivity().mPassword);
//
//                Object myCObj;
//                final ISession session = Session.getInstance();
//                Map<String, Object> queryParams = Maps.newHashMap();
//                queryParams.put("country", "gb");
//                queryParams.put("licenseplate", "*");
//                queryParams.put("format", "json");
//                myCObj = session.executeFunction("VehicleSearchFn", queryParams, false);
//                Log.v(TAG, myCObj.toString());
//                LogUtil.d(TAG, " .. .. "+ myCObj.toString());
//
//                response = client.execute(request);
//                rd = new BufferedReader(new InputStreamReader(response
//                        .getEntity().getContent()));
//                serverTime = rd.readLine();
//                if (0 != getActivity().verifyDate(serverTime)) {
//                    return "0"; // device date wrong
//                } else {
                // verify using network
                return getActivity().startVerification();
//               }
//                if (isTokenValid) {
//                    return getActivity().startVerification();
//                } else {
//                    return "6";
//                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Login--", "server not Accessible " + url[0]);
                getActivity().mIsNetworkAvailable = false;
                if (getActivity().mIsDBexists) {
                    return getActivity().startVerification();
                } else if (Utility.isProxyConnected(e, getActivity())) {
                    return "7";
                } else {
                    return "6"; // server not available
                }
            }
        }

        public void setActivity(GoodYear_eJob_MainActivity activity) {
            mActivityWeakReference = new WeakReference<>(activity);
        }

        private GoodYear_eJob_MainActivity getActivity() {
            return mActivityWeakReference.get();
        }
    }

    /**
     * start entered password and user name verification with server if server
     * ping fails then verify from DB
     */
    private String startVerification() {
        try {
            /* #ForHalosys , #ToDo */
            // if credentials verified then login.
            int result = canVerifyUserNameAndPassword();
            LogUtil.i(TAG, "startVerification() - Result - " + result);
            if (result == 1) {
                // get expiry date from account
//                boolean isdateExpired = DateTimeUTC
//                        .isDateOlderThanToday(mAccount.getPassExpirationDate());
//                // check if password expired
//                if (isdateExpired) {
//                    LogUtil.i(TAG, "startVerification() - isdateExpired - " + isdateExpired);
//                    return "1";
//                }
                Account aTest = Account.getAccountFromDB(this);
                String x = aTest.getLogin();
                return "2"; // validation successful
            } else if (result == 0) {
                return "3"; // validation failed
            } else {
                return "7";
            }
        } catch (Exception e) {
            e.printStackTrace();
            mIsNetworkAvailable = false;
            if (mIsDBexists) {
                return startVerification();
            } else {
                return "6";
            }
        }
    }

    /**
     * verify date with server
     *
     * @param dateFromServer server date
     * @return 0 if same day
     */
    private int verifyDate(String dateFromServer) {
        String[] dayInfo = dateFromServer.split(" ");
        String[] timeInfo = dayInfo[3].split(":");
        String timeOffset = SharedPreferenceUtil.getStringValueFromSharedPref(
                GoodYear_eJob_MainActivity.this, SERVER_TIME_OFFSET,
                DEFAULT_OFFSET_TIME);
        int offsetTimeInt = Integer.parseInt(timeOffset);
        // get server date in date object
        DateTime serverDate = DateTimeUTC
                .getDate(Integer.parseInt(dayInfo[0]),
                        Integer.parseInt(dayInfo[1]),
                        Integer.parseInt(dayInfo[2]),
                        Integer.parseInt(timeInfo[0]),
                        Integer.parseInt(timeInfo[1]), 0);
        // get current date
        DateTime currentDate = DateTimeUTC.getCurrentJodaDate();
        // get date with max offset
        DateTime maxOffsetServerDate = DateTimeUTC.getDateWithMinituesOffset(
                offsetTimeInt, serverDate, true);
        // get date with min offset
        DateTime minOffsetServerDate = DateTimeUTC.getDateWithMinituesOffset(
                offsetTimeInt, serverDate, false);
        int maxOffsetResult = DateTimeUTC.compareDates(minOffsetServerDate,
                currentDate);
        int minOffsetResult = DateTimeUTC.compareDates(currentDate,
                maxOffsetServerDate);
        LogUtil.i(TAG, "server date -- " + serverDate + " min offset date -- "
                + minOffsetServerDate + " max offset date -- "
                + maxOffsetServerDate + " current date -- " + currentDate
                + "\t maxOffsetResult - " + maxOffsetResult + "\t minOffsetResult - " + minOffsetResult);
        if (maxOffsetResult <= 1 && minOffsetResult <= 1) {
            return 0;
        } else {
            return 1;
        }
    }

    private void onAuthPostExecute(String code) {
        dismissProgress();
        LogUtil.d(TAG, "inside onAuthPostExecute; code : " + code.toString());
        mPasswordEdit.getText().clear();
        mPinEdit.getText().clear();
        mPasswordEdit.setEnabled(true);
        mUserNameEdit.setEnabled(true);
        mPinEdit.setEnabled(true);
        int result = 6;
        try {
            result = Integer.parseInt(code);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        switch (result) {
            case 0:
                setErrorMessage(Constants.ERROR_INCORRECT_DATE_ON_DEVICE);
                break;
            case 1:
                setErrorMessage(Constants.ERROR_PASSWORD_EXPIRED);
                break;
            case 2:
                /*#ForHalosys , commenting for Sprint1*/
                // check and notify when password will expire
                boolean dialogDisplayed = passwordExpiryMessage();
                // save the account to db
                if (mAccount != null) {
                    mAccount.saveAccountToDB(getBaseContext());
                }
                if (!dialogDisplayed) {
                    startEjobListActivity(this);
                }
                //startEjobListActivity(this);

               // launchEJobLandingPage();
                break;
            case 3:
                setErrorMessage(Constants.PASSWORD_INCORRECT);
                return;
            // break;
            case 5:
                setErrorMessage(Constants.PASSWORD_EMPTY);
                break;
            case 6:
                setErrorMessage(Constants.ERROR_SERVER_DOWN_ERROR);
                break;
            case 7:
                setErrorMessage(Constants.ERROR_CONNECTION_WITH_SERVER);
                break;
            default:
                setErrorMessage(Constants.ERROR_SERVER_DOWN_ERROR);
                break;
        }
    }

    // all jobs copied into device data when app kills forcefully
    void saveJobsWhenAppKillsForcefully(String userNameLastLogin) {
        DatabaseAdapter db = new DatabaseAdapter(this);
        try {
            db.copyFinalDB(userNameLastLogin);
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * start ejob list activity
     *
     * @param context
     */
    private void startEjobListActivity(Context context) {
        Intent startEjobList = new Intent(context, SyncActivity.class);
        startEjobList.putExtra("sentfrom", "login");

        context.startActivity(startEjobList);
    }

    /**
     *
     */
    @Override
    public void onClickOK(Context context) {
        /* #ForHalosys */
        startEjobListActivity(context);
       // launchEJobLandingPage();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            LogUtil.d("GoodYear_ejob_MainActivity", "OnDestroy()");
            dismissProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create Dialog for OnAction back pressed
     */
    private void createDialogOnCrash() {
        LayoutInflater li = LayoutInflater
                .from(GoodYear_eJob_MainActivity.this);
        View promptsView = li.inflate(R.layout.activity_crash_dialog, null);
        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
                GoodYear_eJob_MainActivity.this);
        alertDialogBuilder.setView(promptsView);
        // update user activity for dialog layout
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });

        // Setting The Info
        TextView infoTv = (TextView) promptsView.findViewById(R.id.crash_msg);
        infoTv.setText("Something went wrong...Please Login Again!!");
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // update user activity on button click
                        alertDialogBuilder.updateInactivityForDialog();

                        mCrashAlertDialog.dismiss();
                    }
                });

        // create alert dialog
        mCrashAlertDialog = alertDialogBuilder.create();
        // show it
        mCrashAlertDialog.show();
        mCrashAlertDialog.setCanceledOnTouchOutside(false);

    }

    private void checkUpdatesForTranslationSync() {
        boolean nodesAvailableInXmlFile = true;
        SharedPreferences preferences = getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);
        String versionFromSetting = (preferences
                .getString("appversioncode", ""));
        int versionCodeFromSettingsFile;
        try {
            versionCodeFromSettingsFile = Integer.parseInt(versionFromSetting);
        } catch (NumberFormatException e) {
            nodesAvailableInXmlFile = createMissingXmlNodesInSettingsFile();
            versionCodeFromSettingsFile = 0;
        }
        if (nodesAvailableInXmlFile) {
            try {
                PackageInfo pinfo;
                pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                int mVersionName = pinfo.versionCode;
                if (versionCodeFromSettingsFile < mVersionName) {
                    HashMap<String, String> settingsUpdatedHashMap = new HashMap<>();
                    settingsUpdatedHashMap.put("LastAppUpgradeDateTime", String
                            .valueOf(DateTimeUTC.getMillisecondsFromUTCdate()));
                    settingsUpdatedHashMap.put("appversioncode",
                            String.valueOf(mVersionName));

                    SettingsLoader.updateSettings(settingsUpdatedHashMap,
                            GoodYear_eJob_MainActivity.this);
                }
            } catch (NameNotFoundException e) {
                LogUtil.e(TAG,
                        "could not retrive app version from menifest file", e);
            }
        }
    }

    /**
     * This method creates XML nodes in settings file when APP is updated and
     * settings does not create required XML nodes for update.
     */
    private boolean createMissingXmlNodesInSettingsFile() {
        try {
            SettingsLoader.createMissingNodesForVersionUpdate();
            return true;
        } catch (Exception e) {
            LogUtil.e(TAG, "could not create xml nodes in settings file", e);
            return false;
        }
    }

    private class UpdateCheckAsyncTask extends
            AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {

            String Str = goingForServerConnection();

            return Str;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject jsonArray = new JSONObject(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    String playStoreVersion = jsonArray.getString("version");
                    String secondIDFromPS = playStoreVersion.substring(2, 3);
                    String thirdIDFromPS = playStoreVersion.substring(4, 6);
                    String fourthIDFromPS = playStoreVersion.substring(7);
                    PackageInfo pinfo;
                    try {
                        pinfo = getPackageManager().getPackageInfo(
                                getPackageName(), 0);
                        String versionNumber = pinfo.versionName;
                        String secondVN = versionNumber.substring(2, 3);
                        String thirdVN = versionNumber.substring(4, 6);
                        String fourthVN = versionNumber.substring(7);
                        if (Integer.parseInt(secondVN) >= Integer
                                .parseInt(secondIDFromPS)
                                && Integer.parseInt(thirdVN) >= Integer
                                .parseInt(thirdIDFromPS)
                                && Integer.parseInt(fourthVN) >= Integer
                                .parseInt(fourthIDFromPS)) {
                            LogUtil.i(TAG, "Application is upto-Date"
                                    + playStoreVersion);
                        } else {
                            if (Integer.parseInt(secondVN) == Integer
                                    .parseInt(secondIDFromPS)) {
                                loadUpdateDialog();
                                return;
                            } else {
                                LogUtil.i(TAG,
                                        "ForceUpdate Dialog called");
                                loadForceUpdateDialog();
                                return;
                            }
                        }
                    } catch (NameNotFoundException e) {
                        LogUtil.i(TAG,
                                "Unable to get the version from App Manifest");
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                LogUtil.i(TAG,
                        "Unable to connect for playSore version check");
                e.printStackTrace();
            }
        }
    }

    public String goingForServerConnection() {
        String resultString = "";
        try {
            JSONObject jobj = new JSONObject();
            System.out.println("------------ >>> JSON : " + jobj.toString());
            resultString = ServerConnection.Connection(jobj.toString());
        } catch (Exception e) {
            System.out.println("Exception Caughtin Json PARSING");
        }
        return resultString;
    }

    /**
     * Method to show update dialog
     */
    private void loadUpdateDialog() {
        try {
            // get prompts.xml view
            LayoutInflater li = LayoutInflater
                    .from(GoodYear_eJob_MainActivity.this);
            View promptsView = li.inflate(R.layout.dialog_update_confirmation,
                    null);
            final AlertDialog.Builder updateDialogBuilder = new AlertDialog.Builder(
                    GoodYear_eJob_MainActivity.this);
            updateDialogBuilder.setView(promptsView);

            TextView mValue_update_msg = (TextView) promptsView
                    .findViewById(R.id.update_msg);
            mValue_update_msg
                    .setText("An Update is Available. Please Install!");
            updateDialogBuilder.setCancelable(false).setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW,
                                        Uri.parse("market://details?id="
                                                + "com.goodyear.ejob")));
                            } catch (Exception ex) {
                                startActivity(new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(Constants.EJOB_DOWNLOAD_URL)));
                                Log.i(TAG, "Couldn't find the app on market");
                            }
                            alertDialog.dismiss();
                        }
                    });

            //Bug 653 : Make sure major release upgrade are blocking old version of ejob to login
            //
            updateDialogBuilder.setCancelable(false).setNegativeButton(
                    "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            alertDialog.dismiss();
                        }
                    });
            // create alert dialog
            alertDialog = updateDialogBuilder.create();
            // show it
            alertDialog.show();
            alertDialog.setCanceledOnTouchOutside(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to show force update Dialog
     */
    private void loadForceUpdateDialog() {
        try {
            // get prompts.xml view
            LayoutInflater li = LayoutInflater
                    .from(GoodYear_eJob_MainActivity.this);
            View promptsView = li.inflate(R.layout.dialog_update_confirmation,
                    null);
            final AlertDialog.Builder updateDialogBuilder = new AlertDialog.Builder(
                    GoodYear_eJob_MainActivity.this);
            updateDialogBuilder.setView(promptsView);

            TextView mValue_update_msg = (TextView) promptsView
                    .findViewById(R.id.update_msg);
            mValue_update_msg
                    .setText("An Update is Available. Please Install!");
            updateDialogBuilder.setCancelable(false).setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW,
                                        Uri.parse("market://details?id="
                                                + "com.goodyear.ejob")));
                            } catch (Exception ex) {
                                startActivity(new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(Constants.EJOB_DOWNLOAD_URL)));
                                Log.i(TAG, "Couldn't find the app on market");
                            }
                            alertDialog.dismiss();
                        }
                    });

            //Bug 653 : Make sure major release upgrade are blocking old version of ejob to login
            //
//			updateDialogBuilder.setCancelable(false).setNegativeButton(
//					"Cancel", new DialogInterface.OnClickListener() {
//						@Override
//						public void onClick(DialogInterface dialog, int id) {
//							alertDialog.dismiss();
//						}
//					});
            // create alert dialog
            alertDialog = updateDialogBuilder.create();
            // show it
            alertDialog.show();
            alertDialog.setCanceledOnTouchOutside(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void recoverFullDb() {
        LogUtil.i(TAG, " Inside recoverFullDb()");
        try {
            String DestinationFileName = mLoginTimeDatabasePath;
            String SourceFileName = Constants.PROJECT_PATH + Constants.FULLDBBackUp_DIR + Constants.USER + ".db3";

            File file = new File(DestinationFileName);
            if (file.exists()) {
                LogUtil.d(TAG, " delete the file ");
                file.delete();
            }
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
                LogUtil.d(TAG, " create new file");
            }

            LogUtil.i(TAG, " SourceFileName : " + SourceFileName);
            LogUtil.i(TAG, " DestinationFileName : " + DestinationFileName);

            LogUtil.i(TAG, " Recovering full Db backup : Start :");
            //Copy source to Destination
            FileOperations.copyFile(SourceFileName, DestinationFileName);

            LogUtil.i(TAG, " Recovering full Db backup : End :");

        } catch (Exception e) {
            LogUtil.i(TAG, "Exception - " + e.getMessage());
        }

    }

    /**
     * This method launch's EjobList Activity
     */
    private void launchEJobListActivity() {
        finish();
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, EjobList.class);
        startActivity(intent);
    }

    private void launchEJobLandingPage() {
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, EjobList.class);
        startActivity(intent);
    }

    /**
     * check if DB exists
     *
     * @return true if DB exists, false if not
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public boolean checkUserDbExists(String username) throws Exception {
        LogUtil.d(TAG, " checkUserDbExists!.... username : " + username);
        SharedPreferences preferences = getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);
        if (preferences.getString(Constants.DATABASE_LOCATION, "not found")
                .equals(Constants.EXTERNAL_DB_LOCATION)) {
            String sdCardPath = System.getenv("SECONDARY_STORAGE");
            try {
                StatFs stat = new StatFs(sdCardPath);
                // if sd card is not available
                if (0 == stat.getAvailableBlocks()) {
                    return canChangeSDCardLocationInSettings();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return canChangeSDCardLocationInSettings();
            }

            String sdCardList[] = sdCardPath.split(":");
            String path = sdCardList[0]
                    + Constants.PROJECT_PATH_FOR_BELOW_KITKAT;

            FileOperations.moveFile(Constants.PROJECT_PATH + "/"
                    + username + ".db3", path, username + ".db3");
            /*Variable to stores database path during Login*/
            mLoginTimeDatabasePath = path + "/" + username + ".db3";

            Log.d(TAG," 1 db exists : "+fileExist(path + "/" + username + ".db3"));
            return fileExist(path + "/" + username + ".db3");
        }
        // phone memory
        else {
            String path = Constants.PROJECT_PATH + "/" + username
                    + ".db3";
            /*Variable to stores database path during Login*/
            mLoginTimeDatabasePath = path;
            Log.d(TAG," 2 db exists : "+fileExist(path));
            return fileExist(path);
        }
    }

    boolean isPinSet(String userName){
        LogUtil.d(TAG,":: inside isPinSet() ");
        boolean pinSet = false;
        try{
            DatabaseAdapter dbAdapter = new DatabaseAdapter(this);
            dbAdapter.createDatabase();
            dbAdapter.open();
            if(dbAdapter.isOfflinePinSet(userName) == 1){
                pinSet = true;
            }
            dbAdapter.close();
        }catch (Exception e){
            pinSet = false;
            LogUtil.d(TAG, " "+e);
        }
        LogUtil.d(TAG, "ispinset :: "+pinSet);
        return pinSet;
    }
}
