package com.goodyear.ejob.dbhelpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.LogUtil;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

public class UserDBHandler {

	private static final String DBNAME = "user";
	private static final int DBVERSION = 1;
	private static String DB_PATH;
	private DataBaseHelper mDBHelper;
	private SQLiteDatabase mDB;
	private Context mContext;

	// Copy the database from assets
	public void copyDataBase() throws IOException {

		LogUtil.i("UserDBHandler", " Inside copyDataBase ");
		String copyTO = getPathForDBFile();

		File db3file = new File(copyTO);
		if (!db3file.exists())
			db3file.createNewFile();

		String inFileName = mDB.getPath();

		InputStream mInput = new FileInputStream(inFileName);
		OutputStream mOutput = new FileOutputStream(db3file);

		byte[] mBuffer = new byte[64];
		int mLength;
		while ((mLength = mInput.read(mBuffer)) > 0) {
			mOutput.write(mBuffer, 0, mLength);
		}
		mOutput.flush();
		mOutput.close();
		mInput.close();
	}

	// this function will move out from here
	public String getPathForDBFile() {

		SharedPreferences preferences = mContext.getSharedPreferences(
				Constants.GOODYEAR_CONF, 0);

		if (preferences.getString(Constants.DATABASE_LOCATION, "not found")
				.equals(Constants.EXTERNAL_DB_LOCATION)) {
			String externalStorage = System.getenv("SECONDARY_STORAGE");
			String sdCardList[] = externalStorage.split(":");
			return sdCardList[0] + Constants.PROJECT_PATH_FOR_BELOW_KITKAT
					+ "/" + Constants.USER + ".db3";
		}
		// else phone memory
		else {
			return Constants.PROJECT_PATH + "/" + Constants.USER + ".db3";
		}
	}

	/**
	 * constructor
	 * @param ctx context of application
	 */
	public UserDBHandler(Context ctx) {
		mDBHelper = new DataBaseHelper(ctx);
		mContext = ctx;
		DB_PATH = ctx.getPackageCodePath() + ctx.getPackageName()
				+ "/databases/";
	}

	/**
	 * create android table
	 */
	public void clearAndroidMetadata() {
		mDB.execSQL("Drop Table " + "android_metadata");
		mDB.execSQL("VACUUM");
	}

	/**
	 * open DB
	 */
	public void openDB() {
		LogUtil.i("UserDBHandler", "Inside openDB ");
		mDB = mDBHelper.getWritableDatabase();
		mDB.rawQuery("PRAGMA page_size=1024", null);
		mDB.execSQL("VACUUM");
	}

	/**
	 * open DB
	 */
	public void openDBForLoadingResponse() {
		mDB = mDBHelper.getWritableDatabase();
	}

	/**
	 * close DB
	 */
	public void closeDB() {
		mDBHelper.close();
	}

	/**
	 * create tables for new user
	 */
	public void createTablesInDB() {
		LogUtil.i("UserDBHandler", " Inside createTablesInDB ");

		try {
			BufferedReader br = null;
			AssetManager assetManager = mContext.getAssets();
			InputStream in = null;
			in = assetManager.open(Constants.NEW_USER_DB_QUERYFILE);
			br = new BufferedReader(new InputStreamReader(in));
			String line = null;
			String queryString = null;
			while (null != (line = br.readLine())) {
				queryString = line;
			}
			String[] query = queryString.split("GO");
			for (int i = 0; i < query.length; i++) {
				String queryToExecute = query[i];
				queryToExecute.trim();
				mDB.execSQL(queryToExecute);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private class DataBaseHelper extends SQLiteOpenHelper {
		public DataBaseHelper(Context context) {
			super(context, DBNAME, null, DBVERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		}
	}

}
