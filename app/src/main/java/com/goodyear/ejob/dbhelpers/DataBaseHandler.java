package com.goodyear.ejob.dbhelpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.goodyear.ejob.authenticator.Authentication;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.LogUtil;

/**
 * Prepare snapshot before sync
 * 
 * @author amitkumar.h
 * 
 */
public class DataBaseHandler {

	public static final String DBNAME = "snapshot";
	private static final int DBVERSION = 1;
	private static String DB_PATH;
	private DataBaseHelper mDBHelper;
	private SQLiteDatabase mDB;
	private Context mContext;

	public DataBaseHandler(Context ctx) {
		
		mDBHelper = new DataBaseHelper(ctx);
		//DB_PATH = "/data/data/" + ctx.getPackageName() + "/databases/";
		
		DB_PATH = ctx.getDatabasePath(DBNAME).getPath();
		mContext = ctx;
		
	}

	// Copy the database from assets
	public void copyDataBase() throws IOException {
		clearAndroidMetadata();
		String copyTO = Constants.PROJECT_PATH + "/temp";
		File tempDir = new File(copyTO);
		if (!tempDir.exists())
			tempDir.mkdir();
		File db3file = new File(copyTO, "/snapshot.db");
		if (!db3file.exists())
			db3file.createNewFile();
		//String inFileName = DB_PATH + DBNAME;
		String inFileName = DB_PATH ;
		InputStream mInput = new FileInputStream(inFileName);
		OutputStream mOutput = new FileOutputStream(db3file);
		copyStreams(mInput, mOutput);
	}

	// this function will move out from here
	@SuppressLint("NewApi")
	public String getPathForDBFile() {
		SharedPreferences preferences = mContext.getSharedPreferences(
				Constants.GOODYEAR_CONF, 0);
		if (preferences.getString(Constants.DATABASE_LOCATION, "not found")
				.equals(Constants.EXTERNAL_DB_LOCATION)) {
			int SDK_INT = android.os.Build.VERSION.SDK_INT;
			if (SDK_INT >= 19) {
				return mContext.getExternalFilesDirs(null)[1].getAbsolutePath()
						+ "/" + Constants.USER + ".db3";
			} else {
				String externalStorage = System.getenv("SECONDARY_STORAGE");
				String sdCardList[] = externalStorage.split(":");
				return sdCardList[0] + Constants.PROJECT_PATH_FOR_BELOW_KITKAT
						+ "/" + Constants.USER + ".db3";
			}
		}
		// else phone memory
		else {
			return Constants.PROJECT_PATH + "/" + Constants.USER + ".db3";
		}
	}

	private void copyStreams(InputStream mInput, OutputStream mOutput)
			throws IOException {
		byte[] mBuffer = new byte[64];
		int mLength;
		while ((mLength = mInput.read(mBuffer)) > 0) {
			mOutput.write(mBuffer, 0, mLength);
		}
		mOutput.flush();
		mOutput.close();
		mInput.close();
	}

	public void deleteDatabaseAfterLoad() {
		mContext.deleteDatabase(DBNAME);
	}

	public void openDB() {
		mDB = mDBHelper.getWritableDatabase();
		mDB.rawQuery("PRAGMA page_size=1024", null);
		mDB.execSQL("VACUUM");
	}

	public void closeDB() {
		mDBHelper.close();
	}

	public void updateSession(String jobRefNumber, String user, String lang) {
		try {
			String sql = "UPDATE Session SET Language = \"" + lang
					+ "\"  WHERE ID = 1";
			mDB.execSQL(sql);
		} catch (SQLException mSQLException) {
			Log.e("DataBaseHandler",
					"getTestData >>" + mSQLException.toString());
		}
	}

	public void updateSession(String jobRefNumber, String user, String lang,
			String session) {
		try {
			String sql = "UPDATE Session SET Language = \"" + lang
					+ "\"  WHERE ID = 1";
			mDB.execSQL(sql);
		} catch (SQLException mSQLException) {
			Log.e("DataBaseHandler",
					"getTestData >>" + mSQLException.toString());
		}
	}

	public void updateSession(String jobRefNumber, String user) {
		try {
		} catch (SQLException mSQLException) {
			Log.e("DataBaseHandler",
					"getTestData >>" + mSQLException.toString());
		}
	}

	public void clearAndroidMetadata() {
		try {
			mDB.execSQL("Drop Table " + "android_metadata");
		}
		catch(Exception e)
		{
			Log.i("DataBaseHandler","Drop Table - android_metadata : "+e.getMessage());
		}
		try {
			mDB.execSQL("VACUUM");
		}
		catch (Exception e)
		{
			Log.i("DataBaseHandler","VACUUM : "+e.getMessage());
		}
	}

	public void createTablesInDB() {
		try {
			mDB.execSQL("ATTACH DATABASE '" + getPathForDBFile() + "' AS mdb");
			AssetManager assetManager = mContext.getAssets();
			InputStream in = null;
			in = assetManager.open("query.sql");
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line = null;
			String queryString = null;
			while (null != (line = br.readLine())) {
				queryString = line;
			}
			String[] query = queryString.split("GO");
			for (int i = 0; i < query.length; i++) {
				try {
					String queryToExecute = query[i];
					queryToExecute.trim();
					mDB.execSQL(queryToExecute);
				}
				catch(Exception e)
				{
					LogUtil.i("createTablesInDB","Query : "+query[i].toString()+", Exception : "+e.getMessage());
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class DataBaseHelper extends SQLiteOpenHelper {
		public DataBaseHelper(Context context) {
			super(context, DBNAME, null, DBVERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		}
	}

}
