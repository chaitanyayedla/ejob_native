package com.goodyear.ejob.dbhelpers;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.util.Log;

import com.goodyear.ejob.sync.Constant;
import com.goodyear.ejob.util.Account;
import com.goodyear.ejob.util.ApplicationVersionDifference;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.ConstantHashForDefaultValues;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.Job;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.Security;
import com.goodyear.ejob.util.Settings;
import com.goodyear.ejob.util.SyncTime;
import com.goodyear.ejob.util.TyreConstants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used for initialize database for creation , insertion, fetch values
 * from DBHelper
 * 
 * @author johnmiya.s modified Shailesh.p
 * 
 */
public class DatabaseAdapter {
	protected static final String TAG = "DataAdapter";

	private static Context mContext = null;
	private SQLiteDatabase mDb;
	private DataBaseHelper mDbHelper;

	public DatabaseAdapter(Context context) {
		mContext = context;
		mDbHelper = new DataBaseHelper(mContext);
	}

	public DatabaseAdapter createDatabase() throws SQLException {
		LogUtil.i(TAG, " Inside createDatabase ");
		try {
			mDbHelper.createDataBase();
		} catch (IOException mIOException) {
			Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
			LogUtil.i(TAG, mIOException.getMessage() + "  UnableToCreateDatabase");
			throw new Error("UnableToCreateDatabase");
		}
		return this;
	}

	public DatabaseAdapter open() throws SQLException {
		LogUtil.i(TAG, " Inside open ");
		try {
			mDbHelper.openDataBase();
			// mDbHelper.close();
			mDb = mDbHelper.getmDataBase();
			// mDb = mDbHelper.getReadableDatabase();
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			LogUtil.i(TAG, "open >>" + mSQLException.getMessage());
			throw mSQLException;
		}
		catch (Exception e)
		{
			LogUtil.i(TAG, "open >>" + e.getMessage().toString());
		}
		return this;
	}

	/**
	 * get Sync Time info for the given user
	 * 
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public SyncTime getSyncTimeInfo() {
		SyncTime syncTime = new SyncTime();
		try {
			// String sql1 =
			// "SELECT * FROM AA15882.sqlite_master WHERE type='table';";
			String sql = "SELECT * from SyncTime where ID = '" + 1 + "';";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (!mCur.moveToFirst()) {
				return null;
			}
			if (mCur != null && mCur.moveToFirst()) {
				syncTime.setValue(mCur.getString(mCur.getColumnIndex("Value")));
				// syncTime.setUiTranslation(mCur.getString(mCur
				// .getColumnIndex("UITransaltionRequired")));
				syncTime.setSyncRequired(mCur.getString(mCur
						.getColumnIndex("SyncRequired")));
			}

		} catch (Exception mSQLException) {
			Log.e("TestAdapter--",
					"get sync info failed " + mSQLException.toString());
			// CommonUtils.log("Test Adapter " +
			// Constants.ERROR_SETTING_NOT_LOADED);
		}
		return syncTime;
	}

	/**
	 * get Sync Time info for the given user
	 * 
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public Settings getSettingsInfo() {
		Settings settings = new Settings();
		/* #ForHalosys , Commenting it for Sprint1 */
		/*try {
			// String sql1 =
			// "SELECT * FROM AA15882.sqlite_master WHERE type='table';";
			String sql = "SELECT * from Settings where ID = '" + 1 + "';";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (!mCur.moveToFirst()) {
				return null;
			}
			if (mCur != null && mCur.moveToFirst()) {
				settings.setLogin(mCur.getString(mCur.getColumnIndex("Login")));
				settings.setLanguage(mCur.getString(mCur
						.getColumnIndex("Language")));
				settings.setLastServerDate(mCur.getString(mCur
						.getColumnIndex("LastServerDate")));
			}

		} catch (Exception mSQLException) {
			Log.e("TestAdapter--",
					"get sync info failed " + mSQLException.toString());
			// CommonUtils.log("Test Adapter " +
			// Constants.ERROR_SETTING_NOT_LOADED);
		}*/
		settings.setLogin(" ");
		settings.setLanguage("EN");
		settings.setLastServerDate("636125571451906000");
		return settings;
	}

	public DatabaseAdapter copyTempDB() throws SQLException, IOException {
		try {
			mDbHelper.copyDataBaseToPhone();
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			throw mSQLException;
		}
		return this;
	}

	public DatabaseAdapter copyFinalDB(String... userName) throws SQLException,
			IOException {
		try {
			mDbHelper.copyUserDBToPhone(userName);
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			LogUtil.i(TAG, "copyFinalDB >>" + mSQLException.getMessage());
			throw mSQLException;
		}
		return this;
	}

	public DatabaseAdapter deleteDB() throws SQLException, IOException {
		LogUtil.i(TAG, " Inside deleteDB ");
		try {
			mDbHelper.deleteDataBase();
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			LogUtil.i(TAG, "deleteDB >>" + mSQLException.getMessage());
			throw mSQLException;
		}
		return this;
	}

	public DatabaseAdapter copyResponseDB() throws SQLException, IOException {
		try {
			mDbHelper.copyDataBase(Constants.USER + "_");
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			throw mSQLException;
		}
		return this;
	}

	/**
	 * * shailesh update session after login
	 * 
	 * @param account
	 * @throws Exception
	 */
	public void updateSession(String jobRefNumber, String user)
			throws Exception {

		try {
			String sql = "UPDATE Session SET jobRefNumber = \"" + jobRefNumber
					+ "\",UserID=\"" + user + "\" WHERE ID = 1";
			mDb.execSQL(sql);
			// mDb.update(table, values, whereClause, whereArgs)
		} catch (SQLException mSQLException) {
			CommonUtils.log(mSQLException.toString());
			Log.e(TAG, "cannot update session info" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public void updateEJobForm(String cellarName, String cellarPhone,
			String defectAuthNo, String driverName) throws Exception {
		try {
			String sql = "UPDATE Job SET CallerName = \"" + cellarName
					+ "\",CallerPhone=\"" + cellarPhone
					+ "\",DefectAuthNumber=\"" + defectAuthNo
					+ "\",DriverName=\"" + driverName + "\" WHERE ID = '"
					+ Constants.GETSELECTEDLISTPOSITION + "' ";
			mDb.execSQL(sql);
		} catch (SQLException mSQLException) {
			CommonUtils.log(mSQLException.toString());
			Log.e(TAG, "cannot update session info" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * * shailesh update Sync time after sync
	 * 
	 * @param account
	 * @throws Exception
	 */
	public void updateSyncTime(long lastSyncTime) {

		try {
			String sql = "UPDATE SyncTime SET Value = \"" + lastSyncTime
					+ "\" WHERE ID = 1";
			mDb.execSQL(sql);
		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"DB Adapter" + "cannot update sync time"
							+ mSQLException.toString());
			LogUtil.i(TAG,
					"DB Adapter" + "cannot update sync time"
							+ mSQLException.toString());
		}
	}

	public DatabaseAdapter createEmptyTables(String path) throws SQLException,
			IOException {
		try {
			mDbHelper.createTablesInDB(path);
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			throw mSQLException;
		}
		return this;
	}

	/**
	 * check if data is received in translation sync
	 * 
	 * @param path
	 *            path to response file
	 * @return true if data received otherwise false
	 */
	public boolean checkIfDataRecieved(String path, String selectedLanguage) {
		return mDbHelper.isDataReceived(path, selectedLanguage);
	}

	public DatabaseAdapter updateTranslationDBTables(String path)
			throws SQLException, IOException {
		try {
			mDbHelper.updateTablesInDB(path,
					Constants.UPDATE_TRANSLATION_FILENAME);
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			throw mSQLException;
		}
		return this;
	}

	public DatabaseAdapter updateDBTablesAfterSync(String path)
			throws SQLException, IOException {
		try {
			mDbHelper.updateTablesInDB(path, Constants.UPDATESYNCFILENAME);
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			LogUtil.i(TAG, "updateDBTablesAfterSync >>" + mSQLException.getMessage());
			throw mSQLException;
		}
		return this;
	}

	public DatabaseAdapter updateDBTables(String path) throws SQLException,
			IOException {
		try {
			mDbHelper.updateTablesInDB(path, Constants.UPDATEUPLOADFILENAME);
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			throw mSQLException;
		}
		return this;
	}

	public DatabaseAdapter updateTyreTable(String path) throws SQLException{
		try{
			mDbHelper.updateTyreTableInDB(path);
		}catch (SQLException mSQLException){
			Log.e(TAG, "open >>" + mSQLException.toString());
			throw mSQLException;
		}
		return this;
	}

	public void close() {
		LogUtil.i(TAG, " Inside close ");
		try {
			mDbHelper.close();
		} catch (Exception e) {
			LogUtil.i("Close",e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * *shailesh* for getting labels
	 * 
	 * @param idNumber
	 * @return label from db
	 */
	public synchronized String getLabel(int idNumber) {
		String label = "";
		Cursor labelCursor = null;
		// For Retaining the labels before downloading DB
		ArrayList<String> mMissingIDlist = new ArrayList<String>();
		mMissingIDlist.add("1");
		mMissingIDlist.add("294");
		mMissingIDlist.add("297");
		mMissingIDlist.add("298");
		mMissingIDlist.add("403");
		mMissingIDlist.add("424");
		mMissingIDlist.add("438");
		mMissingIDlist.add("439");
		mMissingIDlist.add("443");
		mMissingIDlist.add("469");
		mMissingIDlist.add("505");
		mMissingIDlist.add("292");
		mMissingIDlist.add("992");
		mMissingIDlist.add("995");

		try {
			String sql = "SELECT Value FROM Translation WHERE ID = '"
					+ idNumber + "';";
			labelCursor = mDb.rawQuery(sql, null);
			if (CursorUtils.isValidCursor(labelCursor)
					&& labelCursor.moveToFirst()) {
				label = labelCursor.getString(0);
				if (TextUtils.isEmpty(label)) {
					// ConstantHashForDefaultValues.getWiringLabel();
					label = idNumber + ": MISSING LABEL"; // ConstantHashForDefaultValues.labelHashMap.get(idNumber)
					// For Retaining the labels before downloading DB
					for (int i = 0; i < mMissingIDlist.size(); i++) {
						if (String.valueOf(idNumber).equalsIgnoreCase(
								mMissingIDlist.get(i))) {
							label = ConstantHashForDefaultValues.labelHashMap
									.get(idNumber);
						}
					}
					/*
					 * if (idNumber == 1 || idNumber == 294 || idNumber == 297
					 * || idNumber == 298 || idNumber == 403 || idNumber == 424
					 * || idNumber == 438 || idNumber == 439 || idNumber == 443)
					 * { }
					 */
				} else {
					label = labelCursor.getString(0);
				}
			} else {
				// ConstantHashForDefaultValues.getWiringLabel();
				label = idNumber + ": MISSING LABEL"; // ConstantHashForDefaultValues.labelHashMap.get(idNumber);
				// For Retaining the labels before downloading DB
				for (int i = 0; i < mMissingIDlist.size(); i++) {
					if (String.valueOf(idNumber).equalsIgnoreCase(
							mMissingIDlist.get(i))) {
						label = ConstantHashForDefaultValues.labelHashMap
								.get(idNumber);
					}
				}
			}
		} catch (Exception sqlException) {
			LogUtil.e(TAG, "Exception in getLabel::" + sqlException.toString());
			sqlException.printStackTrace();
			label = ConstantHashForDefaultValues.labelHashMap.get(idNumber);
		} finally {
			CursorUtils.closeCursor(labelCursor);
		}
		if (label == null) {
			label = "";
		}
		return label;
	}

	/**
	 * * Update Selected Language from settings
	 * 
	 * @param lang
	 * @throws Exception
	 */
	public void updateSelectedLanguage(String lang) throws Exception {
		try {
			String sql = "UPDATE Settings SET Language = \"" + lang
					+ "\" WHERE ID = 1";
			mDb.execSQL(sql);
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTestData >>" + mSQLException.toString());
			CommonUtils.log("Test Adapter "
					+ Constants.ERROR_SETTING_NOT_LOADED);
			throw mSQLException;
		}
	}

	/**
	 * update account after login
	 * 
	 * @param account
	 * @throws Exception
	 */
	public void updateAccount(Account account) throws Exception {
		String uname = account.getLogin();
		String pass = Security.encryptMessage(account.getPassword());
		long passExpiry = account.getPassExpirationDate();
		long passNotificationType = account.getPassNotificationType();

		try {
			String sql = "UPDATE Account SET Login = \"" + uname + "\",Pass=\""
					+ pass + "\",PassExpirationDate=\"" + passExpiry
					+ "\",PassNotificationType=\"" + passNotificationType
					+ "\" WHERE ID = 1";
			mDb.execSQL(sql);
		} catch (SQLException mSQLException) {
			CommonUtils.log(mSQLException.toString());
			Log.e(TAG, "getTestData >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * * update account after login
	 * 
	 * @param account
	 * @throws Exception
	 */
	public void insertAccount(String uname, String password, Long passExpiry,
			String jobRefNumber, int noficationType) throws Exception {
		password = Security.encryptMessage(password);
		String sql = "Insert OR REPLACE into Account values ('1','" + uname
				+ "','" + password + "','" + jobRefNumber + "','" + passExpiry
				+ "','" + noficationType + "')";
		try {
			mDb.execSQL(sql);
		} catch (SQLException mSQLException) {
			Log.e("TestAdapter--",
					"insert account failed " + mSQLException.toString());
		}
	}

	/**
	 * * this is test method for checking update
	 */
	public void getValues() {
		try {
			String sql = "Select * from Account";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.getString(0);
				mCur.getString(1);
				mCur.getString(2);
				mCur.getString(3);
				mCur.getString(4);
				mCur.getString(5);
			}
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTestData >>" + mSQLException.toString());
			CommonUtils.log("Test Adapter "
					+ Constants.ERROR_SETTING_NOT_LOADED);
			throw mSQLException;
		}
	}

	/**
	 * get account info for the given user
	 * 
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public Account getAccountInfo(String username) {
		Account userAccount = null;
		try {
			// String sql1 =
			// "SELECT * FROM AA15882.sqlite_master WHERE type='table';";
			String sql = "SELECT * from Account where Login = '" + username
					+ "';";
			Cursor mCur = mDb.rawQuery(sql, null);

			if (!mCur.moveToFirst()) {
				return null;
			}
			if (mCur != null && mCur.moveToFirst()) {
				userAccount = new Account();
				userAccount.setLogin(mCur.getString(mCur
						.getColumnIndex("Login")));
				String encryptedPass = mCur.getString(mCur
						.getColumnIndex("Pass"));
				userAccount.setPassword(Security.decryptMessage(encryptedPass));
				userAccount.setJobRefNumber(mCur.getInt(mCur
						.getColumnIndex("jobRefNumber")));
				userAccount.setPassExpirationDate(mCur.getLong(mCur
						.getColumnIndex("PassExpirationDate")));
				userAccount.setPassNotificationType(mCur.getInt(mCur
						.getColumnIndex("PassNotificationType")));
				String encryptedPin = mCur.getString(mCur
						.getColumnIndex("Pin"));
				userAccount.setaPin(Security.decryptMessage(encryptedPin));
				userAccount.setIsAccountPinSet(mCur.getInt(mCur
						.getColumnIndex("isPinSet")));
			}

		} catch (Exception mSQLException) {
			Log.e("TestAdapter--",
					"get account info failed " + mSQLException.toString());
			// CommonUtils.log("Test Adapter " +
			// Constants.ERROR_SETTING_NOT_LOADED);
		}
		return userAccount;
	}

	public static void createNewUserDB() {

		LogUtil.i(TAG, "Inside createNewUserDB ");
		UserDBHandler userDB = new UserDBHandler(mContext);

		try {
			userDB.openDB();
			userDB.createTablesInDB();
			userDB.copyDataBase();
		} catch (IOException e) {
			Log.e("TestAdapter--",
					"Creating New DB for user failed" + e.toString());
		}
		userDB.closeDB();
	}

	// from amit

	// *******getJobReferenceNumber******
	public static String userValidate(String user) {
		// LogUtil.i("*******TestAdapter.userValidate******** :: ", user);
		// if (null != user && user.length() > 0 /*
		// * &&
		// * str.charAt(str.length()-1)=='C'
		// */) {
		// // userID = user.substring(0, user.length()-1);
		// Constants.VALIDATE_USER = user.substring(0, user.length() - 1);
		// }
		return Constants.VALIDATE_USER;
	}

	public Cursor getJobReferenceNumber() {
		try {
			// String sql = "SELECT jobRefNumber FROM Account WHERE Login = '"
			// + Constants.VALIDATE_USER + "'";
			String sql = "SELECT jobRefNumber FROM Account WHERE ID = '" + "1"
					+ "'";
			Log.e(TAG, "getJobReferenceNumber sql>>" + sql);
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getJobReferenceNumber >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getJobReferenceNumber******

	public Cursor getJobReferenceNumberfromJob() {
		try {
			String sql = "SELECT MAX(jobRefNumber) AS jobRefNumber FROM Job where Technitian = '"
					+ Constants.VALIDATE_USER + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getJobReferenceNumberfromJob >>"
							+ mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getVendorName******

	public Cursor getVendorName() {
		try {
			String sql = "SELECT distinct VendorName,Vendor.City FROM Vendor inner join tyre t on t.SAPVendorCodeID = vendor.id "
					+ "inner join contract c on c.ID = t.SAPContractNumberID WHERE UserID = '"
					+ Constants.VALIDATE_USER
					+ "' and "
					+ "T.IsUnmounted = 1 AND (T.Status = 5 OR T.Status = 3 or T.Status = 4) AND "
					+ "(trim(C.CountryCode) = trim(Vendor.CountryCode) OR C.isEU = 1) order by vendorname, c.CustomerName";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getVendorName >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getVendorName For eJOB******

	public Cursor getVendorNameForEJOB() {
		try {
			String sql = "SELECT VendorName,Vendor.City FROM Vendor WHERE UserID = '"
					+ Constants.VALIDATE_USER + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getVendorName >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getRemovalReason******

	public Cursor getRemovalReason() {
		String sql = null;
		try {
			if (Constants.JOBTYPEREGULAR) {
				sql = "SELECT Reason, Code, ExternalId FROM RemovalReason WHERE IsBreakDown = '"
						+ Constants.REGULAR_JOB + "' order by Reason";
			} else {
				sql = "SELECT Reason, Code, ExternalID FROM RemovalReason WHERE IsBreakDown = '"
						+ Constants.BREAKDOWN_JOB + "' order by Reason";
			}
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getRemovalReason >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public String getRemovalReasonFromId(String idCode) {
		String sql = null;
		String result = "";
		try {
			sql = "SELECT Reason FROM RemovalReason WHERE ExternalID = '"
					+ idCode + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {

				result = mCur.getString(0);
			}
			return result;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getRemovalReason >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getCasingRoute******

	public Cursor getCasingRoute() {
		try {
			String sql = "SELECT Description, SAPCode FROM CasingRoute";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getCasingRoute >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public String getCasingRouteFromId(String sapCode) {
		String sql = null;
		String result = "";
		try {
			sql = "SELECT Description FROM CasingRoute WHERE SAPCode = '"
					+ sapCode + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {

				result = mCur.getString(0);
			}
			return result;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getRemovalReason >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getDamageGroup******

	public Cursor getDamageGroup() {
		try {
			//Bug : 711 (Blow out/tyre destroyed is in list of damage reason, it should not be), check based on active
			String sql = "SELECT DamageGroup FROM Damage where Active='1' Group By DamageGroup ";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getDamageGroup >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getDamageArea******

	public Cursor getDamageArea(String damageType) {
		try {
			//Bug : 711 (Blow out/tyre destroyed is in list of damage reason, it should not be), check based on active
			String sql = "SELECT RemovalReasonCode FROM Damage WHERE DamageGroup = '"
					+ damageType + "' AND Active='1' ";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getDamageArea >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public String getDamageAreaFromId(String damageId) {
		String result = "";
		try {
			String sql = "SELECT RemovalReasonCode FROM Damage WHERE ExternalID = '"
					+ damageId + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {

				result = mCur.getString(0);
			}
			return result;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getDamageArea >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getBreakSpotDetails******

	public Cursor getBreakSpotDetails() {
		try {
			String sql = "SELECT DamageGroup, RemovalReasonCode FROM Damage WHERE DamageCode = 'BS'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getBreakSpotDetails >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getDamageExternalId******

	public Cursor getDamageExternalId(String damageType, String damageArea) {
		try {
			String sql = "Select ExternalID FROM Damage WHERE DamageGroup = '"
					+ damageType + "'" + "AND RemovalReasonCode = '"
					+ damageArea + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getDamageExternalId >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getFitterTypeOfVendor******

	public Cursor getFitterTypeOfVendor(String vendorName) {
		try {
			/*
			EJOB - 30 : users are unable to enter anything using a Russian keyboard
			when we try to enter the serial number for a tire (W/O serial) in WORK ON TIRE menu - fixed
			provided the generic fix for any special character in the vendor name.
			This will fix Bug173, vendor name with both single and double quotes will work fine.
			 */
			String sql = "Select FitterType FROM Vendor WHERE VendorName = ?";
			Cursor mCur = mDb.rawQuery(sql, new String[] { vendorName });
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getFitterTypeOfVendor >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getNSKFromSerial******

	public Cursor getNSKFromSerial(String serialNo) {
		try {
			String sql = "Select NSK1, NSK2, NSK3 FROM Tyre WHERE SerialNumber = '"
					+ serialNo + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getNSKFromSerial >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getRimTypeForDismountedTyre******

	public Cursor getRimTypeForDismountedTyre(String tyreExternalId,
			String actionType) {
		try {
			String sql = "Select RimType FROM JobItem WHERE ExternalId = '"
					+ tyreExternalId + "'" + "AND ActionType = '" + actionType
					+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getRimTypeForDismountedTyre >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getImagesForJobItemId******
	public Cursor getImagesForJobItemId(int jobItemId) {
		try {
			String sql = "Select ID, TyreImage, Description FROM TyreImage WHERE JobItemId = '"
					+ jobItemId + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getImagesForJobItemId >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public String getDamegeNotes(String jobItemId) {
		String sql = null;
		String result = "";
		try {
			sql = "Select  Description from TyreImage where JobItemID = '"
					+ jobItemId + "' LIMIT 1";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {

				result = mCur.getString(0);
			}
			return result;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getDamegeNotes >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getVendorCountryCode******

	public Cursor getVendorCountryCode(String vendorName) {
		try {
			String sql = "SELECT CountryCode FROM Vendor WHERE VendorName = ?";
			Cursor mCur = mDb.rawQuery(sql, new String[] { vendorName });
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getVendorName >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getVendorinfo(String vendorName) {
		try {
			String sql = "SELECT * FROM Vendor WHERE VendorName = ?";
			Cursor mCur = mDb.rawQuery(sql, new String[] { vendorName });
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getVendorName >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getvendorID******

	public Cursor getvendorID(String vName) {
		try {
			// String sql =
			// "SELECT SAPVendorCodeID from Tyre WHERE Active = 1 LIMIT 1";
			// SELECT ID, VendorName FROM Vendor WHERE UserID = 'NEN38AM' AND
			// VendorName = 'LOCAT'
			// String sql
			// ="SELECT ID FROM Vendor WHERE UserID = (Select Login FROM Settings WHERE ID = 1)";
			// FIX:: issue no 173 the ' was coming in between of the text so sql
			// was unable to find it, so added "" for text.

			/*Commented below string on 02 - June - 2016 and provided the generic
			fix for any special character in the vendor name. This will fix Bug
			173, vendor name with both single and double quotes will work fine.*/

			/*String sql = "SELECT DISTINCT ID FROM Vendor WHERE UserID  = '"
					+ Constants.VALIDATE_USER + "'" + "AND VendorName = "
					+ "\"" + vName + "\"";*/

			//Fix for Vendor name with both single and double quote's (Bug 173)
			String sql = "SELECT DISTINCT ID FROM Vendor WHERE UserID  = '"
					+ Constants.VALIDATE_USER + "'" + " AND VendorName = ?";
			System.out.println("sql getVendor ID::: " + sql);
			Cursor mCur = mDb.rawQuery(sql, new String[] { vName });
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getvendorID >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getvendorIDByName(String vendorName) {
		try {
			String sql = "SELECT SAPVendorCodeID from Tyre WHERE SAPVendorCodeID = (SELECT ID from vendor where VendorName = ?) AND Active = 1 LIMIT 1";
			Cursor mCur = mDb.rawQuery(sql, new String[] { vendorName });
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getvendorID >>" + mSQLException.toString());
			throw mSQLException;
		}
	}


	public Cursor getvendorIDByNameFromVendorTable(String vendorName) {
		try {
			String sql = "SELECT ID from vendor where VendorName = ? LIMIT 1";
			Cursor mCur = mDb.rawQuery(sql, new String[] { vendorName });
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getvendorID >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getCustomerName******

	public Cursor getCustomerName(String countryCode, String vendorID) {
		try {
			/*
			 * String sql =
			 * "SELECT  CustomerName FROM Contract WHERE CountryCode like '%" +
			 * countryCode + "%'";
			 */

			/*
			 * String sql =
			 * "SELECT DISTINCT C.ID, C.CustomerName FROM [Contract] C inner join Tyre T on T.SAPContractNumberID = C.ID Where T.IsUnmounted = 1 AND (T.Status = 5 OR T.Status = 3 or T.Status = 4) AND (C.CountryCode = '"
			 * + countryCode + "' AND T.SAPVendorCodeID = '" + vendorID +
			 * "' or C.isEU = 1)  ORDER BY C.CustomerName";
			 */
			/*
			 * String sql =
			 * "SELECT DISTINCT T.SAPVendorCodeID,C.CustomerName FROM [Contract] C inner join Tyre T "
			 * +
			 * "on T.SAPContractNumberID = C.ID Where T.IsUnmounted = 1 AND (T.Status = 5 OR T.Status = 3 or T.Status = 4) "
			 * + "AND (C.CountryCode = '"+countryCode+
			 * "' OR C.isEU = 1) AND t.SAPVendorCodeID='"+vendorID+"'";
			 */

			String sql = "SELECT distinct c.CustomerName, c.id FROM tyre t inner join contract c on c.ID = t.SAPContractNumberID "
					+ "WHERE T.IsUnmounted = 1 AND (T.Status = 5 OR T.Status = 3 or T.Status = 4) AND "
					+ "(trim(C.CountryCode) = trim('"
					+ countryCode
					+ "') OR C.isEU = 1) and t.SAPVendorCodeID='"
					+ vendorID
					+ "' order  by c.CustomerName COLLATE NOCASE";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getCustomerName >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getCustomerID(String customerNAME) {
		try {
			String customerNAMESingleQuote = customerNAME.replace("'", "''");
			String sql = "SELECT ID FROM Contract WHERE CustomerName = '"
					+ customerNAMESingleQuote + "'";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getCustomerName >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getTyreSerialNumber(String searchText) {
		try {
			String sql = "SELECT  SerialNumber FROM Tyre where SerialNumber LIKE"
					+ " '%" + searchText + "%'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreSerialNumber >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getTyreSearchResult(String serialNumber, String customerID,
			String vendorID) {
		try {
			/*
			 * String sql =
			 * "SELECT t.SerialNumber, tm.FullTireDetails From Tyre t Left Outer JOIN TireModel tm ON t.SAPMaterialCodeID= tm.ID "
			 * +
			 * "LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID LEFT OUTER JOIN Job J on J.ID = t.FromJobID WHERE"
			 * +
			 * " t.Active = 1 AND (t.Status = 3 OR t.Status = 4 or t.Status = 5) AND (t.IsUnmounted = 1 AND t.ID "
			 * +
			 * "in(SELECT TyreDesign.ID from Tyre NATURAL JOIN TyreDesign where SerialNumber LIKE '%"
			 * + serialNumber + "%' AND " + "SAPContractNumberID = '" +
			 * customerID + "' AND t.SAPVendorCodeID = '" + vendorID +
			 * "') OR Shared = 1) AND (J.Status is null or J.Status = 0)";
			 */

			String sql = "SELECT t.SerialNumber, tm.FullTireDetails From Tyre t LEFT OUTER JOIN TireModel "
					+ "tm ON t.SAPMaterialCodeID= tm.ID LEFT OUTER JOIN Job J on J.ID = t.FromJobID "
					+ "LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID WHERE t.SerialNumber like '%"
					+ serialNumber
					+ "%' AND  (T.Status = 3 OR T.Status = 4 or T.Status = 5) AND "
					+ "((t.IsUnmounted = 1 AND t.SAPContractNumberID = '"
					+ customerID
					+ "' AND t.SAPVendorCodeID = '"
					+ vendorID
					+ "') OR Shared = 1)AND (J.Status is null or J.Status = 0)";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			LogUtil.e(TAG, "getTyreDesign >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getTyreDesign(String serialNumber, String customerID) {
		try {
			String sql = "SELECT tm.FullTireDetails From Tyre t LEFT OUTER JOIN TireModel tm ON t.SAPMaterialCodeID= tm.ID LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID WHERE t.ID =(SELECT TyreDesign.ID from Tyre NATURAL JOIN TyreDesign where SerialNumber = '%"
					+ serialNumber
					+ "%' AND SAPContractNumberID = '"
					+ customerID + "')";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDesign >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getTyreOperation() {

		try {
			String sql = null;

			if (TyreConstants.TyreStatus.equals("5")) {// 2
				sql = "SELECT Description,OperationCode,OperationGroup from TyreOperation where OperationGroup = 3";
			} else if (TyreConstants.TyreStatus.equals("3")) {
				sql = "SELECT Description,OperationCode,OperationGroup from TyreOperation where OperationGroup = 1";
			} else if (TyreConstants.TyreStatus.equals("4")) {
				sql = "SELECT Description,OperationCode,OperationGroup from TyreOperation where OperationGroup = 2";
			}
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDesign >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getTyreSize******

	public Cursor getTyreSize(String design) {
		try {
			String sql = "Select TSize from TyreSizeDesign where DesignID = (Select ID from TyreDesign where Design = '"
					+ design + "')";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getVendorName >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// getTyreStatus
	public Cursor getTyreStatus(String serialNumber) {
		try {
			/* String sql = "SELECT JobID, TyreID, KeyName FROm JobCorrection"; */
			String sql = "SELECT Status FROM Tyre WHERE SerialNumber = '"
					+ serialNumber
					+ "' AND (Status = 3 OR Status = 4 or Status = 5)";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDesign >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getTyreID******

	public Cursor getTyreID(String serialNumber) {
		try {
			String sql = "Select ExternalID from Tyre where SerialNumber = '"
					+ serialNumber + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getVendorName >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getTyreOperationID******

	public Cursor getTyreOperationID(String operation) {
		try {
			String sql = "Select ExternalID from TyreOperation where Description = '"
					+ operation + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getVendorName >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// *******getTyreSapContractID******

	public Cursor getTyreSapContractID(String serialnumber) {
		try {
			String sql = "Select SAPContractNumberID from Tyre where SerialNumber = '"
					+ serialnumber + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreSapContractID >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public List<String> getAllLabels() {
		List<String> labels = new ArrayList<String>();

		// Select All Query
		String selectQuery = "SELECT CustomerName FROM Contract";

		// SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = mDb.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				labels.add(cursor.getString(1));
			} while (cursor.moveToNext());
		}

		// closing connection
		cursor.close();
		// mDb.close();

		// returning lables

		System.out.println("labels------>>>>>>>>" + labels);
		return labels;
	}

	// Insertion in Job Table
	public boolean InsertJobOBj() {
		try {
			ContentValues cv = new ContentValues();

			// cv.put("ID", ID);
			cv.put("ExternalID", Job.getExternalID());
			cv.put("LicenseNumber", Job.getLicenseNumber());
			cv.put("SAPContractNumber", Job.getSapContractNumber());
			cv.put("VehicleID", Job.getVehicleID());
			cv.put("Technitian", Job.getTechnitian());
			cv.put("JobType", Job.getJobType());
			cv.put("OdometerType", Job.getOdometerType());
			cv.put("GPSLocation", Job.getGpsLocation());// lat|long|address as
			// text
			cv.put("Location", Job.getLocation());
			cv.put("TimeFinished", Job.getTimeFinished());
			cv.put("MissingOdometerReasonID", Job.getMissingOdometerReasonID());
			cv.put("AgreeStatus", Job.getAgreeStatus());
			cv.put("VehicleUnattendedStatus", Job.getVehicleUnattendedStatus());
			cv.put("TimeOnSite", Job.getTimeOnSite());
			cv.put("TimeStarted", Job.getTimeStarted());
			cv.put("TimeReceived", Job.getTimeReceived());
			cv.put("CallerPhone", Job.getCallerPhone());// caller phone number
			cv.put("CallerName", Job.getCallerName()); // caller name
			cv.put("VendorCountryID", Job.getVendorCountryID());
			cv.put("ThirtyMinutesRetorqueStatus",
					Job.getThirtyMinutesRetorqueStatus());
			cv.put("DefectAuthNumber", Job.getDefectAuthNumber()); // defect
			// auth
			// number
			cv.put("DriverName", Job.getDriverName());// driver name
			cv.put("ActionLineN", Job.getActionLineN()); // call center ref
			// number
			cv.put("Hudometer", Job.getHudometer()); // secondayr odo
			cv.put("VendorDetail", Job.getVendorDetail());
			cv.put("VendorSAPCode", Job.getVendorSAPCode());
			cv.put("SignatureReasonID", Job.getSignatureReasonID());
			cv.put("StockLocation", Job.getStockLocation()); // stock loc
			cv.put("VehicleODO", Job.getVehicleODO()); // primary odo
			cv.put("SignatureImage", Job.getSignatureImage());
			cv.put("Note", Job.getNote());
			cv.put("Status", Job.getStatus());
			cv.put("jobRefNumber", Job.getJobRefNumber());
			cv.put("JobValidityDate", Job.getJobValidityDate());
			cv.put("TimeOffsetMinutes", Job.getTimeOffsetMinutes());
			cv.put("JobRemovalReasonID", Job.getJobRemovalReasonID());

			mDb.insert("Job", null, cv);

			Log.d("SaveJob", "Tyre Management Data Inserted in Job Table");
			return true;

		} catch (Exception ex) {
			Log.d("SaveJob", ex.toString());
			return false;
		}
	}

	// Insertion in Job Table
	public boolean InsertJob(String ExternalID, String LicenseNumber,
			int SAPContractNumber, String VehicleID, String Technitian,
			String JobType, String OdometerType, String GPSLocation,
			String Location, long TimeFinished, String MissingOdometerReasonID,
			String AgreeStatus, String VehicleUnattendedStatus,
			long TimeOnSite, long TimeStarted, long TimeReceived,
			String CallerPhone, String CallerName, String VendorCountryID,
			String ThirtyMinutesRetorqueStatus, String DefectAuthNumber,
			String DriverName, String ActionLineN, String Hudometer,
			String VendorDetail, String VendorSAPCode,
			String SignatureReasonID, String StockLocation, String VehicleODO,
			String SignatureImage, String Note, String Status,
			String jobRefNumber, long JobValidityDate, long TimeOffsetMinutes,
			String JobRemovalReasonID,String PreviousOdometerValue,String TorqueSettingsSame) {
		try {
			ContentValues cv = new ContentValues();

			// cv.put("ID", ID);
			cv.put("ExternalID", ExternalID);
			cv.put("LicenseNumber", LicenseNumber);
			cv.put("SAPContractNumber", SAPContractNumber);
			cv.put("VehicleID", VehicleID);
			cv.put("Technitian", Technitian);
			cv.put("JobType", JobType);
			cv.put("OdometerType", OdometerType);
			cv.put("GPSLocation", GPSLocation);// lat|long|address as text
			cv.put("Location", Location);
			cv.put("TimeFinished", TimeFinished);
			cv.put("MissingOdometerReasonID", MissingOdometerReasonID);
			cv.put("AgreeStatus", AgreeStatus);
			cv.put("VehicleUnattendedStatus", VehicleUnattendedStatus);
			cv.put("TimeOnSite", TimeOnSite);
			cv.put("TimeStarted", TimeStarted);
			cv.put("TimeReceived", TimeReceived);
			cv.put("CallerPhone", CallerPhone);// caller phone number
			cv.put("CallerName", CallerName); // caller name
			cv.put("VendorCountryID", VendorCountryID);
			cv.put("ThirtyMinutesRetorqueStatus", ThirtyMinutesRetorqueStatus);
			cv.put("DefectAuthNumber", DefectAuthNumber); // defect auth number
			cv.put("DriverName", DriverName);// driver name
			cv.put("ActionLineN", ActionLineN); // call center ref number
			cv.put("Hudometer", Hudometer); // secondayr odo
			cv.put("VendorDetail", VendorDetail);
			cv.put("VendorSAPCode", VendorSAPCode);
			cv.put("SignatureReasonID", SignatureReasonID);
			cv.put("StockLocation", StockLocation); // stock loc
			cv.put("VehicleODO", VehicleODO); // primary odo
			cv.put("SignatureImage", SignatureImage);
			cv.put("Note", Note);
			cv.put("Status", Status);
			cv.put("jobRefNumber", jobRefNumber);
			cv.put("JobValidityDate", JobValidityDate);
			cv.put("TimeOffsetMinutes", TimeOffsetMinutes);
			cv.put("JobRemovalReasonID", JobRemovalReasonID);
			//CR - 548 : Bring the latest SAP tacho rwading to the application and compare to that entered by the user
			//To store last recorded odometer Value
			cv.put("PreviousOdometerValue", PreviousOdometerValue);

			//CR - 550 (some vehicles have different torque settings per axle)
			//To store Torque Settings per axle info
			cv.put("TorqueSettingsSame",TorqueSettingsSame);

			mDb.insert("Job", null, cv);

			Log.d("SaveJob", "Tyre Management Data Inserted in Job Table");
			return true;

		} catch (Exception ex) {
			Log.d("SaveJob", ex.toString());
			return false;
		}
	}

	// Insertion in Job Table
	public boolean InsertJobItem(String JobID, int SAPCodeTiID,
			String ExternalID, int ActionType, int MinNSK, String TyreID,
			String RemovalReasonID, String DamageID, String CasingRouteCode,
			String Regrooved, int TorqueSetting, String Note,
			Double TreadTepth, int Pressure, int GrooveNumber,
			int ReGrooveNumber, int ServiceCount, int RegrooveType,
			int NSK1Before, int NSK1After, int ServiceID, int AxleNumber,
			int AxleServiceType, String WorkOrderNumber, String RepairCompany,
			int NSK2Before, int NSK2After, int NSK3Before, int NSK3After,
			int SwapType, int Sequence, int RecInflactionOriginal,
			int RecInflactionDestination, String SWAPOriginalPosition,
			String OperationID, int RimType) {
		try {
			ContentValues cv = new ContentValues();

			// cv.put("ID", ID);

			cv.put("JobID", JobID);
			cv.put("SAPCodeTiID", SAPCodeTiID);
			cv.put("ExternalID", ExternalID);
			cv.put("ActionType", ActionType);
			cv.put("MinNSK", MinNSK);
			cv.put("TyreID", TyreID);
			cv.put("RemovalReasonID", RemovalReasonID);
			cv.put("DamageID", DamageID);
			cv.put("CasingRouteCode", CasingRouteCode);
			cv.put("Regrooved", Regrooved);
			cv.put("TorqueSetting", TorqueSetting);
			cv.put("Note", Note);
			cv.put("TreadTepth", TreadTepth);
			cv.put("Pressure", Pressure);
			cv.put("GrooveNumber", GrooveNumber);
			cv.put("ReGrooveNumber", ReGrooveNumber);
			cv.put("ServiceCount", ServiceCount);
			cv.put("RegrooveType", RegrooveType);
			cv.put("NSK1Before", NSK1Before);
			cv.put("NSK1After", NSK1After);
			cv.put("ServiceID", ServiceID);
			cv.put("AxleNumber", AxleNumber);
			cv.put("AxleServiceType", AxleServiceType);

			cv.put("WorkOrderNumber", WorkOrderNumber);

			cv.put("RepairCompany", RepairCompany);
			cv.put("NSK2Before", NSK2Before);
			cv.put("NSK2After", NSK2After);
			cv.put("NSK3Before", NSK3Before);
			cv.put("NSK3After", NSK3After);
			cv.put("SwapType", SwapType);
			cv.put("Sequence", Sequence);
			cv.put("RecInflactionOriginal", RecInflactionOriginal);
			cv.put("RecInflactionDestination", RecInflactionDestination);
			cv.put("SWAPOriginalPosition", SWAPOriginalPosition);
			cv.put("OperationID", OperationID);
			cv.put("RimType", RimType);

			mDb.insert("JobItem", null, cv);

			Log.d("SaveJob", "Data Inserted in JobItem Table");
			return true;

		} catch (Exception ex) {
			Log.d("SaveJob", ex.toString());
			return false;
		}
	}

	public boolean InsertAccountFromTM(String Login, String Pass,
			int jobRefNumber, String PassExpirationDate,
			int PassNotificationType) {
		try {
			ContentValues cv = new ContentValues();

			// cv.put("ID", ID);
			cv.put("Login", Login);
			cv.put("Pass", Pass);
			cv.put("jobRefNumber", jobRefNumber);
			cv.put("PassExpirationDate", PassExpirationDate);
			cv.put("PassNotificationType", PassNotificationType);
			mDb.insert("Account", null, cv);

			Log.d("SaveJob", "Tyre Management Data Inserted in Account Table");
			return true;

		} catch (Exception ex) {
			Log.d("SaveJob", ex.toString());
			return false;
		}
	}

	public boolean updateTyre(String TABLE_NAME, String COLUMN_ID,
			String Current_Value, String New_Value) {
		// TODO Auto-generated method stub
		try {
			String selectQuery = "UPDATE " + TABLE_NAME + " SET "
					+ Current_Value + " = '" + New_Value
					+ "'  WHERE SerialNumber = '" + COLUMN_ID + "'";
			mDb.execSQL(selectQuery);
			Log.e(TAG, "Data Updated >>" + "Tyre Starus Updated");
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTestData >>" + mSQLException.toString());
			throw mSQLException;
		}
		return true;
	}

	public Cursor getJobItemValues() {
		try {
			String sql = "Select * from JobItem";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				return mCur;
			}
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTestData >>" + mSQLException.toString());
			// CommonUtils.log("Test Adapter " +
			// Constants.ERROR_SETTING_NOT_LOADED);
			throw mSQLException;
		}
		return null;
	}

	/**
	 * Method to get JobItems based on Job Reference number
	 * @param jobNumber Job reference number
	 * @return cursor
	 */
	public Cursor getJobItemValuesByJobRefID(String jobNumber) {
		try {
			String sql = "Select * from JobItem where JobID in(Select ExternalID from Job where jobRefNumber = '"+jobNumber+"')";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				return mCur;
			}
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTestData >>" + mSQLException.toString());
			// CommonUtils.log("Test Adapter " +
			// Constants.ERROR_SETTING_NOT_LOADED);
			throw mSQLException;
		}
		return null;
	}

	/**
	 * Method to get Tyres based on Job number
	 * @param jobNumber job reference number
	 * @return cursor
	 */
	public Cursor getTyreValuesByJobRefID(String jobNumber) {
		try {

			String sql = "Select * from Tyre where ExternalID in(Select TyreID from JobItem where JobID in(Select ExternalID from Job where jobRefNumber = '"+jobNumber+"')) OR idVehicleJob in(Select VehicleID from Job where jobRefNumber = '"+jobNumber+"')";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				return mCur;
			}
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTestData >>" + mSQLException.toString());
			// CommonUtils.log("Test Adapter " +
			// Constants.ERROR_SETTING_NOT_LOADED);
			throw mSQLException;
		}
		return null;
	}

	public void getTyreValues() {
		try {
			String sql = "Select * from Tyre ";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.getString(0);
				mCur.getString(1);
				mCur.getString(2);
				mCur.getString(3);
				mCur.getString(4);
				mCur.getString(33);
			}
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTestData >>" + mSQLException.toString());
			// CommonUtils.log("Test Adapter " +
			// Constants.ERROR_SETTING_NOT_LOADED);
			throw mSQLException;
		}
	}

	public Cursor getTyreSummary(String serialNumber, String customerID) {
		try {
			String sql = "SELECT t.SerialNumber, tm.FullTireDetails From Tyre t LEFT OUTER JOIN TireModel tm ON t.SAPMaterialCodeID= tm.ID LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID WHERE t.ID =(SELECT TyreDesign.ID from Tyre NATURAL JOIN TyreDesign where SerialNumber LIKE '%"
					+ serialNumber
					+ "%' AND SAPContractNumberID = '"
					+ customerID + "')";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDesign >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * @param selectedSerialNumber
	 * @return
	 */
	public Cursor getTyreSAPVendorID(String serialNumber) {
		// TODO Auto-generated method stub

		try {
			String sql = "Select SAPVendorCodeID from Tyre WHERE SerialNumber = '"
					+ serialNumber + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDesign >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * @param jobid
	 * @return
	 * @throws SQLException
	 */
	public Cursor TyreID_ActionType(String jobid) throws SQLException {
		Cursor mCursor = mDb.query(true, "JobItem", new String[] { "TyreID",
				"ActionType", "WorkOrderNumber", "RepairCompany", "OperationID"

		}, "JobID" + "= '" + jobid + "'", null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public Cursor CheckTyreId(String TyreId) throws SQLException {
		Cursor mCursor = mDb.query(true, "JobItem", new String[] { "TyreID" },
				"TyreID" + "= '" + TyreId + "'", null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	/**
	 * @param ExternalID
	 * @return
	 * @throws SQLException
	 */
	public Cursor SerialNo_SAP(String ExternalID) throws SQLException {
		Cursor mCursor = mDb.query(true, "Tyre", new String[] { "SerialNumber",
				"SAPMaterialCodeID", "Status"

		}, "ExternalID" + "= '" + ExternalID + "'", null, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	/**
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public Cursor fulldetailsTyre(int id) throws SQLException {
		Cursor mCursor = mDb.query(true, "TireModel",
				new String[] { "FullTireDetails"

				}, "ID" + "= '" + id + "'", null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	/**
	 * @param OperationID
	 * @return
	 * @throws SQLException
	 */
	public Cursor operationdetail(String OperationID) throws SQLException {
		Cursor mCursor = mDb.query(true, "TyreOperation",
				new String[] { "Description"

				}, "ExternalID" + "= '" + OperationID + "'", null, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	// nikhil.v
	/**
	 * @return
	 */
	public Cursor Job_GetJobList() {

		return mDb.query("Job", new String[] { "LicenseNumber",
				"SAPContractNumber", "TimeStarted", "Status", "jobRefNumber",
				"ID", "ExternalID", "TimeFinished" }, null, null, null, null,
				null, null);

	}

	public Cursor GetTyre() {
		return mDb.query("Tyre", new String[] { "SerialNumber", "ExternalID" },
				null, null, null, null, null, null);

	}

	/**
	 * Getting The job list status wise
	 * 
	 * @param jobStatus
	 * @return
	 */
	public Cursor Job_GetJobList(int jobStatus) {

		return mDb.query("Job", new String[] { "LicenseNumber",
				"SAPContractNumber", "TimeStarted", "Status", "jobRefNumber",
				"ID", "ExternalID", "TimeFinished" }, "Status=" + jobStatus,
				null, null, null, null, null);

	}

	public void updateStatuc(String status) throws Exception {
		try {
			String sql = "UPDATE Job SET Status = \"" + status
					+ "\" WHERE ID = '" + Constants.GETSELECTEDLISTPOSITION
					+ "' ";
			mDb.execSQL(sql);
		} catch (SQLException mSQLException) {
			CommonUtils.log(mSQLException.toString());
			Log.e(TAG, "cannot update status info" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor Job_GetUpdatedJobList(int JobRefNum) {

		try {
			String sql = "Select jobRefNumber,LicenseNumber, TimeFinished Job Where jobRefNumber = "
					+ JobRefNum + "";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "Job_GetUpdatedJobList >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * Nikhil V
	 * 
	 * @param contractnumber
	 * @return
	 * @throws SQLException
	 */
	public Cursor ContractGetName(int contractnumber) throws SQLException {
		Cursor mCursor = mDb.query(true, "Contract",
				new String[] { "CustomerName"

				}, "ID" + "= '" + contractnumber + "'", null, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	/**
	 * @param refno
	 * @return
	 * @throws SQLException
	 */
	public Cursor JobextID(int refno) throws SQLException {
		Cursor mCursor = mDb.query(true, "Job", new String[] { "ExternalID",
				"VendorDetail"

		}, "jobRefNumber" + "= '" + refno + "'", null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public Cursor getFullSummaryDetails(int jobReferenceNumber) {
		try {
			String sql = " Select T.SerialNumber, T.ExternalID, TM.FullTireDetails, TD.Description, ji.RepairCompany,ji.WorkOrderNumber, ji.ActionType, ji.RegrooveType, ji.TreadTepth from   Tyre T  left outer join tiremodel tm on T.SAPMaterialCodeID = tm.ID  join jobitem ji on ji.TyreID= T.ExternalID join TyreOperation TD on JI.OperationID = TD.ExternalID join job j on JI.jobid=j.externalID where j.JobRefNumber= '"
					+ jobReferenceNumber + "'";

			// "Select T.SerialNumber, TM.FullTireDetails, JI.OperationID, ji.RepairCompany,ji.WorkOrderNumber, ji.ActionType from   Tyre T  join TireModel TM on T.SAPMaterialCodeID = tm.ID  join jobitem ji on ji.TyreID= T.ExternalID join job j on JI.jobid=j.externalID where j.JobRefNumber = '100058'"

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getJobReferenceNumber >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getOperationCode(String description) {
		try {
			String sql = "Select OperationCode from TyreOperation where Description = '"
					+ description + "' LIMIT 1";

			// "Select T.SerialNumber, TM.FullTireDetails, JI.OperationID, ji.RepairCompany,ji.WorkOrderNumber, ji.ActionType from   Tyre T  join TireModel TM on T.SAPMaterialCodeID = tm.ID  join jobitem ji on ji.TyreID= T.ExternalID join job j on JI.jobid=j.externalID where j.JobRefNumber = '100058'"

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getOperationCode >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * @param getDeatilsforSerialValidation
	 * @return
	 */
	public Cursor getDeatilsforSerialValidation(String serialNumber) {
		// TODO Auto-generated method stub

		try {
			String sql = "SELECT t.*, b.Description As BrandName, tm.Deseign As Design, tm.FullTireDetails AS FullTireDetails, tm.AspectRatio as ASP, tm.Diameter as RIM,tm.FSize as Size, tm.EANUPC  From Tyre t, Contract c LEFT OUTER JOIN TireModel tm ON t.SAPMaterialCodeID= tm.ID LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID WHERE c.ID = t.SAPContractNumberID AND t.SerialNumber LIKE '"
					+ serialNumber
					+ "' AND t.IsUnmounted = 1 AND (c.ContractType = 1 OR c.ContractType = 2)";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDesign >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// getJobIDforJobCorrection
	public Cursor getJobIDforJobCorrection() {
		// TODO Auto-generated method stub

		try {
			String sql = "SELECT ExternalID FROM Job WHERE Status = 2 OR Status = 4 OR Status = 6";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getJobIDforJobCorrection >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// Insertion in JobCorrection Table
	public boolean InsertJobCorrection(String JobID, String TyreID,
			String KeyName, String OldValue, String NewValue) {
		try {
			ContentValues cv = new ContentValues();

			cv.put("JobID", JobID);
			cv.put("TyreID", TyreID);
			cv.put("KeyName", KeyName);
			cv.put("OldValue", OldValue);
			cv.put("NewValue", NewValue);

			mDb.insert("JobCorrection", null, cv);

			Log.d("SaveJob", "Data Inserted in JobCorrection Table");
			return true;

		} catch (Exception ex) {
			Log.d("SaveJobCorrection", ex.toString());
			return false;
		}
	}

	// getBrandName for id
	public String getBrandNameForID(String id) {
		// TODO Auto-generated method stub

		try {
			String sql = "Select  Description from TyreBrand where ID = '" + id
					+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				System.out.println("Data inside if" + mCur);
			}

			return mCur.getString(mCur.getColumnIndex("Description"));

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getBrandNameForID >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// getBrandNameForSWAP
	public Cursor getBrandNameForSWAP() {
		// TODO Auto-generated method stub

		try {
			String sql = "Select Description, ID from TyreBrand order by Piority, Description";
			LogUtil.i("TestAdapter :: getBrandNameForSWAP", sql);
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getBrandNameForSWAP >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getStockLocationCode(String userId, String vendorCodeId,
			String purchasingOrg) {
		Cursor mCur = null;
		try {
			String sql = "SELECT ID,PurchasingPlant,StockLocationCode FROM Technician "
					+ "WHERE UserID = '"
					+ userId
					+ "' AND VendorSAPCode = '"
					+ vendorCodeId
					+ "' "
					+ "AND PurchasingOrg = '"
					+ purchasingOrg + "'";

			mCur = mDb.rawQuery(sql, null);
		} catch (SQLException mSQLException) {
			throw mSQLException;
		}
		return mCur;
	}

	public Cursor getBrandNameForPurPlantStockLoc(String purchasingPlant,
			String stockLocation) {
		Cursor mCur = null;
		try {
			String sql = "SELECT Description, ID FROM TyreBrand WHERE ID IN ("
					+ "SELECT DISTINCT(TB.ID) FROM TyreBrand TB "
					+ "LEFT JOIN TireModel TM ON TB.ID = TM.IDTireBrand "
					+ "LEFT JOIN ProductPurchOrg P ON TM.ID = P.IDTireModel "
					+ "WHERE P.PurchasingPlant = '" + purchasingPlant + "' "
					+ "AND P.StockLocationCode = '" + stockLocation + "')";
			LogUtil.i("TestAdapter :: getBrandNameForPurPlantStockLoc", sql);
			mCur = mDb.rawQuery(sql, null);
		} catch (SQLException mSQLException) {
			throw mSQLException;
		}
		return mCur;
	}

	public Cursor getBrandNameForPurPlantStockLocWithSize(
			String purchasingPlant, String stockLocation,
			String selectedtyreSize, String selectedTyreTASP,
			String selectedtyreTRIM) {
		Cursor mCur = null;
		try {
			String sql = "SELECT Description, ID FROM TyreBrand WHERE ID IN ("
					+ "SELECT DISTINCT(TB.ID) FROM TyreBrand TB "
					+ "LEFT JOIN TireModel TM ON TB.ID = TM.IDTireBrand "
					+ "LEFT JOIN ProductPurchOrg P ON TM.ID = P.IDTireModel "
					+ "WHERE P.PurchasingPlant = '" + purchasingPlant + "' "
					+ "AND P.StockLocationCode = '" + stockLocation + "' "
					+ "AND TM.FSize = '" + selectedtyreSize + "' "
					+ "AND TM.Diameter = '" + selectedtyreTRIM + "' "
					+ "AND TM.AspectRatio = '" + selectedTyreTASP + "' " + ") ";
			LogUtil.i("TestAdapter :: getBrandNameForPurPlantStockLoc", sql);
			mCur = mDb.rawQuery(sql, null);
		} catch (SQLException mSQLException) {
			throw mSQLException;
		}
		return mCur;
	}

	public Cursor getBrandNameForPurOrgWithSize(String purchasingOrg,
			String selectedtyreSize, String selectedTyreTASP,
			String selectedtyreTRIM) {
		Cursor mCur = null;
		try {
			String sql = "SELECT Description, ID FROM TyreBrand WHERE ID IN ("
					+ "SELECT DISTINCT(TB.ID) FROM TyreBrand TB "
					+ "LEFT JOIN TireModel TM ON TB.ID = TM.IDTireBrand "
					+ "LEFT JOIN ProductPurchOrg P ON TM.ID = P.IDTireModel "
					+ "WHERE P.PurchasingOrg = '" + purchasingOrg + "' "
					+ "AND TM.FSize = '" + selectedtyreSize + "' "
					+ "AND TM.Diameter = '" + selectedtyreTRIM + "' "
					+ "AND TM.AspectRatio = '" + selectedTyreTASP + "' " + ")";
			LogUtil.i("TestAdapter :: getBrandNameForPurOrg", sql);
			mCur = mDb.rawQuery(sql, null);
		} catch (SQLException mSQLException) {
			throw mSQLException;
		}
		return mCur;
	}

	public Cursor getBrandNameForPurOrg(String purchasingOrg) {
		Cursor mCur = null;
		try {
			String sql = "SELECT Description, ID FROM TyreBrand WHERE ID IN ("
					+ "SELECT DISTINCT(TB.ID) FROM TyreBrand TB "
					+ "LEFT JOIN TireModel TM ON TB.ID = TM.IDTireBrand "
					+ "LEFT JOIN ProductPurchOrg P ON TM.ID = P.IDTireModel "
					+ "WHERE P.PurchasingOrg = '" + purchasingOrg + "' )";
			LogUtil.i("TestAdapter :: getBrandNameForPurOrg", sql);
			mCur = mDb.rawQuery(sql, null);
		} catch (SQLException mSQLException) {
			throw mSQLException;
		}
		return mCur;
	}

	public Cursor getTyreSizeFromBrand(String brandName) {
		try {
			String sql = "Select Distinct(TSize) from TyreSizeDesign where BrandID = (Select ID from TyreBrand where Description = '"
					+ brandName + "') order by TSize";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreSizeFromBrand >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getTyreASPFromSize(String tyreSize, String brandName) {
		try {
			String sql = "Select Distinct(TASP) from TyreSizeDesign where TSize = '"
					+ tyreSize
					+ "'"
					+ "AND BrandID = (Select ID from TyreBrand where Description = '"
					+ brandName + "') order by TASP";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreASPFromSize >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getTyreRimFromSize(String tyreSize, String tyreASP,
			String brandName) {
		try {
			String sql = "Select Distinct(TRIM) from TyreSizeDesign where TSize = '"
					+ tyreSize
					+ "'"
					+ "AND TASP = '"
					+ tyreASP
					+ "'"
					+ "AND BrandID = (Select ID from TyreBrand where Description = '"
					+ brandName + "') order by TRIM";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreRimFromSize >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getTyreDesign(String tyreSize, String tyreASP,
			String tyreRIM, String brandName) {
		try {
			String sql = "Select  Distinct(Deseign) from TireModel where FSize = '"
					+ tyreSize
					+ "'"
					+ "AND AspectRatio = '"
					+ tyreASP
					+ "'"
					+ "AND Diameter = '"
					+ tyreRIM
					+ "'"
					+ "AND IDTireBrand = (Select ID from TyreBrand where Description = '"
					+ brandName + "') order by Deseign";
			System.out.println("getTyreDesign qry :: " + sql);
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDesign >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getTyreDesignByBrandId(String tyreSize, String tyreASP,
			String tyreRIM, String brandId) {
		try {
			String sql = "Select  Distinct(Deseign) from TireModel where FSize = '"
					+ tyreSize
					+ "'"
					+ "AND AspectRatio = '"
					+ tyreASP
					+ "'"
					+ "AND Diameter = '"
					+ tyreRIM
					+ "'"
					+ "AND IDTireBrand = '" + brandId + "' order by Deseign";
			System.out.println("getTyreDesign qry :: " + sql);
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDesign >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * Get Tyre Design based on Purchase Organization, size and brand
	 */
	public Cursor getTyreDesignByPurchaseOrg(String size, String asp,
			String rim, String brandName, String purchaseOrg) {
		try {
			String qry = "SELECT DISTINCT(tm.Deseign) As Deseign FROM TireModel tm, ProductPurchOrg ppo WHERE  ppo.IDTireModel = tm.ID AND tm.IDTireBrand = '"
					+ brandName
					+ "' AND tm.FSize= '"
					+ size
					+ "' AND tm.Diameter='"
					+ rim
					+ "' AND tm.AspectRatio='"
					+ asp
					+ "' AND ppo.PurchasingOrg = '"
					+ purchaseOrg
					+ "' ORDER BY tm.Deseign";
			LogUtil.d("TestAdapter", "getTyreDesignByPurchaseOrg qry :: " + qry);
			Cursor mCur = mDb.rawQuery(qry, null);
			return mCur;
		} catch (Exception mSQLException) {
			LogUtil.d("TestAdapter",
					"Exception in getTyreDesignByPurchaseOrg :: "
							+ mSQLException.toString());
			mSQLException.printStackTrace();
		}
		return null;
	}

	/**
	 * Get Tyre Design based on Stock and purchase plant, size and brand
	 */
	public Cursor getTyreDesignByStockLocAndPurchasePlant(String size,
			String asp, String rim, String brandName, String purchasePlant,
			String stocklocation) {
		try {
			String qry = "SELECT DISTINCT(tm.Deseign) As Deseign FROM TireModel tm, ProductPurchOrg ppo WHERE  ppo.IDTireModel = tm.ID AND tm.IDTireBrand = '"
					+ brandName
					+ "' AND tm.FSize= '"
					+ size
					+ "' AND tm.Diameter='"
					+ rim
					+ "' AND tm.AspectRatio='"
					+ asp
					+ "' AND ppo.PurchasingPlant = '"
					+ purchasePlant
					+ "' AND ppo.StockLocationCode = '"
					+ stocklocation
					+ "' ORDER BY tm.Deseign";
			LogUtil.d("TestAdapter",
					"getTyreDesignByStockLocAndPurchasePlant qry :: " + qry);
			Cursor mCur = mDb.rawQuery(qry, null);
			return mCur;
		} catch (SQLException mSQLException) {
			LogUtil.d("TestAdapter",
					"Exception in getTyreDesignByStockLocAndPurchasePlant :: "
							+ mSQLException.toString());
			mSQLException.printStackTrace();
		}
		return null;
	}

	public Cursor getTyreDetailedDesign(String tyreSize, String tyreASP,
			String tyreRIM, String tyreDesign, String brandName) {
		try {
			String sql = "Select Distinct(FullTireDetails) from TireModel where Deseign = '"
					+ tyreDesign
					+ "'"
					+ "AND FSize = '"
					+ tyreSize
					+ "'"
					+ "AND AspectRatio = '"
					+ tyreASP
					+ "'"
					+ "AND Diameter = '"
					+ tyreRIM
					+ "'"
					+ "AND IDTireBrand = (Select ID from TyreBrand where Description = '"
					+ brandName + "') order by FullTireDetails";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDetailedDesign >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getSAPMaterialCodeIDByDetailedDesign(String tyreFullDesign) {
		try {
			String sql = "Select ID from TireModel where FullTireDetails = '"
					+ tyreFullDesign + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDetailedDesign >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getSAPMaterialCodeID(String tyreSize, String tyreASP,
			String tyreRIM, String tyreDesign, String tyreFullDesign,
			String brandName) {
		try {
			String sql = "Select ID from TireModel where Deseign = '"
					+ tyreDesign
					+ "'"
					+ "AND FullTireDetails = '"
					+ tyreFullDesign
					+ "'"
					+ "AND FSize = '"
					+ tyreSize
					+ "'"
					+ "AND AspectRatio = '"
					+ tyreASP
					+ "'"
					+ "AND Diameter = '"
					+ tyreRIM
					+ "'"
					+ "AND IDTireBrand = (Select ID from TyreBrand where Description = '"
					+ brandName + "')";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDetailedDesign >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// getTyreSizeForSWAP
	public Cursor getContractPolicy(String contractId) {
		try {
			String sql = "Select Balancing, PolicyOnHPValveCap, PolicyOnValveExtension, ValveFitting from ContractTyrePolicy where SAPContractNumber = '"
					+ contractId + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreSizeForSWAP >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// getTyreSizeForSWAP
	public Cursor getTyreDimensionForSWAP(String brandName) {
		// TODO Auto-generated method stub

		try {
			String sql = "Select TSize, TRIM, TASP from TyreSizeDesign where BrandID = (Select ID from TyreBrand where Description = '"
					+ brandName + "')";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreSizeForSWAP >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// getTyreDesignDetails
	public Cursor getTyreDesignDetails(String brandName, String rim,
			String size, String asp) {
		// TODO Auto-generated method stub

		try {
			String sql = "SELECT tm.Deseign As Deseign, tm.FullTireDetails AS FullTireDetails From Tyre t LEFT OUTER JOIN TireModel tm ON t.SAPMaterialCodeID= tm.ID LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID WHERE  t.IsUnmounted = 1 AND t.SAPContractNumberID = '1155000567' AND tm.Diameter = '"
					+ rim
					+ "' AND tm.FSize = '"
					+ size
					+ "' AND tm.AspectRatio = '"
					+ asp
					+ "' AND tm.IDTireBrand in (Select ID from TyreBrand where Description = '"
					+ brandName + "') LIMIT 1";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDesignDetails >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// getPWTTyreDetails
	public Cursor getPWTTyreDetails(String serialNo) {
		try {
			String sql = "Select (Select Description from TyreBrand where ID = IDTireBrand), FSize, AspectRatio, Diameter, Deseign, FullTireDetails from tiremodel where ID = (Select SAPMaterialCodeID from tyre where SerialNumber = '"
					+ serialNo + "')";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getPWTTyreDetails >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getPWTTyreDetailsByTyreId(String tyreid) {
		try {
			String sql = "Select (Select Description from TyreBrand where ID = IDTireBrand), FSize, AspectRatio, Diameter, Deseign, FullTireDetails from tiremodel where ID = (Select SAPMaterialCodeID from tyre where ID = '"
					+ tyreid + "')";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getPWTTyreDetails >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// getPWTSerials
	public Cursor getPWTSerial(String extId) {
		try {
			String sql = "Select  SerialNumber from tyre where ID = '" + extId
					+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getPWTSerials >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// test function not used anywhere
	public Cursor getSerials() {
		try {
			String sql = "Select SerialNumber from tyre";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getPWTTyreDetails >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// getTyreDesignDetails
	public Cursor getTyreSerialNumberForValidation() {
		// TODO Auto-generated method stub

		try {
			String sql = "Select SerialNumber from Tyre Group By SerialNumber";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreSerialNumberForValidation >>"
							+ mSQLException.toString());
			throw mSQLException;
		}
	}

	// JobTable
	public Cursor getAllValuesFromJobTable() {
		try {
			// String sql = "Select * from Job where ID= 1 ";
			String sql = "Select * from Job J LEFT JOIN Contract C ON J.SAPContractNumber = C.ID "
					+ "where J.ID = '"
					+ Constants.GETSELECTEDLISTPOSITION
					+ "' ";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreSerialNumberForValidation >>"
							+ mSQLException.toString());
			throw mSQLException;
		}

	}

	public Cursor getReserveTyreDetails() {
		try {

			String sql = "SELECT * FROM ReservedTyre as R "
					+ "LEFT JOIN Job J ON R.JobID = J.ExternalID "
					+ "LEFT JOIN Tyre as T ON R.TyreID = T.ID "
					+ "LEFT JOIN TireModel as M ON T.SAPMaterialCodeID = M.ID "
					+ "LEFT JOIN TyreBrand as B ON M.IDTireBrand = B.ID "
					+ "WHERE J.ID = '" + Constants.GETSELECTEDLISTPOSITION
					+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreSearchResultForReservePWTWithCP >>"
					+ mSQLException.toString());
			throw mSQLException;
		}
	}

	public void updateJobStatus() {
		try {
			// String sql = "Select * from Job where ID= 1 ";
			String sql = "UPDATE Job SET Status = \"" + 2 + "\" WHERE ID = 1";
			mDb.execSQL(sql);

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreSerialNumberForValidation >>"
							+ mSQLException.toString());
			throw mSQLException;
		}

	}

	// MKS Dev (Starts)
	public Cursor getContaractTyrePolicy(int sapContractNumber) {
		String sql = "";
		Cursor mCur;
		try {
			sql = "SELECT GeometryCheck,GeometryCorrection FROM ContractTyrePolicy WHERE SAPContractNumber ='"
					+ sapContractNumber + "'";
			mCur = mDb.rawQuery(sql, null);
		} catch (SQLException mSQLException) {
			mSQLException.printStackTrace();
			Log.e(TAG, "getContaractTyrePolicy(int sapContractNumber) >>"
					+ mSQLException.toString());
			throw mSQLException;
		}
		return mCur;
	}

	// Getting The Purchasing Org by userID and vendorSAPCode
	public String getPurchasingOrganization(String userID, String vendorSAPCode) {
		String sql = "", purchasingOrg = "";
		Cursor mCursor = null;
		try {
			sql = "SELECT PurchasingOrg FROM Technician WHERE UserID='"
					+ userID + "' AND VendorSAPCode='" + vendorSAPCode + "'";
			System.out.println(sql);
			mCursor = mDb.rawQuery(sql, null);
			if (mCursor.moveToFirst()) {
				purchasingOrg = mCursor.getString(mCursor
						.getColumnIndex("PurchasingOrg"));
				purchasingOrg = purchasingOrg.trim();
			}
		} catch (SQLiteException e) {
			System.out
					.println("Opps,Exception In TestAdapter=>getPurchasingOrganization(String userID,String vendorSAPCode) : ");
			e.printStackTrace();
		}
		return purchasingOrg;
	}

	public Cursor getAdditionalServices(String purchasingOrg,
			int isBreakeDownOnly) {
		String sql = "";
		Cursor mCursor = null;
		try {
			// sql =
			// "SELECT description,FixedQuantity FROM ServiceMaterial WHERE PurchOrgs='"+purchasingOrg+"' AND IsBreakeDownOnly='"+isBreakeDownOnly+"' ORDER BY Description";
			sql = "SELECT SAPCode,Description,FixedQuantity, fav FROM ServiceMaterial WHERE PurchOrgs like'%"
					+ purchasingOrg
					+ "%' AND IsBreakeDownOnly='"
					+ isBreakeDownOnly
					+ "' ORDER BY Description COLLATE NOCASE";
			System.out.println(sql);
			mCursor = mDb.rawQuery(sql, null);

		} catch (SQLiteException e) {
			System.out
					.println("Opps,Exception In TestAdapter=>getAdditionalServices(String purchasingOrg,int IsBreakeDownOnly) : ");
			e.printStackTrace();
		}
		return mCursor;
	}

	public void updateFavService(String serviceName, int favouriteStatus) {
		String sql = "Update ServiceMaterial set fav = '" + favouriteStatus
				+ "' where Description = '" + serviceName + "'";
		mDb.execSQL(sql);
	}

	public Cursor getServiceFavouriteStatus(String serviceName) {
		String sql = "select fav from ServiceMaterial where Description = '"
				+ serviceName + "'";
		return mDb.rawQuery(sql, null);
	}

	// update account table,
	public void updateAccountTable(int jobrefno) throws Exception {
		System.out.println("Fianl Constants.COMESFROMUPDATE:::: "
				+ Constants.EDIT_JOB);
		if (Constants.EDIT_JOB) {
			Constants.EDIT_JOB = false;
			return;
		}
		try {
			String sql = "UPDATE Account SET jobRefNumber = \"" + jobrefno
					+ "\" WHERE ID = 1";
			mDb.execSQL(sql);

		} catch (SQLException mSQLException) {
			CommonUtils.log(mSQLException.toString());
			Log.e(TAG, "cannot update session info" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// JobTable
	public Cursor getRefNoFromAccountTable() {
		try {
			// String sql = "Select * from Job where ID= 1 ";
			String sql = "Select jobRefNumber from Account where ID=  1";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreSerialNumberForValidation >>"
							+ mSQLException.toString());
			throw mSQLException;
		}

	}

	// Getting The Purchasing Org by userID and vendorSAPCode
	public String getServiceByserviceId(int sapCode) {
		String sql = "", serviceDesc = "";
		Cursor mCursor = null;
		try {
			// removed Active based on bug number 471
			sql = "SELECT description FROM ServiceMaterial WHERE SAPCode='"
					+ sapCode + "'";
			System.out.println(sql);
			mCursor = mDb.rawQuery(sql, null);
			if (mCursor.moveToFirst()) {
				serviceDesc = mCursor.getString(mCursor
						.getColumnIndex("Description"));
				serviceDesc = serviceDesc.trim();
			}
		} catch (SQLiteException e) {
			Log.e(TAG,
					"getServiceByserviceId(String sapCode) >>" + e.toString());
			System.out
					.println("Opps,Exception In TestAdapter=>getPurchasingOrganization(String userID,String vendorSAPCode) : ");
			e.printStackTrace();
		}
		return serviceDesc;
	}

	public Cursor getOdometerReason() {
		try {
			String sql = "SELECT ID,Reason,Code from OdometerReason order by reason";
			Cursor mCur = mDb.rawQuery(sql, null);
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getOdometerReason()  >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getSignatureReason() {
		try {
			String sql = "SELECT externalId,reason FROM SignatureReason order by reason";
			Cursor mCur = mDb.rawQuery(sql, null);
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getSignatureReason()  >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// Reserve PWT SELECT t.SerialNumber as SerialNumber, tm.FullTireDetails AS
	// FullTireDetails From Tyre t, Contract c LEFT OUTER JOIN TireModel tm ON
	// t.SAPMaterialCodeID= tm.ID LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand
	// = b.ID WHERE c.ID = t.SAPContractNumberID AND t.SerialNumber like '%{0}%'
	// AND tm.Diameter = {4} AND tm.FSize = {5} AND tm.AspectRatio = {6} AND
	// ((t.IsUnmounted = {1} AND (c.ContractType = {2} OR c.ContractType =
	// {3})AND c.CountryCode = '{7}' AND t.SAPVendorCodeID = {8}) OR Shared = 1)
	// AND (T.FromJobID NOT IN (SELECT ID FROM JOB) or T.FromJobID is null)

	public Cursor getSizeASPRIMFromTireID(String tireIDList) {
		String sql = "SELECT DISTINCT tm.FSize, tm.AspectRatio , tm.Diameter , t.SerialNumber "
				+ "From Tyre t LEFT OUTER JOIN TireModel tm ON t.SAPMaterialCodeID = tm.ID  "
				+ "WHERE t.ID in (" + tireIDList + ")";
		Cursor mCur = mDb.rawQuery(sql, null);
		return mCur;
	}

	public Cursor getTyreSearchResultForReservePWT(String serialNumber,
			String sapVendorCodeID, String sapcontractID) {
		try {

/*			String sql = "SELECT DISTINCT t.SerialNumber, tm.FullTireDetails, t.ID From Tyre t LEFT OUTER JOIN TireModel tm ON t.SAPMaterialCodeID= tm.ID "
					+ "LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID WHERE t.SerialNumber like '%"
					+ serialNumber
					+ "%' AND ((t.IsUnmounted = 1 AND "
					+ "t.SAPContractNumberID = "
					+ sapcontractID
					+ " AND t.SAPVendorCodeID = "
					+ sapVendorCodeID
					+ ") OR Shared = 1) AND (T.FromJobID NOT IN "
					+ "(SELECT ID FROM JOB) or T.FromJobID is null) "
					+ " AND (t.ID NOT IN (SELECT TyreID FROM ReservedTyre))";*/

			/**
			 * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
			 * If material and/or SN is changed when a tyre is removed - this tyre should not be made available immediately
			 * Maintain temporary SAPVENDERCodeID until SAP updates
			 * Since SAPVENDERCodeID for few tyres are null, so by maintaining temporary SAPVENDERCodeID, will make PWT tyre with SAPVENDERCodeID as null, available for Reserve PWT.
			 * Query altered - to support temporary SAPVENDERCodeID and material and/or SN is changes
			 */
			String sql = "SELECT DISTINCT t.SerialNumber, tm.FullTireDetails, t.ID From Tyre t LEFT OUTER JOIN TireModel tm ON t.SAPMaterialCodeID= tm.ID "
					+ "LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID WHERE t.SerialNumber like '%"
					+ serialNumber
					+ "%' AND ((t.IsUnmounted = 1 AND "
					+ "t.SAPContractNumberID = "
					+ sapcontractID
					+ " AND (t.SAPVendorCodeID = '"
					+ sapVendorCodeID
					+ "' OR t.TempSAPVendorCodeID = '"
					+ sapVendorCodeID + "')"
					+ " AND t.IsTyreCorrected = 0 "
					+ ") OR Shared = 1) AND (T.FromJobID NOT IN "
					+ "(SELECT ID FROM JOB) or T.FromJobID is null) "
					+ " AND (t.ID NOT IN (SELECT TyreID FROM ReservedTyre))";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreSearchResultForReservePWT >>"
							+ mSQLException.toString());
			throw mSQLException;
		}
	}

	public void deleteReservedTire(String tyreID) {
		String sql = "delete from ReservedTyre where TyreID = '" + tyreID + "'";
		try {
			mDb.execSQL(sql);
		} catch (SQLException e) {
			LogUtil.e(TAG, "could not delete ");
		}

	}

	/**
	 * Method to delete unused reserved PWT tyre information based on JobID
	 * @param jobrefnum Job reference number
     */
	public void deleteReservedUnusedPWTTireBasedOnJobReferenceNumber(String jobrefnum) {
		String sql = "delete from ReservedTyre where JobID in (Select ExternalID from Job where jobRefNumber = '"+jobrefnum+"')";
		try {
			mDb.execSQL(sql);
		} catch (SQLException e) {
			LogUtil.e(TAG, "could not delete ");
		}

	}

	public Cursor getTyreSearchResultForReservePWTWithSize(String serialNumber,
			ArrayList<String> diameter, ArrayList<String> fSize,
			ArrayList<String> aspectRatio, String sapContractID,
			String sapVendorCodeID) {
		try {
			String sqlcondn = "";
			if (diameter.size() > 0 && fSize.size() > 0) {
				for (int i = 0; i < diameter.size(); i++) {
					String sqlcondnTemp = " (tm.Diameter = " + diameter.get(i)
							+ " AND " + "tm.FSize = " + fSize.get(i)
							+ " AND tm.AspectRatio = " + aspectRatio.get(i)
							+ ") ";
					if (i == 0) {
						sqlcondn = " AND (" + sqlcondnTemp;
					} else {
						sqlcondn += " OR " + sqlcondnTemp;
					}
				}
				sqlcondn += ")";
			}

/*			String sql = "SELECT DISTINCT t.SerialNumber, tm.FullTireDetails, t.ID From Tyre t LEFT OUTER JOIN TireModel tm ON t.SAPMaterialCodeID= tm.ID "
					+ "LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID "
					+ "WHERE t.SerialNumber like '%"
					+ serialNumber
					+ "%' "
					+ sqlcondn
					+ " AND ((t.IsUnmounted = 1 AND t.SAPContractNumberID = "
					+ sapContractID
					+ " AND "
					+ "t.SAPVendorCodeID = "
					+ sapVendorCodeID
					+ ") OR Shared = 1) "
					+ "AND (T.FromJobID NOT IN (SELECT ID FROM JOB) or T.FromJobID is null) "
					+ " AND (t.ID NOT IN (SELECT TyreID FROM ReservedTyre))";*/

			/**
			 * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
			 * If material and/or SN is changed when a tyre is removed - this tyre should not be made available immediately
			 * Maintain temporary SAPVENDERCodeID until SAP updates
			 * Since SAPVENDERCodeID for few tyres are null, so by maintaining temporary SAPVENDERCodeID, will make PWT tyre with SAPVENDERCodeID as null, available for Reserve PWT.
			 * Query altered - to support temporary SAPVENDERCodeID and material and/or SN is changes
			 */
			String sql = "SELECT DISTINCT t.SerialNumber, tm.FullTireDetails, t.ID From Tyre t LEFT OUTER JOIN TireModel tm ON t.SAPMaterialCodeID= tm.ID "
					+ "LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID "
					+ "WHERE t.SerialNumber like '%"
					+ serialNumber
					+ "%' "
					+ sqlcondn
					+ " AND ((t.IsUnmounted = 1 AND t.SAPContractNumberID = "
					+ sapContractID
					+ " AND (t.SAPVendorCodeID = '"
					+ sapVendorCodeID
					+ "' OR t.TempSAPVendorCodeID = '"
					+ sapVendorCodeID + "')"
					+ " AND t.IsTyreCorrected = 0 "
					+ ") OR Shared = 1) "
					+ "AND (T.FromJobID NOT IN (SELECT ID FROM JOB) or T.FromJobID is null) "
					+ " AND (t.ID NOT IN (SELECT TyreID FROM ReservedTyre))";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreSearchResultForReservePWTWithSize >>"
					+ mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getTyreSearchResultForReservePWTWithCP(String serialNumber,
			String countryCode, String sapVendorCodeID) {
		try {

/*			String sql = "SELECT DISTINCT t.SerialNumber, tm.FullTireDetails, t.ID From Tyre t, Contract c LEFT OUTER JOIN TireModel tm ON "
					+ "t.SAPMaterialCodeID= tm.ID LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID WHERE t.SerialNumber "
					+ "like '%"
					+ serialNumber
					+ "%' AND ((c.ID = t.SAPContractNumberID AND t.IsUnmounted = 1 AND (c.ContractType = 1 OR c.ContractType = 2) "
					+ "AND c.CountryCode = '"
					+ countryCode
					+ "' AND t.SAPVendorCodeID = "
					+ sapVendorCodeID
					+ ") OR Shared = 1) AND(T.FromJobID NOT IN (SELECT ID FROM JOB)"
					+ " or T.FromJobID is null) "
					+ " AND (t.ID NOT IN (SELECT TyreID FROM ReservedTyre))";*/

			/**
			 * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
			 * If material and/or SN is changed when a tyre is removed - this tyre should not be made available immediately
			 * Maintain temporary SAPVENDERCodeID until SAP updates
			 * Since SAPVENDERCodeID for few tyres are null, so by maintaining temporary SAPVENDERCodeID, will make PWT tyre with SAPVENDERCodeID as null, available for Reserve PWT.
			 * Query altered - to support temporary SAPVENDERCodeID and material and/or SN is changes
			 */
			String sql = "SELECT DISTINCT t.SerialNumber, tm.FullTireDetails, t.ID From Tyre t, Contract c LEFT OUTER JOIN TireModel tm ON "
					+ "t.SAPMaterialCodeID= tm.ID LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID WHERE t.SerialNumber "
					+ "like '%"
					+ serialNumber
					+ "%' AND ((c.ID = t.SAPContractNumberID AND t.IsUnmounted = 1 AND (c.ContractType = 1 OR c.ContractType = 2) "
					+ "AND c.CountryCode = '"
					+ countryCode
					+ "' AND (t.SAPVendorCodeID = '"
					+ sapVendorCodeID
					+ "' OR t.TempSAPVendorCodeID = '"
					+ sapVendorCodeID + "')"
					+ " AND t.IsTyreCorrected = 0 "
					+ ") OR Shared = 1) AND(T.FromJobID NOT IN (SELECT ID FROM JOB)"
					+ " or T.FromJobID is null) "
					+ " AND (t.ID NOT IN (SELECT TyreID FROM ReservedTyre))";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreSearchResultForReservePWTWithCP >>"
					+ mSQLException.toString());
			throw mSQLException;
		}
	}

	/*
	 * public Cursor getTyreSearchResultForReservePWTWithCPAndSize( String
	 * serialNumber, String diameter, String fSize, String aspectRatio, String
	 * countryCode, String sapVendorCodeID) { try {
	 * 
	 * String sqlcondn = ""; if(!TextUtils.isEmpty(diameter) &&
	 * !TextUtils.isEmpty(fSize)){ if(stringToFloat(diameter) > 0 ||
	 * stringToFloat(fSize) > 0){ sqlcondn = " AND tm.Diameter = "+ diameter +
	 * " AND "+ "tm.FSize = "+ fSize+ " " + " AND tm.AspectRatio = "+
	 * aspectRatio+" "; } }
	 * 
	 * String sql =
	 * "SELECT DISTINCT t.SerialNumber, tm.FullTireDetails, t.ID From Tyre t, Contract c "
	 * + " LEFT OUTER JOIN TireModel tm ON t.SAPMaterialCodeID = tm.ID " +
	 * " LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID " +
	 * " WHERE c.ID = t.SAPContractNumberID AND t.SerialNumber like '%"
	 * +serialNumber+"%' " + sqlcondn +
	 * " AND ((t.IsUnmounted = 1 AND (c.ContractType = 1 OR c.ContractType = 2) "
	 * + " AND c.CountryCode = '"+ countryCode+ "' AND t.SAPVendorCodeID = "+
	 * sapVendorCodeID+")" +
	 * " OR Shared = 1) AND (T.FromJobID NOT IN (SELECT ID FROM JOB) or T.FromJobID is null) "
	 * + " AND (t.ID NOT IN (SELECT TyreID FROM ReservedTyre))"; Cursor mCur =
	 * mDb.rawQuery(sql, null); if (mCur != null) { mCur.moveToNext();
	 * System.out.println("Data inside if" + mCur); }
	 * 
	 * return mCur;
	 * 
	 * } catch (SQLException mSQLException) { Log.e(TAG,
	 * "getTyreSearchResultForReservePWTWithCPAndSize >>" +
	 * mSQLException.toString()); throw mSQLException; } }
	 */

	public Cursor getTyreSearchResultForReservePWTWithCPAndSize(
			String serialNumber, ArrayList<String> diameter,
			ArrayList<String> fSize, ArrayList<String> aspectRatio,
			String countryCode, String sapVendorCodeID) {
		try {

			String sqlcondn = "";
			if (diameter.size() > 0 && fSize.size() > 0) {
				for (int i = 0; i < diameter.size(); i++) {
					String sqlcondnTemp = " (tm.Diameter = " + diameter.get(i)
							+ " AND " + "tm.FSize = " + fSize.get(i)
							+ " AND tm.AspectRatio = " + aspectRatio.get(i)
							+ ") ";
					if (i == 0) {
						sqlcondn = " AND (" + sqlcondnTemp;
					} else {
						sqlcondn += " OR " + sqlcondnTemp;
					}
				}
				sqlcondn += ")";
			}

/*
			String sql = "SELECT DISTINCT t.SerialNumber, tm.FullTireDetails, t.ID From Tyre t, Contract c "
					+ " LEFT OUTER JOIN TireModel tm ON t.SAPMaterialCodeID = tm.ID "
					+ " LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID "
					+ " WHERE c.ID = t.SAPContractNumberID AND t.SerialNumber like '%"
					+ serialNumber
					+ "%' "
					+ sqlcondn
					+ " AND ((t.IsUnmounted = 1 AND (c.ContractType = 1 OR c.ContractType = 2) "
					+ " AND c.CountryCode = '"
					+ countryCode
					+ "' AND t.SAPVendorCodeID = "
					+ sapVendorCodeID
					+ ")"
					+ " OR Shared = 1) AND (T.FromJobID NOT IN (SELECT ID FROM JOB) or T.FromJobID is null) "
					+ " AND (t.ID NOT IN (SELECT TyreID FROM ReservedTyre))";
*/

			/**
			 * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
			 * If material and/or SN is changed when a tyre is removed - this tyre should not be made available immediately
			 * Maintain temporary SAPVENDERCodeID until SAP updates
			 * Since SAPVENDERCodeID for few tyres are null, so by maintaining temporary SAPVENDERCodeID, will make PWT tyre with SAPVENDERCodeID as null, available for Reserve PWT.
			 * Query altered -  to support temporary SAPVENDERCodeID and material and/or SN is changes
			 */
			String sql = "SELECT DISTINCT t.SerialNumber, tm.FullTireDetails, t.ID From Tyre t, Contract c "
					+ " LEFT OUTER JOIN TireModel tm ON t.SAPMaterialCodeID = tm.ID "
					+ " LEFT OUTER JOIN TyreBrand b ON tm.IDTireBrand = b.ID "
					+ " WHERE c.ID = t.SAPContractNumberID AND t.SerialNumber like '%"
					+ serialNumber
					+ "%' "
					+ sqlcondn
					+ " AND ((t.IsUnmounted = 1 AND (c.ContractType = 1 OR c.ContractType = 2) "
					+ " AND c.CountryCode = '"
					+ countryCode
					+ "' AND (t.SAPVendorCodeID = '"
					+ sapVendorCodeID
					+ "' OR t.TempSAPVendorCodeID = '"
					+ sapVendorCodeID + "')"
					+ " AND t.IsTyreCorrected = 0 "
					+ ")"
					+ " OR Shared = 1) AND (T.FromJobID NOT IN (SELECT ID FROM JOB) or T.FromJobID is null) "
					+ " AND (t.ID NOT IN (SELECT TyreID FROM ReservedTyre))";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreSearchResultForReservePWTWithCPAndSize >>"
					+ mSQLException.toString());
			throw mSQLException;
		}
	}

	// Insertion in Job Table insertTyreDamageData
	public boolean InsertReservePWT(String JobID, String TyreID) {
		try {
			ContentValues cv = new ContentValues();

			// cv.put("ID", ID);
			cv.put("JobID", JobID);
			cv.put("TyreID", TyreID);

			mDb.insert("ReservedTyre", null, cv);

			Log.d("SaveJob", "ReservedTyre Data Inserted in ReservedTyre Table");
			return true;

		} catch (Exception ex) {
			Log.d("SaveJob", ex.toString());
			return false;
		}
	}

	// delete reserved tyre based on jobID
	public boolean releaseReservedTyre(String JobExternalID) {
		try {
			String SQL = "SELECT T.ID AS TYREID FROM Tyre T "
					+ " LEFT JOIN JobItem J ON T.ExternalID = J.TyreID "
					+ " WHERE J.ActionType = 25 AND J.JobID = '"
					+ JobExternalID + "'";
			Cursor mCur = mDb.rawQuery(SQL, null);
			if (mCur != null & mCur.getCount() > 0) {
				for (int i = 0; i < mCur.getCount(); i++) {
					if (mCur.moveToPosition(i)) {
						String tyreid = mCur.getString(mCur
								.getColumnIndex("TYREID"));
						updateIsUnmounted(tyreid, 1);
					}
				}
			}
			mDb.delete("ReservedTyre", " JobID = '" + JobExternalID + "'", null);

			LogUtil.d("releaseReservedTyre",
					"ReservedTyre Data is being deleted from ReservedTyre Table");
			return true;
		} catch (Exception ex) {
			Log.d("releaseReservedTyre", ex.toString());
			return false;
		}
	}

	// update the IsUnmounted column
	public int updateIsUnmounted(String tyreId, int isunmounted) {
		try {
			ContentValues cv = new ContentValues();
			cv.put("IsUnmounted", isunmounted);
			return mDb.update("Tyre", cv, "ID = '" + tyreId + "' ", null);
		} catch (Exception ex) {
			Log.d("updateIsUnmounted", ex.toString());
			return 0;
		}
	}

	// Insertion in Job Table insertTyreDamageData
	public boolean insertTyreDamageData(int JobItemID, String ImageType,
			byte[] TyreImage, String Description) {
		try {
			ContentValues cv = new ContentValues();

			// cv.put("ID", ID);
			cv.put("JobItemID", JobItemID);
			cv.put("ImageType", ImageType);
			cv.put("TyreImage", TyreImage);
			cv.put("Description", Description);

			mDb.insert("TyreImage", null, cv);

			Log.d("SaveJob",
					"insertTyreDamage Data Inserted in TyreDamageData Table");
			return true;

		} catch (Exception ex) {
			Log.d("SaveJob", ex.toString());
			return false;
		}
	}

	/**
	 * API to check the if job is present in the jobItem.
	 * 
	 * @param jobId
	 * @return
	 */
	public boolean isJobPresentAtJobItem(String jobId) {

		Cursor tierIdCur = null;
		try {
			String rawQuery = "select TyreId from JobItem where JobID = '"
					+ jobId + "'";
			tierIdCur = mDb.rawQuery(rawQuery, null);
			if (CursorUtils.isValidCursor(tierIdCur)) {
				tierIdCur.moveToFirst();
				return tierIdCur.getCount() > 0 ? true : false;
			}
		} catch (SQLException e) {
			LogUtil.e(TAG, "getTyreIdFromJobItem >>" + e.toString());
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(tierIdCur);
		}
		return false;
	}

	/**
	 * API to get the Tire Id from JobItem
	 * 
	 * @param jobId
	 * @return
	 */
	public ArrayList<String> getTyreIdFromJobItem(String jobId) {
		ArrayList<String> tyreIddsFromJobItem = new ArrayList<String>();
		Cursor tierIdCur = null;
		try {
			String rawQuery = "select TyreId from JobItem where JobID = '"
					+ jobId + "'";
			tierIdCur = mDb.rawQuery(rawQuery, null);
			if (CursorUtils.isValidCursor(tierIdCur)) {
				tierIdCur.moveToFirst();
				do {
					tyreIddsFromJobItem.add(tierIdCur.getString(0));
				} while (tierIdCur.moveToNext());
			}
		} catch (SQLException e) {
			LogUtil.e(TAG, "getTyreIdFromJobItem >>" + e.toString());
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(tierIdCur);
		}
		return tyreIddsFromJobItem;
	}

	/**
	 * API to get the tire id ActionType from job table
	 * 
	 * @param tyreId
	 * @param jobId
	 * @return
	 */
	public String getTyreIdActionTypeFromJobItem(String tyreId, String jobId) {
		Cursor tierIdCur = null;
		try {
			String rawQuery = "select ActionType from JobItem where JobID = '"
					+ jobId + "'" + " AND TyreID = '" + tyreId + "'";
			LogUtil.e(TAG, "getTyreIdActiobTypeFromJobItem:: " + rawQuery);
			tierIdCur = mDb.rawQuery(rawQuery, null);
			if (CursorUtils.isValidCursor(tierIdCur)) {
				tierIdCur.moveToFirst();
				return (tierIdCur.getString(0));
			}
		} catch (SQLException e) {
			LogUtil.e(
					TAG,
					"Exception getTyreIdActiobTypeFromJobItem >>"
							+ e.toString());
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(tierIdCur);
		}
		return "";
	}

	/**
	 * API to get InspectionStatus of a Tyre
	 * @param tyreId tyreid
	 * @param jobId job id
	 * @return
	 */
	public Boolean getInspectionStatusFromJobItem(String tyreId, String jobId) {
		Cursor tierIdCur = null;
		try {
			String rawQuery = "select ActionType from JobItem where JobID = '"
					+ jobId + "'" + " AND TyreID = '" + tyreId + "' " + "AND ActionType = 26 ";
			LogUtil.e(TAG, "getInspectionStatusFromJobItem:: " + rawQuery);
			tierIdCur = mDb.rawQuery(rawQuery, null);
			if (CursorUtils.isValidCursor(tierIdCur)) {
				return true;
			}
		} catch (SQLException e) {
			LogUtil.e(
					TAG,
					"Exception getInspectionStatusFromJobItem >>"
							+ e.toString());
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(tierIdCur);
		}
		return false;
	}


	/**
	 * API to get the tire id ActionType from job table
	 * 
	 * @param tyreId
	 * @param jobId
	 * @return
	 */
	public boolean isMountOperationPerformed(String tyreId, String jobId) {
		boolean isDismounted = false;
		Cursor tierIdCur = null;
		try {
			String rawQuery = "select * from JobItem where JobID = '" + jobId
					+ "'" + " AND TyreID = '" + tyreId
					+ "' AND (ActionType = '2' OR ActionType = '25')";
			LogUtil.e(TAG, "getTyreIdActiobTypeFromJobItem:: " + rawQuery);
			tierIdCur = mDb.rawQuery(rawQuery, null);
			if (CursorUtils.isValidCursor(tierIdCur)) {
				if (tierIdCur.getCount() > 0) {
					isDismounted = true;
				}
			}
		} catch (SQLException e) {
			LogUtil.e(
					TAG,
					"Exception getTyreIdActiobTypeFromJobItem >>"
							+ e.toString());
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(tierIdCur);
		}
		return isDismounted;
	}

	/**
	 * API to check if tire present in job Item table
	 * 
	 * @param tyreId
	 * @return
	 */
	public boolean isTyrePresentInJobItem(String tyreId) {
		Cursor tierPresentCur = null;
		try {
			String rawQuery = "select * from JobItem where TyreID= '" + tyreId
					+ "'";
			LogUtil.i(TAG, "isTyrePresentInJobItem >>" + rawQuery);
			tierPresentCur = mDb.rawQuery(rawQuery, null);
			if (CursorUtils.isValidCursor(tierPresentCur)
					&& tierPresentCur.moveToFirst()) {
				return (tierPresentCur.getCount() > 0) ? true : false;
			}
		} catch (SQLException e) {
			LogUtil.e(TAG, "getTyreIdFromJobItem >>" + e.toString());
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(tierPresentCur);
		}
		return false;
	}

	public Cursor getAllValuesFromJobItemTable(String jobid) {
		try {
			// String sql = "Select * from Job where ID= 1 ";
			String sql = "Select * from JobItem where JobID =  '" + jobid
					+ "' ";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreSerialNumberForValidation >>"
							+ mSQLException.toString());
			throw mSQLException;
		}

	}

	public Cursor getAllValuesFromVehicleTable() {
		try {
			// String sql = "Select * from Job where ID= 1 ";
			String sql = "Select * from Vehicle where SAPCodeVeh=  '"
					+ Constants.VEHICLEIDFORCONFIG + "' ";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreSerialNumberForValidation >>"
							+ mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getValuesFromReservedTyre(String jobid) {
		try {
			String sql = "Select * from ReservedTyre where JobID =  '" + jobid
					+ "' ";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getValuesFromReservedTyre >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getAllValuesFromTireImageTable(String jobid) {
		try {
			// String sql = "Select * from Job where ID= 1 ";
			String sql = "Select * from TyreImage where JobItemID =  '" + jobid
					+ "' ";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreSerialNumberForValidation >>"
							+ mSQLException.toString());
			throw mSQLException;
		}

	}

	public Cursor getAllValuesFromTyreTable(String code) {
		try {
			// String sql = "Select * from Job where ID= 1 ";
			String sql = "Select * from Tyre where idVehicleJob = '" + code
					+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreSerialNumberForValidation >>"
							+ mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getMountedTyreFromTyreTable(String code) {
		try {
			String sql = "Select T.ID as TYREID,T.ExternalID AS TIREEXTERNALID,T.*,"
					+ "TM.ID as TYREMODELID,TM.IDTireBrand,TM.FSize,TM.AspectRatio,TM.Diameter,"
					+ "TM.Deseign,TM.FullTireDetails,"
					+ "TB.ID as TYREBRANDID,TB.Description,"
					+ "TD.ID as TYREDESIGNID,TD.Design FROM Tyre T "
					+ "LEFT JOIN TireModel TM ON T.SAPMaterialCodeID = TM.ID "
					+ "LEFT JOIN TyreBrand TB ON TM.IDTireBrand = TB.ID "
					+ "LEFT JOIN TyreDesign TD ON TM.DesignID = TD.ID "
					+ "WHERE T.idVehicleJob = '" + code + "' "; // AND
																// T.IsUnmounted
																// = 0
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			throw mSQLException;
		}
	}

	public Cursor getTireModelInfo(String code) {
		try {
			// String sql = "Select * from Job where ID= 1 ";
			String sql = "Select * from TireModel where ID = '" + code + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreSerialNumberForValidation >>"
							+ mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getAllValuesFromJobCorrectionTable(String jobid) {
		try {
			// String sql = "Select * from Job where ID= 1 ";
			String sql = "Select * from JobCorrection where JobID = '" + jobid
					+ "' ";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreSerialNumberForValidation >>"
							+ mSQLException.toString());
			throw mSQLException;
		}

	}

	public Cursor getAllValuesFromTorqueSettings(String jobid) {
		try {
			// String sql = "Select * from Job where ID= 1 ";
			String sql = "Select * from TorqueSetting where JobID = '" + jobid
					+ "' ";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreSerialNumberForValidation >>"
							+ mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getJobStatus() {
		try {
			String sql = "Select LicenseNumber,SAPContractNumber,VehicleID from Job where Status in (0, 1)";
			Cursor mCur = mDb.rawQuery(sql, null);
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getJobStatus()  >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getJOBRemovalReason() {
		try {
			String sql = "Select Reason from JobRemovalReason";
			Cursor mCur = mDb.rawQuery(sql, null);
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getJOBRemovalReason()  >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getJOBRemovalReasonID(String reason) {
		try {
			String sql = "Select ID from JobRemovalReason Where reason = '"
					+ reason + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getJOBRemovalReasonID()  >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	// updateJOBTableFOrDelete
	public void updateJOBTableWithStatus(String jobReferenceNumber,
			String status) throws Exception {
		try {
			String sql = "UPDATE Job SET Status = '" + status + "'"
					+ " WHERE jobRefNumber = " + jobReferenceNumber;
			mDb.execSQL(sql);

		} catch (SQLException mSQLException) {
			CommonUtils.log(mSQLException.toString());
			Log.e(TAG, "updateJOBTableFOrDelete" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public void updateJOBTableWithStatusOnDelete(String jobRefNumber,
			int removalID) throws Exception {
		try {
			String sql = "UPDATE Job SET Status = 4, JobRemovalReasonID = "
					+ removalID + " WHERE jobRefNumber = " + jobRefNumber + "";
			mDb.execSQL(sql);

		} catch (SQLException mSQLException) {
			CommonUtils.log(mSQLException.toString());
			Log.e(TAG, "updateJOBTableFOrDelete" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getTyreOriginalNSK(String ID) {
		try {
			String sql = "SELECT  OriginalNSK FROM Tyre where ID = '" + ID
					+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (CursorUtils.isValidCursor(mCur)) {
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreOriginalNSK >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * get tire's nsk 1,2,3 from db
	 * 
	 * @param ID
	 *            tire ID
	 * @return NSK1, NSK2, NSK3
	 */
	public Cursor getTyreNSK(String ID) {
		try {
			String sql = "SELECT  NSK1,NSK2,NSK3 FROM Tyre where ID = '" + ID
					+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (CursorUtils.isValidCursor(mCur)) {
				System.out.println("Data inside if" + mCur);
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreOriginalNSK >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getTyreOriginalNSKByTyreId(String tyreid) {
		try {
			String sql = "SELECT  OriginalNSK FROM Tyre where ID = '" + tyreid
					+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreOriginalNSK >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public String getExternalIdFromSerial(String serialNo) {
		try {
			String sql = "SELECT  ExternalID FROM Tyre where SerialNumber = '"
					+ serialNo + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				System.out.println("Data inside if" + mCur);
			}
			if (mCur.getCount() > 0) {
				return mCur.getString(mCur.getColumnIndex("ExternalID"));
			} else {
				return "";
			}

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getExternalIdFromSerial >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public String getExternalIdFromSerialByTyreId(String tyreid) {
		try {
			String sql = "SELECT  ExternalID FROM Tyre where ID = '" + tyreid
					+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				System.out.println("Data inside if" + mCur);
			}
			if (mCur.getCount() > 0) {
				return mCur.getString(mCur.getColumnIndex("ExternalID"));
			} else {
				return "";
			}

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getExternalIdFromSerial >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * get the SAP Code TI
	 * 
	 * @param serialNo
	 *            serial number of the tire
	 * @return blank if
	 */
	public String getSAPCodeTIIdFromSerial(String serialNo) {
		try {
			String sql = "SELECT SAPCodeTi FROM Tyre where SerialNumber = '"
					+ serialNo + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (CursorUtils.isValidCursor(mCur) && mCur.moveToFirst()) {
				String sapCode = mCur.getString(mCur
						.getColumnIndex("SAPCodeTi"));
				CursorUtils.closeCursor(mCur);
				return sapCode;
			} else {
				CursorUtils.closeCursor(mCur);
				return "";
			}
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getExternalIdFromSerial >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public String getSAPCodeTIIdFromSerialByTyreId(String tyreid) {
		try {
			String sql = "SELECT SAPCodeTi FROM Tyre where ID = '" + tyreid
					+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (CursorUtils.isValidCursor(mCur) && mCur.moveToFirst()) {
				String sapCode = mCur.getString(mCur
						.getColumnIndex("SAPCodeTi"));
				CursorUtils.closeCursor(mCur);
				return sapCode;
			} else {
				CursorUtils.closeCursor(mCur);
				return "";
			}
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getExternalIdFromSerial >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public ArrayList<String> getDamageAreaAndGroup(String externalID) {
		ArrayList<String> mDamageDeatils = new ArrayList<String>();
		try {

			String sql = "Select DamageGroup,RemovalReasonCode FROM Damage WHERE ExternalID = (SELECT DamageID from JobItem where TyreID = '"
					+ externalID + "')";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				System.out.println("Data inside if" + mCur);
			}
			if (mCur.getCount() > 0) {
				mDamageDeatils.add(mCur.getString(mCur
						.getColumnIndex("DamageGroup")));
				mDamageDeatils.add(mCur.getString(mCur
						.getColumnIndex("RemovalReasonCode")));
			}

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getExternalIdFromSerial >>" + mSQLException.toString());
			throw mSQLException;
		}
		return mDamageDeatils;
	}

	/**
	 * Nikhil V Function to fetch images from TyreImage
	 * 
	 * @param JobItemID
	 * @return
	 * @throws SQLException
	 */
	public Cursor getTireSummaryImages(int JobItemID) throws SQLException {
		Cursor mCursor = mDb.query(true, "TyreImage",
				new String[] { "TyreImage"

				}, "JobItemID" + "= '" + JobItemID + "'", null, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public Cursor getlNSK(String desien, String fullDetails) {
		try {
			String sql = "SELECT  OriginalTD FROM TireModel where Deseign = '"
					+ desien + "'" + "and FullTireDetails = '" + fullDetails
					+ "'";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreOriginalNSK >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public int getJobCount() {
		try {
			String sql = "SELECT * FROM Job";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur.getCount();

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreOriginalNSK >>" + mSQLException.toString());
			throw mSQLException;
		}
	}//

	public int getJobItemCount() {
		try {
			String sql = "SELECT * FROM JobItem";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur.getCount();
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreOriginalNSK >>" + mSQLException.toString());
			throw mSQLException;
		}
	}//

	// getSAPMatrialCodeForCorrection
	public Cursor getSAPMatrialCodeForCorrection(String fullDesign) {
		try {
			String sql = "SELECT ID from TireModel WHERE FullTireDetails ='"
					+ fullDesign + "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getSAPMatrialCodeForCorrection()  >>"
							+ mSQLException.toString());
			throw mSQLException;
		}
	}

	public int getReserveTyreCount(String customerID) {
		try {
			String sql = "SELECT * FROM Tyre Where Status = 1 AND SAPContractNumberID = '"
					+ customerID + "'";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}

			return mCur.getCount();

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getReserveTyreCount >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getCameraImage(String tyreID) {
		try {
			String sql = "Select TyreImage,Description from TyreImage where JobItemID in (Select ID from JobItem where TyreID = '"
					+ tyreID + "')";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreOriginalNSK >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor testFile() {
		try {
			String sql = "Select ID, Value from Translation";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreOriginalNSK >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getJobItemIDMAX() {
		try {
			String sql = "SELECT MAX(ID) AS ID FROM JobItem";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			/* int i = mCur.getInt(mCur.getColumnIndex("ID")); */
			return mCur;

		} catch (SQLException mSQLException) {
			throw mSQLException;
		}
	}

	public String getSelectedImageExtID(String tyreID) {
		try {
			String sql = "Select ExternalID from Tyre where SerialNumber = '"
					+ tyreID + "'";

			// Select TyreImage FROM TyreImage t join Tyre t where
			// t.SerialNumber = '1920307325'

			/*
			 * String sql =
			 * "SELECT TyreImage FROM TyreImage t join JobItem it where it.ExternalID = t.JobItemID"
			 * ;
			 */

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur.getString(mCur.getColumnIndex("ExternalID"));

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreOriginalNSK >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getSelectedFleetNO(String lPateNO) {
		try {
			String sql = "Select Vin from Vehicle where LicenseNum = '"
					+ lPateNO + "'";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreOriginalNSK >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	public Cursor getSelectedAccountName(String string) {
		try {
			String sql = "Select CustomerName from Contract where ID = '"
					+ string + "'";

			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreOriginalNSK >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * check if deleted jobs exists
	 */
	public int getCountOfJobsToSync() {
		String quer = "SELECT COUNT(*) FROM Job WHERE Status = 2 OR Status = 4 OR Status = 6";
		Cursor cur = mDb.rawQuery(quer, null);
		cur.moveToFirst();
		return cur.getInt(0);
	}

	/**
	 * 
	 * check count for about content
	 */
	public int getCountAboutRecords() {
		String quer = "SELECT COUNT(*) FROM Translation WHERE ID >= 810000 and ID <= 810099";
		Cursor cur = mDb.rawQuery(quer, null);
		cur.moveToFirst();
		return cur.getInt(0);
	}

	/**
	 * 
	 * check count for Help content
	 */
	public int getCountHelpRecords() {
		String quer = "SELECT COUNT(*) FROM Translation WHERE ID >= 830000 and ID <= 830099";
		Cursor cur = mDb.rawQuery(quer, null);
		cur.moveToFirst();
		return cur.getInt(0);
	}

	public String getDamageId(int id) {
		try {
			String qry = "Select DamageID from JobItem where ID = " + id;
			Cursor cur = mDb.rawQuery(qry, null);
			if (null != cur && cur.getCount() > 0) {
				cur.moveToFirst();
				return cur.getString(0);
			}
		} catch (SQLException ex) {

		}
		return null;
	}

	public Cursor getDamageDetails(String damageId) {
		Cursor mCursor = null;
		try {
			String qry = "Select DamageGroup, RemovalReasonCode from Damage where ExternalID = '"
					+ damageId + "'";
			mCursor = mDb.rawQuery(qry, null);
		} catch (SQLException ex) {

		}
		return mCursor;
	}

	/**
	 * Get JobItemID from jobItem table based on external Id
	 */
	public int getJobIdForImages(String externalId) {
		int id = 0;
		Cursor jobItemIdCur = null;
		try {
			String qry = "SELECT sm.ID from JobItem As sm INNER JOIN TyreImage AS s ON sm.ID == s.JobItemID where sm.ExternalID == '"
					+ externalId + "'";
			jobItemIdCur = mDb.rawQuery(qry, null);
			if (CursorUtils.isValidCursor(jobItemIdCur)) {
				jobItemIdCur.moveToFirst();
				id = jobItemIdCur.getInt(0);
			}
			CursorUtils.closeCursor(jobItemIdCur);
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			CursorUtils.closeCursor(jobItemIdCur);
		}
		return id;
	}

	/**
	 * Get Vendor ID
	 */
	public String getvendorIDFOrMountNewTire(String vendorName) {
		Cursor mCursor = null;
		String result = "";
		try {
			/*
			EJOB - 30 : users are unable to enter anything using a Russian keyboard
			when we try to enter the serial number for a tire (W/O serial) in WORK ON TIRE menu - fixed
			provided the generic fix for any special character in the vendor name.
			This will fix Bug173, vendor name with both single and double quotes will work fine.
			 */
			// removed Active based on bug number 484
			String sql = "SELECT ID from vendor WHERE VendorName = ?";
			mCursor = mDb.rawQuery(sql, new String[] { vendorName });
			if (mCursor != null && mCursor.moveToFirst()) {

				result = mCursor.getString(0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
		LogUtil.i("TestAdapter :: SAP_VENDORCODE_ID",
				Constants.SAP_VENDORCODE_ID);
		return result;
	}

	/**
	 * @param sJobItemToDelete
	 */
	public boolean isJobItemPresentforThisJob(String sJobItemToDelete) {
		String query = "SELECT sm.ID from JobItem As sm INNER JOIN Job AS s ON s.ExternalID == sm.JobID where s.jobRefNumber == '"
				+ sJobItemToDelete + "'";
		Cursor jobItemIdCur = null;
		try {
			jobItemIdCur = mDb.rawQuery(query, null);
			if (CursorUtils.isValidCursor(jobItemIdCur)) {
				jobItemIdCur.moveToFirst();
				return jobItemIdCur.getCount() > 0 ? true : false;
			}
			CursorUtils.closeCursor(jobItemIdCur);
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			CursorUtils.closeCursor(jobItemIdCur);
		}
		return false;
	}

	/**
	 * Get FullTyreDetailDesign based on brandId
	 */
	public Cursor getTyreDetailedDesignByBrandId(String tyreSize,
			String tyreASP, String tyreRIM, String tyreDesign, String brandId) {
		try {
			String sql = "Select Distinct(FullTireDetails) from TireModel where Deseign = '"
					+ tyreDesign
					+ "'"
					+ " AND FSize = '"
					+ tyreSize
					+ "'"
					+ " AND AspectRatio = '"
					+ tyreASP
					+ "'"
					+ " AND Diameter = '"
					+ tyreRIM
					+ "'"
					+ " AND IDTireBrand = '"
					+ brandId
					+ "' order by FullTireDetails";
			LogUtil.d(
					"TestAdapter :: getTyreDetailedDesignByBrandId :: Qry :: ",
					sql);
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreDetailedDesignByBrandId >>"
							+ mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * Get FullTyreDetailDesign based on brandId, purchase plant and
	 * stockLocation
	 */
	public Cursor getTyreDetailedDesignByStockLocAndPurchasePlant(
			String tyreSize, String tyreASP, String tyreRIM, String tyreDesign,
			String brandId, String purchasePlant, String stockLocation) {
		try {
			String sql = "Select Distinct(tm.FullTireDetails) from TireModel tm, ProductPurchOrg ppo where ppo.IDTireModel = tm.ID AND tm.Deseign = '"
					+ tyreDesign
					+ "'"
					+ " AND tm.FSize = '"
					+ tyreSize
					+ "'"
					+ " AND tm.AspectRatio = '"
					+ tyreASP
					+ "'"
					+ " AND tm.Diameter = '"
					+ tyreRIM
					+ "'"
					+ " AND tm.IDTireBrand = '"
					+ brandId
					+ "' AND ppo.PurchasingPlant = '"
					+ purchasePlant
					+ "' AND ppo.StockLocationCode = '"
					+ stockLocation
					+ "' order by FullTireDetails";
			LogUtil.d(
					"TestAdapter :: getTyreDetailedDesignByStockLocAndPurchasePlant :: Qry :: ",
					sql);
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreDetailedDesignByStockLocAndPurchasePlant >>"
					+ mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * Get FullTyreDetailDesign based on brandId and purchaseOrganization
	 */
	public Cursor getTyreDetailedDesignByPurchaseOrg(String tyreSize,
			String tyreASP, String tyreRIM, String tyreDesign, String brandId,
			String purchaeOrg) {
		try {
			String sql = "Select Distinct(tm.FullTireDetails) from TireModel tm, ProductPurchOrg ppo where ppo.IDTireModel = tm.ID AND tm.Deseign = '"
					+ tyreDesign
					+ "'"
					+ " AND tm.FSize = '"
					+ tyreSize
					+ "'"
					+ " AND tm.AspectRatio = '"
					+ tyreASP
					+ "'"
					+ " AND tm.Diameter = '"
					+ tyreRIM
					+ "'"
					+ " AND tm.IDTireBrand = '"
					+ brandId
					+ "' AND ppo.PurchasingOrg = '"
					+ purchaeOrg
					+ "' order by FullTireDetails";
			LogUtil.d(
					"TestAdapter :: getTyreDetailedDesignByPurchaseOrg :: Qry :: ",
					sql);
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				mCur.moveToNext();
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"getTyreDetailedDesignByPurchaseOrg >>"
							+ mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * * amitkumar.h: update Job Status after sync
	 * 
	 * @param account
	 * @throws Exception
	 */
	public void updateJObStatusAfterSync() {
		try {
			{
				String sql = "UPDATE Job SET Status = 0 WHERE Status = 2";
				mDb.execSQL(sql);
			}
			{
				String sql = "UPDATE Job SET Status = 5 WHERE Status = 6";
				mDb.execSQL(sql);
			}
		} catch (SQLException mSQLException) {
			Log.e(TAG,
					"DB Adapter" + "cannot update sync time"
							+ mSQLException.toString());
			LogUtil.i(TAG,
					"DB Adapter" + "cannot update sync time"
							+ mSQLException.toString());
		}
		catch (Exception mSQLException) {
			Log.e(TAG,
					"DB Adapter" + "cannot update sync time"
							+ mSQLException.toString());
			LogUtil.i(TAG,
					"DB Adapter" + "cannot update sync time"
							+ mSQLException.toString());
		}
	}

	/**
	 * Get Vendor ID
	 */
	public String getDamageCodeForDamageGroup(String damageGroup,
			String RemovalReasonCode) {
		String sql = null;
		String result = "";
		Cursor mCur = null;
		try {
			sql = "Select ExternalID from Damage where DamageGroup = '"
					+ damageGroup + "' AND RemovalReasonCode = '"
					+ RemovalReasonCode + "'";
			mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {

				result = mCur.getString(0);
			}
		} catch (SQLException e) {
			result = "";
		} finally {
			if (null != mCur)
				mCur.close();
		}
		return result;

	}

	/**
	 * Get DamageCode from DamageGroup And RemovalReasonCode
	 */
	public String getDamageCodeFromDamageGroupAndRemovalReasonCode(String damageGroup,
											  String RemovalReasonCode) {
		String sql = null;
		String result = "";
		Cursor mCur = null;
		try {
			sql = "Select DamageCode from Damage where DamageGroup = '"
					+ damageGroup + "' AND RemovalReasonCode = '"
					+ RemovalReasonCode + "'";
			mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {

				result = mCur.getString(0);
			}
		} catch (SQLException e) {
			result = "";
		} finally {
			if (null != mCur)
				mCur.close();
		}
		return result;
	}

	/**
	 * Get Vendor Name by SAPVendorID
	 */
	public String getVendorNameFromVendor(String sapVendorID) {
		String sql = null;
		String result = "";
		Cursor mCur = null;
		try {
			sql = "Select VendorName from Vendor where ID = " + sapVendorID
					+ "";
			mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {

				result = mCur.getString(0);
			}
		} catch (SQLException e) {
			result = "";
		} finally {
			if (null != mCur)
				mCur.close();
		}
		return result;

	}
	
	/**
	 * CR :: 505
	 * Method fetching full tire details from DB using serial number and Tyre ID
	 * Method name changed FROM getTyreBySerialNumberTO getTyreBySerialNumberAndTyreID in 1.3.4.5
	 * @param serialNumber
	 * @return
	 */
	public Cursor getTyreBySerialNumberAndTyreID(String serialNumber, String tyreID) {
		try {
			//Query altered : added support to get tyre info via tyreID
			String qry = "Select * from Tyre where SerialNumber= '"
					+ serialNumber + "' AND ID = '"+tyreID+"'";
			Cursor mCur = mDb.rawQuery(qry, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getTyreOriginalNSK >>" + mSQLException.toString());
			throw mSQLException;
		}
	}
	
	/**
	 * Content value declaration for tire insertion
	 */
	private ContentValues mSpecialContentValues;
	/**
	 * Method initializing the content value for tire to be inserted in tire table
	 */
	public void initializeSpecialContentValues()
	{
		mSpecialContentValues=new ContentValues();
	}
	/**
	 * Method nullifying the tire object after insertion
	 */
	public void freeSpecialContentValues()
	{
		mSpecialContentValues=null;
	}
	/**
	 * Method loading data one by one in the new tire row
	 * @param Current_Value
	 * @param New_Value
	 */
	public void addValueToContentValues(String Current_Value, String New_Value)
	{
		mSpecialContentValues.put(Current_Value, New_Value);
	}
	/**
	 * Method returning the loaded tire object to be inserted
	 * @return
	 */
	public ContentValues getSpecialContentValues()
	{
		return mSpecialContentValues;
	}
	
	/**
	 * Method inserting the PWT again in the tire table just after delete and sync
	 * @param mTyreValue
	 * @return
	 */
	
	public Boolean insertPWTAfterDeleteJob(ContentValues mTyreValue){
		try {
			mDb.insert("Tyre", null, mTyreValue);
		Log.d("SaveJob", "Row Inserted in JobItem Table");
		return true;
	} catch (SQLException mSQLException) {
		Log.e(TAG, "getTestData >>" + mSQLException.toString());
		throw mSQLException;
	}
	}

	/**
	 * Method to fetch Distinct Visual Remark Types
	 * @return mCur
	 */
	public Cursor getVisualRemarkTypes()
	{
		try {
			String qry = "Select DISTINCT Type from VisualComment where Active = 1";
			Cursor mCur = mDb.rawQuery(qry, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getVisualRemarkTypes >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * Method to filter Visual Remark Description by Type
	 * @param type filterType
	 * @return mCur
	 */
	public Cursor getVisualRemarkDescriptionsByType(String type)
	{
		try {
			String qry = "Select DISTINCT Description from VisualComment where Active = 1 AND Type = '" + type + "'";
			Cursor mCur = mDb.rawQuery(qry, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getVisualRemarkDescriptionsByType >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * Method to filter Visual Remark Action by Type and Description
	 * @param type filterType
	 * @param description filterDescription
	 * @return mCur
	 */
	public Cursor getVisualRemarkDosByTypeAndDescription(String type, String description)
	{
		try {
			String qry = "Select Action from VisualComment where Active = 1 AND Type = '" + type + "' AND Description = '" + description + "'";
			Cursor mCur = mDb.rawQuery(qry, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			return mCur;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getVisualRemarkDosByTypeAndDescription >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * Method to get Visual Remark Id by type, description and action
	 * @param type filterType
	 * @param description filterDescription
	 * @param action filterAction
	 * @return VisualRemarkId
	 */
	public Integer getVisualRemarkId(String type, String description,String action)
	{
		try {
			String qry = "Select idComment from VisualComment where Active = 1 AND Type = '" + type + "' AND Description = '" + description + "' AND Action = '" + action + "'";
			Cursor mCur = mDb.rawQuery(qry, null);
			if (mCur != null) {
				mCur.moveToNext();
				System.out.println("Data inside if" + mCur);
			}
			if (mCur != null && mCur.moveToFirst()) {

				return mCur.getInt(0);
			}
			else
				return 0;

		} catch (SQLException mSQLException) {
			Log.e(TAG, "getVisualRemarkDosByTypeAndDescription >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/**
	 * Method deleting the partially inspected row from JobItem table
	 * @param jobNumber
	 */
	public void deletePartiallyInspectedJobItem(String jobNumber) {
		String sql = "delete from JobItem where JobID in(Select ExternalID from Job where jobRefNumber = '"+jobNumber+"') AND ActionType = '26'";
		try {
			LogUtil.DBLog(TAG,"deletePartiallyInspectedJobItem","Before Delete");
			mDb.execSQL(sql);
			LogUtil.DBLog(TAG, "deletePartiallyInspectedJobItem", "Data Deleted");
		} catch (SQLException e) {
			LogUtil.DBLog(TAG,"deletePartiallyInspectedJobItem","Exception - "+e.getMessage());
			LogUtil.e(TAG, "could not delete ");
		}

	}

	/**
	 * Method setting is perfect status to partially inspected row from JobItem table
	 * @param jobNumber
	 */
	public void makeJobItemImperfectBasedOnJobReferenceNumber(String jobNumber) {
		String sql = "Update JobItem SET IsPerfect='0' where JobID in(Select ExternalID from Job where jobRefNumber = '"+jobNumber+"') AND ActionType = '26'";
		try {
			LogUtil.DBLog(TAG,"makeJobItemImperfectBasedOnJobReferenceNumber","Before Update");
			mDb.execSQL(sql);
			LogUtil.DBLog(TAG, "makeJobItemImperfectBasedOnJobReferenceNumber", "After Update");
		} catch (SQLException e) {
			LogUtil.DBLog(TAG,"makeJobItemImperfectBasedOnJobReferenceNumber","Exception - "+e.getMessage());
			LogUtil.e(TAG, "could not Update ");
		}

	}


	/**
	 * Method setting is perfect status to partially inspected row from job correction table
	 * @param jobNumber
	 */
	public void makeJobCorrectionImperfectBasedOnJobReferenceNumber(String jobNumber) {


		String sqlUpdateToZero = "Update JobCorrection SET IsPerfect='0' where JobID in(Select ExternalID from Job where jobRefNumber = '"+jobNumber+"') AND TyreID in (select TyreID from JobItem where JobID in(Select ExternalID from Job where jobRefNumber = '"+jobNumber+"') AND IsPerfect='0' )";
		try {
			LogUtil.DBLog(TAG,"makeJobCorrectionImperfectBasedOnJobReferenceNumber","Before Update to 0");
			mDb.execSQL(sqlUpdateToZero);
			LogUtil.DBLog(TAG, "makeJobCorrectionImperfectBasedOnJobReferenceNumber", "After Update to 0");
		} catch (SQLException e) {
			LogUtil.DBLog(TAG,"makeJobCorrectionImperfectBasedOnJobReferenceNumber Update to 0","Exception - "+e.getMessage());
			LogUtil.e(TAG, "could not Update ");
		}

		String sqlUpdateToOne = "Update JobCorrection SET IsPerfect='1' where JobID in(Select ExternalID from Job where jobRefNumber = '"+jobNumber+"') AND TyreID in (select TyreID from JobItem where JobID in(Select ExternalID from Job where jobRefNumber = '"+jobNumber+"') AND IsPerfect='1' )";
		try {
			LogUtil.DBLog(TAG,"makeJobCorrectionImperfectBasedOnJobReferenceNumber","Before Update to 1");
			mDb.execSQL(sqlUpdateToOne);
			LogUtil.DBLog(TAG, "makeJobCorrectionImperfectBasedOnJobReferenceNumber", "After Update to 1");
		} catch (SQLException e) {
			LogUtil.DBLog(TAG,"makeJobCorrectionImperfectBasedOnJobReferenceNumber Update to 1","Exception - "+e.getMessage());
			LogUtil.e(TAG, "could not Update ");
		}

	}

	/**
	 * Method fetching SAPCODETI as per the tire externalID from DB
	 * @param externalId
	 * @return
	 */
	public String getSAPCodeTIIdFromExternalID(String externalId) {
		try {
			String sql = "SELECT SAPCodeTi FROM Tyre where ExternalID = '" + externalId
					+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (CursorUtils.isValidCursor(mCur) && mCur.moveToFirst()) {
				String sapCode = mCur.getString(mCur
						.getColumnIndex("SAPCodeTi"));
				CursorUtils.closeCursor(mCur);
				return sapCode;
			} else {
				CursorUtils.closeCursor(mCur);
				return "";
			}
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getSAPCodeTIIdFromExternalID >>" + mSQLException.toString());
			LogUtil.DBLog(TAG, "getSAPCodeTIIdFromExternalID", "Exception - " + mSQLException.getMessage());
			throw mSQLException;
		}
	}
	/**
	 * Method fetching SAPCODEWP as per the tire externalID from DB 
	 * @param externalId
	 * @return
	 */
	public String getSAPCodeWPExternalID(String externalId) {
		try {
			String sql = "SELECT SAPCodeWp FROM Tyre where ExternalID = '" + externalId
					+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (CursorUtils.isValidCursor(mCur) && mCur.moveToFirst()) {
				String sapCodewp = mCur.getString(mCur
						.getColumnIndex("SAPCodeWp"));
				CursorUtils.closeCursor(mCur);
				return sapCodewp;
			} else {
				CursorUtils.closeCursor(mCur);
				return "";
			}
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getSAPCodeWPExternalID >>" + mSQLException.toString());
			LogUtil.DBLog(TAG, "getSAPCodeWPExternalID", "Exception - " + mSQLException.getMessage());
			throw mSQLException;
		}
	}


	/**
	 * To upgrade DB before Doing job operation or sync operation
	 */
	public void doUpgrade(Context mContext)
	{

		LogUtil.i(TAG," Inside doUpgrade");
		int old_version_number;
		String old_app_version_number;
		SharedPreferences preferences = mContext.getSharedPreferences(Constants.GOODYEAR_CONF, 0);
		old_version_number=preferences.getInt(Constants.OLD_DB_VERSION_NUMBER, 5);
		old_app_version_number = preferences.getString(Constants.OLD_APP_VERSION_NUMBER, Constants.Initial_APP_VERSION_NUMBER);

		try {
			if(old_version_number < DataBaseHelper.DATABASE_VERSION) {
				Constants.Force_Download_Sync_STATUS=true;
				LogUtil.DBLog(TAG, "DB_UPGRADE_OLD_VER", String.valueOf(old_version_number));
				LogUtil.DBLog(TAG, "APP_OLD_VER", String.valueOf(old_app_version_number));
				LogUtil.DBLog(TAG, "DATABASE_VERSION", String.valueOf(DataBaseHelper.DATABASE_VERSION));
				LogUtil.DBLog(TAG, "doUpgrade", "DB UPGRADE");
				mDbHelper.externalDBUpgradeCall(mDb);

				SharedPreferences prefer = mContext.getSharedPreferences(Constants.GOODYEAR_CONF, 0);
				SharedPreferences.Editor editor = prefer.edit();

				editor.putInt(Constants.OLD_DB_VERSION_NUMBER, DataBaseHelper.DATABASE_VERSION);
				editor.commit();

				if(Constants.IsDBExistBeforeLogin) {
					Constants.APP_UPGRADE_STATUS = true;
				}

				if(CommonUtils.checkAppUpgradeType(mContext,old_app_version_number)== ApplicationVersionDifference.MINOR_UPGRADE)
				{
					Constants.APP_UPGRADE_STATUS = false;
				}

				//To Stop app force sync
				Constants.APP_UPGRADE_STATUS = false;

				//Update the Constant
				CommonUtils.updateOldAppVersionNumber(mContext);

			}
		}
		catch(Exception e)
		{
			LogUtil.DBLog(TAG, "doUpgrade", "DB UPGRADE : Exception : "+e.getMessage());
		}
	}

	/**
	 * Method to make Isperfect of unwanted Swap operation to zero and Isperfect of Dismounted Tyre Inspection Operation to Zero
	 * @param jobNumber
	 */
	public void makeSwapJobItemAndUnmountedTyreInspectionJobItemImperfectBasedOnJobNumber(String jobNumber) {

		String qry = "Select DISTINCT TyreID from JobItem where JobID=(Select ExternalID from Job where jobRefNumber = '"+jobNumber+"') AND (TyreID IS NOT NULL AND TyreID!='')";
		ArrayList<String> TyreIds = new ArrayList<>();

		//Logic to get Distinct tyre for a job
		try {
			Cursor mCur = mDb.rawQuery(qry, null);
			if (CursorUtils.isValidCursor(mCur)) {
				mCur.moveToFirst();

				//Loading ArrayList with Cursor Data
				do {
					TyreIds.add(mCur.getString(mCur.getColumnIndex("TyreID")));
				} while (mCur.moveToNext());
			}
		}
		catch (Exception e)
		{
			LogUtil.DBLog(TAG, "makeSwapJobItemImperfectBasedOnJobNumber", "Distinct Tyre : Exception - " + e.getMessage());
		}

		//Logic to make IsPerfect of unwanted Swap JobItem to zero
		for(int i=0;i<TyreIds.size();i++) {

			String sql_swap_type_4 = "UPDATE JobItem SET IsPerfect='0' where (TyreID='" + TyreIds.get(i) + "' AND JobID in(Select ExternalID from Job where jobRefNumber = '" + jobNumber + "')) AND (ActionType='3' AND SwapType='4') AND ExternalID NOT IN (Select ExternalID from JobItem where (TyreID='" + TyreIds.get(i) + "' AND JobID in(Select ExternalID from Job where jobRefNumber = '" + jobNumber + "')) AND (ActionType='3' AND SwapType='4') ORDER BY Sequence DESC LIMIT 1)";

			String sql_swap_type_1 = "UPDATE JobItem SET IsPerfect='0' where (TyreID='" + TyreIds.get(i) + "' AND JobID in(Select ExternalID from Job where jobRefNumber = '" + jobNumber + "')) AND (ActionType='3' AND SwapType='1') AND ExternalID NOT IN (Select ExternalID from JobItem where (TyreID='" + TyreIds.get(i) + "' AND JobID in(Select ExternalID from Job where jobRefNumber = '" + jobNumber + "')) AND (ActionType='3' AND SwapType='1') ORDER BY Sequence DESC LIMIT 1)";

			//Swap Type-4
			try {
				LogUtil.DBLog(TAG, "makeSwapJobItemImperfectBasedOnJobNumber", "Before Swap Type-4 Update : TyreID -"+TyreIds.get(i));
				mDb.execSQL(sql_swap_type_4);
				LogUtil.DBLog(TAG, "makeSwapJobItemImperfectBasedOnJobNumber", "After Swap Type-4 Update");
			} catch (SQLException e) {
				LogUtil.DBLog(TAG, "makeSwapJobItemImperfectBasedOnJobNumber", "Swap Type-4 : Exception - " + e.getMessage());
				LogUtil.e(TAG, "could not Update ");
			}

			//Swap Type-1
			try {
				LogUtil.DBLog(TAG, "makeSwapJobItemImperfectBasedOnJobNumber", "Before Swap Type-1 Update : TyreID -"+TyreIds.get(i));
				mDb.execSQL(sql_swap_type_1);
				LogUtil.DBLog(TAG, "makeSwapJobItemImperfectBasedOnJobNumber", "After Swap Type-1 Update");
			} catch (SQLException e) {
				LogUtil.DBLog(TAG, "makeSwapJobItemImperfectBasedOnJobNumber", "Swap Type-1 : Exception - " + e.getMessage());
				LogUtil.e(TAG, "could not Update ");
			}

			//Change Inspection Isperfect status for an Dismounted Tyre
			makeInspectionForDismountTyreJobItemImperfectBasedOnTyreId(TyreIds.get(i),jobNumber);

		}

	}

    /** Bug 683 :
     * Method to change IsPerfect status of an Dismounted tyre so that operations performed on
     * a tyre before dismount are not sent to server.
     *
     * @param jobNumber jobreference number
     */
    public void makeDismountedTyreJobItemImperfect(String jobNumber) {

        String qry = "Select DISTINCT TyreID from JobItem " +
                " where ActionType = '1' AND JobID =(Select ExternalID from Job where jobRefNumber = '" + jobNumber + "') " +
                " AND (TyreID IS NOT NULL AND TyreID!='')";
        ArrayList<String> TyreIds = new ArrayList<>();

        //Logic to get Distinct tyre for a job
        try {
            Cursor mCur = mDb.rawQuery(qry, null);
            if (CursorUtils.isValidCursor(mCur)) {
                mCur.moveToFirst();

                //Loading ArrayList with Cursor Data
                do {
                    TyreIds.add(mCur.getString(mCur.getColumnIndex("TyreID")));
                } while (mCur.moveToNext());
            }
        } catch (Exception e) {
            LogUtil.DBLog(TAG, "makeDismountedTyreJobItemImperfect", "Distinct Tyre : Exception - " + e.getMessage());
        }

        //Logic to make IsPerfect of dismounted tyre's JobItem to zero
        for (int i = 0; i < TyreIds.size(); i++) {

            String sql_query = "UPDATE JobItem SET IsPerfect='0' where (TyreID='" + TyreIds.get(i) + "' AND JobID in(Select ExternalID from Job where jobRefNumber = '" + jobNumber + "') " +
                    " AND ActionType != '1' )";

            try {
                LogUtil.DBLog(TAG, "makeDismountedTyreJobItemImperfect", "Before dismount Update : TyreID -" + TyreIds.get(i));
                mDb.execSQL(sql_query);
                LogUtil.DBLog(TAG, "makeDismountedTyreJobItemImperfect", "After dismount Update");
            } catch (SQLException e) {
                LogUtil.DBLog(TAG, "makeDismountedTyreJobItemImperfect", " Dismount : Exception - " + e.getMessage());
                LogUtil.e(TAG, "makeDismountedTyreJobItemImperfect : could not Update ");
            }

        }

    }

	/**
	 * Method to get Mounted Status of Tyre using TyreID
	 * @param TyreId tyreid
	 * @return
	 */
	public String getMountedStatusOfTyreUsingTyreId(String TyreId) {
		try {
			String sql = "SELECT * FROM Tyre where ExternalID = '" + TyreId+ "'";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (CursorUtils.isValidCursor(mCur) && mCur.moveToFirst()) {
				int status = mCur.getInt(mCur
						.getColumnIndex("IsUnmounted"));
				CursorUtils.closeCursor(mCur);
				return String.valueOf(status);
			} else {
				CursorUtils.closeCursor(mCur);
				return "0";
			}
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getMountedStatusOfTyreUsingTyreId >>" + mSQLException.toString());
			LogUtil.DBLog(TAG, "getMountedStatusOfTyreUsingTyreId", "Exception - " + mSQLException.getMessage());
			throw mSQLException;
		}
	}

	/**
	 * Method to Change Inspection IsPerfect status of an Dismounted tyre
	 * @param TyreId tyreid
	 * @param jobNumber jobreference number
	 */
	public void makeInspectionForDismountTyreJobItemImperfectBasedOnTyreId(String TyreId,String jobNumber)
	{

		try {
			if(getMountedStatusOfTyreUsingTyreId(TyreId).equalsIgnoreCase("1"))
			{
				String sql_DeletedTyreInspectionJobItemUpdate = "UPDATE JobItem SET IsPerfect='0' where TyreID='" + TyreId + "' AND JobID in (Select ExternalID from Job where jobRefNumber = '" + jobNumber + "') AND ActionType='26'";

				//Dismounted Tyre Isperfect Updation
				try {
					LogUtil.DBLog(TAG, "makeInspectionDeletedTyreJobItemImperfectBasedOnTyreId", "Before Update : TyreID -"+TyreId);
					mDb.execSQL(sql_DeletedTyreInspectionJobItemUpdate);
					LogUtil.DBLog(TAG, "makeInspectionDeletedTyreJobItemImperfectBasedOnTyreId", "After Update");
				} catch (SQLException e) {
					LogUtil.DBLog(TAG, "makeInspectionDeletedTyreJobItemImperfectBasedOnTyreId", "Exception - " + e.getMessage());
					LogUtil.e(TAG, "could not Update ");
				}
			}
		}catch (Exception e)
		{
			LogUtil.DBLog(TAG, "makeInspectionDeletedTyreJobItemImperfectBasedOnTyreId", "Exception -- " + e.getMessage());
		}

	}

	/**
	 * Method to add Deleted Job PWT Tyre info to PWTTyreList Constants
	 * @param jobNumber job Reference Number
	 */
	public void addPWTTyreToDeletedJobPWTTyreList(String jobNumber) {

		String qry = "Select DISTINCT TyreID from JobItem where JobID=(Select ExternalID from Job where jobRefNumber = '"+jobNumber+"') AND ActionType='25' AND (TyreID IS NOT NULL AND TyreID!='')";

		if(Constants.PWTTyreList==null) {
			Constants.PWTTyreList=new ArrayList<>();
		}
		//Logic to get Distinct tyre for a job
		try {
			Cursor mCur = mDb.rawQuery(qry, null);
			if (CursorUtils.isValidCursor(mCur)) {
				mCur.moveToFirst();

				//Loading ArrayList with Cursor Data
				do {
					Constants.PWTTyreList.add(mCur.getString(mCur.getColumnIndex("TyreID")));
				} while (mCur.moveToNext());
			}
		}
		catch (Exception e)
		{
			LogUtil.DBLog(TAG, "addPWTTyreToDeletedJobPWTTyreList", " Exception with job number - "+jobNumber+"\t" + e.getMessage());
		}
	}

	/**
	 * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
	 * If material and/or SN is changed when a tyre is removed - this tyre should not be made available immediately
	 * Method to populate IsTyreCorrected value for dismounted tyre - to maintain material and/or SN change info in tyre table
	 */
	public void populateIsTyreCorrectedValueForDismountedTyre(String jobNumber) {

		String qry = "Select DISTINCT TyreID from JobItem " +
				" where ActionType = '1' AND JobID =(Select ExternalID from Job where jobRefNumber = '" + jobNumber + "') " +
				" AND (TyreID IS NOT NULL AND TyreID!='' AND TyreID in (SELECT DISTINCT TyreID from JobCorrection where JobID =(Select ExternalID from Job where jobRefNumber = '" + jobNumber + "')))";
		ArrayList<String> TyreIds = new ArrayList<>();

		//Logic to get Distinct tyre for a job
		try {
			Cursor mCur = mDb.rawQuery(qry, null);
			if (CursorUtils.isValidCursor(mCur)) {
				mCur.moveToFirst();

				//Loading ArrayList with Cursor Data
				do {
					TyreIds.add(mCur.getString(mCur.getColumnIndex("TyreID")));
				} while (mCur.moveToNext());
			}
		} catch (Exception e) {
			LogUtil.DBLog(TAG, "populateIsTyreCorrectedValueForDismountedTyre", "Distinct Tyre : Exception - " + e.getMessage());
		}

		//Logic update IsTyreCorrected value
		for (int i = 0; i < TyreIds.size(); i++) {

			String sql_query = "UPDATE Tyre SET IsTyreCorrected='1' where ExternalID='" + TyreIds.get(i) + "' ";

			try {
				LogUtil.DBLog(TAG, "populateIsTyreCorrectedValueForDismountedTyre", "Before dismount Update : TyreID -" + TyreIds.get(i));
				mDb.execSQL(sql_query);
				LogUtil.DBLog(TAG, "populateIsTyreCorrectedValueForDismountedTyre", "After dismount Update");
			} catch (SQLException e) {
				LogUtil.DBLog(TAG, "populateIsTyreCorrectedValueForDismountedTyre", " Dismount : Exception - " + e.getMessage());
				LogUtil.e(TAG, "populateIsTyreCorrectedValueForDismountedTyre : could not Update ");
			}

		}

	}

	/**
	 * Method to update jobID for dismounted reusable tyre
	 */
	public void updateJobIDForDismountedReusableTyre(String jobNumber) {

		String qry = "Select DISTINCT TyreID from JobItem " +
				" where ActionType = '1' AND JobID =(Select ExternalID from Job where jobRefNumber = '" + jobNumber + "') " +
				" AND (TyreID IS NOT NULL AND TyreID!='')";

		ArrayList<String> TyreIds = new ArrayList<>();

		//Logic to get Distinct tyre for a job
		try {
			Cursor mCur = mDb.rawQuery(qry, null);
			if (CursorUtils.isValidCursor(mCur)) {
				mCur.moveToFirst();

				//Loading ArrayList with Cursor Data
				do {
					TyreIds.add(mCur.getString(mCur.getColumnIndex("TyreID")));
				} while (mCur.moveToNext());
			}
		} catch (Exception e) {
			LogUtil.DBLog(TAG, "updateJobIDForDismountedReusableTyre", "Distinct Tyre : Exception - " + e.getMessage());
		}

		//Logic update IsTyreCorrected value
		for (int i = 0; i < TyreIds.size(); i++) {

			String sql_query = "UPDATE Tyre SET FromJobID=(Select ID from Job where jobRefNumber = '" + jobNumber + "') where ExternalID='" + TyreIds.get(i) + "' AND Shared='1'";

			try {
				LogUtil.DBLog(TAG, "updateJobIDForDismountedReusableTyre", "Before dismount Update : TyreID -" + TyreIds.get(i));
				mDb.execSQL(sql_query);
				LogUtil.DBLog(TAG, "updateJobIDForDismountedReusableTyre", "After dismount Update");
			} catch (SQLException e) {
				LogUtil.DBLog(TAG, "updateJobIDForDismountedReusableTyre", " Dismount : Exception - " + e.getMessage());
				LogUtil.e(TAG, "updateJobIDForDismountedReusableTyre : could not Update ");
			}

		}

	}

   
	/**
	 * Method to change IsPerfect status of unwanted Job correction.
	 * @param jobNumber
     */
	public void makeUnwantedJobCorrectionImperfectBasedOnJobReferenceNumber(String jobNumber) {

		String sqlQuery = "Update JobCorrection SET IsPerfect='0' where JobID in(Select ExternalID from Job where jobRefNumber = '"+jobNumber+"') AND TyreID NOT in (select TyreID from JobItem where JobID in(Select ExternalID from Job where jobRefNumber = '"+jobNumber+"') AND IsPerfect='1' )";
		try {
			LogUtil.DBLog(TAG,"makeUnwantedJobCorrectionImperfectBasedOnJobReferenceNumber","Before Update to 0");
			mDb.execSQL(sqlQuery);
			LogUtil.DBLog(TAG, "makeUnwantedJobCorrectionImperfectBasedOnJobReferenceNumber", "After Update to 0");
		} catch (SQLException e) {
			LogUtil.DBLog(TAG,"makeUnwantedJobCorrectionImperfectBasedOnJobReferenceNumber Update to 0","Exception - "+e.getMessage());
			LogUtil.e(TAG, "could not Update ");
		}
	}
	public long getDatabaseSize()
	{
		try {
			if (mDbHelper != null) {
				return mDbHelper.getDatabaseSize();
			}
		}
		catch(Exception e)
		{
			LogUtil.DBLog(TAG, "getDatabaseSize", "Exception - " + e.getMessage());
		}
		return 0;
	}

	/**
	 * * update account after login
	 *
	 * @param account
	 * @throws Exception
	 */
	/* #ForHalosys */
	public void insertAccountWithPin(String mUsername, String mpin, int noficationType) throws Exception {
		mpin = Security.encryptMessage(mpin);
		String mPassword = Security.encryptMessage("");
		String sql = "Insert OR REPLACE into Account values ('1','" + mUsername
				+ "','"+mPassword+"','" + '0' + "','0','" + noficationType + "','"+ mpin +"','0')";
		try {
			mDb.execSQL(sql);
		} catch (SQLException mSQLException) {
			Log.e("TestAdapter--",
					"insert account failed " + mSQLException.toString());
		}
	}

	/**
	 * * update account after login
	 *
	 * @param account
	 * @throws Exception
	 */
	/* #ForHalosys */
	public void updateAccountWithPin(String pin) throws Exception {
		LogUtil.d(TAG," :: inside updateAccountWithPin() ");
		String encyPin = Security.encryptMessage(pin);
		try {
			String sql = "Update Account SET Pin = '"+encyPin+"', isPinSet = '1' where ID = '1';";
			mDb.execSQL(sql);
		} catch (SQLException mSQLException) {
			Log.e(TAG,
					" update account failed " + mSQLException.toString());
		}
	}

	/* #ForHalosys */
	public void insertIntoTranslation(String mID, String mText) throws Exception {
		String sql = "Insert OR REPLACE into Translation values ('"+mID+"',?)";
		try {
			mDb.execSQL(sql,new String[] { mText });
		} catch (SQLException mSQLException) {
			LogUtil.i(TAG, "insertIntoTranslation : Exception - " + mSQLException.getMessage());
		}
	}

	/* #ForHalosys */
	public String getPinForUser(String username) {
		String result = "";
		Log.e(TAG, ":: inside getPinForUser() ");
		try {
			String sql = "SELECT Pin FROM Account where Login = '" + username
					+ "';";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null && mCur.moveToFirst()) {
				result = mCur.getString(0);
			}
			return result;
		} catch (SQLException mSQLException) {
			Log.e(TAG, " getPinForUser >>" + mSQLException.toString());
			throw mSQLException;
		}
	}

	/* #ForHalosys */
	public int isOfflinePinSet(String username) {
		int result = 0;
		Log.e(TAG, ":: inside isOfflinePinSet(), username :: "+username);
		try {
			String sql = "SELECT isPinSet FROM Account where Login = '" + username
					+ "';";
			Cursor mCur = mDb.rawQuery(sql, null);
			int index = mCur.getColumnIndex("isPinSet");
			if (index == -1) {
				LogUtil.d(TAG ,":: Db column doesnot exists");
				result = -1;
			} else {
				if (mCur != null && mCur.moveToFirst() && mCur.getString(0) != null) {
					result = Integer.valueOf(mCur.getString(0).toString());
				}
			}
			LogUtil.d(TAG , ":: DB exists, isPinSet :: "+result);
		} catch (SQLException mSQLException) {
			LogUtil.e(TAG, " isOfflinePinSet >> " + mSQLException.toString());
			throw mSQLException;
		} catch (Exception e) {
			LogUtil.e(TAG, " isOfflinePinSet >> " + e);
		}
		return result;
	}
}