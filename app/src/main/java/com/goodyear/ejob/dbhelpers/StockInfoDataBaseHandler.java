package com.goodyear.ejob.dbhelpers;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.goodyear.ejob.halosysobjmodel.FleetServiceProviderStock;
import com.goodyear.ejob.halosysobjmodel.ReUsableStockInfoModel;
import com.goodyear.ejob.halosysobjmodel.StockDetail;
import com.goodyear.ejob.halosysobjmodel.StockInfo;
import com.goodyear.ejob.halosysobjmodel.StockTyre;
import com.goodyear.ejob.halosysobjmodel.TyreInformation;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.Security;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

/**
 * Prepare snapshot before sync
 *
 * @author amitkumar.h
 */
public class StockInfoDataBaseHandler {

    public static final String DBNAME = "stockinfo";
    private static final int DBVERSION = 1;
    private static String DB_PATH;
    private DataBaseHelper mDBHelper;
    private SQLiteDatabase mDB;
    private Context mContext;

    public StockInfoDataBaseHandler(Context ctx) {

        mDBHelper = new DataBaseHelper(ctx);
        //DB_PATH = "/data/data/" + ctx.getPackageName() + "/databases/";

        DB_PATH = ctx.getDatabasePath(DBNAME).getPath();
        mContext = ctx;

    }

    // Copy the database from assets
    public void copyDataBase() throws IOException {
        clearAndroidMetadata();
        String copyTO = Constants.PROJECT_PATH + "/temp";
        File tempDir = new File(copyTO);
        if (!tempDir.exists())
            tempDir.mkdir();
        File db3file = new File(copyTO, "/stockinfo.db");
        if (!db3file.exists())
            db3file.createNewFile();
        //String inFileName = DB_PATH + DBNAME;
        String inFileName = DB_PATH;
        InputStream mInput = new FileInputStream(inFileName);
        OutputStream mOutput = new FileOutputStream(db3file);
        copyStreams(mInput, mOutput);
    }

    // this function will move out from here
    @SuppressLint("NewApi")
    public String getPathForDBFile() {
        SharedPreferences preferences = mContext.getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);
        if (preferences.getString(Constants.DATABASE_LOCATION, "not found")
                .equals(Constants.EXTERNAL_DB_LOCATION)) {
            int SDK_INT = android.os.Build.VERSION.SDK_INT;
            if (SDK_INT >= 19) {
                return mContext.getExternalFilesDirs(null)[1].getAbsolutePath()
                        + "/" + Constants.USER + ".db3";
            } else {
                String externalStorage = System.getenv("SECONDARY_STORAGE");
                String sdCardList[] = externalStorage.split(":");
                return sdCardList[0] + Constants.PROJECT_PATH_FOR_BELOW_KITKAT
                        + "/" + Constants.USER + ".db3";
            }
        }
        // else phone memory
        else {
            return Constants.PROJECT_PATH + "/" + Constants.USER + ".db3";
        }
    }

    private void copyStreams(InputStream mInput, OutputStream mOutput)
            throws IOException {
        byte[] mBuffer = new byte[64];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0) {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    public void deleteDatabaseAfterLoad() {
        mContext.deleteDatabase(DBNAME);
    }

    public void openDB() {
        mDB = mDBHelper.getWritableDatabase();
        mDB.rawQuery("PRAGMA page_size=1024", null);
        mDB.execSQL("VACUUM");
    }

    public void closeDB() {
        mDBHelper.close();
    }

    public void clearAndroidMetadata() {
        try {
            mDB.execSQL("Drop Table " + "android_metadata");
        } catch (Exception e) {
            Log.i("DataBaseHandler", "Drop Table - android_metadata : " + e.getMessage());
        }
        try {
            mDB.execSQL("VACUUM");
        } catch (Exception e) {
            Log.i("DataBaseHandler", "VACUUM : " + e.getMessage());
        }
    }

    public void createTablesInDB() {
        try {
            AssetManager assetManager = mContext.getAssets();
            InputStream in = null;
            in = assetManager.open("tyretable.sql");
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line = null;
            String queryString = null;
            while (null != (line = br.readLine())) {
                queryString = line;
            }
            br.close();
            try {
                mDB.execSQL(queryString);
            } catch (Exception e) {
                LogUtil.i("createTablesInDB", "Query : " + queryString + ", Exception : " + e.getMessage());
            }
        } catch (Exception e) {
        }
    }

    private class DataBaseHelper extends SQLiteOpenHelper {
        public DataBaseHelper(Context context) {
            super(context, DBNAME, null, DBVERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

    }


    public void insertStockInfo(ReUsableStockInfoModel reUsableStockInfoModel, String mSAPVendorCode) throws Exception {

        if (reUsableStockInfoModel != null && reUsableStockInfoModel.getFleetServiceProviderStock() != null && reUsableStockInfoModel.getFleetServiceProviderStock().getStockDetails() != null) {
            List<StockDetail> fleetServiceProviderStock = reUsableStockInfoModel.getFleetServiceProviderStock().getStockDetails();
            for (StockDetail stockDetail : fleetServiceProviderStock) {
                StockInfo stockInfo = stockDetail.getStockInfo();
                if (stockInfo != null) {
                    for (StockTyre stockTyre : stockInfo.getStockTyres()) {
                        TyreInformation tyreInformation = stockTyre.getTyreInformation();
                        /*String sql = "Insert OR REPLACE into Tyre ()values (1,'" + uname
                                + "','" + password + "','" + jobRefNumber + "','" + passExpiry
                                + "','" + noficationType + "')";*/
                        UUID uuid = UUID.randomUUID();
                        insertTyreTable(uuid.toString(), null, tyreInformation.getSerialNumber(), 1, 0, Integer.valueOf(stockInfo.getContractNumber()), Integer.valueOf(mSAPVendorCode),
                                Integer.valueOf(tyreInformation.getProductInfo().getCode()), null, Integer.valueOf(tyreInformation.getTyreId()), 0,
                                Float.valueOf(tyreInformation.getTreadDepthMiddle()).intValue(), 0);
                    }
                }
            }
        }
    }

    public void insertTyreTable(String mExternalID,
                                String mPressure, String mSerialNumber, int mIsUnmounted,
                                int mIsSpare, int mSAPContractNumberID,
                                int mSAPVendorCodeID, int mSAPMaterialCodeID, String mPosition,
                                int mSAPCodeTi, int mSAPCodeWp, int mOriginalNSK, int mSAPCodeVeh) {
        LogUtil.DBLog("StockInfoDataBaseHandler", "Insert Tyre Table", "Start");
        try {
            ContentValues cv = new ContentValues();
            cv.put("ExternalID", mExternalID);
            // cv.put("Pressure", mPressure);
            cv.put("SerialNumber", mSerialNumber);
            cv.put("IsUnmounted", mIsUnmounted);
            cv.put("IsSpare", mIsSpare);
            cv.put("SAPContractNumberID", mSAPContractNumberID);
            cv.put("SAPVendorCodeID", mSAPVendorCodeID);
            cv.put("SAPMaterialCodeID", mSAPMaterialCodeID);
            // cv.put("Position", mPosition);
            cv.put("SAPCodeTi", mSAPCodeTi);
            // cv.put("SAPCodeWp", mSAPCodeWp);
            cv.put("OriginalNSK", mOriginalNSK);
            cv.put("SAPCodeAxID", "");
            // cv.put("SAPCodeVeh", mSAPCodeVeh);
            cv.put("Status", 5);
            cv.put("Active", 1);
            LogUtil.DBLog("StockInfoDataBaseHandler", "Insert Tyre Table", "SerialNumber - " + mSerialNumber);
            LogUtil.DBLog("StockInfoDataBaseHandler", "Insert Tyre Table", "Before Insert Tyre into Table");
            mDB.insert("Tyre", null, cv);
            LogUtil.DBLog("StockInfoDataBaseHandler", "Insert Tyre Table", "After Tyre Inserted");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            LogUtil.DBLog("StockInfoDataBaseHandler", "Insert Tyre Table", "Exception - " + e.getMessage());
            e.printStackTrace();
        }

    }
}
