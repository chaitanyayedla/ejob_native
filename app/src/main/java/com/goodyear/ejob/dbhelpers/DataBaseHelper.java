package com.goodyear.ejob.dbhelpers;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.goodyear.ejob.authenticator.Authentication;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.LogUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * Class used for run external .db3 files and stored data locally
 *
 * @author johnmiya.s
 */
public class DataBaseHelper extends SQLiteOpenHelper {
    private static String TAG = "DataBaseHelper"; // Tag just for the LogCat
    // window
    // destination path (location) of our database on device
    private static String DB_PATH = "";
    private static String DB_NAME = "USER";// Database name
    private SQLiteDatabase mDataBase;
    //CR 441
    private static final String CREATE_VISUAL_COMMENTS_TABLE_QUERY = "CREATE TABLE VisualComments (CommentID integer,"
            + "JobItemID integer,"
            + "Type integer,"
            + "Description text,"
            + "Action integer," + "TimeRef text, FOREIGN KEY(JobItemID) REFERENCES JobItem(ExternalID));";
    private final Context mContext;
    public static final int DATABASE_VERSION = 12;
    private final String ADDITIONAL_SERVICE_FAVOURITE_QUERY = "ALTER TABLE ServiceMaterial ADD COLUMN fav INTEGER DEFAULT '0';";
    File Db;

    public DataBaseHelper(Context context) {
        // TODO: updated db version check if every thing works okay
        super(context, DB_NAME, null, DATABASE_VERSION);

        // DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        if (Constants.USER == null) {
            Constants.USER = " ";
        }
        //DB_NAME = Authentication.getUserName(Constants.USER);

        DB_PATH = context.getDatabasePath(DB_NAME).getPath();
        Db = new File(DB_PATH);
        this.mContext = context;

    }

    // create data and copy data locally
    public void createDataBase() throws IOException {
        // If database not exists copy it from the assets

        boolean mDataBaseExist = checkDataBase();
        if (!mDataBaseExist) {
            this.getReadableDatabase();
            this.close();
            try {
                // Copy the database from assests
                copyDataBase();
                Log.e(TAG, "createDatabase database created");
            } catch (IOException mIOException) {
                mIOException.printStackTrace();
                LogUtil.i(TAG, "createDatabase database created : "+mIOException.getMessage());
                throw new Error("ErrorCopyingDataBase");
            }
        }
    }

    /**
     * get path for DB files
     *
     * @return
     */
    @SuppressLint("NewApi")
    public String getPathForDBFile() {
        SharedPreferences preferences = mContext.getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);
        if (preferences.getString(Constants.DATABASE_LOCATION, "not found")
                .equals(Constants.EXTERNAL_DB_LOCATION)) {
            int SDK_INT = android.os.Build.VERSION.SDK_INT;
            if (SDK_INT >= 19) {
                return mContext.getExternalFilesDirs(null)[1].getAbsolutePath()
                        + "/" + Constants.USER + ".db3";
            } else {
                String externalStorage = System.getenv("SECONDARY_STORAGE");
                String sdCardList[] = externalStorage.split(":");
                return sdCardList[0] + Constants.PROJECT_PATH_FOR_BELOW_KITKAT
                        + "/" + Constants.USER + ".db3";
            }
        }
        // else phone memory
        else {
            return Constants.PROJECT_PATH + "/" + Constants.USER + ".db3";
        }
    }

    private boolean checkDataBase() {
        // File dbFile = new File(DB_PATH + DB_NAME);
        File dbFile = new File(DB_PATH);
        return dbFile.exists();
    }

    /**
     * check if translation table has records
     *
     * @param fileTOAttach path to response file
     * @return if data is received true, else false
     */
    public boolean isDataReceived(String fileTOAttach, String selectedLanguage) {
        int count = 0;
        mDataBase.execSQL("ATTACH DATABASE '" + fileTOAttach + "' AS pdb");
        Cursor cur = mDataBase.rawQuery("select * FROM pdb.Translation", null);
        if (CursorUtils.isValidCursor(cur)) {
            count = cur.getCount();
        }
        CursorUtils.closeCursor(cur);
        mDataBase.execSQL("DETACH DATABASE pdb");
        return (count > 0) ? true : false;
    }

    /**
     * update tables in DB after sync
     *
     * @param fileTOAttach path to temp DB from server
     * @param sqlFile      sql file to be executed
     * @throws IOException
     */
    public void updateTablesInDB(String fileTOAttach, String sqlFile)
            throws IOException {
        BufferedReader br = null;

        Long oldPasswordExp = getPasswordExpirationFromAccountTable();

        try {

            mDataBase.execSQL("ATTACH DATABASE '" + fileTOAttach + "' AS pdb");
            AssetManager assetManager = mContext.getAssets();
            InputStream in = null;
            in = assetManager.open(sqlFile);

            br = new BufferedReader(new InputStreamReader(in));

            String line = null;
            String queryString = null;

            while ((line = br.readLine()) != null) {
                queryString = line;
            }

            String[] query = queryString.split("GO");

            for (int i = 0; i < query.length; i++) {

                try {
                    String queryToExecute = query[i];
                    queryToExecute.trim();
                    mDataBase.execSQL(queryToExecute);
                }
                catch(Exception e)
                {
                    LogUtil.i("updateTablesInDB","Query : "+ query[i].toString()+ ", Exception : " +e.getMessage());
                }
            }

            //We are Updating password expiration time if it is null
            Long newPasswordExp = getPasswordExpirationFromAccountTable();
            try {
                if(newPasswordExp == 0) {
                    String updatePassExpquery = "UPDATE Account SET PassExpirationDate = '" + oldPasswordExp + "'";
                    mDataBase.execSQL(updatePassExpquery);
                }
            }
            catch (Exception e)
            {
                LogUtil.i("updateTablesInDB","Update passwordExp"+ "Exception : " +e.getMessage());
            }

            // CR 447, favourite additional service CR
            // update tables in database
            updateDatabaseTables(mDataBase);
            // Cursor c = null;
            // c = mDataBase.rawQuery("pragma table_info (userCommand)",null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            br.close();
        }
    }

    public void updateTyreTableInDB(String path){
        String insertQurey = "INSERT INTO Tyre(ID, ExternalID, Pressure, SerialNumber, IsUnmounted, IsSpare, SAPContractNumberID," +
                "SAPVendorCodeID, SAPMaterialCodeID, Position, SAPCodeTi, SAPCodeWp, OriginalNSK, SAPCodeAxID, SAPCodeVeh," +
                "Status, Active) SELECT ID, ExternalID, Pressure, SerialNumber, IsUnmounted, IsSpare, SAPContractNumberID," +
                "SAPVendorCodeID, SAPMaterialCodeID, Position, SAPCodeTi, SAPCodeWp, OriginalNSK, SAPCodeAxID, SAPCodeVeh," +
                "Status, Active FROM tdb.Tyre WHERE Active = 1";
        mDataBase.execSQL("ATTACH DATABASE '" + path + "' AS tdb");
        mDataBase.execSQL(insertQurey);
    }

    // Copy the database from assets
    public void copyDataBase() throws IOException {
        String dbfile = getPathForDBFile();

        if (dbfile.endsWith("//null.db3"))
            return;

        File db3file = new File(dbfile);
        if (!db3file.exists())
            db3file.createNewFile();
        InputStream mInput = new FileInputStream(db3file);
        // String outFileName = DB_PATH + DB_NAME;
        String outFileName = DB_PATH;
        OutputStream mOutput = new FileOutputStream(outFileName);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0) {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    // Open the database, so we can query it
    public boolean openDataBase() throws SQLException {
        // String mPath = DB_PATH + DB_NAME;
        String mPath = DB_PATH;
        try {
            mDataBase = SQLiteDatabase.openDatabase(mPath, null,
                    SQLiteDatabase.CREATE_IF_NECESSARY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mDataBase != null;
    }

    @Override
    public void onCreate(SQLiteDatabase arg0) {

    }

    // upgrade database with new version level
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        LogUtil.i(TAG, ":::::::updating database::::::::");
        try {
            updateDatabaseTables(db);
        } catch (Exception e) {
            LogUtil.i(TAG, "table already exists");
        }
//TODO: add try catch
    }

    public void createTablesInDB(String fileTOAttach) throws IOException {
        BufferedReader br = null;
        try {

            AssetManager assetManager = mContext.getAssets();
            InputStream in = null;

            in = assetManager.open(Constants.NEW_USER_DB_QUERYFILE);
            br = new BufferedReader(new InputStreamReader(in));

            String line = null;
            String queryString = null;

            while ((line = br.readLine()) != null) {
                queryString = line;
            }

            String[] query = queryString.split("GO");

            for (int i = 0; i < query.length; i++) {
                String queryToExecute = query[i];
                queryToExecute.trim();
                mDataBase.execSQL(queryToExecute);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }

    // Copy the database from assets
    public void copyDataBase(String path) throws IOException {
        String dbfile = Constants.PROJECT_PATH + "/" + path + ".db3";
        File db3file = new File(dbfile);
        InputStream mInput = new FileInputStream(db3file);
        // String outFileName = DB_PATH + DB_NAME;
        String outFileName = DB_PATH;
        OutputStream mOutput = new FileOutputStream(outFileName);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0) {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    // Copy the database from assets
    public void copyDataBaseToPhone() throws IOException {
        String copyTO = Constants.PROJECT_PATH + "/" + Constants.USER
                + "_temp.db3";

        File db3file = new File(copyTO);
        if (!db3file.exists())
            db3file.createNewFile();

        // String inFileName = DB_PATH + DB_NAME;
        String inFileName = DB_PATH;
        InputStream mInput = new FileInputStream(inFileName);
        OutputStream mOutput = new FileOutputStream(db3file);

        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0) {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    // Copy the database from assets
    public void copyUserDBToPhone(String... userName) throws IOException {
        if (userName.length > 0) {
            if (userName[0] == null || userName[0].equals("")) {
                return;
            } else
                Constants.USER = userName[0];
        }
        String inFileName = DB_PATH;
        if (inFileName.trim().endsWith("databases/")) {
            return;
        }
        String copyTO = getPathForDBFile();
        if (Constants.USER == null)
            return;

        File db3file = new File(copyTO);
        if (db3file.exists()) {
            db3file.delete();
            db3file.createNewFile();
        } else
            db3file.createNewFile();

        InputStream mInput = new FileInputStream(inFileName);
        OutputStream mOutput = new FileOutputStream(db3file);

        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0) {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    public void deleteDataBase() {
        // delete the existing db
        mContext.deleteDatabase(DB_NAME);
    }

    public boolean updateMsg(String New_Value, String TABLE_NAME,
                             String Current_Value, String COLUMN_ID) {
        String selectQuery = "update  " + TABLE_NAME + " set " + Current_Value
                + " = '" + New_Value + "' where " + COLUMN_ID + " = 1";
        ContentValues values = new ContentValues();
        values.put(Current_Value, New_Value);
        SQLiteDatabase db = this.getReadableDatabase();
        db.update(TABLE_NAME, values, selectQuery, null);
        return true;

    }

	/*
     * @Override public synchronized SQLiteDatabase getReadableDatabase(){
	 * openedConnections--; return super.getReadableDatabase(); }
	 */

    @Override
    public synchronized void close() {
        if (mDataBase != null)
            mDataBase.close();
        super.close();
    }

    /**
     * @return
     */
    public SQLiteDatabase getmDataBase() {
        return mDataBase;
    }

    //CR 441
    /**
     * update database tables with the new tables and columns for CR 447
     *
     * @param db
     */
    private static void updateDatabaseTables(SQLiteDatabase db) {
        try {
            //CR 441
            // db.execSQL(CREATE_VISUAL_COMMENTS_TABLE_QUERY);
            db.execSQL("ALTER TABLE ServiceMaterial ADD COLUMN fav INTEGER DEFAULT '0';");

            //added a column to determine whether Inspection done with Operation or Not
        } catch (Exception e) {
            LogUtil.i(TAG, "column already created");
        }

        //CR-447 Inspection
        try
        {
            db.execSQL("CREATE TABLE IF NOT EXISTS VisualComment (idComment INTEGER PRIMARY KEY, Type TEXT, Description TEXT, Action TEXT, Active INTEGER);");
            LogUtil.i(TAG, "VisualComment Table  created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "VisualComment Table  created ");
        }catch(Exception e)
        {
            LogUtil.i(TAG, "Exception : VisualComment Table already created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "Exception : VisualComment Table already created ");
        }

        //Added 3 columns for Inspection (CR-447) {RectPressure, Temperature, VisualCommentsID}
        try
        {
            db.execSQL("ALTER TABLE JobItem ADD COLUMN RectPressure NUMERIC DEFAULT '99.99';");
            LogUtil.i(TAG, "RectPressure column  created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "RectPressure column  created ");
        }catch (Exception e)
        {
            LogUtil.i(TAG, "Exception : RectPressure column already created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "Exception : RectPressure column already created ");
        }

        try
        {
            db.execSQL("ALTER TABLE JobItem ADD COLUMN Temperature INTEGER DEFAULT '0';");
            LogUtil.i(TAG, "Temperature column  created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "Temperature column  created ");
        }catch (Exception e)
        {
            LogUtil.i(TAG, "Exception : Temperature column already created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "Exception : Temperature column already created ");
        }

        try
        {
            db.execSQL("ALTER TABLE JobItem ADD COLUMN VisualCommentsID TEXT DEFAULT '';");
            LogUtil.i(TAG, "VisualCommentsID column  created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "VisualCommentsID column  created ");
        }catch (Exception e)
        {
            LogUtil.i(TAG, "Exception : VisualCommentsID column already created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "Exception : VisualCommentsID column already created ");
        }

        try
        {
            //added a column to determine whether Inspection done with Operation or Not
            db.execSQL("ALTER TABLE JobItem ADD COLUMN InspectionWithOperation INTEGER DEFAULT '0';");
            LogUtil.i(TAG, "InspectionWithOperation column created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "InspectionWithOperation column created ");
        }catch (Exception e)
        {
            LogUtil.i(TAG, "Exception : InspectionWithOperation column already created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables","Exception : InspectionWithOperation column already created ");
        }

        try
        {
            //added a column to determine whether a job item can be added to a snapshot DB or not
            db.execSQL("ALTER TABLE JobItem ADD COLUMN IsPerfect INTEGER DEFAULT '1';");
            LogUtil.i(TAG, "IsPerfect column created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "IsPerfect column created ");
        }catch (Exception e)
        {
            LogUtil.i(TAG, "Exception : IsPerfect column already created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "Exception : IsPerfect column already created ");
        }

        try
        {
            //added a column to determine whether a Job Correction can be added to a snapshot DB or not
            db.execSQL("ALTER TABLE JobCorrection ADD COLUMN IsPerfect INTEGER DEFAULT '1';");
            LogUtil.i(TAG, "IsPerfect(JobCorrection) column created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "IsPerfect(JobCorrection) column created ");
        }catch (Exception e) {
            LogUtil.i(TAG, "Exception : IsPerfect(JobCorrection) column already created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "Exception : IsPerfect(JobCorrection) column already created ");
        }

        //CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
        try
        {
            //added a column to store Previous Odometer Value In Job Table
            db.execSQL("ALTER TABLE Job ADD COLUMN PreviousOdometerValue INTEGER DEFAULT '0';");
            LogUtil.i(TAG, "PreviousOdometerValue (Job) column created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "PreviousOdometerValue (Job) column created ");
        }catch (Exception e)
        {
            LogUtil.i(TAG, "Exception : PreviousOdometerValue (Job) column already created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "Exception : PreviousOdometerValue (Job) column already created ");
        }

        //CR - 550 (some vehicles have different torque settings per axle)
        try
        {
            //added a column to store IsTorqueSettingsSame Value In Job Table
            db.execSQL("ALTER TABLE Job ADD COLUMN TorqueSettingsSame INTEGER DEFAULT '0';");
            LogUtil.i(TAG, "TorqueSettingsSame (Job) column created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "TorqueSettingsSame (Job) column created ");
        }catch (Exception e)
        {
            LogUtil.i(TAG, "Exception : TorqueSettingsSame (Job) column already created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "Exception : TorqueSettingsSame (Job) column already created ");
        }

         /**
          * job-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
          * added a column to store temporary SAPVendor Code information "TempSAPVendorCodeID"  value in tyre Table
         */
        try
        {
            //added a column to store temporary SAPVendor Code information "TempSAPVendorCodeID"  value in tyre Table
            db.execSQL("ALTER TABLE Tyre ADD COLUMN TempSAPVendorCodeID INTEGER;");
            LogUtil.i(TAG, "TempSAPVendorCodeID (Tyre) column created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "TempSAPVendorCodeID (Tyre) column created ");
        }catch (Exception e)
        {
            LogUtil.i(TAG, "Exception : TempSAPVendorCodeID (Tyre) column already created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "Exception : TempSAPVendorCodeID (Tyre) column already created ");
        }

        /**
         * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
         * added a column to store tyre correction change information "IsTyreCorrected"  value in tyre Table
         */
        try
        {
            //added a column to store tyre correction change information "IsTyreCorrected"  value in tyre Table
            db.execSQL("ALTER TABLE Tyre ADD COLUMN IsTyreCorrected INTEGER DEFAULT '0';");
            LogUtil.i(TAG, "IsTyreCorrected (Tyre) column created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "IsTyreCorrected (Tyre) column created ");
        }catch (Exception e)
        {
            LogUtil.i(TAG, "Exception : IsTyreCorrected (Tyre) column already created ");
            LogUtil.DBLog(TAG, "updateDatabaseTables", "Exception : IsTyreCorrected (Tyre) column already created ");
        }

    }

    //To perform DB Upgrade Operation
    public void externalDBUpgradeCall(SQLiteDatabase db)
    {
        updateDatabaseTables(db);
    }

    /**
     * Method to get password Expiration Date from Account Table
     * @return
     */
    private Long getPasswordExpirationFromAccountTable()
    {
        Long passwordExp=0L;
        try{
            String sql = "SELECT PassExpirationDate FROM Account";
            Cursor mCur = mDataBase.rawQuery(sql, null);
            if (mCur != null && mCur.moveToFirst()) {
                passwordExp = mCur.getLong(0);
                return passwordExp;
            }
        }
        catch(Exception e)
        {
            LogUtil.i("updateTablesInDB","get passwordExp"+ "Exception : " +e.getMessage());
            return passwordExp;
        }
        return passwordExp;
    }

    public long getDatabaseSize() {
        try
        {
            if(checkDataBase()) {
                File dbFile = new File(DB_PATH);
                LogUtil.d(TAG," Size of Db : "+ String.valueOf(dbFile.length()));
                return dbFile.length();
            }

        }
        catch (Exception e)
        {
            LogUtil.i("DatabaseHelper","getDatabaseSize"+ "Exception : " +e.getMessage());
        }
        return 0;
    }

}
