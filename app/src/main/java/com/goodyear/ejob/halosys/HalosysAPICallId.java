package com.goodyear.ejob.halosys;

/**
 * Created by giriprasanth.vp
 * Halosys API Call ID's
 */
public class HalosysAPICallId {
    public static final String SERVICE_GET_ESSENTIAL_DATA_DB_ID = "testdb3data23";
    public static final String CLIENTOBJECT_TRANSLATION = "coLocalizationMaster";
    public static final String CLIENTOBJECT_GET_ESSENTIAL_DATA_DB = "coIncrementalDB3Dump";
    public static final String FUNCTION_GET_REUSABLE_STOCK_TYRE = "FnReusableStockBasedOnSAPVendor";
    public static final String FUNCTION_VEHICLE_SEARCH = "VehicleSearchFunction";
}
