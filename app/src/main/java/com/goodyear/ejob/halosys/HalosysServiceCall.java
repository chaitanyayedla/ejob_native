package com.goodyear.ejob.halosys;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbhelpers.StockInfoDataBaseHandler;
import com.goodyear.ejob.halosysobjmodel.EssentialDumpIDResponse;
import com.goodyear.ejob.halosysobjmodel.ReUsableStockInfoModel;
import com.goodyear.ejob.halosysobjmodel.Translation;
import com.goodyear.ejob.halosys.HalosysConstant;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.FileOperations;
import com.goodyear.ejob.util.LogUtil;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.halomem.android.api.HalomemException;
import com.halomem.android.api.IClientObject;
import com.halomem.android.api.IClientObjectType;
import com.halomem.android.api.ISession;
import com.halomem.android.api.impl.ClientObjectType;
import com.halomem.android.api.impl.Session;
import com.halomem.android.api.listeners.OnImageDownload;
import com.halomem.android.api.listeners.OnVerified;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

/**
 * Created by giriprasanth.vp on 10/24/2016.
 * Class to maintain the Halomem service call
 */
public class HalosysServiceCall {
    private static ISession mSession;
    private static String TAG = "HalosysServiceCall";
    private static Object mResponseObj = null;
    private static int mWait;
    private static SQLiteDatabase mDataBase;
    public static final Long Default_DB_ID = -1L;

    /**
     * To maintain token for the valid login
     */
    private static String mSessionToken;

    public static String getmSessionToken() {
        return mSessionToken;
    }

    public static void setmSessionToken(String mSessionToken) {
        HalosysServiceCall.mSessionToken = mSessionToken;
    }

    public HalosysServiceCall() {
        mSessionToken = null;
    }

    public static void initializeSession(Context mContext) {
        try {
            Session.intialize(mContext);
        } catch (HalomemException e) {
            LogUtil.i(TAG, "initializeSession : Halomem Exception - " + e.getMessage());
        } catch (Exception e) {
            LogUtil.i(TAG, "initializeSession : Exception - " + e.getMessage());
        }
    }

    private static ISession getSessionInstance() {
        return Session.getInstance();
    }

    /**
     * Method to call Hybris login
     *
     * @param mActivity     activity context
     * @param paramUserName username
     * @param paramPassword password
     * @return Object(string error and success info)
     */
    public static Object hybrisLogin(final Activity mActivity, final String paramUserName, final String paramPassword) {
        mResponseObj = null;
        setmSessionToken(null);
        try {
            mSession = getSessionInstance();
            mSession.verify(mActivity, new OnVerified() {
                @Override
                public void onSuccess() {
                    try {

                        Log.d(TAG, "username;password :: " + paramUserName + ";" + paramPassword);
                        mSession.login(paramUserName, paramPassword);
                        if (mSession.getAuthToken() == null) {
                            LogUtil.i(TAG, "hybrisLogin :  OnSuccess - token :null");
                            HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                            mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_TOKEN_NULL);
                            mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_TOKEN_NULL);
                            mResponseObj = mhalosysStatusModel;
                        } else {
                            setmSessionToken(mSession.getAuthToken());
                            LogUtil.i(TAG, "hybrisLogin : OnSuccess - token : " + mSession.getAuthToken());
                            HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                            mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_SUCCESS);
                            mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_SUCCESS);
                            mResponseObj = mhalosysStatusModel;
                        }
                    } catch (HalomemException e) {
                        LogUtil.i(TAG, "hybrisLogin : Halomem Exception - OnSuccess - " + e.getMessage());
                        try {
                            HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                            mhalosysStatusModel.setsCode(e.getError().getCode());
                            mhalosysStatusModel.setsMessage(e.getError().getMessage());
                            mResponseObj = mhalosysStatusModel;
                        } catch (Exception ex) {
                            LogUtil.i(TAG, "hybrisLogin : Halomem Exception - OnSuccess -  : Check error - " + e.getMessage());
                            mResponseObj = null;
                        }
                    } catch (Exception e) {
                        LogUtil.i(TAG, "hybrisLogin : Exception -  OnSuccess - " + e.getMessage());
                        HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                        mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
                        mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
                        mResponseObj = mhalosysStatusModel;
                    }
                }

                @Override
                public void onFailure(String s) {
                    LogUtil.i(TAG, "hybrisLogin : onFailure - " + s);
                    HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                    mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_FAILURE);
                    mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_FAILURE);
                    mResponseObj = mhalosysStatusModel;
                }
            });
        } catch (HalomemException e) {
            LogUtil.i(TAG, "hybrisLogin : Halomem Exception - " + e.getMessage());
            try {
                HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                mhalosysStatusModel.setsCode(e.getError().getCode());
                mhalosysStatusModel.setsMessage(e.getError().getMessage());
                mResponseObj = mhalosysStatusModel;
            } catch (Exception ex) {
                LogUtil.i(TAG, "hybrisLogin : Halomem Exception : Check error - " + e.getMessage());
                mResponseObj = null;
            }
        } catch (Exception e) {
            LogUtil.i(TAG, "hybrisLogin : Exception - " + e.getMessage());
            HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
            mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
            mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
            mResponseObj = mhalosysStatusModel;
        }
        return mResponseObj;
    }

    /**
     * Halosys service call to get translation information based on language code
     *
     * @param mContext     Context
     * @param LanguageCode language code
     * @return Halosys Status object
     */
    public static Object getTranslationBasedOnLangCode(Context mContext, final String LanguageCode) {
        mResponseObj = null;
        try {
            if(getmSessionToken()!=null) {
                mSession = getSessionInstance();
                LogUtil.i(TAG, "getTranslationBasedOnLangCode : onSuccess");
                try {
                    List<IClientObject> mEmpClientObjectList;
                    IClientObjectType mEmplClientObject = new ClientObjectType(HalosysAPICallId.CLIENTOBJECT_TRANSLATION);
                    List<String> fields = ImmutableList.of(HalosysConstant.CO_LOCALISED_TEXT, HalosysConstant.CO_TEXT_CODE,
                            HalosysConstant.CO_LANGUAGE_CODE, HalosysConstant.CO_LANGUAGE_ID);

                    mEmplClientObject = mSession.getClientObjectType(HalosysAPICallId.CLIENTOBJECT_TRANSLATION);
                    final Map<String, Object> mFilterParams = Maps.newHashMap();
                    mFilterParams.put(HalosysConstant.CO_LANGUAGE_CODE, LanguageCode);
                    mEmpClientObjectList = mEmplClientObject.search(0, 1000, mFilterParams, fields, false);
                    DatabaseAdapter execute = new DatabaseAdapter(mContext);
                    try {
                        Gson gson = new Gson();
                        execute.open();
                        if (mEmpClientObjectList.size() > 0) {
                            for (int i = 0; i < mEmpClientObjectList.size(); i++) {

                                try {
                                    Map<String, Object> data = mEmpClientObjectList.get(i).getData();
                                    JSONObject jsonObject = new JSONObject(data);
                                    Translation mtranslation = gson.fromJson(jsonObject.toString(), Translation.class);
                                    execute.insertIntoTranslation(mtranslation.getTextCode(), mtranslation.getLocalizedText());
                                } catch (Exception e) {
                                    LogUtil.i(TAG, "getTranslationBasedOnLangCode : Exception - OnSuccess GSON - " + e.getMessage());
                                }
                            }
                        }
                        execute.getValues();
                        execute.copyFinalDB();
                    } catch (Exception e) {
                        LogUtil.i(TAG, "getTranslationBasedOnLangCode : Exception - OnSuccess - " + e.getMessage());
                    } finally {
                        if (execute != null) {
                            execute.close();
                        }
                    }
                    mEmplClientObject.getName();
                    HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                    mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_SUCCESS);
                    mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_SUCCESS);
                    mResponseObj = mhalosysStatusModel;

                } catch (HalomemException e) {
                    LogUtil.i(TAG, "getTranslationBasedOnLangCode : Halomem Exception - OnSuccess - " + e.getMessage());
                    try {
                        HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                        mhalosysStatusModel.setsCode(e.getError().getCode());
                        mhalosysStatusModel.setsMessage(e.getError().getMessage());
                        mResponseObj = mhalosysStatusModel;
                    } catch (Exception ex) {
                        LogUtil.i(TAG, "getTranslationBasedOnLangCode : Halomem Exception - OnSuccess -  : Check error - " + e.getMessage());
                        mResponseObj = null;
                    }
                } catch (Exception e) {
                    LogUtil.i(TAG, "getTranslationBasedOnLangCode : Exception -  OnSuccess - " + e.getMessage());
                    HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                    mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
                    mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
                    mResponseObj = mhalosysStatusModel;
                }
            }
            else
            {
                HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_TOKEN_NULL);
                mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_TOKEN_NULL);
                mResponseObj = mhalosysStatusModel;
            }

        } catch (Exception e) {
            LogUtil.i(TAG, "getTranslationBasedOnLangCode : Exception - " + e.getMessage());
            HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
            mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
            mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
            mResponseObj = mhalosysStatusModel;
        }
        return mResponseObj;
    }


    /**
     * Halosys service call to get Essential Data db3 file
     *
     * @return Halosys status obj
     */
    public static Object getEssentialData() {
        mResponseObj = null;
        mWait = 1;
        try {
            if(getmSessionToken()!=null) {
                mSession = getSessionInstance();
                LogUtil.i(TAG, "getEssentialData : onSuccess");
                try {
                    IClientObjectType mEmplClientObject = new ClientObjectType(HalosysAPICallId.CLIENTOBJECT_GET_ESSENTIAL_DATA_DB);

                /*
                   Code to get ClientObject list (For testing  purpose Only)
                 */
                /*List<IClientObject> mEmpClientObjectList;
                List<String> fields = ImmutableList.of("CreationTS", "DumpID", "GUID", "Incremental_DB3_data", "UserID");
                mEmpClientObjectList=mEmplClientObject.list(0,1000,fields,false);
                final Map<String, Object> mFilterParams = Maps.newHashMap();
                mFilterParams.put("GUID", "ADCED5A9-8B18-4F44-BF96-D813DC936824");*/

                    mEmplClientObject = mSession.getClientObjectType(HalosysAPICallId.CLIENTOBJECT_GET_ESSENTIAL_DATA_DB);

                    ImmutableMap<String, Object> params = ImmutableMap.<String, Object>builder()
                            .put(HalosysConstant.ESSENTIAL_DATA_MAP_FIELDS, HalosysConstant.ESSENTIAL_DATA_DBFIELD_COLUMN_NAME)
                            .build();

                    OnImageDownload mUserEssentialDataDB = new OnImageDownload() {
                        @Override
                        public void onDownloadResult(byte[] bytes) {
                            try {
                                byte[] x1 = bytes;
                                String fileD = Constants.PROJECT_PATH + Constants.RESPONSEFILE_PATH;

                                File file = new File(fileD);
                                if (!file.exists()) {
                                    file.getParentFile().mkdirs();
                                    file.createNewFile();
                                }

                                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fileD));
                                bos.write(x1);
                                bos.flush();
                                bos.close();
                                FileOperations.backupResponse(true);
                                LogUtil.i(TAG, "getEssentialData : OnImageDownload - mUserEssentialDataDB -Success ");
                                HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                                mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_SUCCESS);
                                mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_SUCCESS);
                                mResponseObj = mhalosysStatusModel;
                                mWait = 0;
                            } catch (Exception ex) {
                                LogUtil.i(TAG, "onDownloadResult - Exception - " + ex.getMessage());
                                HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                                mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
                                mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
                                mResponseObj = mhalosysStatusModel;
                                mWait = 0;
                            }
                        }
                    };

                    Long mDatabaseID = getEssentialDataDBID();
                    if (mDatabaseID != Default_DB_ID && mDatabaseID > 0) {
                        mEmplClientObject.downloadBinaryField(mDatabaseID, params, mUserEssentialDataDB, false);

                        int count = 1;
                        while (mWait == 1) {
                            Thread.sleep(1000);
                            count++;
                        }
                        LogUtil.i(TAG, "getEssentialData : count : " + count);
                    } else {
                        HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                        mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_ERROR_DATABASE_ID);
                        mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_ERROR_DATABASE_ID);
                        mResponseObj = mhalosysStatusModel;
                    }

                } catch (HalomemException e) {
                    LogUtil.i(TAG, "getEssentialData : Halomem Exception - OnSuccess - " + e.getMessage());
                    try {
                        HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                        mhalosysStatusModel.setsCode(e.getError().getCode());
                        mhalosysStatusModel.setsMessage(e.getError().getMessage());
                        mResponseObj = mhalosysStatusModel;
                    } catch (Exception ex) {
                        LogUtil.i(TAG, "getEssentialData : Halomem Exception - OnSuccess -  : Check error - " + e.getMessage());
                        mResponseObj = null;
                    }
                } catch (Exception e) {
                    LogUtil.i(TAG, "getEssentialData : Exception -  OnSuccess - " + e.getMessage());
                    HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                    mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
                    mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
                    mResponseObj = mhalosysStatusModel;
                }
            }
            else
            {
                HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_TOKEN_NULL);
                mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_TOKEN_NULL);
                mResponseObj = mhalosysStatusModel;
            }
        } catch (Exception e) {
            LogUtil.i(TAG, "getEssentialData : Exception - " + e.getMessage());
            HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
            mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
            mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
            mResponseObj = mhalosysStatusModel;
        }
        return mResponseObj;
    }

    /**
     * Halosys service call to get reusable tyre stock info by SAP Vendor Code
     *
     * @param mSAPVendorCode SAP vendor Code
     * @return error object or response
     */
    public static Object getReusableStockInfoBySAPVendorCode(Context mContext, String mSAPVendorCode) {
        mResponseObj = null;
        ReUsableStockInfoModel reUsableStockInfoModel;
        mWait = 1;
        try {
            if(getmSessionToken()!=null) {
                mSession = getSessionInstance();
                LogUtil.i(TAG, "getEssentialData : onSuccess");
                Gson gson = new Gson();
                try {
                    Map<String, Object> queryParams = Maps.newHashMap();
                    queryParams.put(HalosysConstant.FORMAT, HalosysConstant.JSON);
                    queryParams.put(HalosysConstant.SAP_VENDOR_CODE, mSAPVendorCode);
                    Object mResponseJson = mSession.executeFunction(HalosysAPICallId.FUNCTION_GET_REUSABLE_STOCK_TYRE, queryParams, false);
                    reUsableStockInfoModel = gson.fromJson(mResponseJson.toString(), ReUsableStockInfoModel.class);
                    StockInfoDataBaseHandler handler = new StockInfoDataBaseHandler(mContext);
                    handler.openDB();
                    handler.createTablesInDB();
                    handler.insertStockInfo(reUsableStockInfoModel, mSAPVendorCode);
                    handler.copyDataBase();
                    handler.deleteDatabaseAfterLoad();
                    handler.closeDB();
                    HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                    mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_SUCCESS);
                    mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_SUCCESS);
                    mResponseObj = mhalosysStatusModel;
                } catch (HalomemException ex) {
                    LogUtil.i(TAG, "getEssentialData : Halomem Exception  " + ex.getMessage());
                    try {
                        HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                        mhalosysStatusModel.setsCode(ex.getError().getCode());
                        mhalosysStatusModel.setsMessage(ex.getError().getMessage());
                        mResponseObj = mhalosysStatusModel;
                    } catch (Exception ex1) {
                        LogUtil.i(TAG, "getReusableStockInfoBySAPVendorCode : Halomem Exception  : Check error - " + ex1.getMessage());
                        mResponseObj = null;
                    }
                } catch (Exception ex) {
                    LogUtil.i(TAG, "getReusableStockInfoBySAPVendorCode : Exception - " + ex.getMessage());
                    HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                    mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
                    mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
                    mResponseObj = mhalosysStatusModel;
                }
            }
            else
            {
                HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_TOKEN_NULL);
                mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_TOKEN_NULL);
                mResponseObj = mhalosysStatusModel;
            }
        } catch (Exception e) {
            LogUtil.i(TAG, "getReusableStockInfoBySAPVendorCode : Exception - " + e.getMessage());
            HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
            mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
            mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
            mResponseObj = mhalosysStatusModel;
        }
        return mResponseObj;
    }

    /**
     * Halosys service call to search vehicle
     *
     * @param mSAPVendorCode SAP vendor Code
     * @return error object or response
     */
    public static Object getSearchVehicle(Context mContext, String mSAPVendorCode, String mCountry, String mSearchKey) {
        mResponseObj = null;
        // ReUsableStockInfoModel reUsableStockInfoModel;
        mWait = 1;
        try {
            if(getmSessionToken()!=null) {
                mSession = getSessionInstance();
                LogUtil.i(TAG, "getSearchVehicle : onSuccess");
                Gson gson = new Gson();
                try {
                    Map<String, Object> queryParams = Maps.newHashMap();
                    queryParams.put(HalosysConstant.FORMAT, HalosysConstant.JSON);
                    queryParams.put(HalosysConstant.COUNTRY, HalosysConstant.COUNTRY_NL);
                    queryParams.put(HalosysConstant.LICENSE_PLATE, HalosysConstant.LICENSE_CCE);
                    queryParams.put(HalosysConstant.SAP_VENDOR_CODE, HalosysConstant.VENDOR_4000096);
                    Object mResponseJson = mSession.executeFunction(HalosysAPICallId.FUNCTION_VEHICLE_SEARCH, queryParams, false);
                } catch (HalomemException ex) {
                    LogUtil.i(TAG, "getEssentialData : Halomem Exception  " + ex.getMessage());
                    try {
                        HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                        mhalosysStatusModel.setsCode(ex.getError().getCode());
                        mhalosysStatusModel.setsMessage(ex.getError().getMessage());
                        mResponseObj = mhalosysStatusModel;
                    } catch (Exception ex1) {
                        LogUtil.i(TAG, "getReusableStockInfoBySAPVendorCode : Halomem Exception  : Check error - " + ex1.getMessage());
                        mResponseObj = null;
                    }
                } catch (Exception ex) {
                    LogUtil.i(TAG, "getReusableStockInfoBySAPVendorCode : Exception - " + ex.getMessage());
                    HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                    mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
                    mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
                    mResponseObj = mhalosysStatusModel;
                }
            }
            else
            {
                HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_TOKEN_NULL);
                mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_TOKEN_NULL);
                mResponseObj = mhalosysStatusModel;
            }
        } catch (Exception e) {
            LogUtil.i(TAG, "getReusableStockInfoBySAPVendorCode : Exception - " + e.getMessage());
            HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
            mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
            mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
            mResponseObj = mhalosysStatusModel;
        }
        return mResponseObj;
    }

    /**
     * Halosys service call to get Essential data DB3 file ID
     * @return Database ID
     */
    public static Long getEssentialDataDBID() {
        mResponseObj = null;
        try {
            if(getmSessionToken()!=null) {
                Object obj;
                mSession = getSessionInstance();
                LogUtil.i(TAG, "getEssentialDataDBID : onSuccess");
                obj = mSession.executeService(HalosysAPICallId.SERVICE_GET_ESSENTIAL_DATA_DB_ID, null, false);
                Gson gson = new Gson();
                EssentialDumpIDResponse essentialDumpIDResponse = gson.fromJson(obj.toString(), EssentialDumpIDResponse.class);
                if (essentialDumpIDResponse != null) {
                    return essentialDumpIDResponse.getDumpID();
                } else {
                    return Default_DB_ID;
                }
            }
            else
            {
                HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_TOKEN_NULL);
                mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_TOKEN_NULL);
                mResponseObj = mhalosysStatusModel;
                return Default_DB_ID;
            }
        }
           catch (HalomemException e){
               LogUtil.i(TAG, "getEssentialDataDBID : HalomemException - " + e.getMessage());
               HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
               mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
               mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
               mResponseObj = mhalosysStatusModel;
               return Default_DB_ID;
            }
            catch (Exception e)
            {
                LogUtil.i(TAG, "getEssentialDataDBID : Exception - " + e.getMessage());
                HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
                mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
                mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
                mResponseObj = mhalosysStatusModel;
                return Default_DB_ID;
            }
    }

    public static Object clearSessionInfo()
    {
        mResponseObj = null;
        mSession = getSessionInstance();
        try {
            setmSessionToken(null);
            if(mSession!=null) {
                mSession.logout();
            }
            HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
            mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_SUCCESS);
            mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_SUCCESS);
            mResponseObj = mhalosysStatusModel;
        }
        catch (HalomemException e)
        {
            LogUtil.i(TAG, "clearSessionInfo : HalomemException - " + e.getMessage());
            HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
            mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
            mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
            mResponseObj = mhalosysStatusModel;
        }
        catch (Exception e)
        {
            LogUtil.i(TAG, "clearSessionInfo : Exception - " + e.getMessage());
            HalosysStatusModel mhalosysStatusModel = new HalosysStatusModel();
            mhalosysStatusModel.setsCode(HalosysCallStatus.CODE_UNKNOWN_ERROR);
            mhalosysStatusModel.setsMessage(HalosysCallStatus.MESSAGE_UNKNOWN_ERROR);
            mResponseObj = mhalosysStatusModel;
        }
        return mResponseObj;
    }


    private static void createTyreTable(String dbPath) {
        String mPath = dbPath;
        try {
            mDataBase = SQLiteDatabase.openDatabase(mPath, null,
                    SQLiteDatabase.CREATE_IF_NECESSARY);
            String queryString = "CREATE TABLE IF NOT EXISTS Tyre(ID INTEGER PRIMARY KEY, ExternalID TEXT NOT NULL, Pressure NUMERIC, SerialNumber TEXT, SAPCodeVeh INTEGER, NSK1 INTEGER, NSK2 INTEGER, NSK3 INTEGER,SAPMaterialCodeID INTEGER, Position TEXT, SAPCodeTi INTEGER, SAPCodeWp INTEGER, OriginalNSK NUMERIC, SAPCodeAxID TEXT, IsUnmounted INTEGER, IsSpare INTEGER,SAPContractNumberID INTEGER, SAPVendorCodeID INTEGER, idVehicleJob INTEGER, Status INTEGER, Active INTEGER, Shared INTEGER DEFAULT 0, FromJobID INTEGER, TempSAPVendorCodeID INTEGER, IsTyreCorrected INTEGER DEFAULT 0)GO";
            mDataBase.execSQL(queryString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
