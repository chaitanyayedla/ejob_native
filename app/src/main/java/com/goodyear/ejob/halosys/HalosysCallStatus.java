package com.goodyear.ejob.halosys;

/**
 * Created by giriprasanth.vp on 10/26/2016.
 */
public class HalosysCallStatus {
    public static final int CODE_SUCCESS = 8000;
    public static final int CODE_UNKNOWN_ERROR = 8001;
    public static final int CODE_TOKEN_NULL = 8002;
    public static final int CODE_FAILURE = 8003;
    public static final int CODE_ERROR_DATABASE_ID = 8004;

    //Login based issue
    public static final int CODE_DENIED_ACCESS_TO_RESOURCE = 45;

    //Client object based issue
    public static final int CODE_INVALID_CLIENT_OBJECT_TYPE = 43;


    /*
    public static final int CODE_BAD_CREDENTIAL = 0;
    public static final int CODE_INVALID_TOKEN_ERROR = 0;
    public static final int CODE_ACCESS_DENIED = 0;*/

    public static final String MESSAGE_SUCCESS = "Success";
    public static final String MESSAGE_UNKNOWN_ERROR = "Unknown error";
    public static final String MESSAGE_TOKEN_NULL = "Token NULL";
    public static final String MESSAGE_FAILURE = "Failure";
    public static final String MESSAGE_ERROR_DATABASE_ID = "Error with Database ID";

}
