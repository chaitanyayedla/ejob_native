package com.goodyear.ejob.halosys;

/**
 * Created by giriprasanth.vp on 10/26/2016.
 */
public class HalosysStatusModel {
    public int getsCode() {
        return sCode;
    }

    public void setsCode(int sCode) {
        this.sCode = sCode;
    }

    public String getsMessage() {
        return sMessage;
    }

    public void setsMessage(String sMessage) {
        this.sMessage = sMessage;
    }

    private int sCode;
    private String sMessage;

}
