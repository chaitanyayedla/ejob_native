package com.goodyear.ejob.halosys;

/**
 * Created by giriprasanth.vp
 * Halosys service call related constant
 */
public class HalosysConstant {
    public static final String ESSENTIAL_DATA_MAP_FIELDS = "fields";
    public static final String ESSENTIAL_DATA_DBFIELD_COLUMN_NAME = "Incremental_DB3_data";
    public static final String COUNTRY = "country";
    public static final String FORMAT = "format";
    public static final String LICENSE_PLATE = "licenseplate";
    public static final String SAP_VENDOR_CODE = "SAPVendorCode";
    public static final String JSON = "json";
    public static final String COUNTRY_NL = "nl";
    public static final String LICENSE_CCE = "cce*";
    public static final String VENDOR_4000096 = "4000096";
    public static final String CO_LOCALISED_TEXT = "LocalizedText";
    public static final String CO_TEXT_CODE =  "TextCode";
    public static final String CO_LANGUAGE_CODE = "LanguageCode";
    public static final String CO_LANGUAGE_ID = "LanguageId";
}
