package com.goodyear.ejob.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.goodyear.ejob.R;

/**
 * @author amit
 * 
 */

public class SummaryListAdaptor extends BaseAdapter {

	private static final String Layout_INFLATER_SERVICE = null;
	private ArrayList<HashMap<String, String>> itemDetailsrrayList;
	private LayoutInflater l_Inflater;

	private int selectedPos = -1;
	public Context type_context;

	/**
	 * @param context
	 * @param results
	 */
	public SummaryListAdaptor(Context context,
			ArrayList<HashMap<String, String>> results) {
		type_context = context;
		itemDetailsrrayList = results;
		l_Inflater = LayoutInflater.from(context);

	}

	public void JobList_Adapter_refresh(
			ArrayList<HashMap<String, String>> results) {
		itemDetailsrrayList = results;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	public int getCount() {
		return itemDetailsrrayList.size();
	}

	/**
	 * @param pos
	 */
	public void setSelectedPosition(int pos) {
		selectedPos = pos;
		// inform the view of this change
		notifyDataSetChanged();
	}

	/**
	 * @return
	 */
	public int getSelectedPosition() {
		return selectedPos;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	public Object getItem(int position) {
		return itemDetailsrrayList.get(position);
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	public long getItemId(int position) {
		return position;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {

			convertView = l_Inflater.inflate(R.layout.summary_listitem, null);

			holder = new ViewHolder();
			holder.txt_serial = (TextView) convertView
					.findViewById(R.id.sum_serialnum);
			holder.txt_design = (TextView) convertView
					.findViewById(R.id.sum_design);
			holder.sum_action = (TextView) convertView
					.findViewById(R.id.sum_action);
			holder.sum_repComapny = (TextView) convertView
					.findViewById(R.id.sum_rep);
			holder.sum_workOrder = (TextView) convertView
					.findViewById(R.id.sum_workord);
			holder.sum_operation = (TextView) convertView
					.findViewById(R.id.sum_op);

			/*
			 * if(Constant._imageVisiblity == false){
			 * holder.image =
			 * (ImageView)convertView.findViewById(R.id.tick_save_back);
			 * holder.image.setVisibility(View.INVISIBLE);
			 * }else{
			 */
			holder.image = (ImageView) convertView
					.findViewById(R.id.sum_tick_save);

			// }
			/*
			 * holder.txt_date= (TextView)
			 * convertView.findViewById(R.id.TextView08);
			 * holder.txt_ref_no= (TextView)
			 * convertView.findViewById(R.id.TextView07);
			 */

			/*
			 * holder.txt_lp.setSelected(true);
			 * holder.txt_customer.setSelected(true);
			 */
			/*
			 * holder.txt_date.setSelected(true);
			 * holder.txt_ref_no.setSelected(true);
			 */
			convertView.setTag(holder);
		} else {

			holder = (ViewHolder) convertView.getTag();
		}

		try {
			holder.txt_serial.setText(itemDetailsrrayList.get(position).get(
					"SerialNumber"));
			holder.txt_design.setText(itemDetailsrrayList.get(position).get(
					"FullTireDetails"));

			holder.sum_action.setText(itemDetailsrrayList.get(position).get(
					"ActionType"));
			holder.sum_repComapny.setText(itemDetailsrrayList.get(position)
					.get("RepairCompany"));
			holder.sum_workOrder.setText(itemDetailsrrayList.get(position).get(
					"WorkOrderNumber"));
			holder.sum_operation.setText(itemDetailsrrayList.get(position).get(
					"Description"));

			/*
			 * if
			 * (itemDetailsrrayList.get(position).get("status").equalsIgnoreCase
			 * ("false")) {
			 * holder.image.setVisibility(View.INVISIBLE);
			 * }else{
			 * holder.image.setVisibility(View.VISIBLE);
			 * }
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * holder.txt_date.setText(itemDetailsrrayList.get(position).get(
		 * "created_date"));
		 * holder.txt_ref_no.setText(itemDetailsrrayList.get(position).get(
		 * "cc_ref_no"));
		 */
		return convertView;
	}

	static class ViewHolder {
		TextView txt_serial, txt_design, txt_date, txt_lp;
		TextView txt_cust, txt_vendor;
		TextView sum_action;
		TextView sum_repComapny;
		TextView sum_workOrder;
		TextView sum_operation;

		ImageView image;

	}

	/*
	 * public void getTyreDetailsGONE()
	 * {
	 * if(itemDetailsrrayList.get("ActionType").equalsIgnoreCase("4"))
	 * {
	 * SumAction.setText("Re-Groove");
	 * }
	 * else if(sum_action.equalsIgnoreCase("18"))
	 * {
	 * SumAction.setText("Minor Repair");
	 * }
	 * else if(sum_action.equalsIgnoreCase("19"))
	 * {
	 * SumAction.setText("Major Repair");
	 * }
	 * SumRepairCom.setText(SummaryRepairCompany);
	 * SumOrderNum.setText(SummaryWorkOrderNumber);
	 * SumOperation.setText(SummaryOperation);
	 * }
	 */

}
