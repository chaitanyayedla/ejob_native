package com.goodyear.ejob.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.goodyear.ejob.fragment.EjobAdditionalServicesFragment;
import com.goodyear.ejob.fragment.EjobAdditionalServicesSummaryFragment;
import com.goodyear.ejob.fragment.EjobAxleServicesFragment;
import com.goodyear.ejob.fragment.EjobInformationFragment;
import com.goodyear.ejob.fragment.EjobSignatureBoxFragment;
import com.goodyear.ejob.fragment.EjobTimeLocationFragment;
import com.goodyear.ejob.fragment.EjobVehicleSummaryFragment;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.util.Constants;

/**
 * @author shailesh.p
 * The Class holds the data needed to load the View-pager
 * for EjobFOrmActionActivity
 */
public class EJobPagerAdapter extends FragmentPagerAdapter {
	// public static int count=2;
	public static int count;
	public int mSelectedIndex = -1;

	/**
	 * Class Constructor
	 * @param fm
	 */
	public EJobPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {
		mSelectedIndex = index;
		if (Constants.COMESFROMVIEW) {
			/*Changes for Jira EJOB-63 start*/
			/*Commenting following code to add job summary page while viewing a completed job*/
			/*if(Constants.NOADDITIONALSERVICES){
				switch (index) {
				case 0:
					return new VehicleSkeletonFragment();
				case 1:
					return new EjobVehicleSummaryFragment();
				case 2:
					return new EjobSignatureBoxFragment();
				}
			}else{*/
				switch (index) {
				case 0:
					return new VehicleSkeletonFragment();
				case 1:
					return new EjobVehicleSummaryFragment();
				case 2:
					return new EjobAdditionalServicesSummaryFragment();
				case 3:
					return new EjobSignatureBoxFragment();
				}
			/*}*/
			/*Changes for Jira EJOB-63 end*/
		} else {
			if (Constants.JOBTYPEREGULAR) {
				
				if(Constants.GEMOTRY_CHECK_CORRECTION){
					switch (index) {
					case 0:
						return new EjobInformationFragment();
					case 1:
						return new EjobTimeLocationFragment();
					case 2:
						return new VehicleSkeletonFragment();
					case 3:
						return new EjobAdditionalServicesFragment();
					case 4:
						return new EjobVehicleSummaryFragment();
					case 5:
						return new EjobAdditionalServicesSummaryFragment();
					case 6:
						return new EjobSignatureBoxFragment();
					}
				}else{
					switch (index) {
					case 0:
						return new EjobInformationFragment();
					case 1:
						return new EjobTimeLocationFragment();
					case 2:
						return new VehicleSkeletonFragment();
					case 3:
						return new EjobAxleServicesFragment();
					case 4:
						return new EjobAdditionalServicesFragment();
					case 5:
						return new EjobVehicleSummaryFragment();
					case 6:
						return new EjobAdditionalServicesSummaryFragment();
					case 7:
						return new EjobSignatureBoxFragment();
					}
				}
				
			} else {
				switch (index) {
				case 0:
					return new EjobInformationFragment();
				case 1:
					return new EjobTimeLocationFragment();
				case 2:
					return new VehicleSkeletonFragment();
				case 3:
					return new EjobAdditionalServicesFragment();
				case 4:
					return new EjobVehicleSummaryFragment();
				case 5:
					return new EjobAdditionalServicesSummaryFragment();
				case 6:
					return new EjobSignatureBoxFragment();
				}
			}
		}
		return null;
	}

	@Override
	public int getCount() {
		return count;
	}
	/**
	 * Method returning selected position of the view-pager
	 * It returns the current index of the view-pager
	 * @return
	 */
	public int getSelectedIndex(){
		return mSelectedIndex-1;
	}
	/**
	 * Method returning current fragment index on which 
	 * user is working 
	 * @return
	 */
	public Fragment getSelectedFragment() {
		if (mSelectedIndex-1 < 0) {
			return null;
		}
		return getItem(mSelectedIndex-1);
	}

}
