
package com.goodyear.ejob.adapter;

import java.util.List;
import java.util.Map;

import com.goodyear.ejob.R;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;

/**
 * @author johnmiya.s
 *
 */
public class JSONListAdapter extends SimpleAdapter{

	/**
	 * @param context
	 * @param data
	 * @param resource
	 * @param from
	 * @param to
	 */
	public Context type_context;
	public JSONListAdapter(Context context,
			List<? extends Map<String, ?>> data, int resource, String[] from,
			int[] to) {
		super(context, data, resource, from, to);
		type_context = context;
		// TODO Auto-generated constructor stub
	}
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
      View view = super.getView(position, convertView, parent);
      if (position % 2 == 1) {
    	  view.setBackgroundColor(type_context.getResources().getColor(R.color.listview_color_up)); // color.white
		} else {
			view.setBackgroundColor(type_context.getResources().getColor(R.color.listview_color_dn)); // color.ltgreay
		}
      return view;
    }
}
