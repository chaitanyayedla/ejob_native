package com.goodyear.ejob.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.goodyear.ejob.R;
import com.goodyear.ejob.util.Constants;

public class JobList_Home_Adapter extends BaseAdapter {

	private ArrayList<HashMap<String, String>> itemDetailsrrayList;
	private LayoutInflater l_Inflater;

	private int selectedPos = -1;
	public Context type_context;

	/**
	 * @param context
	 * @param results
	 */
	public JobList_Home_Adapter(Context context,
			ArrayList<HashMap<String, String>> results) {
		type_context = context;
		itemDetailsrrayList = results;

		l_Inflater = LayoutInflater.from(context);
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	public int getCount() {
		return itemDetailsrrayList.size();
	}

	/**
	 * @param pos
	 */
	public void setSelectedPosition(int pos) {
		selectedPos = pos;
		// inform the view of this change
		notifyDataSetChanged();
	}

	/**
	 * @return
	 */
	public int getSelectedPosition() {
		return selectedPos;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	public Object getItem(int position) {
		return itemDetailsrrayList.get(position);

	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	public long getItemId(int position) {
		return getItem(position).hashCode();
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {

			// convertView = l_Inflater.inflate(R.layout.items_newjob_home,
			// null);
			convertView = l_Inflater.inflate(R.layout.items_newjob_home, null);
			holder = new ViewHolder();
			holder.txt_lp = (TextView) convertView
					.findViewById(R.id.TextView06);

			holder.txt_customer = (TextView) convertView
					.findViewById(R.id.TextView05);

			holder.txt_date = (TextView) convertView
					.findViewById(R.id.TextView08);

			holder.txt_ref_no = (TextView) convertView
					.findViewById(R.id.TextView07);

			holder.tireIcon = (ImageView) convertView
					.findViewById(R.id.imageView1);

			if (Constants.COMESFROMUPDATE) {
				holder.tireIcon.setImageResource(R.drawable.incompleted_job);
			} else {
				holder.tireIcon.setImageResource(R.drawable.tire_management);
			}

			holder.txt_lp.setSelected(true);
			holder.txt_customer.setSelected(true);
			holder.txt_date.setSelected(true);
			holder.txt_ref_no.setSelected(true);
			holder.tireIcon.setSelected(true);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.txt_lp.setText(itemDetailsrrayList.get(position).get("licence"));
		holder.txt_customer.setText(itemDetailsrrayList.get(position).get(
				"cname"));
		holder.txt_date.setText(itemDetailsrrayList.get(position).get("date"));
		holder.txt_ref_no.setText(itemDetailsrrayList.get(position).get(
				"ref_no"));

		String status = itemDetailsrrayList.get(position).get("status");
		Log.e("Jaani", "***********************" + status);

		if (status.equalsIgnoreCase("1")) {
			holder.tireIcon.setImageResource(R.drawable.incompleted_job);
		} else if (status.equalsIgnoreCase("5")) {
			holder.tireIcon.setImageResource(R.drawable.tire_management);
		} else if (status.equalsIgnoreCase("0")) {
			holder.tireIcon.setImageResource(R.drawable.completed_job);
		}
		/*
		 * else if(status.equalsIgnoreCase("0")){
		 * holder.tireIcon.setImageResource(R.drawable.completed_job);
		 * }
		 */
		return convertView;
	}

	static class ViewHolder {
		TextView txt_customer, txt_ref_no, txt_date, txt_lp;
		ImageView tireIcon;

	}
}
