
package com.goodyear.ejob.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goodyear.ejob.R;
import com.goodyear.ejob.VisualRemarksActivity;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.VisualRemark;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.LogUtil;

import java.util.ArrayList;

/**
 * @author giriprasanth.vp
 *         VisualRemarkListAdapter Class to Display Visual Remarks List
 */
public class VisualRemarkListAdapter extends BaseAdapter {

    private ArrayList<VisualRemark> mVisualRemarks;
    private Context mContext;
    private LayoutInflater l_Inflater;
    private int mLongSelectedPosition;
    //User Trace logs trace tag
    private static final String TRACE_TAG = "Visual Remarks - AD";
    /**
     * VisualRemarkListAdapter Constructor
     *
     * @param visualRemarks VisualRemark Array Data Object
     * @param context       Object which allows access to application-specific resources
     */
    public VisualRemarkListAdapter(ArrayList<VisualRemark> visualRemarks, Context context) {
        this.mVisualRemarks = visualRemarks;
        this.mContext = context;
        l_Inflater = LayoutInflater.from(mContext);
        mLongSelectedPosition = 0;
    }

    /**
     * Method to added new Visual Remark from List
     *
     * @param vRemark Visual Remark Object
     */
    public Boolean addVisualRemark(VisualRemark vRemark) {
        int count = 0;
        for (int i=0;i<mVisualRemarks.size();i++) {
            if (!mVisualRemarks.get(count).getVRId().equals(vRemark.getVRId())) {
                count++;
            }
        }
        if (count == mVisualRemarks.size()) {
            CommonUtils.notify(VisualRemarksActivity.mVisualRemarkAdded, mContext);
            mVisualRemarks.add(vRemark);
            //User Trace logs
            try {
                LogUtil.TraceInfo(TRACE_TAG, "1 VR Added", vRemark.toString(), false, true, false);
            }
            catch (Exception  e)
            {
                LogUtil.TraceInfo(TRACE_TAG, "1 VR Add - Error", e.getMessage(), false, true, false);
            }
            notifyDataSetChanged();
            return true;
        } else
        CommonUtils.notify(VisualRemarksActivity.mVisualRemarkAlreadyExist,mContext);
        return false;
    }

    /**
     * Method to delete Visual Remark from List
     *
     * @param position data position
     */
    public void deleteVisualRemark(int position) {
        CommonUtils.notify(VisualRemarksActivity.mVisualRemarkDeleted,mContext);
        //User Trace logs
        try {
            LogUtil.TraceInfo(TRACE_TAG, "1 VR Deleted", mVisualRemarks.get(position).toString(), false, true, false);
        }
        catch(Exception e)
        {
            LogUtil.TraceInfo(TRACE_TAG, "1 VR Delete - Error",e.getMessage(), false, true, false);
        }
        mVisualRemarks.remove(position);
        notifyDataSetChanged();
        ((VisualRemarksActivity) mContext).saveButtonTrigger(true);
    }

    /**
     * Method to return Updated Visual Remark List
     *
     * @return mVisualRemarks
     */
    public ArrayList<VisualRemark> returnVisualRemarkList() {
        if (mVisualRemarks == null)
            return new ArrayList<VisualRemark>();
        return mVisualRemarks;
    }

    /**
     * Method to change Visual Remark List
     * @param visualRemarks
     */
    public void changeVisualRemarkList(ArrayList<VisualRemark> visualRemarks)
    {
        if(this.mVisualRemarks!=null)
        {
            this.mVisualRemarks.clear();
        }
        this.mVisualRemarks=visualRemarks;
        notifyDataSetChanged();
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        return mVisualRemarks.size();
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return mVisualRemarks.get(position);
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int position) {

        return 0;
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        ViewHolder holder = new ViewHolder();
        if (row == null) {
            row = l_Inflater.inflate(R.layout.items_visual_remark, null);
            holder = new ViewHolder();
            holder.txt_Type = (TextView) row.findViewById(R.id.tv_type);
            holder.txt_Desc = (TextView) row.findViewById(R.id.tv_desc);
            holder.txt_Do = (TextView) row.findViewById(R.id.tv_do);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        try {
            holder.txt_Type.setText(mVisualRemarks.get(position).getType());
            holder.txt_Desc.setText(mVisualRemarks.get(position).getDescription());
            holder.txt_Do.setText(mVisualRemarks.get(position).getDo());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (position % 2 == 0) {
            row.setBackgroundColor(mContext.getResources().getColor(R.color.listview_color_dn)); // color.white
        } else {
            row.setBackgroundColor(mContext.getResources().getColor(R.color.listview_color_up)); // color.ltgreay
        }

        return row;
    }


    /**
     * View Holder Class to Hold Data
     */
    static class ViewHolder {
        TextView txt_Type, txt_Desc, txt_Do;
    }

    /**
     * Public method to delete visual remark based on position
     * @param lPosition item position
     */
    public void deleteLongPressedItem(int lPosition)
    {
        mLongSelectedPosition=lPosition;
        if (mVisualRemarks.size() > mLongSelectedPosition) {
            getUserConformationForDeleteAction();
            notifyDataSetChanged();
        }
    }

    /**
     * Method to pop a Decision Dialog before Deleting a Visual Remark from List
     */
    private void getUserConformationForDeleteAction() {
        // get prompts.xml view
        LayoutInflater inflator = LayoutInflater.from(mContext);
        View promptsView = inflator.inflate(R.layout.dialog_yes_no, null);
        TextView dialogText = (TextView) promptsView
                .findViewById(R.id.dialog_text_message);

        // load message
        DatabaseAdapter label = new DatabaseAdapter(mContext);
        label.createDatabase();
        label.open();

        dialogText.setText(VisualRemarksActivity.mAreYouSureWantToDeleteVisualRemark);

        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(mContext);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        // set user activity for
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton(label.getLabel(Constants.YES_LABEL),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteVisualRemark(mLongSelectedPosition);
                        notifyDataSetChanged();
                    }
                });

        alertDialogBuilder.setCancelable(false).setNegativeButton(label.getLabel(Constants.NO_LABEL),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

}
