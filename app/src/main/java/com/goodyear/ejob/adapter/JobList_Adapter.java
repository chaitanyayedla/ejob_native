package com.goodyear.ejob.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.goodyear.ejob.R;

public class JobList_Adapter extends BaseAdapter {

	private ArrayList<HashMap<String, String>> itemDetailsrrayList;
	private LayoutInflater l_Inflater;

	private int selectedPos = -1;
	public Context type_context;
	

	/**
	 * @param context
	 * @param results
	 */
	public JobList_Adapter(Context context,
			ArrayList<HashMap<String, String>> results) {
		type_context = context;
		itemDetailsrrayList = results;
		l_Inflater = LayoutInflater.from(context);
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	public int getCount() {
		return itemDetailsrrayList.size();
	}

	/**
	 * @param pos
	 */
	public void setSelectedPosition(int pos) {
		selectedPos = pos;
		// inform the view of this change
		notifyDataSetChanged();
	}

	/**
	 * @return
	 */
	public int getSelectedPosition() {
		return selectedPos;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	public Object getItem(int position) {
		return itemDetailsrrayList.get(position);
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	public long getItemId(int position) {
		return position;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewHolder holder = new ViewHolder();

		if (row == null) {

			row = l_Inflater.inflate(R.layout.items_newjob, null);
			
			holder = new ViewHolder();
			holder.txt_lp = (TextView) row.findViewById(R.id.TextView06);

			holder.txt_customer = (TextView) row.findViewById(R.id.TextView05);

			holder.txt_lp.setSelected(true);
			holder.txt_customer.setSelected(true);
			
			row.setTag(holder);
		} else {
			holder = (ViewHolder)row.getTag();
		}

		holder.txt_lp.setText(itemDetailsrrayList.get(position).get("licence"));
		holder.txt_customer.setText(itemDetailsrrayList.get(position).get("cname"));
		
		if (position % 2 == 1) {
			row.setBackgroundColor(type_context.getResources().getColor(R.color.listview_color_dn)); // color.white
		} else {
			row.setBackgroundColor(type_context.getResources().getColor(R.color.listview_color_up)); // color.ltgreay
		}

		return row;
	}

	static class ViewHolder {
		TextView txt_customer, txt_ref_no, txt_date, txt_lp;

	}
}
