package com.goodyear.ejob.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.goodyear.ejob.R;
import com.goodyear.ejob.sync.Constant;
import com.goodyear.ejob.util.Constants;

/**
 * @author johnmiya.s Adaptor Class handling the list of items need to displayed
 *         on the ejobList Activity
 */
public class EjobListAdapter extends BaseAdapter {

	Context mContext;
	ArrayList<HashMap<String, String>> mListItems;
	private LayoutInflater mLayoutInflater;
	private ArrayList<Integer> selectedItemIds = new ArrayList<Integer>();

	public EjobListAdapter(Context context,
			ArrayList<HashMap<String, String>> listItems,
			boolean checkedValueUpdatedOnRotation) {
		this.mContext = context;
		this.mListItems = listItems;
		mLayoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (!checkedValueUpdatedOnRotation)
			Constants.CHECKED_JOB_LIST = new ArrayList<String>();
	}

	/**
	 * Method Returning the Selected items It is getter method for the EjobList
	 * 
	 * @return
	 */
	public ArrayList<Integer> getSelectedItemIds() {
		return selectedItemIds;
	}

	/**
	 * Method Setting the Selected items in the arrayList It is setter method
	 * for the EjobList
	 * 
	 * @return
	 */
	public void setSelectedItemIds(ArrayList<Integer> selectedItemIds) {
		this.selectedItemIds = selectedItemIds;
	}

	@Override
	public int getCount() {
		return mListItems.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return getItem(position).hashCode();
	}

	/**
	 * Method loading the view of the list-view of the performed job
	 */
	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		ViewHolder holder;
		if (convertView == null) {

			convertView = mLayoutInflater.inflate(R.layout.items_newjob_home2,
					null);
			holder = new ViewHolder();
			holder.txt_lp = (TextView) convertView
					.findViewById(R.id.TextView06);

			holder.txt_customer = (TextView) convertView
					.findViewById(R.id.TextView05);

			holder.txt_date = (TextView) convertView
					.findViewById(R.id.TextView08);

			holder.txt_ref_no = (TextView) convertView
					.findViewById(R.id.TextView07);

			holder.tireIcon = (ImageView) convertView
					.findViewById(R.id.imageView1);
			holder.checkBox = (CheckBox) convertView
					.findViewById(R.id.checkBox1);

			holder.txt_lp.setSelected(true);
			holder.txt_customer.setSelected(true);
			holder.txt_date.setSelected(true);
			holder.txt_ref_no.setSelected(true);
			holder.tireIcon.setSelected(true);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if (position % 2 == 1) {
			convertView.setBackgroundColor(Color.parseColor("#E6E6E6")); // color.white
		} else {
			convertView.setBackgroundColor(Color.parseColor("#D8D8D8")); // color.ltgreay
		}
		holder.txt_lp.setText(mListItems.get(position).get("licence"));
		holder.txt_customer.setText(mListItems.get(position).get("cname"));

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateInString = mListItems.get(position).get("date");
		String mDeviceDateFormate = "";
		try {
			// TODO Optimize the code
			Date date = formatter.parse(dateInString);
			java.text.DateFormat dateFormat = android.text.format.DateFormat
					.getDateFormat(mContext);
			mDeviceDateFormate = dateFormat.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}

		holder.checkBox.setVisibility(View.VISIBLE);

		holder.txt_date.setText(mDeviceDateFormate);
		holder.txt_ref_no.setText(mListItems.get(position).get("ref_no"));
		if (mListItems.get(position).get("status")
				.equals(Constants.INCOMPLETED_JOB_STATUS)) {
			holder.checkBox.setVisibility(View.INVISIBLE);
		}
		if (mListItems.get(position).get("status")
				.equals(Constants.DELETED_JOB_STATUS)) {
			holder.checkBox.setVisibility(View.INVISIBLE);
		}

		//Fix for Constants.CHECKED_JOB_LIST Null Issue.
		if(Constants.CHECKED_JOB_LIST==null)
		{
			Constants.CHECKED_JOB_LIST = new ArrayList<>();
		}

		// fix added for checkBox issue in landscape
		if (Constants.CHECKED_JOB_LIST.contains(mListItems.get(position).get(
				"ref_no"))) {
			holder.checkBox.setChecked(true);
		} else {
			holder.checkBox.setChecked(false);
		}
		holder.checkBox
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							selectedItemIds.add(position);
							if (!Constants.CHECKED_JOB_LIST.contains(mListItems
									.get(position).get("ref_no"))) {
								Constants.CHECKED_JOB_LIST.add(mListItems.get(
										position).get("ref_no"));
							}
						} else {
							if (Constants.CHECKED_JOB_LIST.contains(mListItems
									.get(position).get("ref_no"))) {
								Constants.CHECKED_JOB_LIST.remove(mListItems
										.get(position).get("ref_no"));
							}
							selectedItemIds.remove(((Integer) position));
						}

						if (isChecked) {
							if (mListItems.get(position).get("status")
									.equals(Constants.COMPLETED_JOB_STATUS)) {
								mListItems.get(position).put("status",
										Constants.COMPLETED_CHECKED_JOB_STATUS);
								mListItems.get(position).put("check_status",
										"true");
							}

							if (mListItems.get(position).get("status")
									.equals(Constants.TM_JOB_STATUS)) {
								mListItems
										.get(position)
										.put("status",
												Constants.COMPLETED_CHECKED_FOR_SYNC_JOB_STATUS);
								mListItems.get(position).put("check_status",
										"true");
							}
						}

						else {
							if (mListItems
									.get(position)
									.get("status")
									.equals(Constants.COMPLETED_CHECKED_JOB_STATUS)) {
								mListItems.get(position).put("status",
										Constants.COMPLETED_JOB_STATUS);
								mListItems.get(position).put("check_status",
										"false");
							}

							if (mListItems
									.get(position)
									.get("status")
									.equals(Constants.COMPLETED_CHECKED_FOR_SYNC_JOB_STATUS)) {
								mListItems.get(position).put("status",
										Constants.TM_JOB_STATUS);
								mListItems.get(position).put("check_status",
										"false");
							}
						}
					}
				});

		String status = mListItems.get(position).get("status");
		if (status.equalsIgnoreCase(Constants.INCOMPLETED_JOB_STATUS)) {
			holder.tireIcon.setImageResource(R.drawable.incompleted_job);
		} else if (status.equalsIgnoreCase(Constants.TM_JOB_STATUS)
				|| status
						.equalsIgnoreCase(Constants.COMPLETED_CHECKED_FOR_SYNC_JOB_STATUS)) {
			holder.tireIcon.setImageResource(R.drawable.tire_management);
		} else if (status
				.equalsIgnoreCase(Constants.COMPLETED_CHECKED_JOB_STATUS)) {
			holder.tireIcon.setImageResource(R.drawable.completed_job);
		} else if (status.equalsIgnoreCase(Constants.COMPLETED_JOB_STATUS)) {
			holder.tireIcon.setImageResource(R.drawable.completed_job);
		} else if (status.equalsIgnoreCase(Constants.DELETED_JOB_STATUS)) {
			holder.tireIcon.setImageResource(R.drawable.deleted_job);
		}
		return convertView;
	}

	public void cleanCheckedJobList() {
		Constants.CHECKED_JOB_LIST = null;
	}

	public ArrayList<String> getSelectedJobs() {
		return Constants.CHECKED_JOB_LIST;
	}

	public void setCheckedJobList(ArrayList<String> checkedList) {
		if (checkedList != null) {
			Constants.CHECKED_JOB_LIST = checkedList;
		} else {
			Constants.CHECKED_JOB_LIST = new ArrayList<>();
		}
	}

	/**
	 * @author amitkumar.h Class containing all the initializations of the UI
	 *         elements need for ejobList Activity
	 */
	static class ViewHolder {
		TextView txt_customer;
		TextView txt_ref_no;
		TextView txt_date;
		TextView txt_lp;
		ImageView tireIcon;
		CheckBox checkBox;
	}

}
