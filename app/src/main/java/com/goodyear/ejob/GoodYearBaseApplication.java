package com.goodyear.ejob;

import android.app.Application;
import android.util.Log;

import com.goodyear.crashhandling.CrashCommons;
import com.goodyear.ejob.inactivity.LogoutHandler;

public class GoodYearBaseApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		// start logout handler broadcast receiver
		LogoutHandler mLogoutHandler = new LogoutHandler(this);
		// To remove the crash handling make CrashCommons.mHandleCrash this
		// boolean as false.
		if (CrashCommons.mHandleCrash) {
			Log.i("GoodYearBaseApplication", "Initial Setup for crash handling");
			CrashCommons.getMyInstance(this);
		}
	}
}
