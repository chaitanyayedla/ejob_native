package com.goodyear.ejob.performance;

import android.content.Context;
import android.os.AsyncTask;

import com.goodyear.ejob.util.GYProgressDialog;

/**
 * @author nikhil.v
 * 
 */
public class PerformanceBaseModel implements IOnPerformanceCallback {

	Context context;
	IOnPerformanceCallback callback;
	boolean showDialog;
	public GYProgressDialog mGYProgressDialog;
	//ProgressDialog mGYProgressDialog;
	public static String MESSAGE_STR = "NO_MESSAGE_EJOB";

	/**
	 * 
	 */
	public PerformanceBaseModel(Context context,
			IOnPerformanceCallback callback, boolean showDialog) {
		this.context = context;
		this.callback = callback;
		if(callback==null){
			throw new NullPointerException();
		}
		this.showDialog = showDialog;
		if (showDialog) {
			mGYProgressDialog = new GYProgressDialog(context);
			//mGYProgressDialog = new ProgressDialog(context);
		}
	}

	/**
	 * 
	 */
	public void perform() {
		String toExecute = callback.onPreExecute();
		if (MESSAGE_STR.equalsIgnoreCase(toExecute)) {
			//callback.closePreviousTask();
		//	showProgressBar(true);
			ReturnSkeltonAsynck asynck = new ReturnSkeltonAsynck(context, this);
			asynck.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			ResponseMessagePerformance message = new ResponseMessagePerformance();
			message.setErrorMessage(toExecute);
			onError(message);
		}
	}

	@Override
	public void updateTables() {
		callback.updateTables();
	}

	@Override
	public void showProgressBar(boolean toShow) {
		if (!showDialog) {
			callback.showProgressBar(toShow);
		} else if (showDialog && !toShow) {
			dismissProgress();
		} else if (showDialog && toShow) {
			showProgress();
		} 
	}

	@Override
	public void onPostExecute(ResponseMessagePerformance message) {
		callback.onPostExecute(message);
	}

	/**
	 * show progress dialog
	 */
	private void showProgress() {
		try {
			mGYProgressDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Dismiss progress dialog
	 */
	private void dismissProgress() {

		try {
			if (mGYProgressDialog != null && mGYProgressDialog.isShowing()) {
				mGYProgressDialog.dismiss();
				mGYProgressDialog = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String onPreExecute() {
		return callback.onPreExecute();
	}

	@Override
	public void onError(ResponseMessagePerformance message) {
		callback.onError(message);
	}

	/* (non-Javadoc)
	 * @see com.goodyear.ejob.performance.IOnPerformanceCallback#closePreviousTask()
	 */
	@Override
	public void closePreviousTask() {
		callback.closePreviousTask();
	}

}
