package com.goodyear.ejob.performance;

/**
 * @author nikhil.v
 * 
 */
public interface IOnPerformanceCallback {
	public String onPreExecute();
	
	public void closePreviousTask();

	public void updateTables();

	/**
	 * 
	 */
	public void showProgressBar(boolean toShow);

	/**
	 * 
	 */
	void onPostExecute(ResponseMessagePerformance message);
	/**
	 * 
	 */
	void onError(ResponseMessagePerformance message);
}
