package com.goodyear.ejob.performance;

import android.content.Context;
import android.os.AsyncTask;

/**
 * @author nikhil.v
 * 
 */
public class ReturnSkeltonAsynck extends AsyncTask<Void, Void, Void> {

	IOnPerformanceCallback mCallback;
	Context mContext;

	public ReturnSkeltonAsynck(Context context, IOnPerformanceCallback callback) {
		this.mCallback = callback;
		this.mContext = context;
	}

	@Override
	protected void onPreExecute() {
		//super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) {
		mCallback.updateTables();
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		//super.onPostExecute(result);
		mCallback.showProgressBar(false);
		ResponseMessagePerformance message = new ResponseMessagePerformance();
		mCallback.onPostExecute(message);
	}
}
