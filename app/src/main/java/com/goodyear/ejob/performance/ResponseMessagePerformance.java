package com.goodyear.ejob.performance;

/**
 * @author nikhil.v
 * 
 */
public class ResponseMessagePerformance {

	private String mErrorMessage = "";

	/**
	 * @return the mErrorMessage
	 */
	public String getErrorMessage() {
		return mErrorMessage;
	}

	/**
	 * @param mErrorMessage
	 *            the mErrorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.mErrorMessage = errorMessage;
	}

}
