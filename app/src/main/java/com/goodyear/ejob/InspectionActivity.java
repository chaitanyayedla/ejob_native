/**
 * TuronOnRim Operations
 */
package com.goodyear.ejob;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.goodyear.ejob.blutooth.BluetoothService;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.job.operation.helpers.PressureCheckListener;
import com.goodyear.ejob.job.operation.helpers.TurnOnRimHelper;
import com.goodyear.ejob.performance.IOnPerformanceCallback;
import com.goodyear.ejob.performance.PerformanceBaseModel;
import com.goodyear.ejob.performance.ResponseMessagePerformance;
import com.goodyear.ejob.util.CameraUtility;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.TireDesignDetails;
import com.goodyear.ejob.util.Tyre;

import java.util.ArrayList;
import java.util.UUID;

/**
 * @version 1.0
 *
 */
public class InspectionActivity extends Activity implements InactivityHandler {

	/**
	 * Brand Array List
	 */
	private ArrayList<String> mBrandArrayList;
	/**
	 * Size Array List
	 */
	private ArrayList<String> mSizeArrayList;
	/**
	 * Rim Array List
	 */
	private ArrayList<String> mTyreTRIMArrayList;

	/**
	 * ASP Array List
	 */
	private ArrayList<String> mTyreTASPArrayList;
	/**
	 * Design Array List
	 */
	private ArrayList<String> mDesignArrayList;
	/**
	 * FullDesign Details Array List
	 */
	private ArrayList<String> mFullDesignArrayList;
	/**
	 * DataBase Helper Object
	 */
	private DatabaseAdapter mDbHelper;
	/**
	 * string variable to store external ID
	 */
	private String external_id = "";
	/**
	 * Size ArrayAdapter
	 */
	private ArrayAdapter<String> mSizeDataAdapter;
	/**
	 * Rim ArrayAdapter
	 */
	private ArrayAdapter<String> mRimDataAdapter;
	/**
	 * ASP ArrayAdapter
	 */
	private ArrayAdapter<String> mASPDataAdapter;
	/**
	 * Design ArrayAdapter
	 */
	private ArrayAdapter<String> mDesignDataAdapter;
	/**
	 * FullDesignDetails ArrayAdapter
	 */
	private ArrayAdapter<String> mFullDesignDataAdapter;
	/**
	 * Selected Brand Name
	 */
	private String mSelectedBrandName;
	/**
	 * Selected Tyre Size
	 */
	private String mSelectedtyreSize;
	/**
	 * Selected Tyre ASP
	 */
	private String mSelectedTyreTASP;
	/**
	 * Selected Tyre RIM
	 */
	private String mSelectedtyreTRIM;
	/**
	 * Selected Tyre Design
	 */
	private String mSelectedTyreDesign;
	/**
	 * Selected Tyre Detail Design
	 */
	private String mSelectedTyreDetailedDesign;
	/**
	 * Final variable for PHOTO_INTENT
	 */
	public static final int PHOTO_INTENT = 105;

	/**
	 * boolean variable for is Camera enabled
	 */
	private boolean mCameraEnabled = false;
	/**
	 * Selected Tire Object
	 */
	private Tyre mSelectedTire;
	/**
	 * Brand for Spinner
	 */
	private Spinner mBrand;
	/**
	 * Spinner Object for Size
	 */
	private Spinner mSize;
	/**
	 * Spinner Object for ASP
	 */
	private Spinner mASP;
	/**
	 * Spinner Object for Rim
	 */
	private Spinner mRim;
	/**
	 * Spinner Object for Design
	 */
	private Spinner mDesign;
	/**
	 * Spinner Object for Tyre
	 */
	private Spinner mType;
	/**
	 * TextView Reference for SerialNumber
	 */
	private TextView mSerialNumber;
	/**
	 * TextView Reference for WheelPosition
	 */
	private TextView mWheelPosition;
	/**
	 * TextView Reference for NSK lable
	 */
	private TextView mLabelNSK;

	/**
	 * TextView Reference for Regroove Type label
	 */
	private TextView mLblRegrooveType;
	/**
	 * RadioGroup Reference for Regroove Type
	 */
	private RadioGroup mRGRegrooveType;
	/**
	 * RadioGroup Reference for Regroove Type Yes
	 */
	private RadioButton mRegrooveTypeYes;
	/**
	 * RadioGroup Reference for Regroove Type No
	 */
	private RadioButton mRegrooveTypeNo;
	/**
	 * TextView reference for Dimension label
	 */
	private TextView mDimensionLabel;
	/**
	 * TextView reference for ASP label
	 */
	private TextView mASPLabel;
	/**
	 * TextView reference for Rim label
	 */
	private TextView mRimLabel;
	/**
	 * TextView reference for Design label
	 */
	private TextView mDesignLabel;
	/**
	 * TextView reference for Size label
	 */
	private TextView mSizeLabel;
	/**
	 * TextView reference for Type lable
	 */
	private TextView mTypeLabel;
	/**
	 * TextView reference for Serial No Label
	 */
	private TextView mSerialNumberLabel;
	/**
	 * TextView reference for Brand Label
	 */
	private TextView mBrandLabel;
	/**
	 * Recommended Pressure
	 */
	private float mRecommendedpressure = 0;
	/**
	 * ScrollView Reference
	 */
	private ScrollView mMainScrollBar;
	/**
	 * Message for BackPress
	 */
	private String mBackPressMessage;
	/**
	 * EditText reference for NSK1
	 */
	private EditText mNskOne;
	/**
	 * EditText reference for NSK2
	 */
	private EditText mNskTwo;
	/**
	 * EditText reference for NSK3
	 */
	private EditText mNskThree;
	/**
	 * TextView reference for Rim Label type
	 */
	private TextView mLabelRimType;
	/**
	 * ImageView for Camera icon
	 */
	private ImageView mCamera_icon;
	/**
	 * TurnOnRimHelper Object
	 */
	private TurnOnRimHelper mTurnOnRimInfo;
	/**
	 * Rec Pressure Layout Object
	 */
	private LinearLayout mRecPressureLayout;
	/**
	 * Rec Pressure Value
	 */
	private EditText mEditRecPressureValue;
	/**
	 * Rec Pressure Switch Object
	 */
	private Switch mRecValueAdjust;
	/**
	 * NoteText variable
	 */
	private static String sNoteText = "";


	private String convertedRecPressureValue="";
	String strbluetoothconnectionfalied = "",
			strTOR_NSK_VALUES_INTERCHANGED_MSG;
	/**
	 * boolean variable for changed mode.
	 */
	private boolean mChangeMade;
	/**
	 * Final variable for NOTES_INTENT
	 */
	public static final int NOTES_INTENT = 105;

	/**
	 * Final variable for OBSERVATION_INTENT
	 */
	public static final int OBSERVATION_INTENT = 209;
	/**
	 * boolean variable for swiped
	 */
	private boolean mSwiped;
	/**
	 * boolean variable for is pressue measurement in PSI
	 */
	private boolean mIsSettingsPSI;
	/**
	 * BluetoothService object
	 */
	private BluetoothService mBTService;
	/**
	 * BroadcastReceiver Object
	 */
	private BroadcastReceiver mReceiver;
	/**
	 * NSK Counter
	 */
	private int mNskCounter = 1;
	/**
	 * Regroove blank message
	 */
	private String mRegrove_blank_msg;
	/**
	 * Selected SAP material Code
	 */
	private String mSelectedSAPMaterialCodeID;
	/**
	 * boolean variable for isSizeSpinner values editable or not
	 */
	private boolean mIsSizeSpinnerEditable = true;
	/**
	 * boolean variable for tost messages
	 */
	private boolean showToast = false;
	/**
	 * Constant value for warning message for Details Required
	 */
	private String mDesignDetailRequired = "Design Details Required";

	/**
	 * Default value for Spinner
	 */
	private String mDefaultValueForSpinner = "Please Select";
	/**
	 * Last Spinner Selection Index, This variable using Spinner manipulation
	 * while orientation
	 */
	private int mLastSpinnerSelection = 0;
	/**
	 * IsOrientationChagned, This variable using Spinner manipulation while
	 * orientation
	 */
	private boolean mIsOrientationChanged = false;
	private String mYeslabel;
	private String mNoLabel;
	private String mNotesLabel;

	Bundle savedInstanceState;
	/**
	 * String message from DB for Please Select Brand
	 */
	private String mPleaseSelectBrand = "";

	BTTask task;
	private TextView mJobDetailsLabel;
	private EditText mEditPressure;
	private boolean mBlockPressureFromBluetooth;
	private String pressureRequired;
	private String mFinalConvertedPressure;
	private TextView pressureUnit;
	private TextView recPressureUnit;
	private int axlePosition;
	private String mTORConfirmationMessage = "";

	private String mRecPressureRequired;
	private String mTemperatureRequired;
	private String mObservation;
	private String mAboutToInspectSelectedTyre;

	/**
	 * RadioGroup Reference for Temperature
	 */
	private RadioGroup mTemperature;
	/**
	 * RadioGroup Reference for Temperature
	 */
	private RadioButton mTemperatureYes;
	/**
	 * RadioGroup Reference for Temperature
	 */
	private RadioButton mTemperatureNo;
	/**
	 * RadioGroup Reference for Temperature
	 */
	private int controlFromResult;

	/**
	 * Initial Pressure Over Axle
	 */
	private float mInitialpressure = 0;

	private String TAG="Inspection Activity";

	/**
	 * Block the Brand and tyre details input and its related validation
	 * 0- to unblock, any other value will block
	 */
	private int blockBrandAndTyreDetails=0;

	/**
	 * TextView Widgets for Inspection Label
	 */
	private TextView lblPressure;
	private TextView lblAdjust;
	private TextView lblPressureRec;
	private TextView lblTemperature;


	//User Trace logs trace tag
	private static final String TRACE_TAG = "Inspection";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.savedInstanceState = savedInstanceState;
		setContentView(R.layout.activity_inspection);
		//User Trace logs
		LogUtil.TraceInfo(TRACE_TAG, "none", "OP", true, false, true);
		//blocking for both maintained and non maintained
		blockBrandAndTyreDetails = 1;
		Intent intent = getIntent();
		if (null != intent
				&& intent.hasExtra(VehicleSkeletonFragment.mAXLEPRESSURE)) {

			//Commented : setting pressure value as zero so that, user can enter new pressure values for tyres
//			mRecommendedpressure = intent.getExtras().getFloat(
//					VehicleSkeletonFragment.mAXLEPRESSURE);
			mInitialpressure = intent.getExtras().getFloat(
					VehicleSkeletonFragment.mAXLEPRESSURE);

					axlePosition = intent.getExtras().getInt(
							VehicleSkeletonFragment.mAXLEPRESSURE_POS);
		}
		if (savedInstanceState == null) {
			sNoteText = "";
		}
		showToast = savedInstanceState == null;
		mSelectedTire = Constants.SELECTED_TYRE;
		initialize();
		mSelectedSAPMaterialCodeID = Constants.SELECTED_TYRE
				.getSapMaterialCodeID();
		if (TextUtils.isEmpty(Constants.SELECTED_TYRE.getSerialNumber())) {
			mSerialNumber.setText(Constants.EDITED_SERIAL_NUMBER);
		}else {
			mSerialNumber.setText(mSelectedTire.getSerialNumber());
		}

		if(TextUtils.isEmpty(Constants.SELECTED_TYRE.getSerialNumber())
				|| Constants.SELECTED_TYRE.getSerialNumber().equalsIgnoreCase("N/R")) {
			//if serial number is empty ot N/R we will block the Spinner input for Brand and tyre details
			blockBrandAndTyreDetails = 1;
		}

		mWheelPosition.setText(mSelectedTire.getPosition());
		TextView wheelPositionLabel = (TextView) findViewById(R.id.lbl_wp);
		wheelPositionLabel.setText(Constants.sLblWheelPos);
		if (savedInstanceState != null) {
			mNskCounter = savedInstanceState.getInt("nsk_counter", 0);
			mIsOrientationChanged = true;

			mSelectedBrandName = savedInstanceState.getString("BRAND");
			if (savedInstanceState.containsKey("SIZE")) {
				mSelectedtyreSize = savedInstanceState.getString("SIZE");
			}
			if (savedInstanceState.containsKey("ASP")) {
				mSelectedTyreTASP = savedInstanceState.getString("ASP");
			}
			if (savedInstanceState.containsKey("RIM")) {
				mSelectedtyreTRIM = savedInstanceState.getString("RIM");
			}
			if (savedInstanceState.containsKey("DESIGN")) {
				mSelectedTyreDesign = savedInstanceState.getString("DESIGN");
			}
			if (savedInstanceState.containsKey("FULLDETAIL")) {
				mSelectedTyreDetailedDesign = savedInstanceState
						.getString("FULLDETAIL");
			}
			mIsSizeSpinnerEditable = savedInstanceState.getBoolean(
					"mIsSizeSpinnerEditable", true);
			checkLatestSpinnerSelectionBeforeOrientationChanges();
		}


		if (TextUtils.isEmpty(mSelectedTyreDetailedDesign)
				|| mDefaultValueForSpinner.equals(mSelectedTyreDetailedDesign)) {
			TireDesignDetails detail = CommonUtils
					.getDetailsFromTyreInSameAxle(mSelectedTire);
			if (null != detail) {
				mSelectedtyreSize = detail.getSize();
				mSelectedTyreTASP = detail.getAsp();
				mSelectedtyreTRIM = detail.getRim();
				mIsSizeSpinnerEditable = false;
			}
		}
		if (!mIsSizeSpinnerEditable || Constants.onBrandBool) {
			loadSizeDetailsFromAxle();
			mIsSizeSpinnerEditable = false;
		}
		if (Constants.onBrandBool
				|| Constants.SELECTED_TYRE.getSerialNumber().equalsIgnoreCase(
				"")
				|| TextUtils
				.isEmpty(Constants.SELECTED_TYRE.getDesignDetails())
				|| mDefaultValueForSpinner.equals(mSelectedTyreDetailedDesign)) {
			loadTyreDetailsOnBrandCorrection();
		} else {
			loadTyreDetails();
		}

		restoreDialogState(savedInstanceState);
		// For Auto Swap
	}
	/**
	 * Overridden method storing all the captured data inside bundle object in
	 * order to handle different orientations
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		LogUtil.i("BT",
				"****************** onSaveInstanceState **********************");
		if (mBrand.getCount() != 0) {
			outState.putString("BRAND", mBrand.getSelectedItem().toString());
		}
		if (mSize.getCount() != 0) {
			outState.putString("SIZE", mSize.getSelectedItem().toString());
		}
		if (mASP.getCount() != 0) {
			outState.putString("ASP", mASP.getSelectedItem().toString());
		}
		if (mRim.getCount() != 0) {
			outState.putString("RIM", mRim.getSelectedItem().toString());
		}
		if (mDesign.getCount() != 0) {
			outState.putString("DESIGN", mDesign.getSelectedItem().toString());
		}
		if (mType.getCount() != 0) {
			outState.putString("FULLDETAIL", mType.getSelectedItem().toString());
		}
		if (mBTService != null) {
			outState.putBoolean("isworkerstopped", mBTService.isWorkerThread());
		}
		outState.putBoolean("mIsSizeSpinnerEditable", mIsSizeSpinnerEditable);
		outState.putInt("nsk_counter", mNskCounter);
		//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
		outState.putBoolean("VisibilityToRecPressure", mRecValueAdjust.isChecked());

		//pressure enable/disable state retaining
		outState.putBoolean("pressure_enabled", mEditPressure.isEnabled());
		saveDialogState(outState);
	}

	@Override
	protected void onStop() {
		LogUtil.i("BT",
				"****************** onStop *****************:mBTService: "
						+ mBTService);
		try {
			if (null != mBTService) {
				mBTService.stop();
				if (task != null && !task.isCancelled()) {
					mBTService.asyncCancel(true);
					task.cancel(true);
					task = null;
				}
			}
			if (null != mReceiver) {
				unregisterReceiver(mReceiver);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onStop();
	}
	/**
	 * Overriden method handling application inactivity by updating the
	 * user-interaction with the application
	 */
	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	@Override
	protected void onStart() {
		// reset countdown timer
		LogoutHandler.setCurrentActivity(this);

		//additional check (whether control is transferred from notes and observation.
		if (!mBlockPressureFromBluetooth)
			mEditPressure.setEnabled(true);
		//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
		if (controlFromResult==1)
			mEditPressure.setEnabled(Constants.PRESSURE_EDIT_STATE);
		if (mBTService != null) {
			mBTService.stop();
		}

		//assigning default value
		controlFromResult=0;
		//mEditPressure.setEnabled(true);
		mNskOne.setEnabled(true);
		mNskTwo.setEnabled(true);
		mNskThree.setEnabled(true);
		if (mBTService != null) {
			mBTService.stop();
		}
		mBTService = new BluetoothService(this);
		if (savedInstanceState != null) {
			boolean isWorkerThread = savedInstanceState
					.getBoolean("isworkerstopped");
			mBTService.setWorkerThread(isWorkerThread);
		}

		Swiper swipeDetector = new Swiper(this, mMainScrollBar);
		mMainScrollBar.setOnTouchListener(swipeDetector);
		IntentFilter intentFilter = new IntentFilter("BLUETOOTH_SENDER");
		mReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.hasExtra("BT_DATA_NSK")) {
					String nsk_value = intent.getStringExtra("BT_DATA_NSK");
					if (mNskCounter == 1) {
						mNskOne.setText(nsk_value);
						mNskTwo.setText(nsk_value);
						mNskThree.setText(nsk_value);
						mNskCounter = 2;
					} else if (mNskCounter == 2) {
						mNskTwo.setText(nsk_value);
						mNskCounter = 3;
					} else if (mNskCounter == 3) {
						mNskCounter = 1;
						mNskThree.setText(nsk_value);
					}
				} else if (intent.hasExtra("BT_DATA_PRE")) {
					String pre_value = intent.getStringExtra("BT_DATA_PRE");
					if (!mBlockPressureFromBluetooth) {
						if (mIsSettingsPSI) {
							String barValue;
							// change PSI to BAR
							barValue = CommonUtils.getPressureValue(context,
									CommonUtils.parseFloat(pre_value));
							mEditPressure.setText(String.valueOf(barValue));
						} else {
							mEditPressure.setText(pre_value);
						}
					}
				} else if (intent.hasExtra("BT_CONN_STATUS")) {
					Toast.makeText(getApplicationContext(),
							strbluetoothconnectionfalied, Toast.LENGTH_LONG)
							.show();
				} else if (intent.hasExtra("BT_STATUS_IS_DISCONNECTED")) {
					boolean isDisconnected = intent.getBooleanExtra(
							"BT_STATUS_IS_DISCONNECTED", false);
					LogUtil.i(
							"Bluetooth in my Operation",
							"Broadcast came to my oeration:: " + isDisconnected
									+ " mBTService: "
									+ mBTService.isConnected());
					mEditPressure.setEnabled(isDisconnected);
					mNskOne.setEnabled(isDisconnected);
					mNskTwo.setEnabled(isDisconnected);
					mNskThree.setEnabled(isDisconnected);
					if (!mBlockPressureFromBluetooth)
						mEditPressure.setEnabled(isDisconnected);
					if (isDisconnected) {
						mBTService.stop();
						initiateBT();
						// mBTService.beginListeningData(0);
					} else {
						LogUtil.i("TOR", "######## Makeing stopworker false ");
						// mBTService.resetSocket();
						mBTService.startReading();
						// mBTService.beginListeningData(0);
					}
				} else if (intent.hasExtra("BT_RETRY_FAIL")) {
					initiateBT();
				}
			}
		};
		try {
			registerReceiver(mReceiver, intentFilter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		initiateBT();
		super.onStart();
	}

	@Override
	public void onResume() {

		//Based on blockBrandAndTyreDetails value, spinner is enabled or disabled
		if(blockBrandAndTyreDetails!=0)
		{
			mBrand.setEnabled(false);
			mSize.setEnabled(false);
			mASP.setEnabled(false);
			mRim.setEnabled(false);
			mDesign.setEnabled(false);
			mType.setEnabled(false);
		}
		super.onResume();

	}
	/**
	 * Method initializing Blue-tooth, checking for connectivity and performing
	 * data transfer while using blue-tooth probes.
	 */
	private void initiateBT() {
		int status = mBTService.getPairedStatus();
		switch (status) {
			case 0:
				Toast.makeText(getApplicationContext(),
						Constants.sLblMultiplePaired, Toast.LENGTH_LONG).show();
				break;
			case 1:
				LogUtil.e("TOR", "Stating AsyncTask initiateBT > ");
				task = new BTTask();
				task.execute();
				break;
			case 2:
				Toast.makeText(getApplicationContext(),
						Constants.sLblPleasePairTLogik, Toast.LENGTH_LONG).show();
				break;
			case 3:
				Toast.makeText(getApplicationContext(),
						Constants.sLblNoPairedDevices, Toast.LENGTH_LONG).show();
				break;
		}
	}
	/**
	 * class handling blue-tooth functionality in a separate non-UI threads and
	 * performing data transfer while using blue-tooth probes.
	 */
	class BTTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... f_url) {
			try {
				if (isCancelled()) {
					LogUtil.e("TOR", "doInBackground Async Task Cancelled  > ");
					return "";
				}
				mBTService.connectToDevice(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				LogUtil.e("TOR", "onPostExecute  > ");
				if (isCancelled()) {
					LogUtil.e("TOR", "onPostExecute Async Task Cancelled  > ");
					return;
				}
				if (mBTService.isConnected()) {
					mEditPressure.setEnabled(false);
					mNskOne.setEnabled(false);
					mNskTwo.setEnabled(false);
					mNskThree.setEnabled(false);
					mEditPressure.setEnabled(false);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		createDialogOnBackPress();
	}

	/**
	 * initialize UI components
	 */
	private void initialize() {
		SharedPreferences preferences = getSharedPreferences(
				Constants.GOODYEAR_CONF, 0);
		if (preferences.getString(Constants.PRESSUREUNIT, "").equals(
				Constants.PSI_PRESSURE_UNIT)) {
			mIsSettingsPSI = true;
		}
		controlFromResult=0;
		mDbHelper = new DatabaseAdapter(this);
		mDesignArrayList = new ArrayList<String>();
		mFullDesignArrayList = new ArrayList<String>();
		mBrandArrayList = new ArrayList<String>();
		mSizeArrayList = new ArrayList<String>();
		mTyreTRIMArrayList = new ArrayList<String>();
		mTyreTASPArrayList = new ArrayList<String>();
		mDimensionLabel = (TextView) findViewById(R.id.lbl_dimenssion);
		mASPLabel = (TextView) findViewById(R.id.lbl_dimenssionASP);
		mRimLabel = (TextView) findViewById(R.id.lbl_dimenssionRIM);
		mDesignLabel = (TextView) findViewById(R.id.lbl_make);
		mSizeLabel = (TextView) findViewById(R.id.lbl_dimenssionSiz);
		mTypeLabel = (TextView) findViewById(R.id.lbl_type);
		mSerialNumberLabel = (TextView) findViewById(R.id.lbl_serialNo);
		mBrandLabel = (TextView) findViewById(R.id.lbl_brand);
		mBrand = (Spinner) findViewById(R.id.value_brand);
		mSize = (Spinner) findViewById(R.id.value_dimenssionSize);
		mASP = (Spinner) findViewById(R.id.value_dimenssionASP);
		mRim = (Spinner) findViewById(R.id.value_dimenssionRIM);
		mDesign = (Spinner) findViewById(R.id.value_design);
		mType = (Spinner) findViewById(R.id.value_type);
		mSerialNumber = (TextView) findViewById(R.id.value_serialNo);
		mWheelPosition = (TextView) findViewById(R.id.value_wp);
		mLblRegrooveType = (TextView) findViewById(R.id.lbl_reGrooved);
		mRGRegrooveType = (RadioGroup) findViewById(R.id.rg_regRooveType);
		mRegrooveTypeYes = (RadioButton) findViewById(R.id.value_regRooveYES);
		mRegrooveTypeNo = (RadioButton) findViewById(R.id.value_regRooveNO);

		mTemperature = (RadioGroup) findViewById(R.id.rg_Temperature);
		mTemperatureYes = (RadioButton) findViewById(R.id.value_TemperatureYES);
		mTemperatureNo = (RadioButton) findViewById(R.id.value_TemperatureNO);
		mTemperatureNo.setChecked(true);
		mNskOne = (EditText) findViewById(R.id.value_NSK1);

		mRecPressureLayout = (LinearLayout) findViewById(R.id.ll_pressure_rec);
		mRecValueAdjust = (Switch) findViewById(R.id.value_Adjust);
		//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
		mRecValueAdjust.setEnabled(false);

		mNskOne.setFilters(new InputFilter[]{new DecimalDigitsInputFilterNSK(
				3, 2)});
		mNskOne.addTextChangedListener(new nskOneChanged());
		mNskTwo = (EditText) findViewById(R.id.value_NSK2);
		mNskTwo.setFilters(new InputFilter[]{new DecimalDigitsInputFilterNSK(
				3, 2)});
		mNskTwo.addTextChangedListener(new nskTwoChanged());
		mNskThree = (EditText) findViewById(R.id.value_NSK3);
		mNskThree
				.setFilters(new InputFilter[]{new DecimalDigitsInputFilterNSK(
						3, 2)});
		mNskThree.addTextChangedListener(new nskThreeChanged());
		mMainScrollBar = (ScrollView) findViewById(R.id.parentnode);
		mLabelNSK = (TextView) findViewById(R.id.lbl_NSK);
		mLabelRimType = (TextView) findViewById(R.id.lbl_RimType);
		mJobDetailsLabel = (TextView) findViewById(R.id.lbl_jobDetails);
		// CR:451
		mCamera_icon = (ImageView) findViewById(R.id.camera_icon);

		mEditPressure = (EditText) findViewById(R.id.value_pressure);
		mEditRecPressureValue = (EditText) findViewById(R.id.value_pressure_rec);
		pressureUnit = (TextView) findViewById(R.id.lbl_pressureunit);
		recPressureUnit=(TextView) findViewById(R.id.lbl_pressureunit_rec);

		lblPressure = (TextView)findViewById( R.id.lbl_pressure );
		lblAdjust = (TextView)findViewById( R.id.lbl_Adjust );
		lblPressureRec = (TextView)findViewById( R.id.lbl_pressure_rec );
		lblTemperature = (TextView)findViewById( R.id.lbl_Temperature );


		//Rec Pressure Switch action
		recPressureSwitchInitializationAndAction();

		// update UI with tire data
		updateVauesToUIFromTire();
		// load labels
		loadLabelsFromDB();
		// set pressure to pressure field
		setValuetoPressueView();
	}
	/**
	 * Loading Labels for UI from the Translation table of the local database
	 * file
	 */
	private void loadLabelsFromDB() {
		DatabaseAdapter label = new DatabaseAdapter(this);
		label.createDatabase();
		label.open();
		mRegrove_blank_msg = label.getLabel(333);
		String activityName = label
				.getLabel(Constants.LABEL_INSPECTION);
		setTitle(activityName);

		String tireDetailsLabel = label.getLabel(Constants.LABEL_TIRE_DETAILS);
		((TextView) findViewById(R.id.lbl_tireDetails))
				.setText(tireDetailsLabel);

		mNotesLabel = label.getLabel(195);
		invalidateOptionsMenu();
		mYeslabel = label.getLabel(Constants.YES_LABEL);
		mNoLabel = label.getLabel(Constants.NO_LABEL);

		String labelFromDB = label.getLabel(Constants.DIMENSION_LABEL);
		mDimensionLabel.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.ASP_LABEL);
		mASPLabel.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.RIM_LABEL);
		mRimLabel.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.DESIGN_LABEL);
		mDesignLabel.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.SIZE_LABEL);
		mSizeLabel.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.TYPE_LABEL);
		mTypeLabel.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.NSK_LABEL);
		mLabelNSK.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.BRAND_LABEL);
		mBrandLabel.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.SERIAL_NUMBER);
		mSerialNumberLabel.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.REGROOVED_LABEL);
		mLblRegrooveType.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.YES_LABEL);
		mRegrooveTypeYes.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.NO_LABEL);
		mRegrooveTypeNo.setText(labelFromDB);
		mBackPressMessage = label.getLabel(Constants.CONFIRM_BACK);
		mDesignDetailRequired = label.getLabel(82);

		labelFromDB = label.getLabel(Constants.PRESSURE_LABEL);
		lblPressure.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.LABEL_ADJUST);
		lblAdjust.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.LABEL_RECTIFIED_PRESSURE);
		lblPressureRec.setText(labelFromDB);
		labelFromDB = label.getLabel(Constants.LABEL_TEMPERATURE);
		lblTemperature.setText(labelFromDB);

		mRecPressureRequired = label.getLabel(Constants.LABEL_RECTIFIED_PRESSURE_REQ);
		mTemperatureRequired = label.getLabel(Constants.LABEL_TEMPERATURE_REQ);
		mObservation=label.getLabel(Constants.LABEL_OBSERVATION);
		mAboutToInspectSelectedTyre=label.getLabel(Constants.LABEL_ABOUT_TO_INSPECT);
		mTemperatureNo.setText(label.getLabel(Constants.LABEL_TYRE_COLD));
		mTemperatureYes.setText(label.getLabel(Constants.LABEL_TYRE_HOT));

		pressureRequired = label.getLabel(Constants.PRESSURE_REQUIRED);


		// strbluetoothconnectionfalied =
		// mDbHelper.getLabel(Constants.BLUETOOTH_CONNECTION_FAILED);
		strTOR_NSK_VALUES_INTERCHANGED_MSG = label
				.getLabel(Constants.TOR_NSK_VALUES_INTERCHANGED_MSG);

		labelFromDB = label.getLabel(Constants.JOB_DETAILS);
		mJobDetailsLabel.setText(labelFromDB);
		mPleaseSelectBrand = label.getLabel(Constants.SELECT_BRAND);
		labelFromDB = label.getLabel(Constants.PLEASE_SELECT_LABEL);
		mDefaultValueForSpinner = labelFromDB;
		mTORConfirmationMessage = label.getLabel(Constants.TOR_FINAL_CONFIRMATION_LABEL);
		mTORConfirmationMessage = mTORConfirmationMessage.replaceAll(Constants.DOTNET_STRING_PLACE_HOLDER_REGEX,
				Constants.JAVA_STRING_PLACE_HOLDER);
		label.close();
		// add listners after change

		mRGRegrooveType.setOnCheckedChangeListener(new RadioChangeHandler());

		mTemperature.setOnCheckedChangeListener(new RadioChangeHandler());
	}

	/**
	 * rec Pressure Enable/Disable Switch Control and OnCheckedChangeListener
	 */
	private void recPressureSwitchInitializationAndAction()
	{
		mRecValueAdjust.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

				//validating pressure value and enabling the adjust switch based on pressure value
				if (!CommonUtils
						.pressureValueValidation(mEditPressure)) {
					CommonUtils.notify(
							Constants.sLblCheckPressureValue,
							getBaseContext());
					//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
					mRecValueAdjust.setEnabled(false);
				}
				else
				{
					setVisibilityToRecPressure(isChecked);

					//To Enable and Disable Pressure mEditPressure based on rec Pressure Switch when user first time enters pressure value
					if(mRecommendedpressure<=0)
					{
						mEditPressure.setEnabled(!isChecked);
					}
				}


			}
		});

		//psi and bar validation for rectified pressure
		if (mIsSettingsPSI) {
			mEditRecPressureValue
					.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
							4, 2) });
		} else {
			mEditRecPressureValue
					.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
							3, 2) });
		}
		//Fix : for adjust button
		/*Bug 728 : Adding context to show toast for higher pressure value*/
		mEditRecPressureValue.addTextChangedListener(new PressureCheckListener(
				mEditRecPressureValue, mIsSettingsPSI, getApplicationContext()));
	}

	/**
	 * Method to Enable or Disable Visibility of Rec Pressure Layout
	 * @param isChecked
	 */
	private void setVisibilityToRecPressure(Boolean isChecked)
	{
		if (isChecked) {
			mEditRecPressureValue.setText("");
			mRecPressureLayout.setVisibility(View.VISIBLE);
		} else {
			//add empty value when switched is off
			mEditRecPressureValue.setText("");
			mRecPressureLayout.setVisibility(View.GONE);
		}
	}

	/**
	 * OnCheckedChangeListener class
	 */
	private class SwitchChangeHandler implements OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
									 boolean isChecked) {
			mChangeMade = true;
		}
	}

	/**
	 * OnCheckedChangeListener class
	 */
	private class RadioChangeHandler implements
			RadioGroup.OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			mChangeMade = true;
		}
	}
	/**
	 * Method loading and setting the full tire details for the corresponding
	 * selected tire
	 */
	private void loadTyreDetails() {
		loadSelectedBrand();
		loadSelectedSize();
		loadSelectedASP();
		loadSelectedRIM();
		loadSelectedDesign();
		loadSelectedDesignDetails();
	}
	/**
	 * Method loading and setting the size details from same axle for the
	 * corresponding selected tire
	 */
	private void loadSizeDetailsFromAxle() {
		loadSelectedSize();
		loadSelectedASP();
		loadSelectedRIM();
	}

	/**
	 * update the UI with tire data
	 */
	private void updateVauesToUIFromTire() {
		if (mSelectedTire == null) {
			return;
		}
		mSelectedBrandName = mSelectedTire.getBrandName();
		mSelectedtyreSize = mSelectedTire.getSize();
		mSelectedTyreTASP = mSelectedTire.getAsp();
		mSelectedtyreTRIM = mSelectedTire.getRim();
		mSelectedTyreDesign = mSelectedTire.getDesign();
		mSelectedTyreDetailedDesign = mSelectedTire.getDesignDetails();
		mSerialNumber.setText(mSelectedTire.getSerialNumber());
		mWheelPosition.setText(mSelectedTire.getPosition());
		// get nsk values
		mNskOne.setText(mSelectedTire.getNsk());
		mNskTwo.setText(mSelectedTire.getNsk2());
		mNskThree.setText(mSelectedTire.getNsk3());
		mChangeMade = false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inspection_munu, menu);
		if (mNotesLabel != null) {
			menu.findItem(R.id.action_note).setTitle(mNotesLabel);
		}
		menu.findItem(R.id.action_observation).setTitle(mObservation);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		//pressure box state is maintained before moving to observation and notes.
		Constants.PRESSURE_EDIT_STATE = mEditPressure.isEnabled();
		int id = item.getItemId();
		if (id == R.id.action_note) {
			LogUtil.TraceInfo(TRACE_TAG, "Option", "Note", false, true, false);
			Intent startNotesActivity = new Intent(this, NoteActivity.class);
			startNotesActivity.putExtra("sentfrom", "inspection");
			startNotesActivity.putExtra("lastnote", sNoteText);
			startActivityForResult(startNotesActivity, NOTES_INTENT);
		}else if(id == R.id.action_observation){
			LogUtil.TraceInfo(TRACE_TAG, "Option", "Observation", false, true, false);
			if(Constants.mGlobalVisualRemarks!=null)
			{
				LogUtil.DBLog(TAG,"onOptionsItemSelected","Visual Remarks " + Constants.mGlobalVisualRemarks.toString());
			}

			Intent startVisualRemarksActivity = new Intent(this, VisualRemarksActivity.class);
			startVisualRemarksActivity.putExtra("sentfrom", "inspection");
			startActivityForResult(startVisualRemarksActivity,OBSERVATION_INTENT);



		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		//retaining pressure edit box state from note or observation activity result
		mEditPressure.setEnabled(Constants.PRESSURE_EDIT_STATE);
		controlFromResult=1;
		if (requestCode == NOTES_INTENT) {
			if (data != null) {
				sNoteText = data.getExtras().getString("mNoteEditText");

			}
		}
		else if(requestCode ==OBSERVATION_INTENT)
		{

		}
	}

	/**
	 * NskOne TextWatcher
	 *
	 */
	private class nskOneChanged implements TextWatcher {
		@Override
		public void afterTextChanged(Editable nsk_One) {
			mChangeMade = true;
			mNskTwo.setText(nsk_One.toString());
			mNskThree.setText(nsk_One.toString());
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
									  int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
								  int count) {
			String strEnteredVal = mNskOne.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {

				} else {
					mNskOne.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mNskOne.setText("");
			}

		}
	}

	/**
	 * NskTwo TextWatcher
	 *
	 */
	private class nskTwoChanged implements TextWatcher {
		@Override
		public void afterTextChanged(Editable nsk_One) {
			mChangeMade = true;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
									  int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
								  int count) {
			String strEnteredVal = mNskTwo.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {

				} else {
					mNskTwo.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mNskTwo.setText("");
			}

		}
	}

	/**
	 * NskThree TextWatcher
	 *
	 */
	private class nskThreeChanged implements TextWatcher {
		@Override
		public void afterTextChanged(Editable nsk_One) {
			mChangeMade = true;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
									  int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
								  int count) {
			String strEnteredVal = mNskThree.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {

				} else {
					mNskThree.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mNskThree.setText("");
			}
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
								 Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_turn_on_rim,
					container, false);
			return rootView;
		}
	}

	/**
	 * If axle have pressure apply the value then disable otherwise it's
	 * editable Setting the Pressure
	 */
	private void setValuetoPressueView() {
		if (mRecommendedpressure > 0) {
			mEditPressure.setText(CommonUtils.getPressureValue(InspectionActivity.this,
					mRecommendedpressure));
			pressureUnit.setText(CommonUtils.getPressureUnit(InspectionActivity.this));
			//setting unit for Rec pressure
			recPressureUnit.setText(CommonUtils.getPressureUnit(InspectionActivity.this));
			mEditPressure.setEnabled(false);
			mBlockPressureFromBluetooth = true;
		} else {
			if (mIsSettingsPSI) {
				mEditPressure
						.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
								4, 2) });
			} else {
				mEditPressure
						.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
								3, 2) });
			}
			/*Bug 728 : Adding context to show toast for higher pressure value*/
			mEditPressure.addTextChangedListener(new PressureCheckListener(
					mEditPressure, mIsSettingsPSI,mRecValueAdjust, getApplicationContext()));


			pressureUnit.setText(CommonUtils.getPressureUnit(InspectionActivity.this));
			//setting unit for Rec pressure
			recPressureUnit.setText(CommonUtils.getPressureUnit(InspectionActivity.this));

		}
	}
	/**
	 * @author amitkumar.h
	 * Class providing the animation when user swipes out
	 * from the activity It also handles the actions performed when use
	 * is swiping out from the activity after matching all the required
	 * conditions
	 */
	public class Swiper implements OnTouchListener, OnClickListener {
		float startX, startY;
		float endX, endY;
		int selectedPositionToDelete;
		ArrayAdapter<String> adapterList;
		ScrollView view;
		private Context ctx;
		public static final float MINIMUM_MOVEMENT_REQUIRED = 100;

		public Swiper(Context ctx, ScrollView view) {
			this.ctx = ctx;
			this.view = view;
			view.setOnClickListener(this);
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getActionMasked()) {
				case MotionEvent.ACTION_DOWN:
					mSwiped = false;
					startX = event.getX();
					startY = event.getY();
					break;
				case MotionEvent.ACTION_UP:
					break;

				case MotionEvent.ACTION_MOVE:
					endX = event.getX();
					endY = event.getY();
					if (Math.abs(endX - startX) > MINIMUM_MOVEMENT_REQUIRED
							&& !mSwiped) {
						mSwiped = true;
						Rect rect = new Rect();
						int childCount = view.getChildCount();
						int[] listViewCoords = new int[2];
						view.getLocationOnScreen(listViewCoords);
						int x = (int) event.getRawX() - listViewCoords[0];
						int y = (int) event.getRawY() - listViewCoords[1];
						View child;
						for (int i = 0; i < childCount; i++) {
							child = view.getChildAt(i);
							child.getHitRect(rect);
							if (rect.contains(x, y)) {
							/*
							 * if(mDesign.getSelectedItemPosition() < 0) {
							 * CommonUtils.notify(mDesignDetailRequired,
							 * TurnOnRim.this); } else
							 */

								//based on blockBrandAndTyreDetails - spinner validation
								if ((mType.getSelectedItemPosition() < 0
										|| mType.getSelectedItem().toString()
										.equals(mDefaultValueForSpinner)) && blockBrandAndTyreDetails==0)
								{
									if(mDesignDetailRequired!=null)
									{
										LogUtil.TraceInfo(TRACE_TAG, "Design Validation","Msg : " +mDesignDetailRequired,false,false,false);
									}
									else
									{
										LogUtil.TraceInfo(TRACE_TAG, "Design Validation","Msg : Check Design",false,false,false);
									}
									CommonUtils.notify(mDesignDetailRequired,
											InspectionActivity.this);
								} else if (!CommonUtils
										.validateNSKValues(mNskOne.getText()
												.toString(), mNskTwo.getText()
												.toString(), mNskThree.getText()
												.toString())) {
									if(Constants.sLblEnterNskValues!=null)
									{
										LogUtil.TraceInfo(TRACE_TAG, "NSK Validation","Msg : " +Constants.sLblEnterNskValues,false,false,false);
									}
									else
									{
										LogUtil.TraceInfo(TRACE_TAG, "NSK Validation","Msg : Check NSK",false,false,false);
									}
									CommonUtils.notify(
											Constants.sLblEnterNskValues,
											getBaseContext());
								}

								//Commented : pressure value in Inspection is not mandatory
//								else if (mEditPressure.getText().toString()
//										.equals("")
//										|| mEditPressure.getText().toString()
//										.equals(" ")) {
//									CommonUtils.notify(pressureRequired,
//											getBaseContext());
//								} else if (!CommonUtils
//										.pressureValueValidation(mEditPressure)) {
//									CommonUtils.notify(
//											Constants.sLblCheckPressureValue,
//											getBaseContext());
//								}

								//pressure validation
								else if (!(mEditPressure.getText().toString()
										.equals("")
										|| mEditPressure.getText().toString()
										.equals(" ")) && !CommonUtils
										.pressureValueValidation(mEditPressure)) {
									if(Constants.sLblCheckPressureValue!=null)
									{
										LogUtil.TraceInfo(TRACE_TAG, "Pressure Value Validation","Msg : " +Constants.sLblCheckPressureValue,false,false,false);
									}
									else
									{
										LogUtil.TraceInfo(TRACE_TAG, "Pressure Value Validation","Msg : Check NSK",false,false,false);
									}
										CommonUtils.notify(
												Constants.sLblCheckPressureValue,
												getBaseContext());

								}

								//validation for rectified pressure
								//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
								else if(mRecValueAdjust.isEnabled() && mRecValueAdjust.isChecked() && !CommonUtils
										.pressureValueValidation(mEditRecPressureValue))
								{

										LogUtil.TraceInfo(TRACE_TAG, "Pressure Value Validation","Msg : Rec Pressure is required",false,false,false);

									CommonUtils.notify(mRecPressureRequired,
											getApplicationContext());
								}

								else if (-1 == mRGRegrooveType
										.getCheckedRadioButtonId()) {

									if(mRegrove_blank_msg!=null)
									{
										LogUtil.TraceInfo(TRACE_TAG, "Regroove Type Validation","Msg : " +mRegrove_blank_msg,false,false,false);
									}
									else
									{
										LogUtil.TraceInfo(TRACE_TAG, "Regroove Type Validation","Msg : Check Regroove Type",false,false,false);
									}

									CommonUtils.notify(mRegrove_blank_msg,
											getApplicationContext());
								}
								else if (-1 == mTemperature
										.getCheckedRadioButtonId()) {
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Value Validation","Msg : Temperature is required",false,false,false);
									CommonUtils.notify(mTemperatureRequired,
											getApplicationContext());
								}


								else {
									showConformDialog();

								}
							}
						}
					}
					break;
			}
			return false;
		}

		@Override
		public void onClick(View arg0) {
		}
	}

	private void showConformDialog() {
		LayoutInflater li = LayoutInflater.from(InspectionActivity.this);
		View promptsView = li.inflate(R.layout.dialog_common_informational,
				null);
		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
				InspectionActivity.this);
		alertDialogBuilder.setView(promptsView);
		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		// Setting The Info
		TextView infoTv = (TextView) promptsView
				.findViewById(R.id.textViewTitle);

		String mMessage = mAboutToInspectSelectedTyre;

		//If serial number is not there different message will be displayed
		if(TextUtils.isEmpty(Constants.SELECTED_TYRE.getSerialNumber())
				|| Constants.SELECTED_TYRE.getSerialNumber().equalsIgnoreCase("N/R"))
		{
			mMessage = mAboutToInspectSelectedTyre;
		}
		mMessage = mMessage + " "+ mSelectedTire.getSerialNumber();

		//Directly accessing value from Selected Tire Object
//		if (mSelectedTire.getSerialNumber().equals(
//				Constants.EDITED_SERIAL_NUMBER)) {
//			mMessage = String.format(mMessage, " "+ mSelectedTire.getSerialNumber());
//		} else {
//			mMessage = String.format(mMessage, " "+ Constants.EDITED_SERIAL_NUMBER);
//		}


		infoTv.setText(mMessage);
		alertDialogBuilder.setCancelable(false).setPositiveButton(mYeslabel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();

						if (dialog != null) {
							dialog.dismiss();
							dialog = null;
						}
						new PerformanceBaseModel(InspectionActivity.this, callback,
								false).perform();
						// onReturnToSkeleton();
					}
				});

		alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();

						dialog.cancel();
					}
				});
		// create alert dialog
		mConformDialog = alertDialogBuilder.create();
		// show it
		mConformDialog.show();
		mConformDialog.setCanceledOnTouchOutside(false);
	}
	/**
	 * Method Getting Tire SAPMaterialCodeID from local DB3 file as per the
	 * selected DetailedDesign
	 */
	public void getSAPMaterialCodeID() {
		Cursor mCursor = null;
		try {
			if (null == mDbHelper) {
				return;
			}
			mDbHelper.open();
			mCursor = mDbHelper.getSAPMaterialCodeID(mSelectedtyreSize,
					mSelectedTyreTASP, mSelectedtyreTRIM, mSelectedTyreDesign,
					mSelectedTyreDetailedDesign, mSelectedBrandName);
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				mSelectedSAPMaterialCodeID = mCursor.getString(
						mCursor.getColumnIndex("ID")).toString();
			}
			CursorUtils.closeCursor(mCursor);
			if (Constants.onBrandBool == true
					|| Constants.onBrandBoolForJOC == true) {
				Constants.NEW_SAPMATERIAL_CODE = mSelectedSAPMaterialCodeID;
				VehicleSkeletonFragment.updateCorrectedBrandInDB();
			}

			//Fix for Serial number updation in Job Correction
			if(TextUtils.isEmpty(Constants.SELECTED_TYRE.getSerialNumber()))
			{
				VehicleSkeletonFragment.updateCorrectedSerialNumberInDB();
			}
//			if (!Constants.SELECTED_TYRE.getSerialNumber().equalsIgnoreCase(
//					Constants.EDITED_SERIAL_NUMBER)) {
//				VehicleSkeletonFragment.updateCorrectedSerialNumberInDB();
//			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Updating recommended axle pressure
	 */
	private void updatePressureValue() {
		boolean isPressureEnabled = mEditPressure.isEnabled();
		if ((isPressureEnabled || !mBlockPressureFromBluetooth)
				&& (null != VehicleSkeletonFragment.mAxleRecommendedPressure)) {
			VehicleSkeletonFragment.mAxleRecommendedPressure.set(axlePosition,
					CommonUtils.parseFloat(mFinalConvertedPressure));
		}
	}
	/**
	 * Method handling the data-updates for the tables(JobItem, tire,
	 * JobCorrection etc) for the selected tire. It checks all the mandatory
	 * fields then allows user to navigate back to Vehicle Skeleton after
	 * updating the data
	 */
	private void onReturnToSkeleton() {

		mTurnOnRimInfo = mSelectedTire.getTorInfo();
		Constants.SELECTED_TYRE.setIsInspected(true);
		// set NSK value
		mTurnOnRimInfo.setNskOne(mNskOne.getText().toString());// nsk1 to nsk3
		mTurnOnRimInfo.setNskTwo(mNskTwo.getText().toString());
		mTurnOnRimInfo.setNskThree(mNskThree.getText().toString());// nsk3 to nsk1

		// mTurnOnRimInfo.setRegrooved(mFlag);
		mTurnOnRimInfo.setRegrooved(mRegrooveTypeYes.isChecked());


		mFinalConvertedPressure = CommonUtils.getFinalConvertedPressureValue(
				mRecommendedpressure, mEditPressure.getText().toString(),
				mIsSettingsPSI);

		//updatePressureValue();

		// update selected tire nsk and pressure
		// set return code
		//based on blockBrandAndTyreDetails , block or unblock getting Sap material code
		if(blockBrandAndTyreDetails==0) {
			getSAPMaterialCodeID();
		}
		updateJobItem();
		// close keyboard if open
		try {
			CommonUtils.hideKeyboard(this, getWindow().getDecorView()
					.getRootView().getWindowToken());
		} catch (Exception e) {
			LogUtil.i("Mount PWT on activity return",
					"could not close keyboard");
		}

		Constants.SELECTED_TYRE.setNsk(mNskOne.getText().toString());
		Constants.SELECTED_TYRE.setNsk2(mNskTwo.getText().toString());
		Constants.SELECTED_TYRE.setNsk3(mNskThree.getText().toString());

		// based on blockBrandAndTyreDetails, block or unblock setting value to selected tyre
		if(blockBrandAndTyreDetails==0) {
			Constants.SELECTED_TYRE.setBrandName(mBrand.getSelectedItem()
					.toString());
			Constants.SELECTED_TYRE.setSize(mSize.getSelectedItem().toString());
			Constants.SELECTED_TYRE.setAsp(mASP.getSelectedItem().toString());
			Constants.SELECTED_TYRE.setRim(mRim.getSelectedItem().toString());
			Constants.SELECTED_TYRE.setDesign(mDesign.getSelectedItem().toString());
			Constants.SELECTED_TYRE.setDesignDetails(mType.getSelectedItem()
					.toString());
			Constants.SELECTED_TYRE
					.setSapMaterialCodeID(mSelectedSAPMaterialCodeID);
		}

		//SETTING PRESSURE BASED ON Recorded pressure and rectified pressure value
		if(!(convertedRecPressureValue.equalsIgnoreCase("0") || convertedRecPressureValue.equalsIgnoreCase("0.0")|| convertedRecPressureValue.equalsIgnoreCase("0.00"))) {
			Constants.SELECTED_TYRE.setPressure(convertedRecPressureValue);
		}
		else
		{
			Constants.SELECTED_TYRE.setPressure(mFinalConvertedPressure);
		}
		// based on blockBrandAndTyreDetails, block or unblock setting tyre serial number
		if(blockBrandAndTyreDetails==0) {
			//Fix for Non maintained tyres
			if (TextUtils.isEmpty(Constants.SELECTED_TYRE.getSerialNumber())) {
				Constants.SELECTED_TYRE
						.setSerialNumber(Constants.EDITED_SERIAL_NUMBER);
			}
		}
		// set Turn on Rim object for tire
		mSelectedTire.setTorInfo(mTurnOnRimInfo);

		//User Trace logs
		try {
			String traceData;
			traceData =	"\n\t\tJob Details : " + Constants.SELECTED_TYRE.getNsk() +
					", " + Constants.SELECTED_TYRE.getNsk2() +
					", " + Constants.SELECTED_TYRE.getNsk3() +
					"| " + Constants.SELECTED_TYRE.getPressure()+
					"| " + convertedRecPressureValue;

			if (mRegrooveTypeYes.isChecked())
			{
				traceData = traceData+	"| Regrooved" ;
			}
			else
			{
				traceData = traceData+	"| Not Regrooved";
			}

			if(mTemperatureYes.isChecked())
			{
				traceData = traceData+	"| Hot" ;
			}
			else
			{
				traceData = traceData+	"| Cold";
			}

			LogUtil.TraceInfo(TRACE_TAG, "none","Data : " + traceData, false, false, false);
		}
		catch (Exception e)
		{
			LogUtil.TraceInfo(TRACE_TAG, "Data : Exception : ", e.getMessage(), false, true, false);
		}
	}

	/**
	 * Method Updating Data for the selected tire in JobItem table with
	 * the corresponding TurnOnRim ActionType
	 */
	private void updateJobItem() {
		external_id = String.valueOf(UUID.randomUUID());
		if (null != mDbHelper) {
			mDbHelper.open();
		}
		int len = VehicleSkeletonFragment.mJobItemList.size();

		int jobID = getJobItemIDForThisJobItem();
		JobItem jobItem = new JobItem();
		jobItem.setActionType("26"); // 4 for re-groove
		jobItem.setAxleNumber("0"); // for axle service
		jobItem.setAxleServiceType("0"); // for axle service
		jobItem.setCasingRouteCode(""); // for dismount tire
		jobItem.setDamageId("");
		jobItem.setExternalId(external_id);//
		jobItem.setGrooveNumber(null);
		jobItem.setJobId(String.valueOf(jobID));
		jobItem.setMinNSK(minNSKSet());
		jobItem.setNote(sNoteText);
		// FIX 294 :: For TOR NSK1 and NSK3 will swap, so settings NSK3 to nsk1
		// , and NSK1 to NSK3
		jobItem.setNskOneAfter(mNskThree.getText().toString());
		jobItem.setNskOneBefore("0");
		jobItem.setNskThreeAfter(mNskOne.getText().toString());
		jobItem.setNskThreeBefore("0");
		jobItem.setNskTwoAfter(mNskTwo.getText().toString());
		jobItem.setNskTwoBefore("0");
		jobItem.setOperationID("");
		/**
		 * Condition checking the value of Rect Pressure and setting it back to jobItem object
		 */
		if(!TextUtils.isEmpty(mEditRecPressureValue.getText().toString()) && mEditRecPressureValue.getText().toString().length()>0) {
			//setting converted Rect pressure value
			convertedRecPressureValue=CommonUtils.getFinalConvertedPressureValue(
					0, mEditRecPressureValue.getText().toString(), mIsSettingsPSI);
			jobItem.setRectPressure(convertedRecPressureValue);
		}
		else
		{
			convertedRecPressureValue="0";
			//Setting Default Value as 99.99 if rec pressure is empty
			jobItem.setRectPressure(Constants.DEFAULT_INSPECTION_PRESSURE_VALUE);
		}
		if (mRecommendedpressure != 0.0) {
			jobItem.setPressure(String.valueOf(mRecommendedpressure));
			// recommended pressure for axle
			jobItem.setRecInflactionDestination(String
					.valueOf(mRecommendedpressure));
		} else {

			//Setting Default Value as 99.99 if pressure is empty
			if(!TextUtils.isEmpty(mEditPressure.getText().toString()) && mEditPressure.getText().toString().length()>0) {
				jobItem.setPressure(mFinalConvertedPressure);

			}
			else
			{
				jobItem.setPressure(Constants.DEFAULT_INSPECTION_PRESSURE_VALUE);
			}
//			//if no pressure provided (xle pressure is applied)
//			if(mFinalConvertedPressure.equalsIgnoreCase("0") ||mFinalConvertedPressure.equalsIgnoreCase("0.0") ||mFinalConvertedPressure.equalsIgnoreCase("0.00") )
//			{
//				jobItem.setPressure(String.valueOf(mInitialpressure));
//			}

			// recommended pressure for axle
			jobItem.setRecInflactionDestination(String
					.valueOf(mRecommendedpressure));
		}
		// pressure from JSON
		jobItem.setRecInflactionOrignal(Constants.SELECTED_TYRE.getPressure());
		// if user selected regrooved then set Regrooved to true otherwise false
		if (mRegrooveTypeYes.isChecked()) {
			jobItem.setRegrooved("True");
		} else {
			jobItem.setRegrooved("False");
		}
		/**
		 * Condition checking the value of Temperature and setting it back to jobItem object
		 */
		if(mTemperatureYes.isChecked())
		{
			jobItem.setTemperature("1");
		}
		else
		{
			jobItem.setTemperature("0");
		}
		jobItem.setReGrooveNumber(null);// alway null
		jobItem.setRegrooveType("0");
		jobItem.setRemovalReasonId("");
		jobItem.setRepairCompany(null);

		jobItem.setSapCodeTilD("0");
		jobItem.setSequence(String.valueOf(len + 1));
		jobItem.setServiceCount("0");// addtinal servcies
		jobItem.setServiceID("0");// axle additional
		jobItem.setSwapType("0");// swap
		jobItem.setThreaddepth("0");// tire mgmt
		jobItem.setTorqueSettings("0"); // update from retorque
		jobItem.setTyreID(mSelectedTire.getExternalID());
		/**
		 * Setting RimType value to 0 by default as recommended by Michael
		 */
		jobItem.setRimType("0");
		jobItem.setWorkOrderNumber(null);
        Constants.SELECTED_TYRE.setExternalID(mSelectedTire.getExternalID());
        jobItem.setThreaddepth("0");
        jobItem.setSwapOriginalPosition(null);
        String mVisualDisplayId;
		/**
		 * Condition checking the value of Visual Display ID and setting it back to jobItem object
		 */
        if (Constants.mGlobalVisualRemarks.size()>0){
            int count=0;
            mVisualDisplayId="";
            do {
                mVisualDisplayId=mVisualDisplayId+Constants.mGlobalVisualRemarks.get(count).getVRId().toString();
                if(Constants.mGlobalVisualRemarks.size()-1>count)
                {
                    mVisualDisplayId=mVisualDisplayId+",";
                }
				count++;
            }while(Constants.mGlobalVisualRemarks.size()>count);

            jobItem.setVisualDisplayID(mVisualDisplayId);

        }else {
            jobItem.setVisualDisplayID("");
        }

        // Fix:: Edited serial no was not coming, so added serial no here. on
        // 15-11
		// based on blockBrandAndTyreDetails, block or unblock setting tyre serial number to jobitem
		if(blockBrandAndTyreDetails==0) {
			if (TextUtils.isEmpty(Constants.SELECTED_TYRE.getSerialNumber())) {
				jobItem.setSerialNumber(Constants.EDITED_SERIAL_NUMBER);
			}
			else
			{
				jobItem.setSerialNumber(Constants.SELECTED_TYRE.getSerialNumber());
			}
			jobItem.setBrandName(mBrand.getSelectedItem().toString());
			jobItem.setFullDesignDetails(mType.getSelectedItem().toString());
		}
		// Till Here

		try {
			LogUtil.DBLog(TAG,"updateJobItem", "\nJobItem : " + jobItem.toString()+"\n");
		}
		catch (Exception e)
		{
			LogUtil.DBLog(TAG, "updateJobItem", "Exception : " + e.getMessage());
			LogUtil.i(TAG,"updateJobItem"+ e.getMessage());
		}

		VehicleSkeletonFragment.mJobItemList.add(jobItem);
		//Updating the flag to true when inspection is done
		Constants.HAS_INSPECTED_TIRE = true;
		sNoteText = "";
	}


	/**
	 * Method returning maximum count of Job present in the JobItem table
	 * @return: Max Job Count
	 */
	private int getJobItemIDForThisJobItem() {
		int countJobItemsNotPresent = 0;
		mDbHelper.open();
		int currentJobItemCountInDB = mDbHelper.getJobItemCount();
		Cursor cursor = null;
		try {
			cursor = mDbHelper.getJobItemValues();
			if (!CursorUtils.isValidCursor(cursor)) {
				return countJobItemsNotPresent + 1
						+ VehicleSkeletonFragment.mJobItemList.size();
			}
			for (JobItem jobItem : VehicleSkeletonFragment.mJobItemList) {
				boolean flag = false;
				for (boolean hasItem = cursor.moveToFirst(); hasItem; hasItem = cursor
						.moveToNext()) {
					if (jobItem.getExternalId().equals(
							cursor.getString(cursor
									.getColumnIndex("ExternalID")))) {
						countJobItemsNotPresent++;
						break;
					}
				}
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			mDbHelper.close();
			// CursorUtils.closeCursor(cursor);
		}
		return currentJobItemCountInDB - countJobItemsNotPresent
				+ VehicleSkeletonFragment.mJobItemList.size() + 1;
	}

	/**
	 * Findout Minimum NSK value
	 */
	private String minNSKSet() {
		float nsk_One = 0;
		float nsk_Two = 0;
		float nsk_Three = 0;

		if (null != mNskOne) {
			nsk_One = CommonUtils.parseFloat(mNskOne.getText().toString());
		}
		if (null != mNskTwo) {
			nsk_Two = CommonUtils.parseFloat(mNskTwo.getText().toString());
		}
		if (null != mNskThree) {
			nsk_Three = CommonUtils.parseFloat(mNskThree.getText().toString());
		}

		if (nsk_One < nsk_Two && nsk_One < nsk_Three) {
			return String.valueOf(nsk_One);
		} else if (nsk_Two < nsk_One && nsk_Two < nsk_Three) {
			return String.valueOf(nsk_Two);
		} else {
			return String.valueOf(nsk_Three);
		}
	}

	private AlertDialog mBackAlertDialog, mConformDialog;
	/**
	 * Method Capturing the dialog state before orientation change
	 */
	private void saveDialogState(Bundle state) {
		state.putBoolean("mBackAlertDialog",
				(mBackAlertDialog != null && mBackAlertDialog.isShowing()));
		state.putBoolean("mConformDialog",
				(mConformDialog != null && mConformDialog.isShowing()));
	}
	/**
	 * Method Capturing the dialog state after orientation change
	 */
	private void restoreDialogState(Bundle state) {


		if (state != null) {

			//Restore Rec Pressure Switch State
			//Fix : "Adjust" switch button is not functioning in 10inch tablets, [without entering pressure value able to switch button]
			setVisibilityToRecPressure(state.getBoolean("VisibilityToRecPressure"));
			mEditPressure.setEnabled(state.getBoolean("pressure_enabled"));
			if (state.getBoolean("mBackAlertDialog")) {
				createDialogOnBackPress();
			} else if (state.getBoolean("mConformDialog")) {
				showConformDialog();
			}
		}
	}

	@Override
	protected void onDestroy() {
		try {

			if(mDbHelper != null){
				mDbHelper.close();
			}
			if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
				mBackAlertDialog.dismiss();
			}
			Constants.ONSTATE_INSPECTION = false;
			Constants.EDITED_SERIAL_NUMBER = "";
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}
	/**
	 * Method handling the action performed where user presses the back button
	 * Alert Box created with two option: YES and NO
	 */
	private void createDialogOnBackPress() {
		if (mChangeMade) {
			sNoteText = "";
			LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", false, true, false);
			LayoutInflater li = LayoutInflater.from(InspectionActivity.this);
			View promptsView = li.inflate(R.layout.dialog_logout_confirmation,
					null);
			final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
					InspectionActivity.this);
			alertDialogBuilder.setView(promptsView);
			// update user activity for dialog layout
			LinearLayout rootNode = (LinearLayout) promptsView
					.findViewById(R.id.layout_root);
			rootNode.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					InactivityUtils.updateActivityOfUser();
				}
			});

			// Setting The Info
			TextView infoTv = (TextView) promptsView
					.findViewById(R.id.logout_msg);
			infoTv.setText(mBackPressMessage);
			alertDialogBuilder.setCancelable(false).setPositiveButton(
					mYeslabel, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							// update user activity on button click
							alertDialogBuilder.updateInactivityForDialog();
							LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
							Intent intentReturn = new Intent();
							intentReturn.putExtra("tor", false);
							setResult(VehicleSkeletonFragment.REGROOVE_INTENT,
									intentReturn);
							Constants.JOB_CORRECTION_LIST = VehicleSkeletonFragment.mJobcorrectionList;
							CameraUtility.resetParams();
							Constants.onBrandBool = false;
							Constants.ONSTATE_INSPECTION = false;
							Constants.EDITED_SERIAL_NUMBER = "";
							finish();
						}
					});
			alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							// update user activity on button click
							alertDialogBuilder.updateInactivityForDialog();
							LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);
							// if user select "No", just cancel this dialog and
							// continue
							// with app
							dialog.cancel();
						}
					});
			// create alert dialog
			mBackAlertDialog = alertDialogBuilder.create();
			// show it
			mBackAlertDialog.show();
			mBackAlertDialog.setCanceledOnTouchOutside(false);
		} else {
			sNoteText = "";
			Constants.JOB_CORRECTION_LIST = VehicleSkeletonFragment.mJobcorrectionList;
			Constants.onBrandBool = false;
			CameraUtility.resetParams();
			finish();
		}
	}
	/**
	 * Method loading Brand Name on UI element as per the selected tire
	 */
	private void loadSelectedBrand() {
		mBrandArrayList.clear();
		mBrandArrayList.add(mSelectedTire.getBrandName());
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mBrandArrayList);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mBrand.setAdapter(dataAdapter);
		mBrand.setEnabled(false);
	}
	/**
	 * Method loading tire-size on UI element as per the selected tire
	 */
	private void loadSelectedSize() {
		mSizeArrayList.clear();
		mSizeArrayList.add(mSelectedtyreSize);
		mSizeDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mSizeArrayList);
		mSizeDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSize.setAdapter(mSizeDataAdapter);
		if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED) {
			mSize.setEnabled(true);
		}else{
			mSize.setEnabled(false);
		}
	}
	/**
	 * Method loading tire-ASP on UI element as per the selected tire
	 */
	private void loadSelectedASP() {
		mTyreTASPArrayList.clear();
		mTyreTASPArrayList.add(mSelectedTyreTASP);
		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mASP.setAdapter(mASPDataAdapter);
		if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED) {
			mASP.setEnabled(true);
		}else{
			mASP.setEnabled(false);
		}
	}
	/**
	 * Method loading tire-RIM on UI element as per the selected tire
	 */
	private void loadSelectedRIM() {
		mTyreTRIMArrayList.clear();
		mTyreTRIMArrayList.add(mSelectedtyreTRIM);
		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mRim.setAdapter(mRimDataAdapter);
		if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED) {
			mRim.setEnabled(true);
		}else{
			mRim.setEnabled(false);
		}
	}
	/**
	 * Method loading tire-Design on UI element as per the selected tire
	 */
	private void loadSelectedDesign() {
		mDesignArrayList.clear();
		mDesignArrayList.add(mSelectedTire.getDesign());
		mDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mDesignArrayList);
		mDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mDesign.setAdapter(mDesignDataAdapter);
		mDesign.setEnabled(false);
	}
	/**
	 * Method loading tire-Design-Details on UI element as per the selected tire
	 */
	private void loadSelectedDesignDetails() {
		mFullDesignArrayList.clear();
		mFullDesignArrayList.add(mSelectedTire.getDesignDetails());
		mFullDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mFullDesignArrayList);
		mFullDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mType.setAdapter(mFullDesignDataAdapter);
		mType.setEnabled(false);
	}
	/**
	 * Method loading tire BrandName from the local DB3 file and populating on
	 * UI element as per the selected tire
	 */
	private void loadTyreDetailsOnBrandCorrection() {
		try {
			if (null != mDbHelper) {
				mDbHelper.open();
			}
			Cursor mCursor = mDbHelper.getBrandNameForSWAP();
			if (!CursorUtils.isValidCursor(mCursor)) {
				return;
			}
			mCursor.moveToFirst();
			mBrandArrayList.clear();
			mBrandArrayList.add(mPleaseSelectBrand);
			while (!mCursor.isAfterLast()) {
				mBrandArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("Description")));
				mCursor.moveToNext();
			}
			mCursor.close();
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mBrandArrayList);
			dataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mBrand.setAdapter(dataAdapter);
			mBrand.setEnabled(true);
			if (mSelectedTire.getSerialNumber().equals(
					Constants.EDITED_SERIAL_NUMBER)) {
				for (int i = 0; i < mBrandArrayList.size(); i++) {
					if (mBrandArrayList.get(i).equals(mSelectedBrandName)) {
						mBrand.setSelection(i);
						break;
					}
				}
			}
			mBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView adapter, View v, int i,
										   long lng) {
					mBrand.setOnItemSelectedListener(listenerSelectBrandName);
				}

				@Override
				public void onNothingSelected(AdapterView arg0) {
					Toast.makeText(getApplicationContext(),
							Constants.sLblNothingSelected, Toast.LENGTH_SHORT)
							.show();
				}
			});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			mDbHelper.close();
		}
	}

	OnItemSelectedListener listenerSelectBrandName = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
								   int position, long id) {
			mSelectedBrandName = mBrand.getSelectedItem().toString();
			if (Constants.onBrandBool && Constants.DIFFERENT_SIZE_ALLOWED){
				mIsSizeSpinnerEditable = true;
			}
			if (mIsSizeSpinnerEditable) {
				clearPreviousSelections(2);
				if (0 < mBrand.getSelectedItemPosition()) {
					getTyreSizes();
				} else {
					clearSpinnersData(2);
				}
			} else {
				clearPreviousSelections(5);
				if (0 < mBrand.getSelectedItemPosition()) {
					getTyreDesign();
				} else {
					clearSpinnersData(5);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub

		}
	};
	/**
	 * Method Getting Tire Sizes from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreSizes() {
		Cursor mCursor = null;
		try {
			if (null != mDbHelper) {
				mDbHelper.open();
			}
			mCursor = mDbHelper.getTyreSizeFromBrand(mSelectedBrandName);
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(2);
				return;
			}
			mCursor.moveToFirst();
			mSizeArrayList.clear();
			// mSizeArrayList.add("");
			while (!mCursor.isAfterLast()) {
				mSizeArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("TSize")));
				mCursor.moveToNext();
			}
			CursorUtils.closeCursor(mCursor);
			loadTyreSize();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method populating Tire Size after getting from local DB3 file as per the
	 * selected tire-brand
	 */
	private void loadTyreSize() {
		try {
			mSizeDataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mSizeArrayList);
			mSizeDataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSize.setAdapter(mSizeDataAdapter);
			for (int i = 0; i < mSizeArrayList.size(); i++) {
				if (mSizeArrayList.get(i).equals(mSelectedtyreSize)) {
					mSize.setSelection(i);
					break;
				}
			}
			mSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView adapter, View v, int i,
										   long lng) {
					mSelectedtyreSize = mSize.getSelectedItem().toString();
					clearPreviousSelections(3);
					if (0 <= mSize.getSelectedItemPosition()) {
						getTyreASP();
					} else {
						clearSpinnersData(3);
					}
				}

				@Override
				public void onNothingSelected(AdapterView arg0) {
				}
			});
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Method Getting Tire ASP from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreASP() {
		Cursor mCursor = null;
		try {
			if (null != mDbHelper) {
				mDbHelper.open();
			}
			mCursor = mDbHelper.getTyreASPFromSize(mSelectedtyreSize,
					mSelectedBrandName);
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(3);
				return;
			}
			mCursor.moveToFirst();
			mTyreTASPArrayList.clear();
			// mTyreTASPArrayList.add("");
			while (!mCursor.isAfterLast()) {
				mTyreTASPArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("TASP")));
				mCursor.moveToNext();
			}
			CursorUtils.closeCursor(mCursor);
			loadTyreTASP();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method populating Tire ASP after getting from local DB3 file as per the
	 * selected tire-brand
	 */
	private void loadTyreTASP() {
		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mASP.setAdapter(mASPDataAdapter);
		for (int i = 0; i < mTyreTASPArrayList.size(); i++) {
			if (mTyreTASPArrayList.get(i).equals(mSelectedTyreTASP)) {
				mASP.setSelection(i);
				break;
			}
		}
		mASP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView adapter, View v, int i,
									   long lng) {
				mSelectedTyreTASP = mASP.getSelectedItem().toString();
				clearPreviousSelections(4);
				if (0 <= mASP.getSelectedItemPosition()) {
					getTyreRIM();
				} else {
					clearSpinnersData(4);
				}
			}

			@Override
			public void onNothingSelected(AdapterView arg0) {
			}
		});
	}
	/**
	 * Method Getting Tire RIM from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreRIM() {
		Cursor mCursor = null;
		try {
			if (null != mDbHelper) {
				mDbHelper.open();
			}
			mCursor = mDbHelper.getTyreRimFromSize(mSelectedtyreSize,
					mSelectedTyreTASP, mSelectedBrandName);
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(4);
				return;
			}
			mCursor.moveToFirst();
			mTyreTRIMArrayList.clear();
			// mTyreTRIMArrayList.add("");
			while (!mCursor.isAfterLast()) {
				mTyreTRIMArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("TRIM")));
				mCursor.moveToNext();
			}
			CursorUtils.closeCursor(mCursor);
			loadTyreTRIM();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method populating Tire RIM after getting from local DB3 file as per the
	 * selected tire-brand
	 */
	private void loadTyreTRIM() {
		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mRim.setAdapter(mRimDataAdapter);
		for (int i = 0; i < mTyreTRIMArrayList.size(); i++) {
			if (mTyreTRIMArrayList.get(i).equals(mSelectedtyreTRIM)) {
				mRim.setSelection(i);
				break;
			}
		}
		mRim.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView adapter, View v, int i,
									   long lng) {
				mSelectedtyreTRIM = mRim.getSelectedItem().toString();
				clearPreviousSelections(5);
				if (0 <= mRim.getSelectedItemPosition()) {
					getTyreDesign();
				} else {
					clearSpinnersData(5);
				}
			}

			@Override
			public void onNothingSelected(AdapterView arg0) {
			}
		});
	}
	/**
	 * Method Getting Tire Design from local DB3 file as per the selected
	 * tire-brand
	 */
	public void getTyreDesign() {
		Cursor mCursor = null;
		try {
			if (null != mDbHelper) {
				mDbHelper.open();
			}
			mCursor = mDbHelper.getTyreDesign(mSelectedtyreSize,
					mSelectedTyreTASP, mSelectedtyreTRIM, mSelectedBrandName);
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(5);
				return;
			}
			mCursor.moveToFirst();
			mDesignArrayList.clear();
			mDesignArrayList.add(mDefaultValueForSpinner);
			while (!mCursor.isAfterLast()) {
				mDesignArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("Deseign")));
				mCursor.moveToNext();
			}
			CursorUtils.closeCursor(mCursor);
			loadTyreDesign();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method populating Tire Design after getting from local DB3 file as per
	 * the selected tire-brand
	 */
	private void loadTyreDesign() {
		mDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mDesignArrayList);
		mDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mDesign.setAdapter(mDesignDataAdapter);
		for (int i = 0; i < mDesignArrayList.size(); i++) {
			if (mDesignArrayList.get(i).equals(mSelectedTyreDesign)) {
				mDesign.setSelection(i);
				break;
			}
		}
		mDesign.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView adapter, View v, int i,
									   long lng) {
				mSelectedTyreDesign = mDesign.getSelectedItem().toString();
				clearPreviousSelections(6);
				if (0 < mDesign.getSelectedItemPosition()) {
					getTyreDetailedDesign();// type
				} else {
					clearSpinnersData(6);
				}
			}

			@Override
			public void onNothingSelected(AdapterView arg0) {

			}
		});
	}
	/**
	 * Method Getting Tire Design-Details from local DB3 file as per the
	 * selected tire-brand
	 */
	public void getTyreDetailedDesign() {
		Cursor mCursor = null;
		try {
			if (null != mDbHelper) {
				mDbHelper.open();
			}
			mCursor = mDbHelper.getTyreDetailedDesign(mSelectedtyreSize,
					mSelectedTyreTASP, mSelectedtyreTRIM, mSelectedTyreDesign,
					mSelectedBrandName);
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(6);
				return;
			}
			mCursor.moveToFirst();
			mFullDesignArrayList.clear();
			mFullDesignArrayList.add(mDefaultValueForSpinner);
			while (!mCursor.isAfterLast()) {
				mFullDesignArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("FullTireDetails")));
				mCursor.moveToNext();
			}
			CursorUtils.closeCursor(mCursor);
			loadTyreDetailedDesign();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method populating Tire Design-Details after getting from local DB3 file
	 * as per the selected tire-brand
	 */
	private void loadTyreDetailedDesign() {
		mFullDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mFullDesignArrayList);
		mFullDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mType.setAdapter(mFullDesignDataAdapter);
		for (int i = 0; i < mFullDesignArrayList.size(); i++) {
			if (mFullDesignArrayList.get(i).equals(mSelectedTyreDetailedDesign)) {
				mType.setSelection(i);
				break;
			}
		}
		mType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView adapter, View v, int i,
									   long lng) {
				mSelectedTyreDetailedDesign = mType.getSelectedItem()
						.toString();
				Constants.FULLDESIGN_SELECTED = mSelectedTyreDetailedDesign;
			}

			@Override
			public void onNothingSelected(AdapterView arg0) {

			}
		});
	}

	/**
	 * When spinner value is null then dependent spinner values refreshing.
	 */
	private void clearSpinnersData(int i) {
		// Don't add break statement, It's sequence of execution
		switch (i) {
			case 1: // Brand Spinner
			case 2: // Size Spinner
				mSizeArrayList.clear();
				if (null != mSizeDataAdapter) {
					mSizeDataAdapter.notifyDataSetChanged();
				}
			case 3: // ASP Spinner
				mTyreTASPArrayList.clear();
				if (null != mASPDataAdapter) {
					mASPDataAdapter.notifyDataSetChanged();
				}
			case 4: // Rim Spinner
				mTyreTRIMArrayList.clear();
				if (null != mRimDataAdapter) {
					mRimDataAdapter.notifyDataSetChanged();
				}
			case 5: // Design Spinner
				mDesignArrayList.clear();
				if (null != mDesignDataAdapter) {
					mDesignDataAdapter.notifyDataSetChanged();
				}
			case 6: // FullDetails Spinner
				mFullDesignArrayList.clear();
				if (mFullDesignDataAdapter != null) {
					mFullDesignDataAdapter.notifyDataSetChanged();
				}
		}
	}
	/**
	 * Method Clearing the Previous Selection Values of the spinners present in
	 * the UI
	 */
	private void clearPreviousSelections(int i) {
		if (mIsOrientationChanged && i < mLastSpinnerSelection) {
			if (i == 6 && mLastSpinnerSelection == 7) {
				mIsOrientationChanged = false;
			}
			return;
		} else if (mIsOrientationChanged) {
			mIsOrientationChanged = false;
		}
		// Don't add break statement, It's sequence of execution
		switch (i) {
			case 1: // Brand Spinner
			case 2: // Size Spinner
				mSelectedtyreSize = "";
			case 3: // ASP Spinner
				mSelectedTyreTASP = "";
			case 4: // Rim Spinner
				mSelectedtyreTRIM = "";
			case 5: // Design Spinner
				mSelectedTyreDesign = "";
			case 6: // FullDetails Spinner
				mSelectedTyreDetailedDesign = "";
		}
	}
	/**
	 * Method Checking the Previous Selected Spinner just before the change in
	 * orientation
	 */
	private void checkLatestSpinnerSelectionBeforeOrientationChanges() {
		if (null == mSelectedBrandName
				|| mPleaseSelectBrand.equals(mSelectedBrandName)) {
			mLastSpinnerSelection = 1;
		} else if (null == mSelectedtyreSize || "".equals(mSelectedtyreSize)) {
			mLastSpinnerSelection = 2;
		} else if (null == mSelectedTyreTASP || "".equals(mSelectedTyreTASP)) {
			mLastSpinnerSelection = 3;
		} else if (null == mSelectedtyreTRIM || "".equals(mSelectedtyreTRIM)) {
			mLastSpinnerSelection = 4;
		} else if (null == mSelectedTyreDesign
				|| mDefaultValueForSpinner.equals(mSelectedTyreDesign)) {
			mLastSpinnerSelection = 5;
		} else if (null == mSelectedTyreDetailedDesign
				|| mDefaultValueForSpinner.equals(mSelectedTyreDetailedDesign)) {
			mLastSpinnerSelection = 6;
		} else {
			mLastSpinnerSelection = 7;
		}
	}

	IOnPerformanceCallback callback = new IOnPerformanceCallback() {

		@Override
		public void updateTables() {
			onReturnToSkeleton();
		}

		@Override
		public void showProgressBar(boolean toShow) {
			if (toShow) {
				// Show Progress Bar here
			} else {
				// Hide Progress bar here
			}
		}

		@Override
		public void onPostExecute(ResponseMessagePerformance message) {
			Intent intentReturn = new Intent();
			intentReturn.putExtra("inspection", true);
			setResult(VehicleSkeletonFragment.INSPECTION_INTENT, intentReturn);
			Constants.JOB_CORRECTION_LIST = VehicleSkeletonFragment.mJobcorrectionList;
			CameraUtility.resetParams();
			Constants.onBrandBool = false;
			finish();
			// sNoteText = "";
			/*CommonUtils.notify("",
					InspectionActivity.this);*/
			InspectionActivity.this.overridePendingTransition(R.anim.slide_left_in,
					R.anim.slide_left_out);
		}

		@Override
		public String onPreExecute() {
			String messageStr = PerformanceBaseModel.MESSAGE_STR;
			// set Re-grooved
			int checkedRadioButtonId = mRGRegrooveType
					.getCheckedRadioButtonId();
			int checkedRadioButtonTemperature = mTemperature
					.getCheckedRadioButtonId();
			if (checkedRadioButtonId == -1) {
				// if re grooved not selected
				messageStr = mRegrove_blank_msg;
			}else if(checkedRadioButtonTemperature == -1){
				messageStr = "Temperature isrequired";
			}
			return messageStr;
		}

		@Override
		public void onError(ResponseMessagePerformance message) {
			CommonUtils.notify(message.getErrorMessage(),
					getApplicationContext());
		}

		@Override
		public void closePreviousTask() {
			if (task != null && !task.isCancelled()) {
				task.cancel(true);
			}

		}
	};

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		sNoteText = "";
		InactivityUtils.logoutFromActivityAboveEjobForm(this);
	}

}
