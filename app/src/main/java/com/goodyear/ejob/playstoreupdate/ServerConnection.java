package com.goodyear.ejob.playstoreupdate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.LogUtil;

import android.content.SharedPreferences;

/**
 * @author amitkumar.h
 * Class creating http request to check the latest playStore version of the app
 * It returns the entire detail of the uploaded app on the playStore
 */
public class ServerConnection {
	static String resultString = null;
	static HttpResponse response = null;
	private final static String TAG = "Server Connection for Update";
	JSONObject jsonObjRecv =null;
	
	/**
	 * Method returning the response from 
	 * the playStore as per the URL passed during the http request
	 * @param message
	 * @return
	 */
	public static String Connection(String message) {

		try {  
			HttpClient client = new DefaultHttpClient(); 
			String postURL = Constants.EJOB_GET_VERSION_URL;
			HttpGet get = new HttpGet(postURL); 
			System.out.println("ok"); 
			get.setHeader("Content-type", "application/json");
			response = client.execute(get);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				// Read the content stream
				InputStream is = entity.getContent();
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
					is = new GZIPInputStream(is);
				}
				// convert content stream to a String
				resultString = getStringFromInputStream(is);
				is.close();
		}
		} catch (Exception e) 
		{
			LogUtil.i(TAG , " "+e);
			e.printStackTrace();
		}
		return resultString;
	}
	/**
	 * Method returning response in string format
	 * after converting the same from Input Sting format
	 * @param is
	 * @return
	 */
	private static String getStringFromInputStream(InputStream is) {
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) { 
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}
}