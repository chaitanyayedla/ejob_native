
package com.goodyear.ejob;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.goodyear.ejob.adapter.VisualRemarkListAdapter;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.VisualRemark;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.ObjectSerializer;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author giriprasanth.vp
 * Visual Remarks Activity  provides the interface to Add and Delete Visual Remarks
 */
public class VisualRemarksActivity extends Activity {

    /**
     * Android Widgets spinners, ImageView and ListView
     */
    private Spinner mDescription;
    private Spinner mType;
    private Spinner mDo;
    private ImageView mAddRemark;
    private ListView mRemarkList;

    private TextView mTypeLabel;
    private TextView mDoLabel;
    private TextView mDescriptionLabel;
    private TextView mTypeTextView;
    private TextView mDescriptionTextView;
    private TextView mDoTextView;

    public static String mVisualRemarkAdded;
    public static String mVisualRemarkDeleted;
    public static String mVisualRemarkAlreadyExist;
    private String mVisualRemarkSaved;
    public static String mAreYouSureWantToDeleteVisualRemark;
    private String mLikeToSaveVisualRemark;

    /**
     * Constants
     */
    public static String TYPE = "TYPE";
    public static String DESCRIPTION = "DESCRIPTION";
    public static String DO = "DO";
    public static String REMARK_LIST = "REMARK_LIST";
    public static String SELECTED_REMARK = "SELECTED_REMARK";
    public static String SAVED_STATE = "SAVED_STATE";

    public static String SELECT_TYPE = "[SELECT TYPE]";
    public static String SELECT_DESCRIPTION = "[SELECT DESCRIPTION]";
    public static String SELECT_DO = "[SELECT DO]";


    /**
     * Array List for Type,Description and Do Spinner Data
     */
    private ArrayList<String> mTypeArrayList;
    private ArrayList<String> mDescriptionArrayList;
    private ArrayList<String> mDoArrayList;

    /**
     * ArrayAdapter for Spinners
     */
    private ArrayAdapter<String> mTypeSpinnerDataAdapter;
    private ArrayAdapter<String> mDescriptionSpinnerDataAdapter;
    private ArrayAdapter<String> mDoSpinnerDataAdapter;

    /**
     * Selected Spinner Position by Default 0
     */
    private int mSelectedTypeSpinner;
    private int mSelectedDescriptionSpinner;
    private int mSelectedDoSpinner;

    /**
     * Adapter to Display Visual Remark List
     */
    private VisualRemarkListAdapter mVisualRemarkListAdapter;

    /**
     * Visual Remark Array Object
     */
    private ArrayList<VisualRemark> mVisualRemarks;

    /**
     * Visual Remark Array Local Object
     */
    private ArrayList<VisualRemark> mLocalReferenceVisualRemarks;

    /**
     * Selected Visual Remark Object
     */
    private VisualRemark mSelectedVisualRemark;

    /**
     * Menu Button and Database adapter
     */
    public static MenuItem buttonSaved;
    private DatabaseAdapter mDbHelper;

    /**
     * Boolean value to get Saved State of Data
     */
    private boolean mIsSaved;

    /**
     * Boolean Value For Orientation Change
     */
    private boolean mOrientationChange;

    private String TAG = "Visual Remark";
    //User Trace logs trace tag
    private static final String TRACE_TAG = "Visual Remarks";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visual_remarks);
        LogUtil.TraceInfo(TRACE_TAG, "none", "OP", true, false, true);
        initialization();
        loadLabelsFromDB();
        loadSpinnerData();
        remarkListInitialization();
    }

    /**
     * Action Bar menu OnCreate
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.visual_remarks_menu, menu);

        buttonSaved = menu.findItem(R.id.action_save);
        saveButtonTrigger(!mIsSaved);
        saveButtonListener();
        return true;
    }


    /**
     * Method to Initialize the variables using findViewById
     */
    private void initialization() {
        mDbHelper = new DatabaseAdapter(this);

        mType = (Spinner) findViewById(R.id.value_type);
        mDescription = (Spinner) findViewById(R.id.value_description);
        mDo = (Spinner) findViewById(R.id.value_do);
        mAddRemark = (ImageView) findViewById(R.id.btn_add_remark);
        mRemarkList = (ListView) findViewById(R.id.llv_remark);

        mTypeLabel = (TextView) findViewById(R.id.lbl_type);
        mDescriptionLabel = (TextView) findViewById(R.id.lbl_description);
        mDoLabel = (TextView) findViewById(R.id.lbl_do);
        mTypeTextView = (TextView) findViewById(R.id.tv_type);
        mDescriptionTextView = (TextView) findViewById(R.id.tv_desc);
        mDoTextView = (TextView) findViewById(R.id.tv_do);

        mSelectedTypeSpinner = 0;
        mSelectedDescriptionSpinner = 0;
        mSelectedDoSpinner = 0;

        mIsSaved = true;

        mTypeArrayList = new ArrayList<>();
        mDescriptionArrayList = new ArrayList<>();
        mDoArrayList = new ArrayList<>();

        mVisualRemarks = new ArrayList<>();
        mLocalReferenceVisualRemarks = new ArrayList<>();
        mSelectedVisualRemark = new VisualRemark();

        mType.setEnabled(false);
        mDescription.setEnabled(false);
        mDo.setEnabled(false);
        mAddRemark.setEnabled(false);

        addRemarkButtonAction();

        if (Constants.mGlobalVisualRemarks != null) {
            loadOldVisualRemarksData();
        }
    }

    /**
     * Method to load Label from Database
     */
    private void loadLabelsFromDB() {
		DatabaseAdapter label = new DatabaseAdapter(this);
		label.createDatabase();
		label.open();

        setTitle(label.getLabel(Constants.LABEL_VISUAL_REMARK));

        mTypeLabel.setText(label.getLabel(Constants.TYPE_LABEL));
        mDoLabel.setText(label.getLabel(Constants.LABEL_DO));
        mDescriptionLabel.setText(label.getLabel(Constants.LABEL_DESC));
        mTypeTextView.setText(label.getLabel(Constants.TYPE_LABEL));
        mDescriptionTextView.setText(label.getLabel(Constants.LABEL_DESC));
        mDoTextView.setText(label.getLabel(Constants.LABEL_DO));

        mVisualRemarkAdded=(label.getLabel(Constants.LABEL_VISUAL_REMARK_ADDED));
        mVisualRemarkDeleted=(label.getLabel(Constants.LABEL_VISUAL_REMARK_REMOVED));
        mVisualRemarkAlreadyExist=(label.getLabel(Constants.ALLREADY_ADDED));
        mVisualRemarkSaved=(label.getLabel(Constants.LABEL_VISUAL_REMARK_SAVED));
        mAreYouSureWantToDeleteVisualRemark=(label.getLabel(Constants.LABEL_DELETE_THE_SELECTED_VISUAL_REMARK));
        mLikeToSaveVisualRemark=(label.getLabel(Constants.LABEL_LIKE_TO_SAVE_VISUAL_REMARK));

        SELECT_TYPE=(label.getLabel(Constants.LABEL_SELECT_TYRE));
        SELECT_DESCRIPTION=(label.getLabel(Constants.LABEL_SELECT_DESCRIPTION));
        SELECT_DO=(label.getLabel(Constants.LABEL_SELECT_DO));

        label.close();
    }

    /**
     * Method to Load Spinner Data and Perform Action
     */
    private void loadSpinnerData() {

        mTypeArrayList = getVisualRemarkTypesFromDB();
        if (mTypeArrayList != null) {
            mTypeArrayList.add(0, SELECT_TYPE);
        }

        mType.setEnabled(true);
        TypeSpinnerDataAndAction();
    }

    /**
     * Method to Perform Type Spinner Action
     */
    private void TypeSpinnerDataAndAction() {

        //Spinner Adapter
        mTypeSpinnerDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mTypeArrayList);
        mTypeSpinnerDataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Spinner Set Adapter
        mType.setAdapter(mTypeSpinnerDataAdapter);

        //If orientation occurs Then the State is Preserved
        if (mOrientationChange) {
            if (mTypeArrayList.size() > mSelectedTypeSpinner) {
                mType.setSelection(mSelectedTypeSpinner);
            } else {
                mType.setSelection(0);
            }
        } else {
            mType.setSelection(0);
        }

        //Touch Listener to clear Previous Selection
        mType.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //clear Previous Selection
                clearPreviousSelection(1);
                return false;
            }
        });

        mType.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                mType.setOnItemSelectedListener(listenerSelectType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    /**
     * Item Selected Listener (watches type item selection)
     */
    OnItemSelectedListener listenerSelectType = new OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {
            if (!mOrientationChange) {
                mSelectedDescriptionSpinner = 0;
            }
            if (arg2 > 0) {
                mSelectedVisualRemark.setType(mTypeArrayList.get(arg2));
                clearSpinnersData(2);
                //Loading Data to ArrayList
                mDescriptionArrayList = getVisualRemarkDescriptionsByTypeFromDB(mSelectedVisualRemark.getType());
                if (mDescriptionArrayList != null) {
                    mDescriptionArrayList.add(0, SELECT_DESCRIPTION);
                }
                mDescription.setEnabled(true);
                DescriptionSpinnerDataAndAction();
            } else {
                mDescriptionArrayList.clear();
                clearSpinnersData(2);
                mDescription.setEnabled(false);
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
        }
    };


    /**
     * Method to Perform Description Spinner Action
     */
    private void DescriptionSpinnerDataAndAction() {

        //Spinner Adapter
        mDescriptionSpinnerDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mDescriptionArrayList);
        mDescriptionSpinnerDataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Spinner Set Adapter
        mDescription.setAdapter(mDescriptionSpinnerDataAdapter);

        //If orientation occurs Then the State is Preserved
        if (mOrientationChange) {
            if (mDescriptionArrayList.size() > mSelectedDescriptionSpinner) {
                mDescription.setSelection(mSelectedDescriptionSpinner);
            } else {
                mDescription.setSelection(0);
            }
        } else {
            mDescription.setSelection(0);
        }

        //Touch Listener to clear Previous Selection
        mDescription.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //clear Previous Selection
                clearPreviousSelection(2);
                return false;
            }
        });

        mDescription.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                mDescription.setOnItemSelectedListener(listenerSelectDescription);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    /**
     * Item Selected Listener (watches Description item selection)
     */
    OnItemSelectedListener listenerSelectDescription = new OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {

            if (!mOrientationChange) {
                mSelectedDoSpinner = 0;
            }

            if (arg2 > 0) {
                mSelectedVisualRemark.setDescription(mDescriptionArrayList.get(arg2));
                clearSpinnersData(3);

                //Loading Data to ArrayList
                mDoArrayList = getVisualRemarkDosByTypeAndDescriptionFromDB(mSelectedVisualRemark.getType(), mSelectedVisualRemark.getDescription());
                if (mDoArrayList != null) {
                    mDoArrayList.add(0, SELECT_DO);
                }

                mDo.setEnabled(true);
                DoSpinnerDataAndAction();
            } else {
                mDoArrayList.clear();
                clearSpinnersData(3);
                mDo.setEnabled(false);
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
        }
    };

    /**
     * Method to Perform Do Spinner Action
     */
    private void DoSpinnerDataAndAction() {

        //Spinner Adapter
        mDoSpinnerDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mDoArrayList);
        mDoSpinnerDataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Spinner Set Adapter
        mDo.setAdapter(mDoSpinnerDataAdapter);

        //If orientation occurs Then the State is Preserved
        if (mOrientationChange) {
            if (mDoArrayList.size() > mSelectedDoSpinner) {
                mDo.setSelection(mSelectedDoSpinner);
            } else {
                mDo.setSelection(0);
            }
        } else {
            mDo.setSelection(0);
        }

        //Touch Listener to clear Previous Selection
        mDo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                //Clear Previous Selection
                clearPreviousSelection(3);
                return false;
            }
        });

        mDo.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                mDo.setOnItemSelectedListener(listenerSelectDo);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    /**
     * Item Selected Listener (watches Do item selection)
     */
    OnItemSelectedListener listenerSelectDo = new OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {
            if (arg2 > 0) {
                mSelectedVisualRemark.setDo(mDoArrayList.get(arg2));
                mAddRemark.setEnabled(true);
            } else {
                mAddRemark.setEnabled(false);
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {

        }
    };

    /**
     * Method which initialize Remark List and loading Visual Remark Data on UI element
     */
    private void remarkListInitialization() {
        mRemarkList.setDivider(null);
        mRemarkList.setDividerHeight(0);
        //Remark List Adapter
        mVisualRemarkListAdapter = new VisualRemarkListAdapter(mVisualRemarks, this);
        mRemarkList.setAdapter(mVisualRemarkListAdapter);

        //Long press on visual remark - delete visual remark
        mRemarkList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                mVisualRemarkListAdapter.deleteLongPressedItem(i);
                return false;
            }
        });
    }

    /**
     * When spinner value is null then dependent spinner values refreshing.
     */
    private void clearSpinnersData(int i) {
        // Don't add break statement, It's sequence of execution
        switch (i) {
            case 1://Type Spinner
                mSelectedVisualRemark = new VisualRemark();
                mDescription.setEnabled(false);
                mDo.setEnabled(false);
                if (mTypeArrayList != null && mTypeArrayList.size() > 0) {
                    mType.setSelection(0);
                }
                if (mTypeSpinnerDataAdapter != null) {
                    mTypeSpinnerDataAdapter.notifyDataSetChanged();
                }
            case 2://Description Spinner
                mDescriptionArrayList.clear();
                if (mDescriptionSpinnerDataAdapter != null) {
                    mDescriptionSpinnerDataAdapter.notifyDataSetChanged();
                }
            case 3://Do Spinner
                mDoArrayList.clear();
                if (mDoSpinnerDataAdapter != null) {
                    mDoSpinnerDataAdapter.notifyDataSetChanged();
                }
        }
    }

    /**
     * Method to clear the previous Selections
     */
    private void clearPreviousSelection(int i) {
        switch (i) {
            case 0: //Initial
                mSelectedTypeSpinner = 0;
                mSelectedDescriptionSpinner = 0;
                mSelectedDoSpinner = 0;
                mSelectedVisualRemark.setType("");
            case 1: //Type
                mSelectedVisualRemark.setDescription("");
            case 2: //Description
                mSelectedVisualRemark.setDo("");
                mAddRemark.setEnabled(false);
            case 3: //Do
                mOrientationChange = false;
        }
    }

    @Override
    public void onBackPressed() {
        LogUtil.i("TAG",
                "Back Button Pressed");
        if (mIsSaved) {
            LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press", false, false, false);
            super.onBackPressed();
        } else {
            getUserSavePreferenceBeforeMovingBack();
        }
    }

    /**
     * Method to Enable and Disable Save Button based on Boolean Value
     *
     * @param key Boolean Value to enable/disable save Button
     */
    public void saveButtonTrigger(Boolean key) {
        if (key) {
            buttonSaved.setIcon(R.drawable.savejob_navigation);
            buttonSaved.setEnabled(true);
            mIsSaved = false;
        } else {
            buttonSaved.setIcon(R.drawable.unsavejob_navigation);
            buttonSaved.setEnabled(false);
            mIsSaved = true;
        }

    }

    /**
     * Method which contains Add Remark Button Action
     */
    private void addRemarkButtonAction() {
        mAddRemark.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                try {
                    mDbHelper.open();
                    //getting Visual Remark ID from DB
                    mSelectedVisualRemark.setVRId(mDbHelper.getVisualRemarkId(mSelectedVisualRemark.getType(),
                            mSelectedVisualRemark.getDescription(), mSelectedVisualRemark.getDo()));


                    //Adding and before that Checking for existence for Visual Remark
                    if (mVisualRemarkListAdapter.addVisualRemark(mSelectedVisualRemark)) {
                        saveButtonTrigger(true);
                    }
                    clearSpinnersData(1);
                    clearPreviousSelection(0);


                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    mDbHelper.close();
                }
            }
        });
    }

    /**
     * Method which contains Save Button Listener
     */
    private void saveButtonListener() {
        buttonSaved.setOnMenuItemClickListener(new OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                saveButtonAction();
                return false;
            }
        });
    }

    /**
     * Method which contains Save Button Action
     */
    private void saveButtonAction() {

        Constants.mGlobalVisualRemarks = loadFromVisualRemarkAdapterAdapter();
        try
        {
            LogUtil.TraceInfo(TRACE_TAG, "Save", "Data : \n"+Constants.mGlobalVisualRemarks.toString(), false, true, false);
            LogUtil.DBLog(TAG, "Visual Remark Data Saved : \n", Constants.mGlobalVisualRemarks.toString());
            LogUtil.i(TAG, "Visual Remark Data Saved ");
        }
        catch (Exception e)
        {
            LogUtil.TraceInfo(TRACE_TAG, "Save","Exception - " + e.getMessage(), false, true, false);
            LogUtil.i(TAG, "Visual Remark Data Saved : Exception - " + e.getMessage() );
        }

        //FIX : Visual Remark Retain Old Value
        loadOldVisualRemarksData();
        CommonUtils.notify(mVisualRemarkSaved,getApplicationContext());
                saveButtonTrigger(false);
    }

    /**
     * Method to load Global Visual Remarks Data from Visual Remark Adapter Data
     */
    private ArrayList<VisualRemark> loadFromVisualRemarkAdapterAdapter()
    {
        ArrayList<VisualRemark> tempVisualRemarks = new ArrayList<>();
        ArrayList<VisualRemark> resultVisualRemarks = mVisualRemarkListAdapter.returnVisualRemarkList();
        for(int i=0;i<mVisualRemarkListAdapter.returnVisualRemarkList().size();i++)
        {
            tempVisualRemarks.add(resultVisualRemarks.get(i));
        }
        return tempVisualRemarks;
    }

    /**
     * Method to fetch Visual Remark Type from DB
     *
     * @return mTypes
     */
    private ArrayList<String> getVisualRemarkTypesFromDB() {
        ArrayList<String> mTypes = new ArrayList<>();
        Cursor mCursor = null;
        try {
            mDbHelper.open();
            //get Cursor from DB
            mCursor = mDbHelper.getVisualRemarkTypes();
            mCursor.moveToFirst();
            //Added Cursor Validation check for mCursor
            if (CursorUtils.isValidCursor(mCursor)) {

                //Loading ArrayList with Cursor Data
                do {
                    mTypes.add(mCursor.getString(mCursor.getColumnIndex("Type")));
                } while (mCursor.moveToNext());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            mDbHelper.close();
            CursorUtils.closeCursor(mCursor);
        }
        return mTypes;
    }

    /**
     * Method to fetch Visual Remark Description from DB by type
     *
     * @param type filterType
     * @return mDescriptions
     */
    private ArrayList<String> getVisualRemarkDescriptionsByTypeFromDB(String type) {
        ArrayList<String> mDescriptions = new ArrayList<>();
        Cursor mCursor = null;
        try {
            mDbHelper.open();
            //get Cursor from DB
            mCursor = mDbHelper.getVisualRemarkDescriptionsByType(type);
            mCursor.moveToFirst();
            //Added Cursor Validation check for mCursor
            if (CursorUtils.isValidCursor(mCursor)) {

                //Loading ArrayList with Cursor Data
                do {
                    mDescriptions.add(mCursor.getString(mCursor.getColumnIndex("Description")));
                } while (mCursor.moveToNext());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            mDbHelper.close();
            CursorUtils.closeCursor(mCursor);
        }
        return mDescriptions;
    }

    /**
     * Method to fetch Visual Remark Actions (DO) from DB by type and description
     *
     * @param type        filterType
     * @param description filterDescription
     * @return mDos
     */
    private ArrayList<String> getVisualRemarkDosByTypeAndDescriptionFromDB(String type, String description) {
        ArrayList<String> mDos = new ArrayList<>();
        Cursor mCursor = null;
        try {
            mDbHelper.open();
            //get Cursor from DB
            mCursor = mDbHelper.getVisualRemarkDosByTypeAndDescription(type, description);
            mCursor.moveToFirst();
            //Added Cursor Validation check for mCursor
            if (CursorUtils.isValidCursor(mCursor)) {

                //Loading ArrayList with Cursor Data
                do {
                    mDos.add(mCursor.getString(mCursor.getColumnIndex("Action")));
                } while (mCursor.moveToNext());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            mDbHelper.close();
            CursorUtils.closeCursor(mCursor);
        }
        return mDos;
    }

    /**
     * Method to Load Existing Visual Remark Information
     */
    private void loadOldVisualRemarksData() {
        mVisualRemarks.clear();
        mLocalReferenceVisualRemarks.clear();
        int mCount = 0;

        //Loading old Values
        while (Constants.mGlobalVisualRemarks.size() > mCount) {
            mVisualRemarks.add(Constants.mGlobalVisualRemarks.get(mCount));
            mLocalReferenceVisualRemarks.add(Constants.mGlobalVisualRemarks.get(mCount));
            mCount++;
        }
    }

    /**
     *  Method to Load Local Values To Global
     *  FIX : Visual Remark Retain Old Value
     */
    private void loadVisualRemarkLocalValueToGlobal()
    {
        Constants.mGlobalVisualRemarks.clear();
        int mCount=0;

        while (mLocalReferenceVisualRemarks.size() > mCount) {
            Constants.mGlobalVisualRemarks.add(mLocalReferenceVisualRemarks.get(mCount));
            mCount++;
        }
    }

    /**
     * Method to alert unsaved Visual Remarks
     */
    private void getUserSavePreferenceBeforeMovingBack() {
        LogUtil.i("TAG",
                "User Save Preference");

        LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", false, true, false);
        // get prompts.xml view
        LayoutInflater inflator = LayoutInflater.from(this);
        View promptsView = inflator.inflate(R.layout.dialog_yes_no, null);
        TextView dialogText = (TextView) promptsView
                .findViewById(R.id.dialog_text_message);

        // load message
        DatabaseAdapter label = new DatabaseAdapter(this);
        label.createDatabase();
        label.open();

        //CONFIRM_SAVE
        dialogText.setText(mLikeToSaveVisualRemark);

        final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        // set user activity for
        LinearLayout rootNode = (LinearLayout) promptsView
                .findViewById(R.id.layout_root);
        rootNode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                InactivityUtils.updateActivityOfUser();
            }
        });

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton(label.getLabel(Constants.YES_LABEL),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
                        saveButtonAction();
                        //Finishing Current Activity
                        finish();
                    }
                });
        // set dialog message
        alertDialogBuilder.setCancelable(false).setNegativeButton(label.getLabel(Constants.NO_LABEL),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mIsSaved = true;

                        LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);
                        //FIX : Visual Remark Retain Old Value
                        loadVisualRemarkLocalValueToGlobal();
                        //Finishing Current Activity
                        finish();
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    /**
     * Method to Restore Data during Orientation change
     *
     * @param savedInstanceState Bundle object
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            //Restoring lastly Selected remark
            LogUtil.TraceInfo(TRACE_TAG, "none", "Orientation Change", true, false, false);
            mOrientationChange = true;

            if (savedInstanceState.containsKey(SELECTED_REMARK)) {
                try {
                    mSelectedVisualRemark = (VisualRemark) ObjectSerializer.deserialize(savedInstanceState.getString(SELECTED_REMARK, ObjectSerializer.serialize(new VisualRemark())));
                } catch (IOException e) {
                    LogUtil.i("TAG",
                            "onRestoreInstanceState : SELECTED_REMARK : Exception - "+ e.getMessage());
                    e.printStackTrace();
                }
            }

            // restoring remark_list
            if (savedInstanceState.containsKey(REMARK_LIST)) {
                try {
                    mVisualRemarks = (ArrayList) ObjectSerializer.deserialize(savedInstanceState.getString(REMARK_LIST, ObjectSerializer.serialize(new ArrayList())));
                } catch (IOException e) {
                    LogUtil.i("TAG",
                            "onRestoreInstanceState : REMARK_LIST : Exception - "+ e.getMessage());
                    e.printStackTrace();
                }

                //Refresh Adapter with updated Data
                mVisualRemarkListAdapter.changeVisualRemarkList(mVisualRemarks);
            }

            if (savedInstanceState.containsKey(TYPE)) {
                //If Type Exist then we will restore type
                if (savedInstanceState.getInt(TYPE) > 0 && savedInstanceState.getInt(TYPE) != 0) {
                    mType.setSelection(savedInstanceState.getInt(TYPE));
                    mSelectedTypeSpinner = savedInstanceState.getInt(TYPE);

                    //Loading data to mDescriptionArrayList
                    mDescriptionArrayList = getVisualRemarkDescriptionsByTypeFromDB(mSelectedVisualRemark.getType());
                    if (mDescriptionArrayList != null) {
                        mDescriptionArrayList.add(0, SELECT_DESCRIPTION);
                    }

                    if (savedInstanceState.containsKey(DESCRIPTION)) {

                        //if Description Exist then we will restore Description
                        if (savedInstanceState.getInt(DESCRIPTION) > 0 && savedInstanceState.getInt(DESCRIPTION) != 0) {

                            //Setting Values and Spinner Action
                            mSelectedDescriptionSpinner = savedInstanceState.getInt(DESCRIPTION);
                            mDescription.setSelection(savedInstanceState.getInt(DESCRIPTION));
                            DescriptionSpinnerDataAndAction();

                            //Loading Data to mDoArrayList
                            mDoArrayList = getVisualRemarkDosByTypeAndDescriptionFromDB(mSelectedVisualRemark.getType(), mSelectedVisualRemark.getDescription());
                            if (mDoArrayList != null) {
                                mDoArrayList.add(0, SELECT_DO);
                            }

                            if (savedInstanceState.containsKey(DO)) {

                                //if Do Exist then we will restore Do
                                if (savedInstanceState.getInt(DO) > 0 && savedInstanceState.getInt(DO) != 0) {

                                    //Setting Values and Spinner Action
                                    mSelectedDoSpinner = savedInstanceState.getInt(DO);
                                    mDo.setSelection(savedInstanceState.getInt(DO));
                                    DoSpinnerDataAndAction();
                                    mAddRemark.setEnabled(true);
                                }
                            }
                        }
                    }
                }
            }

            //Saved State of Data
            saveButtonTrigger(!savedInstanceState.getBoolean(SAVED_STATE));
            //mOrientationChange = false;
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    /**
     * Method to Backup Data during Orientation change
     *
     * @param outState bundle object
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {

        //Selected Type Position
        if (null != mType && mType.getCount() != 0) {
            outState.putInt(TYPE, mType.getSelectedItemPosition());
        } else {
            outState.putInt(TYPE, 0);
        }

        //Selected Description Position
        if (null != mDescription && mDescription.getCount() != 0) {
            outState.putInt(DESCRIPTION, mDescription.getSelectedItemPosition());
        } else {
            outState.putInt(DESCRIPTION, 0);
        }

        //Selected Action Position
        if (null != mDo && mDo.getCount() != 0) {
            outState.putInt(DO, mDo.getSelectedItemPosition());
        } else {
            outState.putInt(DO, 0);
        }

        //Currently Selected Visual Remark Information
        try {
            outState.putString(SELECTED_REMARK, ObjectSerializer.serialize(mSelectedVisualRemark));
        } catch (IOException e) {
            LogUtil.i("TAG",
                    "onSaveInstanceState : SELECTED_REMARK : Exception - "+ e.getMessage());
            e.printStackTrace();
        }

        //Added Visual mark List
        try {
            outState.putString(REMARK_LIST, ObjectSerializer.serialize(mVisualRemarkListAdapter.returnVisualRemarkList()));
        } catch (IOException e) {
            LogUtil.i("TAG",
                    "onSaveInstanceState : REMARK_LIST : Exception - "+ e.getMessage());
            e.printStackTrace();
        }

        //Data Saved State
        outState.putBoolean(SAVED_STATE, mIsSaved);
        super.onSaveInstanceState(outState);
    }

}
