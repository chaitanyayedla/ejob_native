package com.goodyear.ejob;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.goodyear.ejob.blutooth.BluetoothService;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.job.operation.helpers.PressureCheckListener;
import com.goodyear.ejob.job.operation.helpers.TireImageItem;
import com.goodyear.ejob.util.CameraUtility;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.InspectionDataHandler;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.ObjectSerializer;
import com.goodyear.ejob.util.Tyre;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

/**
 * @version 1.0
 * Class handling the tire operation called tire Mount Part Worn
 * It provides mechanism to load design page as per the selected dismounted
 * tire. Moreover it handles the data updates in different tables(JobItem and Tire) 
 * as per the user-entered tire-data during Mount Operation.
 */
public class MountPWTActivity extends Activity implements InactivityHandler {

	final Context context = this;
	private Swiper swipeDetector;
	private ArrayList<String> brandArrayList;
	private String selectedBrandName;
	private String selectedtyreSize;
	private String selectedTyreTASP;
	private String selectedtyreTRIM;
	private String selectedTyreDesign;
	private String selectedTyreDetailedDesign;
	private ArrayList<String> sizeArrayList;
	private ArrayList<String> tyreTRIMArrayList;
	private ArrayList<String> tyreTASPArrayList;
	private Spinner brandSpinner;
	private Spinner sizeSpinner;
	private Spinner tyreTRIMSpinner;
	private Spinner tyreTASPSpinner;
	private Spinner tyreDesignSpinner;
	private Spinner tyreFullDetailSpinner;
	private ArrayAdapter<String> sizeDataAdapter, rimDataAdapter,
			aspDataAdapter, designDataAdapter, fullDesignDataAdapter;
	private TextView value_tyrePosition;
	private TextView value_tyreSerialNumber;
	private ArrayList<String> designArrayList;
	private ArrayList<String> fullDesignArrayList;
	private EditText valueNSK1;
	private EditText valueNSK2;
	private EditText valueNSK3;
	private ScrollView mainScrollBar;
	private TextView nskLabel;
	private TextView dimensionLabel;
	private TextView sizeLabel;
	private TextView aspLabel;
	private TextView rimLabel;
	private TextView designLabel;
	private TextView typeLabel;
	private TextView lbl_serialNo_swap;
	/**
	 * ImageView for Camera icon
	 */
	private ImageView mCamera_icon;
	private static String noteText = "";
	private BluetoothService mBTService;
	private BroadcastReceiver mReceiver;
	private int nskCounter = 1;
	private TextView designTypeLabel;
	private Tyre selectedTire;
	private Switch rimValue;
	private DatabaseAdapter mDbHelper;
	public static final int NOTES_INTENT = 104;
	public static final int WOT_INTENT = 105;
	public static final int DECIMAL_LENGTH = 8;
	private boolean swiped = false;
	private TextView pressureLabel;
	private TextView pressureUnit;
	private EditText setPressure;
	private boolean isSettingsPSI;
	private SharedPreferences preferences;
	private float psiValue;
	private String serialNoFromIntent;
	private String tyreIdFromIntent;
	float recommendedPressure;
	private int axlePosition;
	private String finalConvertedPressure;
	private boolean workedOnTire = false;
	private Tyre dismountedTyre;
	private String pressureRequired;
	private String savedNSK1, savedNSK2, savedNSK3;
	private Bundle instanceState;
	private int nsk23SetFlag;
	private String selectedSAPMaterialCodeID;
	private String fieldIncorrect = "Please fill all the entries";
	private String mBackMsgLabel;
	private String mNoLabel;
	private String mYesLabel;
	private boolean mBlockPressureFromBluetooth;
	String strbluetoothconnectionfalied;
	private AlertDialog mBackAlertDialog;
	/**
	 * Randomly generated External ID for tyre to be used at the time of
	 * updating job items
	 */
	String mExternalIdTyre = "";
	private String mSAPCodeTiTyre;
	private String lblDesignCannotBeBlank = "Design details cant be blank";
	Bundle savedInstanceState;
	/**
	 * string variable to store external ID
	 */
	private String external_id = "";
	/**
	 * boolean variable for is Camera enabled
	 */
	private boolean mCameraEnabled = false;
	private TextView mBrandLabel;
	private TextView mJobDetailsLabel;
	BTTask task;
	private TextView mTireDetailsLabel;
	private String mNotesLabel;
	private static final String TAG = "MountPWTActivity";
	/**
	 * CR 505
	 * Tyre holding the data of the mounted PWT
	 */
	public static Tyre mPWT;
	//User Trace logs trace tag
	private static final String TRACE_TAG = "Mount Part Worn Tyre";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mount_new_tire);
		LogUtil.TraceInfo(TRACE_TAG, "none", "OP", true, false, true);
		this.savedInstanceState = savedInstanceState;
		preferences = getSharedPreferences(Constants.GOODYEAR_CONF, 0);
		if (preferences.getString(Constants.PRESSUREUNIT, "").equals(
				Constants.PSI_PRESSURE_UNIT)) {
			isSettingsPSI = true;
		}
		if (savedInstanceState == null) {
			noteText = "";
		}
		dismountedTyre = new Tyre();
		createDismountedTyre();
		serialNoFromIntent = getIntent().getExtras().getString("SERIAL_NO");
		tyreIdFromIntent = getIntent().getExtras().getString("TYRE_ID");
		recommendedPressure = getIntent().getExtras().getFloat(
				VehicleSkeletonFragment.mAXLEPRESSURE);
		axlePosition = getIntent().getExtras().getInt(
				VehicleSkeletonFragment.mAXLEPRESSURE_POS);
		selectedTire = Constants.SELECTED_TYRE;
		selectedSAPMaterialCodeID = Constants.SELECTED_TYRE
				.getSapMaterialCodeID();
		mDbHelper = new DatabaseAdapter(this);
		designArrayList = new ArrayList<String>();
		fullDesignArrayList = new ArrayList<String>();
		brandArrayList = new ArrayList<String>();
		sizeArrayList = new ArrayList<String>();
		tyreTRIMArrayList = new ArrayList<String>();
		tyreTASPArrayList = new ArrayList<String>();
		lbl_serialNo_swap = (TextView) findViewById(R.id.lbl_serialNo_swap);
		value_tyreSerialNumber = (TextView) findViewById(R.id.value_serialNo);
		value_tyrePosition = (TextView) findViewById(R.id.value_wp);
		brandSpinner = (Spinner) findViewById(R.id.spinner_brand);
		sizeSpinner = (Spinner) findViewById(R.id.spinner_dimenssionSize);
		tyreTASPSpinner = (Spinner) findViewById(R.id.value_dimenssionASP);
		tyreTRIMSpinner = (Spinner) findViewById(R.id.value_dimenssionRIM);
		tyreDesignSpinner = (Spinner) findViewById(R.id.spinner_make);
		tyreFullDetailSpinner = (Spinner) findViewById(R.id.spinner_type);
		valueNSK1 = (EditText) findViewById(R.id.value_NSK1);
		valueNSK2 = (EditText) findViewById(R.id.value_NSK2);
		valueNSK3 = (EditText) findViewById(R.id.value_NSK3);

		nskLabel = (TextView) findViewById(R.id.lbl_NSK);
		dimensionLabel = (TextView) findViewById(R.id.lbl_dimenssion);

		sizeLabel = (TextView) findViewById(R.id.lbl_dimenssionSiz);
		aspLabel = (TextView) findViewById(R.id.lbl_dimenssionASP);
		rimLabel = (TextView) findViewById(R.id.lbl_dimenssionRIM);
		designLabel = (TextView) findViewById(R.id.lbl_make);// lbl_type
		typeLabel = (TextView) findViewById(R.id.lbl_RimType);
		designTypeLabel = (TextView) findViewById(R.id.lbl_type);
		mBrandLabel = (TextView) findViewById(R.id.lbl_brand);
		mJobDetailsLabel = (TextView) findViewById(R.id.lbl_jobDetails);
		mTireDetailsLabel =((TextView) findViewById(R.id.lbl_tireDetails));
		// CR:451
		mCamera_icon = (ImageView) findViewById(R.id.camera_icon);
		mCamera_icon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent photoIntent = new Intent(MountPWTActivity.this,
						CameraActivity.class);
				photoIntent.putExtra("CURNT_IMAGE_COUNT",
						CameraUtility.getImageCount());
				startActivity(photoIntent);
			}
		});
		valueNSK1
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		valueNSK2
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		valueNSK3
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });

		valueNSK1.addTextChangedListener(new nskOneBeforeChanged());
		valueNSK2.addTextChangedListener(new nskTwoBeforeChanged());
		valueNSK3.addTextChangedListener(new nskThreeBeforeChanged());

		rimValue = (Switch) findViewById(R.id.value_RimType);
		pressureLabel = (TextView) findViewById(R.id.lbl_pressure);
		pressureUnit = (TextView) findViewById(R.id.lbl_pressureunit);
		setPressure = (EditText) findViewById(R.id.value_pressure);

		setValuetoPressueView();
		valueNSK1.addTextChangedListener(new nskOneBeforeChanged());
		loadLabelsFromDB();

		mainScrollBar = (ScrollView) findViewById(R.id.scroll_main);
		swipeDetector = new Swiper(getApplicationContext(), mainScrollBar);
		mainScrollBar.setOnTouchListener(swipeDetector);

		nsk23SetFlag = 0;
		if (savedInstanceState != null) {
			nskCounter = savedInstanceState.getInt("nsk_counter", 0);
			selectedBrandName = savedInstanceState.getString("BRAND");
			if (savedInstanceState.containsKey("SIZE")) {
				selectedtyreSize = savedInstanceState.getString("SIZE");
				selectedTyreTASP = savedInstanceState.getString("ASP");
				selectedtyreTRIM = savedInstanceState.getString("RIM");
				selectedTyreDesign = savedInstanceState.getString("DESIGN");
				selectedTyreDetailedDesign = savedInstanceState
						.getString("FULLDETAIL");

				savedNSK1 = savedInstanceState.getString("SAVEDNSK1");
				savedNSK2 = savedInstanceState.getString("SAVEDNSK2");
				savedNSK3 = savedInstanceState.getString("SAVEDNSK3");
			} else {
				selectedtyreSize = "";
			}

			// savedNSK1 = getTyreNSK(tyreIdFromIntent);
			// savedNSK2 = getTyreNSK(tyreIdFromIntent);
			// savedNSK3 = getTyreNSK(tyreIdFromIntent);
		}

		String[] nskFromDB = getTyreNSK(tyreIdFromIntent);
		if (nskFromDB != null) {
			valueNSK1.setText(nskFromDB[0]);
			valueNSK2.setText(nskFromDB[1]);
			valueNSK3.setText(nskFromDB[2]);
		}
		value_tyreSerialNumber.setText(serialNoFromIntent);
		value_tyrePosition.setText(selectedTire.getPosition());

		loadTyreFixedDetails();
		if (savedInstanceState != null) {
			mExternalIdTyre = savedInstanceState.getString("EXTERNALID");
		} else {
			if (TextUtils.isEmpty(mExternalIdTyre)) {
				mExternalIdTyre = String.valueOf(UUID.randomUUID());
			}
		}
		loadRimType();
		restoreDialogState(savedInstanceState);
	}

	/**
	 * Method to move dismounted tyre to new position
	 */
	private void createDismountedTyre() {
		dismountedTyre.setSerialNumber(Constants.SELECTED_TYRE
				.getSerialNumber());
		dismountedTyre.setSize(Constants.SELECTED_TYRE.getSize());
		dismountedTyre.setRim(Constants.SELECTED_TYRE.getRim());
		dismountedTyre.setRimType(Constants.SELECTED_TYRE.getRimType());
		dismountedTyre.setBrandName(Constants.SELECTED_TYRE.getBrandName());
		dismountedTyre.setSapCodeWP(Constants.SELECTED_TYRE.getSapCodeWP());
		dismountedTyre.setSapCodeTI(Constants.SELECTED_TYRE.getSapCodeTI());
		dismountedTyre.setDesign(Constants.SELECTED_TYRE.getDesign());
		dismountedTyre.setDesignDetails(Constants.SELECTED_TYRE
				.getDesignDetails());
		dismountedTyre.setSapMaterialCodeID(Constants.SELECTED_TYRE
				.getSapMaterialCodeID());
		dismountedTyre.setSapContractNumberID(Constants.SELECTED_TYRE
				.getSapContractNumberID());
		dismountedTyre.setSapVenderCodeID(Constants.SELECTED_TYRE
				.getSapVenderCodeID());
		dismountedTyre.setExternalID(Constants.SELECTED_TYRE.getExternalID());
		dismountedTyre.setNsk(Constants.SELECTED_TYRE.getNsk());

		dismountedTyre.setNsk2(Constants.SELECTED_TYRE.getNsk2());
		dismountedTyre.setNsk3(Constants.SELECTED_TYRE.getNsk3());
		dismountedTyre.setPosition(Constants.SELECTED_TYRE.getPosition());
		dismountedTyre.setOriginalPosition(Constants.SELECTED_TYRE
				.getPosition());
		dismountedTyre.setPressure(Constants.SELECTED_TYRE.getPressure());
		dismountedTyre.setIsSpare(Constants.SELECTED_TYRE.getIsSpare());
		dismountedTyre.setRegrooveInfo(Constants.SELECTED_TYRE
				.getRegrooveInfo());
		dismountedTyre.setReGrooved(Constants.SELECTED_TYRE.isReGrooved());
		dismountedTyre.setRetorqued(Constants.SELECTED_TYRE.isRetorqued());
		dismountedTyre.setIsphysicalySwaped(Constants.SELECTED_TYRE
				.isIsphysicalySwaped());
		dismountedTyre.setLogicalySwaped(Constants.SELECTED_TYRE
				.isLogicalySwaped());

		dismountedTyre.setDismounted(Constants.SELECTED_TYRE.isDismounted());
		dismountedTyre.setHasTurnedOnRim(Constants.SELECTED_TYRE
				.hasTurnedOnRim());
		dismountedTyre.setWorkedOn(Constants.SELECTED_TYRE.isWorkedOn());
		dismountedTyre.setStatus(Constants.SELECTED_TYRE.getStatus());
		dismountedTyre.setActive(Constants.SELECTED_TYRE.getActive());
		dismountedTyre.setShared(Constants.SELECTED_TYRE.getShared());
		dismountedTyre.setFromJobId(Constants.SELECTED_TYRE.getFromJobId());
		dismountedTyre.setSapCodeAXID(Constants.SELECTED_TYRE.getSapCodeAXID());
		dismountedTyre.setOrignalNSK(Constants.SELECTED_TYRE.getOrignalNSK());
		dismountedTyre.setPWTMounted(Constants.SELECTED_TYRE.isPWTMounted());

		dismountedTyre.setSapCodeVeh(Constants.SELECTED_TYRE.getSapCodeVeh());
		dismountedTyre.setTorInfo(Constants.SELECTED_TYRE.getTorInfo());
		dismountedTyre.setLogicalSwapinfo(Constants.SELECTED_TYRE
				.getLogicalSwapinfo());
		dismountedTyre.setPhysicalSwapinfo(Constants.SELECTED_TYRE
				.getPhysicalSwapinfo());
		dismountedTyre.setTyreState(Constants.SELECTED_TYRE.getTyreState());
		dismountedTyre.setAsp(Constants.SELECTED_TYRE.getAsp());
		dismountedTyre.setVehicleJob(Constants.SELECTED_TYRE.getVehicleJob());
		/**
		 * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
		 * Maintain temporary SAPVENDERCodeID until SAP updates
		 * Since SAPVENDERCodeID for few tyres are null, so by maintaining temporary SAPVENDERCodeID, will make PWT tyre with SAPVENDERCodeID as null, available for Reserve PWT.
		 * To retain TempSAPVendorCodeID Info
		 */
		dismountedTyre.setTempSAPVendorCodeID(Constants.SELECTED_TYRE.getTempSAPVendorCodeID());
	}

	/**
	 * Method to get NSK values from tyre table
	 */
	private String[] getTyreNSK(String tyreid) {
		String[] nskList = null;
		Cursor nskCursor = null;
		try {
			mDbHelper.open();
			nskCursor = mDbHelper.getTyreNSK(tyreid);
			if (CursorUtils.isValidCursor(nskCursor)) {
				nskCursor.moveToFirst();
				if (nskCursor.getCount() != 0) {
					nskList = new String[3];
					nskList[0] = nskCursor.getString(nskCursor
							.getColumnIndex("NSK1"));
					nskList[1] = nskCursor.getString(nskCursor
							.getColumnIndex("NSK2"));
					nskList[2] = nskCursor.getString(nskCursor
							.getColumnIndex("NSK3"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(nskCursor);
			mDbHelper.close();
		}
		return nskList;
	}

	/**
	 * Method to get NSK values from tyre table
	 */
	private String getOriginalNSKByTyreId(String tyreid) {
		String originalNSK = "";
		Cursor nskCursor = null;
		try {
			mDbHelper.open();
			nskCursor = mDbHelper.getTyreOriginalNSK(tyreid);
			if (CursorUtils.isValidCursor(nskCursor)) {
				nskCursor.moveToFirst();
				if (nskCursor.getCount() != 0) {
					originalNSK = nskCursor.getString(nskCursor
							.getColumnIndex("OriginalNSK"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(nskCursor);
			mDbHelper.close();
		}
		return originalNSK;
	}

	/**
	 * Method to load tire details from tire table
	 */
	private void loadTyreFixedDetails() {
		Cursor mCursor = null;
		try {
			mDbHelper.open();
			mCursor = mDbHelper.getPWTTyreDetailsByTyreId(tyreIdFromIntent);
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				selectedBrandName = mCursor.getString(mCursor
						.getColumnIndex("(Select Description from TyreBrand "
								+ "where ID" + " = IDTireBrand)"));
				selectedtyreSize = mCursor.getString(mCursor
						.getColumnIndex("FSize"));
				selectedTyreTASP = mCursor.getString(mCursor
						.getColumnIndex("AspectRatio"));
				selectedtyreTRIM = mCursor.getString(mCursor
						.getColumnIndex("Diameter"));
				selectedTyreDesign = mCursor.getString(mCursor
						.getColumnIndex("Deseign"));
				selectedTyreDetailedDesign = mCursor.getString(mCursor
						.getColumnIndex("FullTireDetails"));
				CursorUtils.closeCursor(mCursor);
			}
			loadSelectedBrand();
			loadSelectedSize();
			loadSelectedASP();
			loadSelectedRIM();
			loadSelectedDesign();
			loadSelectedDesignDetails();
			mExternalIdTyre = mDbHelper
					.getExternalIdFromSerialByTyreId(tyreIdFromIntent);
			mSAPCodeTiTyre = mDbHelper
					.getSAPCodeTIIdFromSerialByTyreId(tyreIdFromIntent);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method loading Brand Name on UI element as per the selected tire
	 */
	private void loadSelectedBrand() {
		brandArrayList.clear();
		brandArrayList.add(selectedBrandName);
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, brandArrayList);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		brandSpinner.setAdapter(dataAdapter);
		brandSpinner.setEnabled(false);
	}
	/**
	 * Method loading tire-size on UI element as per the selected tire
	 */
	private void loadSelectedSize() {
		sizeArrayList.clear();
		sizeArrayList.add(selectedtyreSize);
		sizeDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, sizeArrayList);
		sizeDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sizeSpinner.setAdapter(sizeDataAdapter);
		sizeSpinner.setEnabled(false);
	}
	/**
	 * Method loading tire-ASP on UI element as per the selected tire
	 */
	private void loadSelectedASP() {
		tyreTASPArrayList.clear();
		tyreTASPArrayList.add(selectedTyreTASP);
		aspDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, tyreTASPArrayList);
		aspDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tyreTASPSpinner.setAdapter(aspDataAdapter);
		tyreTASPSpinner.setEnabled(false);
	}
	/**
	 * Method loading tire-RIM on UI element as per the selected tire
	 */
	private void loadSelectedRIM() {
		tyreTRIMArrayList.clear();
		tyreTRIMArrayList.add(selectedtyreTRIM);
		rimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, tyreTRIMArrayList);
		rimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tyreTRIMSpinner.setAdapter(rimDataAdapter);
		tyreTRIMSpinner.setEnabled(false);
	}
	/**
	 * Method loading tire-Design on UI element as per the selected tire
	 */
	private void loadSelectedDesign() {
		designArrayList.clear();
		designArrayList.add(selectedTyreDesign);
		designDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, designArrayList);
		designDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tyreDesignSpinner.setAdapter(designDataAdapter);
		tyreDesignSpinner.setEnabled(false);
	}
	/**
	 * Method loading tire-Design-Details on UI element as per the selected tire
	 */
	private void loadSelectedDesignDetails() {
		fullDesignArrayList.clear();
		fullDesignArrayList.add(selectedTyreDetailedDesign);
		fullDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, fullDesignArrayList);
		fullDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tyreFullDetailSpinner.setAdapter(fullDesignDataAdapter);
		tyreFullDetailSpinner.setEnabled(false);
	}

	/**
	 * Method to load rim type from dismounted tire
	 */
	private void loadRimType() {
		if (Constants.SELECTED_TYRE.getRimType() != null) {
			if (Constants.SELECTED_TYRE.getRimType().equals("2")) {
				rimValue.setChecked(true);
			} else {
				rimValue.setChecked(false);
			}
		} else {
			rimValue.setChecked(false);
		}
	}

	@Override
	protected void onStop() {
		LogUtil.i("BT",
				"****************** onStop *****************:mBTService: "
						+ mBTService);
		try {
			if (null != mBTService) {
				mBTService.stop();
				if (task != null && !task.isCancelled()) {
					mBTService.asyncCancel(true);
					task.cancel(true);
					task = null;
				}
			}
			if (null != mReceiver) {
				unregisterReceiver(mReceiver);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onStop();
	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	@Override
	protected void onStart() {
		// set this activity as the inactivity handler
		LogoutHandler.setCurrentActivity(this);
		mBTService = new BluetoothService(this);
		if (savedInstanceState != null) {
			boolean isWorkerThread = savedInstanceState
					.getBoolean("isworkerstopped");
			mBTService.setWorkerThread(isWorkerThread);
		}
		IntentFilter intentFilter = new IntentFilter("BLUETOOTH_SENDER");
		mReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.hasExtra("BT_DATA_NSK")) {
					String nsk_value = intent.getStringExtra("BT_DATA_NSK");
					if (nskCounter == 1) {
						valueNSK1.setText(nsk_value);
						valueNSK2.setText(nsk_value);
						valueNSK3.setText(nsk_value);
						nskCounter = 2;
					} else if (nskCounter == 2) {
						valueNSK2.setText(nsk_value);
						nskCounter = 3;
					} else if (nskCounter == 3) {
						nskCounter = 1;
						valueNSK3.setText(nsk_value);
					}
				} else if (intent.hasExtra("BT_DATA_PRE")) {
					String pre_value = intent.getStringExtra("BT_DATA_PRE");
					if (!mBlockPressureFromBluetooth) {
						if (isSettingsPSI) {
							String barValue;
							// change PSI to BAR
							barValue = CommonUtils.getPressureValue(context,
									CommonUtils.parseFloat(pre_value));
							setPressure.setText(String.valueOf(barValue));
						} else {
							setPressure.setText(pre_value);
						}
					}
				} else if (intent.hasExtra("BT_CONN_STATUS")) {
					Toast.makeText(getApplicationContext(),
							strbluetoothconnectionfalied, Toast.LENGTH_LONG)
							.show();
				} else if (intent.hasExtra("BT_STATUS_IS_DISCONNECTED")) {
					boolean isDisconnected = intent.getBooleanExtra(
							"BT_STATUS_IS_DISCONNECTED", false);
					LogUtil.i(
							"Bluetooth in my Operation",
							"Broadcast came to my oeration:: " + isDisconnected
									+ " mBTService: "
									+ mBTService.isConnected());
					valueNSK1.setEnabled(isDisconnected);
					valueNSK2.setEnabled(isDisconnected);
					valueNSK3.setEnabled(isDisconnected);
					if (!mBlockPressureFromBluetooth)
						setPressure.setEnabled(isDisconnected);
					if (isDisconnected) {
						mBTService.stop();
						initiateBT();
						// mBTService.beginListeningData(0);
					} else {
						LogUtil.i("TOR", "######## Makeing stopworker false ");
						// mBTService.resetSocket();
						mBTService.startReading();
						// mBTService.beginListeningData(0);
					}
				} else if (intent.hasExtra("BT_RETRY_FAIL")) {
					initiateBT();
				}
			}
		};
		registerReceiver(mReceiver, intentFilter);

		initiateBT();
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();

	}
	/**
	 * Method initializing Blue-tooth, checking for connectivity and performing
	 * data transfer while using blue-tooth probes.
	 */
	private void initiateBT() {
		int status = mBTService.getPairedStatus();
		switch (status) {
		case 0:
			Toast.makeText(getApplicationContext(),
					Constants.sLblMultiplePaired, Toast.LENGTH_LONG).show();
			break;
		case 1:
			task = new BTTask();
			task.execute();
			break;
		case 2:
			Toast.makeText(getApplicationContext(),
					Constants.sLblPleasePairTLogik, Toast.LENGTH_LONG).show();
			break;
		case 3:
			Toast.makeText(getApplicationContext(),
					Constants.sLblNoPairedDevices, Toast.LENGTH_LONG).show();
			break;
		}
	}
	/**
	 * class handling blue-tooth functionality in a separate non-UI threads and
	 * performing data transfer while using blue-tooth probes.
	 */
	class BTTask extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... f_url) {
			if (isCancelled()) {
				LogUtil.e("MountPWTActivity",
						"doInBackground Async Task Cancelled  > ");
				return "";
			}
			try {
				mBTService.connectToDevice(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if (isCancelled()) {
				LogUtil.e("MountPWTActivity",
						"onPostExecute Async Task Cancelled  > ");
				return;
			}
			try {
				if (mBTService.isConnected()) {
					valueNSK1.setEnabled(false);
					valueNSK2.setEnabled(false);
					valueNSK3.setEnabled(false);
					if (!mBlockPressureFromBluetooth)
						setPressure.setEnabled(false);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		// unregisterReceiver(mReceiver);
		// mBTService.stop();
	}
	/**
	 * Overridden method storing all the captured data inside bundle object in
	 * order to handle different orientations
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (brandSpinner.getCount() != 0) {
			outState.putString("BRAND", brandSpinner.getSelectedItem()
					.toString());
		}
		if (sizeSpinner.getCount() != 0) {
			outState.putString("SIZE", sizeSpinner.getSelectedItem().toString());
		}
		if (tyreTASPSpinner.getCount() != 0) {
			outState.putString("ASP", tyreTASPSpinner.getSelectedItem()
					.toString());
		}
		if (tyreTRIMSpinner.getCount() != 0) {
			outState.putString("RIM", tyreTRIMSpinner.getSelectedItem()
					.toString());
		}
		if (tyreDesignSpinner.getCount() != 0) {
			outState.putString("DESIGN", tyreDesignSpinner.getSelectedItem()
					.toString());
		}
		if (tyreFullDetailSpinner.getCount() != 0) {
			outState.putString("FULLDETAIL", tyreFullDetailSpinner
					.getSelectedItem().toString());
		}

		if (mBTService != null) {
			outState.putBoolean("isworkerstopped", mBTService.isWorkerThread());
		}

		outState.putString("SAVEDNSK1", (valueNSK1.getText().toString()));
		outState.putString("SAVEDNSK2", (valueNSK2.getText().toString()));
		outState.putString("SAVEDNSK3", (valueNSK3.getText().toString()));
		outState.putInt("nsk_counter", nskCounter);
		outState.putString("EXTERNALID", mExternalIdTyre);
		saveDialogState(outState);
		super.onSaveInstanceState(outState);
	}
	/**
	 * Loading Labels for UI from the Translation table of the local database
	 * file
	 */
	private void loadLabelsFromDB() {
		try {
			mDbHelper.open();
			String labelFromDB = mDbHelper.getLabel(418);
			lbl_serialNo_swap.setText(labelFromDB);
			setTitle(mDbHelper.getLabel(Constants.MOUNT_PWT_TIRE_LABEL));
			strbluetoothconnectionfalied = mDbHelper
					.getLabel(Constants.BLUETOOTH_CONNECTION_FAILED);

			mYesLabel = mDbHelper.getLabel(Constants.YES_LABEL);
			mNoLabel = mDbHelper.getLabel(Constants.NO_LABEL);
			mBackMsgLabel = mDbHelper.getLabel(Constants.CONFIRM_BACK);

			labelFromDB = mDbHelper.getLabel(Constants.NSK_LABEL);
			nskLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.PRESSURE_LABEL);
			pressureLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.DIMENSION_LABEL);
			dimensionLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.ASP_LABEL);
			aspLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.RIM_LABEL);
			rimLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(68);
			designLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.SIZE_LABEL);
			sizeLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(137);
			designTypeLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(391);
			typeLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.BRAND_LABEL);
			mBrandLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.JOB_DETAILS);
			mJobDetailsLabel.setText(labelFromDB);
			String tireDetailsLabel = mDbHelper.getLabel(Constants.LABEL_TIRE_DETAILS);
			mTireDetailsLabel.setText(tireDetailsLabel);
			
			pressureRequired = mDbHelper.getLabel(Constants.PRESSURE_REQUIRED);
			lblDesignCannotBeBlank = mDbHelper
					.getLabel(Constants.DESIGN_IS_REQUIRED);
			Constants.sLblCheckPressureValue = mDbHelper.getLabel(Constants.CHECK_PRESSURE_LABEL);
			mNotesLabel = mDbHelper.getLabel(Constants.NOTES_ACTIVITY_NAME);
			mDbHelper.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * TextWatcher for nsk1
	 */
	private class nskOneBeforeChanged implements TextWatcher {
		@Override
		public void afterTextChanged(Editable nskOne) {
			if (instanceState != null) {
				nsk23SetFlag++;
				if (nsk23SetFlag > 6) {
					savedNSK2 = nskOne.toString();
					savedNSK3 = nskOne.toString();
				}
			} else {
				savedNSK2 = nskOne.toString();
				savedNSK3 = nskOne.toString();
			}
			valueNSK2.setText(savedNSK2);
			valueNSK3.setText(savedNSK3);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = valueNSK1.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {
				} else {
					valueNSK1.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				valueNSK1.setText("");
			}

		}
	}
	/**
	 * TextWatcher for nsk2
	 */
	private class nskTwoBeforeChanged implements TextWatcher {

		@Override
		public void afterTextChanged(Editable nskTwo) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = valueNSK2.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {
				} else {
					valueNSK2.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				valueNSK2.setText("");
			}

		}
	}
	/**
	 * TextWatcher for nsk3
	 */
	private class nskThreeBeforeChanged implements TextWatcher {

		@Override
		public void afterTextChanged(Editable nskThree) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = valueNSK3.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {
				} else {
					valueNSK3.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				valueNSK3.setText("");
			}

		}
	}

	InputFilter nskFilter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence changedText, int arg1,
				int arg2, Spanned oldText, int arg4, int arg5) {
			if (!(oldText.toString() + changedText).equals("")) {
				try {
					double nskValue = CommonUtils.parseDouble(oldText
							.toString() + changedText);
					if (nskValue > 30) {
						return "";
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
			return null;
		}
	};
	/**
	 * Method Getting Tire SAPMaterialCodeID from local DB3 file as per the
	 * selected DetailedDesign
	 */
	public void getSAPMaterialCodeID() {
		Cursor mCursor = null;
		try {
			mDbHelper.open();
			mCursor = mDbHelper.getSAPMaterialCodeID(selectedtyreSize,
					selectedTyreTASP, selectedtyreTRIM, selectedTyreDesign,
					selectedTyreDetailedDesign, selectedBrandName);
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				selectedSAPMaterialCodeID = mCursor.getString(
						mCursor.getColumnIndex("ID")).toString();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method handling the data-updates for the tables(JobItem and tire) 
	 * for the selected tire. It checks all the mandatory fields then allows 
	 * user to navigate back to Vehicle Skeleton after updating the data
	 */
	
	public static void storeTyresInSharedPref(ArrayList<Tyre> tyres, Context context)
		{
			SharedPreferences prefs = context.getSharedPreferences(Constants.VALIDATE_USER, Context.MODE_PRIVATE);
	        Editor editor = prefs.edit();
			try {
				editor.putString(Constants.TYRES_PWT, ObjectSerializer.serialize(tyres));
			} catch (IOException e) {
				e.printStackTrace();
				Log.d("1",e.getMessage());
			}
		        editor.commit();
		}
		
	
	private void onReturnSkeleton() {
		Intent intentReturn = new Intent();
		/**
		 * CR : 505
		 */
		getTireBySerialNumberAndTyreID(serialNoFromIntent,tyreIdFromIntent);
		//Retrieve Reserved PWT tyre info from shared pref
		Constants.mPWTList = EjobList.retrieveMountPWTTyresFromSharedPref(context);
		int valid=1;
		for(int i=0;i<Constants.mPWTList.size();i++)
		{
			if(Constants.mPWTList.get(i).getExternalID().equals(mPWT.getExternalID()))
			{
				valid=0;
			}
		}
		if(valid==1)
		{
			Constants.mPWTList.add(mPWT);									
		}
		
		storeTyresInSharedPref(Constants.mPWTList,context);
		finalConvertedPressure = CommonUtils.getFinalConvertedPressureValue(
				recommendedPressure, setPressure.getText().toString(),
				isSettingsPSI);
		updatePressureValue();
		updateJobItem();
		Constants.SELECTED_TYRE.setSerialNumber(serialNoFromIntent);
		Constants.SELECTED_TYRE.setBrandName(brandSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setSize(sizeSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setAsp(tyreTASPSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setRim(tyreTRIMSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setDesign(tyreDesignSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setDesignDetails(tyreFullDetailSpinner
				.getSelectedItem().toString());
		Constants.SELECTED_TYRE.setNsk(valueNSK1.getText().toString());
		Constants.SELECTED_TYRE.setNsk2(valueNSK2.getText().toString());
		Constants.SELECTED_TYRE.setNsk3(valueNSK3.getText().toString());
		Constants.SELECTED_TYRE.setPressure(finalConvertedPressure);
		Constants.SELECTED_TYRE.setShared("0");
		Constants.SELECTED_TYRE.setFromJobId(null);
		Constants.SELECTED_TYRE.setVehicleJob(Constants.contract
				.getVehicleSapCode());

		Constants.SELECTED_TYRE.setStatus("2");
		getSAPMaterialCodeID();
		Constants.SELECTED_TYRE.setSapCodeTI(mSAPCodeTiTyre);
		Constants.SELECTED_TYRE.setSapMaterialCodeID(selectedSAPMaterialCodeID);
		// Corrected wrong implementation for fixing CR 505
				Constants.SELECTED_TYRE.setmIsUnmounted("0");
		/**
		 * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
		 * Maintain temporary SAPVENDERCodeID until SAP updates
		 * Since SAPVENDERCodeID for few tyres are null, so by maintaining temporary SAPVENDERCodeID, will make PWT tyre with SAPVENDERCodeID as null, available for Reserve PWT.
		 * Make TempSAPVendorCodeID Info as null for new tyre
		 */
		Constants.SELECTED_TYRE.setTempSAPVendorCodeID(null);

		//Condition aaded to check and update inspection data for the selected tire
			InspectionDataHandler.getMyInstance(getApplicationContext()).updateJobitemForInspection(Constants.SELECTED_TYRE,false);

		//User Trace logs
		try {
			String traceData;
			traceData = "\n\t\tTyre Details : " + Constants.SELECTED_TYRE.getBrandName() +
					"| " + Constants.SELECTED_TYRE.getSize() +
					"| " + Constants.SELECTED_TYRE.getAsp() +
					"| " + Constants.SELECTED_TYRE.getRim() +
					"| " + Constants.SELECTED_TYRE.getDesign() +
					"| " + Constants.SELECTED_TYRE.getDesignDetails() +


					"\n\t\tJob Details : " + Constants.SELECTED_TYRE.getNsk() +
					", " + Constants.SELECTED_TYRE.getNsk2() +
					", " + Constants.SELECTED_TYRE.getNsk3() +
					"| " +Constants.SELECTED_TYRE.getPressure();


			traceData = traceData+	"| " + Constants.SELECTED_TYRE.getRimType();
			LogUtil.TraceInfo(TRACE_TAG, "none","Data : " + traceData, false, false, false);
		}
		catch (Exception e)
		{
			LogUtil.TraceInfo(TRACE_TAG, "Data : Exception : ", e.getMessage(), false, true, false);
		}


		// close keyboard if open
		try {
			CommonUtils.hideKeyboard(this, getWindow().getDecorView()
					.getRootView().getWindowToken());
		} catch (Exception e) {
			LogUtil.i("Mount PWT on activity return",
					"could not close keyboard");
		}


		intentReturn.putExtra("mount_pwt", true);
		setResult(VehicleSkeletonFragment.MOUNT_PWT_INTENT, intentReturn);
		noteText = "";
		CameraUtility.resetParams();
		finish();
		dismountedTyre.setPosition(dismountedTyre.getPosition() + "$");
		VehicleSkeletonFragment.tyreInfo.add(dismountedTyre);
		MountPWTActivity.this.overridePendingTransition(R.anim.slide_left_in,
				R.anim.slide_left_out);
	}

	private String getRimType() {
		if (rimValue.isChecked()) {
			return "2"; // Alloy
		} else {
			return "1"; // Steel (Defaultvalue)
		}
	}
	/**
	 * Method Updating Data for the selected tire in JobItem table with
	 * the corresponding Mount-PWT ActionType
	 */
	private void updateJobItem() {
try {
			LogUtil.DBLog(TAG,"JobItem Data","Insertion Initialized");
		mDbHelper.open();
		int jobID = getJobItemIDForThisJobItem();
		external_id = String.valueOf(UUID.randomUUID());
		int len = VehicleSkeletonFragment.mJobItemList.size();
		JobItem jobItem = new JobItem();
		jobItem.setActionType("25");
		jobItem.setAxleNumber("0");
		jobItem.setAxleServiceType("0");
		jobItem.setCasingRouteCode("");
		jobItem.setDamageId("");
		jobItem.setExternalId(external_id);
		jobItem.setGrooveNumber(null);
		jobItem.setJobId(String.valueOf(jobID));
		jobItem.setMinNSK(minNSKSet());
		jobItem.setNote(noteText);
		jobItem.setNskOneAfter(valueNSK1.getText().toString());
		jobItem.setNskOneBefore("0");
		jobItem.setNskThreeAfter(valueNSK3.getText().toString());
		jobItem.setNskThreeBefore("0");
		jobItem.setNskTwoAfter(valueNSK2.getText().toString());
		jobItem.setNskTwoBefore("0");
		jobItem.setOperationID("");
		jobItem.setPressure(finalConvertedPressure);
		jobItem.setRecInflactionDestination("");
		jobItem.setRecInflactionOrignal("");
		jobItem.setRegrooved("False");
		jobItem.setReGrooveNumber(null);
		jobItem.setRegrooveType("0");
		jobItem.setRemovalReasonId("");
		jobItem.setRepairCompany(null);
		jobItem.setRimType(getRimType());
		jobItem.setSapCodeTilD("0");
		jobItem.setSequence(String.valueOf(len + 1));
		jobItem.setServiceCount("0");
		jobItem.setServiceID("0");
		jobItem.setSwapType("0");
		jobItem.setTorqueSettings("");
		jobItem.setTyreID(mExternalIdTyre);
		jobItem.setWorkOrderNumber(null);
		jobItem.setThreaddepth("0");
		if (recommendedPressure > 0) {
			jobItem.setRecInflactionDestination(String
					.valueOf(recommendedPressure));
		} else {
			jobItem.setRecInflactionDestination(finalConvertedPressure);
		}
		jobItem.setRecInflactionOrignal("0");
		LogUtil.DBLog(TAG,"Job Item Data","Inserted"+jobItem.toString() );
		VehicleSkeletonFragment.mJobItemList.add(jobItem);
		LogUtil.DBLog(TAG,"Job Item Data","Inserted");
		Constants.SELECTED_TYRE.setExternalID(mExternalIdTyre);
		noteText = "";
		updateTireImages(String.valueOf(jobID));
		mDbHelper.close();
		} catch (SQLException e) {
			LogUtil.DBLog(TAG,"Job Item Data","Exception--"+e.getMessage());
			e.printStackTrace();
		}
	}
	/**
	 * Method handling image capture and image data-updates for Tire Image table
	 * It upgrades the quality of captured images as well
	 */
	private void updateTireImages(String id) {
		if (Constants.sBitmapPhoto1 != null) {
			TireImageItem item1 = new TireImageItem();
			item1.setJobItemId(id);
			if (mCameraEnabled)
				item1.setImageType("0");
			else
				item1.setImageType("1");
			item1.setDamageDescription(Constants.DAMAGE_NOTES1);
			item1.setTyreImage(CameraUtility.getByteFromBitmap(Constants.sBitmapPhoto1,
					Constants.sBitmapPhoto1Quality));
			item1.setJobExternalId(external_id);
			VehicleSkeletonFragment.mTireImageItemList.add(item1);
		}
		if (Constants.sBitmapPhoto2 != null) {
			TireImageItem item2 = new TireImageItem();
			item2.setJobItemId(id);
			item2.setImageType("1");
			item2.setDamageDescription(Constants.DAMAGE_NOTES2);
			item2.setTyreImage(CameraUtility.getByteFromBitmap(Constants.sBitmapPhoto2,
					Constants.sBitmapPhoto2Quality));
			item2.setJobExternalId(external_id);
			VehicleSkeletonFragment.mTireImageItemList.add(item2);
		}
		if (Constants.sBitmapPhoto3 != null) {
			TireImageItem item3 = new TireImageItem();
			item3.setJobItemId(id);
			item3.setImageType("1");
			item3.setDamageDescription(Constants.DAMAGE_NOTES3);
			item3.setTyreImage(CameraUtility.getByteFromBitmap(Constants.sBitmapPhoto3,
					Constants.sBitmapPhoto3Quality));
			item3.setJobExternalId(external_id);
			VehicleSkeletonFragment.mTireImageItemList.add(item3);
		}
	}
	/**
	 * Method returning maximum count of Job present in the JobItem table
	 * @return: Max Job Count
	 */
	private int getJobItemIDForThisJobItem() {
		int countJobItemsNotPresent = 0;
		mDbHelper.open();
		int currentJobItemCountInDB = mDbHelper.getJobItemCount();
		Cursor cursor = null;
		try {
			cursor = mDbHelper.getJobItemValues();
			if (!CursorUtils.isValidCursor(cursor)) {
				return countJobItemsNotPresent + 1
						+ VehicleSkeletonFragment.mJobItemList.size();
			}
			for (JobItem jobItem : VehicleSkeletonFragment.mJobItemList) {
				boolean flag = false;
				for (boolean hasItem = cursor.moveToFirst(); hasItem; hasItem = cursor
						.moveToNext()) {
					if (jobItem.getExternalId().equals(
							cursor.getString(cursor
									.getColumnIndex("ExternalID")))) {
						countJobItemsNotPresent++;
						break;
					}
				}
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			mDbHelper.close();
			// CursorUtils.closeCursor(cursor);
		}
		return currentJobItemCountInDB - countJobItemsNotPresent
				+ VehicleSkeletonFragment.mJobItemList.size() + 1;
	}
	/**
	 * Method returning minimum value of NSK(thread-depth) among the three
	 * captured NSKs
	 * @return: Minimum value of NSK(thread-depth)
	 */
	private String minNSKSet() {
		float smallest;
		float a = CommonUtils.parseFloat(valueNSK1.getText().toString());
		float b = CommonUtils.parseFloat(valueNSK2.getText().toString());
		float c = CommonUtils.parseFloat(valueNSK3.getText().toString());
		if (a < b && a < c) {
			smallest = a;
		} else if (b < c && b < a) {
			smallest = b;
		} else {
			smallest = c;
		}
		return String.valueOf(smallest);
	}
	/**
	 * Method Capturing the dialog state before orientation change
	 */
	private void saveDialogState(Bundle state) {
		state.putBoolean("mBackAlertDialog",
				(mBackAlertDialog != null && mBackAlertDialog.isShowing()));
	}
	/**
	 * Method Capturing the dialog state after orientation change
	 */
	private void restoreDialogState(Bundle state) {
		if (state != null) {
			if (state.getBoolean("mBackAlertDialog")) {
				actionBackPressed();
			}
		}
	}

	@Override
	protected void onDestroy() {
		try {
			if(mDbHelper != null){
				mDbHelper.close();
			}
			if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
				mBackAlertDialog.dismiss();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		actionBackPressed();
	}
	/**
	 * Method handling the action performed where user presses the back button
	 * Alert Box created with two option: YES and NO
	 */
	private void actionBackPressed() {
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", false, true, false);
		LayoutInflater li = LayoutInflater.from(MountPWTActivity.this);
		View promptsView = li
				.inflate(R.layout.dialog_logout_confirmation, null);

		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
				MountPWTActivity.this);
		alertDialogBuilder.setView(promptsView);

		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
		infoTv.setText(mBackMsgLabel);

		alertDialogBuilder.setCancelable(false).setPositiveButton(mYesLabel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
						noteText = "";
						CameraUtility.resetParams();
						MountPWTActivity.this.finish();
					}
				});

		alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);
						dialog.cancel();
					}
				});

		mBackAlertDialog = alertDialogBuilder.create();
		mBackAlertDialog.show();
		mBackAlertDialog.setCanceledOnTouchOutside(false);
	}
	/**
	 * Method creating the action-bar menu Items
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.mount_new_tire_menu, menu);
		if (mNotesLabel != null) {
			menu.findItem(R.id.action_notes).setTitle(mNotesLabel);
		}
		return true;
	}
	/**
	 * Method handling the action performed on Action-bar menu
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_notes) {
			LogUtil.TraceInfo(TRACE_TAG, "Option", "Note", false, false, false);
			Intent startNotesActivity = new Intent(this, NoteActivity.class);
			startNotesActivity.putExtra("sentfrom", "regroove");
			startNotesActivity.putExtra("lastnote", noteText);
			startActivityForResult(startNotesActivity,
					MountPWTActivity.NOTES_INTENT);
		}
		if (id == R.id.action_tire) {
			LogUtil.TraceInfo(TRACE_TAG, "Option", "Tire Action", false, false, false);
			if (validateDetails()) {
				Intent startWOTActivity = new Intent(this, WOTActivity.class);
				startWOTActivity.putExtra("SERIAL_NO", serialNoFromIntent);
				startWOTActivity.putExtra("WHEEL_POS",
						selectedTire.getPosition());
				startWOTActivity.putExtra("FROM_MOUNT_NEW", false);
				startWOTActivity.putExtra("FROM_MOUNT_PWT", true);
				String minNsk = CommonUtils.getMinNskValue(valueNSK1.getText()
						.toString(), valueNSK2.getText().toString(), valueNSK3
						.getText().toString());
				String pressure = CommonUtils.getFinalConvertedPressureValue(
						recommendedPressure, setPressure.getText().toString(),
						isSettingsPSI);
				startWOTActivity.putExtra("MIN_NSK", minNsk);
				startWOTActivity.putExtra(
						VehicleSkeletonFragment.mAXLEPRESSURE,
						CommonUtils.parseFloat(pressure));
				//Fix : Bug 626 (users are reporting that tyre pressures are incorrectly applied to other axles)
				//Fix for Missing Axle Position Information
				startWOTActivity.putExtra(VehicleSkeletonFragment.mAXLEPRESSURE_POS,axlePosition);
				startWOTActivity.putExtra("SELECTED_TYRE_EXTERNAL_ID",
						mExternalIdTyre);
				startActivityForResult(startWOTActivity,
						MountPWTActivity.WOT_INTENT);
			}
		}
		return super.onOptionsItemSelected(item);
	}
	/**
	 * Method checking if all fields of the design page are selected
	 */
	private boolean validateDetails() {

		if (brandSpinner.getSelectedItemPosition() < 0) {
			CommonUtils.toastHandler(fieldIncorrect, this);
			return false;
		}
		String nsk1 = CommonUtils.getMinNskValue(
				valueNSK1.getText().toString(), valueNSK2.getText().toString(),
				valueNSK3.getText().toString());
		if (CommonUtils.parseFloat(nsk1) <= 0) {
			CommonUtils.toastHandler(fieldIncorrect, this);
			return false;
		}
		String pressure = setPressure.getText().toString();
		if (null == pressure || "".equals(pressure)) {
			CommonUtils.toastHandler(fieldIncorrect, this); // INVALID_PRESSURE_ENTRY
			return false;
		} else {
			float val = CommonUtils.parseFloat(pressure);
			if (val > 0.00) {
				return true;
			} else {
				CommonUtils.toastHandler(fieldIncorrect, this);
				return false;
			}
		}
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == MountPWTActivity.NOTES_INTENT) {
			if (data != null) {
				noteText = data.getExtras().getString("mNoteEditText");
			}
		} else if (requestCode == MountPWTActivity.WOT_INTENT) {
			if (data != null) {
				workedOnTire = data.getExtras().getBoolean("wot");
			}
		}
	}
	/**
	 * @author amitkumar.h 
	 * Class providing the animation when user swipes out
	 * from the activity It also handles the actions performed when use
	 * is swiping out from the activity after matching all the required
	 * conditions
	 */
	public class Swiper implements OnTouchListener, OnClickListener {
		float startX, startY;
		float endX, endY;
		int selectedPositionToDelete;
		ArrayAdapter<String> adapterList;
		ScrollView view;
		private Context ctx;
		public static final float MINIMUM_MOVEMENT_REQUIRED = 100;

		public Swiper(Context ctx, ScrollView view) {
			this.ctx = ctx;
			this.view = view;
			view.setOnClickListener(this);
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getActionMasked()) {
			case MotionEvent.ACTION_DOWN:
				swiped = false;
				startX = event.getX();
				startY = event.getY();
				break;
			case MotionEvent.ACTION_UP:
				break;

			case MotionEvent.ACTION_MOVE:
				endX = event.getX();
				endY = event.getY();
				if (Math.abs(endX - startX) > MINIMUM_MOVEMENT_REQUIRED
						&& !swiped) {
					swiped = true;
					Rect rect = new Rect();
					int childCount = view.getChildCount();
					int[] listViewCoords = new int[2];
					view.getLocationOnScreen(listViewCoords);
					int x = (int) event.getRawX() - listViewCoords[0];
					int y = (int) event.getRawY() - listViewCoords[1];
					View child;
					for (int i = 0; i < childCount; i++) {
						child = view.getChildAt(i);
						child.getHitRect(rect);
						if (rect.contains(x, y)) {
							if (valueNSK1.getText().toString().equals("")
									|| valueNSK2.getText().toString()
											.equals("")
									|| valueNSK3.getText().toString()
											.equals("")) {

								//User Trace logs
								if(Constants.sLblEnterNskValues!=null)
								{
									LogUtil.TraceInfo(TRACE_TAG, "NSK Validation","Msg : " +Constants.sLblEnterNskValues,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "NSK Validation","Msg : Check NSK",false,false,false);
								}

								Toast.makeText(ctx,
										Constants.sLblEnterNskValues,
										Toast.LENGTH_SHORT).show();
							} else if (CommonUtils.parseFloat(setPressure
									.getText().toString()) == 0.0) {

								//User Trace logs
								if(pressureRequired!=null)
								{
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Validation","Msg : " +pressureRequired,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Validation","Msg : Check Pressure",false,false,false);
								}
								CommonUtils.notify(pressureRequired,
										getBaseContext());
							} else if (setPressure.getText().toString()
									.equals("")) {

								//User Trace logs
								if(pressureRequired!=null)
								{
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Validation","Msg : " +pressureRequired,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Validation","Msg : Check Pressure",false,false,false);
								}

								CommonUtils.notify(pressureRequired,
										getBaseContext());
							} else if (!CommonUtils
									.pressureValueValidation(setPressure)) {

								//User Trace logs
								if(Constants.sLblCheckPressureValue!=null)
								{
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Value Validation","Msg : " +Constants.sLblCheckPressureValue,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Value Validation","Msg : Check Pressure Value",false,false,false);
								}

								CommonUtils.notify(
										Constants.sLblCheckPressureValue,
										getBaseContext());
							} else if (designArrayList.size() == 0
									|| fullDesignArrayList.size() == 0) {

								//User Trace logs
								if(lblDesignCannotBeBlank!=null)
								{
									LogUtil.TraceInfo(TRACE_TAG, "Design Array  List and Full Design Validation","Msg : " +lblDesignCannotBeBlank,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Design Array  List and Full Design Validation","Msg : Design Array  List and Full Design",false,false,false);
								}

								Toast.makeText(ctx, lblDesignCannotBeBlank,
										Toast.LENGTH_SHORT).show();
							} else {
								onReturnSkeleton();
							}
						}
					}
				}
				break;
			}
			return false;
		}

		@Override
		public void onClick(View arg0) {
		}
	}

	/**
	 * If axle have pressure apply the value then disable otherwise it's
	 * editable Setting the Pressure
	 */
	private void setValuetoPressueView() {
		if (recommendedPressure > 0) {
			setPressure.setText(CommonUtils.getPressureValue(
					MountPWTActivity.this, recommendedPressure));
			pressureUnit.setText(CommonUtils
					.getPressureUnit(MountPWTActivity.this));
			setPressure.setEnabled(false);
			mBlockPressureFromBluetooth = true;
		} else {
			if (isSettingsPSI) {
				setPressure
						.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
								4, 2) });
			} else {
				setPressure
						.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
								3, 2) });
			}
			/*Bug 728 : Adding context to show toast for higher pressure value*/
			setPressure.addTextChangedListener(new PressureCheckListener(
					setPressure, isSettingsPSI, getApplicationContext()));
			pressureUnit.setText(CommonUtils
					.getPressureUnit(MountPWTActivity.this));
		}
	}

	/**
	 * Method Updating recommended axle pressure for the selected axle
	 */
	private void updatePressureValue() {
		if (setPressure.isEnabled()
				&& (null != VehicleSkeletonFragment.mAxleRecommendedPressure)) {
			VehicleSkeletonFragment.mAxleRecommendedPressure.set(axlePosition,
					CommonUtils.parseFloat(finalConvertedPressure));
		}
	}

	/*
	 * (non-Javadoc)
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		noteText = "";
		InactivityUtils.logoutFromActivityAboveEjobForm(this);
	}

	/**
	 * get tire by tire SerialNumber and TyreID
	 * Fetching data from the part worn tire 
	 * @param mSerialNumber
	 * SerialNumber of the tire
	 * Method name changed FROM getTireBySerialNumber TO getTireBySerialNumberAndTyreID in 1.3.4.5
	 * @return tire object if tire found, otherwise null
	 */
	private void getTireBySerialNumberAndTyreID(String mSerialNumber,String mTyreID) {
		Cursor mCursor = null;
		try {
			mDbHelper.open();
			mCursor = mDbHelper.getTyreBySerialNumberAndTyreID(mSerialNumber,mTyreID);
			if (CursorUtils.isValidCursor(mCursor)) {
				mPWT = new Tyre();
				mPWT.setShared(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("Shared"))));
				mPWT.setStatus(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("Status"))));
				mPWT.setSapCodeTI(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("SAPCodeTi"))));
				mPWT.setSapMaterialCodeID(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("SAPMaterialCodeID"))));
				mPWT.setFromJobId(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("FromJobID"))));
				mPWT.setVehicleJob(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("idVehicleJob"))));
				
				
				mPWT.setSerialNumber(mCursor.getString(mCursor
						.getColumnIndex("SerialNumber")));
				mPWT.setExternalID(mCursor.getString(mCursor
						.getColumnIndex("ExternalID")));
				mPWT.setmIsUnmounted(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("IsUnmounted"))));
				mPWT.setIsSpare(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("IsSpare"))));
				mPWT.setSapContractNumberID(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("SAPContractNumberID"))));
				mPWT.setSapVenderCodeID(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("SAPVendorCodeID"))));
				mPWT.setSapCodeWP(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("SAPCodeWp"))));
				mPWT.setOrignalNSK(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("OriginalNSK"))));
				mPWT.setSapCodeAXID(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("SAPCodeAxID"))));
				mPWT.setSapCodeVeh(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("SAPCodeVeh"))));
				
				mPWT.setActive(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("Active"))));
				mPWT.setNsk(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("NSK1"))));
				mPWT.setNsk2(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("NSK2"))));
				mPWT.setNsk3(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("NSK3"))));
				mPWT.setSapCodeVeh(String.valueOf(mCursor.getInt(mCursor
						.getColumnIndex("SAPCodeVeh"))));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			mDbHelper.close();
			CursorUtils.closeCursor(mCursor);
		}
	
	}
}

