package com.goodyear.ejob.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.IBinder;

import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.Constants;

/**
 * Inactivity service, this is started with the launch of ejob list
 * 
 */
public class UserInactivityService extends Service {
	private static final String TAG = UserInactivityService.class
			.getSimpleName();
	private static CountDownTimer mInActivityCounter;
	private final long COUNT_DOWN_TIME = 90 * 60 * 1000;
	private final long INTERVAL = 1000;
	private boolean shouldStop = false;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Service#onBind(android.content.Intent)
	 */
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public static void updateTimer() {
		if (UserInactivityService.mInActivityCounter != null) {
			LogUtil.d(TAG, ":::User activitiy updated:::");
			UserInactivityService.mInActivityCounter.cancel();
			UserInactivityService.mInActivityCounter.start();
		}
	}

	public static void stopTimer() {
		if (UserInactivityService.mInActivityCounter != null) {
			LogUtil.d(TAG, ":::user activity check stoped:::");
			UserInactivityService.mInActivityCounter.cancel();
		}
	}

	@Override
	public void onCreate() {
		shouldStop = false;
		mInActivityCounter = new CountDownTimer(COUNT_DOWN_TIME, INTERVAL) {

			@Override
			public void onTick(long millisUntilFinished) {
				/*if(millisUntilFinished/1000 == 5280){
					SharedPreferences.Editor editor = sharedPreference.edit();
					editor.putBoolean(Constants.AUTO_LOGOUT_KEY+"_"+Constants.VALIDATE_USER,true);
					editor.commit();
					Intent intent = new Intent();
					LogUtil.d(TAG, " Inactive for 88 minutes, sending broadcast");
					intent.setAction(LogoutHandler.INACTIVITY_88MIN_INTENT);
					sendBroadcast(intent);
				}*/
			}

			@Override
			public void onFinish() {
				if (!shouldStop) {
					Intent intent = new Intent();
					intent.setAction(LogoutHandler.INACTIVITY_INTENT);
					sendBroadcast(intent);
					UserInactivityService.this.stopSelf();
				}
				else{
					UserInactivityService.this.stopSelf();
				}
			}
		};
		mInActivityCounter.start();
	}

	@Override
	public void onDestroy() {
		shouldStop = true;
		super.onDestroy();
		stopSelf();
	}
}
