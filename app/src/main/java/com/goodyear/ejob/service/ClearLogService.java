package com.goodyear.ejob.service;

import android.app.IntentService;
import android.content.Intent;

import com.goodyear.ejob.util.LogManager;

/**
 * @author amitkumar.h
 *
 */
public class ClearLogService extends IntentService {

	/**
	 * @param name
	 */
	public ClearLogService() {
		super("Clear Log");

	}
	
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.IntentService#onHandleIntent(android.content.Intent)
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		//Instead of Seven days log clear time, we kept size limit for logs
		//LogManager.clearLogs();

		//To clear Trace Information
		LogManager.clearTraceInfo();
	}

}
