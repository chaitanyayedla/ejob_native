/**copyright message*/
package com.goodyear.ejob.service;

import android.os.Handler;
import android.os.Message;

import com.goodyear.ejob.service.SyncBaseModel.SyncCallback;
import com.goodyear.ejob.util.LogUtil;

/**
 * Handler Class used to send the updates to UI.
 * 
 * @author Nilesh.Pawate
 * @see EjobSyncService
 */
public class MessageHandler extends Handler {
	/**
	 * Class TAG
	 */
	private static final String TAG = MessageHandler.class.getSimpleName();
	/**
	 * Callback
	 */
	private SyncCallback syncCallBack;

	/**
	 * Constructor.
	 * 
	 * @param syncCallBack
	 *            SyncCallback
	 */
	public MessageHandler(SyncCallback syncCallBack) {
		this.syncCallBack = syncCallBack;
	}

	/**
	 * @param syncCallBack
	 *            the syncCallBack to set
	 */
	public void setSyncCallBack(SyncCallback syncCallBack) {
		this.syncCallBack = syncCallBack;
	}

	@Override
	public void handleMessage(Message message) {
		Object object = message.obj;
		if (object instanceof ResponseMessage) {
			ResponseMessage mResponseMessage = (ResponseMessage) message.obj;
			LogUtil.i(TAG,
					"Received a message from Sync Model: sending to the activity:: "
							+ syncCallBack);
			if (syncCallBack != null) {
				LogUtil.i(TAG,
						"Before call back");
				syncCallBack.onSyncComplete(mResponseMessage);
				LogUtil.i(TAG,
						"After call back");
			}
		}
	}
}