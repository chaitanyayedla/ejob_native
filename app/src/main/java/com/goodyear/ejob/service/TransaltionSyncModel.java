/**copyright message*/
package com.goodyear.ejob.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;

import android.content.Context;
import android.os.Environment;
import android.os.Messenger;

import com.goodyear.ejob.dbhelpers.DataBaseHandler;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.sync.AuthSync;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.FileOperations;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.SettingsLoader;

/**
 * Class used to handle the Translation Sync. <br>
 * This should be called from service or thread. Once the sync is complete the
 * control will be passed to the UI with the help of Handler. <br>
 * You will be getting {@link SyncCallback#onSyncComplete(ResponseMessage)}
 * callback with proper message.
 * 
 * @author Nilesh.Pawate
 * @see SyncBaseModel#performSync(Context, int, String, Messenger)
 * @see SyncBaseModel#onSyncPostExecute(Context, int, String, Messenger)
 * @see SyncBaseModel#onSyncPreExecute(Context, int, String, Messenger)
 */
public class TransaltionSyncModel extends SyncBaseModel {

	private static final String TAG = TransaltionSyncModel.class
			.getSimpleName();

	@Override
	protected void onSyncPreExecute(Context context, int syncOptions,
			String lang, Messenger messageHandler) throws IOException {
		DataBaseHandler handler = new DataBaseHandler(context);
		handler.openDB();
		handler.createTablesInDB();
		LogUtil.e(TAG, "onSyncPreExecute lang::" + lang);
		handler.updateSession("", "", lang);
		handler.copyDataBase();
		handler.deleteDatabaseAfterLoad();
		handler.closeDB();
	}

	@Override
	protected void performSync(Context context, int syncOptions, String lang,
			Messenger messageHandler) {
		if (context == null) {
			LogUtil.e(TAG, "performSync context is null so returning");
			return;
		}
		mResponseMessage = new ResponseMessage();
		mResponseMessage.syncOptionsModel(syncOptions);
		try {
			onSyncPreExecute(context, syncOptions, lang, messageHandler);
		} catch (IOException e1) {
			LogUtil.e(TAG, "db files deleted");
			mResponseMessage.setMessage("error DB_DELETED");
			e1.printStackTrace();
			sendHandlerMessage(messageHandler, mResponseMessage);
			return;
		}
		LogUtil.e(TAG, "performSync : lang::" + lang);
		try {
			AuthSync.translationSync(context);
			mResponseMessage.setMessage(lang);
		} catch (FileNotFoundException e) {
			LogUtil.e(TAG, "server not found " + e.getMessage());
			LogUtil.i(TAG, "error Download sycning -- performsync --");
			LogUtil.pst(e);
			mResponseMessage.setMessage("error server_down");
		} catch (SocketTimeoutException e) {
			LogUtil.e(TAG, "socket timed out" + e.getMessage());
			LogUtil.i(TAG, "error SocketTimeoutException sycning -- performsync --");
			LogUtil.pst(e);
			e.printStackTrace();
			mResponseMessage.setMessage("error network");
		} catch (Exception e) {
			LogUtil.e(TAG, "network not working" + e.getMessage());
			LogUtil.i(TAG, "error Exception sycning -- performsync --");
			LogUtil.pst(e);
			mResponseMessage.setMessage("error network");
		}
		onSyncPostExecute(context, syncOptions, lang, messageHandler);
	}

	@Override
	protected void onSyncPostExecute(Context context, int syncOptions,
			String lang, Messenger messageHandler) {
		LogUtil.e(TAG, "onSyncPostExecute " + lang);
		DatabaseAdapter copy_temp = new DatabaseAdapter(context);
		String errorMessage = mResponseMessage.getMessage();
		if (!errorMessage.contains("error")) {
			try {
				// open db, clean it and then reopen
				copy_temp.open();
				if (!copy_temp.checkIfDataRecieved(Constants.PROJECT_PATH + "/"
						+ Constants.USER + "_.db3", lang)) {
					LogUtil.e(TAG, "no data in reply");
					FileOperations.cleanUp();
					mResponseMessage.setMessage("error network");
					sendHandlerMessage(messageHandler, mResponseMessage);
					return;
				}
				copy_temp.updateTranslationDBTables(Constants.PROJECT_PATH
						+ "/" + Constants.USER + "_.db3");
				copy_temp.copyFinalDB();
				copy_temp.deleteDB();
				copy_temp.close();

				copy_temp.createDatabase();
				copy_temp.open();
				copy_temp.updateSyncTime(DateTimeUTC
						.getMillisecondsFromUTCdate());
				copy_temp.updateSelectedLanguage(lang);
				// update last user in settings
				HashMap<String, String> langUpdate = new HashMap<>();
				langUpdate.put("Language",
						Constants.mapLanguageToSettings(lang));
				SettingsLoader.updateSettings(langUpdate, context);
			} catch (Exception e) {
				LogUtil.e(TAG, "Exception at :: onSyncPostExecute  ");
				e.printStackTrace();
			} finally {
				LogUtil.e(TAG, "Finally block ");
				copy_temp.close();
				FileOperations.cleanUp();
			}
		} else {
			FileOperations.cleanUp();
		}
		LogUtil.i(TAG, "Sending message to UI");
		sendHandlerMessage(messageHandler, mResponseMessage);
	}
}
