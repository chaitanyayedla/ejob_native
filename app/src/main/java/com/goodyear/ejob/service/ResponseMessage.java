/**copyright message*/
package com.goodyear.ejob.service;

/**
 * Class used to create the Response message which will be passed to UI from
 * Service.
 * 
 * @author Nilesh.Pawate
 * @see DownloadSyncModel
 */
public class ResponseMessage {
	/**
	 * Message text that will be passed to the callback.
	 */
	private String mMessage = "";
	/**
	 * Sync Options that will be taken from {@link SyncUtils.SyncOptions}
	 */
	private int mSyncOption = -1;

	/**
	 * @return Message as string
	 */
	public String getMessage() {
		return mMessage;
	}

	/**
	 * @return the syncOptions
	 */
	public int getSyncOption() {
		return mSyncOption;
	}

	/**
	 * API used to set the message which will be set from the Sync Module. and
	 * can be used at UI level.
	 * 
	 * @param message
	 *            Message
	 */
	public void setMessage(String message) {
		mMessage = message;
	}

	/**
	 * API to set the SyncOption module no.
	 * 
	 * @param syncOptions
	 *            module no.
	 */
	public void syncOptionsModel(int syncOptions) {
		mSyncOption = syncOptions;
	}

	@Override
	public String toString() {
		return "mSyncOption: " + mSyncOption + " :mMessage: " + mMessage;
	}
}
