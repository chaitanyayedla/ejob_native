/**copyright message*/
package com.goodyear.ejob.service;

import java.io.IOException;

import android.content.Context;
import android.database.SQLException;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.goodyear.ejob.service.SyncUtils.SyncOptions;
import com.goodyear.ejob.util.LogUtil;

/**
 * Base class for all the sync module type.
 * 
 * @author Nilesh.Pawate
 * @see MessageHandler
 * @see EjobSyncService
 * 
 */
public abstract class SyncBaseModel {
	/**
	 * Used to generate the message which will be sent to the UI through
	 * MessageHandler.
	 */
	protected ResponseMessage mResponseMessage;

	/**
	 * Before perform sync,this method is called. In this method snapshot File is created.
	 * 
	 * @param context
	 *            {@link Context}
	 * @param syncOptions
	 *            #SyncOptions
	 * @param lang
	 *            Selected language for Translation Sync, if no language pass ""
	 *            or null.
	 * @param messageHandler
	 *            Messenger
	 * @see SyncOptions
	 */
	protected abstract void onSyncPreExecute(Context context, int syncOptions,
			String lang, Messenger messageHandler) throws SQLException,
			IOException;

	/**
	 * In this Method pack file is created and request to server is made via internet http connection and then server will response with server file 
	 * 
	 * @param context
	 *            {@link Context}
	 * @param syncOptions
	 *            #SyncOptions
	 * @param lang
	 *            Selected language for Translation Sync, if no language pass ""
	 *            or null.
	 * @param messageHandler
	 *            Messenger
	 * @see SyncOptions <br>
	 *      onSyncPreExecute</br>
	 */

	protected abstract void performSync(Context context, int syncOptions,
			String lang, Messenger messageHandler);

	/**
	 * After perform sync, this method is called.
	 * In this method, database will be updated with server response.
	 * 
	 * @param context
	 *            {@link Context}
	 * @param syncOptions
	 *            #SyncOptions
	 * @param lang
	 *            Selected language for Translation Sync, if no language pass ""
	 *            or null.
	 * @param messageHandler
	 *            Messenger
	 * @see SyncOptions <br>
	 *      onSyncPreExecute</br>
	 */
	protected abstract void onSyncPostExecute(Context context, int syncOptions,
			String lang, Messenger messageHandler);

	/**
	 * API used to send the message to #MessageHandler
	 * 
	 * @param messageHandler
	 *            Messenger
	 * @param object
	 *            Object to be passed to UI.
	 * @see SyncCallback
	 */
	protected void sendHandlerMessage(Messenger messageHandler, Object object) {
		Message message = Message.obtain();
		message.obj = object;
		try {
			messageHandler.send(message);
		} catch (RemoteException e) {
			e.printStackTrace();
		}catch (Exception ex) {
			LogUtil.i("SyncBaseModel", "sendHandlerMessage : "+ex.getMessage());
			ex.printStackTrace();
		}
	}

	/**
	 * Callback for results. Implement this interface to get the results back.
	 */
	public interface SyncCallback {
		public void onCanceled(int currentId);

		/**
		 * API used to receive the call back from MessageHandler.
		 * 
		 * @param baseModel
		 *            ResponseMessage
		 */
		public void onSyncComplete(ResponseMessage baseModel);
	}
}
