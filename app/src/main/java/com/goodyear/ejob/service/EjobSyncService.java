/**copyright message*/
package com.goodyear.ejob.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Messenger;

import com.goodyear.ejob.util.LogUtil;

/**
 * IntentService used to download, upload, and translate modules which will be
 * create at {@link SyncUtils#createSyncModule(int)}.
 * 
 * @author Nilesh.Pawate
 * @see SyncUtils
 */
public class EjobSyncService extends IntentService {
	/**
	 * Class TAG
	 */
	private static final String TAG = EjobSyncService.class.getSimpleName();
	/**
	 * Messenger used to communicate with UI. With the help of
	 * {@link MessageHandler}
	 */
	private Messenger messageHandler;

	/**
	 * Constructor
	 */
	public EjobSyncService() {
		super(TAG);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null) {
			Bundle bundle = intent.getExtras();
			messageHandler = (Messenger) bundle.get("MESSENGER");
			LogUtil.i(TAG, "onStartCommand:: " + " messageHandler: "
					+ messageHandler);
			new Thread(new ServerConnector(bundle)).start(); // Server thread
		}
		return START_STICKY;
	}

	/**
	 * Class used to run in background.
	 */
	private class ServerConnector implements Runnable {
		Bundle bundle;

		public ServerConnector(Bundle bundle) {
			this.bundle = bundle;
		}

		@Override
		public void run() {
			syncImpl(bundle);
		}
	}

	/**
	 * API used to create modules to be synced.
	 * 
	 * @see SyncUtils#createSyncModule(int)
	 */
	public void syncImpl(Bundle bundle) {
		if (bundle != null) {
			int syncOptions = bundle.getInt(SyncUtils.SYNC_OPTIONS_KEY);
			LogUtil.d("EjobSyncService", "syncOptions: " + syncOptions);
			String lang = "";
			if (bundle.containsKey(SyncUtils.SYNC_LANG_KEY)) {
				lang = bundle.getString(SyncUtils.SYNC_LANG_KEY);
			}
			SyncBaseModel baseModel = SyncUtils.createSyncModule(syncOptions);
			LogUtil.d("EjobSyncService", "lang: " + lang + " Created Sync Model::"
					+ baseModel);
			baseModel.performSync(this.getApplicationContext(), syncOptions,
					lang, messageHandler);
		}
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		LogUtil.i(TAG, "onHandleIntent:: ");
	}
}
