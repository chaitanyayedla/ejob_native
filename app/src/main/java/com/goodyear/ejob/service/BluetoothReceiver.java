/**copyright message*/
package com.goodyear.ejob.service;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.goodyear.ejob.DismountTireActivity;
import com.goodyear.ejob.RegrooveTireOperation;
import com.goodyear.ejob.TurnOnRim;
import com.goodyear.ejob.blutooth.BluetoothService;
import com.goodyear.ejob.util.LogUtil;

/**
 * BroadCase receiver for Blue-tooth Probe Connection. <br>
 * </br>Send the device connected/ disconnected status to Operations screens
 * like {@link TurnOnRim}, {@link DismountTireActivity} ,
 * {@link RegrooveTireOperation} based on the value NSK values will enable/
 * disabled. These filters declared at manifest level. <br>
 * <br>
 * This broadcast Receive is registered with Action
 * {@link BluetoothDevice.ACTION_ACL_CONNECTED} and
 * {@link BluetoothDevice.ACTION_ACL_DISCONNECTED} at manifest file.
 * 
 * @see BluetoothService
 * @author Nilesh.Pawate
 */
public class BluetoothReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		BluetoothDevice device = intent
				.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		LogUtil.d("BluetoothReceiver",
				"Received a Broadcast:: Connected Device is::: " + device);
		if (device.getName().startsWith("TLOGIK")
				|| device.getName().startsWith("Pneu Logic")) {
			LogUtil.d("BluetoothReceiver", "Inside Bluetooth Receiver::::: ");
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				// Device found
			} else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
				// Device is now connected
				sendToActivity(context, false);
			} else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
					.equals(action)) {
				// Done searching
			} else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED
					.equals(action)) {
				// Device is about to disconnect
			} else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
				sendToActivity(context, true);
			}
		}
	}

	/**
	 * API used to send the broadcast to the activity which has the
	 * BLUETOOTH_SENDER receiver.<br>
	 * The remote receiver should receive the broadcast by
	 * BT_STATUS_IS_DISCONNECTED.
	 * 
	 * @param context
	 *            Activity Context
	 * @param isDisConnected
	 *            true if Disconnected, false otherwise.
	 */
	private void sendToActivity(Context context, boolean isDisConnected) {
		LogUtil.d("BluetoothReceiver",
				"Trying to Send Broadcast to isDisConnected : "
						+ isDisConnected);
		Intent intent = new Intent("BLUETOOTH_SENDER");
		intent.putExtra("BT_STATUS_IS_DISCONNECTED", isDisConnected);
		context.sendBroadcast(intent);
	}
}
