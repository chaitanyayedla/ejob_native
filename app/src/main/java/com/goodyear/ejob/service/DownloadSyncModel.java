/**copyright message*/
package com.goodyear.ejob.service;

import android.content.Context;
import android.database.SQLException;
import android.os.Messenger;
import android.util.Log;

import com.goodyear.ejob.dbhelpers.DataBaseHandler;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.halosys.HalosysCallStatus;
import com.goodyear.ejob.halosys.HalosysServiceCall;
import com.goodyear.ejob.halosys.HalosysStatusModel;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.FileOperations;
import com.goodyear.ejob.util.LogUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketTimeoutException;

/**
 * Class used to handle the Download Sync. This should be called from service or
 * thread. Once the sync is complete the control will be passed to the UI with
 * the help of Handler.
 *
 * @author Nilesh.Pawate.
 * @see UploadSyncModel
 * @see TransaltionSyncModel
 * @see SyncBaseModel#performSync(Context, int, String, Messenger)
 * @see SyncBaseModel#onSyncPostExecute(Context, int, String, Messenger)
 * @see SyncBaseModel#onSyncPreExecute(Context, int, String, Messenger)
 */
public class DownloadSyncModel extends SyncBaseModel {

    private static final String TAG = DownloadSyncModel.class.getSimpleName();
    boolean isEssentialSyncDone = false, isTyreSyncDone = false;
    Object mEssentialDataResponse = null, mTyreDataResponse = null;

    @Override
    protected void onSyncPreExecute(Context context, int syncOptions,
                                    String lang, Messenger messageHandler) throws SQLException,
            IOException {
        LogUtil.i(TAG, "onSyncPreExecute");
        Constants.Force_Download_Sync_STATUS = true;
        try {
            LogUtil.i(TAG, "creating snapshot -- onSyncPreExecute --");
            DatabaseAdapter copy_temp = new DatabaseAdapter(context);
            copy_temp.copyFinalDB();
            copy_temp.close();
            DataBaseHandler handler = new DataBaseHandler(context);
            handler.openDB();
            handler.createTablesInDB();
            handler.copyDataBase();
            handler.deleteDatabaseAfterLoad();
            handler.closeDB();
            isEssentialSyncDone = false;
            isTyreSyncDone = false;
        } catch (Exception e) {
            LogUtil.i(TAG, "error creating snapshot -- onSyncPreExecute --");
            e.printStackTrace();
        }
    }

    @Override
    protected void performSync(final Context context, int syncOptions, String lang, Messenger messageHandler) {
        if (context == null) {
            LogUtil.e(TAG, "performSync context is null so returning");
            return;
        }
        mResponseMessage = new ResponseMessage();
        mResponseMessage.syncOptionsModel(syncOptions);
        try {
            onSyncPreExecute(context, syncOptions, lang, messageHandler);
        } catch (SQLException | IOException e1) {
            LogUtil.e(TAG, "db files deleted");
            //mResponseMessage.setMessage("error DB_DELETED");
            if (Constants.Force_Download_Sync_STATUS) {
                mResponseMessage.setMessage("error network");
            } else {
                mResponseMessage.setMessage("error DB_DELETED");
            }
            e1.printStackTrace();
            sendHandlerMessage(messageHandler, mResponseMessage);
            return;
        }
        LogUtil.i(TAG, "performSync");
        try {
            /*For Halosys
            AuthSync.autoSync(context);*/

            //Concurrent thread for Essential Data
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.d(TAG, "run: Essential Started: " + System.currentTimeMillis());
                        mEssentialDataResponse = null;
                        mEssentialDataResponse = HalosysServiceCall.getEssentialData();
                        Log.d(TAG, "run: Essential End: " + System.currentTimeMillis());
                    } catch (Exception e) {
                        Log.d(TAG, "run: Essential Sync Halosys Exceptoin:" + e.getMessage());
                    } finally {
                        isEssentialSyncDone = true;
                    }
                }
            }).start();

            //Concurrent thread for Tyre data
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.d(TAG, "run: Tyre Started: " + System.currentTimeMillis());
                        mTyreDataResponse = null;
                        mTyreDataResponse = HalosysServiceCall.getReusableStockInfoBySAPVendorCode(context, "4000096");
                        Log.d(TAG, "run: Tyre End: " + System.currentTimeMillis());
                    } catch (Exception e) {
                        Log.d(TAG, "run: Tyre Sync Halosys Exceptoin:" + e.getMessage());
                    } finally {
                        isTyreSyncDone = true;
                    }
                }
            }).start();

            while (!isEssentialSyncDone || !isTyreSyncDone) {
                Thread.sleep(1000);
            }

            Log.d(TAG, "performSync: Thread Execution finished");
            HalosysStatusModel mEssentialStatus = new HalosysStatusModel();
            HalosysStatusModel mTyreStatus = null;                                                //Sprint 2 :: tyre sync will return String response
            if (mEssentialDataResponse != null && mTyreDataResponse != null) {
                mEssentialStatus = (HalosysStatusModel) mEssentialDataResponse;
                mTyreStatus = (HalosysStatusModel) mTyreDataResponse;

                if (mEssentialStatus.getsCode() == HalosysCallStatus.CODE_SUCCESS &&
                        mTyreStatus.getsCode() == HalosysCallStatus.CODE_SUCCESS) {                                        //Sprint 2 :: need to check for the success response for tyre sync
                    FileOperations.convertAndCopyFile(Constants.PROJECT_PATH
                            + Constants.RESPONSEFILE_PATH, Constants.PROJECT_PATH + "/"
                            + Constants.USER + "_.db3");
                } else {
                    LogUtil.i(TAG, "Switch case-- Dafault error --");
                    mResponseMessage.setMessage("error network");
                }
            } else {
                LogUtil.i(TAG, "Switch case-- Dafault error --");
                mResponseMessage.setMessage("error network");
            }

        } catch (FileNotFoundException e) {
            LogUtil.i(TAG, "error Download sycning -- performsync --");
            LogUtil.pst(e);
            LogUtil.e(TAG, "server not found " + e.getMessage());
            mResponseMessage.setMessage("error server_down");
        } catch (SocketTimeoutException e) {
            LogUtil.e(TAG, "socket timed out" + e.getMessage());
            LogUtil.pst(e);
            e.printStackTrace();
            mResponseMessage.setMessage("error network");
        } catch (IOException e) {
            LogUtil.e(TAG, "network not working" + e.getMessage());
            LogUtil.i(TAG, "error IO exception -- performsync --");
            LogUtil.pst(e);
            e.printStackTrace();
            mResponseMessage.setMessage("error network");
        } catch (Exception e) {
            LogUtil.e(TAG, "General Excep" + e.getMessage());
            LogUtil.i(TAG, "Generalexception -- performsync --");
            LogUtil.pst(e);
            e.printStackTrace();
            mResponseMessage.setMessage("error network");
        }
        onSyncPostExecute(context, syncOptions, lang, messageHandler);
    }

    @Override
    protected void onSyncPostExecute(Context context, int syncOptions,
                                     String lang, Messenger messageHandler) {
        LogUtil.i(TAG, "onSyncPostExecute");
        DatabaseAdapter copy_temp = new DatabaseAdapter(context);
        String result = mResponseMessage.getMessage();
        LogUtil.i(TAG, "mResponseMessage :: Sending message to UI:: "
                + mResponseMessage);
        if (result != null && result.contains("error")) {
            String errorMessage;
            if (result.contains("server_down")) {
                errorMessage = Constants.ERROR_SERVER_DOWN_ERROR;
            } else {
                DatabaseAdapter mDBHelper = new DatabaseAdapter(context);
                mDBHelper.open();
                Constants.ERROR_NETWORK_SYNC_ERROR = mDBHelper
                        .getLabel(Constants.CHECK_INTERNET_CONNECTION_LABEL);
                errorMessage = Constants.ERROR_NETWORK_SYNC_ERROR;
                mDBHelper.close();
            }
            FileOperations.cleanUp();
        } else {
            try {
                // open db, clean it and then reopen
                copy_temp.open();
                copy_temp.copyTempDB();
                copy_temp.deleteDB();
                copy_temp.close();

                // copy the reponse from server
                copy_temp.copyResponseDB();
                copy_temp.open();

                // create db with existing and new data
                copy_temp.createEmptyTables("");
                copy_temp
                        .updateDBTables(com.goodyear.ejob.util.Constants.PROJECT_PATH
                                + "/"
                                + com.goodyear.ejob.util.Constants.USER
                                + "_temp.db3");

                String tyreDbPath = Constants.PROJECT_PATH + "/temp/stockinfo.db";
                if ((new File(tyreDbPath)).exists()) {
                    Log.d(TAG, "onSyncPostExecute: Starting Tyre db update");
                    copy_temp.updateTyreTable(tyreDbPath);
                } else {
                    Log.d(TAG, "onSyncPostExecute: Tyre db not found.");
                }

                FileOperations
                        .removeFile(com.goodyear.ejob.util.Constants.PROJECT_PATH
                                + "/"
                                + com.goodyear.ejob.util.Constants.USER
                                + ".db3");
                copy_temp.copyFinalDB();
                copy_temp.deleteDB();
                copy_temp.close();

                copy_temp.createDatabase();
                copy_temp.open();
                copy_temp.updateSyncTime(DateTimeUTC
                        .getMillisecondsFromUTCdate());
                copy_temp.copyFinalDB();
                Constants.Force_Download_Sync_STATUS = false;
                // stop loading screen
            } catch (SQLException | IOException e) {
                LogUtil.e("EjobActivity", "cannot write db");
            } finally {
                Constants.EJOBHEADERVALUESIFDBNULL = false;
                copy_temp.close();
                FileOperations.cleanUp();
            }
        }
        LogUtil.i(TAG, "Sending message to UI");
        // Sending message to UI
        sendHandlerMessage(messageHandler, mResponseMessage);
    }
}