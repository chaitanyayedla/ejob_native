/**copyright message*/
package com.goodyear.ejob.service;

/**
 * Util's class for EjobSyncService. Used to create Sync Modules.
 * 
 * @author Nilesh.Pawate
 * @see DownloadSyncModel
 */
public class SyncUtils {
	/**
	 * Key used to pass while starting of the service, to identify which module
	 * is created.
	 */
	public static final String SYNC_OPTIONS_KEY = "syncOptionsKey";
	/**
	 * Key used for language as parameter.
	 */
	public static final String SYNC_LANG_KEY = "syncLangKey";

	/**
	 * Enum to define the sync Modules.
	 * 
	 * @author Nilesh.Pawate
	 * 
	 */
	public enum SyncOptions {
		DOWNLOAD_SYNC(0), UPLOAD_SYNC(1), TRANSLATE_SYNC(2);
		private int value;

		private SyncOptions(int value) {
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}
	};

	/**
	 * API used to create sync Module.
	 * 
	 * @param moduleNo
	 *            Module No passed from #SyncOptions
	 * @return SyncBaseModel
	 */
	public static SyncBaseModel createSyncModule(int moduleNo) {
		SyncBaseModel baseModel = null;
		SyncOptions whichView = SyncOptions.values()[moduleNo];

		switch (whichView) {
		case DOWNLOAD_SYNC:
			baseModel = new DownloadSyncModel();
			break;
		case UPLOAD_SYNC:
			baseModel = new UploadSyncModel();
			break;
		case TRANSLATE_SYNC:
			baseModel = new TransaltionSyncModel();
			break;
		default:
			baseModel = new DownloadSyncModel();
			break;
		}
		return baseModel;
	}
}
