/**copyright message*/
package com.goodyear.ejob.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketTimeoutException;

import android.content.Context;
import android.database.SQLException;
import android.os.Environment;
import android.os.Messenger;
import android.util.Log;

import com.goodyear.ejob.EjobList;
import com.goodyear.ejob.MountPWTActivity;
import com.goodyear.ejob.dbhelpers.DataBaseHandler;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.sync.AuthSync;
import com.goodyear.ejob.sync.Result;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.FileOperations;
import com.goodyear.ejob.util.LogUtil;

/**
 * Class used to handle the Upload Sync. <br>
 * This should be called from service or thread. Once the sync is complete the
 * control will be passed to the UI with the help of Handler. <br>
 * You will be getting {@link SyncCallback#onSyncComplete(ResponseMessage)}
 * callback with proper message.
 * 
 * @author Nilesh.Pawate
 * @see SyncBaseModel#performSync(Context, int, String, Messenger)
 * @see SyncBaseModel#onSyncPostExecute(Context, int, String, Messenger)
 * @see SyncBaseModel#onSyncPreExecute(Context, int, String, Messenger)
 * 
 */

public class UploadSyncModel extends SyncBaseModel {

	/**
	 * Class TAG
	 */
	private static final String TAG = UploadSyncModel.class.getSimpleName();

	@Override
	protected void onSyncPreExecute(Context context, int syncOptions,
			String lang, Messenger messageHandler) throws SQLException,
			IOException {
		LogUtil.e(TAG, "onSyncPreExecute");
		DatabaseAdapter copy_temp = new DatabaseAdapter(context);
		copy_temp.open();
		copy_temp.copyFinalDB();
		DataBaseHandler handler = new DataBaseHandler(context);
		handler.openDB();
		handler.createTablesInDB();
		try {
			handler.copyDataBase();
		} catch (IOException e) {
			e.printStackTrace();
		}
		handler.deleteDatabaseAfterLoad();
		handler.closeDB();
		copy_temp.close();

		//CR 556 : DB backup
		backUpSnapShotDB();
	}

	@Override
	protected void performSync(Context context, int syncOptions, String lang,
			Messenger messageHandler) {
		if (context == null) {
			LogUtil.e(TAG, "performSync context is null so returning");
			return;
		}
		mResponseMessage = new ResponseMessage();
		mResponseMessage.syncOptionsModel(syncOptions);
		try {
			onSyncPreExecute(context, syncOptions, lang, messageHandler);
		} catch (SQLException | IOException e1) {
			LogUtil.e(TAG, "db files deleted");
			LogUtil.i(TAG,
					"error SQLException sycning IOException -- performsync --");
			LogUtil.pst(e1);
			if(Constants.APP_UPGRADE_STATUS)
			{
				mResponseMessage.setMessage("error network");
			}
			else {
				mResponseMessage.setMessage("error DB_DELETED");
			}
			e1.printStackTrace();
			sendHandlerMessage(messageHandler, mResponseMessage);
			return;
		}
		LogUtil.e(TAG, "performSync");
		try {
			AuthSync.uploadDownloadSync(context);
		} catch (FileNotFoundException e) {
			LogUtil.e("UploadSyncModel sync",
					"server not found " + e.getMessage());
			LogUtil.i(TAG,
					"error FileNotFoundException -- performsync --");
			LogUtil.pst(e);
			mResponseMessage.setMessage("error server_down");
		} catch (SocketTimeoutException e) {
			LogUtil.e(TAG, "socket timed out" + e.getMessage());
			LogUtil.i(TAG,
					"error SocketTimeoutException -- performsync --");
			LogUtil.pst(e);
			mResponseMessage.setMessage("error network");
		} catch (IOException e) {
			LogUtil.e("UploadSyncModel sync",
					"network not working" + e.getMessage());
			LogUtil.i(TAG,
					"error IOException -- performsync --");
			LogUtil.pst(e);
			mResponseMessage.setMessage("error network");
		}
		onSyncPostExecute(context, syncOptions, lang, messageHandler);
	}

	@Override
	protected void onSyncPostExecute(Context context, int syncOptions,
			String lang, Messenger messageHandler) {
		LogUtil.e(TAG, "onSyncPostExecute");
		String result = mResponseMessage.getMessage();
		LogUtil.i(TAG, "mResponseMessage :: Sending message to UI:: "
				+ mResponseMessage);

		//Error Condition Check to perform PostSync Operation
		if (result != null && result.contains("error")) {

			String errorMessage;
			if (result.contains("server_down")) {
				errorMessage = Constants.ERROR_SERVER_DOWN_ERROR;
			} else {
				DatabaseAdapter mDBHelper = new DatabaseAdapter(context);
				mDBHelper.open();
				Constants.ERROR_NETWORK_SYNC_ERROR = mDBHelper
						.getLabel(Constants.CHECK_INTERNET_CONNECTION_LABEL);
				errorMessage = Constants.ERROR_NETWORK_SYNC_ERROR;
				mDBHelper.close();
			}
			LogUtil.i("Upload Sync",errorMessage);
			FileOperations.cleanUp();
		}
		else {
			LogUtil.e(TAG, "onSyncPostExecute : without error");
			DatabaseAdapter copy_temp_post_sync = new DatabaseAdapter(context);
			try {
				// open db, clean it and then reopen
				copy_temp_post_sync.open();
				copy_temp_post_sync.updateDBTablesAfterSync(Constants.PROJECT_PATH + "/"
						+ Constants.USER + "_.db3");
				copy_temp_post_sync.copyFinalDB();
				copy_temp_post_sync.deleteDB();
				copy_temp_post_sync.close();
				copy_temp_post_sync.createDatabase();
				copy_temp_post_sync.open();
				copy_temp_post_sync.updateSyncTime(DateTimeUTC.getMillisecondsFromUTCdate());

			/*	*//**
				 *restorePWTOnDelete Method call moved to Ejoblist (delete module) : to add support for immediate availability of PWT tyre after job deleted
				 * CR 505
				 * Condition handling the method call to restore the deleted PWT data
				 *//*
				//Retrieve Reserved PWT tyre info from shared pref
				Constants.mPWTList = EjobList.retrieveMountPWTTyresFromSharedPref(context);
				if (null != Constants.mPWTList && Constants.mPWTList.size() != 0) {
					EjobList.restorePWTOnDelete(context,false);
				}*/
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				copy_temp_post_sync.close();
				FileOperations.cleanUp();
			}
			Constants.APP_UPGRADE_STATUS = false;
		}
		LogUtil.i(TAG, "Sending message to UI");
		// Sending message to UI
		sendHandlerMessage(messageHandler, mResponseMessage);
	}

	/**
	 * Method To Take Snapshot Database Backup
	 * CR 556 : DB backup
	 */
	private void backUpSnapShotDB()
	{
		String DestinationFileName = Constants.PROJECT_PATH + Constants.DBBackUp_DIR + Constants.USER + DateTimeUTC.getCurrentDataAndTime() + ".db";
		String SourceFileName = Constants.PROJECT_PATH + "/temp/" + DataBaseHandler.DBNAME + ".db";
		try {
			File file = new File(DestinationFileName);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();

			}

			//Backup Folder Path
			File BackUpFolder = new File(Constants.PROJECT_PATH + "/DBBackUp");

			//Folder Size Estimation
			Long estimatedSize = Constants.DBBackUpLimit-FileOperations.folderSize(new File(SourceFileName));

			if((FileOperations.folderSize(new File(SourceFileName))+ FileOperations.folderSize(BackUpFolder))>Constants.DBBackUpLimit)
			{
				//Method to Maintain Backup Directory Size
				FileOperations.deleteFilesInFolderToFitForGivenSize(BackUpFolder, estimatedSize);
			}

			//Copy source to Destination
			FileOperations.copyFile(SourceFileName, DestinationFileName);

			LogUtil.i(TAG, "DBBackupDone");

		}
		catch (Exception e)
		{
			LogUtil.i(TAG,"Exception - "+e.getMessage());
		}
	}
}
