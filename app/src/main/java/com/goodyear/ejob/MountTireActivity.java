/**
 * Mount New Tyre Operation
 */
package com.goodyear.ejob;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.goodyear.ejob.authenticator.Authentication;
import com.goodyear.ejob.blutooth.BluetoothService;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.job.operation.helpers.PressureCheckListener;
import com.goodyear.ejob.job.operation.helpers.TireImageItem;
import com.goodyear.ejob.util.CameraUtility;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.ContractTypes;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.InspectionDataHandler;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.TireDesignDetails;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.TyreFormElements;
import com.goodyear.ejob.util.TyreState;

import java.util.ArrayList;
import java.util.UUID;
/**
 * @author amitkumar.h
 * @version 1.0
 * Class handling the tire operation called tire Mount
 * It provides mechanism to load design page as per the selected dismounted
 * tire. Moreover it handles the data updates in different tables(JobItem and Tire) 
 * as per the user-entered tire-data during Mount Operation.
 */
public class MountTireActivity extends Activity implements InactivityHandler {

	/**
	 * Swipper Object
	 */
	private Swiper mSwipeDetector;
	/**
	 * ArrayList for Brands
	 */
	private ArrayList<String> mBrandArrayList;
	/**
	 * ArrayList for BrandCodes
	 */
	private ArrayList<String> mBrandCodeList;
	/**
	 * Selected Brand Name
	 */
	private String mSelectedBrandName;
	/**
	 * Selected Tyre size
	 */
	private String mSelectedtyreSize;
	/**
	 * Selected Tyre ASP
	 */
	private String mSelectedTyreTASP;
	/**
	 * Selected Tyre RIM
	 */
	private String mSelectedtyreTRIM;
	/**
	 * Selected Tyre Design
	 */
	private String mSelectedTyreDesign;
	/**
	 * Selected tyre details desing
	 */
	private String mSelectedTyreDetailedDesign;
	/**
	 * Size ArrayList
	 */
	private ArrayList<String> mSizeArrayList;
	/**
	 * RIM ArrayList
	 */
	private ArrayList<String> mTyreTRIMArrayList;
	/**
	 * ASP ArrayList
	 */
	private ArrayList<String> mTyreTASPArrayList;
	/**
	 * Spinner reference for Brand
	 */
	private Spinner mBrandSpinner;
	/**
	 * Spinner reference for Size
	 */
	private Spinner mSizeSpinner;
	/**
	 * Spinner reference for RIM
	 */
	private Spinner mTyreTRIMSpinner;
	/**
	 * Spinner reference for ASP
	 */
	private Spinner mTyreTASPSpinner;
	/**
	 * Spinner reference for TyreDesign
	 */
	private Spinner mTyreDesignSpinner;
	/**
	 * Spinner reference for Tyre FullDetails
	 */
	private Spinner mTyreFullDetailSpinner;
	/**
	 * ArrayAdapter for Size
	 */
	private ArrayAdapter<String> mSizeDataAdapter;
	/**
	 * ArrayAdapter for RIM
	 */
	private ArrayAdapter<String> mRimDataAdapter;
	/**
	 * ArrayAdapter for ASP
	 */
	private ArrayAdapter<String> mASPDataAdapter;
	/**
	 * ArrayAdapter for Design
	 */
	private ArrayAdapter<String> mDesignDataAdapter;
	/**
	 * ArrayAdapter for Full Details tyre
	 */
	private ArrayAdapter<String> mFullDesignDataAdapter;
	/**
	 * TextView reference for value tyre position
	 */
	private TextView mValue_tyrePosition;
	/**
	 * TextView reference for value tyre serial number
	 */
	private TextView mValue_tyreSerialNumber;
	/**
	 * ArrayList for Design
	 */
	private ArrayList<String> mDesignArrayList;
	/**
	 * ArrayList for FullDesign Details
	 */
	private ArrayList<String> mFullDesignArrayList;
	/**
	 * EditText reference for valueNSK1
	 */
	private EditText mValueNSK1;
	/**
	 * EditText reference for valueNSK2
	 */
	private EditText mValueNSK2;
	/**
	 * EditText reference for valueNSK3
	 */
	private EditText mValueNSK3;
	/**
	 * ScrollView reference for MainScrollBar
	 */
	private ScrollView mMainScrollBar;
	/**
	 * TextView reference for label NSK
	 */
	private TextView mLabelNSK;
	/**
	 * TextView reference for Dimension label
	 */
	private TextView mDimensionLabel;
	/**
	 * TextView reference for Size label
	 */
	private TextView mSizeLabel;
	/**
	 * TextView reference for ASP label
	 */
	private TextView mASPLabel;
	/**
	 * TextView reference for Rim label
	 */
	private TextView mRimLabel;
	/**
	 * TextView reference for Design label
	 */
	private TextView mDesignLabel;
	/**
	 * TextView reference for Type label
	 */
	private TextView mTypeLabel;
	/**
	 * TextView reference for label serial no
	 */
	private TextView mLbl_serialNo_swap;
	/**
	 * noteText
	 */
	private static String sNoteText = "";
	/**
	 * BluetoothService Object
	 */
	private BluetoothService mBTService;
	/**
	 * BroadcastReceiver Object
	 */
	private BroadcastReceiver mReceiver;
	/**
	 * mNSKCounter
	 */
	private int mNSKCounter = 1;
	/**
	 * TextView reference for DesingType
	 */
	private TextView mDesignTypeLabel;
	/**
	 * TextView reference for Selected tyre
	 */
	private Tyre mSelectedTire;
	/**
	 * Switch object from Rim
	 */
	private Switch mRimValue;
	/**
	 * TestAdapter Object
	 */
	private DatabaseAdapter mDbHelper;
	/**
	 * Final variable for NOTES_INTENT
	 */
	public static final int NOTES_INTENT = 104;
	/**
	 * Final variable for WOT_INTENT
	 */
	public static final int WOT_INTENT = 104;
	/**
	 * Final variable for DECIMAL_LENGTH
	 */
	public static final int DECIMAL_LENGTH = 8;
	/**
	 * boolean variable for swiped
	 */
	private boolean mSwiped = false;
	/**
	 * TextView reference for PressureLabel
	 */
	private TextView mPressureLabel;
	/**
	 * TextView reference for PressureUnit
	 */
	private TextView mPressureUnit;
	/**
	 * EditText reference for SetPressure
	 */
	private EditText mSetPressure;
	/**
	 * ImageView for Camera icon
	 */
	private ImageView mCamera_icon;
	/**
	 * boolean variable for Pressure measurement in PSI
	 */
	private boolean mIsSettingsPSI;
	/**
	 * SharedPreferences object
	 */
	private SharedPreferences mPreferences;
	/**
	 * PSI Value
	 */
	private float mPSIValue;
	/**
	 * Serial Number from previousIntent
	 */
	private String mSerialNoFromIntent;
	/**
	 * Fitter Type
	 */
	private String mFitter_type;
	/**
	 * Recommended Pressure
	 */
	private float mRecommendedPressure;
	/**
	 * Axle position
	 */
	private int mAxlePosition;
	/**
	 * Final Converted Pressure
	 */
	private String mFinalConvertedPressure;
	/**
	 * NSK Default Value
	 */
	private int mNSKDefaultValue;
	/**
	 * Dismounted Tyre
	 */
	private Tyre mDismountedTyre;
	/**
	 * Pressure Required
	 */
	private String mPressureRequired;
	/**
	 * Saved NSK value 1
	 */
	private String mSavedNSK1;
	/**
	 * Saved NSK value 2
	 */
	private String mSavedNSK2;
	/**
	 * Saved NSK value 3
	 */
	private String mSavedNSK3;
	/**
	 * Bundle Instance
	 */
	private Bundle mInstanceState;
	/**
	 * NSK23 set flag
	 */
	private int mNSK23SetFlag;

	/**
	 * NSK Count
	 */
	private int mGetnskCount;
	/**
	 * Selected SAP MaterialCode
	 */
	private String mSelectedSAPMaterialCodeID;
	/**
	 * Field in Correct
	 */
	private String fieldIncorrect;
	/**
	 * boolean variable for size Spinner Editable
	 */
	private boolean mIsSizeSpinnerEditable = true;
	/**
	 * Is Vehicle allow Differ sizes tire
	 */
	private boolean isDifferntSizeAllow = false;
	/**
	 * Validation Message for complete operations
	 */
	private String mDesignDetailRequired = "Design Details Required";

	/**
	 * Default value for Spinner
	 */
	private String mDefaultValueForSpinner = "Please Select";
	/**
	 * Last Spinner Selection Index, This variable using Spinner manipulation
	 * while orientation
	 */
	private int mLastSpinnerSelection = 0;
	/**
	 * IsOrientationChagned, This variable using Spinner manipulation while
	 * orientation
	 */
	private boolean mIsOrientationChanged = false;
	private String mAlloyLabel;
	private String mSteelLabel;
	private String mNotesLabel;
	String strbluetoothconnectionfalied;
	/**
	 * Saving instance is NSK edittext fields or editable or not
	 */
	private boolean mIsNSKEditTextEditable = false;

	/**
	 * Randomly generated External ID for tyre to be used at the time of
	 * updating job items
	 */
	String mExternalIdTyre = "";
	private TyreFormElements mFormElements;

	/**
	 * Vendor Id
	 */
	String mVendorId;

	/**
	 * PurchaseOrganization
	 */
	String mPurchaseOrg;

	/**
	 * Purchase Plant
	 */
	String mPurchasePlant;

	/**
	 * StockLocatiion
	 */
	String mStockLocation;

	/**
	 * Selected Brand Id
	 */
	String mSelectedBrandId;
	/**
	 * string variable to store external ID
	 */
	private String external_id = "";
	/**
	 * boolean variable for is Camera enabled
	 */
	private boolean mCameraEnabled = false;
	private TextView mBrandLabel;
	private TextView mJobDetailsLabel;
	/**
	 * String message from DB for Please Select Brand
	 */
	private String mPleaseSelectBrand = "Select a brand";
	private static final String TAG = "MountNewTireActivity";
	//User Trace logs trace tag
	private static final String TRACE_TAG = "Mount New Tyre";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mount_new_tire);
		//User Trace logs
		LogUtil.TraceInfo(TRACE_TAG, "none", "OP", true, false, true);
		mPreferences = getSharedPreferences(Constants.GOODYEAR_CONF, 0);
		if (mPreferences.getString(Constants.PRESSUREUNIT, "").equals(
				Constants.PSI_PRESSURE_UNIT)) {
			mIsSettingsPSI = true;
		}
		if (savedInstanceState == null) {
			sNoteText = "";
		}
		mDismountedTyre = new Tyre();
		createDismountedTyre();
		mSerialNoFromIntent = getIntent().getExtras().getString("SERIAL_NO");
		// BUG ::: 652 :: N/R should be allowed for SAP
		if(Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP)){
			mSerialNoFromIntent = Constants.SERIAL_NUMBER_ALLLOWED_FOR_SAP;
		}
		if(Constants.EDITED_SERIAL_NUMBER.equalsIgnoreCase(Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP)){
			mSerialNoFromIntent = Constants.SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP;
		}
		mRecommendedPressure = getIntent().getExtras().getFloat(
				VehicleSkeletonFragment.mAXLEPRESSURE);
		mAxlePosition = getIntent().getExtras().getInt(
				VehicleSkeletonFragment.mAXLEPRESSURE_POS);
		mSelectedTire = Constants.SELECTED_TYRE;
		mDbHelper = new DatabaseAdapter(this);
		mDesignArrayList = new ArrayList<String>();
		mFullDesignArrayList = new ArrayList<String>();
		mBrandArrayList = new ArrayList<String>();
		mBrandCodeList = new ArrayList<String>();
		mSizeArrayList = new ArrayList<String>();
		mTyreTRIMArrayList = new ArrayList<String>();
		mTyreTASPArrayList = new ArrayList<String>();
		mLbl_serialNo_swap = (TextView) findViewById(R.id.lbl_serialNo_swap);
		mValue_tyreSerialNumber = (TextView) findViewById(R.id.value_serialNo);
		mValue_tyrePosition = (TextView) findViewById(R.id.value_wp);
		mBrandSpinner = (Spinner) findViewById(R.id.spinner_brand);
		mSizeSpinner = (Spinner) findViewById(R.id.spinner_dimenssionSize);
		mTyreTASPSpinner = (Spinner) findViewById(R.id.value_dimenssionASP);
		mTyreTRIMSpinner = (Spinner) findViewById(R.id.value_dimenssionRIM);
		mTyreDesignSpinner = (Spinner) findViewById(R.id.spinner_make);
		// mTyreDesignSpinner.setEnabled(false);
		mTyreFullDetailSpinner = (Spinner) findViewById(R.id.spinner_type);
		// mTyreFullDetailSpinner.setEnabled(false);
		mValueNSK1 = (EditText) findViewById(R.id.value_NSK1);
		mValueNSK2 = (EditText) findViewById(R.id.value_NSK2);
		mValueNSK3 = (EditText) findViewById(R.id.value_NSK3);
		mValueNSK1.setEnabled(false);
		mValueNSK2.setEnabled(false);
		mValueNSK3.setEnabled(false);
		mLabelNSK = (TextView) findViewById(R.id.lbl_NSK);
		mDimensionLabel = (TextView) findViewById(R.id.lbl_dimenssion);
		mSizeLabel = (TextView) findViewById(R.id.lbl_dimenssionSiz);
		mASPLabel = (TextView) findViewById(R.id.lbl_dimenssionASP);
		mRimLabel = (TextView) findViewById(R.id.lbl_dimenssionRIM);
		mDesignLabel = (TextView) findViewById(R.id.lbl_make);// lbl_type
		mTypeLabel = (TextView) findViewById(R.id.lbl_RimType);
		mDesignTypeLabel = (TextView) findViewById(R.id.lbl_type);
		mBrandLabel = (TextView) findViewById(R.id.lbl_brand);
		mJobDetailsLabel = (TextView) findViewById(R.id.lbl_jobDetails);
		// CR:451
		mCamera_icon = (ImageView) findViewById(R.id.camera_icon);
		mCamera_icon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent photoIntent = new Intent(MountTireActivity.this,
						CameraActivity.class);
				photoIntent.putExtra("CURNT_IMAGE_COUNT",
						CameraUtility.getImageCount());
				startActivity(photoIntent);
			}
		});

		// Setting filters and Listeners to NSK values
		mValueNSK1
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mValueNSK2
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mValueNSK3
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mValueNSK1.addTextChangedListener(new NSKOneBeforeChanged());
		mValueNSK2.addTextChangedListener(new nskTwoBeforeChanged());
		mValueNSK3.addTextChangedListener(new nskThreeBeforeChanged());
		mRimValue = (Switch) findViewById(R.id.value_RimType);
		mPressureLabel = (TextView) findViewById(R.id.lbl_pressure);
		mPressureUnit = (TextView) findViewById(R.id.lbl_pressureunit);
		mSetPressure = (EditText) findViewById(R.id.value_pressure);
		setValuetoPressueView();
		mSelectedTire = Constants.SELECTED_TYRE;
		mSelectedSAPMaterialCodeID = Constants.SELECTED_TYRE
				.getSapMaterialCodeID();
		mMainScrollBar = (ScrollView) findViewById(R.id.scroll_main);
		mSwipeDetector = new Swiper(getApplicationContext(), mMainScrollBar);
		mMainScrollBar.setOnTouchListener(mSwipeDetector);
		mNSK23SetFlag = 0;
		mGetnskCount = 0;
		if (savedInstanceState != null) {
			mNSKCounter = savedInstanceState.getInt("nsk_counter", 0);
			mIsOrientationChanged = true;
			mInstanceState = savedInstanceState;
			mSelectedBrandName = savedInstanceState.getString("BRAND");
			if (savedInstanceState.containsKey("SIZE")) {
				mSelectedtyreSize = savedInstanceState.getString("SIZE");
				mSelectedTyreTASP = savedInstanceState.getString("ASP");
				mSelectedtyreTRIM = savedInstanceState.getString("RIM");
				mSelectedTyreDesign = savedInstanceState.getString("DESIGN");
				mSelectedTyreDetailedDesign = savedInstanceState
						.getString("FULLDETAIL");

				mSavedNSK1 = savedInstanceState.getString("SAVEDNSK1");
				mSavedNSK2 = savedInstanceState.getString("SAVEDNSK2");
				mSavedNSK3 = savedInstanceState.getString("SAVEDNSK3");
				if (mSavedNSK1 != null) {
					if (!mSavedNSK1.equals("")) {
						mGetnskCount++;
					}
				}
				mIsNSKEditTextEditable = savedInstanceState.getBoolean(
						"ISNSKEDITABLE", false);
			} else {
				mSelectedtyreSize = "";
			}
			checkLatestSpinnerSelectionBeforeOrientationChanges();

			mExternalIdTyre = savedInstanceState
					.getString("retainExternalIdTyre");
		} else {
			mSelectedtyreSize = mSelectedTire.getSize();
			mSelectedTyreTASP = mSelectedTire.getAsp();
			mSelectedtyreTRIM = mSelectedTire.getRim();
			mExternalIdTyre = String.valueOf(UUID.randomUUID());
		}

		TireDesignDetails detail = CommonUtils
				.getDetailsFromTyreInSameAxle(mSelectedTire);
		if (null != detail) {
			mSelectedtyreSize = detail.getSize();
			mSelectedTyreTASP = detail.getAsp();
			mSelectedtyreTRIM = detail.getRim();
			mIsSizeSpinnerEditable = false;
		}
		if (!mIsSizeSpinnerEditable) {
			loadSizeDetailsFromAxle();
		}
		if (Constants.contract.getIsDIffSizeAllowedForVehicle().equals("true")) {
			isDifferntSizeAllow = true;
			mSizeSpinner.setEnabled(true);
			mTyreTASPSpinner.setEnabled(true);
			mTyreTRIMSpinner.setEnabled(true);
		}
		loadLabelsFromDB();
		mValue_tyreSerialNumber.setText(mSerialNoFromIntent);
		mValue_tyrePosition.setText(mSelectedTire.getPosition());
		getFitterDetails();
		loadTyreDetailsFromOldTyre(0);
		loadRimType();
		restoreDialogState(savedInstanceState);
		captureInitialState(savedInstanceState);
	}

	/**
	 * Method to create a new position for dismounted tire
	 */
	private void createDismountedTyre() {
		if (null == Constants.SELECTED_TYRE) {
			return;
		}
		mDismountedTyre.setSerialNumber(Constants.SELECTED_TYRE
				.getSerialNumber());
		mDismountedTyre.setSize(Constants.SELECTED_TYRE.getSize());
		mDismountedTyre.setRim(Constants.SELECTED_TYRE.getRim());
		mDismountedTyre.setRimType(Constants.SELECTED_TYRE.getRimType());
		mDismountedTyre.setBrandName(Constants.SELECTED_TYRE.getBrandName());
		mDismountedTyre.setSapCodeWP(Constants.SELECTED_TYRE.getSapCodeWP());
		mDismountedTyre.setSapCodeTI(Constants.SELECTED_TYRE.getSapCodeTI());
		mDismountedTyre.setDesign(Constants.SELECTED_TYRE.getDesign());
		mDismountedTyre.setDesignDetails(Constants.SELECTED_TYRE
				.getDesignDetails());
		mDismountedTyre.setSapMaterialCodeID(Constants.SELECTED_TYRE
				.getSapMaterialCodeID());
		mDismountedTyre.setSapContractNumberID(Constants.SELECTED_TYRE
				.getSapContractNumberID());
		mDismountedTyre.setSapVenderCodeID(Constants.SELECTED_TYRE
				.getSapVenderCodeID());
		mDismountedTyre.setExternalID(Constants.SELECTED_TYRE.getExternalID());
		mDismountedTyre.setNsk(Constants.SELECTED_TYRE.getNsk());
		mDismountedTyre.setNsk2(Constants.SELECTED_TYRE.getNsk2());
		mDismountedTyre.setNsk3(Constants.SELECTED_TYRE.getNsk3());
		mDismountedTyre.setPosition(Constants.SELECTED_TYRE.getPosition());
		mDismountedTyre.setOriginalPosition(Constants.SELECTED_TYRE
				.getPosition());
		mDismountedTyre.setPressure(Constants.SELECTED_TYRE.getPressure());
		mDismountedTyre.setIsSpare(Constants.SELECTED_TYRE.getIsSpare());
		mDismountedTyre.setRegrooveInfo(Constants.SELECTED_TYRE
				.getRegrooveInfo());
		mDismountedTyre.setReGrooved(Constants.SELECTED_TYRE.isReGrooved());
		mDismountedTyre.setRetorqued(Constants.SELECTED_TYRE.isRetorqued());
		mDismountedTyre.setIsphysicalySwaped(Constants.SELECTED_TYRE
				.isIsphysicalySwaped());
		mDismountedTyre.setLogicalySwaped(Constants.SELECTED_TYRE
				.isLogicalySwaped());
		mDismountedTyre.setDismounted(Constants.SELECTED_TYRE.isDismounted());
		mDismountedTyre.setHasTurnedOnRim(Constants.SELECTED_TYRE
				.hasTurnedOnRim());
		mDismountedTyre.setWorkedOn(Constants.SELECTED_TYRE.isWorkedOn());
		mDismountedTyre.setStatus(Constants.SELECTED_TYRE.getStatus());
		mDismountedTyre.setActive(Constants.SELECTED_TYRE.getActive());
		// mDismountedTyre.setShared(Constants.SELECTED_TYRE.getShared());
		mDismountedTyre.setShared(Constants.SELECTED_TYRE.getShared());
		mDismountedTyre.setFromJobId(Constants.SELECTED_TYRE.getFromJobId());
		mDismountedTyre
				.setSapCodeAXID(Constants.SELECTED_TYRE.getSapCodeAXID());
		mDismountedTyre.setOrignalNSK(Constants.SELECTED_TYRE.getOrignalNSK());
		mDismountedTyre.setPWTMounted(Constants.SELECTED_TYRE.isPWTMounted());
		mDismountedTyre.setSapCodeVeh(Constants.SELECTED_TYRE.getSapCodeVeh());
		mDismountedTyre.setTorInfo(Constants.SELECTED_TYRE.getTorInfo());
		mDismountedTyre.setLogicalSwapinfo(Constants.SELECTED_TYRE
				.getLogicalSwapinfo());
		mDismountedTyre.setPhysicalSwapinfo(Constants.SELECTED_TYRE
				.getPhysicalSwapinfo());
		mDismountedTyre.setTyreState(Constants.SELECTED_TYRE.getTyreState());
		mDismountedTyre.setAsp(Constants.SELECTED_TYRE.getAsp());
		mDismountedTyre.setVehicleJob(Constants.SELECTED_TYRE.getVehicleJob());
		/**
		 * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
		 * Maintain temporary SAPVENDERCodeID until SAP updates
		 * Since SAPVENDERCodeID for few tyres are null, so by maintaining temporary SAPVENDERCodeID, will make PWT tyre with SAPVENDERCodeID as null, available for Reserve PWT.
		 * To retain TempSAPVendorCodeID Info
		 */
		mDismountedTyre.setTempSAPVendorCodeID(Constants.SELECTED_TYRE.getTempSAPVendorCodeID());
		TextView wheelPositionLabel = (TextView) findViewById(R.id.lbl_wp);
		wheelPositionLabel.setText(Constants.sLblWheelPos);
	}
	/**
	 * Method to load brand, size, design details if there is no brand
	 * correction
	 */
	private void loadSizeDetailsFromAxle() {
		loadSelectedSize();
		loadSelectedASP();
		loadSelectedRIM();
	}
	/**
	 * Method loading tire-size on UI element as per the selected tire
	 */
	private void loadSelectedSize() {
		mSizeArrayList.clear();
		mSizeArrayList.add(mSelectedtyreSize);
		mSizeDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mSizeArrayList);
		mSizeDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSizeSpinner.setAdapter(mSizeDataAdapter);
		mSizeSpinner.setEnabled(false);
	}
	/**
	 * Method loading tire-ASP on UI element as per the selected tire
	 */
	private void loadSelectedASP() {
		mTyreTASPArrayList.clear();
		mTyreTASPArrayList.add(mSelectedTyreTASP);
		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTASPSpinner.setAdapter(mASPDataAdapter);
		mTyreTASPSpinner.setEnabled(false);
	}
	/**
	 * Method loading tire-RIM on UI element as per the selected tire
	 */
	private void loadSelectedRIM() {
		mTyreTRIMArrayList.clear();
		mTyreTRIMArrayList.add(mSelectedtyreTRIM);
		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTRIMSpinner.setAdapter(mRimDataAdapter);
		mTyreTRIMSpinner.setEnabled(false);
	}

	/**
	 * Method to load Rim type based on dismounted tire
	 */
	private void loadRimType() {
		if (Constants.SELECTED_TYRE.getRimType() != null) {
			if (Constants.SELECTED_TYRE.getRimType().equals("2")) {
				mRimValue.setChecked(true);
			} else {
				mRimValue.setChecked(false);
			}
		} else {
			mRimValue.setChecked(false);
		}
	}

	/**
	 * Method to get type of fitter(Internal/External)
	 */
	private void getFitterDetails() {
		Cursor fitterCursor = null;
		try {
			fitterCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				fitterCursor = mDbHelper
						.getFitterTypeOfVendor(Constants.VENDER_NAME);
			}
			if (CursorUtils.isValidCursor(fitterCursor)) {
				fitterCursor.moveToFirst();
				mFitter_type = fitterCursor.getString(fitterCursor
						.getColumnIndex("FitterType"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(fitterCursor);
			if (null != mDbHelper) {
				mDbHelper.close();
			}
		}
	}

	@Override
	protected void onStop() {
		try {
			if (null != mReceiver) {
				unregisterReceiver(mReceiver);
			}
			if (null != mBTService) {
				mBTService.stop();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		super.onStop();
	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	@Override
	protected void onStart() {
		// set this activity as the log out handler
		LogoutHandler.setCurrentActivity(this);

		mBTService = new BluetoothService(this);
		IntentFilter intentFilter = new IntentFilter("BLUETOOTH_SENDER");
		mReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.hasExtra("BT_DATA_NSK")
						&& (null != mValueNSK1 && mValueNSK1.isEnabled())) {
					String nsk_value = intent.getStringExtra("BT_DATA_NSK");
				} else if (intent.hasExtra("BT_DATA_PRE")) {
					String pre_value = intent.getStringExtra("BT_DATA_PRE");
					if (mIsSettingsPSI) {
						String barValue;
						// change PSI to BAR
						barValue = CommonUtils.getPressureValue(context,
								CommonUtils.parseFloat(pre_value));
						mSetPressure.setText(String.valueOf(barValue));
					} else {
						mSetPressure.setText(pre_value);
					}
				} else if (intent.hasExtra("BT_STATUS_IS_DISCONNECTED")) {
					boolean isDisconnected = intent.getBooleanExtra(
							"BT_STATUS_IS_DISCONNECTED", false);
					LogUtil.i(
							"Bluetooth in my Operation",
							"Broadcast came to my oeration:: " + isDisconnected
									+ " mBTService: "
									+ mBTService.isConnected());
					if (!mBlockPressureFromBluetooth)
						mSetPressure.setEnabled(isDisconnected);
					if (isDisconnected) {
						mBTService.stop();
						initiateBT();
						// mBTService.beginListeningData(0);
					} else {
						LogUtil.i("TOR", "######## Makeing stopworker false ");
						// mBTService.resetSocket();
						mBTService.startReading();
						// mBTService.beginListeningData(0);
					}
				} else if (intent.hasExtra("BT_RETRY_FAIL")) {
					initiateBT();
				}// There is no need of Probe disconnect and connect here
					// because the nsk values are coming from DB
			}
		};
		try {
			registerReceiver(mReceiver, intentFilter);
		} catch (Exception e) {
			e.printStackTrace();
		}

		initiateBT();
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
	}
	/**
	 * Method initializing Blue-tooth, checking for connectivity and performing
	 * data transfer while using blue-tooth probes.
	 */
	private void initiateBT() {
		int status = mBTService.getPairedStatus();
		switch (status) {
		case 0:
			Toast.makeText(getApplicationContext(),
					Constants.sLblMultiplePaired, Toast.LENGTH_LONG).show();
			break;
		case 1:
			BTTask task = new BTTask();
			task.execute();
			break;
		case 2:
			Toast.makeText(getApplicationContext(),
					Constants.sLblPleasePairTLogik, Toast.LENGTH_LONG).show();
			break;
		case 3:
			Toast.makeText(getApplicationContext(),
					Constants.sLblNoPairedDevices, Toast.LENGTH_LONG).show();
			break;
		}
	}
	/**
	 * class handling blue-tooth functionality in a separate non-UI threads and
	 * performing data transfer while using blue-tooth probes.
	 */
	class BTTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... f_url) {
			try {
				mBTService.connectToDevice(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if (isCancelled()) {
				LogUtil.e("Regroove", "onPostExecute Async Task Cancelled  > ");
				return;
			}
			try {
				if (mBTService.isConnected()) {
					if (!mBlockPressureFromBluetooth)
						mSetPressure.setEnabled(false);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();

	}
	/**
	 * Overridden method storing all the captured data inside bundle object in
	 * order to handle different orientations
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (null != mBrandSpinner && mBrandSpinner.getCount() != 0) {
			outState.putString("BRAND", mBrandSpinner.getSelectedItem()
					.toString());
		}
		if (null != mSizeSpinner && mSizeSpinner.getCount() != 0) {
			outState.putString("SIZE", mSizeSpinner.getSelectedItem()
					.toString());
		}
		if (null != mTyreTASPSpinner && mTyreTASPSpinner.getCount() != 0) {
			outState.putString("ASP", mTyreTASPSpinner.getSelectedItem()
					.toString());
		}
		if (null != mTyreTRIMSpinner && mTyreTRIMSpinner.getCount() != 0) {
			outState.putString("RIM", mTyreTRIMSpinner.getSelectedItem()
					.toString());
		}
		if (null != mTyreDesignSpinner && mTyreDesignSpinner.getCount() != 0) {
			outState.putString("DESIGN", mTyreDesignSpinner.getSelectedItem()
					.toString());
		}
		if (null != mTyreFullDetailSpinner
				&& mTyreFullDetailSpinner.getCount() != 0) {
			outState.putString("FULLDETAIL", mTyreFullDetailSpinner
					.getSelectedItem().toString());
		}
		outState.putString("SAVEDNSK1", (mValueNSK1.getText().toString()));
		outState.putString("SAVEDNSK2", (mValueNSK2.getText().toString()));
		outState.putString("SAVEDNSK3", (mValueNSK3.getText().toString()));
		outState.putBoolean("ISNSKEDITABLE", mValueNSK1.isEnabled());
		outState.putString("retainExternalIdTyre", mExternalIdTyre);
		outState.putInt("nsk_counter", mNSKCounter);
		saveDialogState(outState);
		saveInitialState(outState);
		super.onSaveInstanceState(outState);
	}
	/**
	 * Loading Labels for UI from the Translation table of the local database
	 * file
	 */
	private void loadLabelsFromDB() {
		try {
			if (null == mDbHelper) {
				return;
			}
			mDbHelper.open();
			setTitle(mDbHelper.getLabel(Constants.LABEL_MOUNT_NEW_TYRE));
			strbluetoothconnectionfalied = mDbHelper
					.getLabel(Constants.BLUETOOTH_CONNECTION_FAILED);
			String tireDetailsLabel = mDbHelper
					.getLabel(Constants.LABEL_TIRE_DETAILS);
			TextView tireDetialsHead = (TextView) findViewById(R.id.lbl_tireDetails1);
			if (tireDetialsHead != null) {
				tireDetialsHead.setText(tireDetailsLabel);
			}
			((TextView) findViewById(R.id.lbl_tireDetails))
					.setText(tireDetailsLabel);

			mNotesLabel = mDbHelper.getLabel(195);
			invalidateOptionsMenu();

			String labelFromDB = mDbHelper.getLabel(418);
			mLbl_serialNo_swap.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.NSK_LABEL);
			mLabelNSK.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.PRESSURE_LABEL);
			mPressureLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.DIMENSION_LABEL);
			mDimensionLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.ASP_LABEL);
			mASPLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.RIM_LABEL);
			mRimLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(68);
			mDesignLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(Constants.SIZE_LABEL);
			mSizeLabel.setText(labelFromDB);

			labelFromDB = mDbHelper.getLabel(137);
			mDesignTypeLabel.setText(labelFromDB);

			mPressureRequired = mDbHelper.getLabel(Constants.PRESSURE_REQUIRED);
			mDesignDetailRequired = mDbHelper.getLabel(82);

			labelFromDB = mDbHelper.getLabel(391);
			mTypeLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.BRAND_LABEL);
			mBrandLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.JOB_DETAILS);
			mJobDetailsLabel.setText(labelFromDB);
			labelFromDB = mDbHelper.getLabel(Constants.PLEASE_SELECT_LABEL);
			mDefaultValueForSpinner = labelFromDB;
			mPleaseSelectBrand = mDbHelper.getLabel(Constants.SELECT_BRAND);
			fieldIncorrect = "Please fill all the entries";
			mSteelLabel = mDbHelper.getLabel(Constants.STEEL);
			mAlloyLabel = mDbHelper.getLabel(Constants.ALLOY);
			mRimValue.setTextOff(mSteelLabel);
			mRimValue.setTextOn(mAlloyLabel);
			Constants.sLblCheckPressureValue = mDbHelper.getLabel(Constants.CHECK_PRESSURE_LABEL);
			mDbHelper.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * TextWatcher for NSK1
	 */
	private class NSKOneBeforeChanged implements TextWatcher {
		@Override
		public void afterTextChanged(Editable nskOne) {
			/*
			 * if (mInstanceState != null) { mNSK23SetFlag++; if (mNSK23SetFlag
			 * > 4) { mSavedNSK2 = nskOne.toString(); mSavedNSK3 =
			 * nskOne.toString(); } } else { mSavedNSK2 = nskOne.toString();
			 * mSavedNSK3 = nskOne.toString(); }
			 */
			mValueNSK2.setText(nskOne.toString());
			mValueNSK3.setText(nskOne.toString());
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mValueNSK1.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {

				} else {
					mValueNSK1.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mValueNSK1.setText("");
			}

		}
	}

	/**
	 * TextWatcher for NSK2
	 */
	private class nskTwoBeforeChanged implements TextWatcher {

		@Override
		public void afterTextChanged(Editable nskTwo) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mValueNSK2.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {
				} else {
					mValueNSK2.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mValueNSK2.setText("");
			}

		}
	}

	/**
	 * TextWatcher for NSK3
	 */
	private class nskThreeBeforeChanged implements TextWatcher {

		@Override
		public void afterTextChanged(Editable nskThree) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mValueNSK3.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {
				} else {
					mValueNSK3.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mValueNSK3.setText("");
			}

		}
	}

	/**
	 * Method to get tire details from old tire
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreDetailsFromOldTyre(final int route) {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				getVendorDetails();
				if (null != mFitter_type
						&& mFitter_type.equalsIgnoreCase("I")
						&& ContractTypes.ZYMC.equals(Constants.contract
								.getContractType())) {
					if (mIsSizeSpinnerEditable) {
						mCursor = mDbHelper.getBrandNameForPurPlantStockLoc(
								mPurchasePlant, mStockLocation);
					} else {
						mCursor = mDbHelper
								.getBrandNameForPurPlantStockLocWithSize(
										mPurchasePlant, mStockLocation,
										mSelectedtyreSize, mSelectedTyreTASP,
										mSelectedtyreTRIM);
					}

				} else if (ContractTypes.ZYPG.equals(Constants.contract
						.getContractType())
						|| ContractTypes.ZYBC.equals(Constants.contract
								.getContractType())) {
					if (mIsSizeSpinnerEditable) {
						mCursor = mDbHelper.getBrandNameForPurOrg(mPurchaseOrg);
					} else {
						mCursor = mDbHelper.getBrandNameForPurOrgWithSize(
								mPurchaseOrg, mSelectedtyreSize,
								mSelectedTyreTASP, mSelectedtyreTRIM);
					}
				} else {
					mCursor = mDbHelper.getBrandNameForSWAP();
				}
				//FIX :: 533
				if(Constants.DIFFERENT_SIZE_ALLOWED){
					CursorUtils.closeCursor(mCursor);
					mCursor = null;
					mCursor = mDbHelper.getBrandNameForSWAP();
				}
			}
			mBrandArrayList.clear();
			mBrandArrayList.add(mPleaseSelectBrand);
			mBrandCodeList.clear();
			mBrandCodeList.add("NA");
			if (CursorUtils.isValidCursor(mCursor)) {
				mCursor.moveToFirst();
				while (!mCursor.isAfterLast()) {
					mBrandArrayList.add(mCursor.getString(mCursor
							.getColumnIndex("Description")));
					mBrandCodeList.add(mCursor.getString(mCursor
							.getColumnIndex("ID")));
					mCursor.moveToNext();
				}
				CursorUtils.closeCursor(mCursor);
			}
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mBrandArrayList);
			dataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mBrandSpinner.setAdapter(dataAdapter);
			mBrandSpinner.setEnabled(true);
			mBrandSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							mBrandSpinner
									.setOnItemSelectedListener(listenerSelectBrandName);
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
							Toast.makeText(getApplicationContext(),
									Constants.sLblNothingSelected,
									Toast.LENGTH_SHORT).show();
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}

	OnItemSelectedListener listenerSelectBrandName = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			mSelectedBrandName = mBrandSpinner.getSelectedItem().toString();
			if (mSelectedBrandName.equalsIgnoreCase(mPleaseSelectBrand)) {
				mTyreFullDetailSpinner.setEnabled(false);
				mTyreDesignSpinner.setEnabled(false);
			} else {
				mTyreFullDetailSpinner.setEnabled(true);
				mTyreDesignSpinner.setEnabled(true);
			}
			if (isDifferntSizeAllow || mIsSizeSpinnerEditable) {
				// Bug :: 584 :: wrong method call removed
				if (0 < mBrandSpinner.getSelectedItemPosition()) {
					getTyreSizes();
				} else {
					clearSpinnersData(2);
				}
			} else {
				clearPreviousSelections(5);
				if (0 < mBrandSpinner.getSelectedItemPosition()) {
					getTyreDesign();
				} else {
					clearSpinnersData(5);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView arg0) {
			Toast.makeText(getApplicationContext(),
					Constants.sLblNothingSelected, Toast.LENGTH_SHORT).show();
		}
	};

	/**
	 * Method to get tire sizes for brand correction
	 */
	public void getTyreSizes() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				mCursor = mDbHelper.getTyreSizeFromBrand(mSelectedBrandName);
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(2);
				return;
			}
			mCursor.moveToFirst();
			mSizeArrayList.clear();
			// mSizeArrayList.add("");
			while (!mCursor.isAfterLast()) {
				mSizeArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("TSize")));
				mCursor.moveToNext();
			}
			CursorUtils.closeCursor(mCursor);
			loadTyreSize();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Method to load tire sizes for brand correction
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreSize() {
		try {
			mSizeDataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mSizeArrayList);
			mSizeDataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSizeSpinner.setAdapter(mSizeDataAdapter);
			if (Constants.contract.getIsDIffSizeAllowedForVehicle().equals(
					"false")) {
				mSizeSpinner.setEnabled(false);
			}
			for (int i = 0; i < mSizeArrayList.size(); i++) {
				if (mSizeArrayList.get(i).equals(mSelectedtyreSize)) {
					mSizeSpinner.setSelection(i);
					break;
				}
			}
			mSizeSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							mSelectedtyreSize = mSizeSpinner.getSelectedItem()
									.toString();
							clearPreviousSelections(3);
							if (0 <= mSizeSpinner.getSelectedItemPosition()) {
								getTyreASP();
							} else {
								clearSpinnersData(3);
							}
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
							Toast.makeText(getApplicationContext(),
									Constants.sLblNothingSelected,
									Toast.LENGTH_SHORT).show();
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to get tire ASP for brand correction
	 */
	public void getTyreASP() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				mCursor = mDbHelper.getTyreASPFromSize(mSelectedtyreSize,
						mSelectedBrandName);
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(3);
				return;
			}
			mCursor.moveToFirst();
			mTyreTASPArrayList.clear();
			// mTyreTASPArrayList.add("");
			while (!mCursor.isAfterLast()) {
				mTyreTASPArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("TASP")));
				mCursor.moveToNext();
			}
			mCursor.close();
			loadTyreTASP();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Method to load tire ASP for brand correction
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreTASP() {
		mASPDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTASPArrayList);
		mASPDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTASPSpinner.setAdapter(mASPDataAdapter);
		if (Constants.contract.getIsDIffSizeAllowedForVehicle().equals("false")) {
			mTyreTASPSpinner.setEnabled(false);
		}
		for (int i = 0; i < mTyreTASPArrayList.size(); i++) {
			if (mTyreTASPArrayList.get(i).equals(mSelectedTyreTASP)) {
				mTyreTASPSpinner.setSelection(i);
				break;
			}
		}
		mTyreTASPSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedTyreTASP = mTyreTASPSpinner.getSelectedItem()
								.toString();
						clearPreviousSelections(4);
						if (0 <= mTyreTASPSpinner.getSelectedItemPosition()) {
							getTyreRIM();
						} else {
							clearSpinnersData(4);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
						Toast.makeText(getApplicationContext(),
								Constants.sLblNothingSelected,
								Toast.LENGTH_SHORT).show();
					}
				});
	}

	/**
	 * Method to get tire RIM for brand correction
	 */
	public void getTyreRIM() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				mCursor = mDbHelper.getTyreRimFromSize(mSelectedtyreSize,
						mSelectedTyreTASP, mSelectedBrandName);
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(4);
				return;
			}
			mCursor.moveToFirst();
			mTyreTRIMArrayList.clear();
			// mTyreTRIMArrayList.add("");
			while (!mCursor.isAfterLast()) {
				mTyreTRIMArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("TRIM")));
				mCursor.moveToNext();
			}
			CursorUtils.closeCursor(mCursor);
			loadTyreTRIM();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Method to load tire RIM for brand correction
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreTRIM() {
		mRimDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mTyreTRIMArrayList);
		mRimDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreTRIMSpinner.setAdapter(mRimDataAdapter);
		if (Constants.contract.getIsDIffSizeAllowedForVehicle().equals("false")) {
			mTyreTRIMSpinner.setEnabled(false);
		}
		int size = mTyreTRIMArrayList.size();
		for (int i = 0; i < size; i++) {
			if (mTyreTRIMArrayList.get(i).equals(mSelectedtyreTRIM)) {
				mTyreTRIMSpinner.setSelection(i);
				break;
			}
		}
		mTyreTRIMSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedtyreTRIM = mTyreTRIMSpinner.getSelectedItem()
								.toString();
						clearPreviousSelections(5);
						if (0 <= mTyreTRIMSpinner.getSelectedItemPosition()) {
							getTyreDesign();
						} else {
							clearSpinnersData(5);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
						Toast.makeText(getApplicationContext(),
								Constants.sLblNothingSelected,
								Toast.LENGTH_SHORT).show();
					}
				});
	}

	/**
	 * Method to get tire design for brand correction
	 */
	public void getTyreDesign() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				mSelectedBrandId = mBrandCodeList.get(mBrandSpinner
						.getSelectedItemPosition());
				if (ContractTypes.ZYPG.equals(Constants.contract
						.getContractType())
						|| ContractTypes.ZYBC.equals(Constants.contract
								.getContractType())) {
					mCursor = mDbHelper.getTyreDesignByPurchaseOrg(
							mSelectedtyreSize, mSelectedTyreTASP,
							mSelectedtyreTRIM, mSelectedBrandId, mPurchaseOrg);
				} else if (ContractTypes.ZYMC.equals(Constants.contract
						.getContractType())
						&& null != mFitter_type
						&& mFitter_type.equalsIgnoreCase("I")) {
					mCursor = mDbHelper
							.getTyreDesignByStockLocAndPurchasePlant(
									mSelectedtyreSize, mSelectedTyreTASP,
									mSelectedtyreTRIM, mSelectedBrandId,
									mPurchasePlant, mStockLocation);
				} else {
					mCursor = mDbHelper.getTyreDesignByBrandId(
							mSelectedtyreSize, mSelectedTyreTASP,
							mSelectedtyreTRIM, mSelectedBrandId);
				}
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(5);
				return;
			}
			mCursor.moveToFirst();
			mDesignArrayList.clear();
			mDesignArrayList.add(mDefaultValueForSpinner);
			while (!mCursor.isAfterLast()) {
				mDesignArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("Deseign")));
				mCursor.moveToNext();
			}
			mCursor.close();
			loadTyreDesign();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Method to load tire design for brand correction
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreDesign() {
		mDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mDesignArrayList);
		mDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreDesignSpinner.setAdapter(mDesignDataAdapter);
		for (int i = 0; i < mDesignArrayList.size(); i++) {
			if (mDesignArrayList.get(i).equals(mSelectedTyreDesign)) {
				mTyreDesignSpinner.setSelection(i);
				break;
			}
		}
		mTyreDesignSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedTyreDesign = mTyreDesignSpinner
								.getSelectedItem().toString();
						clearPreviousSelections(6);
						if (0 < mTyreDesignSpinner.getSelectedItemPosition()) {
							getTyreDetailedDesign();// type
						} else {
							clearSpinnersData(6);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
						Toast.makeText(getApplicationContext(),
								Constants.sLblNothingSelected,
								Toast.LENGTH_SHORT).show();
					}
				});
	}

	/**
	 * Method to get NSK from tire model
	 */
	private void getNSK(String selectedTyreDesign, String fullTireDetails) {
		String nskDefaultValueString = "";
		boolean isEditable = false;
		Cursor mCursor = null;
		try {
			if (mGetnskCount > 0) {
				mValueNSK1.setText(mSavedNSK1);
				mValueNSK2.setText(mSavedNSK2);
				mValueNSK3.setText(mSavedNSK3);
				mGetnskCount = 0;
				isEditable = mIsNSKEditTextEditable;
			} else {
				if (null != mDbHelper) {
					mDbHelper.open();
					mCursor = mDbHelper.getlNSK(selectedTyreDesign,
							fullTireDetails);
					if (CursorUtils.isValidCursor(mCursor)) {
						mCursor.moveToFirst();
						nskDefaultValueString = mCursor.getString(mCursor
								.getColumnIndex("OriginalTD"));
						CursorUtils.closeCursor(mCursor);
					}
				}
				float value_float = 0;
				try {
					if (TextUtils.isEmpty(nskDefaultValueString)) {
						value_float = 0;
					} else {
						value_float = CommonUtils
								.parseFloat(nskDefaultValueString);
					}
				} catch (NumberFormatException ne) {

				}
				if (value_float <= 0) {
					isEditable = true;
					mNSKCounter = 1;
				}

				mValueNSK1.setText(nskDefaultValueString);
				mValueNSK2.setText(nskDefaultValueString);
				mValueNSK3.setText(nskDefaultValueString);
			}

			if (isEditable) {
				mValueNSK1.setEnabled(true);
				mValueNSK2.setEnabled(true);
				mValueNSK3.setEnabled(true);
			} else {
				mValueNSK1.setEnabled(false);
				mValueNSK2.setEnabled(false);
				mValueNSK3.setEnabled(false);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}

	}

	/**
	 * Method to get tire full design for brand correction
	 */
	public void getTyreDetailedDesign() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();

				if (ContractTypes.ZYPG.equals(Constants.contract
						.getContractType())
						|| ContractTypes.ZYBC.equals(Constants.contract
								.getContractType())) {
					mCursor = mDbHelper.getTyreDetailedDesignByPurchaseOrg(
							mSelectedtyreSize, mSelectedTyreTASP,
							mSelectedtyreTRIM, mSelectedTyreDesign,
							mSelectedBrandId, mPurchaseOrg);
				} else if (ContractTypes.ZYMC.equals(Constants.contract
						.getContractType())
						&& null != mFitter_type
						&& mFitter_type.equalsIgnoreCase("I")) {
					mCursor = mDbHelper
							.getTyreDetailedDesignByStockLocAndPurchasePlant(
									mSelectedtyreSize, mSelectedTyreTASP,
									mSelectedtyreTRIM, mSelectedTyreDesign,
									mSelectedBrandId, mPurchasePlant,
									mStockLocation);
				} else {
					mCursor = mDbHelper.getTyreDetailedDesignByBrandId(
							mSelectedtyreSize, mSelectedTyreTASP,
							mSelectedtyreTRIM, mSelectedTyreDesign,
							mSelectedBrandId);
				}
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				clearSpinnersData(6);
				return;
			}
			mCursor.moveToFirst();
			mFullDesignArrayList.clear();
			mFullDesignArrayList.add(mDefaultValueForSpinner);
			while (!mCursor.isAfterLast()) {
				mFullDesignArrayList.add(mCursor.getString(mCursor
						.getColumnIndex("FullTireDetails")));
				mCursor.moveToNext();
			}
			CursorUtils.closeCursor(mCursor);
			loadTyreDetailedDesign();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Method to load tire full design for brand correction
	 */
	@SuppressWarnings("rawtypes")
	private void loadTyreDetailedDesign() {
		mFullDesignDataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mFullDesignArrayList);
		mFullDesignDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTyreFullDetailSpinner.setAdapter(mFullDesignDataAdapter);
		for (int i = 0; i < mFullDesignArrayList.size(); i++) {
			if (mFullDesignArrayList.get(i).equals(mSelectedTyreDetailedDesign)) {
				mTyreFullDetailSpinner.setSelection(i);
				break;
			}
		}
		mTyreFullDetailSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView adapter, View v,
							int i, long lng) {
						mSelectedTyreDetailedDesign = mTyreFullDetailSpinner
								.getSelectedItem().toString();
						if (0 < mTyreFullDetailSpinner
								.getSelectedItemPosition()) {
							getNSK(mSelectedTyreDesign,
									mSelectedTyreDetailedDesign);
						} else {
							clearSpinnersData(7);
						}
					}

					@Override
					public void onNothingSelected(AdapterView arg0) {
						Toast.makeText(getApplicationContext(),
								Constants.sLblNothingSelected,
								Toast.LENGTH_SHORT).show();
					}
				});
	}
	/**
	 * Method Getting Tire SAPMaterialCodeID from local DB3 file as per the
	 * selected DetailedDesign
	 */
	public void getSAPMaterialCodeID() {
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				mCursor = mDbHelper.getSAPMaterialCodeID(mSelectedtyreSize,
						mSelectedTyreTASP, mSelectedtyreTRIM,
						mSelectedTyreDesign, mSelectedTyreDetailedDesign,
						mSelectedBrandName);
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				return;
			}
			mCursor.moveToFirst();
			mSelectedSAPMaterialCodeID = mCursor.getString(
					mCursor.getColumnIndex("ID")).toString();
			CursorUtils.closeCursor(mCursor);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}
	/**
	 * Method handling the data-updates for the tables(JobItem and tire) 
	 * for the selected tire. It checks all the mandatory fields then allows 
	 * user to navigate back to Vehicle Skeleton after updating the data
	 */
	private void onReturnSkeleton() {
		Intent intentReturn = new Intent();

		mFinalConvertedPressure = CommonUtils.getFinalConvertedPressureValue(
				mRecommendedPressure, mSetPressure.getText().toString(),
				mIsSettingsPSI);

		updatePressureValue();
		mDismountedTyre.setPosition(mDismountedTyre.getPosition() + "$");
		VehicleSkeletonFragment.tyreInfo.add(mDismountedTyre);

		Constants.SELECTED_TYRE.setSerialNumber(mSerialNoFromIntent);
		Constants.SELECTED_TYRE.setBrandName(mBrandSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setSize(mSizeSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setAsp(mTyreTASPSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setRim(mTyreTRIMSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setDesign(mTyreDesignSpinner.getSelectedItem()
				.toString());
		Constants.SELECTED_TYRE.setDesignDetails(mTyreFullDetailSpinner
				.getSelectedItem().toString());
		Constants.SELECTED_TYRE.setNsk(mValueNSK1.getText().toString());
		Constants.SELECTED_TYRE.setNsk2(mValueNSK2.getText().toString());
		Constants.SELECTED_TYRE.setNsk3(mValueNSK3.getText().toString());
		Constants.SELECTED_TYRE.setPressure(mFinalConvertedPressure);
		Constants.SELECTED_TYRE.setShared("0");
		Constants.SELECTED_TYRE.setTyreState(TyreState.NEW_TIRE);
		Constants.SELECTED_TYRE.setFromJobId(null);
		Constants.SELECTED_TYRE.setVehicleJob(Constants.contract
				.getVehicleSapCode());
		Constants.SELECTED_TYRE.setSapCodeTI(null);
		Constants.SELECTED_TYRE.setStatus("0");
		getSAPMaterialCodeID();
		Constants.SELECTED_TYRE
				.setSapMaterialCodeID(mSelectedSAPMaterialCodeID);
		/**
		 * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
		 * Maintain temporary SAPVENDERCodeID until SAP updates
		 * Since SAPVENDERCodeID for few tyres are null, so by maintaining temporary SAPVENDERCodeID, will make PWT tyre with SAPVENDERCodeID as null, available for Reserve PWT.
		 * Make TempSAPVendorCodeID Info as null for new tyre
		 */
		Constants.SELECTED_TYRE.setTempSAPVendorCodeID(null);

		// close keyboard if open
		try {
			CommonUtils.hideKeyboard(this, getWindow().getDecorView()
					.getRootView().getWindowToken());
		} catch (Exception e) {
			LogUtil.i("Mount NEW on activity return",
					"could not close keyboard");
		}

		updateJobItem();

		//Condition aaded to check and update inspection data for the selected tire
			InspectionDataHandler.getMyInstance(getApplicationContext()).updateJobitemForInspection(Constants.SELECTED_TYRE,false);

		//User Trace logs
		try {
			String traceData;
			traceData = "\n\t\tTyre Details : " + Constants.SELECTED_TYRE.getBrandName() +
					"| " + Constants.SELECTED_TYRE.getSize() +
					"| " + Constants.SELECTED_TYRE.getAsp() +
					"| " + Constants.SELECTED_TYRE.getRim() +
					"| " + Constants.SELECTED_TYRE.getDesign() +
					"| " + Constants.SELECTED_TYRE.getDesignDetails() +


					"\n\t\tJob Details : " + Constants.SELECTED_TYRE.getNsk() +
					", " + Constants.SELECTED_TYRE.getNsk2() +
					", " + Constants.SELECTED_TYRE.getNsk3() +
					"| " +Constants.SELECTED_TYRE.getPressure();


			traceData = traceData+	"| " + Constants.SELECTED_TYRE.getRimType();
			LogUtil.TraceInfo(TRACE_TAG, "none","Data : " + traceData, false, false, false);
		}
		catch (Exception e)
		{
			LogUtil.TraceInfo(TRACE_TAG, "Data : Exception : ", e.getMessage(), false, true, false);
		}

		intentReturn.putExtra("mount", true);
		setResult(VehicleSkeletonFragment.MOUNT_INTENT, intentReturn);
		CameraUtility.resetParams();
		finish();
		MountTireActivity.this.overridePendingTransition(R.anim.slide_left_in,
				R.anim.slide_left_out);
	}

	/**
	 * Get Rim Type
	 */
	private String getRimType() {
		if (mRimValue.isChecked()) {
			return "2"; // Alloy
		} else {
			return "1"; // Steel (Defaultvalue)
		}
	}
	/**
	 * Method Updating Data for the selected tire in JobItem table with
	 * the corresponding Mount-New ActionType
	 */
	private void updateJobItem() {
		try {
		LogUtil.DBLog(TAG,"JobItem Data","Insertion Initialized");
		external_id = String.valueOf(UUID.randomUUID());
		int jobID = getJobItemIDForThisJobItem();
		int len = VehicleSkeletonFragment.mJobItemList.size();
		JobItem jobItem = new JobItem();
		jobItem.setActionType("2");
		jobItem.setAxleNumber("0");
		jobItem.setAxleServiceType("0");
		jobItem.setCasingRouteCode("");
		jobItem.setDamageId("");
		jobItem.setExternalId(external_id);
		jobItem.setGrooveNumber(null);
		jobItem.setJobId(String.valueOf(jobID));
		jobItem.setMinNSK(minNSKSet());
		jobItem.setNote(sNoteText);
		jobItem.setNskOneAfter(mValueNSK1.getText().toString());
		jobItem.setNskOneBefore("0");
		jobItem.setNskThreeAfter(mValueNSK3.getText().toString());
		jobItem.setNskThreeBefore("0");
		jobItem.setNskTwoAfter(mValueNSK2.getText().toString());
		jobItem.setNskTwoBefore("0");
		jobItem.setOperationID("");
		jobItem.setPressure(mFinalConvertedPressure);
		jobItem.setRecInflactionDestination("0");
		jobItem.setRecInflactionOrignal("0");
		jobItem.setRegrooved("False");
		jobItem.setReGrooveNumber(null);
		jobItem.setRegrooveType("0");
		jobItem.setRemovalReasonId("");
		jobItem.setRepairCompany(null);
		jobItem.setRimType(getRimType());
		jobItem.setSapCodeTilD("0");
		jobItem.setSequence(String.valueOf(len + 1));
		jobItem.setServiceCount("0");
		jobItem.setSwapOriginalPosition(null);
		jobItem.setServiceID("0");
		jobItem.setSwapType("0");
		jobItem.setTorqueSettings("");
		jobItem.setTyreID(mExternalIdTyre);
		jobItem.setWorkOrderNumber(null);
		jobItem.setThreaddepth("0");

		// Fix:: Edited serial no was not coming, so added serial no here. on
		// 15-11
		jobItem.setSerialNumber(Constants.SELECTED_TYRE.getSerialNumber());
		jobItem.setBrandName(Constants.SELECTED_TYRE.getBrandName());
		jobItem.setFullDesignDetails(Constants.SELECTED_TYRE.getDesignDetails());
		// Till Here
		LogUtil.DBLog(TAG,"Job Item Data","Inserted"+jobItem.toString() );
		VehicleSkeletonFragment.mJobItemList.add(jobItem);
		LogUtil.DBLog(TAG,"Job Item Data","Inserted");
		Constants.SELECTED_TYRE.setExternalID(mExternalIdTyre);
		if (null != mDbHelper) {
			mDbHelper.close();
		}
		updateTireImages(String.valueOf(jobID));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtil.DBLog(TAG,"Job Item Data","Exception--"+e.getMessage());
			e.printStackTrace();
		}
	}
	/**
	 * Method handling image capture and image data-updates for Tire Image table
	 * It upgrades the quality of captured images as well
	 */
	private void updateTireImages(String id) {
		if (Constants.sBitmapPhoto1 != null) {
			TireImageItem item1 = new TireImageItem();
			item1.setJobItemId(id);
			if (mCameraEnabled)
				item1.setImageType("0");
			else
				item1.setImageType("1");
			item1.setDamageDescription(Constants.DAMAGE_NOTES1);
			item1.setTyreImage(CameraUtility.getByteFromBitmap(Constants.sBitmapPhoto1,
					Constants.sBitmapPhoto1Quality));
			item1.setJobExternalId(external_id);
			VehicleSkeletonFragment.mTireImageItemList.add(item1);
		}
		if (Constants.sBitmapPhoto2 != null) {
			TireImageItem item2 = new TireImageItem();
			item2.setJobItemId(id);
			item2.setImageType("1");
			item2.setDamageDescription(Constants.DAMAGE_NOTES2);
			item2.setTyreImage(CameraUtility.getByteFromBitmap(Constants.sBitmapPhoto2,
					Constants.sBitmapPhoto2Quality));
			item2.setJobExternalId(external_id);
			VehicleSkeletonFragment.mTireImageItemList.add(item2);
		}
		if (Constants.sBitmapPhoto3 != null) {
			TireImageItem item3 = new TireImageItem();
			item3.setJobItemId(id);
			item3.setImageType("1");
			item3.setDamageDescription(Constants.DAMAGE_NOTES3);
			item3.setTyreImage(CameraUtility.getByteFromBitmap(Constants.sBitmapPhoto3,
					Constants.sBitmapPhoto3Quality));
			item3.setJobExternalId(external_id);
			VehicleSkeletonFragment.mTireImageItemList.add(item3);
		}
	}
	/**
	 * Method returning maximum count of Job present in the JobItem table
	 * @return: Max Job Count
	 */
	private int getJobItemIDForThisJobItem() {
		int countJobItemsNotPresent = 0;
		mDbHelper.open();
		int currentJobItemCountInDB = mDbHelper.getJobItemCount();
		Cursor cursor = null;
		try {
			cursor = mDbHelper.getJobItemValues();
			if (!CursorUtils.isValidCursor(cursor)) {
				return countJobItemsNotPresent + 1
						+ VehicleSkeletonFragment.mJobItemList.size();
			}
			for (JobItem jobItem : VehicleSkeletonFragment.mJobItemList) {
				boolean flag = false;
				for (boolean hasItem = cursor.moveToFirst(); hasItem; hasItem = cursor
						.moveToNext()) {
					if (jobItem.getExternalId().equals(
							cursor.getString(cursor
									.getColumnIndex("ExternalID")))) {
						countJobItemsNotPresent++;
						break;
					}
				}
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			mDbHelper.close();
			// CursorUtils.closeCursor(cursor);
		}
		return currentJobItemCountInDB - countJobItemsNotPresent
				+ VehicleSkeletonFragment.mJobItemList.size() + 1;
	}
	/**
	 * Method returning minimum value of NSK(thread-depth) among the three
	 * captured NSKs
	 * @return: Minimum value of NSK(thread-depth)
	 */
	private String minNSKSet() {
		float smallest;
		float a = CommonUtils.parseFloat(mValueNSK1.getText().toString());
		float b = CommonUtils.parseFloat(mValueNSK2.getText().toString());
		float c = CommonUtils.parseFloat(mValueNSK3.getText().toString());
		if (a < b && a < c) {
			smallest = a;
		} else if (b < c && b < a) {
			smallest = b;
		} else {
			smallest = c;
		}
		return String.valueOf(smallest);
	}

	private AlertDialog mBackAlertDialog;
	private boolean mBlockPressureFromBluetooth;

	private void saveDialogState(Bundle state) {
		state.putBoolean("mBackAlertDialog",
				(mBackAlertDialog != null && mBackAlertDialog.isShowing()));
	}

	private void restoreDialogState(Bundle state) {
		if (state != null) {
			if (state.getBoolean("mBackAlertDialog")) {
				actionBackPressed();
			}
		}
	}

	@Override
	protected void onDestroy() {
		try {
			if(mDbHelper != null){
				mDbHelper.close();
			}
			if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
				mBackAlertDialog.dismiss();
			}
			Constants.ONSTATE_MOUNT = false;
			Constants.EDITED_SERIAL_NUMBER = "";
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		if (areAnyChangesMade()) {
			actionBackPressed();
		} else {
			LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press", false, false, false);
			super.onBackPressed();
		}
	};
	/**
	 * Method handling the action performed where user presses the back button
	 * Alert Box created with two option: YES and NO
	 */
	private void actionBackPressed() {

		DatabaseAdapter testAdapter = new DatabaseAdapter(this);
		testAdapter.open();
		String confirmMsg = testAdapter.getLabel(Constants.CONFIRM_BACK);
		String yesLabel = testAdapter.getLabel(Constants.YES_LABEL);
		String noLabel = testAdapter.getLabel(Constants.NO_LABEL);
		testAdapter.close();

		LayoutInflater li = LayoutInflater.from(MountTireActivity.this);
		View promptsView = li
				.inflate(R.layout.dialog_logout_confirmation, null);
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", false, true, false);
		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
				MountTireActivity.this);
		alertDialogBuilder.setView(promptsView);
		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
		infoTv.setText(confirmMsg);
		alertDialogBuilder.setCancelable(false).setPositiveButton(yesLabel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
						CameraUtility.resetParams();
						Constants.ONSTATE_MOUNT = false;
						Constants.EDITED_SERIAL_NUMBER = "";
						MountTireActivity.this.finish();
					}
				});
		alertDialogBuilder.setCancelable(false).setNegativeButton(noLabel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);
						dialog.cancel();
					}
				});
		mBackAlertDialog = alertDialogBuilder.create();
		mBackAlertDialog.show();
		mBackAlertDialog.setCanceledOnTouchOutside(false);
	}
	/**
	 * Method creating the action-bar menu Items
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.mount_new_tire_menu, menu);
		if (!TextUtils.isEmpty(mNotesLabel)) {
			menu.findItem(R.id.action_notes).setTitle(mNotesLabel);
		}
		return true;
	}
	/**
	 * Method handling the action performed on Action-bar menu
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_notes) {
			LogUtil.TraceInfo(TRACE_TAG, "Option", "Note", false, false, false);
			Intent startNotesActivity = new Intent(this, NoteActivity.class);
			startNotesActivity.putExtra("sentfrom", "regroove");
			startNotesActivity.putExtra("lastnote", sNoteText);
			startActivityForResult(startNotesActivity,
					MountTireActivity.NOTES_INTENT);
		} else if (id == R.id.action_tire) {
			LogUtil.TraceInfo(TRACE_TAG, "Option", "Tire Action", false, false, false);
			Intent startWOTActivity = new Intent(this, WOTActivity.class);

			if (validateDetails()) {
				startWOTActivity.putExtra("SERIAL_NO", mSerialNoFromIntent);
				startWOTActivity.putExtra("WHEEL_POS",
						mSelectedTire.getPosition());
				startWOTActivity.putExtra("FROM_MOUNT_NEW", true);
				startWOTActivity.putExtra("FROM_MOUNT_PWT", false);
				String minNsk = CommonUtils.getMinNskValue(mValueNSK1.getText()
						.toString(), mValueNSK2.getText().toString(),
						mValueNSK3.getText().toString());

				String pressure = CommonUtils.getFinalConvertedPressureValue(
						mRecommendedPressure,
						mSetPressure.getText().toString(), mIsSettingsPSI);
				startWOTActivity.putExtra("MIN_NSK", minNsk);
				startWOTActivity.putExtra(
						VehicleSkeletonFragment.mAXLEPRESSURE,
						CommonUtils.parseFloat(pressure));
				//Fix : Bug 626 (users are reporting that tyre pressures are incorrectly applied to other axles)
				//Fix for Missing Axle Position Information
				startWOTActivity.putExtra(VehicleSkeletonFragment.mAXLEPRESSURE_POS,mAxlePosition);
				startWOTActivity.putExtra("SELECTED_TYRE_EXTERNAL_ID",
						mExternalIdTyre);
				startActivityForResult(startWOTActivity,
						MountTireActivity.WOT_INTENT);
			}

		}
		return super.onOptionsItemSelected(item);
	}
	/**
	 * Method checking if all fields of the design page are selected
	 */
	private boolean validateDetails() {
		if (mBrandSpinner.getSelectedItemPosition() <= 0) {
			CommonUtils.toastHandler(fieldIncorrect, this);
			return false;
		}
		String nsk1 = CommonUtils.getMinNskValue(mValueNSK1.getText()
				.toString(), mValueNSK2.getText().toString(), mValueNSK3
				.getText().toString());
		if (Float.parseFloat(nsk1) <= 0) {
			CommonUtils.toastHandler(fieldIncorrect, this);
			return false;
		}
		String pressure = mSetPressure.getText().toString();
		if (null == pressure || "".equals(pressure)) {
			CommonUtils.toastHandler(fieldIncorrect, this); // INVALID_PRESSURE_ENTRY
			return false;
		} else {
			float val = CommonUtils.parseFloat(pressure);
			if (val > 0.00) {
				return true;
			} else {
				CommonUtils.toastHandler(fieldIncorrect, this);
				return false;
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == MountTireActivity.NOTES_INTENT) {
			if (data != null) {
				sNoteText = data.getExtras().getString("mNoteEditText");
			}
		}
	}

	/**
	 * check if empty or zero
	 * 
	 * @param value
	 * @return
	 */
	private boolean checkIfEmptyOrZero(String value) {
		if (value.equals("") || CommonUtils.parseFloat(value) == 0.0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * @author amitkumar.h 
	 * Class providing the animation when user swipes out
	 * from the activity It also handles the actions performed when use
	 * is swiping out from the activity after matching all the required
	 * conditions
	 */
	public class Swiper implements OnTouchListener, OnClickListener {
		float startX, startY;
		float endX, endY;
		int selectedPositionToDelete;
		ArrayAdapter<String> adapterList;
		ScrollView view;
		private Context ctx;
		public static final float MINIMUM_MOVEMENT_REQUIRED = 100;

		public Swiper(Context ctx, ScrollView view) {
			this.ctx = ctx;
			this.view = view;
			view.setOnClickListener(this);
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getActionMasked()) {
			case MotionEvent.ACTION_DOWN:
				mSwiped = false;
				startX = event.getX();
				startY = event.getY();
				break;
			case MotionEvent.ACTION_UP:
				break;

			case MotionEvent.ACTION_MOVE:
				endX = event.getX();
				endY = event.getY();
				if (Math.abs(endX - startX) > MINIMUM_MOVEMENT_REQUIRED
						&& !mSwiped) {
					mSwiped = true;
					Rect rect = new Rect();
					int childCount = view.getChildCount();
					int[] listViewCoords = new int[2];
					view.getLocationOnScreen(listViewCoords);
					int x = (int) event.getRawX() - listViewCoords[0];
					int y = (int) event.getRawY() - listViewCoords[1];
					View child;
					for (int i = 0; i < childCount; i++) {
						child = view.getChildAt(i);
						child.getHitRect(rect);
						if (rect.contains(x, y)) {
							if (mTyreDesignSpinner.getSelectedItemPosition() < 0
									|| mTyreDesignSpinner.getSelectedItem()
											.toString()
											.equals(mDefaultValueForSpinner)) {
								//User Trace logs
								if(mDesignDetailRequired!=null)
								{
									LogUtil.TraceInfo(TRACE_TAG, "Design Validation","Msg : " +mDesignDetailRequired,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Design Validation","Msg : Check Design",false,false,false);
								}
								CommonUtils.notify(mDesignDetailRequired,
										MountTireActivity.this);
							} else if (mTyreFullDetailSpinner
									.getSelectedItemPosition() < 0
									|| mTyreFullDetailSpinner.getSelectedItem()
											.toString()
											.equals(mDefaultValueForSpinner)) {
								//User Trace logs
								if(mDesignDetailRequired!=null)
								{
									LogUtil.TraceInfo(TRACE_TAG, "Tyre Validation","Msg : " +mDesignDetailRequired,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Tyre Validation","Msg : Check Design",false,false,false);
								}
								CommonUtils.notify(mDesignDetailRequired,
										MountTireActivity.this);
							} else if (!CommonUtils.validateNSKValues(
									mValueNSK1.getText().toString(), mValueNSK2
											.getText().toString(), mValueNSK3
											.getText().toString())) {
								//User Trace logs
								if(Constants.sLblEnterNskValues!=null)
								{
									LogUtil.TraceInfo(TRACE_TAG, "NSK Validation","Msg : " +Constants.sLblEnterNskValues,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "NSK Validation","Msg : Check NSK",false,false,false);
								}

								CommonUtils.notify(
										Constants.sLblEnterNskValues,
										getBaseContext());
							} else if (CommonUtils.parseFloat(mSetPressure
									.getText().toString()) == 0.0) {
								//User Trace logs
								if(mPressureRequired!=null)
								{
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Validation","Msg : " +mPressureRequired,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Validation","Msg : Check Pressure",false,false,false);
								}

								CommonUtils.notify(mPressureRequired,
										getBaseContext());
							} else if (mSetPressure.getText().toString()
									.equals("")) {
								//User Trace logs
								if(mPressureRequired!=null)
								{
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Validation","Msg : " +mPressureRequired,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Validation","Msg : Check Pressure",false,false,false);
								}
								CommonUtils.notify(mPressureRequired,
										getBaseContext());
							} else if (!CommonUtils
									.pressureValueValidation(mSetPressure)) {
								//User Trace logs
								if(Constants.sLblCheckPressureValue!=null)
								{
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Value Validation","Msg : " +Constants.sLblCheckPressureValue,false,false,false);
								}
								else
								{
									LogUtil.TraceInfo(TRACE_TAG, "Pressure Value Validation","Msg : Check Pressure Value",false,false,false);
								}
								CommonUtils.notify(
										Constants.sLblCheckPressureValue,
										getBaseContext());
							} else {
								onReturnSkeleton();
							}
						}
					}
				}
				break;
			}
			return false;
		}

		@Override
		public void onClick(View arg0) {
		}
	}

	/**
	 * When spinner value is null then dependent spinner values refreshing.
	 */
	private void clearSpinnersData(int i) {
		// Don't add break statement, It's sequence of execution
		switch (i) {
		case 1: // Brand Spinner
		case 2: // Size Spinner
			mSizeArrayList.clear();
			if (null != mSizeDataAdapter) {
				mSizeDataAdapter.notifyDataSetChanged();
			}
		case 3: // ASP Spinner
			mTyreTASPArrayList.clear();
			if (null != mASPDataAdapter) {
				mASPDataAdapter.notifyDataSetChanged();
			}
		case 4: // Rim Spinner
			mTyreTRIMArrayList.clear();
			if (null != mRimDataAdapter) {
				mRimDataAdapter.notifyDataSetChanged();
			}
		case 5: // Design Spinner
			mDesignArrayList.clear();
			if (null != mDesignDataAdapter) {
				mDesignDataAdapter.notifyDataSetChanged();
			}
		case 6: // FullDetails Spinner
			mFullDesignArrayList.clear();
			if (mFullDesignDataAdapter != null) {
				mFullDesignDataAdapter.notifyDataSetChanged();
			}
		case 7:
			mValueNSK1.setText("");
			mValueNSK1.setEnabled(false);
			mValueNSK2.setText("");
			mValueNSK2.setEnabled(false);
			mValueNSK3.setText("");
			mValueNSK3.setEnabled(false);

		}
	}
	/**
	 * Method Clearing the Previous Selection Values of the spinners present in
	 * the UI
	 */
	private void clearPreviousSelections(int i) {
		if (mIsOrientationChanged && i < mLastSpinnerSelection) {
			if (i == 7 && mLastSpinnerSelection == 8) {
				mIsOrientationChanged = false;
			}
			return;
		} else if (mIsOrientationChanged) {
			mIsOrientationChanged = false;
		}
		// Don't add break statement, It's sequence of execution
		switch (i) {
		case 1: // Brand Spinner
		case 2: // Size Spinner
			mSelectedtyreSize = "";
		case 3: // ASP Spinner
			mSelectedTyreTASP = "";
		case 4: // Rim Spinner
			mSelectedtyreTRIM = "";
		case 5: // Design Spinner
			mSelectedTyreDesign = "";
		case 6: // FullDetails Spinner
			mSelectedTyreDetailedDesign = "";
		case 7:
			mValueNSK1.setText("");
			mValueNSK1.setEnabled(false);
			mValueNSK2.setText("");
			mValueNSK2.setEnabled(false);
			mValueNSK3.setText("");
			mValueNSK3.setEnabled(false);
		}
	}
	/**
	 * Method Checking the Previous Selected Spinner just before the change in
	 * orientation
	 */
	private void checkLatestSpinnerSelectionBeforeOrientationChanges() {
		if (null == mSelectedBrandName
				|| mPleaseSelectBrand.equals(mSelectedBrandName)) {
			mLastSpinnerSelection = 1;
		} else if (null == mSelectedtyreSize || "".equals(mSelectedtyreSize)) {
			mLastSpinnerSelection = 2;
		} else if (null == mSelectedTyreTASP || "".equals(mSelectedTyreTASP)) {
			mLastSpinnerSelection = 3;
		} else if (null == mSelectedtyreTRIM || "".equals(mSelectedtyreTRIM)) {
			mLastSpinnerSelection = 4;
		} else if (null == mSelectedTyreDesign
				|| mDefaultValueForSpinner.equals(mSelectedTyreDesign)) {
			mLastSpinnerSelection = 5;
		} else if (null == mSelectedTyreDetailedDesign
				|| mDefaultValueForSpinner.equals(mSelectedTyreDetailedDesign)) {
			mLastSpinnerSelection = 6;
		} else if (!TextUtils.isEmpty(mSavedNSK1)) {
			mLastSpinnerSelection = 7;
		} else {
			mLastSpinnerSelection = 8;
		}
	}

	/**
	 * If axle have pressure apply the value then disable otherwise it's
	 * editable Setting the Pressure
	 */
	private void setValuetoPressueView() {
		if (mRecommendedPressure > 0) {
			mSetPressure.setText(CommonUtils.getPressureValue(
					MountTireActivity.this, mRecommendedPressure));
			mPressureUnit.setText(CommonUtils
					.getPressureUnit(MountTireActivity.this));
			mSetPressure.setEnabled(false);
			mBlockPressureFromBluetooth = true;
		} else {
			if (mIsSettingsPSI) {
				mSetPressure
						.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
								4, 2) });
			} else {
				mSetPressure
						.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
								3, 2) });
			}
			/*Bug 728 : Adding context to show toast for higher pressure value*/
			mSetPressure.addTextChangedListener(new PressureCheckListener(
					mSetPressure, mIsSettingsPSI, getApplicationContext()));
			mPressureUnit.setText(CommonUtils
					.getPressureUnit(MountTireActivity.this));
			mValueNSK3.setOnEditorActionListener(new OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId,
						KeyEvent event) {
					boolean handled = false;
					if (actionId == EditorInfo.IME_ACTION_NEXT) {
						mSetPressure.requestFocus();
						handled = true;
					}
					return handled;
				}
			});
		}
	}
	/**
	 * Method Updating recommended axle pressure for the selected axle
	 */
	private void updatePressureValue() {
		if (mSetPressure.isEnabled()
				&& (null != VehicleSkeletonFragment.mAxleRecommendedPressure)) {
			VehicleSkeletonFragment.mAxleRecommendedPressure.set(mAxlePosition,
					CommonUtils.parseFloat(mFinalConvertedPressure));
		}
	}
	/**
	 * Method storing state of the tire when user initiates a
	 * tire operation
	 * @param bundle
	 */
	private void captureInitialState(Bundle bundle) {
		if (bundle != null) {
			mFormElements = bundle.getParcelable("MountTireForm");
		} else {
			mFormElements = new TyreFormElements();
			mFormElements.setBrand(mBrandSpinner.getSelectedItemPosition());
			mFormElements.setDesign(mTyreDesignSpinner
					.getSelectedItemPosition());
			mFormElements.setType(mTyreFullDetailSpinner
					.getSelectedItemPosition());
			mFormElements.setNsk1(mValueNSK1.getText().toString());
			mFormElements.setNsk2(mValueNSK2.getText().toString());
			mFormElements.setNsk3(mValueNSK3.getText().toString());
			mFormElements.setPressure(mSetPressure.getText().toString());
			mFormElements.setRimType(mRimValue.isChecked());
		}
	}

	private void saveInitialState(Bundle bundle) {
		bundle.putParcelable("MountTireForm", mFormElements);
	}
	/**
	 * Method Returns true if any change made
	 * @return
	 */
	private boolean areAnyChangesMade() {
		boolean isSameAsInitial = true;
		isSameAsInitial &= mFormElements.getBrand() == mBrandSpinner
				.getSelectedItemPosition();
		isSameAsInitial &= mFormElements.getDesign() == mTyreDesignSpinner
				.getSelectedItemPosition();
		isSameAsInitial &= mFormElements.getType() == mTyreFullDetailSpinner
				.getSelectedItemPosition();
		isSameAsInitial &= mFormElements.getNsk1().equals(
				mValueNSK1.getText().toString());
		isSameAsInitial &= mFormElements.getNsk2().equals(
				mValueNSK2.getText().toString());
		isSameAsInitial &= mFormElements.getNsk3().equals(
				mValueNSK3.getText().toString());
		isSameAsInitial &= mFormElements.getPressure().equals(
				mSetPressure.getText().toString());
		isSameAsInitial &= mFormElements.isRimType() == mRimValue.isChecked();
		return !isSameAsInitial;
	}
	/**
	 * Method fetching vendor details from the local database file
	 * as per the selected empty tire for mounting
	 */
	public void getVendorDetails() {
		if (null != mDbHelper) {
			mDbHelper.open();
			LogUtil.i("Mount New Tire :: Contract Type",
					Constants.contract.getContractType());
			LogUtil.i("Mount New Tire :: Fitter Type", mFitter_type);
			//passing the vendor name selected during launching a new job
			mVendorId = mDbHelper.getvendorIDFOrMountNewTire(Constants.VENDER_NAME);
			mPurchaseOrg = mDbHelper.getPurchasingOrganization(
					Authentication.getUserName(Constants.USER), mVendorId);
			Cursor curStockLocCode = null;
			try {
				curStockLocCode = mDbHelper.getStockLocationCode(
						Authentication.getUserName(Constants.USER), mVendorId,
						mPurchaseOrg);
				if (CursorUtils.isValidCursor(curStockLocCode)
						&& curStockLocCode.moveToFirst()) {
					mStockLocation = curStockLocCode.getString(curStockLocCode
							.getColumnIndex("StockLocationCode"));
					mPurchasePlant = curStockLocCode.getString(curStockLocCode
							.getColumnIndex("PurchasingPlant"));
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				CursorUtils.closeCursor(curStockLocCode);
			}
		}
		LogUtil.d("VendorDetails", mVendorId + "::" + mPurchaseOrg + "::"
				+ mPurchasePlant + "::" + mStockLocation);
	}

	/*
	 * (non-Javadoc)
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		sNoteText = "";
		InactivityUtils.logoutFromActivityAboveEjobForm(this);
	}
}
