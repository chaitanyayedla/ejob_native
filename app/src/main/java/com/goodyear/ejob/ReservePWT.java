package com.goodyear.ejob;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.goodyear.ejob.adapter.JobList_Adapter;
import com.goodyear.ejob.adapter.JobList_Adapter_Saved;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.ContractTypes;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.JsonParser;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.ReservePWTSearchState;
import com.goodyear.ejob.util.ReservePWTUtils;
import com.goodyear.ejob.util.TireDesignDetails;
import com.goodyear.ejob.util.Tyre;
import com.goodyear.ejob.util.TyreConstants;
import com.goodyear.ejob.util.VehicleSkeletonInfo;

public class ReservePWT extends Activity implements InactivityHandler {

	private ImageButton mBtnSearch;
	private EditText mSearchBox;

	private ListView mListView, mListViewTyreAction;
	JobList_Adapter mAdapter, mAdapterTyreAction;
	JobList_Adapter_Saved mAdapterTyreActionSaved;
	public ArrayList<HashMap<String, String>> mListItems = new ArrayList<HashMap<String, String>>();

	HashMap<String, String> mHmap = null, mHmapTyreAction = null;
	//
	public static LinearLayout mll_result;
	final Context mContext = this;
	private static DatabaseAdapter mDbHelper;

	// public ArrayList<Tyre> mTyreInfo;
	// public ArrayList<String> mTyreIDList;
	public static String mCustomerID;
	private TextView mSerailheader;
	private TextView mDesignHeader;
	private TextView mResultText;
	private TextView mResultDesign;
	private int flag = 0;
	private MenuItem mButtonSaved;
	private String mSearchResultSerialNum = null;
	private String mSearchResultDesign = null;
	private String mSapVendorID;
	private TextView mActionSerailheader;
	private TextView mResult;
	private TextView mYourActionText;
	private int mGetCount;
	private int mContractPolicy;
	/*
	 * private String mTyreDiameter; private String mTyreSize; private String
	 * mTyreASP;
	 */
	private String mContrantID;
	private ArrayList<String> mBundleSerielNOVec;
	private TextView mActionDesignheader;
	protected String mTyreIDSelected;
	int mRetainSwipedItem;
	private int mDeleteDialogPos;
	public int mRemovetirevec_pos;
	Bundle mInstanceState;
	private String mComingFrom;
	private final String BUNDLE_COMING_FROM = "COMING_FROM";
	private final String COMING_FROM_SELECTED_TIRE = "selectedtire";
	private final String COMING_FROM_TOP_CORNER = "topcorner";
	private String strALLREADY_ADDED;
	private String strALLREADY_ADDEDVEHICLE, strNoDatatoSave;
	private String mLblNoTyresFound = "No tyres found";
	//User Trace logs trace tag
	private static final String TRACE_TAG = "Reserve PWT";

	private static int mReservePWTSearchState;

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mDbHelper = new DatabaseAdapter(this);
		mDbHelper.open();
		setTitle(mDbHelper.getLabel(Constants.LABEL_RESERVE_PWT));
		setContentView(R.layout.activity_reserve_pwt);
		//User Trace logs
		LogUtil.TraceInfo(TRACE_TAG, "none", "OP", true, false, true);
		mListView = (ListView) findViewById(R.id.list_view_search_result_pwt);
		mListView.setDivider(null);
		mListView.setDividerHeight(4);
		mListViewTyreAction = (ListView) findViewById(R.id.list_view_ActionOnTyre_pwt);
		mListViewTyreAction.setDivider(null);
		mListViewTyreAction.setDividerHeight(4);
		mBtnSearch = (ImageButton) findViewById(R.id.btnSearch_pwt);
		mSearchBox = (EditText) findViewById(R.id.edt_search_pwt);
		mSearchBox.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
				20) });
		mResultText = (TextView) findViewById(R.id.result_text_pwt);
		mResultDesign = (TextView) findViewById(R.id.tyre_design_result);
		loadScreenLabelsFromDB();
		mDeleteDialogPos = -1;

		mInstanceState = savedInstanceState;
		if (mInstanceState != null) {
			mListItems = TyreConstants.getBundle_ListItems();
			Constants.Search_TM_ListItems = TyreConstants
					.getBundle_Search_TM_ListItems();
			mRetainSwipedItem = mInstanceState.getInt("retainSwipedItem");
			mDeleteDialogPos = mInstanceState.getInt("retainDeleteDialog");
			if (null != mButtonSaved) {
				if (mInstanceState.getBoolean("retainButtonSaved")) {
					mButtonSaved.setEnabled(true);
					mButtonSaved.setIcon(R.drawable.savejob_navigation);
				} else {
					mButtonSaved.setEnabled(false);
					mButtonSaved.setIcon(R.drawable.unsavejob_navigation);
				}
			}
			mComingFrom = mInstanceState.getString("RETAINCOMINGFROM");
		} else {
			mListItems = new ArrayList<HashMap<String, String>>();
			if (TyreConstants.Bundle_Search_TM_ListItems_Temporary != null
					&& TyreConstants.Bundle_Search_TM_ListItems_Temporary
							.size() > 0) {
				TyreConstants.Bundle_Search_TM_ListItems_Temporary.clear();
			}

			if (Constants.Search_TM_ListItems != null
					&& Constants.Search_TM_ListItems.size() > 0) {
				// mListItems = Constants.Search_TM_ListItems;
				TyreConstants
						.setBundle_ListItems(Constants.Search_TM_ListItems);
			} else {
				Constants.Search_TM_ListItems = new ArrayList<HashMap<String, String>>();
			}

			mRetainSwipedItem = -1;

			mComingFrom = COMING_FROM_TOP_CORNER;
			if (getIntent().hasExtra(BUNDLE_COMING_FROM)) {
				mComingFrom = getIntent().getExtras().getString(
						BUNDLE_COMING_FROM);
			}
		}
		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				hideKeyboard(view);

				Constants._imageVisiblity = false;

				mHmapTyreAction = (HashMap<String, String>) mListView
						.getItemAtPosition(position);

				mSearchResultSerialNum = mHmapTyreAction.get("licence");
				mSearchResultDesign = mHmapTyreAction.get("cname");
				mTyreIDSelected = mHmapTyreAction.get("TYRE_ID");
				TyreConstants
						.setSerialNumberSearchResult(mSearchResultSerialNum);
				TyreConstants.setDesignSearchResult(mSearchResultDesign);

				HashMap<String, String> tm_map = new HashMap<String, String>();
				tm_map.put("licence", mSearchResultSerialNum);
				tm_map.put("cname", mSearchResultDesign);
				tm_map.put("TYRE_ID", mTyreIDSelected);
				tm_map.put("status", "false");
				if (!Constants.Search_TM_ListItems.contains(tm_map)) {

					if (!iSSerialInVehicle(mSearchResultSerialNum)
							&& !isPWTTyreInList(mSearchResultSerialNum)) {
						Constants.Search_TM_ListItems.add(tm_map);
						mButtonSaved.setIcon(R.drawable.savejob_navigation);
						mButtonSaved.setEnabled(true);
						TyreConstants.Bundle_Search_TM_ListItems_Temporary
								.add(tm_map);
						//User Trace logs
						try
						{
							LogUtil.TraceInfo(TRACE_TAG,"1 Tyre Selected",mHmapTyreAction.toString(),false,true,false);
						}catch(Exception e)
						{
							LogUtil.TraceInfo(TRACE_TAG,"Tyre Selection","Exception : "+e.getMessage(),false,true,false);
						}
					} else {
						//User Trace logs
						LogUtil.TraceInfo(TRACE_TAG,"Tyre Selection","Msg : "+strALLREADY_ADDEDVEHICLE,false,true,false);
						Toast.makeText(getApplicationContext(),
								strALLREADY_ADDEDVEHICLE, Toast.LENGTH_SHORT)
								.show();
					}
				} else {
					//User Trace logs
					LogUtil.TraceInfo(TRACE_TAG,"Tyre Selection","Msg : "+strALLREADY_ADDED,false,true,false);
					Toast.makeText(getApplicationContext(), strALLREADY_ADDED,
							Toast.LENGTH_SHORT).show();
				}
				if (Constants.Search_TM_ListItems != null) {
					mAdapterTyreActionSaved = new JobList_Adapter_Saved(
							ReservePWT.this, Constants.Search_TM_ListItems);
				}
				mListViewTyreAction.setVisibility(View.VISIBLE);
				mListViewTyreAction.setAdapter(mAdapterTyreActionSaved);
				mGetCount = mListViewTyreAction.getCount();
			}
		});

		mListViewTyreAction
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int arg2, long arg3) {
						confirmDeleteDialog(arg2);

						return true;
					}
				});
		mSearchBox.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable arg0) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				try {
					if (!mSearchBox.getText().toString().equals("")) {
						searchTyreResultAction();
					} else {
						mListView.setAdapter(null);
						mListItems.clear();
						if (Constants.getBundle_Retained_Jobitem() != null
								&& Constants.getBundle_Retained_Jobitem()
										.size() > 0) {
							Constants.getBundle_Retained_Jobitem().clear();
						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		if (mDeleteDialogPos > -1) {
			confirmDeleteDialog(mDeleteDialogPos);
		}
		restoreDialogState(savedInstanceState);
	}

	/**
	 * Getting tires to be reserved
	 */
	protected void searchTyreResultAction() {
		// hideKeyboard(v);
		ArrayList<Tyre> mTyreInfo = new ArrayList<Tyre>();
		if (mSearchBox.getText().toString().equals("")) {
			Toast.makeText(getApplicationContext(),
					Constants.sLblEnterSerialNumber, Toast.LENGTH_SHORT).show();// 367
		} else {
			mListView.setVisibility(View.VISIBLE);
			mListItems = new ArrayList<HashMap<String, String>>();
			Cursor mCursor = null;

			// fetching dimension from vehicle
			try {

				/*
				 * if(Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW){
				 * mTyreInfo = Constants.TIRE_INFO; }else{ mTyreInfo =
				 * JsonParser .getTyreInfo(Constants.tireJson); }
				 */
				try {
					if (mTyreInfo != null) {
						mTyreInfo.clear();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (Constants.COMESFROMUPDATE || Constants.COMESFROMVIEW) {
					// mTyreInfo = Constants.TIRE_INFO;
					mTyreInfo.addAll(Constants.TIRE_INFO);
				} else if (!TextUtils.isEmpty(Constants.tireJson)
						&& JsonParser.getTyreInfo(Constants.tireJson) != null
						&& JsonParser.getTyreInfo(Constants.tireJson).size() > 0) {
					// mTyreInfo = JsonParser.getTyreInfo(Constants.tireJson);
					mTyreInfo
							.addAll(JsonParser.getTyreInfo(Constants.tireJson));

				}
				if (VehicleSkeletonFragment.tyreInfo != null
						&& VehicleSkeletonFragment.tyreInfo.size() > 0) {
					mTyreInfo.addAll(VehicleSkeletonFragment.tyreInfo);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if (mTyreInfo == null && mTyreInfo.size() == 0) {
				return;
			}
			ArrayList<String> mTyreDiameterArr = new ArrayList<String>();
			ArrayList<String> mTyreSizeArr = new ArrayList<String>();
			ArrayList<String> mTyreASPArr = new ArrayList<String>();
			mReservePWTSearchState= ReservePWTSearchState.NONE;

			if(Constants.contract.getIsDIffSizeAllowedForVehicle().equalsIgnoreCase("true"))
			{
				mReservePWTSearchState=ReservePWTSearchState.DIFFERENT_SIZE_ALLOWED_FOR_VEHICLE;
			}
			else {
				if (mComingFrom.equals(COMING_FROM_SELECTED_TIRE)) {
					TireDesignDetails detail = CommonUtils
							.getDetailsFromTyreInSameAxle(Constants.SELECTED_TYRE);
					mReservePWTSearchState=ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_SELECTED_TYRE_WITH_NON_MAINTAINED_AXLE;
					if (null != detail) {
					/*
					 * mTyreSize = detail.getSize(); mTyreASP = detail.getAsp();
					 * mTyreDiameter = detail.getRim();
					 */
						String diameter = detail.getRim();
						String size = detail.getSize();
						String asp = detail.getAsp();
						mTyreDiameterArr.add(diameter);
						mTyreSizeArr.add(size);
						mTyreASPArr.add(asp);
						mReservePWTSearchState=ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_SELECTED_TYRE_WITH_MAINTAINED_AXLE;
					}
				} else if (mComingFrom.equals(COMING_FROM_TOP_CORNER)) {
					VehicleSkeletonInfo vehicleInfo = new VehicleSkeletonInfo(Constants.vehicleConfiguration);
					int axleCount = vehicleInfo.getAxlecount();
					int maintainedAxleCount = 0;
					ArrayList<Boolean> isMaintainedAxle = new ArrayList();
					for (int x = 0; x < axleCount; x++) {
						isMaintainedAxle.add(x, false);
					}

					for (int i = 0; i < mTyreInfo.size(); i++) {
						Tyre tire = mTyreInfo.get(i);
						if (!(tire.isDismounted() && Boolean.getBoolean(tire
								.isSpare()))) {
							String diameter = tire.getRim();
							String size = tire.getSize();
							String asp = tire.getAsp();
							int position = Integer.parseInt(tire.getPosition().substring(0, 1));

							if (!TextUtils.isEmpty(diameter)
									&& !TextUtils.isEmpty(size)) {
								if (CommonUtils.parseFloat(diameter) > 0
										|| CommonUtils.parseFloat(size) > 0) {
									mTyreDiameterArr.add(diameter);
									mTyreSizeArr.add(size);
									mTyreASPArr.add(asp);
									try {
										if (!isMaintainedAxle.get(position - 1)) {
											isMaintainedAxle.set(position - 1, true);
										}
									} catch(Exception e){
										LogUtil.d(" "," Exception :  "+ e.getMessage());
									}
								}
							}
						}
					}

					for (int x = 0; x < axleCount; x++) {
						if(isMaintainedAxle.get(x))
						{
							maintainedAxleCount++;
						}
					}
					if(maintainedAxleCount==axleCount)
					{
						mReservePWTSearchState=ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_TYRES_COMPLETELY_MAINTAINED;
					} else if(maintainedAxleCount==0) {
						mReservePWTSearchState=ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_TYRES_NON_MAINTAINED;
					}
					else
					{
						mReservePWTSearchState=ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_TYRES_PARTIALLY_MAINTAINED;
					}

				} else {
					mReservePWTSearchState=ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_SELECTED_TYRE_WITH_NON_MAINTAINED_AXLE;
					for (int i = 0; i < mTyreInfo.size(); i++) {
						Tyre tire = mTyreInfo.get(i);
						if (!(tire.isDismounted() && Boolean.getBoolean(tire
								.isSpare()))) {
						/*
						 * mTyreSize = tire.getSize(); mTyreASP = tire.getAsp();
						 * mTyreDiameter = tire.getRim();
						 */
							String diameter = tire.getRim();
							String size = tire.getSize();
							String asp = tire.getAsp();
							if (!TextUtils.isEmpty(diameter)
									&& !TextUtils.isEmpty(size)) {
								if (CommonUtils.parseFloat(diameter) > 0
										&& CommonUtils.parseFloat(size) > 0) {
									mTyreDiameterArr.add(diameter);
									mTyreSizeArr.add(size);
									mTyreASPArr.add(asp);
									mReservePWTSearchState=ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_SELECTED_TYRE_WITH_MAINTAINED_AXLE;
									break;
								}
							}
						}
					}
				}
			}

			if (Constants.SAP_VENDORCODE_ID == null
					|| Constants.SAP_VENDORCODE_ID.equalsIgnoreCase("-1")
					|| Constants.SAP_VENDORCODE_ID.equalsIgnoreCase("")) {
				mSapVendorID = getVendorSAPID(Constants.VENDER_NAME);
				LogUtil.d(TRACE_TAG," getVendorSAPID, SAP vendor ID : " + mSapVendorID);
				Constants.SAP_VENDORCODE_ID = mSapVendorID;
			}

			if (Constants.contract.getContractType().equals(ContractTypes.ZYPG)) {

				/**
				 * Ejob-2 : where a vehicle has an axle non maintained, yet other axle(s) are maintained the PWT search results should not only be limited to the tyres on the maintained axle
				 * IsDIffSizeAllowedForVehicle check condition removed
				 * Changes made in eJob version number: 1.3.4.6
				 */
				if (mReservePWTSearchState==ReservePWTSearchState.NONE
						|| mReservePWTSearchState==ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_TYRES_NON_MAINTAINED
						|| mReservePWTSearchState==ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_TYRES_PARTIALLY_MAINTAINED
						|| mReservePWTSearchState==ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_SELECTED_TYRE_WITH_NON_MAINTAINED_AXLE
						|| mReservePWTSearchState==ReservePWTSearchState.DIFFERENT_SIZE_ALLOWED_FOR_VEHICLE) {

					mCursor = mDbHelper.getTyreSearchResultForReservePWT(
							mSearchBox.getText().toString(),
							Constants.SAP_VENDORCODE_ID,
							Constants.SAP_CUSTOMER_ID_JOB);
				} else {
					mCursor = mDbHelper
							.getTyreSearchResultForReservePWTWithSize(
									mSearchBox.getText().toString(),
									mTyreDiameterArr, mTyreSizeArr,
									mTyreASPArr, Constants.SAP_CUSTOMER_ID_JOB,
									Constants.SAP_VENDORCODE_ID);

				}

//				/**
//				 * Ejob-2 : where a vehicle has an axle non maintained, yet other axle(s) are maintained the PWT search results should not only be limited to the tyres on the maintained axle
//				 * Search PWT without Size attribute
//				 * Changes made in eJob version number: 1.3.4.6
//				 */
//				mCursor = mDbHelper.getTyreSearchResultForReservePWT(
//						mSearchBox.getText().toString(),
//						Constants.SAP_VENDORCODE_ID,
//						Constants.SAP_CUSTOMER_ID_JOB);

			} else if (Constants.contract.getContractType().equals(
					ContractTypes.ZYMC)
					|| Constants.contract.getContractType().equals(
							ContractTypes.ZYBC)) {
				/**
				 * Ejob-2 : where a vehicle has an axle non maintained, yet other axle(s) are maintained the PWT search results should not only be limited to the tyres on the maintained axle
				 * IsDIffSizeAllowedForVehicle check condition removed
				 * Changes made in eJob version number: 1.3.4.6
				 */
				if (mReservePWTSearchState==ReservePWTSearchState.NONE
						|| mReservePWTSearchState==ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_TYRES_NON_MAINTAINED
						|| mReservePWTSearchState==ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_TYRES_PARTIALLY_MAINTAINED
						|| mReservePWTSearchState==ReservePWTSearchState.DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_SELECTED_TYRE_WITH_NON_MAINTAINED_AXLE
						|| mReservePWTSearchState==ReservePWTSearchState.DIFFERENT_SIZE_ALLOWED_FOR_VEHICLE) {
					mCursor = mDbHelper.getTyreSearchResultForReservePWTWithCP(
							mSearchBox.getText().toString(),
							Constants.VENDORCOUNTRY_ID,
							Constants.SAP_VENDORCODE_ID);
				} else {
					mCursor = mDbHelper
							.getTyreSearchResultForReservePWTWithCPAndSize(
									mSearchBox.getText().toString(),
									mTyreDiameterArr, mTyreSizeArr,
									mTyreASPArr, Constants.VENDORCOUNTRY_ID,
									Constants.SAP_VENDORCODE_ID);
				}

//				/**
//				 * Ejob-2 : where a vehicle has an axle non maintained, yet other axle(s) are maintained the PWT search results should not only be limited to the tyres on the maintained axle
//				 * Search PWT without Size attribute
//				 * Changes made in eJob version number: 1.3.4.6
//				 */
//				mCursor = mDbHelper.getTyreSearchResultForReservePWTWithCP(
//						mSearchBox.getText().toString(),
//						Constants.VENDORCOUNTRY_ID,
//						Constants.SAP_VENDORCODE_ID);
			}

			System.out.println("mCursor.getCount()--" + mCursor.getCount());
			if (CursorUtils.isValidCursor(mCursor))// 0 i.e Cursor is empty

			{
				mCursor.moveToFirst();
				while (!mCursor.isAfterLast()) {
					mHmap = new HashMap<String, String>();
					mHmap.put("licence", mCursor.getString(mCursor
							.getColumnIndex("SerialNumber")));// Serial

					mHmap.put("cname", mCursor.getString(mCursor
							.getColumnIndex("FullTireDetails")));// Design

					mHmap.put("TYRE_ID",
							mCursor.getString(mCursor.getColumnIndex("ID")));

					mListItems.add(mHmap);
					mCursor.moveToNext();
				}
				CursorUtils.closeCursor(mCursor);
			} else {
				mListItems.removeAll(mListItems);
				System.out.println("Sorry, No Data Found!!!!");
				mListView.setVisibility(View.INVISIBLE);
				Toast toast = Toast.makeText(getApplicationContext(),
						mLblNoTyresFound, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.show();

			}

			mAdapter = new JobList_Adapter(ReservePWT.this, mListItems);
			mListView.setAdapter(mAdapter);
			TyreConstants.setBundle_ListItems(mListItems);
			//User Trace logs
			try
			{
				String Data="";
				for(int x=0;x<mListItems.size();x++)
				{
					Data=Data +mListItems.get(x).toString() ;
					if(x-1<mListItems.size())
					{
						Data =Data+ " , ";
					}
				}
				LogUtil.TraceInfo(TRACE_TAG,"Search Result","\n\t\tData :  ["+Data+"]",false,true,false);
			}catch (Exception e)
			{
				LogUtil.TraceInfo(TRACE_TAG,"Search Result","Exception : "+e.getMessage(),false,true,false);
			}
			mHmapTyreAction = new HashMap<String, String>();

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mDbHelper.close();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mDbHelper = new DatabaseAdapter(this);
		mDbHelper.open();
		if (!Constants.Search_TM_ListItems.isEmpty()) {
			for (int i = 0; i < Constants.Search_TM_ListItems.size(); i++) {
				String serialNum = Constants.Search_TM_ListItems.get(i).get(
						"licence");
				if (iSSerialInVehicle(serialNum)) {
					Constants.Search_TM_ListItems.remove(i);
				}
			}
			mAdapterTyreActionSaved = new JobList_Adapter_Saved(
					ReservePWT.this, Constants.Search_TM_ListItems);
			mListViewTyreAction.setVisibility(View.VISIBLE);
			mListViewTyreAction.setAdapter(mAdapterTyreActionSaved);
			mGetCount = mListViewTyreAction.getCount();
			Log.v("getBundle_ListItem SIZE", "_"
					+ TyreConstants.getBundle_ListItems().size());
			if (!mListItems.isEmpty()) {
				mListItems = TyreConstants.getBundle_ListItems();
				mAdapter = new JobList_Adapter(ReservePWT.this, mListItems);
				mListView.setAdapter(mAdapter);
				TyreConstants.setBundle_ListItems(mListItems);
			}
		}
	}

	private boolean isPWTTyreInList(String serialNumber) {
		for (int i = 0; i < VehicleSkeletonFragment.mReserveTyreList.size(); i++) {
			if (VehicleSkeletonFragment.mReserveTyreList.get(i).equals(
					serialNumber)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * action on delete
	 */
	private void confirmDeleteDialog(final int mPosition) {
		try {
			mDeleteDialogPos = mPosition;
			LayoutInflater li = LayoutInflater.from(ReservePWT.this);
			View promptsView = li.inflate(R.layout.dialog_logout_confirmation,
					null);
			final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
					ReservePWT.this);
			alertDialogBuilder.setView(promptsView);

			// update user activity for dialog layout
			LinearLayout rootNode = (LinearLayout) promptsView
					.findViewById(R.id.layout_root);
			rootNode.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					InactivityUtils.updateActivityOfUser();
				}
			});

			TextView infoTv = (TextView) promptsView
					.findViewById(R.id.logout_msg);
			mRetainSwipedItem = mPosition;
			infoTv.setText(mDbHelper.getLabel(339));
			alertDialogBuilder.setCancelable(false).setPositiveButton(
					mDbHelper.getLabel(331),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							// update user activity on button click
							alertDialogBuilder.updateInactivityForDialog();

							try {
								if (Constants.Search_TM_ListItems.size() != 0) {

									System.out.println("Search_TM_ListItems.size: "
											+ Constants.Search_TM_ListItems
													.size()
											+ " & "
											+ "listViewTyreAction.getCount(): "
											+ mListViewTyreAction.getCount());

									String getLiecenceNO = Constants.Search_TM_ListItems
											.get(mPosition).get("licence");
									
									String tyreID = Constants.Search_TM_ListItems
											.get(mPosition).get("TYRE_ID");
									
									mDbHelper.deleteReservedTire(tyreID);
									try {

										HashMap<String, String> removableobject = Constants.Search_TM_ListItems
												.get(mPosition);
										if (TyreConstants.Bundle_Search_TM_ListItems_Temporary
												.contains(removableobject)) {
											TyreConstants.Bundle_Search_TM_ListItems_Temporary
													.remove(removableobject);
										}
										Constants.Search_TM_ListItems
												.remove(mPosition);
										// mAdapterTyreActionSaved.refresh(Constants.Search_TM_ListItems);

										mAdapterTyreActionSaved
												.notifyDataSetChanged();
										mRetainSwipedItem = -1;
									} catch (Exception e) {
										e.printStackTrace();
									}
									try {
										Constants.mBundleSerielNOVec
												.remove(getLiecenceNO);
									} catch (Exception e) {
										e.printStackTrace();
									}
									// mBundleSerialNum = null;

									if (Constants.Search_TM_ListItems.size() == 0) {
										mButtonSaved
												.setIcon(R.drawable.unsavejob_navigation);
										mButtonSaved.setEnabled(false);
									} else {
										mButtonSaved
												.setIcon(R.drawable.savejob_navigation);
										mButtonSaved.setEnabled(true);
									}
									//User Trace logs
									LogUtil.TraceInfo(TRACE_TAG,"1 Item Deleted","Tyre ID : "+tyreID,false,true,false);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}


							Log.v("listViewTyreAction.getTag()", "_"
									+ mListViewTyreAction.getTag());
							/*
							 * if(Constants._tireIDVec != null &&
							 * mSearch_TM_ListItems != null){ if
							 * (Constants._tireIDVec.size() == 1 &&
							 * mSearch_TM_ListItems
							 * .get(mUndoPosition).get("status"
							 * ).equalsIgnoreCase("true") ||
							 * Constants._tireIDVec.size() == 0) {
							 * buttonSaved.setIcon
							 * (R.drawable.unsavejob_navigation);
							 * buttonSaved.setEnabled(false); } }
							 */
							mDeleteDialogPos = -1;
						}
					});
			alertDialogBuilder.setCancelable(false).setNegativeButton(
					mDbHelper.getLabel(332),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							// update user activity on button click
							alertDialogBuilder.updateInactivityForDialog();

							mDeleteDialogPos = -1;
							dialog.cancel();
						}
					});
			AlertDialog alertDialog = alertDialogBuilder.create();
			alertDialog.show();
			alertDialog.setCanceledOnTouchOutside(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		if (mSearchBox != null && mSearchBox.getText().toString().isEmpty()) {
			LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press", false, false, false);
			goBack();
		} else {
			actionBackPressed();
		}
	}

	private AlertDialog mBackAlertDialog;
	private String mCancelJobLabel;
	private String mNOLabel;
	private String mYESLabel;

	private void saveDialogState(Bundle state) {
		state.putBoolean("mBackAlertDialog",
				(mBackAlertDialog != null && mBackAlertDialog.isShowing()));
	}

	private void restoreDialogState(Bundle state) {
		if (state != null) {
			if (state.getBoolean("mBackAlertDialog")) {
				actionBackPressed();
			}
		}
	}

	@Override
	protected void onDestroy() {
		try {
			if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
				mBackAlertDialog.dismiss();
			}
			if(mDbHelper != null){
				mDbHelper.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	/**
	 * 
	 */
	private void actionBackPressed() {
		// TODO Auto-generated method stub

		try {
			LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", false, true, false);
			LayoutInflater li = LayoutInflater.from(ReservePWT.this);
			View promptsView = li.inflate(R.layout.dialog_logout_confirmation,
					null);

			final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
					ReservePWT.this);
			alertDialogBuilder.setView(promptsView);

			// update user activity for dialog layout
			LinearLayout rootNode = (LinearLayout) promptsView
					.findViewById(R.id.layout_root);
			rootNode.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					InactivityUtils.updateActivityOfUser();
				}
			});

			// Setting The Info
			TextView infoTv = (TextView) promptsView
					.findViewById(R.id.logout_msg);
			infoTv.setText(mCancelJobLabel);// 241

			alertDialogBuilder.setCancelable(false).setPositiveButton(mYESLabel,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							// update user activity on button click
							alertDialogBuilder.updateInactivityForDialog();
							LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
							goBack();

						}
					});

			alertDialogBuilder.setCancelable(false).setNegativeButton(mNOLabel,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							// update user activity on button click
							alertDialogBuilder.updateInactivityForDialog();
							LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);
							// if user select "No", just cancel this dialog and
							// continue
							// with app
							dialog.cancel();
						}
					});

			// create alert dialog
			mBackAlertDialog = alertDialogBuilder.create();

			// show it
			mBackAlertDialog.show();
			mBackAlertDialog.setCanceledOnTouchOutside(false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void goBack() {
		// if this button is clicked, close
		// current activity
		for (int i = 0; i < TyreConstants.Bundle_Search_TM_ListItems_Temporary
				.size(); i++) {
			HashMap<String, String> mTemporaryObject = TyreConstants.Bundle_Search_TM_ListItems_Temporary
					.get(i);
			if (Constants.Search_TM_ListItems.contains(mTemporaryObject)) {
				Constants.Search_TM_ListItems.remove(mTemporaryObject);
			}
		}
		this.finish();
	}

	protected void hideKeyboard(View view) {
		InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public String getVendorSAPID(String vendorName) {
		// TODO Auto-generated method stub
		Cursor mCursor = null;
		String sapVendorID = "";
		try {
			mCursor = mDbHelper.getvendorIDByNameFromVendorTable(vendorName);
			if (CursorUtils.isValidCursor(mCursor)) {
				sapVendorID = mCursor.getString(mCursor
						.getColumnIndex("ID"));

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
		}
		return sapVendorID;
	}

	// getLabels
	private void loadScreenLabelsFromDB() {
		// TODO Auto-generated method stub
		try {

			String SearchHint = mDbHelper.getLabel(367);
			mSearchBox.setHint(SearchHint);

			strNoDatatoSave = mDbHelper.getLabel(Constants.NoDatatoSave);
			strALLREADY_ADDEDVEHICLE = mDbHelper
					.getLabel(Constants.ALREADYADDEDVEHICLE);
			strALLREADY_ADDED = mDbHelper.getLabel(Constants.ALLREADY_ADDED);
			String Designheaderlabel = mDbHelper.getLabel(68);
			mDesignHeader = (TextView) findViewById(R.id.headerDesign_pwt);
			mDesignHeader.setText(Designheaderlabel);

			String YourResult = mDbHelper.getLabel(429);
			mResult = (TextView) findViewById(R.id.yourResult_pwt);
			mResult.setText(YourResult);

			String YourAction = mDbHelper.getLabel(417);
			mYourActionText = (TextView) findViewById(R.id.actionTyre_text_pwt);
			mYourActionText.setText(YourAction);

			String Serailheaderlabel = mDbHelper.getLabel(418);
			mSerailheader = (TextView) findViewById(R.id.headerSerialNum_pwt);
			mSerailheader.setText(Serailheaderlabel);
			mActionSerailheader = (TextView) findViewById(R.id.header_SerialNumber_pwt);
			mActionSerailheader.setText(Serailheaderlabel);
			mActionDesignheader = (TextView) findViewById(R.id.header_design_pwt);
			mActionDesignheader.setText(Designheaderlabel);
			mLblNoTyresFound = mDbHelper.getLabel(Constants.NO_TYRES_FOUND);
			mCancelJobLabel = mDbHelper.getLabel(Constants.CONFIRM_BACK);
			mNOLabel = mDbHelper.getLabel(Constants.NO_LABEL);
			mYESLabel = mDbHelper.getLabel(Constants.YES_LABEL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		MenuItem action_settings = menu.findItem(R.id.action_settings);
		action_settings.setVisible(false);

		MenuItem action_about = menu.findItem(R.id.action_about);
		action_about.setVisible(false);

		MenuItem action_help = menu.findItem(R.id.action_help);
		action_help.setVisible(false);

		MenuItem action_logout = menu.findItem(R.id.action_logout);
		action_logout.setVisible(false);

		MenuItem action_summary = menu.findItem(R.id.action_summary);
		action_summary.setVisible(false);

		MenuItem action_summary_back = menu.findItem(R.id.action_save_summary);
		action_summary_back.setVisible(false);

		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tyremanagement_menu, menu);

		mButtonSaved = menu.findItem(R.id.action_save);
		mButtonSaved.setIcon(R.drawable.unsavejob_navigation);
		mButtonSaved.setEnabled(false);

		if (mInstanceState != null) {

			if (mInstanceState.containsKey("retainButtonSaved")) {
				boolean retainedButtonStatus = mInstanceState
						.getBoolean("retainButtonSaved");
				if (retainedButtonStatus) {
					mButtonSaved.setEnabled(true);
					mButtonSaved.setIcon(R.drawable.savejob_navigation);
				} else {
					mButtonSaved.setEnabled(false);
					mButtonSaved.setIcon(R.drawable.unsavejob_navigation);
				}
			}
		}

		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();

		if (id == R.id.action_save) {
			LogUtil.TraceInfo(TRACE_TAG, "Option", "Save", false, true, false);
			if (mListViewTyreAction.getCount() == 0) {
				Toast toast = Toast.makeText(getApplicationContext(),
						strNoDatatoSave, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.show();
			} /*
			 * else if (Constants.UNDO_STATE == true) {// commented as per
			 * Amit's suggestion Toast.makeText(getApplicationContext(),
			 * "Please Conplete the DELETE operation!!",
			 * Toast.LENGTH_SHORT).show(); } else if (mListView.getCount() == 0)
			 * { Toast.makeText( getApplicationContext(),
			 * "Already Reseved, Please Search Another Serial Number!!",
			 * Toast.LENGTH_SHORT).show(); }
			 */else {
				ReservePWTUtils pwtutils = new ReservePWTUtils();
				pwtutils.updateReserveTyre(ReservePWT.this);
				finish();
			}

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onSaveInstanceState(Bundle newState) {
		super.onSaveInstanceState(newState);

		newState.putInt("retainSwipedItem", mRetainSwipedItem);
		newState.putInt("retainDeleteDialog", mDeleteDialogPos);
		newState.putBoolean("retainButtonSaved", mButtonSaved.isEnabled());
		newState.putString("RETAINCOMINGFROM", mComingFrom);
		try {
			TyreConstants.setBundle_ListItems(mListItems);
			System.out.println(mListItems.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TyreConstants
				.setBundle_Search_TM_ListItems(Constants.Search_TM_ListItems);
		System.out.println(Constants.Search_TM_ListItems.size());
		saveDialogState(newState);
	}

	public Boolean iSSerialInVehicle(String reservedSerialNumber) {

		boolean check = false;
		try {

			Iterator<Tyre> itrate = VehicleSkeletonFragment.tyreInfo.iterator();
			while (itrate.hasNext()) {
				Tyre currentTire = itrate.next();
				if (currentTire.getSerialNumber().equalsIgnoreCase(
						reservedSerialNumber)) {
					check = true;
					break;
				} else {
					check = false;
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return check;

	}

	@Override
	public void onStart() {
		super.onStart();
		LogoutHandler.setCurrentActivity(this);
	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		InactivityUtils.logoutFromActivityAboveEjobForm(this);
	}
}
