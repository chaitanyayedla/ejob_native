/**
 * Work on Tire Operations
 */
package com.goodyear.ejob;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.goodyear.ejob.blutooth.BluetoothService;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.WOTFormElements;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.job.operation.helpers.PressureCheckListener;
import com.goodyear.ejob.job.operation.helpers.TireImageItem;
import com.goodyear.ejob.performance.IOnPerformanceCallback;
import com.goodyear.ejob.performance.PerformanceBaseModel;
import com.goodyear.ejob.performance.ResponseMessagePerformance;
import com.goodyear.ejob.ui.tyremanagement.TyreSummary;
import com.goodyear.ejob.util.CameraUtility;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.InspectionDataHandler;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.TireActionType;

import java.util.ArrayList;
import java.util.UUID;

/**
 * @author amitkumar.h
 * @version 1.0
 */
public class WOTActivity extends Activity implements InactivityHandler {

	/**
	 * Swiper Object for mSwipeDetector
	 */
	private Swiper mSwipeDetector;
	/**
	 * Note Text
	 */
	private static String sNoteText = "";
	/**
	 * BluetoothService Object
	 */
	private BluetoothService mBTService;
	/**
	 * BroadcastReceiver Object
	 */
	private BroadcastReceiver mReceiver;
	/**
	 * TestAdapter Object
	 */
	private DatabaseAdapter mDbHelper;
	/**
	 * final variable for NOTES_INTENT
	 */
	public static final int NOTES_INTENT = 104;
	/**
	 * final variable for DECIMAL_LENGTH
	 */
	public static final int DECIMAL_LENGTH = 8;
	/**
	 * boolean variable for Swipe
	 */
	private boolean mSwiped = false;
	/**
	 * TestAdapter object
	 */
	private DatabaseAdapter mLabel;
	/**
	 * boolean variable for FromMountNew
	 */
	private boolean mFromMountNew = false;
	/**
	 * boolean variable for MountPWT
	 */
	private boolean mFromMountPWT = false;
	/**
	 * boolean variable for Define Tire
	 */
	private boolean mFromDefineTire = false;
	/**
	 * Serial number from Previous Intent
	 */
	private String mSerialNoFromIntent;
	/**
	 * position from Previous Intent
	 */
	private String mPositionFromIntent;

	private LinearLayout mLayoutValveFitted;

	private LinearLayout mLayoutHpValveFitted;

	private LinearLayout mLayoutValveFixingSupport;

	private LinearLayout mLayoutTireFill;

	private LinearLayout mLayoutBalancing;

	private LinearLayout mLayoutValveExtnFitted;

	private LinearLayout mLayoutRepair;

	/**
	 * TextView reference for serial label
	 */
	private TextView mSerial_Label;
	/**
	 * TextView reference for serial value
	 */
	private TextView mSerial_Value;
	/**
	 * TextView reference for position value
	 */
	private TextView mPosition_Value;
	/**
	 * TextView reference for nsk label1
	 */
	private TextView mNsk_label;
	/**
	 * EditText reference for nsk value
	 */
	private EditText mNsk_value;
	/**
	 * TextView reference for Pressure label
	 */
	private TextView mPressure_label;
	/**
	 * EditText reference for pressure value
	 */
	private EditText mPressure_value;
	/**
	 * TextView reference for pressure unit label
	 */
	private TextView mPressure_unit_label;
	/**
	 * TextView reference for valve fitted label
	 */
	private TextView mValve_fitted_label;
	/**
	 * Swith reference for valve fitted value
	 */
	private Switch mValve_fitted_value;
	/**
	 * TextView reference for HP valve fitted label
	 */
	private TextView mHp_valve_fitted_label;
	/**
	 * Switch reference for HP valve fitted value
	 */
	private Switch mHp_valve_fitted_value;
	/**
	 * TextView reference for valve fixing label
	 */
	private TextView mValve_fixing_label;
	/**
	 * Switch reference for Value fixing value
	 */
	private Switch mValve_fixing_value;
	/**
	 * TextView reference for Tire fill label
	 */
	private TextView mTire_fill_label;
	/**
	 * Switch reference for Tire fill value
	 */
	private Switch mTire_fill_value;
	/**
	 * TextView reference for Balancing label
	 */
	private TextView mBalancing_label;
	/**
	 * Spinner reference for Balancing value
	 */
	private Spinner mBalancing_value;
	/**
	 * ArrayList for Balancing array
	 */
	private ArrayList<String> mBalacing_array = new ArrayList<String>();
	/**
	 * ArrayAdapter for Balancing Adapter
	 */
	private ArrayAdapter<String> mBalacing_adapter;
	/**
	 * TextView reference for Valve extn label
	 */
	private TextView mValve_extn_label;
	/**
	 * TextView reference for valve extn value
	 */
	private Spinner mValve_extn_value;
	/**
	 * ArrayList for Valve extn array
	 */
	private ArrayList<String> mValve_extn_array = new ArrayList<String>();
	/**
	 * ArrayAdapter for Valve extn adapter
	 */
	private ArrayAdapter<String> mValve_extn_adapter;
	/**
	 * TextView reference for Repair label
	 */
	private TextView mRepair_label;
	/**
	 * Spinner reference for repair value
	 */
	private Spinner mRepair_value;
	/**
	 * ArrayList for repair array
	 */
	private ArrayList<String> mRepair_array = new ArrayList<String>();
	/**
	 * ArrayAdapter for repair adapter
	 */
	private ArrayAdapter<String> mRepair_adapter;
	/**
	 * Minimum NSK From Mount new Tire
	 */
	private String minNSKFromMountTire;
	/**
	 * Pressure from Mount new Tire
	 */
	// private String pressureFromMountTire;
	/**
	 * boolean variable for pressure measurement for isSettingsPSI
	 */
	private boolean mIsSettingsPSI;
	/**
	 * SharedPreferences object
	 */
	private SharedPreferences mPreferences;
	/**
	 * PSI value
	 */
	private float mPSIValue;
	/**
	 * PSI Converted value
	 */
	private String mFinalConvertedPressure;
	/**
	 * Contract ID
	 */
	private String mContract_id;
	/**
	 * ScrollView reference
	 */
	private ScrollView mMainScrollBar;

	/**
	 * Position of the axle for which work on tire is being performed
	 */
	private int axlePosition;

	/**
	 * Form elements to be captured when screen loads
	 */
	private WOTFormElements mInitialElements;
	/**
	 * Form elements to be captured when there's change in any screen form and
	 * at the time of saving form
	 */
	private WOTFormElements mModifiedElements;

	/**
	 * Pressure of the same axle
	 */
	private float recommendedpressure;
	private String mConfirmMsg;
	private String mNoLabel;
	private String mYesLabel;
	private String mNotesLabel;

	/**
	 * Randomly generated External ID for tyre to be used at the time of
	 * updating job items private String mConfirmMsg; private String mNoLabel;
	 * private String mYesLabel; private String mNotesLabel; /** boolean
	 * variable for is Camera enabled
	 */
	private boolean mCameraEnabled = false;
	/**
	 * ImageView for Camera icon
	 */
	private ImageView mCamera_icon;
	/**
	 * string variable to store external ID
	 */
	private String external_id = "";
	private int jobID;

	/**
	 * Randomly generated External ID for tyre to be used at the time of
	 * updating job items >>>>>>> .r1846
	 */
	String mExternalIdTyre = "";
	private ArrayList<Bitmap> mBitmapArray;
	private ArrayList<String> mDamageNoteArray;
	private String mjobIDForImages;
	private String mExternal_idForImages;
	private boolean mCameraClicked;

	private static final String PREVIOUS_JOBID_KEY = "jobidwithimagekey";
	private static final String PREVIOUS_EXTERNAL_ID_KEY = "externalidwithimagekey";
	private static final String TAG = "WorkOnTireOperation";

	//User Trace logs trace tag
	private static final String TRACE_TAG = "Work On tyre";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.work_on_tire);
		//User Trace logs
		LogUtil.TraceInfo(TRACE_TAG, "none", "OP", true, false, true);
		TextView wheelPositionLabel = (TextView) findViewById(R.id.lbl_wp);
		wheelPositionLabel.setText(Constants.sLblWheelPos);
		mDbHelper = new DatabaseAdapter(this);
		mLabel = new DatabaseAdapter(this);
		mPreferences = getSharedPreferences(Constants.GOODYEAR_CONF, 0);
		if (mPreferences.getString(Constants.PRESSUREUNIT, "").equals(
				Constants.PSI_PRESSURE_UNIT)) {
			mIsSettingsPSI = true;
		}
		Intent intent = getIntent();
		if (intent.hasExtra(VehicleSkeletonFragment.mAXLEPRESSURE_POS)) {
			axlePosition = intent.getExtras().getInt(
					VehicleSkeletonFragment.mAXLEPRESSURE_POS);
		}
		if (intent.hasExtra(VehicleSkeletonFragment.mAXLEPRESSURE)) {
			recommendedpressure = intent.getExtras().getFloat(
					VehicleSkeletonFragment.mAXLEPRESSURE);
		}

		mMainScrollBar = (ScrollView) findViewById(R.id.main_scroll);
		mSwipeDetector = new Swiper(getApplicationContext(), mMainScrollBar);
		mMainScrollBar.setOnTouchListener(mSwipeDetector);
		mSerialNoFromIntent = getIntent().getExtras().getString("SERIAL_NO");
		mPositionFromIntent = getIntent().getExtras().getString("WHEEL_POS");
		minNSKFromMountTire = getIntent().getExtras().getString("MIN_NSK");

		if (getIntent().hasExtra("FROM_MOUNT_NEW")) {
			mFromMountNew = getIntent().getExtras()
					.getBoolean("FROM_MOUNT_NEW");
		}
		if (getIntent().hasExtra("FROM_MOUNT_PWT")) {
			mFromMountPWT = getIntent().getExtras()
					.getBoolean("FROM_MOUNT_PWT");
		}
		if (getIntent().hasExtra("FROM_DEFINE_TIRE")) {
			mFromDefineTire = getIntent().getExtras().getBoolean(
					"FROM_DEFINE_TIRE");
		}

		if (savedInstanceState != null) {
			mjobIDForImages = savedInstanceState
					.getString(PREVIOUS_JOBID_KEY);
			mExternal_idForImages= savedInstanceState
					.getString(PREVIOUS_EXTERNAL_ID_KEY);
			mExternalIdTyre = savedInstanceState
					.getString("retainExternalIdTyre");
		} else {
			mExternalIdTyre = Constants.SELECTED_TYRE.getExternalID();
			if (getIntent().hasExtra("SELECTED_TYRE_EXTERNAL_ID")) {
				mExternalIdTyre = getIntent().getExtras().getString(
						"SELECTED_TYRE_EXTERNAL_ID");
			}
			
			
		}
		mCamera_icon = (ImageView) findViewById(R.id.camera_icon);
		mSerial_Label = (TextView) findViewById(R.id.lbl_serialNo);
		mSerial_Value = (TextView) findViewById(R.id.value_serialNo);
		mPosition_Value = (TextView) findViewById(R.id.value_wp);
		mNsk_label = (TextView) findViewById(R.id.lbl_nsk);
		mNsk_value = (EditText) findViewById(R.id.value_nsk);
		mNsk_value
				.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
						3, 2) });
		mNsk_value.addTextChangedListener(new nskChanged());
		mNsk_value.setText(Constants.SELECTED_TYRE.getNsk());
		if (minNSKFromMountTire != null) {
			mNsk_value.setText(minNSKFromMountTire);
		}
		mPressure_label = (TextView) findViewById(R.id.lbl_pressure);
		mPressure_value = (EditText) findViewById(R.id.value_pressure);
		mPressure_unit_label = (TextView) findViewById(R.id.lbl_psi);
		setValuetoPressueView();
		/*
		 * mPressure_value.addTextChangedListener(new PressureCheckListener(
		 * mPressure_value, mIsSettingsPSI)); if (mIsSettingsPSI) {
		 * mPressure_value .setFilters(new InputFilter[] { new
		 * DecimalDigitsInputFilterNSK( 4, 2) });// PSI 220.00 } else {
		 * mPressure_value .setFilters(new InputFilter[] { new
		 * DecimalDigitsInputFilterNSK( 3, 2) });// BAR 15.00 }
		 * 
		 * try { mPSIValue =
		 * CommonUtils.parseFloat(Constants.SELECTED_TYRE.getPressure()); }
		 * catch (NumberFormatException e) { e.printStackTrace(); } if
		 * ((Float.isNaN(mPSIValue) || mPSIValue == 0.0 || mPSIValue == 0) &&
		 * (!Float.isNaN(recommendedpressure) && recommendedpressure != 0)) {
		 * mPSIValue = recommendedpressure; }
		 * 
		 * if (mIsSettingsPSI) {
		 * mPressure_unit_label.setText(Constants.PSI_PRESSURE_UNIT); mPSIValue
		 * = mPSIValue * 14.5038f;
		 * mPressure_value.setText(String.valueOf(Math.round(mPSIValue))); }
		 * else { mPressure_unit_label.setText(Constants.BAR_PRESSURE_UNIT);
		 * mPressure_value.setText(String.valueOf(mPSIValue)); } if
		 * (pressureFromMountTire != null) {
		 * mPressure_value.setText(pressureFromMountTire); }
		 */

		mLayoutValveFitted = (LinearLayout) findViewById(R.id.ll_valveFitted);

		mLayoutHpValveFitted = (LinearLayout) findViewById(R.id.ll_hpValveFitted);

		mLayoutValveFixingSupport = (LinearLayout) findViewById(R.id.ll_valveFixingSupport);

		mLayoutTireFill = (LinearLayout) findViewById(R.id.ll_tireFill);

		mLayoutBalancing = (LinearLayout) findViewById(R.id.ll_balancing);

		mLayoutValveExtnFitted = (LinearLayout) findViewById(R.id.ll_valveExtnFitted);

		mLayoutRepair = (LinearLayout) findViewById(R.id.ll_repair);

		mValve_fitted_label = (TextView) findViewById(R.id.lbl_valveFitted);
		mValve_fitted_value = (Switch) findViewById(R.id.value_valveFitted);
		mHp_valve_fitted_label = (TextView) findViewById(R.id.lbl_hpValveFitted);
		mHp_valve_fitted_value = (Switch) findViewById(R.id.value_hpValveFitted);
		mValve_fixing_label = (TextView) findViewById(R.id.lbl_valveFixingSupport);
		mValve_fixing_value = (Switch) findViewById(R.id.value_valveFixingSupport);
		mTire_fill_label = (TextView) findViewById(R.id.lbl_tireFill);
		mTire_fill_value = (Switch) findViewById(R.id.value_tireFill);
		mBalancing_label = (TextView) findViewById(R.id.lbl_balancing);
		mBalancing_value = (Spinner) findViewById(R.id.spinner_balancing);
		mValve_extn_label = (TextView) findViewById(R.id.lbl_valveExtnFitted);
		mValve_extn_value = (Spinner) findViewById(R.id.spinner_valveExtnFitted);
		mRepair_label = (TextView) findViewById(R.id.lbl_repair);
		mRepair_value = (Spinner) findViewById(R.id.spinner_repair);
		mSerial_Value.setText(mSerialNoFromIntent);
		mPosition_Value.setText(mPositionFromIntent);

		loadLabelsFromDB();
		loadSpinnerDetails();
		applyContractPolicy();

		if (savedInstanceState != null) {
			mInitialElements = savedInstanceState
					.getParcelable("retainInitialElement");
			mModifiedElements = savedInstanceState
					.getParcelable("retainFinalElement");
		} else {
			mInitialElements = new WOTFormElements();
			mModifiedElements = new WOTFormElements();

			captureInitialState();
		}
		restoreDialogState(savedInstanceState);

		if (mFromMountNew || mFromMountPWT || mFromDefineTire) {
			mNsk_value.setEnabled(false);
			mPressure_value.setEnabled(false);
			mCamera_icon.setVisibility(View.INVISIBLE);
		} else {
			/*
			 * String getPressureVal = mPressure_value.getText().toString(); if
			 * ((TextUtils.isEmpty(getPressureVal) || getPressureVal
			 * .equals("0.0") || getPressureVal.equals("0"))) {
			 * applyFirstValidationRule(); addListnerToViewElements(); } else {
			 * mNsk_value.setEnabled(false); mPressure_value.setEnabled(false);
			 * }
			 */
			String getPressureValStr = mPressure_value.getText().toString();
			Float getPressureVal = CommonUtils.parseFloat(getPressureValStr);
			if (getPressureVal > 0) {
				mNsk_value.setEnabled(false);
				mPressure_value.setEnabled(false);
				mCamera_icon.setVisibility(View.INVISIBLE);
				addListnerToViewElements();
			} else {
				applyFirstValidationRule();
				addListnerToViewElements();
			}
		}
		applyFirstValidationRule();
		addListnerToViewElements();
		applySecondValidationRule();
		// CR:451
		mCamera_icon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent photoIntent = new Intent(WOTActivity.this,
						CameraActivity.class);
				mCameraClicked = true;
				photoIntent.putExtra("CURNT_IMAGE_COUNT", getImageCount());
				startActivity(photoIntent);
			}
		});
		if (savedInstanceState == null && !mFromMountNew && !mFromMountPWT) {
			checkAvailableImages();
		}
	}

	/**
	 * 
	 */
	private void checkAvailableImages() {
		String jobIDWithImage = "";
		String jobIdIfNoImage = "";
		String external_idWithImages = "";
		String external_idWhenNoImage = "";
		CameraUtility.resetParams();
		if (!mCameraClicked) {
			mBitmapArray = new ArrayList<Bitmap>();
			mDamageNoteArray = new ArrayList<String>();
			for (int i = 0; i < VehicleSkeletonFragment.mJobItemList.size(); i++) {
				JobItem jobItem = VehicleSkeletonFragment.mJobItemList.get(i);
				if (Integer.parseInt(VehicleSkeletonFragment.mJobItemList
						.get(i).getActionType()) > 8
						&& Integer.parseInt(jobItem.getActionType()) < 24) {
					if (jobItem.getTyreID().equals(
							Constants.SELECTED_TYRE.getExternalID())) {
						jobIdIfNoImage = jobItem.getJobId();
						external_idWhenNoImage = jobItem
								.getExternalId();
						for (int j = 0; j < VehicleSkeletonFragment.mTireImageItemList
								.size(); j++) {
							if (VehicleSkeletonFragment.mTireImageItemList
									.get(j).getJobItemId()
									.equals(jobItem.getJobId())) {
								jobIDWithImage = jobItem.getJobId();
								external_idWithImages = jobItem
										.getExternalId();
								mBitmapArray
										.add(TyreSummary
												.getPhoto(VehicleSkeletonFragment.mTireImageItemList
														.get(j).getTyreImage()));
								mDamageNoteArray
										.add(VehicleSkeletonFragment.mTireImageItemList
												.get(j).getDamageDescription());

							}
						}
						if (null != mBitmapArray && mBitmapArray.size() > 0) {
							Constants.sBitmapPhoto1 = mBitmapArray.get(0);
							if (null != mDamageNoteArray
									&& mDamageNoteArray.size() > 0
									&& !mDamageNoteArray.get(0)
											.equalsIgnoreCase("")) {
								Constants.DAMAGE_NOTES1 = mDamageNoteArray
										.get(0);
							}
						}
						if (null != mBitmapArray && mBitmapArray.size() > 1) {
							Constants.sBitmapPhoto2 = mBitmapArray.get(1);
							if (null != mDamageNoteArray
									&& mDamageNoteArray.size() > 1
									&& !mDamageNoteArray.get(1)
											.equalsIgnoreCase("")) {
								Constants.DAMAGE_NOTES2 = mDamageNoteArray
										.get(1);
							}
						}
						if (null != mBitmapArray && mBitmapArray.size() > 2) {
							Constants.sBitmapPhoto3 = mBitmapArray.get(2);
							if (null != mDamageNoteArray
									&& mDamageNoteArray.size() > 2
									&& !mDamageNoteArray.get(2)
											.equalsIgnoreCase("")) {
								Constants.DAMAGE_NOTES3 = mDamageNoteArray
										.get(2);
							}
						}
					}
				}
			}
		}
		
		if(jobIDWithImage.equals("")){
			jobIDWithImage = jobIdIfNoImage;
			external_idWithImages = external_idWhenNoImage;
		}
		
		if (!Constants.SELECTED_TYRE.isWorkedOn()) {
			mjobIDForImages = "";
			mExternal_idForImages = "";
		} else {
			mjobIDForImages = jobIDWithImage;
			mExternal_idForImages = external_idWithImages;
		}
	}

	/**
	 * Get Current Image Count
	 */
	private int getImageCount() {
		int curntImgCount = 0;
		if (null != Constants.sBitmapPhoto1) {
			curntImgCount++;
		}
		if (null != Constants.sBitmapPhoto2) {
			curntImgCount++;
		}
		if (null != Constants.sBitmapPhoto3) {
			curntImgCount++;
		}
		return curntImgCount;
	}
	/**
	 * Overridden method handling application inactivity by updating the
	 * user-interaction with the application
	 */
	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	@Override
	protected void onStart() {
		super.onStart();
		LogoutHandler.setCurrentActivity(this);
	}

	/**
	 * Add listener to view elemnts
	 */
	private void addListnerToViewElements() {
		if (mValve_fitted_value.getVisibility() == View.VISIBLE) {
			mValve_fitted_value
					.setOnCheckedChangeListener(valveFittedValListener);
		}
		if (mHp_valve_fitted_value.getVisibility() == View.VISIBLE) {
			mHp_valve_fitted_value
					.setOnCheckedChangeListener(hpValveFittedValListener);
		}
		if (mValve_fixing_value.getVisibility() == View.VISIBLE) {
			mValve_fixing_value
					.setOnCheckedChangeListener(valveFixingValListener);
		}
		if (mTire_fill_value.getVisibility() == View.VISIBLE) {
			mTire_fill_value.setOnCheckedChangeListener(tireFillValListener);
		}
		if (mBalancing_value.getVisibility() == View.VISIBLE) {
			mBalancing_value
					.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int pos, long id) {
							mBalancing_value
									.setOnItemSelectedListener(balancingValListener);
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {

						}
					});
		}

		if (mValve_extn_value.getVisibility() == View.VISIBLE) {
			mValve_extn_value
					.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int pos, long id) {
							mValve_extn_value
									.setOnItemSelectedListener(valveExtnListener);
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {

						}
					});
		}

		if (mRepair_value.getVisibility() == View.VISIBLE) {
			mRepair_value
					.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int pos, long id) {
							mRepair_value
									.setOnItemSelectedListener(repairValListener);
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {
						}
					});
		}
	}

	OnItemSelectedListener balancingValListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			mModifiedElements.setBalancing(pos);
			if (CommonUtils.parseFloat(mInitialElements.getPressureValue()) > 0) {
				applyThirdValidationRule();
			} else {
				applyFirstValidationRule();
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {

		}
	};

	OnItemSelectedListener valveExtnListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			mModifiedElements.setValveExtnFitted(pos);
			if (CommonUtils.parseFloat(mInitialElements.getPressureValue()) > 0) {
				applyThirdValidationRule();
			} else {
				applyFirstValidationRule();
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {

		}
	};

	OnItemSelectedListener repairValListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			mModifiedElements.setRepair(pos);
			if (CommonUtils.parseFloat(mInitialElements.getPressureValue()) > 0) {
				applyThirdValidationRule();
			} else {
				applyFirstValidationRule();
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {

		}
	};

	OnCheckedChangeListener valveFittedValListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			mModifiedElements.setValveFitted(isChecked);
			if (CommonUtils.parseFloat(mInitialElements.getPressureValue()) > 0) {
				applyThirdValidationRule();
			} else {
				applyFirstValidationRule();
			}
		}
	};

	OnCheckedChangeListener hpValveFittedValListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			mModifiedElements.setHpValveCapFitted(isChecked);
			if (CommonUtils.parseFloat(mInitialElements.getPressureValue()) > 0) {
				applyThirdValidationRule();
			} else {
				applyFirstValidationRule();
			}
		}
	};

	OnCheckedChangeListener valveFixingValListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			mModifiedElements.setValveFixingSupport(isChecked);
			if (CommonUtils.parseFloat(mInitialElements.getPressureValue()) > 0) {
				applyThirdValidationRule();
			} else {
				applyFirstValidationRule();
			}
		}
	};

	OnCheckedChangeListener tireFillValListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			mModifiedElements.setTireFill(isChecked);
			if (CommonUtils.parseFloat(mInitialElements.getPressureValue()) > 0) {
				applyThirdValidationRule();
			} else {
				applyFirstValidationRule();
			}
		}
	};

	/**
	 * <ul>
	 * <li><b>rule 1:</b> The NSK / Pressure should be disable until something
	 * change on the screen</li>
	 * <li><b>rule 2:</b> If the user goes to the WOT screen, change nothing
	 * then pressure / NSK should not be asked, the green tick should not appear
	 * </li>
	 * <li><b>rule 3:</b> the probe can be used in the screen</li>
	 * <li><b>rule 4:</b> when entering the WOT screen the field pressure and
	 * NSK should be disable</li>
	 * <li><b>rule 5:</b> if the user select something on the screen, the
	 * pressure/nsk field should be enable</li>
	 * <li><b>rule 6:</b> if the user goes to initial state of the screen
	 * (default), the NSK/pressure fields should get back to original state what
	 * ever a pressure/NSK was set.</li>
	 * <li><b>rule 7:</b> If the screen comes from mount new / mount pwt, then
	 * NSK / Pressure should always be disabled</li>
	 * </ul>
	 * */
	private void applyFirstValidationRule() {
		if (checkOriginalState()) {
			mNsk_value.setText(mInitialElements.getNskValue());
			mPressure_value.setText(mInitialElements.getPressureValue());
			mNsk_value.setEnabled(false);
			mPressure_value.setEnabled(false);
			mCamera_icon.setVisibility(View.INVISIBLE);
		} else {
			mNsk_value.setEnabled(true);
			mNsk_value.setSelection(mNsk_value.getText().length());
			mPressure_value.setEnabled(true);
			mCamera_icon.setVisibility(View.VISIBLE);
			mPressure_value.setSelection(mPressure_value.getText().length());
		}
	}

	/**
	 * On performing any operation NSK should be enabled
	 */
	private void applyThirdValidationRule() {
		if (checkOriginalState()) {
			mNsk_value.setText(mInitialElements.getNskValue());
			mNsk_value.setEnabled(false);
			mCamera_icon.setVisibility(View.INVISIBLE);
		} else {
			mNsk_value.setEnabled(true);
			mCamera_icon.setVisibility(View.VISIBLE);
			mNsk_value.setSelection(mNsk_value.getText().length());
		}
	}

	/**
	 * In case of Work On Tyre: For services Valve fitted, HP Valve cap fitted,
	 * Valve fixing support, Tyre fill, Balancing, Valve extenstion fitted,
	 * Repair if user selects any operation to YES(or from selection form
	 * dropdown list) then these options should be locked. User should not allow
	 * to remove/switchoff/change from YES to NO(or selected operations form
	 * drop down list) in next WOT operation. User can add options by changing
	 * NO to YES. But Changing form YES to NO is not allowed in second and
	 * consecutive WOT operations.
	 * 
	 * Ex: If user select Valve fittes as YES during WOT, then application
	 * should not allow to switch off the same option in next WOT on the same
	 * tyre
	 */
	private void applySecondValidationRule() {
		if (mValve_fitted_value.isChecked()) {
			mValve_fitted_value.setEnabled(false);
		}
		if (mHp_valve_fitted_value.isChecked()) {
			mHp_valve_fitted_value.setEnabled(false);
		}
		if (mValve_fixing_value.isChecked()) {
			mValve_fixing_value.setEnabled(false);
		}
		if (mTire_fill_value.isChecked()) {
			mTire_fill_value.setEnabled(false);
		}
		if (mBalancing_value.getSelectedItemPosition() > 0) {
			mBalancing_value.setEnabled(false);
		}
		if (mValve_extn_value.getSelectedItemPosition() > 0) {
			mValve_extn_value.setEnabled(false);
		}
		if (mRepair_value.getSelectedItemPosition() > 0) {
			mRepair_value.setEnabled(false);
		}
	}

	/**
	 * Captures all the values of form fields when screen loaded
	 */
	private void captureInitialState() {

		if (mNsk_value != null && mNsk_value.getVisibility() == View.VISIBLE) {
			mInitialElements.setNskValue(mNsk_value.getText().toString());
			mModifiedElements.setNskValue(mNsk_value.getText().toString());
		}
		if (mPressure_value != null
				&& mPressure_value.getVisibility() == View.VISIBLE) {
			mInitialElements.setPressureValue(mPressure_value.getText()
					.toString());
			mModifiedElements.setPressureValue(mPressure_value.getText()
					.toString());
		}
		if (mValve_fitted_value != null
				&& mValve_fitted_value.getVisibility() == View.VISIBLE) {
			mInitialElements.setValveFitted(mValve_fitted_value.isChecked());
			mModifiedElements.setValveFitted(mValve_fitted_value.isChecked());
		}
		if (mHp_valve_fitted_value != null
				&& mHp_valve_fitted_value.getVisibility() == View.VISIBLE) {
			mInitialElements.setHpValveCapFitted(mHp_valve_fitted_value
					.isChecked());
			mModifiedElements.setHpValveCapFitted(mHp_valve_fitted_value
					.isChecked());
		}
		if (mValve_fixing_value != null
				&& mValve_fixing_value.getVisibility() == View.VISIBLE) {
			mInitialElements.setValveFixingSupport(mValve_fixing_value
					.isChecked());
			mModifiedElements.setValveFixingSupport(mValve_fixing_value
					.isChecked());
		}
		if (mTire_fill_value != null
				&& mTire_fill_value.getVisibility() == View.VISIBLE) {
			mInitialElements.setTireFill(mTire_fill_value.isChecked());
			mModifiedElements.setTireFill(mTire_fill_value.isChecked());
		}
		if (mBalancing_value != null
				&& mBalancing_value.getVisibility() == View.VISIBLE) {
			mInitialElements.setBalancing(mBalancing_value
					.getSelectedItemPosition());
			mModifiedElements.setBalancing(mBalancing_value
					.getSelectedItemPosition());
		}
		if (mValve_extn_value != null
				&& mValve_extn_value.getVisibility() == View.VISIBLE) {
			mInitialElements.setValveExtnFitted(mValve_extn_value
					.getSelectedItemPosition());
			mModifiedElements.setValveExtnFitted(mValve_extn_value
					.getSelectedItemPosition());
		}
		if (mRepair_value != null
				&& mRepair_value.getVisibility() == View.VISIBLE) {
			mInitialElements.setRepair(mRepair_value.getSelectedItemPosition());
			mModifiedElements
					.setRepair(mRepair_value.getSelectedItemPosition());
		}
	}

	private void captureFinalState() {

		if (mNsk_value != null && mNsk_value.getVisibility() == View.VISIBLE) {
			mModifiedElements.setNskValue(mNsk_value.getText().toString());
		}
		if (mPressure_value != null
				&& mPressure_value.getVisibility() == View.VISIBLE) {
			mModifiedElements.setPressureValue(mPressure_value.getText()
					.toString());
		}
		if (mValve_fitted_value != null
				&& mValve_fitted_value.getVisibility() == View.VISIBLE) {
			mModifiedElements.setValveFitted(mValve_fitted_value.isChecked());
		}
		if (mHp_valve_fitted_value != null
				&& mHp_valve_fitted_value.getVisibility() == View.VISIBLE) {
			mModifiedElements.setHpValveCapFitted(mHp_valve_fitted_value
					.isChecked());
		}
		if (mValve_fixing_value != null
				&& mValve_fixing_value.getVisibility() == View.VISIBLE) {
			mModifiedElements.setValveFixingSupport(mValve_fixing_value
					.isChecked());
		}
		if (mTire_fill_value != null
				&& mTire_fill_value.getVisibility() == View.VISIBLE) {
			mModifiedElements.setTireFill(mTire_fill_value.isChecked());
		}
		if (mBalancing_value != null
				&& mBalancing_value.getVisibility() == View.VISIBLE) {
			mModifiedElements.setBalancing(mBalancing_value
					.getSelectedItemPosition());
		}
		if (mValve_extn_value != null
				&& mValve_extn_value.getVisibility() == View.VISIBLE) {
			mModifiedElements.setValveExtnFitted(mValve_extn_value
					.getSelectedItemPosition());
		}
		if (mRepair_value != null
				&& mRepair_value.getVisibility() == View.VISIBLE) {
			mModifiedElements
					.setRepair(mRepair_value.getSelectedItemPosition());
		}
	}

	private boolean checkOriginalState() {
		boolean isInOriginalState = false;
		if (mInitialElements.equals(mModifiedElements)) {
			isInOriginalState = true;
		}
		return isInOriginalState;
	}

	/**
	 * Applying Contract Policy
	 */
	private void applyContractPolicy() {
		if (mFromMountNew) {
			mLayoutTireFill.setVisibility(View.GONE);
			mTire_fill_label.setVisibility(View.GONE);
			mTire_fill_value.setVisibility(View.GONE);

			mLayoutRepair.setVisibility(View.GONE);
			mRepair_label.setVisibility(View.GONE);
			mRepair_value.setVisibility(View.GONE);
		} else {
			if (Constants.SELECTED_TYRE.isHasMounted()) {
				mLayoutTireFill.setVisibility(View.GONE);
				mTire_fill_label.setVisibility(View.GONE);
				mTire_fill_value.setVisibility(View.GONE);

				mLayoutRepair.setVisibility(View.GONE);
				mRepair_label.setVisibility(View.GONE);
				mRepair_value.setVisibility(View.GONE);
			}
		}
		mContract_id = Constants.contract.getContractId();
		Cursor mCursor = null;
		try {
			mCursor = null;
			if (null != mDbHelper) {
				mDbHelper.open();
				mCursor = mDbHelper.getContractPolicy(mContract_id);
			}
			if (null == mCursor) {
				return;
			}
			if (!CursorUtils.isValidCursor(mCursor)) {
				return;
			}
			mCursor.moveToFirst();
			String balancePolicy = mCursor.getString(mCursor
					.getColumnIndex("Balancing"));
			String hPValvePolicy = mCursor.getString(mCursor
					.getColumnIndex("PolicyOnHPValveCap"));
			String valveExtnPolicy = mCursor.getString(mCursor
					.getColumnIndex("PolicyOnValveExtension"));
			String valveFittingPolicy = mCursor.getString(mCursor
					.getColumnIndex("ValveFitting"));
			if (balancePolicy.equals("0")) {
				mLayoutBalancing.setVisibility(View.GONE);
				mBalancing_label.setVisibility(View.GONE);
				mBalancing_value.setVisibility(View.GONE);
			}
			if (hPValvePolicy.equals("0")) {
				mLayoutHpValveFitted.setVisibility(View.GONE);
				mHp_valve_fitted_label.setVisibility(View.GONE);
				mHp_valve_fitted_value.setVisibility(View.GONE);
			}
			if (Constants.JOBTYPEBREAKDOWN) {
				mLayoutBalancing.setVisibility(View.GONE);
				mBalancing_label.setVisibility(View.GONE);
				mBalancing_value.setVisibility(View.GONE);
			}
			if (valveExtnPolicy.equals("0")) {
				mLayoutValveExtnFitted.setVisibility(View.GONE);
				mValve_extn_label.setVisibility(View.GONE);
				mValve_extn_value.setVisibility(View.GONE);
			}
			if (valveFittingPolicy.equals("0")) {
				mLayoutValveFitted.setVisibility(View.GONE);
				mValve_fitted_label.setVisibility(View.GONE);
				mValve_fitted_value.setVisibility(View.GONE);
			}
			if (checkForActionType(TireActionType.VALVEFITTED)) {
				mValve_fitted_value.setChecked(true);
			} else {
				mValve_fitted_value.setChecked(false);
			}
			if (checkForActionType(TireActionType.HIGHTPRESSUREVALUECAPFITTED)) {
				mHp_valve_fitted_value.setChecked(true);
			} else {
				mHp_valve_fitted_value.setChecked(false);
			}
			if (checkForActionType(TireActionType.VALUEFIXINGSUPPORT)) {
				mValve_fixing_value.setChecked(true);
			} else {
				mValve_fixing_value.setChecked(false);
			}
			if (checkForActionType(TireActionType.TIREFILL)) {
				mTire_fill_value.setChecked(true);
			} else {
				mTire_fill_value.setChecked(false);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(mCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Checking Action type
	 */
	private boolean checkForActionType(String actionType) {
		boolean isActionType = false;
		try {
			if (VehicleSkeletonFragment.mJobItemList != null
					&& VehicleSkeletonFragment.mJobItemList.size() > 0) {
				for (int i = 0; i < VehicleSkeletonFragment.mJobItemList.size(); i++) {
					JobItem jobitem = VehicleSkeletonFragment.mJobItemList
							.get(i);
					String temp_actionType = jobitem.getActionType();
					String temp_typeId = jobitem.getTyreID();
					int deletedStatus = jobitem.getDeleteStatus();
					if (temp_actionType.equals(actionType)
							&& temp_typeId.equals(mExternalIdTyre)
							&& deletedStatus != 1) {
						isActionType = true;
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isActionType;
	}

	/**
	 * Load Spinner Details
	 */
	private void loadSpinnerDetails() {
		try {
			if (null == mLabel) {
				return;
			}
			mLabel.open();
			mBalacing_array.clear();
			mBalacing_array.add(mLabel.getLabel(300));
			mBalacing_array.add(mLabel.getLabel(250));
			mBalacing_array.add(mLabel.getLabel(251));
			mBalacing_adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mBalacing_array);
			mBalacing_adapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mBalancing_value.setAdapter(mBalacing_adapter);
			if (checkForActionType(TireActionType.DYNAMICBALANCING)) {
				mBalancing_value.setSelection(1);
			} else if (checkForActionType(TireActionType.STATICBALANCING)) {
				mBalancing_value.setSelection(2);
			} else {
				mBalancing_value.setSelection(0);
			}
			mValve_extn_array.clear();
			mValve_extn_array.add(mLabel.getLabel(300));
			mValve_extn_array.add(mLabel.getLabel(253));
			mValve_extn_array.add(mLabel.getLabel(252));
			mValve_extn_adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mValve_extn_array);
			mValve_extn_adapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mValve_extn_value.setAdapter(mValve_extn_adapter);
			if (checkForActionType(TireActionType.FLEXIBLEVALVEEXTENSINFITTED)) {
				mValve_extn_value.setSelection(1);
			} else if (checkForActionType(TireActionType.RIGIDVALVEEXTENSINFITTED)) {
				mValve_extn_value.setSelection(2);
			} else {
				mValve_extn_value.setSelection(0);
			}
			mRepair_array.clear();
			mRepair_array.add(mLabel.getLabel(300));
			mRepair_array.add(mLabel.getLabel(259));
			mRepair_array.add(mLabel.getLabel(257));
			mRepair_array.add(mLabel.getLabel(260));
			mRepair_array.add(mLabel.getLabel(256));
			if (Constants.JOBTYPEREGULAR) {
				mRepair_array.add(mLabel.getLabel(254));
			}
			mRepair_array.add(mLabel.getLabel(258));
			mRepair_adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mRepair_array);
			mRepair_adapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mRepair_value.setAdapter(mRepair_adapter);
			if (checkForActionType(TireActionType.BREAKSPOTREPAIR)) {
				mRepair_value.setSelection(1);
			} else if (checkForActionType(TireActionType.MAJORREPAIR)) {
				mRepair_value.setSelection(2);
			} else if (checkForActionType(TireActionType.MAJORREPAIRWITHVULCANISATION)) {
				mRepair_value.setSelection(3);
			} else if (checkForActionType(TireActionType.MINORREPAIR)) {
				mRepair_value.setSelection(4);
			} else if (checkForActionType(TireActionType.TYREREPAIREANDREINFORCEMENT)) {
				if (Constants.JOBTYPEREGULAR) {
					mRepair_value.setSelection(6);
				} else {
					mRepair_value.setSelection(5);
				}
			} else if (checkForActionType(TireActionType.PUNCTUREDREPAIRREPAIRED)) {
				mRepair_value.setSelection(5);
			}
			mLabel.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		// Currently Bluetooth function is not required for WOT.
		/*
		 * IntentFilter intentFilter = new IntentFilter("BLUETOOTH_SENDER");
		 * mReceiver = new BroadcastReceiver() {
		 * 
		 * @Override public void onReceive(Context context, Intent intent) { if
		 * (intent.hasExtra("BT_DATA_NSK")) { String nsk_value =
		 * intent.getStringExtra("BT_DATA_NSK");
		 * 
		 * } else if (intent.hasExtra("BT_DATA_PRE")) { String pre_value =
		 * intent.getStringExtra("BT_DATA_PRE"); if (!mIsSettingsPSI) { float
		 * barValue; barValue = CommonUtils.parseFloat(pre_value) * 0.068947f;
		 * mPressure_value.setText(String.valueOf(Math .round(barValue))); }
		 * else mPressure_value.setText(pre_value); } } }; try {
		 * registerReceiver(mReceiver, intentFilter); } catch (Exception e) {
		 * e.printStackTrace(); } mBTService = new
		 * BluetoothService(getApplicationContext()); initiateBT();
		 */
	}

	/**
	 * Initializing Bluetooth
	 */
	/*
	 * private void initiateBT() { int status = mBTService.getPairedStatus();
	 * switch (status) { case 0: Toast.makeText( getApplicationContext(),
	 * "Multiple paired devices found, please unpair which are not used",
	 * Toast.LENGTH_LONG).show(); break; case 1: BTTask task = new BTTask();
	 * task.execute(); break; case 2: Toast.makeText(getApplicationContext(),
	 * "Please pair a TLOGIK device", Toast.LENGTH_LONG).show(); break; case 3:
	 * Toast.makeText(getApplicationContext(), "No paired devices",
	 * Toast.LENGTH_LONG).show(); break; } }
	 *//**
	 * Bluetooth Connection establishment in background
	 * 
	 */
	/*
	 * class BTTask extends AsyncTask<String, String, String> {
	 * 
	 * @Override protected String doInBackground(String... f_url) { try {
	 * mBTService.connectToDevice(0); } catch (Exception e) {
	 * e.printStackTrace(); } return null; } }
	 */

	@Override
	public void onPause() {
		super.onPause();
		/*
		 * try { if (null != mReceiver) { unregisterReceiver(mReceiver); } if
		 * (null != mBTService) { mBTService.stop(); } } catch (Exception ex) {
		 * ex.printStackTrace(); }
		 */
		captureFinalState();
	}
	/**
	 * Loading Labels for UI from the Translation table of the local database
	 * file
	 */
	private void loadLabelsFromDB() {
		try {
			if (null == mLabel) {
				return;
			}
			mLabel.open();
			setTitle(mLabel.getLabel(Constants.LABEL_WORK_ON_TIRE));
			mConfirmMsg = mLabel.getLabel(241);
			mNoLabel = mLabel.getLabel(Constants.NO_LABEL);
			mYesLabel = mLabel.getLabel(Constants.YES_LABEL);

			String labelFromDB = mLabel.getLabel(418);
			mSerial_Label.setText(labelFromDB);

			labelFromDB = mLabel.getLabel(322);
			mNsk_label.setText(labelFromDB);

			labelFromDB = mLabel.getLabel(306);
			mPressure_label.setText(labelFromDB);

			labelFromDB = mLabel.getLabel(373);
			mValve_fitted_label.setText(labelFromDB);

			labelFromDB = mLabel.getLabel(374);
			mHp_valve_fitted_label.setText(labelFromDB);

			labelFromDB = mLabel.getLabel(376);
			mValve_fixing_label.setText(labelFromDB);

			labelFromDB = mLabel.getLabel(255);
			mTire_fill_label.setText(labelFromDB);

			labelFromDB = mLabel.getLabel(377);
			mBalancing_label.setText(labelFromDB);

			labelFromDB = mLabel.getLabel(378);
			mValve_extn_label.setText(labelFromDB);

			labelFromDB = mLabel.getLabel(379);
			mRepair_label.setText(labelFromDB);

			String yesLabel = mLabel.getLabel(Constants.YES_LABEL);
			String noLabel = mLabel.getLabel(Constants.NO_LABEL);
			mNotesLabel = mLabel.getLabel(Constants.NOTES_ACTIVITY_NAME);

			mValve_fitted_value.setTextOn(yesLabel);
			mValve_fitted_value.setTextOff(noLabel);

			mHp_valve_fitted_value.setTextOn(yesLabel);
			mHp_valve_fitted_value.setTextOff(noLabel);

			mValve_fixing_value.setTextOn(yesLabel);
			mValve_fixing_value.setTextOff(noLabel);

			mTire_fill_value.setTextOn(yesLabel);
			mTire_fill_value.setTextOff(noLabel);
			Constants.sLblCheckPressureValue = mLabel.getLabel(Constants.CHECK_PRESSURE_LABEL);

			mLabel.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Return to VechicleSkeleton screen
	 */
	private void onReturnSkeleton() {

		if (!mFromDefineTire) {
			Constants.SELECTED_TYRE.setNsk(mNsk_value.getText().toString());
			Constants.SELECTED_TYRE.setNsk2(mNsk_value.getText().toString());
			Constants.SELECTED_TYRE.setNsk3(mNsk_value.getText().toString());
		}

		mFinalConvertedPressure = CommonUtils.getFinalConvertedPressureValue(
				recommendedpressure, mPressure_value.getText().toString(),
				mIsSettingsPSI);

		Constants.PRESSURE_SET_WOT = mFinalConvertedPressure;
		Constants.SELECTED_TYRE.setPressure(mFinalConvertedPressure);

		//Fix : Bug 626 (users are reporting that tyre pressures are incorrectly applied to other axles)
		//Fix for Missing Axle Position Information
		if (mPressure_value.isEnabled()
				&& (null != VehicleSkeletonFragment.mAxleRecommendedPressure)) {
			VehicleSkeletonFragment.mAxleRecommendedPressure.set(axlePosition,
					CommonUtils.parseFloat(mFinalConvertedPressure));
		}

		if (mValve_fitted_value.getVisibility() == View.VISIBLE) {
			if (mValve_fitted_value.isChecked()) {
				if (!checkForActionType(TireActionType.VALVEFITTED))
					updateJobItem(TireActionType.VALVEFITTED);
			} else {
				if (checkForActionType(TireActionType.VALVEFITTED))
					removeJobItem(TireActionType.VALVEFITTED);
			}
		}
		if (mHp_valve_fitted_value.getVisibility() == View.VISIBLE) {
			if (mHp_valve_fitted_value.isChecked()) {
				if (!checkForActionType(TireActionType.HIGHTPRESSUREVALUECAPFITTED))
					updateJobItem(TireActionType.HIGHTPRESSUREVALUECAPFITTED);
			} else {
				if (checkForActionType(TireActionType.HIGHTPRESSUREVALUECAPFITTED))
					removeJobItem(TireActionType.HIGHTPRESSUREVALUECAPFITTED);
			}
		}
		if (mValve_fixing_value.getVisibility() == View.VISIBLE) {
			if (mValve_fixing_value.isChecked()) {
				if (!checkForActionType(TireActionType.VALUEFIXINGSUPPORT))
					updateJobItem(TireActionType.VALUEFIXINGSUPPORT);
			} else {
				if (checkForActionType(TireActionType.VALUEFIXINGSUPPORT))
					removeJobItem(TireActionType.VALUEFIXINGSUPPORT);
			}
		}
		if (mTire_fill_value.getVisibility() == View.VISIBLE) {
			if (mTire_fill_value.isChecked()) {
				if (!checkForActionType(TireActionType.TIREFILL))
					updateJobItem(TireActionType.TIREFILL);
			} else {
				if (checkForActionType(TireActionType.TIREFILL))
					removeJobItem(TireActionType.TIREFILL);
			}
		}
		if (mBalancing_value.getVisibility() == View.VISIBLE) {
			if (mBalancing_value.getSelectedItemPosition() == 1) {
				if (!checkForActionType(TireActionType.DYNAMICBALANCING))
					updateJobItem(TireActionType.DYNAMICBALANCING);
				if (checkForActionType(TireActionType.STATICBALANCING))
					removeJobItem(TireActionType.STATICBALANCING);
			} else if (mBalancing_value.getSelectedItemPosition() == 2) {
				if (!checkForActionType(TireActionType.STATICBALANCING))
					updateJobItem(TireActionType.STATICBALANCING);
				if (checkForActionType(TireActionType.DYNAMICBALANCING))
					removeJobItem(TireActionType.DYNAMICBALANCING);
			} else {
				if (checkForActionType(TireActionType.DYNAMICBALANCING))
					removeJobItem(TireActionType.DYNAMICBALANCING);
				if (checkForActionType(TireActionType.STATICBALANCING))
					removeJobItem(TireActionType.STATICBALANCING);
			}
		}
		if (mValve_extn_value.getVisibility() == View.VISIBLE) {
			if (mValve_extn_value.getSelectedItemPosition() == 1) {
				if (!checkForActionType(TireActionType.FLEXIBLEVALVEEXTENSINFITTED))
					updateJobItem(TireActionType.FLEXIBLEVALVEEXTENSINFITTED);
				if (checkForActionType(TireActionType.RIGIDVALVEEXTENSINFITTED))
					removeJobItem(TireActionType.RIGIDVALVEEXTENSINFITTED);
			} else if (mValve_extn_value.getSelectedItemPosition() == 2) {
				if (!checkForActionType(TireActionType.RIGIDVALVEEXTENSINFITTED))
					updateJobItem(TireActionType.RIGIDVALVEEXTENSINFITTED);
				if (checkForActionType(TireActionType.FLEXIBLEVALVEEXTENSINFITTED))
					removeJobItem(TireActionType.FLEXIBLEVALVEEXTENSINFITTED);
			} else {
				if (checkForActionType(TireActionType.FLEXIBLEVALVEEXTENSINFITTED))
					removeJobItem(TireActionType.FLEXIBLEVALVEEXTENSINFITTED);
				if (checkForActionType(TireActionType.RIGIDVALVEEXTENSINFITTED))
					removeJobItem(TireActionType.RIGIDVALVEEXTENSINFITTED);
			}
		}
		if (mRepair_value.getVisibility() == View.VISIBLE) {
			if (mRepair_value.getSelectedItemPosition() == 1) {
				removeJobItem(TireActionType.MAJORREPAIR);
				removeJobItem(TireActionType.MAJORREPAIRWITHVULCANISATION);
				removeJobItem(TireActionType.MINORREPAIR);
				removeJobItem(TireActionType.TYREREPAIREANDREINFORCEMENT);
				removeJobItem(TireActionType.PUNCTUREDREPAIRREPAIRED);
				if (!checkForActionType(TireActionType.BREAKSPOTREPAIR))
					updateJobItem(TireActionType.BREAKSPOTREPAIR);
			} else if (mRepair_value.getSelectedItemPosition() == 2) {
				removeJobItem(TireActionType.BREAKSPOTREPAIR);
				removeJobItem(TireActionType.MAJORREPAIRWITHVULCANISATION);
				removeJobItem(TireActionType.MINORREPAIR);
				removeJobItem(TireActionType.TYREREPAIREANDREINFORCEMENT);
				removeJobItem(TireActionType.PUNCTUREDREPAIRREPAIRED);
				if (!checkForActionType(TireActionType.MAJORREPAIR))
					updateJobItem(TireActionType.MAJORREPAIR);
			} else if (mRepair_value.getSelectedItemPosition() == 3) {
				removeJobItem(TireActionType.BREAKSPOTREPAIR);
				removeJobItem(TireActionType.MAJORREPAIR);
				removeJobItem(TireActionType.MINORREPAIR);
				removeJobItem(TireActionType.TYREREPAIREANDREINFORCEMENT);
				removeJobItem(TireActionType.PUNCTUREDREPAIRREPAIRED);
				if (!checkForActionType(TireActionType.MAJORREPAIRWITHVULCANISATION))
					updateJobItem(TireActionType.MAJORREPAIRWITHVULCANISATION);
			} else if (mRepair_value.getSelectedItemPosition() == 4) {
				removeJobItem(TireActionType.BREAKSPOTREPAIR);
				removeJobItem(TireActionType.MAJORREPAIR);
				removeJobItem(TireActionType.MAJORREPAIRWITHVULCANISATION);
				removeJobItem(TireActionType.TYREREPAIREANDREINFORCEMENT);
				removeJobItem(TireActionType.PUNCTUREDREPAIRREPAIRED);
				if (!checkForActionType(TireActionType.MINORREPAIR))
					updateJobItem(TireActionType.MINORREPAIR);
			}
			if (mRepair_array.size() == 6) {
				if (mRepair_value.getSelectedItemPosition() == 5) {
					removeJobItem(TireActionType.BREAKSPOTREPAIR);
					removeJobItem(TireActionType.MAJORREPAIR);
					removeJobItem(TireActionType.MAJORREPAIRWITHVULCANISATION);
					removeJobItem(TireActionType.MINORREPAIR);
					removeJobItem(TireActionType.PUNCTUREDREPAIRREPAIRED);
					if (!checkForActionType(TireActionType.TYREREPAIREANDREINFORCEMENT))
						updateJobItem(TireActionType.TYREREPAIREANDREINFORCEMENT);
				}
			} else if (mRepair_array.size() == 7) {
				if (mRepair_value.getSelectedItemPosition() == 5) {
					removeJobItem(TireActionType.BREAKSPOTREPAIR);
					removeJobItem(TireActionType.MAJORREPAIR);
					removeJobItem(TireActionType.MAJORREPAIRWITHVULCANISATION);
					removeJobItem(TireActionType.MINORREPAIR);
					removeJobItem(TireActionType.TYREREPAIREANDREINFORCEMENT);
					if (!checkForActionType(TireActionType.PUNCTUREDREPAIRREPAIRED))
						updateJobItem(TireActionType.PUNCTUREDREPAIRREPAIRED);
				} else if (mRepair_value.getSelectedItemPosition() == 6) {
					removeJobItem(TireActionType.BREAKSPOTREPAIR);
					removeJobItem(TireActionType.MAJORREPAIR);
					removeJobItem(TireActionType.MAJORREPAIRWITHVULCANISATION);
					removeJobItem(TireActionType.MINORREPAIR);
					removeJobItem(TireActionType.PUNCTUREDREPAIRREPAIRED);
					if (!checkForActionType(TireActionType.TYREREPAIREANDREINFORCEMENT))
						updateJobItem(TireActionType.TYREREPAIREANDREINFORCEMENT);
				}
			}
			if (mRepair_value.getSelectedItemPosition() == 0) {
				removeJobItem(TireActionType.BREAKSPOTREPAIR);
				removeJobItem(TireActionType.MAJORREPAIR);
				removeJobItem(TireActionType.MAJORREPAIRWITHVULCANISATION);
				removeJobItem(TireActionType.MAJORREPAIRWITHVULCANISATION);
				removeJobItem(TireActionType.TYREREPAIREANDREINFORCEMENT);
				removeJobItem(TireActionType.PUNCTUREDREPAIRREPAIRED);
			}
		}
		//Condition aaded to check and update inspection data for the selected tire
		if(!Constants.SELECTED_TYRE.isInspected()) {
			InspectionDataHandler.getMyInstance(getApplicationContext()).updateJobitemForInspection(Constants.SELECTED_TYRE,false);
		}

		//User Trace logs
		try {
			String traceData;
			traceData = "\n\t\t"+mModifiedElements.getNskValue()+
					"| " +Constants.SELECTED_TYRE.getPressure()+
					"| " +mModifiedElements.isValveFitted()+
					"| " +mModifiedElements.isHpValveCapFitted()+
					"| " +mModifiedElements.isValveFixingSupport()+
					"| " +mModifiedElements.isTireFill()+
					"| " +mValve_extn_value.getSelectedItem().toString()+
					"| " +mRepair_value.getSelectedItem().toString() ;

			LogUtil.TraceInfo(TRACE_TAG, "none","Data : " + traceData, false, false, false);
		}
		catch (Exception e)
		{
			LogUtil.TraceInfo(TRACE_TAG, "Data : Exception : ", e.getMessage(), false, true, false);
		}

		updateTireImages(String.valueOf(jobID), external_id);
		// close keyboard if open
		try {
			CommonUtils.hideKeyboard(this, getWindow().getDecorView()
					.getRootView().getWindowToken());
		} catch (Exception e) {
			LogUtil.i("Mount PWT on activity return",
					"could not close keyboard");
		}
		captureFinalState();
	}

	/**
	 * Removing Job Item
	 */
	private void removeJobItem(String actionType) {
		if (VehicleSkeletonFragment.mJobItemList != null
				&& VehicleSkeletonFragment.mJobItemList.size() > 0) {
			for (int i = 0; i < VehicleSkeletonFragment.mJobItemList.size(); i++) {
				JobItem jobItem = VehicleSkeletonFragment.mJobItemList.get(i);
				if (jobItem.getActionType().equals(actionType)
						&& Constants.SELECTED_TYRE.getExternalID().equals(
								jobItem.getTyreID())) {
					jobItem.setDeleteStatus(1);
					break;
				}
			}
		}
	}

	/**
	 * Update the Job Item
	 */
	private void updateJobItem(String actionType) {
		try {
			LogUtil.DBLog(TAG,"JobItem Data","Insertion Initialized");
		if (VehicleSkeletonFragment.mJobItemList != null) {
			jobID = getJobItemIDForThisJobItem();
			external_id = String.valueOf(UUID.randomUUID());
			int len = VehicleSkeletonFragment.mJobItemList.size();
			JobItem jobItem = new JobItem();
			jobItem.setActionType(actionType);
			jobItem.setAxleNumber("0");
			jobItem.setAxleServiceType("0");
			jobItem.setCasingRouteCode("");
			jobItem.setDamageId("");
			jobItem.setExternalId(external_id);
			jobItem.setGrooveNumber(null);
			jobItem.setJobId(String.valueOf(jobID));
			jobItem.setMinNSK(mNsk_value.getText().toString());
			jobItem.setNote(sNoteText);
			jobItem.setNskOneAfter("0");
			jobItem.setNskOneBefore("0");
			jobItem.setNskThreeAfter("0");
			jobItem.setNskThreeBefore("0");
			jobItem.setNskTwoAfter("0");
			jobItem.setNskTwoBefore("0");
			jobItem.setOperationID("");
			jobItem.setPressure(mFinalConvertedPressure);
			jobItem.setRecInflactionDestination("0");
			jobItem.setRecInflactionOrignal(Constants.SELECTED_TYRE
					.getPressure());
			jobItem.setRegrooved("False");
			jobItem.setReGrooveNumber(null);
			jobItem.setRegrooveType("0");
			jobItem.setRemovalReasonId("");
			jobItem.setRepairCompany(null);
			jobItem.setRimType("0");
			jobItem.setSapCodeTilD("0");
			jobItem.setSequence(String.valueOf(len + 1));
			jobItem.setServiceCount("1");
			jobItem.setServiceID("0");
			jobItem.setSwapType("0");
			jobItem.setTorqueSettings("0");
			jobItem.setThreaddepth("0");
			jobItem.setTyreID(mExternalIdTyre);
			jobItem.setWorkOrderNumber(null);
			jobItem.setSwapOriginalPosition(null);
			jobItem.setDeleteStatus(-1);
			LogUtil.DBLog(TAG,"Job Item Data","Inserted"+jobItem.toString() );
			VehicleSkeletonFragment.mJobItemList.add(jobItem);
			LogUtil.DBLog(TAG,"Job Item Data","Inserted");
			}
		} catch (Exception e) {
			LogUtil.DBLog(TAG,"Job Item Data","Exception--"+e.getMessage());
			e.printStackTrace();
		}
	}

	private int getImagePositionInImageObject(String id, int currentPos) {
		//TODO: remove this and move to check state change
		Constants.SELECTED_TYRE.setWorkedOn(true);
		int counter = 1;
		for (int imageIterator = 0; imageIterator < VehicleSkeletonFragment.mTireImageItemList
				.size(); imageIterator++) {
			if (VehicleSkeletonFragment.mTireImageItemList.get(imageIterator)
					.getJobItemId().equals(id)) {
				if (counter == currentPos) {
					return imageIterator;
				}
				counter++;
			}
		}
		return -1;
	}

	/**
	 * inserting tire images in TyreImage Object
	 */
	private void updateTireImages(String id, String externalID) {
		try {
			int posToAdd = -1;
			if (TextUtils.isEmpty(mjobIDForImages)
					&& Constants.sBitmapPhoto1 != null) {
				mjobIDForImages = id;
				mExternal_idForImages = externalID;
			}
			if (Constants.sBitmapPhoto1 != null) {
				posToAdd = getImagePositionInImageObject(mjobIDForImages, 1);
				TireImageItem item1 = new TireImageItem();
				item1.setJobItemId(mjobIDForImages);
				if (mCameraEnabled)
					item1.setImageType("0");
				else
					item1.setImageType("1");
				item1.setDamageDescription(Constants.DAMAGE_NOTES1);
				item1.setTyreImage(CameraUtility
						.getByteFromBitmap(Constants.sBitmapPhoto1,
								Constants.sBitmapPhoto1Quality));
				item1.setJobExternalId(mExternal_idForImages);
				if (posToAdd > -1) {
					VehicleSkeletonFragment.mTireImageItemList.set(posToAdd,
							item1);
					posToAdd = -1;
				} else {
					VehicleSkeletonFragment.mTireImageItemList.add(item1);
				}
			}
			if (Constants.sBitmapPhoto2 != null) {
				posToAdd = getImagePositionInImageObject(mjobIDForImages, 2);
				TireImageItem item2 = new TireImageItem();
				item2.setJobItemId(mjobIDForImages);
				item2.setImageType("1");
				item2.setDamageDescription(Constants.DAMAGE_NOTES2);
				item2.setTyreImage(CameraUtility
						.getByteFromBitmap(Constants.sBitmapPhoto2,
								Constants.sBitmapPhoto2Quality));
				item2.setJobExternalId(mExternal_idForImages);
				//VehicleSkeletonFragment.mTireImageItemList.add(item2);
				if (posToAdd > -1) {
					VehicleSkeletonFragment.mTireImageItemList.set(posToAdd,
							item2);
					posToAdd = -1;
				} else {
					VehicleSkeletonFragment.mTireImageItemList.add(item2);
				}
			}
			if (Constants.sBitmapPhoto3 != null) {
				posToAdd = getImagePositionInImageObject(mjobIDForImages, 3);
				TireImageItem item3 = new TireImageItem();
				item3.setJobItemId(mjobIDForImages);
				item3.setImageType("1");
				item3.setDamageDescription(Constants.DAMAGE_NOTES3);
				item3.setTyreImage(CameraUtility
						.getByteFromBitmap(Constants.sBitmapPhoto3,
								Constants.sBitmapPhoto3Quality));
				item3.setJobExternalId(mExternal_idForImages);
				//VehicleSkeletonFragment.mTireImageItemList.add(item3);
				if (posToAdd > -1) {
					VehicleSkeletonFragment.mTireImageItemList.set(posToAdd,
							item3);
					posToAdd = -1;
				} else {
					VehicleSkeletonFragment.mTireImageItemList.add(item3);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @return
	 */
	private int getJobItemIDForThisJobItem() {
		int countJobItemsNotPresent = 0;
		mDbHelper.open();
		int currentJobItemCountInDB = mDbHelper.getJobItemCount();
		Cursor cursor = null;
		try {
			cursor = mDbHelper.getJobItemValues();
			if (!CursorUtils.isValidCursor(cursor)) {
				return countJobItemsNotPresent + 1
						+ VehicleSkeletonFragment.mJobItemList.size();
			}
			for (JobItem jobItem : VehicleSkeletonFragment.mJobItemList) {
				boolean flag = false;
				for (boolean hasItem = cursor.moveToFirst(); hasItem; hasItem = cursor
						.moveToNext()) {
					if (jobItem.getExternalId().equals(
							cursor.getString(cursor
									.getColumnIndex("ExternalID")))) {
						countJobItemsNotPresent++;
						break;
					}
				}
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			mDbHelper.close();
			// CursorUtils.closeCursor(cursor);
		}
		return currentJobItemCountInDB - countJobItemsNotPresent
				+ VehicleSkeletonFragment.mJobItemList.size() + 1;
	}

	@Override
	public void onBackPressed() {
		if (!checkOriginalState()) {
			actionBackPressed();
		} else {
			LogUtil.TraceInfo(TRACE_TAG, "none", "Back Press", false, false, false);
			super.onBackPressed();
		}
	}

	private AlertDialog mBackAlertDialog;

	private void saveDialogState(Bundle state) {
		state.putBoolean("mBackAlertDialog",
				(mBackAlertDialog != null && mBackAlertDialog.isShowing()));
	}

	private void restoreDialogState(Bundle state) {
		if (state != null) {
			if (state.getBoolean("mBackAlertDialog")) {
				actionBackPressed();
			}
		}
	}

	@Override
	protected void onDestroy() {
		try {
			if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
				mBackAlertDialog.dismiss();
			}
			if(mDbHelper != null){
				mDbHelper.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	/**
	 * On Back press handling
	 */
	private void actionBackPressed() {
		LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", false, true, false);
		LayoutInflater li = LayoutInflater.from(WOTActivity.this);
		View promptsView = li
				.inflate(R.layout.dialog_logout_confirmation, null);
		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
				WOTActivity.this);
		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		alertDialogBuilder.setView(promptsView);
		// Setting The Info
		TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);

		infoTv.setText(mConfirmMsg);
		alertDialogBuilder.setCancelable(false).setPositiveButton(mYesLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						CameraUtility.resetParams();
						WOTActivity.this.finish();
					}
				});

		alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();

						// if user select "No", just cancel this dialog and
						// continue
						// with app
						dialog.cancel();
					}
				});
		// create alert dialog
		mBackAlertDialog = alertDialogBuilder.create();
		// show it
		mBackAlertDialog.show();
		mBackAlertDialog.setCanceledOnTouchOutside(false);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == MountTireActivity.NOTES_INTENT) {
			if (data != null) {
				sNoteText = data.getExtras().getString("mNoteEditText");
			}
		}
	}

	/**
	 * NSK Changed Textwatcher
	 */
	private class nskChanged implements TextWatcher {
		@Override
		public void afterTextChanged(Editable nskOne) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String strEnteredVal = mNsk_value.getText().toString();
			if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
				Float num = CommonUtils.parseFloat(strEnteredVal);
				if (num <= 30) {

				} else {
					mNsk_value.setText("");
				}
			} else if (strEnteredVal.equals(".")) {
				mNsk_value.setText("");
			}
		}
	}

	/**
	 * Swiper class
	 */
	public class Swiper implements OnTouchListener, OnClickListener {
		float startX, startY;
		float endX, endY;
		int selectedPositionToDelete;
		ArrayAdapter<String> adapterList;
		ScrollView view;
		private Context ctx;
		public static final float MINIMUM_MOVEMENT_REQUIRED = 100;

		public Swiper(Context ctx, ScrollView view) {
			this.ctx = ctx;
			this.view = view;
			view.setOnClickListener(this);
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getActionMasked()) {
			case MotionEvent.ACTION_DOWN:
				mSwiped = false;
				startX = event.getX();
				startY = event.getY();
				break;
			case MotionEvent.ACTION_UP:
				break;

			case MotionEvent.ACTION_MOVE:
				endX = event.getX();
				endY = event.getY();
				if (Math.abs(endX - startX) > MINIMUM_MOVEMENT_REQUIRED
						&& !mSwiped) {
					mSwiped = true;
					Rect rect = new Rect();
					int childCount = view.getChildCount();
					int[] listViewCoords = new int[2];
					view.getLocationOnScreen(listViewCoords);
					int x = (int) event.getRawX() - listViewCoords[0];
					int y = (int) event.getRawY() - listViewCoords[1];
					View child;
					for (int i = 0; i < childCount; i++) {
						child = view.getChildAt(i);
						child.getHitRect(rect);
						if (rect.contains(x, y)) {
							captureFinalState();
							String nskVal = mModifiedElements.getNskValue();
							float nskValFloat = CommonUtils.parseFloat(nskVal);

							if (!checkOriginalState()) {
								if (nskValFloat == 0.0 || nskValFloat == 0) {
									if(Constants.sLblEnterNskValues!=null)
									{
										LogUtil.TraceInfo(TRACE_TAG, "NSK Validation","Msg : " +Constants.sLblEnterNskValues,false,false,false);
									}
									else
									{
										LogUtil.TraceInfo(TRACE_TAG, "NSK Validation","Msg : Check NSk",false,false,false);
									}
									CommonUtils.notify(
											Constants.sLblEnterNskValues,
											getBaseContext());
								} else if (!CommonUtils
										.pressureValueValidation(mPressure_value)) {

									if(Constants.sLblCheckPressureValue!=null)
									{
										LogUtil.TraceInfo(TRACE_TAG, "Pressure Value Validation","Msg : " +Constants.sLblCheckPressureValue,false,false,false);
									}
									else
									{
										LogUtil.TraceInfo(TRACE_TAG, "Pressure Value Validation","Msg : Check Pressure Value",false,false,false);
									}

									CommonUtils.notify(
											Constants.sLblCheckPressureValue,
											getBaseContext());
								} else {
									Constants.SELECTED_TYRE.setWorkedOn(true);
									Constants.NSK_SET_WOT = nskVal;
									new PerformanceBaseModel(WOTActivity.this,
											callback, false).perform();
									// onReturnSkeleton();
								}
							} else {
								/**
								 * //actionBackPressed(); Bug Id.331 doing a WOT
								 * with no change in the screen should not
								 * display the
								 * "are you really sure you want to cancel"
								 */
								/*external_id = mExternal_idForImages;
								updateTireImages(String.valueOf(jobID),
										external_id);*/
								CameraUtility.resetParams();
								finish();
								WOTActivity.this.overridePendingTransition(
										R.anim.slide_left_in,
										R.anim.slide_left_out);
							}
						}
					}
				}
				break;
			}
			return false;
		}

		@Override
		public void onClick(View arg0) {
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable("retainInitialElement", mInitialElements);
		outState.putParcelable("retainFinalElement", mModifiedElements);
		outState.putString("retainExternalIdTyre", mExternalIdTyre);
		outState.putString(PREVIOUS_JOBID_KEY, mjobIDForImages);
		outState.putString(PREVIOUS_EXTERNAL_ID_KEY, mExternal_idForImages);
		saveDialogState(outState);
	}

	/**
	 * If axle have pressure apply the value then disable otherwise it's
	 * editable Setting the Pressure Reseting Parameters
	 */
	public void resetParams() {
		Constants.sBitmapPhoto1 = null;
		Constants.sBitmapPhoto2 = null;
		Constants.sBitmapPhoto3 = null;
	}

	/**
	 * If axle have pressure apply the value then disable otherwise it's
	 * editable Setting the Pressure
	 */
	private void setValuetoPressueView() {
		if (recommendedpressure > 0) {
			mPressure_value.setText(CommonUtils.getPressureValue(
					WOTActivity.this, recommendedpressure));
			mPressure_unit_label.setText(CommonUtils
					.getPressureUnit(WOTActivity.this));
			mPressure_value.setEnabled(false);
		} else {
			if (mIsSettingsPSI) {
				mPressure_value
						.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
								4, 2) });
			} else {
				mPressure_value
						.setFilters(new InputFilter[] { new DecimalDigitsInputFilterNSK(
								3, 2) });
			}
			/*Bug 728 : Adding context to show toast for higher pressure value*/
			mPressure_value.addTextChangedListener(new PressureCheckListener(
					mPressure_value, mIsSettingsPSI, getApplicationContext()));
			mPressure_unit_label.setText(CommonUtils
					.getPressureUnit(WOTActivity.this));
		}
	}

	IOnPerformanceCallback callback = new IOnPerformanceCallback() {

		@Override
		public void updateTables() {
			onReturnSkeleton();
		}

		@Override
		public void showProgressBar(boolean toShow) {
			if (toShow) {
				// Show Progress Bar here
			} else {
				// Hide Progress bar here
			}
		}

		@Override
		public void onPostExecute(ResponseMessagePerformance message) {
			boolean putTickMark = true;
			putTickMark = ((mFromDefineTire) ? false : checkOriginalState());
			Intent intentReturn = new Intent();
			intentReturn.putExtra("puttickmark", putTickMark);
			intentReturn.putExtra("wot", true);
			setResult(VehicleSkeletonFragment.WOT_INTENT, intentReturn);
			CameraUtility.resetParams();
			finish();
			WOTActivity.this.overridePendingTransition(R.anim.slide_left_in,
					R.anim.slide_left_out);
		}

		@Override
		public String onPreExecute() {
			String messageStr = PerformanceBaseModel.MESSAGE_STR;
			return messageStr;
		}

		@Override
		public void onError(ResponseMessagePerformance message) {
			CommonUtils.notify(message.getErrorMessage(),
					getApplicationContext());
		}

		@Override
		public void closePreviousTask() {

		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		sNoteText = "";
		InactivityUtils.logoutFromActivityBelowEjobForm(this);
	}

}
