package com.goodyear.ejob;

import android.app.Activity;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.CursorUtils;
import com.goodyear.ejob.util.LogUtil;

import java.util.ArrayList;

public class DamageNotesActivity extends Activity implements InactivityHandler{

	/**
	 * Used for common database handler
	 */
	private DatabaseAdapter mDbHelper;
	/**
	 * ArrayList containing damage type options
	 */
	private ArrayList<String> mDamageTypeList;
	/**
	 * ArrayList containing damage area options
	 */
	private ArrayList<String> mDamageAreaList;
	/**
	 * String that stores selected damage type option
	 */
	private String mSelectedDamageType;
	/**
	 * String that stores selected damage area option
	 */
	private String mSelectedDamageArea;
	// VIEWS
	/**
	 * Spinner to select the tyre damage type
	 */
	private Spinner mDamageTypeSpinner;
	/**
	 * Spinner to select the tyre damage area
	 */
	private Spinner mDamageAreaSpinner;
	/**
	 * EditText that takedown the notes for the damaged tyre
	 */
	private EditText mNotesEditText;

	//User Trace logs trace tag
	private static final String TRACE_TAG = "Damage Note";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.damage_tire_note);
		//User Trace logs
		LogUtil.TraceInfo(TRACE_TAG, "none", TRACE_TAG, false, false, false);
		mDbHelper = new DatabaseAdapter(this);
		mDamageTypeList = new ArrayList<>();
		mDamageAreaList = new ArrayList<>();
		mDamageTypeSpinner = (Spinner) findViewById(R.id.spinner_DamageType);
		mDamageAreaSpinner = (Spinner) findViewById(R.id.spinner_DamageArea);
		mNotesEditText = (EditText) findViewById(R.id.note);
		loadDamageType();
		if (Constants.DAMAGE_NOTES1 != null) {
			String[] damageTextWithDelimiter = Constants.DAMAGE_NOTES1
					.split("#");
			String damageNote;
			if (damageTextWithDelimiter.length > 1) {
				damageNote = damageTextWithDelimiter[1];
			} else {
				damageNote = damageTextWithDelimiter[0];
			}
			mNotesEditText.setText(damageNote);
		}
	}

	/**
	 * Fetches the damage type options from database,saves it to mDamageTypeList
	 * and sets the adapter to mDamageTypeSpinner
	 */
	private void loadDamageType() {
		Cursor damageTypeCursor = null;
		Cursor breakSpotCursor = null;
		try {
			mDbHelper.open();
			damageTypeCursor = mDbHelper.getDamageGroup();
			if (CursorUtils.isValidCursor(damageTypeCursor)) {
				damageTypeCursor.moveToFirst();
				mDamageTypeList.clear();
				while (!damageTypeCursor.isAfterLast()) {
					mDamageTypeList.add(damageTypeCursor
							.getString(damageTypeCursor
									.getColumnIndex("DamageGroup")));
					damageTypeCursor.moveToNext();
				}
				CursorUtils.closeCursor(damageTypeCursor);
			}
			ArrayAdapter<String> damageTypeAdapter = new ArrayAdapter<>(this,
					android.R.layout.simple_spinner_item, mDamageTypeList);
			damageTypeAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mDamageTypeSpinner.setAdapter(damageTypeAdapter);
			if (Constants.SELECTED_DAMAGE_TYPE != null) {
				mSelectedDamageType = Constants.SELECTED_DAMAGE_TYPE;
			} else if (Constants.ISBREAKSPOT) {
				breakSpotCursor = mDbHelper.getBreakSpotDetails();
				if (CursorUtils.isValidCursor(breakSpotCursor)) {
					breakSpotCursor.moveToFirst();
					mSelectedDamageType = breakSpotCursor
							.getString(breakSpotCursor
									.getColumnIndex("DamageGroup"));
					mDamageTypeSpinner.setEnabled(false);
					CursorUtils.closeCursor(breakSpotCursor);
				}
			}
			for (int i = 0; i < mDamageTypeList.size(); i++) {
				if (mDamageTypeList.get(i).equals(mSelectedDamageType)) {
					mDamageTypeSpinner.setSelection(i);
				}
			}
			mDamageTypeSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							mSelectedDamageType = mDamageTypeSpinner
									.getSelectedItem().toString();
							loadDamageArea();
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {
							Toast.makeText(getApplicationContext(),
									Constants.sLblNothingSelected,
									Toast.LENGTH_SHORT).show();
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(damageTypeCursor);
			CursorUtils.closeCursor(breakSpotCursor);
			mDbHelper.close();
		}
	}

	/**
	 * Fetches the damage area options from database,saves it to mDamageAreaList
	 * and sets the adapter to mDamageAreaSpinner
	 */
	private void loadDamageArea() {
		Cursor damageAreaCursor = null;
		Cursor breakSpotCursor = null;
		try {
			mDbHelper.open();
			damageAreaCursor = mDbHelper.getDamageArea(mSelectedDamageType);
			if (CursorUtils.isValidCursor(damageAreaCursor)) {
				damageAreaCursor.moveToFirst();
				mDamageAreaList.clear();
				while (!damageAreaCursor.isAfterLast()) {
					mDamageAreaList.add(damageAreaCursor
							.getString(damageAreaCursor
									.getColumnIndex("RemovalReasonCode")));
					damageAreaCursor.moveToNext();
				}
			}
			CursorUtils.closeCursor(damageAreaCursor);
			ArrayAdapter<String> damageAreaAdapter = new ArrayAdapter<>(this,
					android.R.layout.simple_spinner_item, mDamageAreaList);
			damageAreaAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mDamageAreaSpinner.setAdapter(damageAreaAdapter);
			if (Constants.SELECTED_DAMAGE_AREA != null) {
				mSelectedDamageArea = Constants.SELECTED_DAMAGE_AREA;
			} else if (Constants.ISBREAKSPOT) {
				breakSpotCursor = mDbHelper.getBreakSpotDetails();
				if (CursorUtils.isValidCursor(breakSpotCursor)) {
					breakSpotCursor.moveToFirst();
					mSelectedDamageArea = breakSpotCursor
							.getString(breakSpotCursor
									.getColumnIndex("RemovalReasonCode"));
					mDamageAreaSpinner.setEnabled(false);
					CursorUtils.closeCursor(breakSpotCursor);
				}
			}
			for (int i = 0; i < mDamageAreaList.size(); i++) {
				if (mDamageAreaList.get(i).equals(mSelectedDamageArea)) {
					mDamageAreaSpinner.setSelection(i);
				}
			}
			mDamageAreaSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView adapter, View v,
								int i, long lng) {
							mSelectedDamageArea = mDamageAreaSpinner
									.getSelectedItem().toString();
						}

						@Override
						public void onNothingSelected(AdapterView arg0) {

							Toast.makeText(getApplicationContext(),
									Constants.sLblNothingSelected,
									Toast.LENGTH_SHORT).show();
						}
					});
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CursorUtils.closeCursor(damageAreaCursor);
			CursorUtils.closeCursor(breakSpotCursor);
			mDbHelper.close();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.note, menu);
		return true;
	}

	@Override
	protected void onPause() {
		Constants.SELECTED_DAMAGE_TYPE = mDamageTypeSpinner.getSelectedItem()
				.toString();
		Constants.SELECTED_DAMAGE_AREA = mDamageAreaSpinner.getSelectedItem()
				.toString();
		if (!mNotesEditText.getText().toString().equals("")
				&& mNotesEditText.getText().toString() != null) {
			Constants.DAMAGE_NOTES1 = mNotesEditText.getText().toString();
		}
		super.onPause();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			LogUtil.TraceInfo(TRACE_TAG, "none", "Settings", false, false, false);
			return true;
		case R.id.action_save:
			LogUtil.TraceInfo(TRACE_TAG, "none", "Save", false, false, false);
			Constants.SELECTED_DAMAGE_TYPE = mDamageTypeSpinner
					.getSelectedItem().toString();
			Constants.SELECTED_DAMAGE_AREA = mDamageAreaSpinner
					.getSelectedItem().toString();
			if (!mNotesEditText.getText().toString().equals("")
					&& mNotesEditText.getText().toString() != null) {
				Constants.DAMAGE_NOTES1 = mNotesEditText.getText().toString();
			}
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStart() {
		super.onStart();
		// set user inactivity handler for this activity
		LogoutHandler.setCurrentActivity(this);
	}

	@Override
	public void onUserInteraction() {
		InactivityUtils.updateActivityOfUser();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
	 * ()
	 */
	@Override
	public void logUserOutDueToInactivity() {
		InactivityUtils.logoutFromActivityAboveEjobForm(this);
	}
}
