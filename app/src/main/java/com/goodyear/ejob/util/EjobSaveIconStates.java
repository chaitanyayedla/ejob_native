package com.goodyear.ejob.util;

/**
 * this class holds the different states on save icon. 0 = grey (not saveble) 1
 * = black (saveble but nothing new in objects) 2 = red (saveble and new data in
 * objects)
 */
public class EjobSaveIconStates {
	public static final int ICON_GRAY = 0;
	public static final int ICON_BLACK = 1;
	public static final int ICON_RED = 2;
}
