package com.goodyear.ejob.util;

/**
 * @author shailesh.p
 * 
 */
public class SyncTime {
	private String value;
	private String uiTranslation;
	private String SyncRequired;

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the uiTranslation
	 */
	public String getUiTranslation() {
		return uiTranslation;
	}

	/**
	 * @param uiTranslation the uiTranslation to set
	 */
	public void setUiTranslation(String uiTranslation) {
		this.uiTranslation = uiTranslation;
	}

	/**
	 * @return the syncRequired
	 */
	public String getSyncRequired() {
		return SyncRequired;
	}

	/**
	 * @param syncRequired the syncRequired to set
	 */
	public void setSyncRequired(String syncRequired) {
		SyncRequired = syncRequired;
	}
}
