package com.goodyear.ejob.util;

/**
 * @author shailesh.p
 * 
 */
public class Contract {
	private String contractId;
	private String contractType;
	private String customerName;
	private String licenseNum;
	private String chassisNum;
	private String vin;
	private String vehichleConfiguration;
	private String isDIffSizeAllowedForVehicle;
	private String vehicleSapCode;
	private String vehicleODO;

	/**
	 * @return the contractId
	 */
	public String getContractId() {
		return contractId;
	}

	/**
	 * @param contractId the contractId to set
	 */
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	/**
	 * @return the contractType
	 */
	public String getContractType() {
		return contractType;
	}

	/**
	 * @param contractType the contractType to set
	 */
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the licenseNum
	 */
	public String getLicenseNum() {
		return licenseNum;
	}

	/**
	 * @param licenseNum the licenseNum to set
	 */
	public void setLicenseNum(String licenseNum) {
		this.licenseNum = licenseNum;
	}

	/**
	 * @return the chassisNum
	 */
	public String getChassisNum() {
		return chassisNum;
	}

	/**
	 * @param chassisNum the chassisNum to set
	 */
	public void setChassisNum(String chassisNum) {
		this.chassisNum = chassisNum;
	}

	/**
	 * @return the vin
	 */
	public String getVin() {
		return vin;
	}

	/**
	 * @param vin the vin to set
	 */
	public void setVin(String vin) {
		this.vin = vin;
	}

	/**
	 * @return the vehichleConfiguration
	 */
	public String getVehichleConfiguration() {
		return vehichleConfiguration;
	}

	/**
	 * @param vehichleConfiguration the vehichleConfiguration to set
	 */
	public void setVehichleConfiguration(String vehichleConfiguration) {
		this.vehichleConfiguration = vehichleConfiguration;
	}

	/**
	 * @return the isDIffSizeAllowedForVehicle
	 */
	public String getIsDIffSizeAllowedForVehicle() {
		return isDIffSizeAllowedForVehicle;
	}

	/**
	 * @param isDIffSizeAllowedForVehicle the isDIffSizeAllowedForVehicle to set
	 */
	public void setIsDIffSizeAllowedForVehicle(
			String isDIffSizeAllowedForVehicle) {
		this.isDIffSizeAllowedForVehicle = isDIffSizeAllowedForVehicle;
	}

	/**
	 * @return the vehicleSapCode
	 */
	public String getVehicleSapCode() {
		return vehicleSapCode;
	}

	/**
	 * @param vehicleSapCode the vehicleSapCode to set
	 */
	public void setVehicleSapCode(String vehicleSapCode) {
		this.vehicleSapCode = vehicleSapCode;
	}

	/**
	 * @return the vehicleODO
	 */
	public String getVehicleODO() {
		return vehicleODO;
	}

	/**
	 * @param vehicleODO the vehicleODO to set
	 */
	public void setVehicleODO(String vehicleODO) {
		this.vehicleODO = vehicleODO;
	}

	@Override
	public String toString() {
		return "\n\t\tContract Id : "+ contractId +", "+
				" Contract Type : "+ contractType+", " +
				" Customer Name : "+ CommonUtils.getBase64DecodedValue(customerName) +", "+
				" License Number : "+ licenseNum+", " +
				" Chassis Num : "+ chassisNum+", " +
				" Vin : "+ vin +", "+
				" Vehicle Configuration : "+ vehichleConfiguration+", " +
				" Is DIff Size Allowed For Vehicle : "+ isDIffSizeAllowedForVehicle +", "+
				" Vehicle Sap Code : "+ vehicleSapCode+", " +
				" Vehicle ODO : "+ vehicleODO;

	}



}
