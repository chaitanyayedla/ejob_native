package com.goodyear.ejob.util;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.goodyear.ejob.fragment.VehicleSkeletonFragment;

/**
 * @author shailesh.p
 * 
 */
public class JsonParser {

	/**
	 * parse the json received from server into Contract object in a array list
	 * 
	 * @param jsonArray
	 *            json String
	 * @return Array list with parsed contract json object
	 * @throws JSONException
	 */
	public static ArrayList<Contract> getContracts(String jsonArray)
			throws JSONException {
		// get json array from json string
		JSONArray jarray = new JSONArray(jsonArray);
		ArrayList<Contract> contractListFromServer = new ArrayList<Contract>();

		for (int i = 0; i < jarray.length(); i++) {
			// create new contract object
			Contract contract = new Contract();
			JSONObject contractfromJson = jarray.getJSONObject(i);

			// fill contract object
			contract.setContractId(contractfromJson.getString("ContractID"));
			contract.setContractType(contractfromJson.getString("ContractType"));
			contract.setCustomerName(contractfromJson.getString("CustomerName"));
			contract.setLicenseNum(contractfromJson.getString("LicenseNum"));
			contract.setChassisNum(contractfromJson.getString("ChassisNum"));
			contract.setVin(contractfromJson.getString("Vin"));
			contract.setVehichleConfiguration(contractfromJson
					.getString("VehicleConfiguration"));
			contract.setIsDIffSizeAllowedForVehicle(contractfromJson
					.getString("IsDiffSizeAllowedForVehicle"));
			contract.setVehicleSapCode(contractfromJson
					.getString("VehicleSapCode"));
			contract.setVehicleODO(contractfromJson.getString("VehicleODO"));

			// add contract object to arraylist
			contractListFromServer.add(contract);
		}
		return contractListFromServer;
	}

	/**
	 * Parse the tire json string (received from server) into an arraylist with
	 * tire objects
	 * 
	 * @param tireJson
	 *            tire json string
	 * @return arraylist with parsed tire objects
	 * @throws JSONException
	 */
	public static ArrayList<Tyre> getTyreInfo(String tireJson)
			throws JSONException {
		return getTyreInfo(tireJson, 0);
	}

	/**
	 * Parse the tire json string (received from server) into an arraylist with
	 * tire objects
	 * 
	 * @param tireJson
	 *            tire json string
	 * @param axleCount
	 *            axle count for this vehicle
	 * @return arraylist with parsed tire objects
	 * @throws JSONException
	 */
	public static ArrayList<Tyre> getTyreInfo(String tireJson, int axleCount)
			throws JSONException {
		// get json array
		JSONArray jarray = new JSONArray(tireJson);
		ArrayList<Tyre> tyreInfo = new ArrayList<Tyre>();

		int spareCounter = 1;
		for (int i = 0; i < jarray.length(); i++) {
			// get each tire
			JSONObject tyre = jarray.getJSONObject(i);

			// new tire object
			Tyre newTireFromServer = new Tyre();
			// set newtirefromserver
			newTireFromServer.setSerialNumber(tyre.getString("SerialNumber"));
			newTireFromServer.setSize(tyre.getString("Size"));
			newTireFromServer.setRim(tyre.getString("RIM"));
			newTireFromServer.setSapCodeWP(tyre.getString("SAPCodeWP"));
			newTireFromServer.setSapCodeTI(tyre.getString("SAPCodeTi"));
			newTireFromServer.setBrandName(tyre.getString("BrandName"));
			newTireFromServer.setDesign(tyre.getString("Design"));
			newTireFromServer.setDesignDetails(tyre.getString("DesignDetails"));
			newTireFromServer.setSapMaterialCodeID(tyre
					.getString("SAPMaterialCodeID"));
			newTireFromServer.setSapContractNumberID(tyre
					.getString("SAPContractNumberID"));
			newTireFromServer.setSapVenderCodeID(tyre
					.getString("SAPVendorCodeID"));
			newTireFromServer.setExternalID(tyre.getString("ExternalID"));

			String nskFromServer;
			try {
				float nskVal = CommonUtils.parseFloat(tyre.getString("NSK"));
				nskFromServer = String.valueOf(nskVal);
			} catch (Exception e) {
				e.printStackTrace();
				nskFromServer = tyre.getString("NSK");
			}
			newTireFromServer.setNsk(nskFromServer);
			newTireFromServer.setNsk2(nskFromServer);
			newTireFromServer.setNsk3(nskFromServer);
			newTireFromServer.setOrignalNSK(nskFromServer);
			newTireFromServer.setPosition(tyre.getString("Position"));
			newTireFromServer.setOriginalPosition(tyre.getString("Position"));
			newTireFromServer.setPressure(tyre.getString("Pressure"));

			if (axleCount != 0 && Boolean.parseBoolean(tyre.getString("IsSpare"))) {
				int axleCountForSpare = axleCount + 1;
					newTireFromServer.setPosition(axleCountForSpare + "SP"// changed to SP
							+ spareCounter++);
			}
			newTireFromServer.setSpare(tyre.getString("IsSpare"));
			newTireFromServer.setSerialNumber(tyre.getString("SerialNumber"));
			newTireFromServer.setAsp(tyre.getString("ASP"));
			newTireFromServer.setVehicleJob(Constants.contract
					.getVehicleSapCode());
			newTireFromServer.setStatus("0");
			// if no serial set tire as non maintained tyre
			if (newTireFromServer.getSerialNumber().equals("")
					|| newTireFromServer.getSerialNumber().equalsIgnoreCase(
							"n/a"))
				newTireFromServer.setTyreState(TyreState.NON_MAINTAINED);
			else
				newTireFromServer.setTyreState(TyreState.PART_WORN);
			// add object to array list.
			tyreInfo.add(newTireFromServer);
		}
		return tyreInfo;
	}
}
