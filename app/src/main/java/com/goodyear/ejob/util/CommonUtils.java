package com.goodyear.ejob.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goodyear.ejob.GoodYear_eJob_MainActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.dbhelpers.DataBaseHelper;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.EjobSignatureBoxFragment;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.interfaces.AlertDialogHandler;
import com.goodyear.ejob.service.UserInactivityService;
import com.goodyear.ejob.sync.Constant;

import java.io.FileNotFoundException;
import java.io.IOException;
import android.database.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Locale;

/**
 * @author shailesh.p
 * 
 */
public class CommonUtils {

	public static final String TAG = CommonUtils.class.getSimpleName();

	/**
	 * make toast message
	 * 
	 * @param message
	 */
	public static void displayErrorMessage(String message, Context ctx) {

		Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
	}

	public static Date getDate(long datevalue) {

		return new Date(datevalue);
	}

	public static Date currentDateTime() {

		return new Date();
	}

	public static long daysDiffWithToday(Date after) {
		Calendar calendar1 = new GregorianCalendar();
		Calendar calendar2 = new GregorianCalendar();
		calendar1.setTime(currentDateTime());
		calendar2.setTime(after);
		long milliseconds1 = calendar1.getTimeInMillis();
		long milliseconds2 = calendar2.getTimeInMillis();
		long diff = milliseconds2 - milliseconds1;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		return diffDays; // 1000 * 60 * 60 * 24
	}

	public static void notify(String msg, Context ctx) {
		// notification code
		final Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.show();

	}

	// for Length Long
	public static void notifyLong(String msg, Context ctx) {
		// notification code
		try {
			final Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			toast.show();
		} catch(Exception e){
			e.printStackTrace();
		}


	}

	public static void toastHandler(String msg, Context ctx) {
		final Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				toast.cancel();
			}
		}, 1000);
	}

	/**
	 * log the message
	 * 
	 * @param msg
	 * @throws FileNotFoundException
	 */
	public static void log(String msg) {

	}

	public static void createInformationDialog(Context context, String message) {
		createInformationDialog(context, message, null);
	}

	/**
	 * create information dialog
	 * 
	 * @param context
	 *            current context
	 * @param message
	 *            message to be displayed
	 * @param handler
	 *            handler on OK
	 */
	public static void createInformationDialog(final Context context,
			final String message, final AlertDialogHandler handler) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li
				.inflate(R.layout.dialog_logout_confirmation, null);
		final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(context);
		alertDialogBuilder.setView(promptsView);

		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		// Setting The Info
		TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
		infoTv.setText(message);
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// update user activity on button click
						alertDialogBuilder.updateInactivityForDialog();
						LogUtil.i("CommonUtils", "Checking condition for ForceLogOut");
						LogUtil.i("Common Utils", "Constants.APP_UPGRADE_STATUS - " + Constants.APP_UPGRADE_STATUS);
						LogUtil.i("Common Utils", "Constants.Force_Download_Sync_STATUS - " + Constants.Force_Download_Sync_STATUS);
						LogUtil.i("Common Utils", "Constants.IS_SHOWING_DIALOG - " + Constants.IS_SHOWING_DIALOG);

						//Added Force Logout Module
						//If force sync is failed - APP_UPGRADE_STATUS will be true
						if(Constants.APP_UPGRADE_STATUS || Constants.Force_Download_Sync_STATUS)
						{
							LogUtil.i("CommonUtils","ForceLogout");
							Constants.IS_SHOWING_DIALOG=false;
							forceLogout(context);
						}
						else{
							LogUtil.i("CommonUtils","else of ForceLogout");
							if (handler != null) {
								LogUtil.i("CommonUtils","else of ForceLogout && handler is not nll");
								handler.onClickOK(context);
							} else {
								LogUtil.i("CommonUtils","else of ForceLogout && handler is nll");
								dialog.dismiss();
								Constants.ALERTFORNONETWORKWHILESYNC = true;
							}
						}
					}
				});
		try {
			LogUtil.i("CommonUtils","Show Dialog");

			if(Constants.APP_UPGRADE_STATUS || Constants.Force_Download_Sync_STATUS)
			{
				LogUtil.i("Common Utils", "show dialog : Constants.IS_SHOWING_DIALOG - " + Constants.IS_SHOWING_DIALOG);
				if(!Constants.IS_SHOWING_DIALOG)
				{
					Constants.IS_SHOWING_DIALOG=true;
					LogUtil.i("Common Utils", "show dialog : ++ - ");
					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
				}
			}
			else {
				LogUtil.i("Common Utils", "show dialog : + - ");
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
			}

		}
		catch(Exception e)
		{
			Constants.IS_SHOWING_DIALOG=false;
			LogUtil.i("CommonUtils",e.getMessage());
			//createInformationDialog(Constants.mainActivity,message,handler);
		}
	}

	public static String getPathForDBFile(Context mContext) {

		SharedPreferences preferences = mContext.getSharedPreferences(
				Constants.GOODYEAR_CONF, 0);

		if (preferences.getString(Constants.DATABASE_LOCATION, "not found")
				.equals(Constants.EXTERNAL_DB_LOCATION)) {
			return System.getenv("SECONDARY_STORAGE") + Constants.PROJECT_PATH
					+ "/" + Constants.USER + ".db3";
		}
		// else phone memory
		else {
			return Constants.PROJECT_PATH + "/" + Constants.USER + ".db3";
		}
	}

	public static String getPressureUnit(Context mContext) {
		
		try {
			SharedPreferences preferences = mContext.getSharedPreferences(
				Constants.GOODYEAR_CONF, 0);
			if (preferences.getString(Constants.PRESSUREUNIT, "").equals(
					Constants.PSI_PRESSURE_UNIT)) {
				return Constants.PSI_PRESSURE_UNIT;
			} else
				return Constants.BAR_PRESSURE_UNIT;
		} catch (Exception e) {
			return "";
		}
	}

	public static String getPressureValue(Context mContext, float valueInBar) {
		if (null == mContext) {
			return String.valueOf(0.0f);
		}
		String measure = getPressureUnit(mContext);
		if (measure.equals(Constants.PSI_PRESSURE_UNIT)) {
			return String.valueOf(Math.round(valueInBar * 14.5038f));
		} else {
			/*Bug 728 : Bar maximum value is changed to 10.34*/
			if (valueInBar > Constants.BAR_MAX_VALUE) {
				/**
				 * Internal bug: If a user enters a value 150 in PSI and then
				 * changes the settings to BAR it may show greater than 10.34.
				 * But as per client's requirement, BAR value can not be greater
				 * than 10.34.
				 */
				return String.valueOf(Constants.BAR_MAX_VALUE);
			} else {
				return String.format(Locale.ENGLISH, "%.2f", valueInBar);
			}
		}
	}

	public static String getFinalConvertedPressureValue(
			float recommendedpressure, String editTextPressureValue,
			boolean isSettingsPSI) {
		String mFinalConvertedPressure = "0";
		try {
			if (recommendedpressure > 0) {
				mFinalConvertedPressure = String.valueOf(recommendedpressure);
			} else if (isSettingsPSI) {
				/*
				 * PSI to BAR Conversion and value storing into
				 * mFinalConvertedPressure variable
				 */
				mFinalConvertedPressure = String.valueOf(CommonUtils
						.parseFloat(editTextPressureValue) * 0.068947f);
			} else {
				mFinalConvertedPressure = String.valueOf(editTextPressureValue);
			}
		} catch (Exception e) {
			LogUtil.e(TAG, "getFinalConvertedPressureValue: " + e.getMessage());
		}

		return mFinalConvertedPressure;
	}

	public static String getPressureValue(Context mContext, String valueInBar) {
		if (null != valueInBar) {
			try {
				float barValueInFloat = CommonUtils.parseFloat(valueInBar);
				return getPressureValue(mContext, barValueInFloat);
			} catch (NumberFormatException ne) {
				ne.printStackTrace();
			}
		}
		return "0";
	}

	public static String pressureConversionInBarFromPSI(String valueInPSI) {
		if (null != valueInPSI && "".equals(valueInPSI)) {
			try {
				float psiValue = CommonUtils.parseFloat(valueInPSI);
				return String.valueOf(psiValue * 0.068947f);
			} catch (NumberFormatException ne) {
				ne.printStackTrace();
			}
		}
		return "0";
	}

	public static String getRFIDReaderType(Context mContext) {
		SharedPreferences preferences = mContext.getSharedPreferences(
				Constants.GOODYEAR_CONF, 0);

		if (preferences.getString(Constants.RFIDREADER, "").equals(
				Constants.RFID_TYPE_NONE)) {
			return Constants.RFID_TYPE_NONE;
		} else
			return Constants.RFID_TYPE_TLOGIC;
	}

	public static ArrayList<String> removeDuplicatesFromArrayList(
			ArrayList<String> al) {
		HashSet hs = new HashSet();
		hs.addAll(al);
		al.clear();
		al.addAll(hs);
		return al;
	}

	public static int parseInt(String value) {
		int parsedValue = 0;
		try {
			if (!TextUtils.isEmpty(value)) {
				Locale theLocale = Locale.getDefault();
				NumberFormat numberFormat = DecimalFormat
						.getInstance(theLocale);
				Number theNumber;
				try {
					theNumber = numberFormat.parse(value);
					return theNumber.intValue();
				} catch (ParseException e) {
					LogUtil.i(TAG, "Handling Exception while parsing to double");
					String valueWithDot = value.replaceAll(",", ".");
					parsedValue = Integer.valueOf(valueWithDot);
				}
			}
		} catch (Exception e) {
			LogUtil.i(TAG,
					"Exception>> Parse String into Int: " + e.getMessage());
		}
		return parsedValue;
	}

	public static float parseFloat(String value) {
		float parsedValue = 0.0f;
		try {
			if (!TextUtils.isEmpty(value)) {
				Locale theLocale = Locale.getDefault();
				NumberFormat numberFormat = DecimalFormat
						.getInstance(theLocale);
				Number theNumber;
				try {
					theNumber = numberFormat.parse(value);
					return theNumber.floatValue();
				} catch (ParseException e) {
					LogUtil.i(TAG, "Handling Exception while parsing to double");
					String valueWithDot = value.replaceAll(",", ".");
					parsedValue = Float.valueOf(valueWithDot);
				}
			}
		} catch (Exception e) {
			LogUtil.i(TAG,
					"Exception>> Parse String into Float: " + e.getMessage());
		}
		return parsedValue;
	}

	public static double parseDouble(String value) {
		double parsedValue = 0;
		try {
			if (!TextUtils.isEmpty(value)) {
				Locale theLocale = Locale.getDefault();
				NumberFormat numberFormat = DecimalFormat
						.getInstance(theLocale);
				Number theNumber;
				try {
					theNumber = numberFormat.parse(value);
					parsedValue = theNumber.doubleValue();
				} catch (ParseException e) {
					LogUtil.i(TAG, "Handling Exception while parsing to double");
					String valueWithDot = value.replaceAll(",", ".");
					parsedValue = Double.valueOf(valueWithDot);
				}
			}
		} catch (Exception e) {
			LogUtil.i(TAG,
					"Exception>> Parse String into Double: " + e.getMessage());
		}
		return parsedValue;
	}

	// request focus
	public static void setFocus(final EditText editText) {
		editText.post(new Runnable() {
			@Override
			public void run() {
				View v = ((Activity) editText.getContext()).getCurrentFocus();
				if (v != null) {
					v.clearFocus();
				}
				try {
					editText.setFocusableInTouchMode(true);
					editText.setSelection(editText.getText().length());
					if (editText.requestFocus()) {
						InputMethodManager imm = (InputMethodManager) editText
								.getContext().getSystemService(
										Context.INPUT_METHOD_SERVICE);
						if (imm.isActive(v)) {
							imm.hideSoftInputFromWindow(v.getWindowToken(),
									InputMethodManager.HIDE_NOT_ALWAYS);
						}
						imm.showSoftInput(editText,
								InputMethodManager.SHOW_FORCED);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// hide keyboard
	public static void hideKeyboard(Context context, IBinder iBinder) {
		InputMethodManager in = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(iBinder, 0);
	}

	public static String getMinNskValue(String nsk1, String nsk2, String nsk3) {
		float nsk1_val, nsk2_val, nsk3_val;
		if ((null == nsk1) || (null != nsk1 && "".equals(nsk1))) {
			nsk1_val = 0;
		} else {
			nsk1_val = CommonUtils.parseFloat(nsk1);
		}
		if ((null == nsk1) || (null != nsk2 && "".equals(nsk2))) {
			nsk2_val = 0;
		} else {
			nsk2_val = CommonUtils.parseFloat(nsk2);
		}
		if ((null == nsk1) || (null != nsk3 && "".equals(nsk3))) {
			nsk3_val = 0;
		} else {
			nsk3_val = CommonUtils.parseFloat(nsk3);
		}
		return String.valueOf(Math.min(Math.min(nsk1_val, nsk2_val), nsk3_val));
	}

	/**
	 * @return
	 */
	public static TireDesignDetails getDetailsFromTyreInSameAxle(
			Tyre selectedTire) {
		if (null == selectedTire) {
			return null;
		}

		boolean isSpare = Boolean.parseBoolean(selectedTire.isSpare());
		if (selectedTire != null && isSpare
				&& !TextUtils.isEmpty(selectedTire.getDesignDetails())) {
			return new TireDesignDetails(selectedTire.getSize(),
					selectedTire.getAsp(), selectedTire.getRim());
		} else if (!isSpare) {

			/*
			 * if(Boolean.parseBoolean(selectedTire.isSpare())){ return new
			 * TireDesignDetails(selectedTire.getSize(), selectedTire.getAsp(),
			 * selectedTire.getRim()); }
			 */
			int axle = Integer.parseInt(selectedTire.getPosition().substring(0,
					1));
			for (Tyre tireInVehicle : VehicleSkeletonFragment.tyreInfo) {
				int currentAxlePos = Integer.parseInt(tireInVehicle
						.getPosition().substring(0, 1));
				if (currentAxlePos == axle
						&& !TextUtils.isEmpty(tireInVehicle.getDesignDetails())) {
					return new TireDesignDetails(tireInVehicle.getSize(),
							tireInVehicle.getAsp(), tireInVehicle.getRim());
				}
			}
		}
		return null;

	}

	public static TireDesignDetails getDetailsFromTyreInSameAxleForSwap(
			Tyre selectedTire) {
		if (null == selectedTire) {
			return null;
		}
		int axle = Integer.parseInt(selectedTire.getPosition().substring(0, 1));
		for (Tyre tireInVehicle : VehicleSkeletonFragment.tyreInfo) {
			int currentAxlePos = Integer.parseInt(tireInVehicle.getPosition()
					.substring(0, 1));
			if (currentAxlePos == axle
					&& !TextUtils.isEmpty(tireInVehicle.getDesignDetails())) {
				return new TireDesignDetails(tireInVehicle.getSize(),
						tireInVehicle.getAsp(), tireInVehicle.getRim());
			}
		}
		return new TireDesignDetails("0", "0", "0");
	}

	/**
	 * Filter to check if letter only
	 */
	public static InputFilter alphaFilter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end,
				Spanned dest, int dstart, int dend) {
			boolean keepOriginal = true;
			StringBuilder sb = new StringBuilder(end - start);
			for (int i = start; i < end; i++) {
				char c = source.charAt(i);
				int asciiVal = (int) c;
				// Patch: blocking pi (960, 928 for nexus 7) and delta (916)
				if (asciiVal == 916 || asciiVal == 960 || asciiVal == 928) {
					keepOriginal = false;
				} else {
					if (isCharAllowed(c)) { // put your condition here
						sb.append(c);
					} else {
						keepOriginal = false;
					}
				}
			}
			if (keepOriginal) {
				return null;
			} else {
				if (source instanceof Spanned) {
					SpannableString sp = new SpannableString(sb);
					TextUtils.copySpansFrom((Spanned) source, start,
							sb.length(), null, sp, 0);
					return sp;
				} else {
					return sb;
				}
			}
		}

		/**
		 * It will allow only letter and space.
		 * 
		 * @param c
		 * @return boolean
		 */
		private boolean isCharAllowed(char c) {
			int asciiVal = (int) c;
			LogUtil.v("ASCII code", "_" + asciiVal);
			return (Character.isLetter(c) || Character.isSpaceChar(c));
		}
	};

	/**
	 * Return the NSK value with proper format
	 */
	public static String getParsedNSKValue(String nsk) {
		if (null == nsk) {
			return "0.0";
		}
		if ("0.0".equals(nsk)) {
			return nsk;
		}
		String val = nsk.replaceFirst("^0+(?!$)", "");
		if (TextUtils.isEmpty(val)) {
			return "0.0";
		}

		if (val.startsWith(".")) {
			val = "0" + val;
		}
		if (val.contains(".") && (val.endsWith("0") && !val.endsWith(".0"))) {
			val = val.substring(0, val.length() - 1);
		}
		if (!val.contains(".")) {
			val = val + ".0";
		}
		if (val.endsWith(".")) {
			val = val + "0";
		}
		return val;
	}

	/**
	 * Validating NSK value. If all nsk values more than 0 then it will return
	 * true, other wise retun false
	 */
	public static boolean validateNSKValues(String nsk1, String nsk2,
			String nsk3) {
		float nsk1_val, nsk2_val, nsk3_val;
		if (TextUtils.isEmpty(nsk1)) {
			nsk1_val = 0;
		} else {
			nsk1_val = CommonUtils.parseFloat(nsk1);
		}
		if (TextUtils.isEmpty(nsk2)) {
			nsk2_val = 0;
		} else {
			nsk2_val = CommonUtils.parseFloat(nsk2);
		}
		if (TextUtils.isEmpty(nsk3)) {
			nsk3_val = 0;
		} else {
			nsk3_val = CommonUtils.parseFloat(nsk3);
		}
		if (nsk1_val > 0 && nsk2_val > 0 && nsk3_val > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean pressureValueValidation(EditText edittxt) {
		if (null != edittxt && !TextUtils.isEmpty(edittxt.getText().toString())) {
			double valueInFloat = parseDouble(edittxt.getText().toString());
			if (Constants.PSI_PRESSURE_UNIT.equals(CommonUtils
					.getPressureUnit(edittxt.getContext()))) {
				if (valueInFloat <= Constants.PSI_MAX_VALUE
						&& valueInFloat >= Constants.PSI_MIN_VALUE) {
					return true;
				} else {
					return false;
				}
			} else {
				if (valueInFloat <= Constants.BAR_MAX_VALUE
						&& valueInFloat >= Constants.BAR_MIN_VALUE) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * API used to allow/block the operations on different axels
	 * 
	 * @return // False is to allow, True is to block
	 */
	public static boolean checkDifferentSizeForAxle() {
		boolean check = false;
		TireDesignDetails detail = null, detail2 = null;
		if (Constants.SELECTED_TYRE == null) {
			Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
		}
		if (Constants.SECOND_SELECTED_TYRE == null) {
			Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
		}
		String firstAxleNo = Constants.SELECTED_TYRE.getPosition().charAt(0)
				+ "";
		String secondAxleNo = Constants.SECOND_SELECTED_TYRE.getPosition()
				.charAt(0) + "";

		if (!firstAxleNo.equalsIgnoreCase(secondAxleNo)) {
			detail = CommonUtils
					.getDetailsFromTyreInSameAxleForSwap(Constants.SELECTED_TYRE);
			detail2 = CommonUtils
					.getDetailsFromTyreInSameAxleForSwap(Constants.SECOND_SELECTED_TYRE);
			if (!detail.getSize().equalsIgnoreCase(detail2.getSize())
					&& (detail == null && detail2 == null)
					&& (!(Boolean.parseBoolean(Constants.SELECTED_TYRE
							.isSpare())) || (Boolean
							.parseBoolean(Constants.SECOND_SELECTED_TYRE
									.isSpare())))) {
				check = true;
			}
		}
		return check;
	}

	/**
	 * API used to allow/block the operations on different axels
	 * 
	 * @return // False is to allow, True is to block
	 */
	public static boolean checkDifferentAxleForSwap() {
		TireDesignDetails detail = null, detail2 = null;
		if (Constants.SELECTED_TYRE == null) {
			Constants.SELECTED_TYRE = Constants.SOURCE_TYRE;
		}
		if (Constants.SECOND_SELECTED_TYRE == null) {
			Constants.SECOND_SELECTED_TYRE = Constants.DES_TYRE;
		}
		String firstAxleNo = Constants.SELECTED_TYRE.getPosition().charAt(0)
				+ "";
		String secondAxleNo = Constants.SECOND_SELECTED_TYRE.getPosition()
				.charAt(0) + "";

		boolean isSpare = (Boolean.valueOf(Constants.SELECTED_TYRE.isSpare()) || Boolean
				.valueOf(Constants.SECOND_SELECTED_TYRE.isSpare())) ? true
				: false;
		if (!firstAxleNo.equalsIgnoreCase(secondAxleNo)) {
			if (isSpare) {
				detail = new TireDesignDetails(
						Constants.SELECTED_TYRE.getSize(), "0", "0");
				detail2 = new TireDesignDetails(
						Constants.SECOND_SELECTED_TYRE.getSize(), "0", "0");
				if (!Boolean.valueOf(Constants.SELECTED_TYRE.isSpare())) {
					detail = CommonUtils
							.getDetailsFromTyreInSameAxleForSwap(Constants.SELECTED_TYRE);
				}
				if (!Boolean.valueOf(Constants.SECOND_SELECTED_TYRE.isSpare())) {
					detail2 = CommonUtils
							.getDetailsFromTyreInSameAxleForSwap(Constants.SECOND_SELECTED_TYRE);
				}
			} else {
				detail = CommonUtils
						.getDetailsFromTyreInSameAxleForSwap(Constants.SELECTED_TYRE);
				detail2 = CommonUtils
						.getDetailsFromTyreInSameAxleForSwap(Constants.SECOND_SELECTED_TYRE);
			}
			if ((detail.getSize().equalsIgnoreCase("0") || detail2.getSize()
					.equalsIgnoreCase("0"))
					|| ((TextUtils.isEmpty(detail.getSize()) || TextUtils
							.isEmpty(detail2.getSize())))) {
				return false;
			} else if ((detail != null && detail2 != null)
					&& detail.getSize().equalsIgnoreCase(detail2.getSize())) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	/**
	 * takes the note with delimiter and returns the user entered note to
	 * display
	 * 
	 * @param noteText
	 *            note text with delimiter
	 * @return user entered note
	 */
	public static String getUserEnteredNoteAndRemoveDelimiter(String noteText) {
		String[] damageTextWithDelimiter = noteText.split("\\|");
		String userDamagenote = "";
		if (damageTextWithDelimiter.length > 1) {
			userDamagenote = damageTextWithDelimiter[1];
		}
		return userDamagenote;
	}

	/**
	 * takes the note with delimiter and returns the max min nsk to display
	 * 
	 * @param noteText
	 *            note text with delimiter
	 * @return user entered max min nsk
	 */
	public static String[] getUserEnteredMaxMinNSKFromNoteWithDelimiter(
			String noteText) {
		// this is the format ==> TD maximum : 5 TD minimum : 2|
		// "|" is the delimiter set by Goodyear
		if (noteText.contains("|")) {
			String valueofNSKMax;
			String valueofNSKMin;
			// for nsk max
			valueofNSKMax = getNSKValue(noteText, ":", "mm ", 1);
			// for nsk min
			valueofNSKMin = getNSKValue(noteText, ":", "mm", 2);

			try {
				Float.parseFloat(valueofNSKMin.trim());
				Float.parseFloat(valueofNSKMax.trim());
				return new String[] { valueofNSKMax, valueofNSKMin };
			} catch (NumberFormatException e) {
				return null;
			}
		}
		// String[] damageTextWithDelimiter = noteText.split("#");
		// if (damageTextWithDelimiter[0].contains("$")) {
		// String[] minMaxNSK = damageTextWithDelimiter[0].split("\\$");
		// return minMaxNSK;
		// } else {
		return null;
		// }
	}

	private static String getNSKValue(String note, String startExp,
			String endExp, int flag) {
		int startIndex;
		int endIndex;
		if (flag == 1) {
			startIndex = note.indexOf(startExp) + 1;
			String partialNote = note.substring(startIndex, note.length())
					.trim();
			endIndex = startIndex + partialNote.indexOf(endExp) + 1;
		} else {
			startIndex = note.lastIndexOf(startExp);
			endIndex = note.lastIndexOf(endExp);
		}
		if (startIndex > 0 && endIndex > 0) {
			return note.substring(startIndex + 1, endIndex);
		}
		return "";
	}

	public static String getBase64DecodedValue(String encodedString) {
		byte[] data = Base64.decode(encodedString, Base64.DEFAULT);
		String result;
		try {
			result=new String(data, "UTF-8");
			return result;
		}
		catch (Exception e)
		{
			return e.getMessage();
		}
	}

	/**
	 * CR:: 469 This method is collecting all the info related to the
	 * application installed and the device details It then inserts all the
	 * collected data in the note field of Job table
	 */
	public static String getDeviceInfo(Context context) {
		String mAppversionNumber = "";
		try {
			PackageInfo pinfo;
			pinfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
			mAppversionNumber = pinfo.versionName;
		} catch (PackageManager.NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String deviceBrand = Build.BRAND.toUpperCase();
		String deviceModel = Build.MODEL.toUpperCase();
		String deviceManufacturer = Build.MANUFACTURER.toUpperCase();
		String deviceOSVersion = Build.VERSION.RELEASE;

		return "App info : " + mAppversionNumber + " , Device info :  " + deviceBrand
				+ " | " + deviceModel + " | " + deviceManufacturer + " | "
				+ deviceOSVersion + " | " + EjobSignatureBoxFragment.getScreenResolution(context);
	}

	/**
	 * Method to perform Force Logout
	 * @param context
	 */
	public static void forceLogout(Context context)
	{
		LogUtil.i("CommonUtils","Forcelogout");
		Constants._CHECKFORLOGINAUTOFILL = true;
		DatabaseAdapter db = new DatabaseAdapter(context);
		db.createDatabase();
		db.open();
		try {
			db.copyFinalDB();
			db.deleteDB();
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		db.close();

		// stop inactivity service
		InactivityUtils.stopUserActivityUpdates();
		context.stopService(new Intent(context,
				UserInactivityService.class));

		Intent intent = new Intent(context,
				GoodYear_eJob_MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	/**
	 * Method to get application upgrade level (minor, major)
	 * @param context Context
	 * @param currentVersion current app version
	 * @return
	 */
	public static int checkAppUpgradeType(Context context,String currentVersion)
	{
		String secondIDFromPS = currentVersion.substring(2, 3);
		String thirdIDFromPS = currentVersion.substring(4, 5);
		String fourthIDFromPS = currentVersion.substring(6, 7);
		PackageInfo pinfo;
		try {
			pinfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
			String versionNumber = pinfo.versionName;

			String secondVN = versionNumber.substring(2, 3);
			String thirdVN = versionNumber.substring(4, 5);
			String fourthVN = versionNumber.substring(6, 7);
			if (Integer.parseInt(secondIDFromPS) >= Integer
					.parseInt(secondVN)
					&& Integer.parseInt(thirdIDFromPS) >= Integer
					.parseInt(thirdVN)
					&& Integer.parseInt(fourthIDFromPS) >= Integer
					.parseInt(fourthVN)) {
				return ApplicationVersionDifference.NO_UPGRADE;
			} else {
				if (Integer.parseInt(secondVN) == Integer
						.parseInt(secondIDFromPS)) {
					return ApplicationVersionDifference.MINOR_UPGRADE;
				} else {

					return ApplicationVersionDifference.MAJOR_UPGRADE;
				}
			}
		} catch (PackageManager.NameNotFoundException e) {
			LogUtil.i(TAG,"checkAppUpgradeType : Exception - "+e.getMessage());
			e.printStackTrace();
			return ApplicationVersionDifference.OTHER;
		}
	}

	/**
	 * Method to update the Constants OLD_APP_VERSION_NUMBER
	 * @param context
	 */
	public static void updateOldAppVersionNumber (Context context)
	{
		PackageInfo pinfo;
		try {
			pinfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
			String versionNumber = pinfo.versionName;
			SharedPreferences prefer = context.getSharedPreferences(Constants.GOODYEAR_CONF, 0);
			SharedPreferences.Editor editor = prefer.edit();

			editor.putString(Constants.OLD_APP_VERSION_NUMBER, versionNumber);
			editor.commit();
			LogUtil.i(TAG,
					"updateOldAppVersionNumber - updated");
		}
		catch(Exception e)
		{
			LogUtil.i(TAG,
					"updateOldAppVersionNumber : Exception - " +e.getMessage());
		}
	}

}
