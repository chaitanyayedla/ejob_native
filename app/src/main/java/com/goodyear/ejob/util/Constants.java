package com.goodyear.ejob.util;

import android.app.Activity;
import android.graphics.Bitmap;

import com.goodyear.ejob.dbmodel.JobCorrection;
import com.goodyear.ejob.dbmodel.VisualRemark;
import com.goodyear.ejob.job.operation.helpers.JobItem;
import com.goodyear.ejob.job.operation.helpers.TireImageItem;
import com.goodyear.ejob.job.operation.helpers.TorqueSettings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.WeakHashMap;

/**
 * @author shailesh.p
 * 
 */
public class Constants {

	public static final int LABEL_DISMOUNT_TYRE = 174;
	public static final int LABEL_TIRE_DETAILS = 74;
	public static final int LABEL_ADDITIONAL_SERVICES = 147;
	public static final int LABEL_ADDITIONAL_SERVICES_SUMMARY = 435;
	public static final int LABEL_SERVICE = 139;
	public static final int LABEL_QUANTITY = 140;
	public static final int LABEL_NONE = 300;
	public static final int LABEL_GEOMETRY_CHECK = 192;
	public static final int LABEL_GEOMETRY_CHECK_N_CORRECTION = 193;
	public static final int LABEL_EJOBS = 1;
	public static final int LABEL_DATE_DIALOG_TITLE = 234;
	public static final int LABEL_OK = 292;
	public static final int LABEL_ALREADY_ADDED = 395;
	public static final int LABEL_SEARCH = 6;
	public static final int LABEL_LOGICAL_SWAP = 110;
	public static final int LABEL_MOUNT_NEW_TYRE = 173;
	public static final int LABEL_PHYSICAL_SWAP = 111;
	public static final int LABEL_RESERVE_PWT = 350;
	public static final int LABEL_TORQUE_SETTINGS = 316;
	public static final int LABEL_WOT = 69;
	public static final int LABEL_EDIT = 295;
	public static final int LABEL_CONFIRM = 22;
	public static final int LABEL_WORK_ON_TIRE = 372;
	public static final int GPS_ADDRESS_LABEL = 436;
	public static final int APPLY_LABEL = 437;
	public static final int VEHICLE_FRONT = 442;
	public static final int VEHICLE_REAR = 450;
	public static final int GPS_LAT = 445;
	public static final int GPS_LNG = 447;
	public static final int PLEASE_PAIR_TLOGIK = 472;
	public static final int MULTIPLE_PAIRED_DEVICES_FOUND = 467;
	public static final int NO_NETWORK = 469;
	public static final int MANDATORY_DAMAGE_NOTES = 463;
	public static final int WHEEL_POS = 452;
	public static final int LAUNCH_NEW_JOB_TITLE = 446;
	public static final int REC_INFLACTION = 451;
	public static final int POSITION_LABEL = 141;
	public static final int RECCOMENDED_PRESSURE = 448;
	public static final int NM_RETORQUE_UNIT = 449;
	public static final int TIRE_HISTORY = 498;
	public static final int SWAP_NOTES = 195;// 481;
	public static final int REGROOE_NOTES = 195;// 482;
	public static final int TOR_NOTES = 195;// 483;
	public static final int DISMOUNT_TIRE_NOTES = 195;// 488;
	public static final int MOUNT_PWT_NOTES = 195;// 480;
	public static final int MOUNT_NEW_TIRE = 195;// 489;
	//public static final int PLEASE_SELECT_BRAND = 490;
	public static final int NO_PAIRED_DEVICES = 470;
	public static final int SELECT_BRAND = 480;
	public static final int FLEET_NO = 441;
	public static final int DEVICE_DOES_NOT_SUPPORT_BT = 477;
	public static final int ENTER_NSK_VALUES = 89;
	public static final int PLEASE_ENTER_SERIAL_NUM = 367;
	public static final int SELECT_RESERVE_SERIAL_NUM = 482;
	public static final int DESIGN_IS_REQUIRED = 82;
	public static final int PHOTO_IS_REQUIRED = 185;
	public static final int NO_NETWORK_TRY_AGAIN = 342;
	public static final int NO_TYRES_FOUND = 398;
	public static final int SELECTED_DATE_CANNOT_BE_LESS_THAN_CURR = 411;
	public static final int TYRE_TYPE_IS_REQUIRED = 82;
	public static final int TYRE_BRAND_IS_REQUIRED = 155;
	public static final int CHECK_PRESSURE_VALUE = 223;
	public static final int NOTHING_SELECTED = 172;
	public static final int JOB_DETAILS = 444;
	public static final int PLEASE_SELECT_LABEL = 232;


	public static String sLblNoNetwork = "No Network!";
	public static String sLblPleasePairTLogik = "Please pair a TLOGIK device";
	public static String sLblMultiplePaired = "Multiple paired devices found, please unpair which are not used";
	public static String sLblDeviceDontSupportBT = "Your device does not support Bluetooth";
	public static String sLblWheelPos = "WP";
	public static String sLblNoPairedDevices = "No paired devices";
	public static String sLblEnterNskValues = "You'll need to enter ar least one NSK";
	public static String sLblEnterSerialNumber = "Please enter a serial number";
	public static String sLblNothingSelected = "Please select an action";

	public static String USER;
	public static String VALIDATE_USER;

	public static int position;
	public static boolean _imageVisiblity = false;

	// project
	public static String GOODYEAR_CONF = "goodyear_shared";
	public static String PROJECT_PATH = "";
	public static String PROJECT_PATH_FOR_BELOW_KITKAT = "/Android/data/com.goodyear.ejob/files";
	public static String LOG_FILENAME = "logs.txt";
	// Need to change
	public static byte _BODYSIZE = 64;// 78;
	public static String _URL = "";

	// settings page
	public static final String PSI_PRESSURE_UNIT = "PSI";
	public static final String BAR_PRESSURE_UNIT = "BAR";
	public static final String SWITCH_ON = "ON";
	public static final String SWITCH_OFF= "OFF";
	public static String TORQUE_ENABLE= "OFF";
	
	public static String CAMERA_ENABLE_FOR_MIN_TD = "OFF";
	public static String INSPECTION_ENABLE = "OFF";
	public static float THRESHOLD_TD_VALUE = 1;
	public static float MAX_NSK_LEVEL = 30;
	public static String WRONG_NSK_LEVEL = "Wrong NSK Level";

	public static final String LANGUAGE_SETTINGS = "Language";
	/*#ForHalosys*/
	public static final String OFFLINE_PIN_SETTINGS = "OfflinePin";
	public static final String DATABASE_LOCATION = "DatabaseLoc";
	public static final String PRESSUREUNIT = "PressureUnit";
	public static final String RFIDREADER = "RFIDReader";
	public static final String PRESSURE = "PressureUnit";
	public static final String TORQUE = "TorqueSetting";
	public static final String INSPECTION = "InspectionSetting";
	public static final String CAMERA_FOR_MIN_NSK = "CameraSetting";
	public static final String MIN_TD_LEVEL_FOR_CAMERA = "ThresholdNSK";
	public static final String RFID_TIMOUT = "RFIDTimeOutReading";
	public static final String APP_VERSION = "appversion";
	public static final String INTERNAL_DB_LOCATION = "Phone Memory";
	public static final String EXTERNAL_DB_LOCATION = "SD Card";
	public static final String LastAppUpgradeFromPlayStore = "LastAppUpgradeFromPlayStore";
	public static final String AppUpgradeFromPlayStoreCancelled = "AppUpgradeFromPlayStoreCancelled";
	public static final String RFID_TYPE_NONE = "None";
	public static final String RFID_TYPE_TLOGIC = "Translogik";

	// re torque
	public static final String WHEELPOS = "wheel";
	public static final String JOBID = "jobid";
	public static final String ISWHEELREMOVED = "iswheelremoved";
	public static final String SAPCODEWP = "sapcode";

	public static final int EJOB_FORM_VEHICLE_SKELETON = 36;

	// language mappings
	public static final String ENGLISH = "English";
	public static final String FRENCH = "French";
	public static final String GERMAN = "German";
	public static final String POLISH = "Polish";
	public static final String NETHERLANDS = "Dutch";
	public static final String ITALY = "Italian";
	public static final String SPANISH = "Spanish";
	public static final String PORTUGUESE = "Portuguese";
	public static final String GREEK = "Greek";
	public static final String TURKISH = "Turkish";
	public static final String RUSSIAN = "Russian";
	public static final String AUSTRIAN = "Austrian";
	public static final String BELGIAN = "Belgian";
	public static final String BULGARIAN = "Bulgarian";
	public static final String SWISS = "Swiss";
	public static final String CZECH = "Czech";
	public static final String DANISH = "Danish";
	public static final String ESTONIAN = "Estonian";
	public static final String FINISH = "Finnish";
	public static final String CROATIAN = "Croatian";
	public static final String HUNGARIAN = "Hungarian";
	public static final String IRISH = "Irish";
	public static final String LITHUANIAN = "Lithuanian";
	public static final String LUXEMBOURG = "Luxembourgish";
	public static final String LATVIAN = "Latvian";
	public static final String NORWEGIAN = "Norwegian";
	public static final String ROMANIAN = "Romanian";
	public static final String SWEDISH = "Swedish";
	public static final String SLOVENIAN = "Slovenian";
	public static final String SLOVAKIAN = "Slovakian";

	public static final String ENGLISH_SHORT = "EN";
	public static final String FRENCH_SHORT = "FR";
	public static final String GERMAN_SHORT = "DE";
	public static final String POLISH_SHORT = "PL";
	public static final String NETHERLANDS_SHORT = "NL";
	public static final String ITALY_SHORT = "IT";
	public static final String SPANISH_SHORT = "SP";
	public static final String PORTUGUESE_SHORT = "PT";
	public static final String GREEK_SHORT = "EL";
	public static final String TURKISH_SHORT = "TR";
	public static final String RUSSIAN_SHORT = "RU";
	public static final String AUSTRIAN_SHORT = "AT";
	public static final String BELGIAN_SHORT = "BE";
	public static final String BULGARIAN_SHORT = "BG";
	public static final String SWISS_SHORT = "CH";
	public static final String CZECH_SHORT = "CZ";
	public static final String DANISH_SHORT = "DK";
	public static final String ESTONIAN_SHORT = "EE";
	public static final String FINISH_SHORT = "FI";
	public static final String CROATIAN_SHORT = "HR";
	public static final String HUNGARIAN_SHORT = "HU";
	public static final String IRISH_SHORT = "IE";
	public static final String LITHUANIAN_SHORT = "LT";
	public static final String LUXEMBOURG_SHORT = "LU";
	public static final String LATVIAN_SHORT = "LV";
	public static final String NORWEGIAN_SHORT = "NO";
	public static final String ROMANIAN_SHORT = "RO";
	public static final String SWEDISH_SHORT = "SE";
	public static final String SLOVENIAN_SHORT = "SI";
	public static final String SLOVAKIAN_SHORT = "SK";

	// login expiry messages
	public static final String PASSWORD_EXPIRE_14_DAYS = "Password will expire in 14 days";
	public static final String PASSWORD_EXPIRE_10_DAYS = "Password will expire in 10 days";
	public static final String PASSWORD_EXPIRE_7_DAYS = "Password will expire in 7 days";
	public static final String PASSWORD_EXPIRE_5_DAYS = "Password will expire in 5 days";
	public static final String PASSWORD_EXPIRE_3_DAYS = "Password will expire in 3 days";
	public static final String PASSWORD_EXPIRE_2_DAYS = "Password will expire in 2 days";
	public static final String PASSWORD_EXPIRE_1_DAYS = "Password will expire in 1 days";

	// error messages
	public static final String ERROR_NODB_NONETWORK = "Hello, You need an internet connection for your first eJob login.";
	public static final String ERROR_PASSWORD_EXPIRED = "Password Expired";
	public static final String ERROR_NO_SERVERULR = "no url for server time - please check the URL's in settings file";
	public static final String ERROR_USERNAME_PASSWORD_NOT_VALID = "Username or Password not valid";
	public static final String ERROR_INCORRECT_DATE_ON_DEVICE = "Please set correct date on the PDA";
	public static final String ERROR_NO_USER_FOUND = "No user found";
	public static final String ERROR_SETTING_NOT_LOADED = "Could not load Settings from XML";

	public static final int ERROR_DBFILE_NOT_MOVED = 462;
	public static final int ERROR_NO_DISK_SPACE = 461;

	public static final String ERROR_CONNECTION_WITH_SERVER = "Please check connections and try again.";
	public static final String PASSWORD_INCORRECT = "Please check your userid and/or password and try again.";
	public static final String PASSWORD_EMPTY = "User name and Password is required.";
	public static final String NEW_USER_DB_QUERYFILE = "createblankdb3.sql";
	public static final String NO_USER_FOUND = "No User Was Found in db";
	public static final String ERROR_NO_NETWORK_TO_SYNC = "Network not available to sync.";
	public static final String ERROR_SD_CARD_NOT_AVAILABLE = "SD card is not available, please insert SD Card";
	public static final String ERROR_SD_CARD_NOT_WRITEABLE = "SD card is not writeable";

	// vehicle skeleton
	public static final String INFO_TEXT_VIEW_NAME = "tireinfo";
	public static final String TIRE_HISTORY_HEADER = "tirehistoryhead";
	public static final String TIRE_HISTORY_HEADER_TEXT = "Tire History";
	public static boolean JOBTYPEREGULAR = false;
	public static boolean JOBTYPEBREAKDOWN = false;
	public static boolean GEMOTRY_CHECK_CORRECTION = false;
	public final static int SERIAL_INVALID = 75;

	// dismount screen
	public static final int REGULAR_JOB = 0;
	public static final int BREAKDOWN_JOB = 1;
	public static boolean ONSTATE_DISMOUNT = false;
	public static boolean ISBREAKSPOT = false;
	public static String SELECTED_DAMAGE_TYPE;
	public static String SELECTED_DAMAGE_AREA;
	public static String DAMAGE_NOTES1 = "";
	public static String DAMAGE_NOTES2 = "";
	public static String DAMAGE_NOTES3 = "";

	// mount screen
	public static boolean ONSTATE_MOUNT = false;

	// mount pwt screen
	public static boolean ONSTATE_MOUNTPWT = false;

	// mount pwt screen
	public static boolean ONSTATE_WOT = false;
	// inspection screen
	public static boolean ONSTATE_INSPECTION = false;
	public static boolean PRESSURE_EDIT_STATE;

	// re groove screen
	public static final int REGROOVE_RIB = 1;
	public static final int REGROOVE_BLOCK = 2;
	public static final int REGROOVE_RIB_LABEL = 226;
	public static final int REGROOVE_BLOCK_LABEL = 225;
	public static final int SERIAL_NUMBER = 418;
	public static final int REGROOV_ACTIVITY_NAME = 200;
	public static final int INVALID_PRESSURE_ENTRY = 223;

	// notes activity
	public static final int NOTES_ACTIVITY_NAME = 195;
	public static final String NOTES_CANCLE_MSG = "Do you want to cancel the Note?";

	// turn on rim
	public static int RIM_TYPE_LABEL = 391;
	public static int REGROOVED_LABEL = 133;
	public static final int TURN_ON_RIM_ACTIVITY_NAME = 112;
	public static int ALLOY = 360;
	public static int STEEL = 359;
	public static final int TOR_NSK_VALUES_INTERCHANGED_MSG = 471;

	// retorque
	public static final int RETORQUE_LABEL_ISSUED_LABEL = 236;
	public static final int WRENCH_NUMBER_LABEL = 314;
	public static final int WAS_THE_WHEEL_REMOVED_LABEL = 266;
	public static final int WAS_THE_RECOMMENDED_SETTINGS_APPLIED_LABEL = 267;

	// pressure check
	public static final int SAVE_LABEL = 370;
	public static final int CANCEL_LABEL = 293;
	public static final int YES_LABEL = 331;
	public static final int NO_LABEL = 332;
	public static final int REGROOVE_IS_REQUIRED = 333;
	public static final int REGROOVE_TYPE_IS_REQUIRED = 207;
	public static final int  DELETE_REASON_LABEL = 481;
	public static final int ERROR_MESSAGE = 199;
	// public static final int APPLY = ;
	// public static final int REC_INFLACTION_LABEL = ;
	// public static final int NEW_RECOMMENDED = ;
	public static final int RETORQUE_VALUE_APPLIED = 237;
	public static final int TORQUE_SETTINGS = 316;

	// db label row numbers
	public static int SETTINGS_ACTIVITY_NAME_LABEL = 278;
	public static int LANGUAGE_LABEL_DB_ROW = 282;
	public static int DATABASE_LABEL_DB_ROW = 328;
	public static int PRESSURE_LABEL_DB_ROW = 281;
	public static int TORQUE_LABEL_DB_ROW = 113;
	public static int VERSION_LABEL_DB_ROW = 327;
	public static int RFID_TIME_OUT_LABEL = 414;
	public static int RFID_TYPE_LABEL = 415;
	public static int EJOB_CONFIG_LABEL = 279;
	public static int SECONDS_LABEL = 416;

	public static int ACCOUNT_LABEL = 319;
	public static int VEH_ID_LABEL = 318;
	public static int REF_NUMBER_LABEL = 298;
	public static int AXLE_SERVICES_TITLE = 146;
	public static int WORK_ON_AXLE_LABEL = 188;
	public static int AGREE_STATUS_LABEL = 381;
	public static int VEHICLE_UNUTTENDED_LABEL = 383;
	public static int RETORQE_LABEL = 382;
	public static int NO_SIGNATURE_REASION = 384;
	public static int NO_ODOMETER_REASION = 197;
	public static int DRIVE = 261;
	public static int LIFT = 262;
	public static int PASSIVE = 263;
	public static int STEERS = 264;
	public static int STEERnDRIVE = 265;

	public static int SIGNATURE_BOX_TITLE_LBL = 380;
	public static int VEHICLE_SUMMARY_TITLE = 440;

	public static int PRIMARY_ODO_LABEL = 37;
	public static int SECONDARY_ODO_LABEL = 38;
	public static int GPS_LOCATION_ODO_LABEL = 48;
	public static int TECHNICIAN_LABEL = 32;
	public static int CALL_CENTER_REF_NUMBER_LABEL = 39;
	public static int DRIVER_NAME_LABEL = 40;
	public static int DEFECT_NUMBER_LABEL = 41;
	public static int CALLER_NAME_LABEL = 42;
	public static int CALLER_PHONE_NUMBER_LABEL = 43;

	public static int TIME_STARTED_LABEL = 45;
	public static int TIME_RECIEVED_LABEL = 44;
	public static int TIME_ON_SITE_LABEL = 46;
	public static int TIME_FINISHED_LABEL = 47;
	public static int LOCATION_LABEL = 49;
	public static int STOCK_LOCATION_LABEL = 337;
	public static int GPS_ERROR_MESSAGE = 50;

	public static int LOCATION_REQUIRED_MESSAGE = 53;
	public static int TIME_RECEIVED_SHOULD_BE_EARLIER_THEN_STARTED = 54;
	public static int TIME_STARTED_SHOULD_BE_EARLIER_THEN_ON_SITE = 55;
	public static int TIME_ON_SITE_SHOULD_BE_EARLIER_THEN_FINISHED = 56;

	public static int PRESSURE_LABEL = 306;
	public static int BEFORE_LABEL = 307;
	public static int AFTER_LABEL = 308;
	public static int NSK1_LABEL = 303;
	public static int NSK_LABEL = 322;

	public static int NSK2_LABEL = 304;
	public static int NSK3_LABEL = 305;
	public static int REGROOVE_TYPE_LABEL = 128;
	public static int DIMENSION_LABEL = 65;
	public static int ASP_LABEL = 66;
	public static int BRAND_LABEL = 64;
	public static int RIM_LABEL = 67;
	public static int DESIGN_LABEL = 68;
	public static int SIZE_LABEL = 321;
	public static int TYPE_LABEL = 137;
	public static int REGROOVE_TIRE_LABEL = 177;// SWAP_OPERATION_MSG
	public static int SWAP_OPERATION_MSG = 158;
	public static int SERIAL_CONFIRMATION_LABEL = 483;

	public static final int NSKONE_BEFORE_IS_REQUIRED_MESSAGE = 208;
	public static final int NSKTWO_BEFORE_IS_REQUIRED_MESSAGE = 209;
	public static final int NSKTHREE_BEFORE_IS_REQUIRED_MESSAGE = 210;
	public static final int NSKONE_AFTER_IS_REQUIRED_MESSAGE = 211;
	public static final int NSKTWO_AFTER_IS_REQUIRED_MESSAGE = 212;
	public static final int NSKTHREE_AFTER_IS_REQUIRED_MESSAGE = 213;
	public static final int PRESSURE_REQUIRED = 224;

	public static final int NSKONE_CANNOT_BE_LESS_THEN_BEFORE = 220;
	public static final int NSKTWO_CANNOT_BE_LESS_THEN_BEFORE = 221;
	public static final int NSKTHREE_CANNOT_BE_LESS_THEN_BEFORE = 222;

	// New Translation Record ID
	public static final int SELECT_CUSTOMER_NAME = 403;
	public static final int TIRE_MANAGEMENT_LABEL = 424;
	public static final int INCOMPLETE_JOBS_LABEL = 443;
	public static final int COMPLETED_JOBS_LABEL = 438;
	public static final int DELETED_JOBS_LABEL = 439;
	public static final int SIZE_NOT_ALLOWED = 344;

	// siganture form
	public static String EjobSignatureBoxNotes;
	public static final int SIGN_SIGNATURE_FORM = 233;
	public static final int SEL_REASON_NO_ODO = 197;
	public static final int FINSIH_TIME_AFTER_START_TIME = 56;
	public static final int JOB_IS_EMPTY = 423;
	public static final int DUPLICATE_SERIAL_LABEL = 97;

	// dialog messages
	public static String CONFIRM_SAVE = "Would you like to save changes?";
	public static String COMPLETE_JOB_WITH_PARTIAL_INSPECTION_ALERT = "Are you sure you want to complete this job without all tyres inspected?";
	public static final int CONFIRM_BACK = 241;
	public static String ERROR_NETWORK_SYNC_ERROR = "Please, Check your Internet Connection and start the synchronisation again";
	public static String ERROR_SERVER_DOWN_ERROR = "We cannot connect you right now, please check your data connections and settings";
	public static String ERROR_DB_DELETED = "Snapshot DB files have been deleted";
	public static final String ERROR_NETWORK_SYNC_ERROR_RELOGIN = "Please, Check your Internet Connection and start the synchronisation again";
	public static final String JOB_ALREADY_ADDED = "Please be careful, you already have a job for the same vehicle. Work on the same positions will lead to errors.";
	public static final int JOB_HAS_BEEN_DELETED = 430;
	// launch new ejob hard coded
	public static CharSequence ACCOUNTNAME = "W.M MORRISION.C";
	public static CharSequence VEHID = "T4848.C";
	public static CharSequence FLEETNUM = "T4848.C";
	public static CharSequence JOBREFNUMBER = "-1";
	public static String vehicleConfiguration;
	public static boolean DIFFERENT_SIZE_ALLOWED;
	public static String tireJson;
	public static final int EJOB_FORM_ACTIVITY_NAME = 364;
	public static final int GPS_NOT_AVAILABLE = 50;

	public static boolean _AUTOSYNC_FLAG = false;
	public static String _USER_AUTOFILL = "AA15882";

	// sync files
	public static String COMPRESSED_FILEPATH = "/temp/snapshotcompressed.db";
	public static String PACKFILE_PATH = "/pack.pack";
	public static String RESPONSEFILE_PATH = "/temp/response.txt";

	public static final String UPDATESYNCFILENAME = "syncupdate.sql";
	public static final String UPDATEUPLOADFILENAME = "UpdateTables.sql";
	public static final String UPDATE_TRANSLATION_FILENAME = "transaltion.sql";

	public static int JOB_RESULT_LISTSIZE = 0;
	public static int JOBREFNUMBERVALUE = 0;
	public static int JobRefNumberForPartialInspectionCheck = -1;
	public static boolean GETJOBREFFNO = true;

	public static float FORSWAP_BARVALUE;
	public static int RETORQUEVALUE_SAVED;

	public static boolean _CHECKFORLOGINAUTOFILL = false;
	public static boolean RELOAD_REFF = false;
	public static boolean RELOAD_SCREEN = false;
	public static boolean REGROOVETYPE = false;
	public static String _SAPJSONVAl;
	public static String SAVEDJOBREFNO;
	public static ArrayList<String> _mBundleActionTypeVec = new ArrayList<String>();
	public static ArrayList<String> _mBundleOperationIDVec = new ArrayList<String>();
	public static ArrayList<String> _repairCompVec = new ArrayList<String>();
	public static ArrayList<String> _workOrderVec = new ArrayList<String>();
	public static ArrayList<String> _mBundleCurrentstatusVec = new ArrayList<String>();
	public static ArrayList<String> _tireIDVec = new ArrayList<String>();

	public static ArrayList<String> mBundleSerielNOVec = new ArrayList<String>();
	public static ArrayList<String> mBundleDescOperationCodeVec = new ArrayList<String>();
	public static ArrayList<String> mBundleOperationCodeVec = new ArrayList<String>();
	public static ArrayList<String> mBundleFinalTyreStatusVec = new ArrayList<String>();
	public static ArrayList<Integer> _tireIDVec_Pos = new ArrayList<Integer>();
	public static ArrayList<String> SELECTED_DAMAGE_TYPE_LIST = new ArrayList<String>();
	public static ArrayList<String> SELECTED_DAMAGE_AREA_LSIT = new ArrayList<String>();// FROM_TM_OPERATION
	public static boolean FROM_TM_OPERATION = false;

	public static String jsonContractVal;

	public static String jsonSAPContractVal;

	public static String mapLanguageToSettings(String language) {

		switch (language) {

		case Constants.ENGLISH_SHORT:
			return Constants.ENGLISH;

		case Constants.FRENCH_SHORT:
			return Constants.FRENCH;

		case Constants.GERMAN_SHORT:
			return Constants.GERMAN;

		case Constants.POLISH_SHORT:
			return Constants.POLISH;

		case Constants.NETHERLANDS_SHORT:
			return Constants.NETHERLANDS;

		case Constants.ITALY_SHORT:
			return Constants.ITALY;

		case Constants.SPANISH_SHORT:
			return Constants.SPANISH;

		case Constants.PORTUGUESE_SHORT:
			return Constants.PORTUGUESE;

		case Constants.GREEK_SHORT:
			return Constants.GREEK;

		case Constants.TURKISH_SHORT:
			return Constants.TURKISH;

		case Constants.RUSSIAN_SHORT:
			return Constants.RUSSIAN;
		case Constants.AUSTRIAN_SHORT:
			return Constants.AUSTRIAN;
		case Constants.BELGIAN_SHORT:
			return Constants.BELGIAN;
		case Constants.BULGARIAN_SHORT:
			return Constants.BULGARIAN;
		case Constants.SWISS_SHORT:
			return Constants.SWISS;
		case Constants.CZECH_SHORT:
			return Constants.CZECH;
		case Constants.DANISH_SHORT:
			return Constants.DANISH;
		case Constants.ESTONIAN_SHORT:
			return Constants.ESTONIAN;
		case Constants.FINISH_SHORT:
			return Constants.FINISH;
		case Constants.CROATIAN_SHORT:
			return Constants.CROATIAN;
		case Constants.HUNGARIAN_SHORT:
			return Constants.HUNGARIAN;
		case Constants.IRISH_SHORT:
			return Constants.IRISH;
		case Constants.LITHUANIAN_SHORT:
			return Constants.LITHUANIAN;
		case Constants.LUXEMBOURG_SHORT:
			return Constants.LUXEMBOURG;
		case Constants.LATVIAN_SHORT:
			return Constants.LATVIAN;
		case Constants.NORWEGIAN_SHORT:
			return Constants.NORWEGIAN;
		case Constants.ROMANIAN_SHORT:
			return Constants.ROMANIAN;
		case Constants.SWEDISH_SHORT:
			return Constants.SWEDISH;
		case Constants.SLOVENIAN_SHORT:
			return Constants.SLOVENIAN;
		case Constants.SLOVAKIAN_SHORT:
			return Constants.SLOVAKIAN;

		default:
			return null;
		}
	}

	public static String STATUSVALUES = "";
	public static boolean COMESFROMUPDATE = false;
	public static boolean COMESFROMVIEW = false;
	public static int GETSELECTEDLISTPOSITION;
	public static String VEHICLEIDFORCONFIG;
	public static boolean EDIT_JOB = false;
	public static boolean NOADDITIONALSERVICES = false;

	// vehicle skeleton
	public static ArrayList<JobItem> JOB_ITEMLIST;
	public static ArrayList<Tyre> TIRE_INFO;
	public static ArrayList<JobCorrection> JOB_CORRECTION_LIST;
	public static ArrayList<TireImageItem> JOB_TIREIMAGE_ITEMLIST;
	public static ArrayList<TorqueSettings> TORQUE_SETTINGS_ARRAY;
	// additional services
	public static ArrayList<ServicesEditHelper> servicesEdit;

	public static ArrayList<JobItem> Bundle_Retained_Jobitem = new ArrayList<JobItem>();

	public static ArrayList<JobItem> getBundle_Retained_Jobitem() {
		return Bundle_Retained_Jobitem;
	}

	public static void setBundle_Retained_Jobitem(
			ArrayList<JobItem> bundle_Retained_Services) {
		Bundle_Retained_Jobitem = bundle_Retained_Services;
	}

	public static Tyre DISMOUNTED_TYRE;

	public static String BRAND_NAME = "";
	public static String sourceTyrePosition = "";
	public static String targetTyrePosition = "";
	public static String NSK1_SWAP = "";
	public static String NSK2_SWAP = "";
	public static String NSK3_SWAP = "";
	public static String CURRENT_SERIAL_NUMBER = "";
	public static String EDITED_SERIAL_NUMBER = "";// DES_PRESSURE_CAPTURED
	public static String SOURCE_PRESSURE_CAPTURED = "";
	public static String DES_PRESSURE_CAPTURED = "";
	public static Tyre CURRENT_TYRE;
	public static String EDITED_SERIAL_NUMBER_FIRST_SELECTED = "";
	public static String EDITED_SERIAL_NUMBER_SECOND_TYRE = "";
	public static String EDITED_SERIAL_NUMBER_SOURCE = "";
	public static String EDITED_SERIAL_NUMBER_DES = "";
	public static Tyre SELECTED_TYRE;
	public static Tyre SECOND_SELECTED_TYRE;
	public static Tyre THIRD_SELECTED_TYRE;
	public static Tyre FOURTH_SELECTED_TYRE;
	/**
	 * boolean for staring swap drag
	 */
	public static boolean ONDRAG = false;
	/**
	 * boolean for staring swap from menu (not drag)
	 */
	public static boolean ONPRESS = false;
	public static boolean DOUBLESWAP = false;
	public static boolean TRIPLESWAP = false;
	public static Tyre SOURCE_TYRE;
	public static Tyre DES_TYRE;
	public static String SELECTED_TYRE_SerialNumber = "";
	public static String SELECTED_TYRE_setNsk = "";
	public static String SELECTED_TYRE_setNsk2 = "";
	public static String SELECTED_TYRE_setNsk3 = "";
	public static String SELECTED_TYRE_setVehicleJob = "";
	public static String SELECTED_TYRE_setRimType = "";
	public static String SELECTED_TYRE_setBrandName = "";
	public static String SELECTED_TYRE_setSize = "";
	public static String SELECTED_TYRE_setAsp = "";
	public static String SELECTED_TYRE_setRim = "";
	public static String SELECTED_TYRE_setDesign = "";
	public static String SELECTED_TYRE_setDesignDetails = "";
	public static Float SELECTED_TYRE_setAxleRecommendedPressure = null;
	/**
	 * used to identify physical for first tire is going on. works for physical
	 * swap from menu select (not for drag)
	 */
	public static boolean onReturnBool = false;
	/**
	 * used to identify physical for second tire is going on. works for physical
	 * swap from menu select (not for drag)
	 */
	public static boolean onGOBool = false;
	public static boolean onTouchBool = false;
	public static boolean onSwapOptionsBool = false;
	public static boolean onGOBoolLogical = false;
	public static boolean onBrandBool = false;
	public static boolean onBrandBoolForJOC = false;
	/**
	 * true for drag on physical swap
	 */
	public static boolean onDragPSBool = false;
	/**
	 * if brand is blank or null for selected tire
	 */
	public static boolean onBrandBlank = false;// For brand
	public static boolean onBrandDesignBlank = false;// For brand
	// public static boolean onBrandDesignBool = false;
	/**
	 * true when physical swap is on for second tire used to cancel physical
	 * swap to identify that we are on second tire
	 */
	public static boolean onDRAGReturnBoolPS = false;
	public static boolean onDragBool_LS = false;// onBrandDesignBlank
	public static boolean DO_PHYSICAL_SWAP = false;// Constants.TRIGGER_LOGICAL
	public static boolean CHECK_LOGICAL_SWAP = false;
	public static int triggerMultipleEdit = 0;
	public static int triggerMultipleSwap = 0;
	public static int checkSingleSwap = 0;
	public static boolean ONLONG_PRESS = false;
	public static String SAPCONTRACTNUMBER;
	public static String SAP_CONTRACTNUMBER_ID;
	public static String NEW_SWAP_POSITION = "";
	public static boolean TRIGGER_LOGICAL = false;
	public static int CHECK_LOGICAL_DONE = -1; // if >=1 then done
	public static boolean IS_DES_LOGICAL_SWAP_DONE = false; // if true then done
	/**
	 * 1 if job item for swap inserted, else 0 used to check if swap item
	 * inserted
	 */
	public static int INSERT_SWAP_DATA = 0;
	public static boolean DUPLICATE_SERIAL_ENTRY = false;
	public static boolean SECOND_TYRE_ACTIVE = false;

	public static String firstTireAXLE;
	public static String seccondTireAXLE;
	public static String AXLENO = "begin";
	public static String SAP_VENDORCODE_ID;
	public static String VENDORCOUNTRY_ID;
	public static String VENDORSAPCODE;
	public static boolean UPDATESTATUSWHILESWIPE = false;
	public static int JOBREFNUMPASSFORNEWJOB;
	public static int JOBREFNUMPASSFROMEJOB;
	public static Contract contract;
	public static ArrayList<Integer> torqueSettingsIDS;
	public static String VENDER_NAME;
	public static String TYRE_ID;;
	public static boolean ONSTATE_TOR = false;
	public static String STATUSONSELECTEDLIST;
	public static boolean UPDATE_SERIAL_DB = false;
	public static String CURRENTDATE;
	public static String CURRENTIME;
	public static boolean EJOBHEADERVALUESIFDBNULL = true;
	public static boolean UPDATE_APPLIED_ON_EDIT = false;

	public static boolean FROM_EJOBLIST = false;

	public static String jobIdEdit;
	public static boolean CHEKIFPICTAKENORNOT = false;
	public static boolean DELETEICONCHANGE = false;
	public static int DELETEDPOSITION;
	public static boolean GPS_LOCK = false;
	public static boolean TIME_LOCATION_SET = false;
	public static boolean ALERTFORNONETWORKWHILESYNC = false;

	// Amit SERIAL_NUMBER_TM
	public static ArrayList<HashMap<String, String>> Search_TM_ListItems = new ArrayList<HashMap<String, String>>();
	public static String SAP_CUSTOMER_ID;
	public static ArrayList<Integer> REGROOVE_TYPE_TM = new ArrayList<Integer>();
	public static ArrayList<Double> THREAD_DEPTH_TM = new ArrayList<Double>();
	public static int TM_CAMERA_ICON_CLICKED = 0;
	public static String SERIAL_NUMBER_TM = "";
	public static String CURRENT_STATUS_TM = "";
	public static boolean IsREGROOVED_TM = false;
	public static boolean TM_CAMERA_ICON_CLICKED_FLAG = false;
	public static WeakHashMap<Bitmap, Integer> bitmapQualityMap = new WeakHashMap<>();
	public static ArrayList<Bitmap> damageImage_TM_LIST_Bitmap1 = new ArrayList<Bitmap>();
	public static ArrayList<Bitmap> damageImage_TM_LIST_Bitmap2 = new ArrayList<Bitmap>();
	public static ArrayList<Bitmap> damageImage_TM_LIST_Bitmap3 = new ArrayList<Bitmap>();

	public static ArrayList<String> damageNote_TM_LIST1 = new ArrayList<String>();
	public static ArrayList<String> damageNote_TM_LIST2 = new ArrayList<String>();
	public static ArrayList<String> damageNote_TM_LIST3 = new ArrayList<String>();
	
	/**
	 * Bitmap photo 1
	 */
	public static Bitmap sBitmapPhoto1;
	/**
	 * Bitmap photo 2
	 */
	public static Bitmap sBitmapPhoto2;
	/**
	 * Bitmap photo 3
	 */
	public static Bitmap sBitmapPhoto3;
	/**
	 * Bitmap photo 1 image quality value
	 */
	public static int sBitmapPhoto1Quality;
	/**
	 * Bitmap photo 2 image quality value
	 */
	public static int sBitmapPhoto2Quality;
	/**
	 * Bitmap photo 3 image quality value
	 */
	public static int sBitmapPhoto3Quality;

	public static String NSK_SET_WOT = "";
	public static String PRESSURE_SET_WOT = "";

	public static boolean CLICKED_SERIAL_CONFIRM = false;
	/**
	 * flat to know logical swap going on.
	 */
	public static boolean DO_LOGICAL_SWAP = false;

	public static ArrayList<Integer> retorqueUpdateID;

	public static String FULLDESIGN_SELECTED = "";
	public static String NEW_SAPMATERIAL_CODE = "";// UNDO_STATE
	public static String SAP_CUSTOMER_ID_JOB = "";

	public static boolean UNDO_STATE = false;
	public static boolean ASCDESC = true;
	public static boolean CAMIMAGEENABLE = false;
	public static int SELECTEDGROUPPOSITION;
	public static boolean CAMERAIMAGECLICKENABLE = false;
	public static boolean SINGLESWIPETODELETEEJOBLIST = false;
	public static int SINGLESWIPETODELETEEJOBLISTPOS;
	public static boolean SINGLESWIPETODELETETM = false;
	public static boolean SINGLESWIPETODELETENEWJOB = false;
	public static boolean SINGLESWIPETODELETEBACKBUTT = false;
	public static boolean SAVEEXTDBWHENAPPSAVE = true;
	public static boolean TM_NOT_ALLOWED = false;

	public static Bitmap RETAIN_BITMAP = null;// ONSTATE_REGROOVE
	public static boolean ONSTATE_REGROOVE = false;

	// Bluetooth Max limit for connection
	public static final int RETRY_MAXLIMIT = 5;
	public static boolean COMESFROMDELETE;
	public static int mbutton_layout1State;
	public static int mbutton_layout2State;
	public static int mbutton_layout3State;

	// eJobList
	public static final String COMPLETED_JOB_STATUS = "0";
	public static final String COMPLETED_CHECKED_JOB_STATUS = "2";
	public static final String INCOMPLETED_JOB_STATUS = "1";
	public static final String TM_JOB_STATUS = "5";
	public static final String DELETED_JOB_STATUS = "4";
	public static final String COMPLETED_CHECKED_FOR_SYNC_JOB_STATUS = "6";
	public static final int LICENCE_PLATE_FROM_DB = 294;
	public static final int CUST_NAME_FROM_DB = 403;
	public static final int DATE_FROM_DB = 297;

	public static String IMAGES_PATH = Constants.PROJECT_PATH + "/images";
	public static final int MIN_LENGTH_SERIALNUMBER = 5;
	public static boolean LOGICAL_SWAP_INITIAL = false;
	public static String tempTyreSerial = "";
	public static String tempTyrePosition = ""; // OnSerialBool
	public static String tempTyrePositionNew = "";
	public static boolean onBrandDesignBool = false;
	public static boolean OnSerialBool = false;

	public static boolean IS_FORM_ELEMENTS_CLEANED = false;

	public static String sLblCheckPressureValue = "Please Check pressure value";

	public static final double PSI_MIN_VALUE = 14.5037738;
	/*Bug 728 : changed max PSI, BAR to 150 and 10.34 respectively */
	public static final double PSI_MAX_VALUE = 150;
	public static final double BAR_MIN_VALUE = 1;
	public static final double BAR_MAX_VALUE = 10.34;
	public static boolean IS_SIGANTURE_DONE = false;
	public static boolean CheckStatus = false;

	public static final int ALLREADY_ADDED = 431;

	public static final int SETTING_SAVED = 434;
	public static final int APPLICATIONDB = 456;
	public static final int ALREADYADDEDVEHICLE = 455;
	public static final int BLUETOOTH_CONNECTION_FAILED = 457;

	public static final int CHOOSEANOTHEROPERATION = 458;
	public static final int CHOOSEANOTHERTYRE = 459;
	public static final int CHOOSEDIFFERENTTYRETOSWAP = 460;
	public static final int DataDownloadingFailed = 464;
	public static final int Jobsaved = 466;
	public static final int NoDatatoSave = 468;
	public static final int SerialNumber = 473;
	public static final int TaskDone = 476;
	public static boolean BLUETOOTH_CONNECTED = false;
	
	
	public static final int MOUNT_PWT_TIRE_LABEL = 129;
	// camera activity
	public static final int TD_MAX_LABEL = 478;
	public static final int TD_MIN_LABEL = 479;
	public static final int NSK_WRONG_LABEL = 245;
	public static final int BREAK_SPOT_INFO_LABEL = 484;
	public static final int PHOTO_LABEL = 486;
	public static final int NOTE_LABEL = 70;
	public static final int TAP_TO_TAKE_PICTURE_LABEL = 485;
	public static final int DAMAGE_AREA_LABEL = 317;
	public static final int DAMAGE_GROUP_LABEL = 183;
	
	//Newly Added Labels
	public static final int SYNC_LABEL = 487;
	public static final int HELP_LABEL = 488;
	public static final int ABOUT_LABEL = 489;
	public static final int LOGOUT_LABEL = 490;
	public static final int LOGOUT_CONFIRMATION_LABEL = 491;
	public static final int DEFINE_TIRE_LABEL = 492;
	public static final int TIRE_STATUS_LABEL = 493;
	public static final int FILL_ALL_ENTRY_LABEL = 494;
	public static final int SAVE_CHANGE_CONFIRMATION_LABEL = 495;
	public static final int ENTER_MANDATORY_FIELD_LABEL = 496;
	public static final int SAVE_YOUR_NOTE_LABEL = 497;
	public static final int CHECK_PRESSURE_LABEL = 500;
	public static final int INVALID_NSK_LABEL = 501;
	public static final int SIGNATURE_FORM_SAVED = 502;
	public static final int CANCEL_NOTE_CONFIRMATION_LABEL = 504;
	public static final int CHECK_INTERNET_CONNECTION_LABEL = 505;
	public static final int CANNOT_CONNECT_LABEL = 506;
	public static final int DATABASE_FILE_DELETED_LABEL = 507;
	public static final int INVALID_RETORQUE__MESSAGE_LABEL = 503;
	public static final int SWAP_FINAL_CONFIRMATION_LABEL = 156;
	public static final int TOR_FINAL_CONFIRMATION_LABEL = 160;
	
	/** Bug :: 559
	 * Variable holding always allowed serial number 
	 */
	public static final String SERIAL_NUMBER_ALLLOWED_NA = "NA";
	public static final String SERIAL_NUMBER_ALLLOWED_NA_FOR_SAP = "N/A";
	public static final String SERIAL_NUMBER_ALLLOWED = "NR";
	public static final String SERIAL_NUMBER_ALLLOWED_FOR_SAP = "N/R";
	public static final String DOTNET_STRING_PLACE_HOLDER_REGEX = "\\{\\d+\\}";
	public static final String JAVA_STRING_PLACE_HOLDER = "%s";
	/**
	 * ArrayList containing all checked reference numbers
	 */
    public static ArrayList<String> CHECKED_JOB_LIST;
    /**
	 * CR :: 469
	 * Variable holding full details 
	 * related to the application installed and the device details
	 */
	public static String FULL_DEVICE_DETAILS = "";
	/**
	 * CR :: 455
	 * Variable holding the threshold NSK level for the session
	 */
	public static String THRESHOLD_TD_FOR_CAMERA = "0";
	/**
	 * CR :: 455
	 * Variable holding the status for photo capture on entered NSK
	 */
	public static boolean PHOTO_CAPTURE_ON_MIN_TD = false;
	public static boolean FINAL_SAVE_STATE = false;
	public static int JOB_CORRECTION_COUNT_SWAP = 0;
	public static final String EJOB_DOWNLOAD_URL = "https://play.google.com/store/apps/details?id=com.goodyear.ejob";
	public static final String EJOB_GET_VERSION_URL = "https://androidquery.appspot.com/api/market?app=com.goodyear.ejob";
	public static String INCOMPLETE_SWAP_ERROR_MESSAGE = "Please complete your operation";

	/**
	 * CR 505
	 * List containing the used Part worn tires 
	 */
	public static ArrayList<Tyre> mPWTList = new ArrayList<Tyre>();
	public static ArrayList<String> PWTTyreList;
	/**
	 * Final Key string for PWT Tires
	 */
	public static final String TYRES_PWT = "TYRES";

	/**
	 * Inspection 447
	 * List Containing Visual Remarks
	 */
	public static ArrayList<VisualRemark> mGlobalVisualRemarks;

	public static String PARTIALLY_INSPECTYED;
	public static boolean HAS_INSPECTED_TIRE = false;
	public static String PARTIALLY_INSPECTED_WITH_OPERATION;
	//Setting Default Value as 99.99 for pressure
	public static String DEFAULT_INSPECTION_PRESSURE_VALUE="99.99";

	//DB Upgrade initiation
	public static int DB_UPGRADE_OLD_VER=6;
	public static boolean IS_TYRE_INSPECTED = false;

	//CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
	public static Long LastRecordedOdometerValue;
	public static int IsOdometerValueIsCorrect = 0;

	//CR - 550
	public static String SwapType = "SwapType";
	public static int SwapTypeConstant=0;


	//constants for force sync
	public static boolean APP_UPGRADE_STATUS = false;
	public static boolean Force_Download_Sync_STATUS = false;
	public static boolean IsDBExistBeforeLogin = false;
	public static Activity mainActivity;
	public static boolean IS_SHOWING_DIALOG = false;
	public static final String OLD_DB_VERSION_NUMBER = "OLD_DB_VERSION_NUMBER";
	public static final String OLD_APP_VERSION_NUMBER = "OLD_APP_VERSION_NUMBER";
	public static final String Initial_APP_VERSION_NUMBER = "0.0.0.0";

	public static ArrayList<String> TyreIdListToHandlerBackPress;
	public static int finalResponseFromServer;

	//Inspection Labels
	public static final int LABEL_INSPECTION = 508;
	public static final int LABEL_RECTIFIED_PRESSURE = 509;
	public static final int LABEL_TEMPERATURE = 510;
	public static final int LABEL_ADJUST = 511;
	public static final int LABEL_RECTIFIED_PRESSURE_REQ = 512;
	public static final int LABEL_TEMPERATURE_REQ = 513;
	public static final int LABEL_ABOUT_TO_INSPECT = 514;
	public static final int LABEL_SELECT_TYRE = 515;
	public static final int LABEL_SELECT_DESCRIPTION = 516;
	public static final int LABEL_SELECT_DO = 517;
	public static final int LABEL_OBSERVATION = 518;
	public static final int LABEL_VISUAL_REMARK = 519;
	public static final int LABEL_DESC = 520;
	public static final int LABEL_DO = 521;
	public static final int LABEL_VISUAL_REMARK_ADDED = 522;
	public static final int LABEL_VISUAL_REMARK_REMOVED = 523;
	public static final int LABEL_VISUAL_REMARK_SAVED = 524;
	public static final int LABEL_DELETE_THE_SELECTED_VISUAL_REMARK = 525;
	public static final int LABEL_LIKE_TO_SAVE_VISUAL_REMARK = 526;
	public static final int LABEL_WANT_TO_COMPLETE_JOB_WITHOUT_ALL_TYRES_INSPECTED = 527;
	public static final int LABEL_PARTIAL_INSPECTION_NOTE = 528;
	public static final int LABEL_RECORDED_PRESSURE = 529;
	public static final int LABEL_CORRECTED_PRESSURE = 530;
	public static final int LABEL_TYRE_INSPECTED = 531;
	public static final int LABEL_TYRE_HOT = 532;
	public static final int LABEL_TYRE_COLD = 533;
	/*Adding this message for Bug 729, restricting swap on dismounted tyre*/
	public static final int NO_SWAP_ON_DISMOUNTED_TYRE = 534;

	//DB BAckUp
	public static final Long DBBackUpLimit = 40000000L;
	public static final String DBBackUp_DIR = "/DBBackUp/";

	/*File path to copy DB backup during auto logout*/
	public static final String FULLDBBackUp_DIR = "/FullDBBackUp/";

	//Logs
	public static final Long LogSizeLimit = 60000000L;

	/* Bug 665,JIRA EJOB-42 : This message is prompted to the user when trying to open a blacklisted vehicle*/
	public static final int BLACKLISTED_VEHICLE_MESSAGE = 535;
	public static final int BLACKLISTED_VEHICLE_LICENCE = LICENCE_PLATE_FROM_DB;
	public static final int BLACKLISTED_VEHICLE_SAP_VEHICLE_CODE = 536;

	public static final String AUTO_LOGOUT_PREFERENCE = "Goodyear_autologout_preference";
	public static final String AUTO_LOGOUT_KEY="goodyear_auto_logout";

	public static final String FIRST_TIME_LOGIN_PREFERENCE = "Goodyear_firsttime_preference";
	public static final String FIRST_TIME_LOGIN_KEY="goodyear_firsttime_login";

    public static final int Label_Photo_at_min_TD_Removal = 539;
	public static final int Label_Are_the_torque_setting_same = 543;
	public static final int Label_Summary_Mount_new = 545;
	public static final int Label_Summary_Dismount = 544;
	public static final int Label_Summary_Logical_swap = 548;
	public static final int Label_Summary_Physical_swap = 549;
	public static final int Label_High_pressure_valve_cap_fitted=247;
	public static final int Label_Valve_fixing_support=249;
	public static final int Label_Valve_fitted=246;
	public static final int Label_Dynamic_balancing = 250;
	public static final int Label_Static_balancing = 251;
	public static final int Label_Rigid_valve_extension_fitted = 252;
	public static final int Label_Flexible_valve_extension_fitted =253;
	public static final int Label_Puncture_repair = 254;
	public static final int Label_Tyre_fill = 255;
	public static final int Label_Minor_repair = 256;
	public static final int Label_Major_repair = 257;
	public static final int Label_Tyre_repair_and_reinforcement =258;
	public static final int Label_Brake_spot_repair = 259;
	public static final int Label_Major_repair_with_vulcanisation =260;
	public static final int Label_Thresold_TD = 540;
	public static final int Label_Wrong_NSK =541;
	public static final int Label_INCOMPLETE_SWAP_ERROR_MESSAGE=547;
	public static final int Label_Enter_Value = 546;
	public static final int Label_Wrong_OdoMeter_Value =556;
	public static final int Label_Last_Recorded_Odometer_Reading = 557;

	/*#ForHalosys*/
	public static final String SET_OFFLINE_PIN = "Please enter 6 digit offline pin for future login";
	public static final String SET_PIN_SUCCESSFUL = "Pin is set successfully";

	public static final String FIRST_LOGIN = "first_login";

}
