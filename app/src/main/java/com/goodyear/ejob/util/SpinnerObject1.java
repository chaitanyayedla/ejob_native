package com.goodyear.ejob.util;

/**
 * @author munna.kumar
 * 
 */
public class SpinnerObject1 {

	private String databaseId;
	private String databaseValue;

	public SpinnerObject1(String databaseId, String databaseValue) {
		this.databaseId = databaseId;
		this.databaseValue = databaseValue;
	}

	public String getId() {
		return databaseId;
	}

	public String getValue() {
		return databaseValue;
	}

	@Override
	public String toString() {
		return databaseValue;
	}

}
// In order to retrieve the id of the currently selected item, you do this :
// int databaseId = Integer.parseInt ( ( (SpinnerObject) spin2.getSelectedItem
// () ).getId () )
