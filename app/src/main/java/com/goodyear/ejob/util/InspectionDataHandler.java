package com.goodyear.ejob.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.job.operation.helpers.JobItem;

import java.util.UUID;

/**
 * Created by amitkumar.h on 28-10-2015.
 */
public class InspectionDataHandler {
    private Context mContext;
    private DatabaseAdapter mDbHelper;
    private static InspectionDataHandler sDialogUtils;
    private String TAG="InspectionDataHandler";
    //User Trace logs trace tag
    private static final String TRACE_TAG = "Auto Inspection";

    private InspectionDataHandler(Context context) {
        mContext = context;
    }
    /**
     * initializing the instance of AutoSwapImp Class
     * @param context
     * @return
     */
    public static InspectionDataHandler getMyInstance(Context context) {
        if (sDialogUtils == null) {
            sDialogUtils = new InspectionDataHandler(context);
        }
        return sDialogUtils;
    }
    public void updateJobitemForInspection(Tyre mInspectedTyre,Boolean canUpdateTyreIdToHandlerBackPress){
        /**
         * Updating Data for Inspection along with other tire operation
         */

        SharedPreferences preferences =mContext.getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);


        //447:: Inspection :: True if Inspection button is ON in Settings
        if (!TextUtils.isEmpty(preferences.getString(Constants.INSPECTION, ""))) {
            Constants.INSPECTION_ENABLE = (preferences.getString(Constants.INSPECTION, ""));
            if (Constants.INSPECTION_ENABLE.equalsIgnoreCase("ON")) {
                try {
                    int len = VehicleSkeletonFragment.mJobItemList.size();
                    com.goodyear.ejob.job.operation.helpers.JobItem jobItem = new com.goodyear.ejob.job.operation.helpers.JobItem();
                    int jobID = getJobItemIDForThisJobItem();
                    jobItem.setActionType("26");
                    jobItem.setAxleNumber("0");
                    jobItem.setAxleServiceType("0");
                    jobItem.setCasingRouteCode("");
                    jobItem.setDamageId("");
                    jobItem.setExternalId(String.valueOf(UUID.randomUUID()));
                    jobItem.setGrooveNumber(null);
                    jobItem.setJobId(String.valueOf(jobID));
                    jobItem.setMinNSK(mInspectedTyre.getMinNSK());
                    jobItem.setNote("");
                    jobItem.setNskOneAfter(mInspectedTyre.getNsk());
                    jobItem.setNskOneBefore("0");
                    jobItem.setNskThreeAfter(mInspectedTyre
                            .getNsk3());
                    jobItem.setNskThreeBefore("0");
                    jobItem.setNskTwoAfter(mInspectedTyre
                            .getNsk2());
                    jobItem.setNskTwoBefore("0");
                    jobItem.setOperationID("");
                    jobItem.setRecInflactionOrignal(mInspectedTyre
                            .getPressure());
                    jobItem.setRegrooved("False");
                    jobItem.setReGrooveNumber(null);
                    jobItem.setRegrooveType("0");
                    jobItem.setRemovalReasonId("");
                    jobItem.setRepairCompany(null);
                    jobItem.setRimType("0");
                    jobItem.setSapCodeTilD("0");
                    jobItem.setSequence(String.valueOf(len + 1));
                    jobItem.setServiceCount("0");
                    jobItem.setServiceID("0");
                    jobItem.setSwapType("0");
                    jobItem.setThreaddepth("0");
                    jobItem.setTyreID(mInspectedTyre.getExternalID());
                    jobItem.setWorkOrderNumber(null);
                    jobItem.setSwapOriginalPosition(null);
                    jobItem.setPressure(String
                            .valueOf(Constants.FORSWAP_BARVALUE));
                    jobItem.setTorqueSettings(String
                            .valueOf(Constants.RETORQUEVALUE_SAVED));
                    jobItem.setRecInflactionDestination(String
                            .valueOf(Constants.FORSWAP_BARVALUE));
                    jobItem.setSerialNumber(mInspectedTyre.getSerialNumber());
                    jobItem.setBrandName(mInspectedTyre.getBrandName());
                    jobItem.setFullDesignDetails(mInspectedTyre
                            .getDesignDetails());
                    //setting "1" if inspection is done
                    jobItem.setInspectedWithOperation("1");

                    if(jobItem!=null) {
                        LogUtil.DBLog(TAG, "update Jobitem For Inspection", "JobItem - " + jobItem.toString());
                        LogUtil.TraceInfo(TRACE_TAG,null,"Added Inspection",true,false,false);
                    }

                    VehicleSkeletonFragment.mJobItemList.add(jobItem);
                    mInspectedTyre.setIsInspected(true);

                    if(canUpdateTyreIdToHandlerBackPress)
                    {
                        Constants.TyreIdListToHandlerBackPress.add(mInspectedTyre.getExternalID());
                    }
            }catch (Exception e) {
                    LogUtil.DBLog(TAG, "update Jobitem For Inspection", "Exception - "+e.getMessage());
                    e.printStackTrace();
                }
        }

        }
    }
    /**
     * @return
     */
    private int getJobItemIDForThisJobItem() {
        int countJobItemsNotPresent = 0;
        mDbHelper = new DatabaseAdapter(mContext);
        mDbHelper.createDatabase();
        mDbHelper.open();
        int currentJobItemCountInDB = mDbHelper.getJobItemCount();
        Cursor cursor = null;
        try {
            cursor = mDbHelper.getJobItemValues();
            if (!CursorUtils.isValidCursor(cursor)) {
                return countJobItemsNotPresent + 1
                        + VehicleSkeletonFragment.mJobItemList.size();
            }
            for (JobItem jobItem : VehicleSkeletonFragment.mJobItemList) {
                boolean flag = false;
                for (boolean hasItem = cursor.moveToFirst(); hasItem; hasItem = cursor
                        .moveToNext()) {
                    if (jobItem.getExternalId().equals(
                            cursor.getString(cursor
                                    .getColumnIndex("ExternalID")))) {
                        countJobItemsNotPresent++;
                        break;
                    }
                }
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            mDbHelper.close();
            // CursorUtils.closeCursor(cursor);
        }
        return currentJobItemCountInDB - countJobItemsNotPresent
                + VehicleSkeletonFragment.mJobItemList.size() +1;
    }
}
