package com.goodyear.ejob.util;

import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * This is a Utility class that provides different API's for logging. This class
 * is used to turn on/off the logs. <br>
 * The log statements can be disabled by setting the {@link LogUtil#DEBUG} flag
 * as <b>false</b>.
 * 
 */
public class LogUtil {

	/** The Constant flag DEBUG.This flag is used to turn on/off the logs */
	private static final boolean DEBUG = true;
	private static final boolean DEBUG_WRITE_LOGS_TO_FILE = true;
	private static final boolean INFO_LOG_FILE = true;
	public static final String TAG = "GoodYearEjob";

	/**
	 * Log error
	 * 
	 * @param tag
	 *            - The tag to use for this logging event
	 * 
	 * @param msg
	 *            - The message to print out for this logging event
	 */
	public static void e(String tag, String msg) {
		if (DEBUG && msg != null) {
			Log.e(tag, msg);
			if (DEBUG_WRITE_LOGS_TO_FILE) {
				FileOperations.writeFile(getLogFilePath(), tag + " $$ " + msg,
						true);
			}
		}

	}

	/**
	 * Log for error.
	 * 
	 * @param tag
	 *            the tag
	 * 
	 * @param msg
	 *            the message
	 * @param tr
	 *            the Throwable
	 */
	public static void e(String tag, String msg, Throwable tr) {
		if (DEBUG && msg != null) {
			Log.e(tag, msg, tr);
			if (DEBUG_WRITE_LOGS_TO_FILE) {
				FileOperations.writeFile(getLogFilePath(), tag + " $$ " + msg,
						true);
			}
		}
	}

	/**
	 * log for debug.
	 * 
	 * @param tag
	 *            the tag
	 * 
	 * @param msg
	 *            the message
	 * @param tr
	 *            the Throwable
	 */
	public static void d(String tag, String msg, Throwable tr) {
		if (DEBUG && msg != null) {
			LogUtil.d(tag, msg, tr);
			if (DEBUG_WRITE_LOGS_TO_FILE) {
				FileOperations.writeFile(getLogFilePath(), tag + " $$ " + msg,
						true);
			}
		}

	}

	/**
	 * Log debug
	 * 
	 * @param tag
	 *            - The tag to use for this logging event
	 * 
	 * @param msg
	 *            - The message to print out for this logging event
	 */
	public static void d(String tag, String msg) {
		if (DEBUG && msg != null) {
			Log.d(tag, msg);
			if (DEBUG_WRITE_LOGS_TO_FILE) {
				FileOperations.writeFile(getLogFilePath(), tag + " $$ " + msg,
						true);
			}
		}
	}

	/**
	 * Log verbose
	 * 
	 * @param tag
	 *            - The tag to use for this logging event
	 * 
	 * @param msg
	 *            - The message to print out for this logging event
	 */
	public static void v(String tag, String msg) {
		if (DEBUG && msg != null) {
			Log.v(tag, msg);
			if (DEBUG_WRITE_LOGS_TO_FILE) {
				FileOperations.writeFile(getLogFilePath(), tag + " $$ " + msg,
						true);
			}
		}
	}

	/**
	 * Log warning.
	 * 
	 * @param tag
	 *            the tag
	 * @param msg
	 *            the message
	 */
	public static void w(String tag, String msg) {
		if (DEBUG && msg != null) {
			Log.w(tag, msg);
			if (DEBUG_WRITE_LOGS_TO_FILE) {
				FileOperations.writeFile(getLogFilePath(), tag + " $$ " + msg,
						true);
			}
		}
	}

	/**
	 * Log warning.
	 * 
	 * @param tag
	 *            the tag
	 * @param msg
	 *            the message
	 * @param tr
	 *            the Throwable
	 */
	public static void w(String tag, String msg, Throwable tr) {
		if (DEBUG && msg != null) {
			Log.w(tag, msg, tr);
			if (DEBUG_WRITE_LOGS_TO_FILE) {
				FileOperations.writeFile(getLogFilePath(), tag + " $$ " + msg,
						true);
			}
		}
	}

	/**
	 * Log info.
	 * 
	 * @param tag
	 *            the tag
	 * @param msg
	 *            the message
	 */
	public static void i(String tag, String msg) {
		if (DEBUG && msg != null) {
			Log.i(tag, msg);
			if (DEBUG_WRITE_LOGS_TO_FILE || INFO_LOG_FILE) {
				FileOperations.writeFile(getLogFilePath(), tag + " $$ " + msg,
						true);
			}
		}
	}

	/**
	 * Print the stack trace
	 * 
	 * @param e
	 *            - The exception to use for this stack trace
	 * 
	 */
	public static void pst(Throwable e) {
		if (DEBUG) {
			e.printStackTrace();
			if (DEBUG_WRITE_LOGS_TO_FILE) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				FileOperations.writeFile(getLogFilePath(), sw.toString(), true);
			}
		}
	}

	public static void DBLog(String tag, String method, String msg)
	{
		FileOperations.writeDBInfoToFile(getDBLogFilePath(), tag + " $$ " +method+ " $$ " + msg);
	}

	/**
	 * Method to Add trace information to file
	 * @param PageInfo information about the page
	 * @param Type type of the control (widgets or touch input)
	 * @param UserInput user input
	 */
	public static void TraceInfo(String PageInfo, String Type, String UserInput,boolean needPageInfo,boolean needType,boolean line)
	{
		if(needPageInfo) {
			if(needType) {
				FileOperations.writeTraceInfoToFile(getTraceInfoFilePath(), PageInfo + " $ " + Type + " $ " + UserInput, line);
			}
			else
			{
				FileOperations.writeTraceInfoToFile(getTraceInfoFilePath(), PageInfo + " $ "  + UserInput, line);
			}
		}
		else
		{
			if(needType) {
				FileOperations.writeTraceInfoToFile(getTraceInfoFilePath(), Type + " $ " + UserInput, line);
			}
			else
			{
				FileOperations.writeTraceInfoToFile(getTraceInfoFilePath(), UserInput, line);
			}
		}
	}

	private static String getLogFilePath() {
		return Constants.PROJECT_PATH + LogManager.getLogFileName();
	}

	private static String getDBLogFilePath() {
		return Constants.PROJECT_PATH + LogManager.getDBLogFileName();
	}

	/**
	 * Method to get Trace file Location
	 * @return Trace file Location
	 */
	private static String getTraceInfoFilePath() {
		return Constants.PROJECT_PATH + LogManager.getTraceInfoFileName();
	}
}
