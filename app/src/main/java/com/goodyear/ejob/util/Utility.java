package com.goodyear.ejob.util;

import android.content.Context;
import android.database.Cursor;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;

/**
 * Class Used for return particular column or row values
 * 
 * @author johnmiya.s
 * 
 */
public class Utility {

	public static String GetColumnValue(Cursor cur, String ColumnName) {
		try {
			return cur.getString(cur.getColumnIndex(ColumnName));
		} catch (Exception ex) {
			return "";
		}
	}

	public static String GetString(EditText source) {
		try {
			return GetString(source.getText().toString());
		} catch (Exception ex) {
			return "";
		}
	}

	public static String GetString(TextView source) {
		try {
			return GetString(source.getText().toString());
		} catch (Exception ex) {
			return "";
		}
	}

	public static String GetString(Object source) {
		try {
			return GetString(source.toString());
		} catch (Exception ex) {
			return "";
		}
	}

	public static void ShowMessageBox(Context cont, String msg) {
		Toast toast = Toast.makeText(cont, msg, Toast.LENGTH_SHORT);
		// toast.setGravity(Gravity.CENTER, 0, 0);
		toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
		toast.show();
	}

	public static boolean isHostConnectionException(Exception e){
		return true;
//		return (e instanceof UnknownHostException)
//				|| (e instanceof ConnectException)
//				|| (e instanceof ConnectTimeoutException);
	}

	public static  boolean pingGoogle(){
		try {
			HttpGet httpGet = new HttpGet("http://www.google.com");
			HttpParams params = httpGet.getParams();
			params.setParameter(ClientPNames.HANDLE_REDIRECTS, Boolean.FALSE);
			httpGet.setParams(params);
			final HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
			HttpConnectionParams.setSoTimeout(httpParams, 10000);
			return new DefaultHttpClient(httpParams).execute(httpGet).getStatusLine().getStatusCode()==200;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isProxyConnected(Exception e, Context ctx) {
		return false;
//		return Utility.isHostConnectionException(e)
//				&& Validation.isNetworkAvailable(ctx)
//				&& !Utility.pingGoogle();
	}

}
