package com.goodyear.ejob.util;

/**
 * Model to Determine the status of app upgrade
 * Created by giriprasanth.vp on 2/8/2016.
 */
public class ApplicationVersionDifference {
    public static final int NO_UPGRADE = 0;
    public static final int MINOR_UPGRADE = 1;
    public static final int MAJOR_UPGRADE = 2;
    public static final int OTHER = 4;

}
