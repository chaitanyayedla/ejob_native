package com.goodyear.ejob.util;

/**
 * @author shailesh.p
 * 
 */
public class Settings {
	private String login;
	private String language;
	private String lastServerDate;

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the lastServerDate
	 */
	public String getLastServerDate() {
		return lastServerDate;
	}

	/**
	 * @param lastServerDate the lastServerDate to set
	 */
	public void setLastServerDate(String lastServerDate) {
		this.lastServerDate = lastServerDate;
	}

}
