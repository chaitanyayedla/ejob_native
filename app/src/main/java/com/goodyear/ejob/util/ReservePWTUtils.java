package com.goodyear.ejob.util;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.dbmodel.ReservedTyre;

/**
 *
 */
public class ReservePWTUtils {

	public static ArrayList<ReservedTyre> mReserveTyreList = new ArrayList<ReservedTyre>();
	public static ArrayList<ReservedTyre> mReserveTyreListForSelectedTyre = new ArrayList<ReservedTyre>();

	/**
	 * 
	 */
	public static void filterReserveTireListForCurrentTire(Context context,
			String size, String rim, String asp) {
		if (TextUtils.isEmpty(size) || TextUtils.isEmpty(asp)
				|| TextUtils.isEmpty(rim)) {
			return;
		}
		DatabaseAdapter mDbHelper = new DatabaseAdapter(context);
		mDbHelper.open();
		String tireIDs = "";
		try {
			for (ReservedTyre reserved_tire : mReserveTyreList) {
				String tireID = "'" + reserved_tire.getTyreID() + "'";
				tireIDs += tireID + ",";
			}
			if(mReserveTyreListForSelectedTyre==null)
			{
				mReserveTyreListForSelectedTyre = new ArrayList<ReservedTyre>();
			}
			mReserveTyreListForSelectedTyre.clear();
			mReserveTyreListForSelectedTyre.addAll(mReserveTyreList);
			if (tireIDs.length() > 0) {
				tireIDs = tireIDs.substring(0, tireIDs.length() - 1);
				Cursor sizeAspRimCursor = mDbHelper
						.getSizeASPRIMFromTireID(tireIDs);
				if (CursorUtils.isValidCursor(sizeAspRimCursor)) {
					sizeAspRimCursor.moveToFirst();
					do {
						double sizeDB = Double
								.valueOf(sizeAspRimCursor.getString(0));
						double aspDB = Double
								.valueOf(sizeAspRimCursor.getString(1));
						double rimDB = Double
								.valueOf(sizeAspRimCursor.getString(2));
						String currentTireID = sizeAspRimCursor.getString(3);
							if (sizeDB !=  Double.valueOf(size) || aspDB !=  Double.valueOf(asp)
									|| rimDB !=  Double.valueOf(rim)) {
								removeTyreFromList(currentTireID);
						} else {
							continue;
						}
					} while (sizeAspRimCursor.moveToNext());
					CursorUtils.closeCursor(sizeAspRimCursor);
				}
			}
		} catch (Exception e) {
			LogUtil.e("com.goodyear.ejob.util.ReservePWTUtis", e.getMessage());
			e.printStackTrace();
		}
	}

	private static void removeTyreFromList(String tireID) {
		for (int i = 0; i < mReserveTyreListForSelectedTyre.size(); i++) {
			if (tireID.equals(mReserveTyreListForSelectedTyre.get(i).getTyreSerialNo())) {
				mReserveTyreListForSelectedTyre.remove(i);
			}
		}
	}

	public void updateReserveTyre(Context cntxt) {
		String mJobID;
		DatabaseAdapter mDbHelper = new DatabaseAdapter(cntxt);
		mDbHelper.open();
		try {
			if (Constants.COMESFROMUPDATE)
				mJobID = String.valueOf(Constants.GETSELECTEDLISTPOSITION);
			else
				mJobID = String.valueOf(mDbHelper.getJobCount() + 1);

			mReserveTyreList.clear();

			for (int i = 0; i < Constants.Search_TM_ListItems.size(); i++) {
				HashMap<String, String> mTemporaryObject = Constants.Search_TM_ListItems
						.get(i);

				ReservedTyre reservePWT = new ReservedTyre();
				reservePWT.setJobID(mJobID);
				reservePWT.setTyreID(mTemporaryObject.get("TYRE_ID"));
				reservePWT.setTyreSerialNo(mTemporaryObject.get("licence"));
				// TYRE_ID is the db Tyre table id here, not tyre external id
				if (!mReserveTyreList.contains(reservePWT)) {
					mReserveTyreList.add(reservePWT);
				}

			}
			if(mReserveTyreList!=null && mReserveTyreList.size()>0) {
				//User Trace logs
				LogUtil.TraceInfo("null", "Final Selected Tyre - Save BT : ", mReserveTyreList.toString(), false, true, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != mDbHelper) {
				mDbHelper.close();
			}
		}
	}

	/**
	 * Method to load reserved PWT tire into a temporary Mount PWT tyre list without filter
	 */
	public static void noFilterLoadReserveTireListForCurrentTire()
	{
		if(mReserveTyreListForSelectedTyre==null)
		{
			mReserveTyreListForSelectedTyre = new ArrayList<ReservedTyre>();
		}
		mReserveTyreListForSelectedTyre.clear();
		mReserveTyreListForSelectedTyre.addAll(mReserveTyreList);
	}

}
