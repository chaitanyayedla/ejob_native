package com.goodyear.ejob.util;

/**
 * @author munna.kumar
 * 
 */
public class SpinnerObject {

	private int databaseId;
	private String databaseValue;
	private String code;

	public SpinnerObject(int databaseId, String databaseValue) {
		this.databaseId = databaseId;
		this.databaseValue = databaseValue;
	}

	public SpinnerObject(int databaseId, String databaseValue, String code) {
		this.databaseId = databaseId;
		this.databaseValue = databaseValue;
		this.code = code;
	}

	public int getId() {
		return databaseId;
	}

	public String getValue() {
		return databaseValue;
	}

	public String getCode() {
		return code;
	}

	@Override
	public String toString() {
		return databaseValue;
	}

}
// In order to retrieve the id of the currently selected item, you do this :
// int databaseId = Integer.parseInt ( ( (SpinnerObject) spin2.getSelectedItem
// () ).getId () )
