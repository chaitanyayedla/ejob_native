package com.goodyear.ejob.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import android.content.Context;
import android.util.Log;

import com.goodyear.ejob.authenticator.Authentication;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;

/**
 * @author shailesh.p
 * 
 */
public class Account {
	private String login;
	private String password;
	private int jobRefNumber;
	private long passExpirationDate;
	private int passNotificationType;

	//To support pin in Halosys implementation
	private String aPin;
	private int isAccountPinSet;

	/**
	 * save user account to DB after login
	 * 
	 * @param ctx
	 */
	public void saveAccountToDB(Context ctx) {
		DatabaseAdapter check = new DatabaseAdapter(ctx);
		check.createDatabase();
		check.open();

		try {
			check.updateAccount(this);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			check.close();
		}
	}

	/**
	 * get user account from DB
	 * 
	 * @param ctx context
	 * @return account if exist, null if not
	 * @throws Exception
	 */
	public static Account getAccountFromDB(Context ctx) throws Exception {
		LogUtil.i("Account", " Inside getAccountFromDB");
		DatabaseAdapter check = new DatabaseAdapter(ctx);
		check.deleteDB();
		check.close();
		check.createDatabase();
		check.open();
		//CR-447 db upgrade
		check.doUpgrade(ctx);


		// get account object from db
		Account tempAccount = check.getAccountInfo(Authentication
				.getUserName(Constants.USER));

		check.close();
		return tempAccount;
	}

	public String getLogin() {
		return login;
	}

	/**
	 * @param login
	 *        the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the password
	 * @throws UnsupportedEncodingException
	 */
	public String getPassword() {
		String pass = "";
		try {
			pass = Security.decryptMessage(password);
		} catch (Exception e) {
			// log cannot decrypt password
			Log.e("Encryption--", "Decryption Failed" + password);
			System.out.println("check db password");
		}
		return pass;
	}

	/**
	 * @param password
	 *        the password to set
	 * @throws IOException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws InvalidAlgorithmParameterException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	public void setPassword(String password) {
		try {
			this.password = Security.encryptMessage(password);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | InvalidAlgorithmParameterException
				| IllegalBlockSizeException | BadPaddingException | IOException e) {
			// TODO Auto-generated catch block
			Log.e("Encryption--", "Encryption Failed" + password);
		}
	}

	/**
	 * @return the jobRefNumber
	 */
	public int getJobRefNumber() {
		return jobRefNumber;
	}

	/**
	 * @param jobRefNumber
	 *        the jobRefNumber to set
	 */
	public void setJobRefNumber(int jobRefNumber) {
		this.jobRefNumber = jobRefNumber;
	}

	/**
	 * @return the passExpirationDate
	 */
	public long getPassExpirationDate() {
		return passExpirationDate;
	}

	/**
	 * @param passExpirationDate
	 *        the passExpirationDate to set
	 */
	public void setPassExpirationDate(long passExpirationDate) {
		this.passExpirationDate = passExpirationDate;
	}

	/**
	 * @return the passNotificationType
	 */
	public long getPassNotificationType() {
		return passNotificationType;
	}

	/**
	 * @param passNotificationType
	 *        the passNotificationType to set
	 */
	public void setPassNotificationType(int passNotificationType) {
		this.passNotificationType = passNotificationType;
	}

	public String getaPin() {
		String Pin = "";
		try {
			Pin = Security.decryptMessage(aPin);
		} catch (Exception e) {
			// log cannot decrypt password
			Log.e("Encryption--", "Decryption Failed" + aPin);
			System.out.println("check db password");
		}
		return Pin;
	}

	/*
	Settler and getter for aPin and isPinSet
	 */
	public void setaPin(String aPin) {
		try {
			this.aPin = Security.encryptMessage(aPin);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | InvalidAlgorithmParameterException
				| IllegalBlockSizeException | BadPaddingException | IOException e) {
			// TODO Auto-generated catch block
			Log.e("Encryption--", "Encryption Failed" + aPin);
		}
	}

	public int getIsAccountPinSet() {
		return isAccountPinSet;
	}

	public void setIsAccountPinSet(int isAccountPinSet) {
		this.isAccountPinSet = isAccountPinSet;
	}
}
