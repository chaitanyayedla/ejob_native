package com.goodyear.ejob.util;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shailesh.p on 11/19/2014.
 */
public class TyreFormElements implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public TyreFormElements createFromParcel(Parcel in) {
            return new TyreFormElements(in);
        }

        public TyreFormElements[] newArray(int size) {
            return new TyreFormElements[size];
        }
    };
    private int brand;
    private int design;
    private int type;
    private String nsk1;
    private String nsk2;
    private String nsk3;
    private int removalReason;
    private int castingRoute;
    private int regrooveType;
    private boolean rimType;
    private String pressure;

    public TyreFormElements(Parcel in) {
        this.brand = in.readInt();
        this.design = in.readInt();
        this.type = in.readInt();
        this.nsk1 = in.readString();
        this.nsk2 = in.readString();
        this.nsk3 = in.readString();
        this.pressure = in.readString();
        this.removalReason = in.readInt();
        this.castingRoute = in.readInt();
        this.regrooveType = in.readInt();
        boolean[] temp = new boolean[1];
        in.readBooleanArray(temp);
        rimType = temp[0];
    }

    public TyreFormElements() {

    }


    public int getBrand() {
        return brand;
    }

    public void setBrand(int brand) {
        this.brand = brand;
    }

    public int getDesign() {
        return design;
    }

    public void setDesign(int design) {
        this.design = design;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getNsk1() {
        return nsk1;
    }

    public void setNsk1(String nsk1) {
        this.nsk1 = nsk1;
    }

    public String getNsk2() {
        return nsk2;
    }

    public void setNsk2(String nsk2) {
        this.nsk2 = nsk2;
    }

    public String getNsk3() {
        return nsk3;
    }

    public void setNsk3(String nsk3) {
        this.nsk3 = nsk3;
    }

    public int getRemovalReason() {
        return removalReason;
    }

    public void setRemovalReason(int removalReason) {
        this.removalReason = removalReason;
    }

    public int getCastingRoute() {
        return castingRoute;
    }

    public void setCastingRoute(int castingRoute) {
        this.castingRoute = castingRoute;
    }

    public int getRegrooveType() {
        return regrooveType;
    }

    public void setRegrooveType(int regrooveType) {
        this.regrooveType = regrooveType;
    }

    public boolean isRimType() {
        return rimType;
    }

    public void setRimType(boolean rimType) {
        this.rimType = rimType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.brand);
        dest.writeInt(this.design);
        dest.writeInt(this.type);
        dest.writeString(this.nsk1);
        dest.writeString(this.nsk2);
        dest.writeString(this.nsk3);
        dest.writeString(this.pressure);
        dest.writeInt(this.removalReason);
        dest.writeInt(this.castingRoute);
        dest.writeInt(this.regrooveType);
        dest.writeBooleanArray(new boolean[]{rimType});
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getPressure() {
        return pressure;
    }
}
