package com.goodyear.ejob.util;

import java.io.Serializable;

/**
 * @author shailesh.p
 * 
 */
public class RegrooveTyre implements Serializable{

	private int reGrooveType;
	private String nsk1Before;
	private String nsk1After;
	private String nsk2Before;
	private String nsk2After;
	private String nsk3Before;
	private String nsk3After;
	private String pressure;

	/**
	 * @return the reGrooveType
	 */
	public int getReGrooveType() {
		return reGrooveType;
	}

	/**
	 * @param reGrooveType the reGrooveType to set
	 */
	public void setReGrooveType(int reGrooveType) {
		this.reGrooveType = reGrooveType;
	}

	/**
	 * @return the nsk1Before
	 */
	public String getNsk1Before() {
		return nsk1Before;
	}

	/**
	 * @param nsk1Before the nsk1Before to set
	 */
	public void setNsk1Before(String nsk1Before) {
		this.nsk1Before = nsk1Before;
	}

	/**
	 * @return the nsk1After
	 */
	public String getNsk1After() {
		return nsk1After;
	}

	/**
	 * @param nsk1After the nsk1After to set
	 */
	public void setNsk1After(String nsk1After) {
		this.nsk1After = nsk1After;
	}

	/**
	 * @return the nsk2Before
	 */
	public String getNsk2Before() {
		return nsk2Before;
	}

	/**
	 * @param nsk2Before the nsk2Before to set
	 */
	public void setNsk2Before(String nsk2Before) {
		this.nsk2Before = nsk2Before;
	}

	/**
	 * @return the nsk2After
	 */
	public String getNsk2After() {
		return nsk2After;
	}

	/**
	 * @param nsk2After the nsk2After to set
	 */
	public void setNsk2After(String nsk2After) {
		this.nsk2After = nsk2After;
	}

	/**
	 * @return the nsk3Before
	 */
	public String getNsk3Before() {
		return nsk3Before;
	}

	/**
	 * @param nsk3Before the nsk3Before to set
	 */
	public void setNsk3Before(String nsk3Before) {
		this.nsk3Before = nsk3Before;
	}

	/**
	 * @return the nsk3After
	 */
	public String getNsk3After() {
		return nsk3After;
	}

	/**
	 * @param nsk3After the nsk3After to set
	 */
	public void setNsk3After(String nsk3After) {
		this.nsk3After = nsk3After;
	}

	/**
	 * @return the pressure
	 */
	public String getPressure() {
		return pressure;
	}

	/**
	 * @param pressure the pressure to set
	 */
	public void setPressure(String pressure) {
		this.pressure = pressure;
	}

}
