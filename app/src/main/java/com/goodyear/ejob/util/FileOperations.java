package com.goodyear.ejob.util;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * @author shailesh.p
 *
 */
public class FileOperations {

	private static final String TAG = FileOperations.class.getSimpleName();

	/**
	 * Delete the specified file
	 *
	 * @param path
	 */
	public static void removeFile(String path) {

		File fileToDelete = new File(path);
		// if file exists delete it
		if (fileToDelete.exists()) {
			fileToDelete.delete();
		}
	}

	/**
	 * compress files
	 *
	 * @param file
	 *            - path to file to be compressed
	 * @param gzipFile
	 *            - path to decompressed file
	 */
	public static void compressGzipFile(String file, String gzipFile) {
		try {
			FileInputStream fis = new FileInputStream(file);
			FileOutputStream fos = new FileOutputStream(gzipFile);
			GZIPOutputStream gzipOS = new GZIPOutputStream(fos);
			byte[] buffer = new byte[1024];
			int len;
			while ((len = fis.read(buffer)) != -1) {
				gzipOS.write(buffer, 0, len);
			}
			// close resources
			gzipOS.close();
			fos.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * un-compress byte array
	 *
	 * @param response
	 *            - compressed bytes
	 * @return - decompressed String
	 * @throws IOException
	 */
	public static String decompressGzipStream(byte[] response)
			throws IOException {
		ByteArrayInputStream memstream = new ByteArrayInputStream(response);

		GZIPInputStream gzin = new GZIPInputStream(memstream);

		byte[] tempBuffer = new byte[32767 + 1];
		int len = 0;
		String decompressedResponse = "";
		while ((len = gzin.read(tempBuffer)) > 0) {
			for (int i = 0; i < len; i++) {
				decompressedResponse = decompressedResponse
						+ (char) tempBuffer[i];
			}
		}
		return decompressedResponse;
	}

	/**
	 * un-compress file
	 *
	 * @param gzipFile
	 *            - zip file
	 * @param newFile
	 *            - unzipped file
	 * @throws IOException
	 */
	public static void decompressGzipFile(String gzipFile, String newFile)
			throws IOException {
		LogUtil.i(TAG, "decompressing response -- decompressGzipFile -- ");
		FileInputStream fis = new FileInputStream(gzipFile);
		GZIPInputStream gis = new GZIPInputStream(fis);
		FileOutputStream fos = new FileOutputStream(newFile);
		byte[] buffer = new byte[1024];
		int len;
		while ((len = gis.read(buffer)) != -1) {
			fos.write(buffer, 0, len);
		}

		// close resources
		fos.close();
		gis.close();
		LogUtil.i(TAG, "finished decompressing response -- decompressGzipFile -- ");
	}

	/**
	 * read specified file
	 *
	 * @param filepath
	 *            - file path to be read
	 * @return - return bytes read
	 */
	public static byte[] readFile(String filepath) {
		byte[] buffer = new byte[32767 + 1];
		try {
			FileInputStream file = new FileInputStream(filepath);
			file.read(buffer);
			file.close();
		} catch (IOException e) {
			Log.e("FileOperations", "Could not read file " + filepath);
		}
		return buffer;
	}

	/**
	 * write spcified file
	 *
	 * @param filepath
	 *            - path to the file
	 * @param buffer
	 *            - buffer to be written
	 * @param append
	 *            - true to append, false otherwise
	 */
	public static void writeFile(String filepath, byte[] buffer, boolean append) {
		try {
			FileOutputStream file = new FileOutputStream(filepath, append);
			file.write(buffer);
			file.close();
		} catch (IOException e) {
			Log.e("FileOperations", "Could not read file " + filepath);
		}
	}

	/**
	 * write specified file
	 *
	 * @param filepath
	 *            - path to the file
	 * @param buffer
	 *            - buffer to be written
	 * @param append
	 *            - true to append, false otherwise
	 */
	public static void writeFile(String filepath, String buffer, boolean append) {
		String Buffer = "";
		try {
			File file = new File(filepath);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					file, true));
			String dateTime = DateTimeUTC.getCurrentDateWithTime();
//			bufferedWriter.newLine();
//			bufferedWriter.write("::::");
//			bufferedWriter.write(dateTime + ":::");
//			bufferedWriter.write(buffer);

			//Log Encryption
			Buffer = Buffer +"\n"+"::::"+ dateTime + ":::" + buffer;
			bufferedWriter.write(SecureMyData.encrypt(Buffer));
			bufferedWriter.newLine();
			bufferedWriter.close();
		} catch (IOException e) {
			LogUtil.e("FileOperations",
					"Writer String :: Could not write file " + filepath);
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method write information into file (specially used for DB Related Information Writing)
	 * @param filepath file path
	 * @param buffer buffer
	 * @param append append data
	 */
	public static void writeDBInfoToFile(String filepath, String buffer) {
		String Buffer = "";
		try {
			File file = new File(filepath);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					file, true));
			String dateTime = DateTimeUTC.getCurrentDateWithTime();

//			bufferedWriter.newLine();
//			bufferedWriter.write(":: ");
//			bufferedWriter.write(dateTime + " :: ");
//			bufferedWriter.write(buffer);

			//Log Encryption
			Buffer = Buffer +"\n"+":: "+ dateTime + " :: " + buffer;
			bufferedWriter.write(SecureMyData.encrypt(Buffer));
			bufferedWriter.newLine();
			bufferedWriter.close();
		} catch (IOException e) {
			LogUtil.e("FileOperations",
					"Writer String :: Could not write file " + filepath);
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method write information into file (specially used for DB Related Information Writing)
	 * @param filepath file path
	 * @param buffer buffer
	 * @param append append data
	 */
	public static void writeTraceInfoToFile(String filepath, String buffer,boolean line) {
		String Buffer = "";
		try {
			File file = new File(filepath);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					file, true));
			String dateTime = DateTimeUTC.getCurrentDateWithTime();
			if(line) {
				//bufferedWriter.newLine();
				Buffer = Buffer+"\n";
			}
//			bufferedWriter.write(dateTime + " : ");
//			bufferedWriter.write(buffer);
			Buffer = Buffer + dateTime + " : " + buffer;
			//bufferedWriter.write(Security.encryptMessage(Buffer));
			bufferedWriter.write(SecureMyData.encrypt(Buffer));
			//bufferedWriter.write(Buffer);
			bufferedWriter.newLine();

			bufferedWriter.close();
		} catch (IOException e) {
			LogUtil.e("FileOperations",
					"Writer String :: Could not write file " + filepath);
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
	}

	/**
	 * copy one file to another
	 *
	 * @param sourceFile
	 * @param destinationFile
	 * @param append
	 */
	public static void copyFile(String sourceFile, String destinationFile) {
		try {
			byte[] buffer = new byte[32767 + 1];
			FileInputStream sourcefile = new FileInputStream(sourceFile);
			FileOutputStream destfile = new FileOutputStream(destinationFile,
					true);
			int len;
			while ((len = sourcefile.read(buffer)) != -1) {
				destfile.write(buffer, 0, len);
			}
			sourcefile.close();
			destfile.close();
		} catch (IOException e) {
			Log.e("FileOperations", "Could not read file " + sourceFile);
		}
	}

	/**
	 * clear DB after SYNC
	 */
	public static void cleanUp() {
		removeFile(Constants.PROJECT_PATH + "/temp/" + "/snapshot.db");

		removeFile(Constants.PROJECT_PATH + "/temp/" + "/snapshotcom.db");

		removeFile(Constants.PROJECT_PATH + "/temp/" + "/snapshotcompressed.db");

		removeFile(Constants.PROJECT_PATH + "/temp/" + "/response.txt");

		removeFile(Constants.PROJECT_PATH + "/" + Constants.USER + "_temp.db3");

		removeFile(Constants.PROJECT_PATH + "/" + Constants.USER
				+ "_response.db3");

		removeFile(Constants.PROJECT_PATH + "/" + Constants.USER + "_.db3");

		removeFile(Constants.PROJECT_PATH + "/" + "pack.pack");

		//removeFile(Constants.PROJECT_PATH + "/temp/" + "stockinfo.db");

	}

	/**
	 * check if the given file exists or not.
	 *
	 * @param file
	 *            file to check if exists
	 * @return true if exists, false if not
	 */
	public static boolean isFileExisting(String file) {
		File fileTocheck = new File(file);
		return fileTocheck.exists();
	}

	/**
	 * move files from old path to new path
	 *
	 * @param oldPath
	 *            old path or current path of the file
	 * @param newPath
	 *            new path or destination path of the file
	 * @throws IOException
	 */
	public static void moveFile(String oldPath, String newPath, String fileName)
			throws IOException {
		File sourceFile = new File(oldPath);
		if (!sourceFile.exists())
			return;
		File destinationDir = new File(newPath);
		destinationDir.mkdirs();
		File destinationFile = new File(newPath + "/" + fileName);
		destinationFile.createNewFile();
		// copy input to output file
		InputStream mInput = new FileInputStream(sourceFile);
		OutputStream mOutput = new FileOutputStream(destinationFile);
		byte[] mBuffer = new byte[1024];
		int mLength;
		while ((mLength = mInput.read(mBuffer)) > 0) {
			mOutput.write(mBuffer, 0, mLength);
		}
		mOutput.flush();
		mOutput.close();
		mInput.close();
	}

	/**
	 * check if the file path is a directory or not
	 *
	 * @param path
	 *            the path to check for directory
	 * @return true if path is directory otherwise false
	 */
	public static boolean isGivenPathDir(String path) {
		File file = new File(path);
		return file.isDirectory();
	}

	/**
	 * get the list of all the DB and config files that are stored at the given
	 * path
	 *
	 * @param path
	 *            path to search files for
	 * @return Arraylist of files, null if no files are found
	 */
	public static ArrayList<File> getListOfDBFiles(String path) {
		File dirToQueryDBfiles = new File(path);
		ArrayList<File> dbFiles = null;
		if (path != null) {
			dbFiles = new ArrayList<>(Arrays.asList(dirToQueryDBfiles
					.listFiles()));
			for (int iterator = dbFiles.size() - 1; iterator >= 0; iterator--) {
				if (!(dbFiles.get(iterator).getName().endsWith(".db3") || dbFiles
						.get(iterator).getName().endsWith(".xml"))) {
					dbFiles.remove(iterator);
				}
			}
		}
		return dbFiles;
	}

	/**
	 * Removes all the files and directories under the given dir
	 *
	 * @param path
	 *            path to the directory to be deleted
	 */
	public static void clearDir(File dir) {
		if (dir == null) {
			return;
		}
		if (!dir.exists()) {
			return;
		}
		if (!dir.isDirectory())
			return;

		// get the list of files under the dir
		File[] filesUnderDir = dir.listFiles();
		if (filesUnderDir != null) {
			for (File file : filesUnderDir) {
				// if the file is directory clear this directory also
				if (file.isDirectory()) {
					clearDir(file);
				} else {
					file.delete();
				}
			}
		}
		dir.delete();
	}

	/**
	 * check if files exists in a given directory and move all files to the
	 * given directory
	 *
	 * @param oldPath
	 *            source directory
	 * @param newPath
	 *            destination directory
	 */
	public static void checkAndMoveAllFilesIfExists(String oldPath,
			String newPath) {
		checkAndMoveAllFilesIfExists(oldPath, newPath, false);
	}

	/**
	 * check if files exists in a given directory and move all files to the
	 * given directory, however if onlyDB flag is set then only the db files
	 * will be moved
	 *
	 * @param oldPath
	 *            source directory
	 * @param newPath
	 *            destination directory
	 * @param onlyDB
	 *            if true only moves the DB files and cleans them from source
	 *            directory
	 */
	public static void checkAndMoveAllFilesIfExists(String oldPath,
			String newPath, boolean onlyDb) {
		boolean isOldFileExisting = FileOperations.isFileExisting(oldPath);
		try {
			if (isOldFileExisting && FileOperations.isGivenPathDir(oldPath)) {
				ArrayList<File> dbFiles = FileOperations
						.getListOfDBFiles(oldPath);
				for (File file : dbFiles) {
					// if only db files needs to move
					if (onlyDb && !file.getName().contains("db3")) {
						continue;
					}
					FileOperations.moveFile(file.getAbsolutePath(), newPath,
							file.getName());
					if (onlyDb && file != null) {
						file.delete();
					}
				}
			}
		} catch (IOException exception) {
			exception.printStackTrace();
			LogUtil.i(TAG, "could not file from previous verison");
		}
	}

	/**
	 * check if files exists in a given directory and move all files to the
	 * given directory
	 *
	 * @param oldPath
	 *            source directory
	 * @param newPath
	 *            destination directory
	 */
	public static void checkAndMoveAllDBFilesIfExists(String oldPath,
			String newPath) {
		checkAndMoveAllFilesIfExists(oldPath, newPath, true);
	}

	public static void deleteFilesBeforeGivenDays(int days, String filePath) {
		File file = new File(filePath);
		if (!file.exists()) {
			return;
		}
		long diff = DateTimeUTC.getCurrentDateMilisecond()
				- file.lastModified();

		if (diff > days * 24 * 60 * 60 * 1000) {
			file.delete();
		}
	}

	/**
	 * This method returns the list of all the files under given directory
	 *
	 * @param path
	 *            path of the directory to check
	 * @return Arraylist of all the files
	 */
	public static ArrayList<File> getListOfFilesAtGivenDir(String path) {
		File dir = new File(path);
		File[] fileArray = dir.listFiles();
		if(fileArray != null){
			return new ArrayList<>(Arrays.asList(dir.listFiles()));
		}
		else
			return null;
	}

	/**
	 * Method to get Folder Size
	 * @param directory Folder
	 * @return folder size
	 */
	public static long folderSize(File directory) {
		long length = 0;
		if(directory.listFiles()!=null)
		{
			for (File file : directory.listFiles())
			{
				if (file.isFile()) {
					length += file.length();
				}
				else {
					length += folderSize(file);
				}
			}
		}
		return length;
	}

	/**
	 * Method to get Folder or File Size In MegaByte
	 * @param directory Folder
	 * @return folder or file size in Megabyte
	 */
	public static long getFolderorFileSizeInMegaByte(File directory)
	{
		return folderSize(directory)/1000000;
	}

	/**
	 * Method to delete Old Files in a folder based on size limit
	 * @param directory directory
	 * @param sizeLimit folderSizeLimit
	 */
	public static void deleteFilesInFolderToFitForGivenSize(File directory,long sizeLimit)
	{
		if (directory.listFiles() != null)
		{
			File files[] = directory.listFiles();
			for (int i=0;i<files.length;i++)
			{
				if (files[i].isFile())
				{
					for (int j=i;j<files.length;j++)
					{
						if(files[i].lastModified()>files[j].lastModified())
						{
							File tempFile = files[i];
							files[i] = files[j];
							files[j] = tempFile;
						}
					}
				}
			}

			int fileIndexToDelete=0;
			while(folderSize(directory)>sizeLimit)
			{
				if (files[fileIndexToDelete].exists()) {
					files[fileIndexToDelete].delete();
				}
				fileIndexToDelete++;
			}
		}
	}

	/**
	 * Method to Sort Given File list in Ascending Order based on Last Modified Date
	 * @param files directory path
	 * @return file list in ascending order
	 */
	public static ArrayList<File> sortFilesInAscendingOrder(ArrayList<File> files)
	{
		if (files.size() >0) {
			for (int i = 0; i < files.size(); i++) {
				if (files.get(i).isFile()) {
					for (int j = i; j < files.size(); j++) {
						if (files.get(i).lastModified() > files.get(j).lastModified()) {
							File tempFile1 = files.get(i);
							File tempFile2 = files.get(j);
							files.remove(i);
							files.add(i,tempFile2);
							files.remove(j);
							files.add(j,tempFile1);
						}
					}
				}
			}
			return files;
		}
		return files;
	}

	/**
	 * Method to delete Old Files in a Multiple folder based on size limit
	 * @param directorys Multiple folder
	 * @param sizeLimit limit
	 */
	public static void deleteFilesFromMultipleFolderToFitForGivenSize(ArrayList<File> directorys,long sizeLimit)
	{
		ArrayList<File> fileCollection = new ArrayList<File>();
		for(int i=0;i<directorys.size();i++)
		{
			if (directorys.get(i).listFiles() != null) {
				File filesUnderDirectory[] = directorys.get(i).listFiles();
				for(int j=0;j<filesUnderDirectory.length;j++)
					{
						fileCollection.add(filesUnderDirectory[j]);
					}
			}
		}

		// To get Sorted List
		ArrayList<File> sortedFilecollection=sortFilesInAscendingOrder(fileCollection);

		int fileIndexToDelete=0;
		//calculate total size of directories
		Long totalDirectorySize = calculateMultipleFolderSize(directorys);

		//Code to delete files to fit the capacity (size limit) of folder
		while(totalDirectorySize > sizeLimit)
		{
			//if file exist.. do delete operation
			if (sortedFilecollection.get(fileIndexToDelete).exists()) {
				sortedFilecollection.get(fileIndexToDelete).delete();
			}
			fileIndexToDelete++;
			totalDirectorySize= calculateMultipleFolderSize(directorys);
		}
	}

	/**
	 * Method to calculate total size of multiple folder
	 * @param directorys folder list
	 * @return total folder size
	 */
	public static long calculateMultipleFolderSize(ArrayList<File> directorys)
	{
		Long totalSize=0L;
		for(int i=0;i<directorys.size();i++)
		{
			totalSize = totalSize+folderSize(directorys.get(i));
		}
		return totalSize;
	}

	public static void convertAndCopyFile(String mInputFile, String mOutputFile)
			throws IOException {
		LogUtil.i(TAG, "decompressing response -- decompressGzipFile -- ");
		FileInputStream mfis = new FileInputStream(mInputFile);
		FileOutputStream mfos = new FileOutputStream(mOutputFile);
		byte[] buffer = new byte[1024];
		int len;
		while ((len = mfis.read(buffer)) != -1) {
			mfos.write(buffer, 0, len);
		}
		mfis.close();
		mfos.close();
		LogUtil.i(TAG, "finished decompressing response -- decompressGzipFile -- ");
	}

	/**
	 * Method to back up Server Response information
	 * Copied from HttpRequest_Ejob file
	 * @param doBackup doBackup
	 */
	public static void backupResponse(Boolean doBackup) {
		if (doBackup) {
			LogUtil.i(TAG, "Response Backup");
			String DestinationFileName = Constants.PROJECT_PATH + "/ResponseBackup/" + Constants.USER + DateTimeUTC.getCurrentDataAndTime() + ".txt";
			String SourceFileName = Constants.PROJECT_PATH + Constants.RESPONSEFILE_PATH;
			try {
				File file = new File(DestinationFileName);
				if (!file.exists()) {
					file.getParentFile().mkdirs();
					file.createNewFile();

				}
				//Copy source to Destination
				FileOperations.copyFile(SourceFileName, DestinationFileName);
			} catch (Exception e) {
				LogUtil.i(TAG, "Response Exception - " + e.getMessage());
			}
		}
	}
}
