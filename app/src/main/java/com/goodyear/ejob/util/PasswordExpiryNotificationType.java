package com.goodyear.ejob.util;

/**
 * @author shailesh.p
 * 
 */
public class PasswordExpiryNotificationType {

	public static int None = 0;
	public static int TwoWeeks = 1;
	public static int TenDays = 2;
	public static int OneWeek = 3;
	public static int FiveDays = 4;
	public static int ThreeDays = 5;
	public static int TwoDays = 6;
	public static int OneDay = 7;

}
