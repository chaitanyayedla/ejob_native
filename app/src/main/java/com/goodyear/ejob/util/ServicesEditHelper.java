package com.goodyear.ejob.util;

/**
 * @author amitkumar.h
 * 
 */
public class ServicesEditHelper {
	public String sapCode;
	public String count;

	/**
	 * @return the sapCode
	 */
	public String getSapCode() {
		return sapCode;
	}

	/**
	 * @param sapCode the sapCode to set
	 */
	public void setSapCode(String sapCode) {
		this.sapCode = sapCode;
	}

	/**
	 * @return the count
	 */
	public String getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(String count) {
		this.count = count;
	}
}
