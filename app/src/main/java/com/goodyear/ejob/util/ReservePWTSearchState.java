package com.goodyear.ejob.util;

/**
 * Maintaining state, to query available reserve PWT tyre info.
 * Created by giriprasanth.vp on 7/20/2016.
 */
public class ReservePWTSearchState
{
    public static final int NONE =0;
    public static final int DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_TYRES_NON_MAINTAINED = 1;
    public static final int DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_TYRES_PARTIALLY_MAINTAINED = 2;
    public static final int DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_TYRES_COMPLETELY_MAINTAINED =3;
    public static final int DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_SELECTED_TYRE_WITH_NON_MAINTAINED_AXLE = 4;
    public static final int DIFFERENT_SIZE_NOT_ALLOWED_FOR_VEH_SELECTED_TYRE_WITH_MAINTAINED_AXLE = 5;
    public static final int DIFFERENT_SIZE_ALLOWED_FOR_VEHICLE = 6;
}
