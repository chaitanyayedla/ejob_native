package com.goodyear.ejob.util;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author amitkumar.h
 * 
 */
public class TyreJobDataBean implements Parcelable {
	private String serial_parcable;
	private String design_parcable;
	private String action_parcable;
	private String repairComapny_parcable;
	private String workOrder_parcable;
	private String operation_parcable;
	private String depth_parcable;
	private String jobUUID_parcable;
	private String jobItemUUID_parcable;
	private String vendorName_parcable;
	private String customerName_parcable;
	private String tyreId_parcable;
	private String SAPContractID_parcable;
	private String status_parcable;

	public String getSerial_parcable() {
		return serial_parcable;
	}

	public String getDesign_parcable() {
		return design_parcable;
	}

	public String getAction_parcable() {
		return action_parcable;
	}

	public String getRepairComapny_parcable() {
		return repairComapny_parcable;
	}

	public String getWorkOrder_parcable() {
		return workOrder_parcable;
	}

	public String getOperation_parcable() {
		return operation_parcable;
	}

	public String getDepth_parcable() {
		return depth_parcable;
	}

	public String getJobUUID_parcable() {
		return jobUUID_parcable;
	}

	public String getJobItemUUID_parcable() {
		return jobItemUUID_parcable;
	}

	public String getVendorName_parcable() {
		return vendorName_parcable;
	}

	public String getCustomerName_parcable() {
		return customerName_parcable;
	}

	public String getTyreId_parcable() {
		return tyreId_parcable;
	}

	public String getSAPContractID_parcable() {
		return SAPContractID_parcable;
	}

	public String getStatus_parcable() {
		return status_parcable;
	}

	public static Parcelable.Creator getCreator() {
		return CREATOR;
	}

	public TyreJobDataBean() {

	}

	// Constructor
	public TyreJobDataBean(String serial_parcable, String design_parcable,
			String action_parcable, String repairComapny_parcable,
			String workOrder_parcable, String operation_parcable,
			String depth_parcable, String jobUUID_parcable,
			String jobItemUUID_parcable, String vendorName_parcable,
			String SAPContractID_parcable, String tyreId_parcable,
			String status_parcable, String customerName_parcable) {
		this.serial_parcable = serial_parcable;
		this.design_parcable = design_parcable;
		this.action_parcable = action_parcable;
		this.repairComapny_parcable = repairComapny_parcable;
		this.workOrder_parcable = workOrder_parcable;
		this.operation_parcable = operation_parcable;
		this.depth_parcable = depth_parcable;

		this.vendorName_parcable = vendorName_parcable;
		this.customerName_parcable = customerName_parcable;
		this.tyreId_parcable = tyreId_parcable;
		this.tyreId_parcable = SAPContractID_parcable;
		this.jobUUID_parcable = jobUUID_parcable;
		this.jobItemUUID_parcable = jobItemUUID_parcable;
		this.status_parcable = status_parcable;
	}

	// Getter and setter methods

	// Parceling part
	public TyreJobDataBean(Parcel in) {
		String[] data = new String[14];

		in.readStringArray(data);
		this.serial_parcable = data[0];
		this.design_parcable = data[1];
		this.action_parcable = data[2];
		this.repairComapny_parcable = data[3];
		this.workOrder_parcable = data[4];
		this.operation_parcable = data[5];
		this.depth_parcable = data[5];
		this.jobUUID_parcable = data[6];
		this.jobItemUUID_parcable = data[7];
		this.vendorName_parcable = data[8];
		this.customerName_parcable = data[9];
		this.customerName_parcable = data[10];
		this.tyreId_parcable = data[11];
		this.SAPContractID_parcable = data[12];
		this.status_parcable = data[13];
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStringArray(new String[] { this.serial_parcable,
				this.design_parcable, this.action_parcable,
				this.repairComapny_parcable, this.workOrder_parcable,
				this.operation_parcable, this.depth_parcable,
				this.jobItemUUID_parcable, this.vendorName_parcable,
				this.customerName_parcable, this.customerName_parcable,
				this.tyreId_parcable, this.SAPContractID_parcable,
				this.status_parcable });
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public TyreJobDataBean createFromParcel(Parcel in) {
			return new TyreJobDataBean(in);
		}

		public TyreJobDataBean[] newArray(int size) {
			return new TyreJobDataBean[size];
		}
	};
}