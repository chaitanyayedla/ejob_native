package com.goodyear.ejob.util;

import java.util.Arrays;
import java.util.List;

/**
 * @author amitkumar.h
 * 
 */
public class TireActionType {
	public static final String NONE = "0";
	public final static String REMOVE = "1";
	public final static String MOUNTNEW = "2";
	public final static String SWAP = "3";
	public final static String REGROOVE = "4";
	public final static String SERVICEMATERIAL = "5";
	public final static String TIRESERVICE = "6";
	public final static String AXLESERVICE = "7";
	public final static String TOR = "8";
	public final static String VALVEFITTED = "9";
	public final static String HIGHTPRESSUREVALUECAPFITTED = "10";
	public final static String Retorquing = "11";
	public final static String VALUEFIXINGSUPPORT = "12";
	public final static String DYNAMICBALANCING = "13";
	public final static String STATICBALANCING = "14";
	public final static String RIGIDVALVEEXTENSINFITTED = "15";
	public final static String FLEXIBLEVALVEEXTENSINFITTED = "16";
	public final static String PUNCTUREDREPAIRREPAIRED = "17";
	public final static String MINORREPAIR = "18";
	public final static String MAJORREPAIR = "19";
	public final static String TYREREPAIREANDREINFORCEMENT = "20";
	public final static String BREAKSPOTREPAIR = "21";
	public final static String MAJORREPAIRWITHVULCANISATION = "22";
	public final static String TIREFILL = "23";
	public final static String LOGICALSWAP = "24";
	public final static String MOUNTPW = "25";
	public final static String SWAP_ACTION_PHYSICAL = "1";
	//Inspection Action Type
	public final static String INSPECTION = "26";

	// Variables added to compare int value
	public static final int NONE_INT = 0;
	public final static int REMOVE_INT = 1;
	public final static int MOUNTNEW_INT = 2;
	public final static int SWAP_INT = 3;
	public final static int REGROOVE_INT = 4;
	public final static int SERVICEMATERIAL_INT = 5;
	public final static int TIRESERVICE_INT = 6;
	public final static int AXLESERVICE_INT = 7;
	public final static int TOR_INT = 8;
	public final static int VALVEFITTED_INT = 9;
	public final static int HIGHTPRESSUREVALUECAPFITTED_INT = 10;
	public final static int RETORQING_INT = 11;
	public final static int VALUEFIXINGSUPPORT_INT = 12;
	public final static int DYNAMICBALANCING_INT = 13;
	public final static int STATICBALANCING_INT = 14;
	public final static int RIGIDVALVEEXTENSINFITTED_INT = 15;
	public final static int FLEXIBLEVALVEEXTENSINFITTED_INT = 16;
	public final static int PUNCTUREDREPAIRREPAIRED_INT = 17;
	public final static int MINORREPAIR_INT = 18;
	public final static int MAJORREPAIR_INT = 19;
	public final static int TYREREPAIREANDREINFORCEMENT_INT = 20;
	public final static int BREAKSPOTREPAIR_INT = 21;
	public final static int MAJORREPAIRWITHVULCANISATION_INT = 22;
	public final static int TIREFILL_INT = 23;
	public final static int LOGICALSWAP_INT = 24;
	public final static int MOUNTPW_INT = 25;
	public final static int INSPECTION_INT = 26;

	public static List<String> ACTIONTYPE_WOT = Arrays.asList(VALVEFITTED, HIGHTPRESSUREVALUECAPFITTED, 
			VALUEFIXINGSUPPORT, DYNAMICBALANCING, STATICBALANCING, RIGIDVALVEEXTENSINFITTED, 
			FLEXIBLEVALVEEXTENSINFITTED, PUNCTUREDREPAIRREPAIRED, MINORREPAIR, MAJORREPAIR, 
			TYREREPAIREANDREINFORCEMENT, BREAKSPOTREPAIR, MAJORREPAIRWITHVULCANISATION, TIREFILL);
}
