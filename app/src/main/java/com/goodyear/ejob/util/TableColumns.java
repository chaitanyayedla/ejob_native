
package com.goodyear.ejob.util;
/**
 * @author Sonata-software
 * This class will have column names of local database tables
 * 
 * NOTE: Separate inner classes need to be maintained for each tables
 *
 */
public class TableColumns {
	
	public class JobTableColumn{
		public static final String COL_SIGNATURE_IMAGE = "SignatureImage";
		public static final String COL_AGREE_STATUS = "AgreeStatus";
		public static final String COL_THIRTY_MINUTES_ROTQUE_STATUS = "ThirtyMinutesRetorqueStatus";
		public static final String COL_VEHICLE_UNATTENEDED_STATUS = "VehicleUnattendedStatus";
		public static final String COL_VEHICLE_OOD = "VehicleODO";
		public static final String COL_DRIVER_NAME = "DriverName";
		public static final String COL_SIGNATURE_REASON_ID = "SignatureReasonID";
		public static final String COL_MISSING_ODOMETER_REASON_ID = "MissingOdometerReasonID";
	}
}
