package com.goodyear.ejob.util;

/**
 * @author Shal
 * 
 */
public class Job {
	private static String externalID = "";
	private static String licenseNumber = "";
	private static String sapContractNumber = "";
	private static String vehicleID = "";
	private static String technitian = "";
	private static String jobType = "";
	private static String odometerType = "";
	private static String gpsLocation = "";
	private static String location = "";
	private static String timeFinished = "";
	private static String missingOdometerReasonID = "";
	private static String agreeStatus = "";
	private static String vehicleUnattendedStatus = "";
	private static String timeOnSite = "";
	private static String timeStarted = "";
	private static String timeReceived = "";
	private static String callerPhone = "";
	private static String vendorCountryID = "";
	private static String thirtyMinutesRetorqueStatus = "";
	private static String defectAuthNumber = "";
	private static String driverName = "";
	private static String actionLineN = "";
	private static String hudometer = "";
	private static String vendorDetail = "";
	private static String vendorSAPCode = "";
	private static String signatureReasonID = "";
	private static String stockLocation = "";
	private static String vehicleODO = "";
	private static byte[] signatureImage;
	private static String note = "";
	private static String status = "";
	private static String jobRefNumber = "";
	private static String jobValidityDate = "";
	private static String timeOffsetMinutes = "";
	private static String jobRemovalReasonID = "";
	private static String callerName = "";
	private static String customerName = "";

	//CR - 548 : Bring the latest SAP tacho reading to the application and compare to that entered by the user
	//Last recorded odometer value
	private static String previousOdometerValue = "0";

	public static String getPreviousOdometerValue() {
		return previousOdometerValue;
	}

	public static void setPreviousOdometerValue(String previousOdometerValue) {
		Job.previousOdometerValue = previousOdometerValue;
	}

	//CR - 550 (some vehicles have different torque settings per axle)
	//Torque Settings per axle info
	private static String TorqueSettingsSame = "0";

	public static String getTorqueSettingsSame() {
		return TorqueSettingsSame;
	}

	public static void setTorqueSettingsSame(String torqueSettingsSame) {
		TorqueSettingsSame = torqueSettingsSame;
	}


	/**
	 * @return the customerName
	 */
	public static String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public static void setCustomerName(String customerName) {
		Job.customerName = customerName;
	}

	/**
	 * @return the callName
	 */
	public static String getCallerName() {
		return callerName;
	}

	/**
	 * @param callName the callName to set
	 */
	public static void setCallerName(String callName) {
		Job.callerName = callName;
	}

	/**
	 * @return the externalID
	 */
	public static String getExternalID() {
		return externalID;
	}

	/**
	 * @param externalID the externalID to set
	 */
	public static void setExternalID(String externalID) {
		Job.externalID = externalID;
	}

	/**
	 * @return the licenseNumber
	 */
	public static String getLicenseNumber() {
		return licenseNumber;
	}

	/**
	 * @param licenseNumber the licenseNumber to set
	 */
	public static void setLicenseNumber(String licenseNumber) {
		Job.licenseNumber = licenseNumber;
	}

	/**
	 * @return the sapContractNumber
	 */
	public static String getSapContractNumber() {
		return sapContractNumber;
	}

	/**
	 * @param sapContractNumber the sapContractNumber to set
	 */
	public static void setSapContractNumber(String sapContractNumber) {
		Job.sapContractNumber = sapContractNumber;
	}

	/**
	 * @return the vehicleID
	 */
	public static String getVehicleID() {
		return vehicleID;
	}

	/**
	 * @param vehicleID the vehicleID to set
	 */
	public static void setVehicleID(String vehicleID) {
		Job.vehicleID = vehicleID;
	}

	/**
	 * @return the technitian
	 */
	public static String getTechnitian() {
		return technitian;
	}

	/**
	 * @param technitian the technitian to set
	 */
	public static void setTechnitian(String technitian) {
		Job.technitian = technitian;
	}

	/**
	 * @return the jobType
	 */
	public static String getJobType() {
		return jobType;
	}

	/**
	 * @param jobType the jobType to set
	 */
	public static void setJobType(String jobType) {
		Job.jobType = jobType;
	}

	/**
	 * @return the odometerType
	 */
	public static String getOdometerType() {
		return odometerType;
	}

	/**
	 * @param odometerType the odometerType to set
	 */
	public static void setOdometerType(String odometerType) {
		Job.odometerType = odometerType;
	}

	/**
	 * @return the gpsLocation
	 */
	public static String getGpsLocation() {
		return gpsLocation;
	}

	/**
	 * @param gpsLocation the gpsLocation to set
	 */
	public static void setGpsLocation(String gpsLocation) {
		Job.gpsLocation = gpsLocation;
	}

	/**
	 * @return the location
	 */
	public static String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public static void setLocation(String location) {
		Job.location = location;
	}

	/**
	 * @return the timeFinished
	 */
	public static String getTimeFinished() {
		return timeFinished;
	}

	/**
	 * @param timeFinished the timeFinished to set
	 */
	public static void setTimeFinished(String timeFinished) {
		Job.timeFinished = timeFinished;
	}

	/**
	 * @return the missingOdometerReasonID
	 */
	public static String getMissingOdometerReasonID() {
		return missingOdometerReasonID;
	}

	/**
	 * @param missingOdometerReasonID the missingOdometerReasonID to set
	 */
	public static void setMissingOdometerReasonID(String missingOdometerReasonID) {
		Job.missingOdometerReasonID = missingOdometerReasonID;
	}

	/**
	 * @return the agreeStatus
	 */
	public static String getAgreeStatus() {
		return agreeStatus;
	}

	/**
	 * @param agreeStatus the agreeStatus to set
	 */
	public static void setAgreeStatus(String agreeStatus) {
		Job.agreeStatus = agreeStatus;
	}

	/**
	 * @return the vehicleUnattendedStatus
	 */
	public static String getVehicleUnattendedStatus() {
		return vehicleUnattendedStatus;
	}

	/**
	 * @param vehicleUnattendedStatus the vehicleUnattendedStatus to set
	 */
	public static void setVehicleUnattendedStatus(String vehicleUnattendedStatus) {
		Job.vehicleUnattendedStatus = vehicleUnattendedStatus;
	}

	/**
	 * @return the timeOnSite
	 */
	public static String getTimeOnSite() {
		return timeOnSite;
	}

	/**
	 * @param timeOnSite the timeOnSite to set
	 */
	public static void setTimeOnSite(String timeOnSite) {
		Job.timeOnSite = timeOnSite;
	}

	/**
	 * @return the timeStarted
	 */
	public static String getTimeStarted() {
		return timeStarted;
	}

	/**
	 * @param timeStarted the timeStarted to set
	 */
	public static void setTimeStarted(String timeStarted) {
		Job.timeStarted = timeStarted;
	}

	/**
	 * @return the timeReceived
	 */
	public static String getTimeReceived() {
		return timeReceived;
	}

	/**
	 * @param timeReceived the timeReceived to set
	 */
	public static void setTimeReceived(String timeReceived) {
		Job.timeReceived = timeReceived;
	}

	/**
	 * @return the callerPhone
	 */
	public static String getCallerPhone() {
		return callerPhone;
	}

	/**
	 * @param callerPhone the callerPhone to set
	 */
	public static void setCallerPhone(String callerPhone) {
		Job.callerPhone = callerPhone;
	}

	/**
	 * @return the vendorCountryID
	 */
	public static String getVendorCountryID() {
		return vendorCountryID;
	}

	/**
	 * @param vendorCountryID the vendorCountryID to set
	 */
	public static void setVendorCountryID(String vendorCountryID) {
		Job.vendorCountryID = vendorCountryID;
	}

	/**
	 * @return the thirtyMinutesRetorqueStatus
	 */
	public static String getThirtyMinutesRetorqueStatus() {
		return thirtyMinutesRetorqueStatus;
	}

	/**
	 * @param thirtyMinutesRetorqueStatus the thirtyMinutesRetorqueStatus to set
	 */
	public static void setThirtyMinutesRetorqueStatus(
			String thirtyMinutesRetorqueStatus) {
		Job.thirtyMinutesRetorqueStatus = thirtyMinutesRetorqueStatus;
	}

	/**
	 * @return the defectAuthNumber
	 */
	public static String getDefectAuthNumber() {
		return defectAuthNumber;
	}

	/**
	 * @param defectAuthNumber the defectAuthNumber to set
	 */
	public static void setDefectAuthNumber(String defectAuthNumber) {
		Job.defectAuthNumber = defectAuthNumber;
	}

	/**
	 * @return the driverName
	 */
	public static String getDriverName() {
		return driverName;
	}

	/**
	 * @param driverName the driverName to set
	 */
	public static void setDriverName(String driverName) {
		Job.driverName = driverName;
	}

	/**
	 * @return the actionLineN
	 */
	public static String getActionLineN() {
		return actionLineN;
	}

	/**
	 * @param actionLineN the actionLineN to set
	 */
	public static void setActionLineN(String actionLineN) {
		Job.actionLineN = actionLineN;
	}

	/**
	 * @return the hudometer
	 */
	public static String getHudometer() {
		return hudometer;
	}

	/**
	 * @param hudometer the hudometer to set
	 */
	public static void setHudometer(String hudometer) {
		Job.hudometer = hudometer;
	}

	/**
	 * @return the vendorDetail
	 */
	public static String getVendorDetail() {
		return vendorDetail;
	}

	/**
	 * @param vendorDetail the vendorDetail to set
	 */
	public static void setVendorDetail(String vendorDetail) {
		Job.vendorDetail = vendorDetail;
	}

	/**
	 * @return the vendorSAPCode
	 */
	public static String getVendorSAPCode() {
		return vendorSAPCode;
	}

	/**
	 * @param vendorSAPCode the vendorSAPCode to set
	 */
	public static void setVendorSAPCode(String vendorSAPCode) {
		Job.vendorSAPCode = vendorSAPCode;
	}

	/**
	 * @return the signatureReasonID
	 */
	public static String getSignatureReasonID() {
		return signatureReasonID;
	}

	/**
	 * @param signatureReasonID the signatureReasonID to set
	 */
	public static void setSignatureReasonID(String signatureReasonID) {
		Job.signatureReasonID = signatureReasonID;
	}

	/**
	 * @return the stockLocation
	 */
	public static String getStockLocation() {
		return stockLocation;
	}

	/**
	 * @param stockLocation the stockLocation to set
	 */
	public static void setStockLocation(String stockLocation) {
		Job.stockLocation = stockLocation;
	}

	/**
	 * @return the vehicleODO
	 */
	public static String getVehicleODO() {
		return vehicleODO;
	}

	/**
	 * @param vehicleODO the vehicleODO to set
	 */
	public static void setVehicleODO(String vehicleODO) {
		Job.vehicleODO = vehicleODO;
	}

	/**
	 * @return the signatureImage
	 */

	/**
	 * @param signatureImage the signatureImage to set
	 */

	/**
	 * @return the note
	 */
	public static String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public static void setNote(String note) {
		Job.note = note;
	}

	/**
	 * @return the status
	 */
	public static String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public static void setStatus(String status) {
		Job.status = status;
	}

	/**
	 * @return the jobRefNumber
	 */
	public static String getJobRefNumber() {
		return jobRefNumber;
	}

	/**
	 * @param jobRefNumber the jobRefNumber to set
	 */
	public static void setJobRefNumber(String jobRefNumber) {
		Job.jobRefNumber = jobRefNumber;
	}

	/**
	 * @return the jobValidityDate
	 */
	public static String getJobValidityDate() {
		return jobValidityDate;
	}

	/**
	 * @param jobValidityDate the jobValidityDate to set
	 */
	public static void setJobValidityDate(String jobValidityDate) {
		Job.jobValidityDate = jobValidityDate;
	}

	/**
	 * @return the timeOffsetMinutes
	 */
	public static String getTimeOffsetMinutes() {
		return timeOffsetMinutes;
	}

	/**
	 * @param timeOffsetMinutes the timeOffsetMinutes to set
	 */
	public static void setTimeOffsetMinutes(String timeOffsetMinutes) {
		Job.timeOffsetMinutes = timeOffsetMinutes;
	}

	/**
	 * @return the jobRemovalReasonID
	 */
	public static String getJobRemovalReasonID() {
		return jobRemovalReasonID;
	}

	/**
	 * @param jobRemovalReasonID the jobRemovalReasonID to set
	 */
	public static void setJobRemovalReasonID(String jobRemovalReasonID) {
		Job.jobRemovalReasonID = jobRemovalReasonID;
	}

	/**
	 * @return the signatureImage
	 */
	public static byte[] getSignatureImage() {
		return signatureImage;
	}

	/**
	 * @param signatureImage the signatureImage to set
	 */
	public static void setSignatureImage(byte[] signatureImage) {
		Job.signatureImage = signatureImage;
	}

	public static void clearJobObject() {
		externalID = "";
		licenseNumber = "";
		sapContractNumber = "";
		vehicleID = "";
		technitian = "";
		jobType = "";
		odometerType = "";
		gpsLocation = "";
		location = "";
		timeFinished = "";
		missingOdometerReasonID = "";
		agreeStatus = "";
		vehicleUnattendedStatus = "";
		timeOnSite = "";
		timeStarted = "";
		timeReceived = "";
		callerPhone = "";
		vendorCountryID = "";
		thirtyMinutesRetorqueStatus = "";
		defectAuthNumber = "";
		driverName = "";
		actionLineN = "";
		hudometer = "";
		vendorDetail = "";
		vendorSAPCode = "";
		signatureReasonID = "";
		stockLocation = "";
		vehicleODO = "";
		signatureImage = null;
		note = "";
		status = "";
		jobRefNumber = "";
		jobValidityDate = "";
		timeOffsetMinutes = "";
		jobRemovalReasonID = "";
		callerName = "";
		customerName = "";
	}

}
