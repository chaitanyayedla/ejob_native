package com.goodyear.ejob.util;

import java.io.File;
import java.util.ArrayList;

/**
 * @author amitkumar.h
 * 
 */
public class LogManager {
	private static final String TAG = LogManager.class.getSimpleName();
	public static final String LOG_DIR = "/logs/";
	public static final String DB_LOG_DIR = "/DBLogs/";
	public static final String TRACE_INFO_DIR = "/TraceInfo/";
	public static final int DAY_TO_DELETE = 3;
	public static final int DAY_TO_DELETE_TRACE_INFO = 7;
	public static final String DBLogFile = "DBLogs";

	/**
	 * This method gets the name for the log file
	 * 
	 * @return log file name
	 */
	public static String getLogFileName() {
		// current date is used as the filename
		String currentDate = DateTimeUTC.getCurrentDate();
		return LOG_DIR + currentDate + ".txt";
	}

	/**
	 * THis method gets the name for the DB log file
	 * @return DB log file name
	 */
	public static String getDBLogFileName()
	{
		String currentDate = DateTimeUTC.getCurrentDate();
		return  DB_LOG_DIR + Constants.VALIDATE_USER + "_" + currentDate + ".txt";
	}

	/**
	 * THis method gets the name for the DB log file
	 * @return DB log file name
	 */
	public static String getTraceInfoFileName()
	{
		String currentDate = DateTimeUTC.getCurrentDate();
		return  TRACE_INFO_DIR + Constants.VALIDATE_USER + "_" + currentDate + ".txt";
	}


	public static void clearLogs() {
		try {
			ArrayList<File> filesUnderDir = FileOperations
					.getListOfFilesAtGivenDir(Constants.PROJECT_PATH + LOG_DIR);
			if (filesUnderDir == null) {
				return;
			}
			for (File file : filesUnderDir) {
				FileOperations.deleteFilesBeforeGivenDays(DAY_TO_DELETE,
						file.getAbsolutePath());
			}
		} catch (Exception e) {
			LogUtil.e(TAG, "Could not clear logs...");
			LogUtil.pst(e);
		}
	}

	/**
	 * Method to clear Trace Information
	 */
	public static void clearTraceInfo() {
		try {
			ArrayList<File> filesUnderDir = FileOperations
					.getListOfFilesAtGivenDir(Constants.PROJECT_PATH + TRACE_INFO_DIR);
			if (filesUnderDir == null) {
				return;
			}
			for (File file : filesUnderDir) {
				FileOperations.deleteFilesBeforeGivenDays(DAY_TO_DELETE_TRACE_INFO,
						file.getAbsolutePath());
			}
		} catch (Exception e) {
			LogUtil.e(TAG, "Could not Trace Info...");
			LogUtil.pst(e);
		}
	}
}
