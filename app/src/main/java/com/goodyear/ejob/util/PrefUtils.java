package com.goodyear.ejob.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * @author johnmiya.s
 * 
 */
public class PrefUtils {

	public static int getInt(Context context, int key, int defValue) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(context);
		return pref.getInt(context.getString(key), defValue);
	}

	public static boolean setInt(Context context, int key, int value) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = pref.edit();
		editor.putInt(context.getString(key), value);
		return editor.commit();
	}
}