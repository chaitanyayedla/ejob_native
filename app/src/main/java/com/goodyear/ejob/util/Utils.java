package com.goodyear.ejob.util;

import java.util.Calendar;

import org.joda.time.DateTime;

/**
 *
 *
 */
public class Utils {
	/*private static final long TICKS_AT_EPOCH = 621355968000000000L;
	private static final long TICKS_PER_MILLISECOND = 10000;

	public static DateTime convertMillisecondsToUTCDateTime(long milliseconds) {
		DateTime date = null;
		try {
			date = new DateTime((milliseconds - TICKS_AT_EPOCH)
					/ TICKS_PER_MILLISECOND);
		} catch (Exception e) {
		}

		return date;
	}
	
	*//**
	 * @return Current date with format: dd-mm-YYYY
	 *//* 
	public static String getCurrentDate() {
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		return day + "-" + (month + 1) + "-" + year;
	}
	
	
	*//**
	 * @return Splitted date with dd,mm & YYYY in int[]
	 * @param datepassed
	 * @throws NumberFormatException
	 *//*
	public static int[] getSplittedDate(String datepassed) throws NumberFormatException{
		int splitteddate[] = null;
		
		String[] splitteddatestr = datepassed.split("-");
		if(splitteddatestr.length==3){
			splitteddate = new int[3];
			
			splitteddate[0] = Integer.parseInt(splitteddatestr[0]);
			splitteddate[1] = Integer.parseInt(splitteddatestr[1]);
			splitteddate[2] = Integer.parseInt(splitteddatestr[2]);
		}
		
		return splitteddate;
	}

	*//**
	 * @return Current time with format: HH:mm
	 *//*
	public static String getCurrentTime() {
		String minutes;
		final Calendar c = Calendar.getInstance();
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int min = c.get(Calendar.MINUTE);

		if (min < 10) {
			minutes = "0" + min;
		} else
			minutes = String.valueOf(min);
		return hour + ":" + minutes;
	}
	
	*//**
	 * @return Splitted time with HH & mm in int[]
	 * @param timepassed
	 * @throws NumberFormatException
	 *//*
	public static int[] getSplittedTime(String timepassed) throws NumberFormatException{
		int splittedtime[] = null;
		
		String[] splittedtimestr = timepassed.split(":");
		if(splittedtimestr.length==2){
			splittedtime = new int[2];
			
			splittedtime[0] = Integer.parseInt(splittedtimestr[0]);
			splittedtime[1] = Integer.parseInt(splittedtimestr[1]);
		}
		
		return splittedtime;
	}*/
	
	

}
