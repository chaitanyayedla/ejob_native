
package com.goodyear.ejob.util;
/**
 * @author johnmiya.s
 *
 */
public class TireDesignDetails {
	private String size;
	private String asp;
	private String rim;
	
	/**
	 * @param size
	 * @param asp
	 * @param rim
	 */
	public TireDesignDetails(String size, String asp, String rim) {
		super();
		this.size = size;
		this.asp = asp;
		this.rim = rim;
	}
	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}
	/**
	 * @return the asp
	 */
	public String getAsp() {
		return asp;
	}
	/**
	 * @return the rim
	 */
	public String getRim() {
		return rim;
	}
	
}
