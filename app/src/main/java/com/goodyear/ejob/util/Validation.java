package com.goodyear.ejob.util;

/**
 * @author shailesh.p
 * 
 */
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import com.goodyear.ejob.inactivity.EjobAlertDialog;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Validation {

	private static String dateFormat = "dd-MMM-yyyy";

	private static int USERNAME_MIN = 7;
	private static int USERNAME_MAX = 12;

	private static int PASSWORD_MIN = 3;
	private static int PASSWORD_MAX = 12;

	private static int SEARCHFIELD_MIN = 2;
	private static int SEARCHFIELD_MAX = 30;

	private static int PRIMARYODO_MIN = 1;
	private static int PRIMARYODO_MAX = 12;

	private static int RETORQUE_MIN = 2;
	private static int RETORQUE_MAX = 4;

	private static int SERIALNUMBERSEARCH_MIN = 3;
	private static int SERIALNUMBERSEARCH_MAX = 50;

	private static int SECONDARYODO_MIN = 0;
	private static int SECONDARYODO_MAX = 12;

	private static int CALLCENTERREFNUM_MIN = 0;
	private static int CALLCENTERREFNUM_MAX = 15;

	private static int DEFECTAUTHNUM_MIN = 0;
	private static int DEFECTAUTHNUM_MAX = 15;

	private static int NAME_MIN = 0;
	private static int NAME_MAX = 25;

	private static int PHONENUMBER_MIN = 0;
	private static int PHONENUMBER_MAX = 25;

	private static int NOTE_MIN = 0;
	private static int NOTE_MAX = 25;

	private static int WORKORDER_MIN = 0;
	private static int WORKORDER_MAX = 30;

	private static int REPAIRCOMPANY_MIN = 0;
	private static int REPAIRCOMPANY_MAX = 50;

	private static int WRENCH_MIN = 0;
	private static int WRENCH_MAX = 50;

	/*Bug 728 : changed max PSI, BAR to 150 and 10.34 respectively */
	private static double PRESSURE_MIN = 14.5037738;
	private static double PRESSURE_MAX = 150.00;

	private static double BAR_MIN = 1.00;
	private static double BAR_MAX = 10.34;

	private static double NSK_MIN = 1.00;
	private static double NSK_MAX = 30.00;

	private static int STOCK_MIN = 0;
	private static int LOCATION_MIN = 1;
	private static int LOCATION_MAX = 50;

	/**
	 * Varify if a Date is correct
	 * 
	 * @param date
	 *            the date in string format
	 * @return true if date is correct and false if date is wrong a
	 *         ParseException will be printed.
	 */
	public static boolean verifyDate(String date) {
		SimpleDateFormat dateverify = new SimpleDateFormat(dateFormat,
				Locale.ENGLISH);
		dateverify.setLenient(false);
		try {
			dateverify.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Get the Date object from int values
	 * 
	 * @param year
	 *            The year selected
	 * @param month
	 *            the month selected
	 * @param day
	 *            the day selected
	 * @param hour
	 *            the hour selected
	 * @param minute
	 *            the minute selected
	 * @param seconds
	 *            the second selected
	 * @return The Date object for the time and date
	 */
	public static Date getDate(int year, int month, int day, int hour,
			int minute, int seconds) {
		// create the calendar object
		GregorianCalendar cal = new GregorianCalendar(year, month, day, hour,
				minute, seconds);
		// return the current time
		return cal.getTime();
	}

	/**
	 * To verify the which is first date
	 * 
	 * @param first
	 *            First Date
	 * @param second
	 *            Second Date
	 * @return 0 if both are same, 1 if first is before second and 2 if first if
	 *         after second
	 */
	public static int verifyDate(Date first, Date second) {
		boolean isBefore = first.before(second);
		boolean isAfter = first.after(second);

		// if both are equal (same)
		if (!isBefore && !isAfter) {
			return 0;
		}
		// if first is before
		else if (isBefore) {
			return 1;
		}
		// if second is before
		else
			return 2;
	}

	/**
	 * Verify User Name (minimum length = 7 , maximum length = 12)
	 * 
	 * @param uname
	 *            user name as String
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyUserName(String uname) {
		if (Validation.verifyAlphaNumeric(uname)
				&& Validation.verifyLength(uname, USERNAME_MIN, USERNAME_MAX))
			return true;
		else
			return false;
	}

	/**
	 * Verify Password (minimum length = 3 , maximum length = 12)
	 * 
	 * @param password
	 *            as String
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyPassword(String password) {
		if (Validation.verifyAlphaNumeric(password)
				&& Validation
						.verifyLength(password, PASSWORD_MIN, PASSWORD_MAX))
			return true;
		else
			return false;
	}

	/**
	 * Verify Serial Number (minimum length = 3 , maximum length = 12)
	 * 
	 * @param serial_number
	 *            as String
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifySerialNumberSearch(String serialNumberSearch) {
		if (Validation.verifyAlphaNumeric(serialNumberSearch)
				&& Validation.verifyLength(serialNumberSearch,
						SERIALNUMBERSEARCH_MIN, SERIALNUMBERSEARCH_MAX))
			return true;
		else
			return false;
	}

	/**
	 * Verify Search Field (minimum length = 2 , maximum length = 30)
	 * 
	 * @param Search_Field
	 *            as String
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifySearchField(String searchField) {
		if (verifyAlphaNumeric(searchField)
				&& verifyLength(searchField, SEARCHFIELD_MIN, SEARCHFIELD_MAX))
			return true;
		else
			return false;
	}

	/**
	 * Verify Retorque value value.
	 * 
	 * @param number
	 *            Retorque value as string
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyRetorque(String retorque) {
		if (verifyNumeric(retorque)
				&& verifyLength(retorque, RETORQUE_MIN, RETORQUE_MAX))
			return true;
		else
			return false;
	}

	/**
	 * Verify PrimaryODO value.
	 * 
	 * @param number
	 *            PrimaryODO value as long (as the max len is 12 digit which
	 *            exceeds the int range
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyPrimaryODO(String number) {
		if (verifyNumeric(number)
				&& verifyLength(number, PRIMARYODO_MIN, PRIMARYODO_MAX))
			return true;
		else
			return false;
	}

	/**
	 * Verify SecondaryODO value.
	 * 
	 * @param number
	 *            SecondaryODO value as long (as the max len is 12 digit which
	 *            exceeds the int range
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifySecondaryODO(String number) {
		if (verifyNumeric(number)
				&& verifyLength(number, SECONDARYODO_MIN, SECONDARYODO_MAX))
			return true;
		else
			return false;
	}

	/**
	 * Verify Phone Number.
	 * 
	 * @param number
	 *            Phone number
	 * 
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyPhoneNumber(String number) {
		if (verifyNumeric(number)
				&& verifyLength(number, PHONENUMBER_MIN, PHONENUMBER_MAX))
			return true;
		else
			return false;
	}

	/**
	 * Verify Name
	 * 
	 * @param name
	 *            Name to be verified
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyName(String name) {
		if (verifyAlphabet(name) && verifyLength(name, NAME_MIN, NAME_MAX))
			return true;
		else
			return false;
	}

	/**
	 * verify call center ref number
	 * 
	 * @param refNumber
	 *            call center ref number
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyCallCenterRefNum(String refNumber) {
		if (verifyLength(refNumber, CALLCENTERREFNUM_MIN, CALLCENTERREFNUM_MAX))
			return true;
		else
			return false;
	}

	/**
	 * verify Defect/Auth number
	 * 
	 * @param refNumber
	 *            defect/ Auth number
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyDefectAuthNum(String refNumber) {
		if (verifyLength(refNumber, DEFECTAUTHNUM_MIN, DEFECTAUTHNUM_MAX))
			return true;
		else
			return false;
	}

	/**
	 * verify stock location
	 * 
	 * @param refNumber
	 *            Stock Location
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyStockLocation(String location) {
		if (verifyLength(location, STOCK_MIN, LOCATION_MAX))
			return true;
		else
			return false;
	}

	/**
	 * verify Location field
	 * 
	 * @param refNumber
	 *            Location string
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyLocation(String location) {
		if (verifyLength(location, LOCATION_MIN, LOCATION_MAX))
			return true;
		else
			return false;
	}

	/**
	 * verify Repair Company
	 * 
	 * @param refNumber
	 *            repair comapany string
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyRepairCompany(String repairCompany) {
		if (verifyLength(repairCompany, REPAIRCOMPANY_MIN, REPAIRCOMPANY_MAX))
			return true;
		else
			return false;
	}

	/**
	 * verify work order field
	 * 
	 * @param refNumber
	 *            work order as string
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyWorkOrder(String workOrder) {
		if (verifyLength(workOrder, WORKORDER_MIN, WORKORDER_MAX))
			return true;
		else
			return false;
	}

	/**
	 * verify Wrench number field
	 * 
	 * @param refNumber
	 *            work order as string
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyWrenchNumber(String wrechNumber) {
		if (verifyLength(wrechNumber, WRENCH_MIN, WRENCH_MAX))
			return true;
		else
			return false;
	}

	/**
	 * verify Note
	 * 
	 * @param refNumber
	 *            Note string
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyNote(String note) {
		if (verifyAlphabet(note) && verifyLength(note, NOTE_MIN, NOTE_MAX))
			return true;
		else
			return false;
	}

	/**
	 * Verify the pressure values
	 * 
	 * @param pressureValue
	 *            pressure value as string
	 * @return true if verified correctly, false if not
	 */
	public static boolean verifyPressure(String pressureValue) {
		if (verifyDecimal(pressureValue)
				&& verifyRange(pressureValue, PRESSURE_MIN, PRESSURE_MAX))
			return true;
		else
			return false;
	}

	/**
	 * verify the NSK values
	 * 
	 * @param nskValue
	 *            NSK values as string
	 * @return true if verified correctly, false if not
	 */
	public static boolean verifyNSK(String nskValue) {
		if (verifyDecimal(nskValue) && verifyRange(nskValue, NSK_MIN, NSK_MAX))
			return true;
		else
			return false;
	}

	/**
	 * Verify the BAR (unit) of pressure MIN = 0.10 MAX = 10.34
	 * 
	 * @param barValue
	 *            pressure value (BAR unit) in string
	 * @return true if verified correctly, false if not.
	 */
	public static boolean verifyBAR(String barValue) {
		if (verifyDecimal(barValue) && verifyRange(barValue, BAR_MIN, BAR_MAX))
			return true;
		else
			return false;
	}

	/**
	 * To check if decimal values are in range
	 * 
	 * @param checkVal
	 *            the decimal value to check
	 * @param min
	 *            minimum accepted value
	 * @param max
	 *            maximum accepted value
	 * @return true if correct, false if not
	 */
	public static boolean verifyRange(String checkVal, double min, double max) {
		double tempValue = CommonUtils.parseDouble(checkVal);
		if (tempValue < min || tempValue > max)
			return false;
		else
			return true;
	}

	/**
	 * Check if the string is alphanumeric string or not.
	 * 
	 * @param alphaNumericString
	 *            String to verify
	 * @return true if alphanumeric, false if not.
	 */
	private static boolean verifyAlphaNumeric(String alphaNumericString) {
		// \pL is regex for letter and \pN is regex for number
		String alphaNumericPattern = "^[a-zA-Z0-9_]*$";
		if (alphaNumericString.matches(alphaNumericPattern))
			return true;
		else
			return false;
	}

	/**
	 * Check if the string is alphabet string or not.
	 * 
	 * @param alphabetString
	 *            String to verify
	 * @return true if alphabet, false if not.
	 */
	private static boolean verifyAlphabet(String alphabetString) {
		// \pL is regex for letter
		String alphaNumericPattern = "^[\\p{L}]*$";
		if (alphabetString.matches(alphaNumericPattern))
			return true;
		else
			return false;
	}

	/**
	 * Verify if passed String is a numeric
	 * 
	 * @param numeric
	 *            numeric as string (hint:use String.valueOf() method)
	 * @return true if number, else otherwise.
	 */
	private static boolean verifyNumeric(String numeric) {
		// \pN is regex for numeric
		String numericPattern = "^[\\p{N}]+$|^$";
		if (numeric.matches(numericPattern))
			return true;
		else
			return false;
	}

	/**
	 * verify the lenth of the string passed
	 * 
	 * @param alphaNumericString
	 *            String to be verified
	 * @param minLen
	 *            Minimum length acceptable
	 * @param maxLen
	 *            Maximum length acceptable
	 * @return true if verified, false if not
	 */
	private static boolean verifyLength(String alphaNumericString, int minLen,
			int maxLen) {
		if (alphaNumericString.length() < minLen
				|| alphaNumericString.length() > maxLen)
			return false;
		else
			return true;
	}

	/**
	 * verify if decimal value
	 * 
	 * @param decimalString
	 *            string to check
	 * @return true if decimal, false if not.
	 */
	public static boolean verifyDecimal(String decimalString) {

		// need clarification on this
		// if(!decimalString.contains("."))
		// decimalString = decimalString + ".0";

		String decimalPattern = "^[0-9]*\\.[0-9]{0,2}";
		if (decimalString.matches(decimalPattern))
			return true;
		else
			return false;
	}

	/**
	 * To check if the bluetooth is enabled
	 * 
	 * @return true if bluetooth is enabled, false if not
	 */
	public static boolean isBlueToothEnabled() {
		BluetoothAdapter blueAdapter = BluetoothAdapter.getDefaultAdapter();
		if (blueAdapter != null) {

			int val = blueAdapter.getState();
			System.out.println(val);
			if (blueAdapter.isEnabled()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * \ Checks if gps is enabled
	 * 
	 * @param ctx
	 *            context object
	 * @return true is gps is enabled, false if not enabled
	 */
	public static boolean isGpsEnabled(Context ctx) {
		LocationManager locationManager = (LocationManager) ctx
				.getSystemService(Context.LOCATION_SERVICE);
		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			return true;
		}
		return false;
	}

	/**
	 * checks if network is available
	 * 
	 * @param ctx
	 *            context object
	 * @return true if network is available, false if not
	 */
	public static boolean isNetworkAvailable(Context ctx) {
		ConnectivityManager connectionManager = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netinfo = connectionManager.getActiveNetworkInfo();
		return netinfo != null && netinfo.isConnected();
	}

	private void buildAlertMessageNoGps(final Context ctx) {
		final EjobAlertDialog builder = new EjobAlertDialog(ctx);
		builder.setMessage(
				"Your GPS seems to be disabled, do you want to enable it?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog,
									final int id) {
								// update user activity on button click
								builder.updateInactivityForDialog();

								ctx.startActivity(new Intent(
										android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(final DialogInterface dialog,
							final int id) {
						// update user activity on button click
						builder.updateInactivityForDialog();

						dialog.cancel();
					}
				});
		final AlertDialog alert = builder.create();
		alert.show();
	}
}
