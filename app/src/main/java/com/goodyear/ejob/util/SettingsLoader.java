package com.goodyear.ejob.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * @author shailesh.p
 * 
 */
public class SettingsLoader {
	private static final String TAG = SettingsLoader.class.getSimpleName();
	private static Context context;
	private static File settingsFile;

	private static String getSettingFileLocation() {
		return Constants.PROJECT_PATH + "/";
	}

	public static void loadSettings(Context ctx) throws Exception {

		context = ctx;
		// check if torque settings option is avaliable in settings xml (config)
		// file
		try {
			updateSettingsFile();
		} catch (Exception e) {
			e.printStackTrace();
			LogUtil.i(TAG,
					"Could not add Torque settings in  settings xml file");
		}
		try {
			if (checkSettingsFileExistsInPhoneMemory())
				copySettingsInToSharedPreference();
			else {
				copyDefaultSettingToPhone();
				copySettingsInToSharedPreference();
			}
		} catch (Exception e) {
			throw new Exception("problem reading settings xml");
		}
	}

	/**
	 * @throws Exception
	 * 
	 */
	private static void copyDefaultSettingToPhone() throws Exception {
		// get assets
		AssetManager assetManager = context.getAssets();
		InputStream in = null;
		OutputStream out = null;
		try {
			in = assetManager.open("settings.xml");

			File outdir = new File(getSettingFileLocation());
			outdir.mkdir();
			File outfile = new File(outdir, "settings.xml");
			outfile.createNewFile();
			out = new FileOutputStream(outfile);
			copyFile(in, out);
			in.close();
			in = null;
			out.flush();
			out.close();
			out = null;
		} catch (IOException e) {
			throw new Exception("cannot move default setting to phone");
		}
	}

	public static void updateSettingsFile() throws SAXException,
			TransformerException {
		try {
			Node node = null;
			Node settings = null;
			// get the settings file
			settingsFile = new File(getSettingFileLocation() + "settings.xml");
			// load the xml parser
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(settingsFile);

			// Get the settings element by tag name\

			settings = doc.getElementsByTagName("appSettings").item(0);

			// loop the settings child nodes
			NodeList list = settings.getChildNodes();

			boolean check = false;
			for (int i = 1; i < list.getLength(); i = i + 2) {
				node = list.item(i);
				String settingKey = node.getNodeName();
				if (settingKey.equalsIgnoreCase("TorqueSetting")) {
					check = true;
					break;
				}
			}
			if (!check) {
				Element torqueElement = doc.createElement("TorqueSetting");
				torqueElement.appendChild(doc.createTextNode("OFF"));
				settings.appendChild(torqueElement);

				// commit changes to XML file
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(settingsFile);
				transformer.transform(source, result);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void copyFile(InputStream in, OutputStream out)
			throws IOException {
		byte[] buffer = new byte[1024];
		int read;
		while ((read = in.read(buffer)) != -1) {
			out.write(buffer, 0, read);
		}
	}

	public static boolean checkSettingsFileExistsInPhoneMemory() {
		String path = getSettingFileLocation() + "settings.xml";
		settingsFile = new File(path);
		if (settingsFile.exists()) {
			return true;
		} else
			return false;
	}

	public static void copySettingsInToSharedPreference()
			throws ParserConfigurationException, SAXException, IOException {
		try {
			createMissingNodesForCameraSettings();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// init shared preferences
		SharedPreferences sharedPref = context.getSharedPreferences(
				Constants.GOODYEAR_CONF, 0);
		Editor sharedPrefEditor = sharedPref.edit();

		// get the settings file
		settingsFile = new File(getSettingFileLocation() + "settings.xml");

		// load the xml parser
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(settingsFile);

		// Get the settings element by tag name
		Node settings = doc.getElementsByTagName("appSettings").item(0);

		// loop the settings child nodes
		NodeList list = settings.getChildNodes();

		// loop and load values in SP
		for (int i = 1; i < list.getLength(); i = i + 2) {
			Node node = list.item(i);
			String settingKey = node.getNodeName();

			String settingValue = node.getTextContent();
			sharedPrefEditor.putString(settingKey, settingValue);
			if (settingKey.equals("TorqueSetting")
					&& settingValue.equals("OFF")) {
				Constants.TORQUE_ENABLE = "OFF";
			} else if (settingKey.equals("TorqueSetting")
					&& settingValue.equals("ON")) {
				Constants.TORQUE_ENABLE = "ON";
			} else if (settingKey.equals("CameraSetting")
					&& settingValue.equals("ON")) {
				Constants.CAMERA_ENABLE_FOR_MIN_TD = "ON";
			} else if (settingKey.equals("CameraSetting")
					&& settingValue.equals("OFF")) {
				Constants.CAMERA_ENABLE_FOR_MIN_TD = "OFF";
			}else if(settingKey.equals("InspectionSetting")
					&& settingValue.equals("ON")){
				Constants.INSPECTION_ENABLE = "ON";
			}else if(settingKey.equals("InspectionSetting")
					&& settingValue.equals("OFF")){
				Constants.INSPECTION_ENABLE = "OFF";
			}
		}

		sharedPrefEditor.commit();
	}

	public static boolean updateSettings(
			HashMap<String, String> updatedSettings, Context ctx) {
		LogUtil.i(TAG, "Inside updateSettings ");
		// set context of application
		context = ctx;
		try {
			// if file does not exist
			if (!checkSettingsFileExistsInPhoneMemory())
				copySettingsInToSharedPreference();

			// init shared preferences
			SharedPreferences sharedPref = context.getSharedPreferences(
					Constants.GOODYEAR_CONF, 0);
			Editor sharedPrefEditor = sharedPref.edit();

			// get the settings file
			settingsFile = new File(getSettingFileLocation() + "settings.xml");

			// load the xml parser
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(settingsFile);

			Set<String> keyvalues = updatedSettings.keySet();
			// Iterator<String> nodeName = keyvalues.iterator();

			for (String nodeName : keyvalues) {
				// get value for the node
				String nodeValue = updatedSettings.get(nodeName);

				// update xml file
				Node nodeToUpdate = doc.getElementsByTagName(nodeName).item(0);
				nodeToUpdate.setTextContent(nodeValue);

				// update Shared Preferences
				sharedPrefEditor.putString(nodeName, nodeValue);
			}

			// commit changes to SP
			sharedPrefEditor.commit();

			// commit changes to XML file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(settingsFile);
			transformer.transform(source, result);

			return true;

		} catch (Exception e) {
			Log.e("SettingsLoader", "could not update settings file");
		}
		return false;
	}

	/**
	 * This method create new nodes in the existing XML file when APP is updated
	 * for a build <= 1.1.10.0 (no sync on APP update)
	 * 
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerException
	 */
	public static void createMissingNodesForVersionUpdate()
			throws ParserConfigurationException, SAXException, IOException,
			TransformerException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(settingsFile);

		// Get the settings element by tag name
		Node settings = doc.getElementsByTagName("appSettings").item(0);

		Element versionCode = doc.createElement("appversioncode");
		versionCode.appendChild(doc.createTextNode("0"));
		settings.appendChild(versionCode);

		Element lastUpdateTime = doc.createElement("LastAppUpgradeDateTime");
		lastUpdateTime.appendChild(doc.createTextNode("0"));
		settings.appendChild(lastUpdateTime);

		Element cameraSettingsForNsk = doc.createElement("CameraSetting");
		cameraSettingsForNsk.appendChild(doc.createTextNode("OFF"));
		settings.appendChild(cameraSettingsForNsk);

		Element thresholdNskForDismount = doc.createElement("ThresholdNSK");
		thresholdNskForDismount.appendChild(doc.createTextNode("0"));
		settings.appendChild(thresholdNskForDismount);

		//For Inspection
		Element inspectionSetting = doc.createElement("InspectionSetting");
		inspectionSetting.appendChild(doc.createTextNode("OFF"));
		settings.appendChild(inspectionSetting);

		// commit changes to XML file
		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(settingsFile);
		transformer.transform(source, result);

	}

	/**
	 * This method create new nodes in the existing XML file when APP is updated
	 * for a build <= 1.1.10.0 and 1.2.1.3(no sync on APP update)
	 * 
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerException
	 */
	public static void createMissingNodesForCameraSettings()
			throws ParserConfigurationException, SAXException, IOException,
			TransformerException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(settingsFile);

		// Get the settings element by tag name
		Node settings = doc.getElementsByTagName("appSettings").item(0);

		if (doc.getElementsByTagName("CameraSetting").item(0) == null) {
			Element cameraSettingsForNsk = doc.createElement("CameraSetting");
			cameraSettingsForNsk.appendChild(doc.createTextNode("OFF"));
			settings.appendChild(cameraSettingsForNsk);
		}

		if (doc.getElementsByTagName("ThresholdNSK").item(0) == null) {
			Element thresholdNskForDismount = doc.createElement("ThresholdNSK");
			thresholdNskForDismount.appendChild(doc.createTextNode("0"));
			settings.appendChild(thresholdNskForDismount);
		}

		if (doc.getElementsByTagName("InspectionSetting").item(0) == null) {
			Element inspectionSetting = doc.createElement("InspectionSetting");
			inspectionSetting.appendChild(doc.createTextNode("OFF"));
			settings.appendChild(inspectionSetting);
		}

		// commit changes to XML file
		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(settingsFile);
		transformer.transform(source, result);

	}


}
