package com.goodyear.ejob.util;

import android.content.Context;
import com.goodyear.ejob.dbhelpers.DataBaseHandler;
import java.io.File;

/**
 * @author Pragati.PJ
 */
public class DbBackupUtil {
    private String TAG = getClass().getSimpleName();
    private Context mContext;

    public DbBackupUtil(Context context){
        this.mContext = context.getApplicationContext();
    }

    public void backUpFullDB() {
        LogUtil.i(TAG, " Inside backUpFullDB()");
        String DestinationFileName = Constants.PROJECT_PATH + Constants.FULLDBBackUp_DIR + Constants.USER + ".db3";

        try {
            File file = new File(DestinationFileName);
            if(file.exists()){
                LogUtil.d(TAG, " delete the file ");
                file.delete();
            }
            if (!file.exists()) {
                LogUtil.d(TAG, " create file");
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            DataBaseHandler dbHandler = new DataBaseHandler(mContext);
            String SourceFileName = dbHandler.getPathForDBFile() ;

            LogUtil.i(TAG, " SourceFileName : " + SourceFileName);
            LogUtil.i(TAG, " DestinationFileName : " + DestinationFileName);

            LogUtil.i(TAG, " Full Db backup started");
            //Copy source to Destination
            FileOperations.copyFile(SourceFileName, DestinationFileName);

            LogUtil.i(TAG, " Full DB backup done");

        } catch (Exception e) {
            LogUtil.i(TAG, "Exception - " + e.getMessage());
        }
    }
}
