package com.goodyear.ejob.util;

import java.util.LinkedHashMap;

/**
 * @author johnmiya.s
 *         Class used for set label values if label coming from Translation table
 */
public class ConstantHashForDefaultValues {

	public static LinkedHashMap<Integer, String> labelHashMap = new LinkedHashMap<Integer, String>();

	public static LinkedHashMap<Integer, String> getWiringLabel() {

		labelHashMap.put(1, "eJobs");
		labelHashMap.put(294, "Licence plate");
		labelHashMap.put(297, "Date");
		labelHashMap.put(298, "eJob number");
		labelHashMap.put(403, "CustomerName");
		labelHashMap.put(424, "Tire Management");
		labelHashMap.put(438, "Completed Jobs");
		labelHashMap.put(439, "Deleted Jobs");
		labelHashMap.put(443, "Incomplete Jobs");
		labelHashMap.put(469, "no network...");
		labelHashMap.put(505, "Please, Check your Internet Connection and start the synchronization again");
		labelHashMap.put(292, "OK");
		labelHashMap.put(992, "Please sign the signature form or select reason if not signed !!");
		labelHashMap.put(995,
				"Finshed Time must be greater than starts time!!!!");
		
	/*	labelHashMap.put(1, "eJobs");
		labelHashMap.put(2, "Submit");
		labelHashMap.put(3, "not yet defined");
		labelHashMap
				.put(4,
						"This eJob is locked as it has been signed.\r\nYou can review it but not edit it, OK?");
		labelHashMap
				.put(5,
						"Your job is over 3 days old \r\n You can continue, but as an offline job");
		labelHashMap.put(6, "Search");
		labelHashMap.put(7, "Regular eJob");
		labelHashMap.put(8, "Breakdown eJob");
		labelHashMap.put(9, "Search by");
		labelHashMap.put(10, "Vin");
		labelHashMap.put(11, "Chassis No");
		labelHashMap
				.put(12,
						"Really? All changes made in this eJob since last save will be lost. If no save has been made, all eJob data will be lost.");
		labelHashMap.put(13, "Exit the job");
		labelHashMap.put(14, "Additional Info");
		labelHashMap.put(15, "Please, select eJob Type");
		labelHashMap.put(16, "Contract");
		labelHashMap.put(17, "Please select contract");
		labelHashMap.put(18, "Please enter Licence Plate");
		labelHashMap.put(19,
				"you can't use some of those characters, please retry");
		labelHashMap.put(20, "Please select Vendor");
		labelHashMap
				.put(21,
						"This vehicle is not recognised, please try again, or record it on paper (you may want to inform your manager too)");
		labelHashMap.put(22, "Confirm");
		labelHashMap.put(23, "Information");
		labelHashMap.put(24, "Vendor");
		labelHashMap.put(25, "Not Found");
		labelHashMap.put(26, "Please select a Vendor");
		labelHashMap.put(27, "Contract Name");
		labelHashMap.put(28, "Licence Plate/Reg No/Trailer No");
		labelHashMap.put(29, "Vehicle Identification No");
		labelHashMap.put(30, "Contract Info");
		labelHashMap.put(31, "Job");
		labelHashMap.put(32, "Technician");
		labelHashMap.put(33, "Time Now");
		labelHashMap.put(34, "Time/Location");
		labelHashMap.put(35, "Info");
		labelHashMap.put(36, "Vehicle");
		labelHashMap.put(37, "Primary Odo");
		labelHashMap.put(38, "Secondary Odo");
		labelHashMap.put(39, "Call center ref. Number");
		labelHashMap.put(40, "Driver's Name");
		labelHashMap.put(41, "Defect / Auth Number");
		labelHashMap.put(42, "Caller's Name");
		labelHashMap.put(43, "Caller's Phone Number");
		labelHashMap.put(44, "Time Recieved");
		labelHashMap.put(45, "Time Started");
		labelHashMap.put(46, "Time On Site");
		labelHashMap.put(47, "Time Finished");
		labelHashMap.put(48, "GPS Location");
		labelHashMap.put(49, "Location");
		labelHashMap.put(50, "GPS Not Available");
		labelHashMap.put(51, "oops, wrong format for Vehicle ODO");
		labelHashMap.put(52, "oops, wrong format for Hubometer");
		labelHashMap.put(53, "Location's required");
		labelHashMap.put(54,
				"Time Received should be earlier than Time Started");
		labelHashMap
				.put(55, "Time Started should be earlier than Time On Site");
		labelHashMap.put(56,
				"Time On Site should be earlier than Time Finished");
		labelHashMap.put(57, "Make");
		labelHashMap.put(59, "Removal NSK");
		labelHashMap.put(60, "Casing route");
		labelHashMap.put(61, "Removal reason");
		labelHashMap.put(62, "Picture viewer");
		labelHashMap.put(63, "Type of Service");
		labelHashMap.put(64, "Brand");
		labelHashMap.put(65, "Dimension");
		labelHashMap.put(66, "ASP");
		labelHashMap.put(67, "RIM");
		labelHashMap.put(68, "Design");
		labelHashMap.put(69, "Work On tyre");
		labelHashMap.put(70, "Note");
		labelHashMap.put(71, "Please enter a serial number");
		labelHashMap.put(72, "Dismount old tyre");
		labelHashMap.put(74, "Tyre Details");
		labelHashMap.put(75, "oops, serial number is invalid");
		labelHashMap.put(76, "You need to enter the ASP");
		labelHashMap.put(77, "You need to enter the Size");
		labelHashMap.put(78, "You need to enter the RIM");
		labelHashMap.put(79, "You must enter NSK1");
		labelHashMap.put(80, "You must enter NSK2");
		labelHashMap.put(81, "You must enter NSK3");
		labelHashMap.put(82, "The tyre Design is required");
		labelHashMap.put(83, "Wrong format for ASP, please try again");
		labelHashMap.put(84, "Wrong format for Size, please try again");
		labelHashMap.put(85, "Wrong format for RIM, please try again");
		labelHashMap.put(86, "Wrong format for NSK1, please try again");
		labelHashMap.put(87, "Wrong format for NSK2, please try again");
		labelHashMap.put(88, "Wrong format for NSK3, please try again");
		labelHashMap.put(89, "You'll need to enter ar least one NSK");
		labelHashMap.put(90, "NSK1 can't be above {0} mm");
		labelHashMap.put(91, "NSK2 can't be above {0} mm");
		labelHashMap.put(92, "NSK3 can't be above {0} mm");
		labelHashMap.put(93, "Please enter a removal reason");
		labelHashMap.put(94, "Please enter a casing route");
		labelHashMap.put(95, "Removal reason.");
		labelHashMap.put(96, "Casing Route.");
		labelHashMap
				.put(97,
						"This tyre Serial number is not unique, is it OK to have duplicate serial numbers in this job?");
		labelHashMap.put(98, "enter the Serial Number you want to find");
		labelHashMap.put(99, "ah, that Tyre's not been found.");
		labelHashMap.put(100, "Summary");
		labelHashMap.put(101, "eJob info");
		labelHashMap.put(102, "Goods Removed");
		labelHashMap.put(103, "Goods Supplied");
		labelHashMap.put(104, "Services");
		labelHashMap.put(105, "No Torque Setting Reason: {0}");
		labelHashMap.put(106, "Retorque value");
		labelHashMap.put(107, "Tyre-off");
		labelHashMap.put(108, "Tyre-on");
		labelHashMap.put(110, "Data only (logical) SWAP");
		labelHashMap.put(111, "SWAP tyres (physical swap)");
		labelHashMap.put(112, "TOR");
		labelHashMap.put(113, "Retorque");
		labelHashMap.put(114, "Tyre service");
		labelHashMap.put(115, "Tyre removed");
		labelHashMap.put(116, "Tyre fitted");
		labelHashMap.put(117, "Tyre regrooved");
		labelHashMap.put(118, "Tyre logically swapped");
		labelHashMap.put(119, "Tyre physically swapped");
		labelHashMap.put(120, "Tyre turned on rim");
		labelHashMap.put(121, "Retorqe applied");
		labelHashMap.put(123, "Mounting NSK");
		labelHashMap.put(124, "Original position");
		labelHashMap.put(125, "SAP code");
		labelHashMap.put(126, "NSK Before");
		labelHashMap.put(127, "NSK After");
		labelHashMap.put(128, "Regroove Type");
		labelHashMap.put(129, "Mount part worn Tyre");
		labelHashMap
				.put(130,
						"You can't complete a job with tyres missing from wheel positions. Please mount tyres and then complete the job");
		labelHashMap.put(131, "Dismounted");
		labelHashMap.put(132, "Added");
		labelHashMap.put(133, "Regrooved");
		labelHashMap.put(134, "Swapped");
		labelHashMap.put(135, "Turned on the Rim");
		labelHashMap.put(136, "Axle #");
		labelHashMap.put(137, "Type");
		labelHashMap.put(139, "Service");
		labelHashMap.put(140, "Quantity");
		labelHashMap.put(141, "Position");
		labelHashMap.put(142, "Pattern");
		labelHashMap.put(143, "Additional");
		labelHashMap.put(145, "Action");
		labelHashMap.put(146, "Axle Services");
		labelHashMap.put(147, "Additional Services");
		labelHashMap.put(148, "Not Mantained");
		labelHashMap.put(149, "P");
		labelHashMap.put(150, "You'll have to dismount a tyre first");
		labelHashMap.put(151, "Please define removed tyre details first");
		labelHashMap.put(152, "Is this Serial Number correct?\r\n{0}");
		labelHashMap
				.put(153,
						"There is an error in this eJob with tyre {0}\r\n Would you like to cancel your job?.\r\nPress No to enter the correct Serial Number");
		labelHashMap
				.put(154,
						"This tyre {0} has been logically swapped with pos {1}\r\n\r\nThe tyre defined is now on {2}");
		labelHashMap.put(155, "Is the tyre brand / model correct?");
		labelHashMap.put(156,
				"The tyre will be moved from position axle {0} to axle {1}");
		labelHashMap
				.put(157,
						"Are you sure you want to dismount this tyre? Serial Number:{0}");
		labelHashMap
				.put(158,
						"To swap this tyre press YES, and the select the other position (it can be an empty position)");
		labelHashMap
				.put(159,
						"This eJob has conflicting tyre sizes. Record it on paper and inform your manager");
		labelHashMap
				.put(160,
						"You're about to turn this tyre on the rim, OK? Serial Number:{0}");
		labelHashMap.put(170, "SN");
		labelHashMap.put(171, "Select an action");
		labelHashMap.put(172, "Please select an action");
		labelHashMap.put(173, "Mount NEW Tyre");
		labelHashMap.put(174, "Dismount Tyre");
		labelHashMap.put(175, "Data only (logical) SWAP.");
		labelHashMap.put(177, "Regroove tyre");
		labelHashMap.put(180, "Serial Number search");
		labelHashMap.put(181, "oops, Wrong Serial Number");
		labelHashMap.put(182, "Damage");
		labelHashMap.put(183, "Damage Group");
		labelHashMap.put(184, "Take Photo please");
		labelHashMap.put(185, "A photo is required");
		labelHashMap.put(186, "Take a picture now");
		labelHashMap.put(187, "Axle Service Form");
		labelHashMap.put(188, "Work on axle");
		labelHashMap.put(189, "Axle");
		labelHashMap.put(190, "Axle Type");
		labelHashMap.put(191, "Geometry");
		labelHashMap.put(192, "Geometry check");
		labelHashMap.put(193, "Geometry check & correction");
		labelHashMap.put(194, "Job Finish Time");
		labelHashMap.put(195, "Notes");
		labelHashMap.put(196, "Input Method");
		labelHashMap.put(197, "Reason for missing odometer");
		labelHashMap.put(198, "Please select a reason");
		labelHashMap.put(199, "Rec. Inflaction is required");
		labelHashMap.put(200, "Regroove");
		labelHashMap.put(201, "NSK1 Before");
		labelHashMap.put(202, "NSK2 Before");
		labelHashMap.put(203, "NSK3 Before");
		labelHashMap.put(204, "NSK1 After");
		labelHashMap.put(205, "NSK2 After");
		labelHashMap.put(206, "NSK3 After");
		labelHashMap.put(207, "Please enter the regroove type");
		labelHashMap.put(208, "NSK1 Before is required");
		labelHashMap.put(209, "NSK2 Before is required");
		labelHashMap.put(210, "NSK3 Before is required");
		labelHashMap.put(211, "NSK1 After is required");
		labelHashMap.put(212, "NSK2 After is required");
		labelHashMap.put(213, "NSK3 After is required");
		labelHashMap.put(214, "Wrong format for NSK1 Before");
		labelHashMap.put(215, "Wrong format for NSK2 Before");
		labelHashMap.put(216, "Wrong format for NSK3 Before");
		labelHashMap.put(217, "Wrong format for NSK1 After");
		labelHashMap.put(218, "Wrong format for NSK2 After");
		labelHashMap.put(219, "Wrong format for NSK3 After");
		labelHashMap.put(220,
				"Your start NSK1 cannot be greater than the finish NSK1");
		labelHashMap.put(221,
				"Your start NSK2 cannot be greater than the finish NSK2");
		labelHashMap.put(222,
				"Your start NSK3 cannot be greater than the finish NSK3");
		labelHashMap
				.put(223, "Your pressure entry's not correct, please retry");
		labelHashMap.put(224, "A pressure is required");
		labelHashMap.put(225, "Block");
		labelHashMap.put(226, "RIB");
		labelHashMap.put(227, "New tyre");
		labelHashMap.put(228, "Part worn tyres");
		labelHashMap.put(229, "Retreads tyres");
		labelHashMap.put(230, "Select a Service Material");
		labelHashMap.put(231, "Select a {0}:");
		labelHashMap.put(232, "Please make a selection.");
		labelHashMap
				.put(233,
						"Get a signature for this ejob or select a reason why it's not signed");
		labelHashMap.put(234, "Set current time as eJob finished time?");
		labelHashMap.put(235, "oops, Wrong format for ODO, please try again");
		labelHashMap.put(236, "Retorque label issued");
		labelHashMap.put(237, "Retorque value applied");
		labelHashMap.put(238, "A retorque value is required");
		labelHashMap.put(239, "Wrong format for retorque value");
		labelHashMap.put(240, "Tyres that were found");
		labelHashMap.put(241, "Are you really sure you want to cancel?");
		labelHashMap.put(242, "The pressure can't be above {0} {1}");
		labelHashMap.put(243, "NSK is required");
		labelHashMap.put(244, "NSK can't be above {0} mm");
		labelHashMap.put(245, "The format for the NSK is wrong");
		labelHashMap.put(246, "Valve fitted");
		labelHashMap.put(247, "High pressure valve cap fitted");
		labelHashMap.put(248, "Retorquing");
		labelHashMap.put(249, "Valve fixing support");
		labelHashMap.put(250, "Dynamic balancing");
		labelHashMap.put(251, "Static balancing");
		labelHashMap.put(252, "Rigid valve extension fitted");
		labelHashMap.put(253, "Flexible valve extension fitted");
		labelHashMap.put(254, "Puncture repair");
		labelHashMap.put(255, "Tyre fill");
		labelHashMap.put(256, "Minor repair");
		labelHashMap.put(257, "Major repair");
		labelHashMap.put(258, "Tyre repair and reinforcement");
		labelHashMap.put(259, "Brake spot repair");
		labelHashMap.put(260, "Major repair with vulcanisation");
		labelHashMap.put(261, "Drive");
		labelHashMap.put(262, "Lift");
		labelHashMap.put(263, "Passive");
		labelHashMap.put(264, "Steer");
		labelHashMap.put(265, "Steer & Drive");
		labelHashMap.put(266, "Was the wheel removed?");
		labelHashMap.put(267,
				"Was the recommended wheel torque setting applied?");
		labelHashMap.put(268, "Reason for NO retorque");
		labelHashMap.put(269, "Login");
		labelHashMap.put(270, "User ID:");
		labelHashMap.put(271, "Password:");
		labelHashMap.put(272, "Log-in");
		labelHashMap
				.put(273,
						"That is embarrassing, The configuration file is missing, please call for support");
		labelHashMap.put(274, "A Login is required");
		labelHashMap.put(275, "A Password is required");
		labelHashMap.put(276,
				"Please check your data connection to do the first activation");
		labelHashMap.put(277, "Wrong login or password");
		labelHashMap.put(278, "Settings");
		labelHashMap.put(279, "eJob Configuration");
		labelHashMap.put(280, "COM Port:");
		labelHashMap.put(281, "Pressure unit:");
		labelHashMap.put(282, "Language:");
		labelHashMap.put(283, "Advanced settings");
		labelHashMap.put(284, "System:");
		labelHashMap.put(285, "URL Security (https)");
		labelHashMap
				.put(286,
						"Please sync with the server to load the updated language settings");
		labelHashMap.put(287, "we're initializing synchronisation...");
		labelHashMap.put(288, "Creating a request...");
		labelHashMap
				.put(289,
						"ooops look like we had some sort of data connection error, please check and try again");
		labelHashMap.put(290, "Connecting to the server {0}...");
		labelHashMap.put(291, "Saving...");
		labelHashMap.put(292, "OK");
		labelHashMap.put(293, "Cancel");
		labelHashMap.put(294, "Licence plate");
		labelHashMap.put(295, "Edit");
		labelHashMap.put(296, "Name");
		labelHashMap.put(297, "Date");
		labelHashMap.put(298, "Ref Number");
		labelHashMap.put(299, "Error");
		labelHashMap.put(300, "None");
		labelHashMap.put(301, "Question");
		labelHashMap.put(302, "Wrong format");
		labelHashMap.put(303, "NSK1");
		labelHashMap.put(304, "NSK2");
		labelHashMap.put(305, "NSK3");
		labelHashMap.put(306, "Pressure");
		labelHashMap.put(307, "Before");
		labelHashMap.put(308, "After");
		labelHashMap.put(309, "Confirmation");
		labelHashMap.put(310, "Back");
		labelHashMap.put(312, "Next");
		labelHashMap.put(313, "Warning");
		labelHashMap.put(314, "Wrench number");
		labelHashMap.put(315, "That value is not correct, please retry");
		labelHashMap.put(316, "Torque setting");
		labelHashMap.put(317, "Damage area");
		labelHashMap.put(318, "Veh ID");
		labelHashMap.put(319, "Account");
		labelHashMap.put(320, "Access");
		labelHashMap.put(321, "Size");
		labelHashMap.put(322, "NSK");
		labelHashMap.put(323, "Regrooved...");
		labelHashMap
				.put(324,
						"New version is available on the server. Would you like to update?");
		labelHashMap.put(325,
				"You are not able to sync because you use outdated version.");
		labelHashMap.put(326,
				"Please complete related Job before posting this one.");
		labelHashMap.put(327, "Version");
		labelHashMap.put(328, "Database:");
		labelHashMap.put(329, "Device Memory");
		labelHashMap.put(330, "Flash Card");
		labelHashMap.put(331, "Yes");
		labelHashMap.put(332, "No");
		labelHashMap.put(333, "Regroove is required.");
		labelHashMap.put(334, "Are you sure you want to delete this picture?");
		labelHashMap.put(335, "Damage picture");
		labelHashMap.put(336, "Description");
		labelHashMap.put(337, "Stock Loc");
		labelHashMap.put(338, "Please, select a Tire");
		labelHashMap.put(339, "Are you sure you want to delete this tyre?");
		labelHashMap.put(340, "Job Removal Reason");
		labelHashMap.put(341, "Enable Offline mode");
		labelHashMap
		.put(342,
				"There's no network connection. This version of eJob does not work offline. Test your connection and try again");
		labelHashMap.put(343, "Timeout");
		labelHashMap
		.put(344,
				"You are trying to swap tyres with different tyre sizes. This is not permitted.");
		labelHashMap.put(345, "No Action");
		labelHashMap.put(346, "Tread Depth");
		labelHashMap.put(347, "Tread Depth is required.");
		labelHashMap.put(348, "Wrong format for Tread Depth.");
		labelHashMap.put(349, "Please select a status");
		labelHashMap.put(350, "Reserve PWT");
		labelHashMap.put(351, "Your GoPass is going to expire in 1 week");
		labelHashMap.put(352, "Your GoPass is going to expire in 10 days");
		labelHashMap.put(353,
				"Advanced warning - Your GoPass will expire in 2 weeks");
		labelHashMap.put(354,
				"Your GoPass will expire in 5 days. Please renew soon");
		labelHashMap.put(355,
				"Your GoPass expires in 3 days. Please renew asap");
		labelHashMap.put(356, "GoPass expires in 2 days. Renew URGENTLY");
		labelHashMap.put(357,
				"GoPass will expire tomorrow. You must renew today");
		labelHashMap
				.put(358,
						"O dear your GoPass has expired, you cannot login to eJob until it is renewed");
		labelHashMap.put(359, "Steel");
		labelHashMap.put(360, "Alloy");
		labelHashMap.put(361, "Rim Type...");
		labelHashMap.put(362, "Rim Type is required");
		labelHashMap.put(363, "Service Material Form");
		labelHashMap.put(364, "Job Form");
		labelHashMap.put(365, "Job Items Form");
		labelHashMap.put(366, "No Tyre Details");
		labelHashMap.put(367, "Enter correct Serial Number");
		labelHashMap.put(368, "Delete");
		labelHashMap.put(369, "Tyre Design Form");
		labelHashMap.put(370, "Save");
		labelHashMap.put(371, "Work on tyre form");
		labelHashMap.put(372, "Work on tyre.");
		labelHashMap.put(373, "Valve fitted.");
		labelHashMap.put(374, "HP valve cap fitted");
		labelHashMap.put(375, "Additional retorque");
		labelHashMap.put(376, "Valve fixing support.");
		labelHashMap.put(377, "Balancing :");
		labelHashMap.put(378, "Valve extension fitted :");
		labelHashMap.put(379, "Repair :");
		labelHashMap.put(380, "Signature");
		labelHashMap
				.put(381,
						"It is essential that all wheel nuts be Re-Torqued after either 30 minutes or after the vehicle has travelled 40-80 KM (25-60 Miles).");
		labelHashMap.put(382, "30 minutes re-torque completed?");
		labelHashMap.put(383, "Vehicle unattended");
		labelHashMap.put(384, "No signature reason:");
		labelHashMap.put(385, "Clear");
		labelHashMap.put(386, "Mounting pressure:");
		labelHashMap.put(387, "Fitting NSK:");
		labelHashMap.put(388, "Retorque value:");
		labelHashMap.put(389, "Add");
		labelHashMap.put(390, "Operation");
		labelHashMap.put(391, "Rim Type");
		labelHashMap
				.put(395,
						"Please be careful, you already have a job for the same vehicle. Work on the same positions will lead to errors.");
		labelHashMap
				.put(396,
						"You already have a job for the same vehicle on the same position.");
		labelHashMap.put(397, "This tyre has already been used in this job.");
		labelHashMap.put(398, "No tyres found");
		labelHashMap.put(399, "Search PW Tyres");
		labelHashMap
				.put(400,
						"Your device is running low on disk space. Please clear space or contact helpdesk");
		labelHashMap.put(401, "Action On Tire");
		labelHashMap.put(402, "Current Status");
		labelHashMap.put(403, "CustomerName");
		labelHashMap.put(404, "Data out of date");
		labelHashMap.put(405, "Do you want to save the unsaved job value?");
		labelHashMap.put(406, "Enter the Name");
		labelHashMap.put(407, "Enter the Number");
		labelHashMap.put(408, "Enter the Tread Depth");
		labelHashMap.put(409, "Password is not valid");
		labelHashMap.put(410, "Please scan tyre on position:");
		labelHashMap.put(411, "Please set correct date on the Mobile");
		labelHashMap.put(412, "Repair Company");
		labelHashMap
				.put(413,
						"RFID Tag not found, please try again or do the action manually");
		labelHashMap.put(414, "RFID Time Out");
		labelHashMap.put(415, "RFID Type");
		labelHashMap.put(416, "Seconds");
		labelHashMap.put(417, "Selected Tire List");
		labelHashMap.put(418, "Serial Number");
		labelHashMap
				.put(419,
						"Serial number is not correct. Do you want to do a automatic serial correction ?");
		labelHashMap.put(420, "Serial Number is required.");
		labelHashMap.put(421, "There are no PW tyres available");
		labelHashMap.put(422,
				"There are no vendors assign, please contact an administrator");
		labelHashMap
				.put(423,
						"This job is empty, No action are recorded. Do you want to Cancel this Job ?");
		labelHashMap.put(424, "Tire Management");
		labelHashMap
				.put(425,
						"Tyre Model is not correct. Do you want to do a automatic material correction ?");
		labelHashMap.put(426, "Work Order #");
		labelHashMap.put(427, "Work Order Number");
		labelHashMap.put(428,
				"You should select a Damage Group and Damage Area.");
		labelHashMap.put(429, "Your Search Result");
		// from 430 manual add
		labelHashMap.put(430, "Select Customer Name");//
		labelHashMap.put(431, "eJob Summary");
		labelHashMap.put(435, "Additional Services Summary");
		labelHashMap.put(207, "Regroove type is required.");
		labelHashMap.put(436, "Address");
		labelHashMap.put(437, "Apply");
		labelHashMap.put(438, "Completed Jobs");
		labelHashMap.put(439, "Deleted Jobs");
		labelHashMap.put(440, "eJob Summary");
		labelHashMap.put(441, "Fleet No.");
		labelHashMap.put(442, "Front");
		labelHashMap.put(443, "Incomplete Jobs");
		labelHashMap.put(445, "Lat");
		labelHashMap.put(446, "Launch New eJob");
		labelHashMap.put(447, "Long");
		labelHashMap.put(448, "New Recommended");
		labelHashMap.put(449, "NM");
		labelHashMap.put(450, "Rear");
		labelHashMap.put(451, "Rec. Inflaction");
		labelHashMap.put(452, "WP");
		labelHashMap.put(453, "Please select reason for no ODO meter");
		labelHashMap.put(454, "Signature Form info saved successfully...");
		labelHashMap.put(495,
				"Finshed Time must be greater than starts time!!!!");
		labelHashMap.put(457, "Bluetooth connection failed");
		labelHashMap.put(463, "Damage Notes is mandatory");
		labelHashMap
				.put(467,
						"Multiple paired devices found, please unpair which are not used");
		labelHashMap.put(469, "No Network");
		labelHashMap.put(470, "No paired devices");
		labelHashMap.put(471, "NSK1 & NSK3 values are interchanged.");
		labelHashMap.put(472, "Please pair a TLOGIK device");
		labelHashMap.put(477, "Your device does not support Bluetooth");
		labelHashMap.put(480, "Mount PWT Tire Notes");
		labelHashMap.put(481, "Swap Notes");
		labelHashMap.put(482, "Regroove Notes");
		labelHashMap.put(483, "TOR Notes");
		labelHashMap.put(487, "Tire History");
		labelHashMap.put(488, "Dismount Tire Notes");
		labelHashMap.put(489, "Mount New Tire Notes");
		labelHashMap.put(490, "Please select a Brand");
		labelHashMap.put(492, "Please sign the signature form or select reason if not signed !!");
		labelHashMap.put(499, "Select Brand");
		labelHashMap.put(810000,
				"<div Style=\" background-color: rgb(212, 219, 221)\"><div align=\"center\" >");
		labelHashMap.put(810001,
				"<h2 Style=\"background-color: rgb(18, 132, 202)\"><u>|820000|</u></h2></div><div Style=\"margin:3px\"><p>|820001|<br>|820002|</p><br>");
		labelHashMap.put(810002,
				"<h4>|820003|</h4><ul><li>|820004|</li><li>|820005|</li><li>|820006|</li></ul><br>");
		labelHashMap.put(810003,
				"<h4>|820007|</h4><ul><li>|200|</li><li>|257|</li><li>|256|</li></ul><br>");
		labelHashMap.put(810004,
				"<h4>|820008|</h4><ul><li>|820009|</li><li>|820010|</li><li>|820011|</li><li>|820012|</li><li>|820013|</li><li>|820014|</li><li>|820015|</li><li>|820016|</li><li>|820017|</li><li>|820018|</li><li>|820019|</li><li>|820020|</li></ul></div></div>");

		labelHashMap.put(820000, "Goodyear eJob - about this application");
		labelHashMap.put(820001,
				"Dear Service Provider, welcome to our FleetOnlineSolutions (FOS) eJob mobile application.");
		labelHashMap.put(820002,
				"We provide you with a wide range of mobile vehicle / tyre based services");
		labelHashMap.put(820003, "Vehicle actions");
		labelHashMap.put(820004, "Job Recording - Regular service");
		labelHashMap.put(820005, "Job Recording - Breakdown service");
		labelHashMap.put(820006, "Inspection - (later version)");
		labelHashMap.put(820007, "Tyre management actions");
		labelHashMap.put(820008, "The following languages are supported in Goodyear eJob:");
		labelHashMap.put(820009, "Czech");
		labelHashMap.put(820010, "Danish");
		labelHashMap.put(820011, "German");
		labelHashMap.put(820012, "English");
		labelHashMap.put(820013, "Spanish");
		labelHashMap.put(820014, "French");
		labelHashMap.put(820015, "Italian");
		labelHashMap.put(820016, "Dutch");
		labelHashMap.put(820017, "Norwegian");
		labelHashMap.put(820018, "Polish");
		labelHashMap.put(820019, "Swedish");
		labelHashMap.put(820020, "Turkish");

		labelHashMap.put(830000,
				"<div Style=\" background-color: rgb(212, 219, 221)\"><div align=\"center\" ><h2 Style=\"background-color: rgb(18, 132, 202)\"><u>|840000|</u></h2></div><div Style=\"margin:3px\">");
		labelHashMap
				.put(830001,
						"<h4>|840001|</h4><ul><li>|840002|</li><ul><li>|840003|</li></ul></ul><ul><li>|840004|</li><ul><li>|840005|</li></ul></ul>");
		labelHashMap
				.put(830002,
						"<ul><li>|840006|</li><ul><p>|840007|</p><ul><li>|840008|</li><li>|840009|</li><li>|840010|</li><li>|840011|</li><li>|840012|</li></ul></ul></ul>");
		labelHashMap
				.put(830003,
						"<ul><li>|840013|</li><ul><li>|840014|</li><li>|840015|</li><li>|840016|</li></ul></ul><ul><li>|840017|</li><ul><li>|840018|</li></ul></ul>");
		labelHashMap
				.put(830004,
						"<ul><li>|840019|</li><ul><li>|332|</li></ul></ul><ul><li>|840020|</li><ul><li>|840021|</li></ul></ul><ul><li>|840022|</li><ul><li>|840023|</li><li>|840024|</li></ul></ul>");
		labelHashMap
				.put(830005,
						"<ul><li>|840025|</li><ul><li>|840026|</li></ul></ul><ul><li>|840027|</li><ul><li>|840028|</li></ul></ul><ul><li>|840029|</li><ul><li>|840030|</li><li>|840031|</li></ul></ul></div></div>");

		labelHashMap.put(840000,
				"Goodyear eJob - FAQ - Frequently asked questions");
		labelHashMap.put(840001, "Goodyear eJob - FAQ");
		labelHashMap.put(840002, "what is eJob?");
		labelHashMap
				.put(840003,
						"eJob is a mobile tyre husbandry application designed to work with FleetOnlineSolutions. Only vehicles managed using FOS can currently be accessed");
		labelHashMap.put(840004, "How do I get access to eJob?");
		labelHashMap
				.put(840005,
						"eJob is currently available for Android devices only via the Google Play store. eJob is designed to work with Android 4.0 and higher");
		labelHashMap.put(840006, "What devices are supported?");
		labelHashMap
				.put(840007,
						"In theory, any device with Android 4.0 or higher should work. Goodyear has tested the latest version on the following devices and cannot guarantee it will work without issue on others");
		labelHashMap.put(840008, "Google Nexus 7 3g (gen1)");
		labelHashMap.put(840009, "Google Nexus 7 3/4g (gen2 2012)");
		labelHashMap.put(840010, "Google Nexus 10 3g");
		labelHashMap.put(840011, "Samsung Galaxy Tab 2 3g 7\"");
		labelHashMap.put(840012, "Samsung Galaxy Tab 2 3g 10\"");
		labelHashMap.put(840013, "Are there any special device requirements?");
		labelHashMap
				.put(840014,
						"The eJob is an application designed to work &apos;by the side of the truck&apos; and therefore a connection to the internet is essential whilst &apos;in the field&apos;");
		labelHashMap
				.put(840015,
						"The eJob allows the user to take photographs of the tyre(s) removed. A rear facing camera is seriously recommended");
		labelHashMap
				.put(840016,
						"The eJob is designed to optionally work with Bluetooth tread depth and Pressure collection tools, a Bluetooth connection is desirable");
		labelHashMap.put(840017, "How can I get access to login to the eJob?");
		labelHashMap.put(840018,
				"You&apos;ll need a Goodyear GoPass to enter the eJob");
		labelHashMap.put(840019, "Can I add my own fleets to eJob?");
		labelHashMap.put(840020, "Where do i find the eJobs?");
		labelHashMap.put(840021, "Your eJobs will be available in FOS.");
		labelHashMap.put(840022,
				"Can more than one person use eJob on the same device?");
		labelHashMap
				.put(840023,
						"Yes, to change user log out and back in again using the new user&apos;s ID");
		labelHashMap
				.put(840024,
						"The &apos;other&apos; user(s) saved jobs will be retained until they log back in and synchronise");
		labelHashMap.put(840025, "What happens if I do not sync my jobs?");
		labelHashMap
				.put(840026,
						"They will not be processed through FOS and you will not get any payments due");
		labelHashMap.put(840027, "Do I have to use eJob?");
		labelHashMap.put(840028, "Hell yes");
		labelHashMap.put(840029,
				"Who are the genius&apos; who made eJob happen?");
		labelHashMap.put(840030,
				"The greatest human being to work on ejob is Stuart Budden");
		labelHashMap
				.put(840031,
						"Michael Lambe, Hubert Lepoint, Leigh-Ann Scheepers and Sonata all played a minor role");
*/
		return labelHashMap;
	}

}
