package com.goodyear.ejob.util;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.goodyear.ejob.R;


/**
 * Created by Rajesh Batth on 10/10/2014.
 */
public class GYProgressDialog extends Dialog {

    private ImageView mImageView;

    public GYProgressDialog(Context context) {
        super(context, R.style.TransparentProgressDialog);
        init(context);
    }

    private void init(Context context) {
        try {
			WindowManager.LayoutParams windowLayoutParams = getWindow().getAttributes();
			windowLayoutParams.gravity = Gravity.CENTER_HORIZONTAL;
			getWindow().setAttributes(windowLayoutParams);
			setTitle(null);
			setCancelable(false);
			LinearLayout layout = new LinearLayout(context);
			layout.setOrientation(LinearLayout.VERTICAL);
			LayoutParams params = new LayoutParams(MATCH_PARENT, WRAP_CONTENT);
			mImageView = new ImageView(context);
			mImageView.setImageResource(R.drawable.frame000);
			layout.addView(mImageView, params);
			addContentView(layout, params);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public void show() {
    
    	try {
			super.show();
			RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
			anim.setInterpolator(new LinearInterpolator());
			anim.setRepeatCount(Animation.INFINITE);
			anim.setDuration(2000);
			mImageView.setAnimation(anim);
			mImageView.startAnimation(anim);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

	@Override
	public void dismiss() {
		try {
			super.dismiss();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
