package com.goodyear.ejob.util;

import java.util.ArrayList;
import java.util.HashMap;

public class TyreConstants {
	public static String AccountID = "AA15882";
	public static String TyreStatus;
	public static String CountryCode;
	public static String serialNumberSearchResult = null;

	public static ArrayList<HashMap<String, String>> Bundle_ListItems = new ArrayList<HashMap<String, String>>();

	public static ArrayList<HashMap<String, String>> Bundle_Search_TM_ListItems = new ArrayList<HashMap<String, String>>();

	public static ArrayList<HashMap<String, String>> Bundle_Search_TM_ListItems_Temporary = new ArrayList<HashMap<String, String>>();
	
	/**
	 * @return the bundle_Search_TM_ListItems
	 */
	public static ArrayList<HashMap<String, String>> getBundle_Search_TM_ListItems() {
		return Bundle_Search_TM_ListItems;
	}

	/**
	 * @param bundle_Search_TM_ListItems the bundle_Search_TM_ListItems to set
	 */
	public static void setBundle_Search_TM_ListItems(
			ArrayList<HashMap<String, String>> bundle_Search_TM_ListItems) {
		Bundle_Search_TM_ListItems = bundle_Search_TM_ListItems;
	}

	/**
	 * @return the bundle_ListItems
	 */
	public static ArrayList<HashMap<String, String>> getBundle_ListItems() {
		return Bundle_ListItems;
	}

	/**
	 * @param bundle_ListItems the bundle_ListItems to set
	 */
	public static void setBundle_ListItems(
			ArrayList<HashMap<String, String>> bundle_ListItems) {
		Bundle_ListItems = bundle_ListItems;
	}

	/**
	 * @return the serialNumberSearchResult
	 */
	public static String getSerialNumberSearchResult() {
		return serialNumberSearchResult;
	}

	/**
	 * @param serialNumberSearchResult the serialNumberSearchResult to set
	 */
	public static void setSerialNumberSearchResult(
			String serialNumberSearchResult) {
		TyreConstants.serialNumberSearchResult = serialNumberSearchResult;
	}

	/**
	 * @return the designSearchResult
	 */
	public static String getDesignSearchResult() {
		return designSearchResult;
	}

	/**
	 * @param designSearchResult the designSearchResult to set
	 */
	public static void setDesignSearchResult(String designSearchResult) {
		TyreConstants.designSearchResult = designSearchResult;
	}

	public static String designSearchResult = null;

}
