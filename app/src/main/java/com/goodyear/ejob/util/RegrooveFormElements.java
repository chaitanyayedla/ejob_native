package com.goodyear.ejob.util;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shailesh.p on 11/19/2014.
 */
public class RegrooveFormElements implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public RegrooveFormElements createFromParcel(Parcel in) {
            return new RegrooveFormElements(in);
        }

        public RegrooveFormElements[] newArray(int size) {
            return new RegrooveFormElements[size];
        }
    };
    private int brand;
    private int design;
    private int type;
    private String afterNsk1;
    private String afterNsk2;
    private String afterNsk3;
    private String beforeNsk1;
    private String beforeNsk2;
    private String beforeNsk3;
    private String pressure;
    private int regrooveType;

    public RegrooveFormElements(Parcel in) {
        this.brand = in.readInt();
        this.design = in.readInt();
        this.type = in.readInt();
        this.afterNsk1 = in.readString();
        this.afterNsk2 = in.readString();
        this.afterNsk3 = in.readString();
        this.beforeNsk1 = in.readString();
        this.beforeNsk2 = in.readString();
        this.beforeNsk3 = in.readString();
        this.pressure = in.readString();
        this.regrooveType = in.readInt();
    }

    public int getRegrooveType() {
        return regrooveType;
    }

    public void setRegrooveType(int regrooveType) {
        this.regrooveType = regrooveType;
    }

    public RegrooveFormElements() {

    }

    public int getBrand() {
        return brand;
    }

    public void setBrand(int brand) {
        this.brand = brand;
    }

    public int getDesign() {
        return design;
    }

    public void setDesign(int design) {
        this.design = design;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAfterNsk1() {
        return afterNsk1;
    }

    public void setAfterNsk1(String afterNsk1) {
        this.afterNsk1 = afterNsk1;
    }

    public String getAfterNsk2() {
        return afterNsk2;
    }

    public void setAfterNsk2(String afterNsk2) {
        this.afterNsk2 = afterNsk2;
    }

    public String getAfterNsk3() {
        return afterNsk3;
    }

    public void setAfterNsk3(String afterNsk3) {
        this.afterNsk3 = afterNsk3;
    }

    public String getBeforeNsk1() {
        return beforeNsk1;
    }

    public void setBeforeNsk1(String beforeNsk1) {
        this.beforeNsk1 = beforeNsk1;
    }

    public String getBeforeNsk2() {
        return beforeNsk2;
    }

    public void setBeforeNsk2(String beforeNsk2) {
        this.beforeNsk2 = beforeNsk2;
    }

    public String getBeforeNsk3() {
        return beforeNsk3;
    }

    public void setBeforeNsk3(String beforeNsk3) {
        this.beforeNsk3 = beforeNsk3;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.brand);
        dest.writeInt(this.design);
        dest.writeInt(this.type);
        dest.writeString(this.afterNsk1);
        dest.writeString(this.afterNsk2);
        dest.writeString(this.afterNsk3);
        dest.writeString(this.beforeNsk1);
        dest.writeString(this.beforeNsk2);
        dest.writeString(this.beforeNsk3);
        dest.writeString(this.pressure);
        dest.writeInt(this.regrooveType);
    }
}
