
package com.goodyear.ejob.util;
/**
 * Clear all Constants values
 */
public class ClearValues {
	
	/**
	 * Clear all constants values 
	 */
	public static void clearValues() {
				
		Constants.JOBTYPEREGULAR = false;
		Constants.JOBTYPEBREAKDOWN = false;
		Constants.GEMOTRY_CHECK_CORRECTION = false;
		// dismount screen		
		Constants.ONSTATE_DISMOUNT = false;
		Constants.ISBREAKSPOT = false;
		Constants.SELECTED_DAMAGE_TYPE = "";
		Constants.SELECTED_DAMAGE_AREA = "";
		Constants.DAMAGE_NOTES1 = "";
		Constants.DAMAGE_NOTES2 = "";
		Constants.DAMAGE_NOTES3 = "";


		Constants.vehicleConfiguration = "";
		Constants.DIFFERENT_SIZE_ALLOWED = false;
		Constants.tireJson = "";

		Constants.JOB_RESULT_LISTSIZE = 0;
		Constants.JOBREFNUMBERVALUE = 0;
		Constants.GETJOBREFFNO = true;
		
		Constants._SAPJSONVAl = "" ;
		Constants.SAVEDJOBREFNO = "";
		if(null != Constants._mBundleActionTypeVec) {
			Constants._mBundleActionTypeVec.clear();
		}
		
		if(null != Constants._mBundleOperationIDVec) {
			Constants._mBundleOperationIDVec.clear();
		}
		
		if(null != Constants._repairCompVec) {
			Constants._repairCompVec.clear();
		}
		
		if(null != Constants._workOrderVec) {
			Constants._workOrderVec.clear();
		}
		
		if(null != Constants._mBundleCurrentstatusVec) {
			Constants._mBundleCurrentstatusVec.clear();
		}
		
		if(null != Constants.mBundleSerielNOVec) {
			Constants.mBundleSerielNOVec.clear();
		}
		
		if(null != Constants._tireIDVec) {
			Constants._tireIDVec.clear();
		}
		
		if(null != Constants.mBundleDescOperationCodeVec) {
			Constants.mBundleDescOperationCodeVec.clear();
		}
		
		if(null != Constants.mBundleOperationCodeVec) {
			Constants.mBundleOperationCodeVec.clear();
		}
		
		if(null != Constants.mBundleFinalTyreStatusVec) {
			Constants.mBundleFinalTyreStatusVec.clear();
		}
		
		if(null != Constants._tireIDVec_Pos) {
			Constants._tireIDVec_Pos.clear();
		}
		
		if(null != Constants.SELECTED_DAMAGE_TYPE_LIST) {
			Constants.SELECTED_DAMAGE_TYPE_LIST.clear();
		}
		
		if(null != Constants.SELECTED_DAMAGE_AREA_LSIT) {
			Constants.SELECTED_DAMAGE_AREA_LSIT.clear();
		}	
		
		Constants.FROM_TM_OPERATION = false;
		Constants.jsonContractVal = "";
		Constants.jsonSAPContractVal = "";
		Constants.STATUSVALUES = "";
		Constants.COMESFROMUPDATE = false;
		Constants.COMESFROMVIEW = false;
		Constants.GETSELECTEDLISTPOSITION = 0;
		Constants.VEHICLEIDFORCONFIG = "";
		Constants.EDIT_JOB = false;

		// vehicle skeleton
		if(null != Constants.JOB_ITEMLIST) {
			Constants.JOB_ITEMLIST.clear();
		}
		
		if(null != Constants.TIRE_INFO) {
			Constants.TIRE_INFO.clear();
		}
		
		if(null != Constants.JOB_CORRECTION_LIST) {
			Constants.JOB_CORRECTION_LIST.clear();
		}
		
		if(null != Constants.JOB_TIREIMAGE_ITEMLIST) {
			Constants.JOB_TIREIMAGE_ITEMLIST.clear();
		}
		
		if(null != Constants.TORQUE_SETTINGS_ARRAY) {
			Constants.TORQUE_SETTINGS_ARRAY.clear();
		}
		// additional services
		if(null != Constants.servicesEdit) {
			Constants.servicesEdit.clear();
		}
		
		if(null != Constants.Bundle_Retained_Jobitem) {
			Constants.Bundle_Retained_Jobitem.clear();
		}
		
		Constants.BRAND_NAME = "";
		Constants.sourceTyrePosition = "";
		Constants.targetTyrePosition = "";
		Constants.NSK1_SWAP = "";
		Constants.NSK2_SWAP = "";
		Constants.NSK3_SWAP = "";
		Constants.CURRENT_SERIAL_NUMBER = "";
		Constants.EDITED_SERIAL_NUMBER = "";// DES_PRESSURE_CAPTURED
		Constants.SOURCE_PRESSURE_CAPTURED = "";
		Constants.DES_PRESSURE_CAPTURED = "";
		Constants.CURRENT_TYRE = null;
		Constants.EDITED_SERIAL_NUMBER_FIRST_SELECTED = "";
		Constants.EDITED_SERIAL_NUMBER_SECOND_TYRE = "";
		Constants.EDITED_SERIAL_NUMBER_SOURCE = "";
		Constants.EDITED_SERIAL_NUMBER_DES = "";
		Constants.SELECTED_TYRE = null;
		Constants.SECOND_SELECTED_TYRE = null;
		Constants.ONDRAG = false;
		Constants.ONPRESS = false;
		Constants.DOUBLESWAP = false;
		Constants.TRIPLESWAP = false;
		Constants.SOURCE_TYRE = null;
		Constants.DES_TYRE = null;
		Constants.SELECTED_TYRE_SerialNumber= "";
		Constants.SELECTED_TYRE_setNsk= "";
		Constants.SELECTED_TYRE_setNsk2= "";
		Constants.SELECTED_TYRE_setNsk3= "";
		Constants.SELECTED_TYRE_setVehicleJob= "";
		Constants.SELECTED_TYRE_setRimType= "";
		Constants.SELECTED_TYRE_setBrandName= "";
		Constants.SELECTED_TYRE_setSize= "";
		Constants.SELECTED_TYRE_setAsp= "";
		Constants.SELECTED_TYRE_setRim= "";
		Constants.SELECTED_TYRE_setDesign= "";
		Constants.SELECTED_TYRE_setDesignDetails= "";
		Constants.SELECTED_TYRE_setAxleRecommendedPressure = null;
		Constants.onReturnBool = false;
		Constants.onGOBool = false;
		Constants.onTouchBool = false;
		Constants.onSwapOptionsBool = false;
		Constants.onGOBoolLogical = false;
		Constants.onBrandBool = false;
		Constants.onDragPSBool = false;
		// public static boolean onBrandDesignBool = false;
		Constants.onDRAGReturnBoolPS = false;
		Constants.onDragBool_LS = false;// INSERT_SWAP_DATE
		Constants.DO_PHYSICAL_SWAP = false;// Constants.TRIGGER_LOGICAL
		Constants.CHECK_LOGICAL_SWAP = false;
		Constants.triggerMultipleEdit = 0;
		Constants.triggerMultipleSwap = 0;
		Constants.checkSingleSwap = 0;
		Constants.ONLONG_PRESS = false;
		Constants.SAPCONTRACTNUMBER = "";
		Constants.SAP_CONTRACTNUMBER_ID ="";
		Constants.NEW_SWAP_POSITION = "";
		Constants.TRIGGER_LOGICAL = false;
		Constants.INSERT_SWAP_DATA = 0;
		Constants.firstTireAXLE = "";
		Constants.seccondTireAXLE = "";
		Constants.SAP_VENDORCODE_ID = "";
		Constants.VENDORCOUNTRY_ID = "";
		Constants.VENDORSAPCODE = "";
		Constants.UPDATESTATUSWHILESWIPE = false;
		Constants.JOBREFNUMPASSFORNEWJOB = 0;
		Constants.JOBREFNUMPASSFROMEJOB = 0;
		Constants.contract = null;
		if(null != Constants.torqueSettingsIDS) {
			Constants.torqueSettingsIDS.clear();
		}
		Constants.VENDER_NAME = "";
		Constants.TYRE_ID = "";
		Constants.ONSTATE_TOR = false;
		Constants.STATUSONSELECTEDLIST = "";
		Constants.UPDATE_SERIAL_DB = false;
		Constants.CURRENTDATE = "";
		Constants.CURRENTIME = "";
		Constants.EJOBHEADERVALUESIFDBNULL = true;
		Constants.UPDATE_APPLIED_ON_EDIT = false;
		Constants.FROM_EJOBLIST = false;
		Constants.jobIdEdit = "";
		Constants.CHEKIFPICTAKENORNOT = false;
		Constants.DELETEICONCHANGE = false;
		Constants.DELETEDPOSITION = 0;
		Constants.GPS_LOCK = false;
		Constants.TIME_LOCATION_SET = false;
		Constants.ALERTFORNONETWORKWHILESYNC = false;

		// Amit SERIAL_NUMBER_TM
		if(null != Constants.Search_TM_ListItems) {
			Constants.Search_TM_ListItems.clear();
		}		
		Constants.SAP_CUSTOMER_ID = "";
		if(null != Constants.REGROOVE_TYPE_TM) {
			Constants.REGROOVE_TYPE_TM.clear();
		}
		if(null != Constants.THREAD_DEPTH_TM) {
			Constants.THREAD_DEPTH_TM.clear();
		}		
		Constants.TM_CAMERA_ICON_CLICKED = 0;
		Constants.SERIAL_NUMBER_TM = "";
		Constants.CURRENT_STATUS_TM = "";
		Constants.IsREGROOVED_TM = false;
		Constants.TM_CAMERA_ICON_CLICKED_FLAG = false;
		Constants.bitmapQualityMap.clear();
		if(null != Constants.damageImage_TM_LIST_Bitmap1) {
			Constants.damageImage_TM_LIST_Bitmap1.clear();
		}
		if(null != Constants.damageImage_TM_LIST_Bitmap2) {
			Constants.damageImage_TM_LIST_Bitmap2.clear();
		}
		if(null != Constants.damageImage_TM_LIST_Bitmap3) {
			Constants.damageImage_TM_LIST_Bitmap3.clear();
		}

		if(null != Constants.damageNote_TM_LIST1) {
			Constants.damageNote_TM_LIST1.clear();
		}
		if(null != Constants.damageNote_TM_LIST2) {
			Constants.damageNote_TM_LIST2.clear();		
		}
		if(null != Constants.damageNote_TM_LIST3) {
			Constants.damageNote_TM_LIST3.clear();
		}
		
		Constants.NSK_SET_WOT = "";
		Constants.PRESSURE_SET_WOT = "";
		Constants.CLICKED_SERIAL_CONFIRM = false;
		Constants.DO_LOGICAL_SWAP = false;
		Constants.FULLDESIGN_SELECTED = "";
		Constants.NEW_SAPMATERIAL_CODE = "";// UNDO_STATE
		Constants.SAP_CUSTOMER_ID_JOB = "";
		Constants.UNDO_STATE = false;
		Constants.ASCDESC = true;
		Constants.CAMIMAGEENABLE = false;		
		Constants.CAMERAIMAGECLICKENABLE = false;
		Constants.SINGLESWIPETODELETEEJOBLIST = false;
		Constants.SINGLESWIPETODELETETM = false;
		Constants.SINGLESWIPETODELETENEWJOB = false;
		Constants.SINGLESWIPETODELETEBACKBUTT = false;
		Constants.SAVEEXTDBWHENAPPSAVE = true;
		Constants.TM_NOT_ALLOWED = false;
		Constants.RETAIN_BITMAP = null;// ONSTATE_REGROOVE
		Constants.ONSTATE_REGROOVE = false;		
		Constants.LOGICAL_SWAP_INITIAL = false;
		Constants.tempTyreSerial = "";
		Constants.tempTyrePosition = ""; // OnSerialBool
		Constants.tempTyrePositionNew = "";
		Constants.onBrandDesignBool = false;
		Constants.OnSerialBool = false;
	}
}
