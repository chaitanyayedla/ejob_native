package com.goodyear.ejob.util;

import java.io.Serializable;

import com.goodyear.ejob.job.operation.helpers.LogicalSwapInfo;
import com.goodyear.ejob.job.operation.helpers.PhysicalSwapInfo;
import com.goodyear.ejob.job.operation.helpers.TurnOnRimHelper;

/**
 * @author shailesh.p
 */
public class Tyre implements Serializable, Comparable<Tyre> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String serialNumber = "";
    private String size = "";
    private String rim = "";
    private String rimType = "";
    private String brandName = "";
    private String sapCodeWP = "";
    private String sapCodeTI = "";
    private String design = "";
    private String designDetails = "";
    private String sapMaterialCodeID = "";
    private String sapContractNumberID = "";
    private String sapVenderCodeID = "";
    private String externalID = "";
    private String nsk = "";
    private String mActionType = "";
    private String mIsUnmounted = "";

    /**
     * Ejob-36 (738) - Dismounted tyre with reusable reason will be available as PWT only after sync.
     * If material and/or SN is changed when a tyre is removed - this tyre should not be made available immediately
     * Maintain temporary SAPVENDERCodeID until SAP updates
     * Since SAPVENDERCodeID for few tyres are null, so by maintaining temporary SAPVENDERCodeID, will make PWT tyre with SAPVENDERCodeID as null, available for Reserve PWT.
     * Setter and getter added for IsTyreCorrected and TempSAPVendorCodeID
     */
    private String IsTyreCorrected="0";
    private String TempSAPVendorCodeID="";

    public String getIsTyreCorrected() {
        return IsTyreCorrected;
    }

    public void setIsTyreCorrected(String isTyreCorrected) {
        IsTyreCorrected = isTyreCorrected;
    }

    public String getTempSAPVendorCodeID() {
        return TempSAPVendorCodeID;
    }

    public void setTempSAPVendorCodeID(String tempSAPVendorCodeID) {
        TempSAPVendorCodeID = tempSAPVendorCodeID;
    }



    /**
     * @return the mIsUnmounted
     */
    public String getmIsUnmounted() {
        if (mIsUnmounted == "") {
            return "0";
        }
        return mIsUnmounted;
    }

    /**
     * @param mIsUnmounted the mIsUnmounted to set
     */
    public void setmIsUnmounted(String mIsUnmounted) {
        this.mIsUnmounted = mIsUnmounted;
    }

    /**
     * API used to set the action type for the tyre
     *
     * @param actionType
     */
    public void setActionType(String actionType) {
        mActionType = actionType;
    }

    /**
     * Used to get the action type for selected tyre.
     *
     * @return
     */
    public String getActionType() {
        return mActionType;
    }

    /**
     * @return the vehicleJob
     */
    public String getVehicleJob() {
        return vehicleJob;
    }

    /**
     * @param vehicleJob the vehicleJob to set
     */
    public void setVehicleJob(String vehicleJob) {
        this.vehicleJob = vehicleJob;
    }

    private String nsk2;
    private String nsk3;
    private String position;
    private String originalPosition;

    public boolean isInspected() {
        return isInspected;
    }

    public void setIsInspected(boolean isInspected) {
        this.isInspected = isInspected;
    }

    private String pressure;
    private String isSpare;
    private RegrooveTyre regrooveInfo;
    private boolean isReGrooved;
    private boolean isRetorqued;
    private boolean isphysicalySwaped;
    private boolean isLogicalySwaped;
    private boolean isInspected;
    private boolean isInspectedWithOperation;

    /**
     * @param isInspectedWithOperation
     */
    public void setInspectedWithOperation(boolean isInspectedWithOperation)
    {
        this.isInspectedWithOperation=isInspectedWithOperation;
    }

    /**
     * @return isInspectedWithOperation
     */
    public boolean getInspectedWithOperation()
    {
        return isInspectedWithOperation;
    }

    /**
     * @return the isLogicalySwaped
     */
    public boolean isLogicalySwaped() {
        return isLogicalySwaped;
    }

    /**
     * @param isLogicalySwaped the isLogicalySwaped to set
     */
    public void setLogicalySwaped(boolean isLogicalySwaped) {
        this.isLogicalySwaped = isLogicalySwaped;
    }

    private boolean hasMounted;

    /**
     * @return the hasMounted
     */
    public boolean isHasMounted() {
        return hasMounted;
    }

    /**
     * @param hasMounted the hasMounted to set
     */
    public void setHasMounted(boolean hasMounted) {
        this.hasMounted = hasMounted;
    }

    private boolean isDismounted;
    private boolean hasTurnedOnRim;
    private boolean isWorkedOn;
    private String status;
    private String active;
    private String shared;
    private String fromJobId;
    private String sapCodeAXID;
    private String orignalNSK;
    private String vehicleJob;
    private boolean isPWTMounted;

    /**
     * @return the orignalNSK
     */
    public String getOrignalNSK() {
        return orignalNSK;
    }

    /**
     * @param orignalNSK the orignalNSK to set
     */
    public void setOrignalNSK(String orignalNSK) {
        this.orignalNSK = orignalNSK;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the active
     */
    public String getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(String active) {
        this.active = active;
    }

    /**
     * @return the shared
     */
    public String getShared() {
        return shared;
    }

    /**
     * @param shared the shared to set
     */
    public void setShared(String shared) {
        this.shared = shared;
    }

    /**
     * @return the fromJobId
     */
    public String getFromJobId() {
        return fromJobId;
    }

    /**
     * @param fromJobId the fromJobId to set
     */
    public void setFromJobId(String fromJobId) {
        this.fromJobId = fromJobId;
    }

    /**
     * @return the sapCodeAXID
     */
    public String getSapCodeAXID() {
        return sapCodeAXID;
    }

    /**
     * @param sapCodeAXID the sapCodeAXID to set
     */
    public void setSapCodeAXID(String sapCodeAXID) {
        this.sapCodeAXID = sapCodeAXID;
    }

    /**
     * @return the sapCodeVeh
     */
    public String getSapCodeVeh() {
        return sapCodeVeh;
    }

    /**
     * @param sapCodeVeh the sapCodeVeh to set
     */
    public void setSapCodeVeh(String sapCodeVeh) {
        this.sapCodeVeh = sapCodeVeh;
    }

    /**
     * @return the hasTurnedOnRim
     */
    public boolean isHasTurnedOnRim() {
        return hasTurnedOnRim;
    }

    private String sapCodeVeh;
    private TurnOnRimHelper torInfo;
    private LogicalSwapInfo logicalSwapinfo;

    private PhysicalSwapInfo physicalSwapinfo;

    /**
     * @return the logicalSwapinfo
     */
    public LogicalSwapInfo getLogicalSwapinfo() {
        return logicalSwapinfo;
    }

    /**
     * @param logicalSwapinfo the logicalSwapinfo to set
     */
    public void setLogicalSwapinfo(LogicalSwapInfo logicalSwapinfo) {
        this.logicalSwapinfo = logicalSwapinfo;
    }

    /**
     * @return the physicalSwapinfo
     */
    public PhysicalSwapInfo getPhysicalSwapinfo() {
        return physicalSwapinfo;
    }

    /**
     * @param physicalSwapinfo the physicalSwapinfo to set
     */
    public void setPhysicalSwapinfo(PhysicalSwapInfo physicalSwapinfo) {
        this.physicalSwapinfo = physicalSwapinfo;
    }

    /**
     * @return the isphysicalySwaped
     */
    public boolean isIsphysicalySwaped() {
        return isphysicalySwaped;
    }

    /**
     * @param isphysicalySwaped the isphysicalySwaped to set
     */
    public void setIsphysicalySwaped(boolean isphysicalySwaped) {
        this.isphysicalySwaped = isphysicalySwaped;
    }

    /**
     * @return the isDismounted
     */
    public boolean isDismounted() {
        return isDismounted;
    }

    /**
     * @param isDismounted the isDismounted to set
     */
    public void setDismounted(boolean isDismounted) {
        this.isDismounted = isDismounted;
    }

    /**
     * @return the isPWTMounted
     */
    public boolean isPWTMounted() {
        return isPWTMounted;
    }

    /**
     * @param isPWTMounted the isPWTMounted to set
     */
    public void setPWTMounted(boolean isPWTMounted) {
        this.isPWTMounted = isPWTMounted;
    }

    /**
     * @return the isWorked
     */
    public boolean isWorkedOn() {
        return isWorkedOn;
    }

    /**
     * @param isDismounted the isDismounted to set
     */
    public void setWorkedOn(boolean isWorkedOn) {
        this.isWorkedOn = isWorkedOn;
    }

    private int tyreState;

    /**
     * @return the tyreState
     */
    public int getTyreState() {
        return tyreState;
    }

    /**
     * @param tyreState the tyreState to set
     */
    public void setTyreState(int tyreState) {
        this.tyreState = tyreState;
    }

    /**
     * @return the hasTurnedOnRim
     */
    public boolean hasTurnedOnRim() {
        return hasTurnedOnRim;
    }

    /**
     * @param hasTurnedOnRim the hasTurnedOnRim to set
     */
    public void setHasTurnedOnRim(boolean hasTurnedOnRim) {
        this.hasTurnedOnRim = hasTurnedOnRim;
    }

    /**
     * @return the nsk2
     */
    public String getNsk2() {
        return nsk2;
    }

    /**
     * @param nsk2 the nsk2 to set
     */
    public void setNsk2(String nsk2) {
        this.nsk2 = nsk2;
    }

    /**
     * @return the nsk3
     */
    public String getNsk3() {
        return nsk3;
    }

    /**
     * @param nsk3 the nsk3 to set
     */
    public void setNsk3(String nsk3) {
        this.nsk3 = nsk3;
    }

    /**
     * @return the torInfo
     */
    public TurnOnRimHelper getTorInfo() {
        if (torInfo == null) {
            return new TurnOnRimHelper();
        }
        return torInfo;
    }

    /**
     * @param torInfo the torInfo to set
     */
    public void setTorInfo(TurnOnRimHelper torInfo) {
        this.torInfo = torInfo;
    }

    /**
     * @return the isRetorqued
     */
    public boolean isRetorqued() {
        return isRetorqued;
    }

    /**
     * @param isRetorqued the isRetorqued to set
     */
    public void setRetorqued(boolean isRetorqued) {
        this.isRetorqued = isRetorqued;
    }

    /**
     * @return the isReGrooved
     */
    public boolean isReGrooved() {
        return isReGrooved;
    }

    /**
     * @param isReGrooved the isReGrooved to set
     */
    public void setReGrooved(boolean isReGrooved) {
        this.isReGrooved = isReGrooved;
    }

    /**
     * @return the regrooveInfo
     */
    public RegrooveTyre getRegrooveInfo() {
        if (regrooveInfo == null) {
            return new RegrooveTyre();
        }
        return regrooveInfo;
    }

    /**
     * @param regrooveInfo the regrooveInfo to set
     */
    public void setRegrooveInfo(RegrooveTyre regrooveInfo) {
        this.regrooveInfo = regrooveInfo;
    }

    private String asp = "";

    /**
     * @return the isSpare
     */
    public String getIsSpare() {
        return isSpare;
    }

    /**
     * @param isSpare the isSpare to set
     */
    public void setIsSpare(String isSpare) {
        this.isSpare = isSpare;
    }

    /**
     * @return the asp
     */
    public String getAsp() {
        return asp;
    }

    /**
     * @param asp the asp to set
     */
    public void setAsp(String asp) {
        this.asp = asp;
    }

    /**
     * @return the serialNumber
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * @param serialNumber the serialNumber to set
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * @return the rim
     */
    public String getRim() {
        return rim;
    }

    /**
     * @param rim the rim to set
     */
    public void setRim(String rim) {
        this.rim = rim;
    }

    /**
     * @return the rim type
     */
    public String getRimType() {
        return rimType;
    }

    /**
     * @param rimType the rimType to set
     */
    public void setRimType(String rimType) {
        this.rimType = rimType;
    }

    /**
     * @return the brandName
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * @param brandName the brandName to set
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    /**
     * @return the sapCodeWP
     */
    public String getSapCodeWP() {
        return sapCodeWP;
    }

    /**
     * @param sapCodeWP the sapCodeWP to set
     */
    public void setSapCodeWP(String sapCodeWP) {
        this.sapCodeWP = sapCodeWP;
    }

    /**
     * @return the sapCodeTI
     */
    public String getSapCodeTI() {
        return sapCodeTI;
    }

    /**
     * @param sapCodeTI the sapCodeTI to set
     */
    public void setSapCodeTI(String sapCodeTI) {
        this.sapCodeTI = sapCodeTI;
    }

    /**
     * @return the design
     */
    public String getDesign() {
        return design;
    }

    /**
     * @param design the design to set
     */
    public void setDesign(String design) {
        this.design = design;
    }

    /**
     * @return the designDetails
     */
    public String getDesignDetails() {
        return designDetails;
    }

    /**
     * @param designDetails the designDetails to set
     */
    public void setDesignDetails(String designDetails) {
        this.designDetails = designDetails;
    }

    /**
     * @return the sapMaterialCodeID
     */
    public String getSapMaterialCodeID() {
        return sapMaterialCodeID;
    }

    /**
     * @param sapMaterialCodeID the sapMaterialCodeID to set
     */
    public void setSapMaterialCodeID(String sapMaterialCodeID) {
        this.sapMaterialCodeID = sapMaterialCodeID;
    }

    /**
     * @return the sapContractNumberID
     */
    public String getSapContractNumberID() {
        return sapContractNumberID;
    }

    /**
     * @param sapContractNumberID the sapContractNumberID to set
     */
    public void setSapContractNumberID(String sapContractNumberID) {
        this.sapContractNumberID = sapContractNumberID;
    }

    /**
     * @return the sapVenderCodeID
     */
    public String getSapVenderCodeID() {
        return sapVenderCodeID;
    }

    /**
     * @param sapVenderCodeID the sapVenderCodeID to set
     */
    public void setSapVenderCodeID(String sapVenderCodeID) {
        this.sapVenderCodeID = sapVenderCodeID;
    }

    /**
     * @return the externalID
     */
    public String getExternalID() {
        return externalID;
    }

    /**
     * @param externalID the externalID to set
     */
    public void setExternalID(String externalID) {
        this.externalID = externalID;
    }

    /**
     * @return the nsk
     */
    public String getNsk() {
        return nsk;
    }

    /**
     * @param nsk the nsk to set
     */
    public void setNsk(String nsk) {
        this.nsk = nsk;
    }

    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * @return the original position
     */
    public String getOriginalPosition() {
        return originalPosition;
    }

    /**
     * @param set the original position to set
     */
    public void setOriginalPosition(String originalPosition) {
        this.originalPosition = originalPosition;
    }

    /**
     * @return the pressure
     */
    public String getPressure() {
        return pressure;
    }

    /**
     * @param pressure the pressure to set
     */
    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    /**
     * @return the isSpare
     */
    public String isSpare() {
        return isSpare;
    }

    /**
     * @param isSpare the isSpare to set
     */
    public void setSpare(String isSpare) {
        this.isSpare = isSpare;
    }

    /**
     * Get minimum NSK
     */
    public String getMinNSK() {
        return CommonUtils.getMinNskValue(nsk, nsk2, nsk3);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Tyre another) {
        int positive_limit = 1;
        int negative_limit = -1;
        String anotherTirePos = another.getPosition();
        String tirePosition = this.getPosition();
        //if second tire is a dismounted tire
        if (anotherTirePos.contains("$")) {
            anotherTirePos = anotherTirePos.substring(0,
                    anotherTirePos.length() - 1);
            //if both tires are at same position then place dismounted first
            int compareResult = tirePosition.compareTo(anotherTirePos);
            if (compareResult == 0) {
                return positive_limit;
            }
        }
        //if first tire is a dismounted tire
        if (tirePosition.contains("$")) {
            tirePosition = tirePosition.substring(0, tirePosition.length() - 1);
            //if both tires are at same position then place dismounted first
            int compareResult = anotherTirePos.compareTo(tirePosition);
            if (compareResult == 0) {
                return negative_limit;
            }
        }
        // compare both the tire on position
        return this.getPosition().compareTo(another.getPosition());
    }

}
