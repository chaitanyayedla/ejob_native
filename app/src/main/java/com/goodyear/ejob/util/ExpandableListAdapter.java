package com.goodyear.ejob.util;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.goodyear.ejob.R;

/**
 * @author johnmiya.s
 * 
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<String> _listDataHeader; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<String>> _listDataChild;
	private HashMap<String, List<String>> _listDataChild1;

	// private List<String> _listDataHeader1;

	public ExpandableListAdapter(Context context, List<String> listDataHeader,
			HashMap<String, List<String>> listChildData,
			HashMap<String, List<String>> listDataChild1) {
		this._context = context;

		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;
		this._listDataChild1 = listDataChild1;

	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);
	}

	/*
	 * public Object getChild1(int groupPosition, int childPosititon) {
	 * return this._listDataChild1.get(this._listDataHeader.get(groupPosition))
	 * .get(childPosititon);
	 * }
	 */

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final String childText = (String) getChild(groupPosition, childPosition);

		final String childText1 = _listDataChild1.get(
				this._listDataHeader.get(groupPosition)).get(childPosition);

		// final String childText1 = (String) getChild1(groupPosition,
		// childPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_item, null);
		}

		TextView txtListChild = (TextView) convertView
				.findViewById(R.id.lblListItem);
		TextView txtListChildvalues = (TextView) convertView
				.findViewById(R.id.lblListItemvalue);
		ImageView im = (ImageView) convertView.findViewById(R.id.expandbleCam);
		Log.i("Jaani", "childPosition:" + childPosition + "group:"
				+ groupPosition);
		if (childPosition == 4) {
			Log.i("Jaani", "inside childPosition:" + childPosition + "group:"
					+ groupPosition);
			if (Constants.CAMIMAGEENABLE == true
					&& Constants.SELECTEDGROUPPOSITION == groupPosition) {
				im.setVisibility(View.VISIBLE);
				Constants.CAMERAIMAGECLICKENABLE = true;
			} else if (Constants.CAMIMAGEENABLE == false
					&& Constants.SELECTEDGROUPPOSITION == groupPosition) {
				im.setVisibility(View.INVISIBLE);
				Constants.CAMERAIMAGECLICKENABLE = false;

			}
		} else {
			Log.i("Jaani", "outside childPosition:" + childPosition + "group:"
					+ groupPosition);
			im.setVisibility(View.INVISIBLE);
			Constants.CAMERAIMAGECLICKENABLE = false;

		}

		txtListChild.setText(childText);
		txtListChildvalues.setText(childText1);
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {

		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		String headerTitle = (String) getGroup(groupPosition);

		// String headerTitle1 = _listDataHeader1.get(groupPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);
		/*
		 * TextView lblListHeader1 = (TextView) convertView
		 * .findViewById(R.id.lblListgroupvalue);
		 */

		// lblListHeader1.setTypeface(null, Typeface.BOLD);
		lblListHeader.setTypeface(null, Typeface.BOLD);
		// lblListHeader1.setText(headerTitle1);
		lblListHeader.setText(headerTitle);

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
