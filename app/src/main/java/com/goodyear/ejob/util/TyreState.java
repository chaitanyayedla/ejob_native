package com.goodyear.ejob.util;

/**
 * @author shailesh.p
 * 
 */
public class TyreState {
	public static final int EMPTY = 0;
	public static final int NEW_TIRE = 1;
	public static final int PART_WORN = 2;
	public static final int NON_MAINTAINED = 3;
}
