package com.goodyear.ejob.util;

/**
 * Created by amitkumar.h on 25-11-2015.
 */
public class InspectionState {
    public static final int NO_INSPECTION_NO_OPERATION = 0;
    public static final int ONLY_INSPECTION_PARTIAL = 1;
    public static final int ONLY_INSPECTION_COMPLETE = 2;
    public static final int ONLY_OPERATION_COMPLETE = 3;
    public static final int ONLY_OPERATION_PARTIAL=4;
    public static final int OPERATION_WITH_INSPECTION_PARTIAL = 5;
    public static final int OPERATION_WITH_INSPECTION_COMPLETE = 6;
    public static final int EMPTY_DATA=7;
}
