/**copyright message*/
package com.goodyear.ejob.util;

import android.text.TextUtils;

import com.goodyear.ejob.R;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.job.operation.helpers.JobItem;

/**
 * Class used to get the proper tyre icon based on tyre state.
 * {@link #getTireIcon(Tyre, boolean)} should be called to get the tyre icon.
 * 
 * @author Nilesh.Pawate
 * 
 */
public class TireUtils {
	/**
	 * 
	 * @param tyre
	 *            Tyre object for which we need icon.
	 * @param isClicked
	 *            true if needed clicked icon, false otherwise.
	 * @return Image icon integer which can be set to the View.
	 */
	public static int getTireIcon(Tyre tyre, boolean isClicked) {
		int drawableId = R.drawable.normal_tire;
		int tireState = getTirestate(tyre);
		LogUtil.d("TireUtils", "Tyre Action Type is::: " + tireState);

		switch (tireState) {
		case 1:// Dismount
			if (TextUtils.isEmpty(tyre.getSerialNumber())) {// Non maintained
				if (isClicked) {
					drawableId = R.drawable.no_maintained_tire_s;
				} else {
					drawableId = R.drawable.no_maintained_tire;
				}
			} else if ("1".equalsIgnoreCase(tyre.getmIsUnmounted())
					&& !tyre.isHasMounted()) {
				if (isClicked) {
					drawableId = R.drawable.dismount_tire_s;
				} else {
					drawableId = R.drawable.dismount_tire;
				}
			} else if (isClicked) {
				drawableId = R.drawable.dismount_tire_s;
			} else {
				drawableId = R.drawable.dismount_tire;
			}
			break;

		case 2:// Mount
		case 25:// PWT Mount
		case 3:// PWT Mount
		case 4:// PWT Mount
		case 8:// PWT Mount

		case 9:// WOT
		case 10:// WOT
		case 12:// WOT
		case 13:// WOT
		case 14:// WOT
		case 15:// WOT

		case 16:// WOT
		case 17:// WOT
		case 18:// WOT
		case 19:// WOT
		case 20:// WOT
		case 21:// WOT
		case 22:// WOT
		case 23:// WOT
		case 26:// Inspection
			if (TextUtils.isEmpty(tyre.getSerialNumber())) {// Non maintained

				//Fix : New Icon for Non maintained tyre with Inspection
				if(tireState==26)
				{
					if (isClicked) {
					drawableId = R.drawable.no_maintained_with_inspection_tire_s;
				} else {
					drawableId = R.drawable.no_maintained_with_inspection_tire;
				}
				}
				else {
					if (isClicked) {
						drawableId = R.drawable.no_maintained_tire_s;
					} else {
						drawableId = R.drawable.no_maintained_tire;
					}
				}
			} else if ("1".equalsIgnoreCase(tyre.getShared())
					&& !tyre.isHasMounted()) {
				if (isClicked) {
					drawableId = R.drawable.dismount_tire_s;
				} else {
					drawableId = R.drawable.dismount_tire;
				}
			} else if (isClicked) {
				drawableId = R.drawable.worked_wheel_s;
			} else {
				drawableId = R.drawable.worked_wheel;
			}
			break;

		case -1:
			if (TextUtils.isEmpty(tyre.getSerialNumber())) {// Non maintained
				if (isClicked) {
					drawableId = R.drawable.no_maintained_tire_s;
				} else {
					drawableId = R.drawable.no_maintained_tire;
				}
			} else if ("1".equalsIgnoreCase(tyre.getShared())
					&& !tyre.isHasMounted()) {
				if (isClicked) {
					drawableId = R.drawable.dismount_tire_s;
				} else {
					drawableId = R.drawable.dismount_tire;
				}
			} else { // normal tire
				if (isClicked) {
					drawableId = R.drawable.selected_tire;
				} else {
					drawableId = R.drawable.normal_tire;
				}
			}
			break;

		default:
			if (TextUtils.isEmpty(tyre.getSerialNumber())) {// Non maintened
				if (isClicked) {
					drawableId = R.drawable.no_maintained_tire_s;
				} else {
					drawableId = R.drawable.no_maintained_tire;
				}
			} else if ("1".equalsIgnoreCase(tyre.getShared())
					&& !tyre.isHasMounted()) {
				if (isClicked) {
					drawableId = R.drawable.dismount_tire_s;
				} else {
					drawableId = R.drawable.dismount_tire;
				}
			} else { // normal tire
				if (isClicked) {
					drawableId = R.drawable.selected_tire;
				} else {
					drawableId = R.drawable.normal_tire;
				}
			}
			break;
		}
		return drawableId;
	}

	/**
	 * Private API used to get the tire state.
	 * 
	 * @param tyre
	 *            Tyre object
	 * @return tyre state.
	 */
	private static int getTirestate(Tyre tyre) {
		if (Constants.JOB_ITEMLIST == null) {
			Constants.JOB_ITEMLIST = VehicleSkeletonFragment.mJobItemList;
		}
		if (Constants.JOB_ITEMLIST == null) {
			return -1;
		}
		for (int i = Constants.JOB_ITEMLIST.size() - 1; i >= 0; i--) {
			JobItem jobItem = Constants.JOB_ITEMLIST.get(i);
			if (jobItem == null)
				continue;
			if (tyre != null
					&& tyre.getExternalID().equals(jobItem.getTyreID())) {
				String actionType = jobItem.getActionType();
				if (tyre.getTyreState() == TyreState.EMPTY
						&& (tyre.isIsphysicalySwaped() || tyre
								.isLogicalySwaped())) {
					return Integer.parseInt("1");
				} else if (!TextUtils.isEmpty(actionType)) {
					return Integer.parseInt(jobItem.getActionType());
				}
			}
		}
		return -1;
	}

}
