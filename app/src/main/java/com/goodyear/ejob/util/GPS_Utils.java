package com.goodyear.ejob.util;

import java.util.List;

import com.goodyear.ejob.R;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class GPS_Utils extends Service implements LocationListener {
	public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;

	public static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

	private final Context mContext;

	// flag for GPS status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;

	// flag for GPS status
	boolean canGetLocation = false;

	public static Location location;
	double latitude;
	double longitude;

	// Location Manager
	protected LocationManager locationManager;

	public GPS_Utils(Context context) {
		this.mContext = context;
		getLocation();
	}

	public Location getLocation() {
		try {
			locationManager = (LocationManager) mContext
					.getSystemService(LOCATION_SERVICE);

			// GPS status
			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// Network status
			isNetworkEnabled = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled) {
				Toast.makeText(getBaseContext(), Constants.sLblNoNetwork,
						Toast.LENGTH_SHORT).show();
			} else {
				this.canGetLocation = true;
				if (isNetworkEnabled) {
					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER,
							MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

					if (locationManager != null) {
						location = locationManager
								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
						}
					}
				}

				// GPS Enabled get lat/long using GPS provider
				if (isGPSEnabled) {
					if (location == null) {
						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER,
								MIN_TIME_BW_UPDATES,
								MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

						if (locationManager != null) {
							location = locationManager
									.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							if (location != null) {
								latitude = location.getLatitude();
								longitude = location.getLongitude();
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return location;
	}

	/**
	 * Calling this will stop using GPS in your app
	 * */
	public void stopUsingGPS() {
		if (locationManager != null) {
			locationManager.removeUpdates(GPS_Utils.this);
		}
	}

	/**
	 * get latitude
	 * */
	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		}
		return latitude;
	}

	/**
	 * get longitude
	 * */
	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		}
		return longitude;
	}

	/**
	 * check GPS/wifi enabled
	 * 
	 * @return boolean
	 * */
	public boolean canGetLocation() {
		getLocation();
		return this.canGetLocation;
	}

	/**
	 * Function to show settings alert dialog
	 * */
	public void showSettingsAlert() {
		final EjobAlertDialog alertDialog = new EjobAlertDialog(mContext);

		LayoutInflater li = LayoutInflater.from(mContext);
		View promptsView = li
				.inflate(R.layout.dialog_simple_one_textview, null);
		alertDialog.setView(promptsView);

		// set user activity for
		LinearLayout rootNode = (LinearLayout) promptsView
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});

		final TextView textMessage = (TextView) promptsView
				.findViewById(R.id.text_message);
		final TextView title = (TextView) promptsView
				.findViewById(R.id.text_dialog_title);

		// setting dialog title
		title.setVisibility(TextView.VISIBLE);
		title.setText("GPS settings");

		// Setting Dialog Message
		textMessage.setText("GPS is not enabled."
				+ " Do you want to go to settings menu?");

		// On pressing Settings button
		alertDialog.setPositiveButton("Change Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// update user activity on button click
						alertDialog.updateInactivityForDialog();

						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						mContext.startActivity(intent);
					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// update user activity on button click
						alertDialog.updateInactivityForDialog();

						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.location.LocationListener#onLocationChanged(android.location.
	 * Location)
	 */
	@Override
	public void onLocationChanged(Location location) {

		if (location != null) {
			latitude = location.getLatitude();
			longitude = location.getLongitude();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.location.LocationListener#onProviderDisabled(java.lang.String)
	 */
	@Override
	public void onProviderDisabled(String provider) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.location.LocationListener#onProviderEnabled(java.lang.String)
	 */
	@Override
	public void onProviderEnabled(String provider) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.location.LocationListener#onStatusChanged(java.lang.String,
	 * int, android.os.Bundle)
	 */
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Service#onBind(android.content.Intent)
	 */
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	/**
	 * @param latB
	 * @param longB
	 * @param latA
	 * @param longA
	 * @return Function to calculate distance between 2 lat and long.
	 */
	public static double Calculate_Distance(Double latB, Double longB,
			Double latA, Double longA) {
		double distance;
		Location locationA = new Location("point A");
		locationA.setLatitude(latA);
		locationA.setLongitude(longA);
		Location locationB = new Location("point B");
		locationB.setLatitude(latB);
		locationB.setLongitude(longB);
		distance = locationA.distanceTo(locationB);
		return distance;
	}

	/**
	 * get address
	 * 
	 * @return Address
	 */
	public String getAddress() {

		Geocoder addressLocator = new Geocoder(mContext);
		String locationAddress = "";
		try {
			List<Address> address = addressLocator.getFromLocation(
					this.getLatitude(), this.getLongitude(), 1);
			locationAddress = address.get(0).getAddressLine(0);
		} catch (Exception e) {
			Log.e("GPS--", "cannot get location");
		}
		return locationAddress;
	}

	public boolean checkNetwork() {
		ConnectivityManager connectivityManager = (ConnectivityManager) mContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

}