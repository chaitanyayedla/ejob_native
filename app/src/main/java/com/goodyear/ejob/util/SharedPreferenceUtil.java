package com.goodyear.ejob.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * This class provides an interface to the shared preferences data.
 */
public class SharedPreferenceUtil {

	/**
	 * get string value from shared preferences for a given key.
	 * 
	 * @param context
	 *            application context
	 * @param key
	 *            shared preference key
	 * @return value retrieved from shared preferences
	 */
	public static String getStringValueFromSharedPref(Context context,
			String key, String defaultValue) {
		String value = null;
		if (context != null && !TextUtils.isEmpty(key)) {
			SharedPreferences pref = context.getSharedPreferences(
					Constants.GOODYEAR_CONF, Context.MODE_PRIVATE);
			value = pref.getString(key, defaultValue);
		}
		return value;
	}
}
