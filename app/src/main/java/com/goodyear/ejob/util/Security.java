package com.goodyear.ejob.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

public class Security {

	private static final String CTYPT_KEY = "cI54oYspu8Dh8NWQtK0gt/Hs9YtC9ZQpaGhTQt11Iss=";
	private static final String CTYPT_IV = "3yS5Gl+WY0F7v8/tmGlLwA==";

	/**
	 * Encrypt a string
	 * 
	 * @param message
	 *        String to be encrpted
	 * @return
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 */
	public static String encryptMessage(String message)
			throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidAlgorithmParameterException,
			IllegalBlockSizeException, BadPaddingException, IOException {

		byte[] rawData = message.getBytes();
		byte[] key = Base64.decode(CTYPT_KEY, Base64.DEFAULT);
		byte[] iv = Base64.decode(CTYPT_IV, Base64.DEFAULT);

		byte[] encrypted = encrypt(iv, key, rawData);

		return Base64.encodeToString(encrypted, Base64.DEFAULT);
	}

	/**
	 * Decrypt a string
	 * 
	 * @param message
	 *        String to be decrypted
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String decryptMessage(String message)
			throws UnsupportedEncodingException {

		byte[] rawData = Base64.decode(message, Base64.DEFAULT);
		byte[] key = Base64.decode(CTYPT_KEY, Base64.DEFAULT);
		byte[] iv = Base64.decode(CTYPT_IV, Base64.DEFAULT);

		byte[] encrypted = null;
		try {
			encrypted = decrypt(iv, key, rawData);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | InvalidAlgorithmParameterException
				| IllegalBlockSizeException | BadPaddingException
				| ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new String(encrypted, "UTF-8");
	}

	/**
	 * to encrypt a byte array
	 * 
	 * @param ivBytes
	 * @param keyBytes
	 *        key bytes
	 * @param msg
	 *        string to encrypt
	 * @throws IllegalBlockSizeException
	 */
	private static byte[] encrypt(byte[] ivBytes, byte[] keyBytes, byte[] msg)
			throws IllegalBlockSizeException, BadPaddingException {

		AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		SecretKeySpec newKey = new SecretKeySpec(keyBytes, "AES");
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

			cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException
				| NoSuchAlgorithmException | NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cipher.doFinal(msg);
	}

	/**
	 * Decrypt a byte array
	 * 
	 * @param ivBytes
	 * @param keyBytes
	 *        key as byte array
	 * @param msg
	 *        message to decrypt
	 * @return decrypted array of bytes
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private static byte[] decrypt(byte[] ivBytes, byte[] keyBytes, byte[] msg)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException,
			IllegalBlockSizeException, BadPaddingException, IOException,
			ClassNotFoundException {

		AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		SecretKeySpec newKey = new SecretKeySpec(keyBytes, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
		return cipher.doFinal(msg);

	}
}
