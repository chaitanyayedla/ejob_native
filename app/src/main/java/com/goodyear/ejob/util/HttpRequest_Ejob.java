package com.goodyear.ejob.util;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

import android.os.Environment;
import android.util.Log;

import com.goodyear.ejob.sync.BodyType;
import com.goodyear.ejob.sync.Result;

/**
 * @author shailesh.p
 * 
 */
public class HttpRequest_Ejob {
	private static final String TAG = HttpRequest_Ejob.class.getSimpleName();
	//Read_Timeout value increased by +30000 (old value 60000), as per client need
	private static final int READ_TIMEOUT = 90000;
	private static final int CONNECTION_TIMEOUT = 10000;

	/**
	 * Make a request to server for login
	 * 
	 * @return compressed byte array from server
	 * @throws Exception
	 */
	public static byte[] makeRequestForKeyValueRequest() throws Exception {
		LogUtil.i(TAG, "making request for key value");
		// Create a request for the URL.
		URL url = new URL(Constants._URL);

		// create HTTP connection
		HttpURLConnection httpURLConnection = (HttpURLConnection) url
				.openConnection();
		httpURLConnection.setConnectTimeout(CONNECTION_TIMEOUT);
		//Read_Timeout value increased by +5000 (old value 15000), as per client need
		httpURLConnection.setReadTimeout(20000);

		httpURLConnection.setRequestMethod("PUT");
		httpURLConnection.setRequestProperty("Content-Type",
				"application/x-www-form-urlencoded");

		byte[] buffer = new byte[32767 + 1];

		// read pack file
		FileInputStream packFile = new FileInputStream(Constants.PROJECT_PATH
				+ "/pack.pack");
		int read = packFile.read(buffer);
		packFile.close();
		httpURLConnection.setDoOutput(true);

		// write pack file to HTTP stream
		OutputStream reqDataStream = new DataOutputStream(
				httpURLConnection.getOutputStream());
		reqDataStream.write(buffer, 0, read);

		// get response stream
		InputStream response = httpURLConnection.getInputStream();

		byte[] tempBuffer = new byte[32767 + 1];
		byte[] compressedResponse = new byte[32767 + 1];

		int len;
		int size = 0;
		// read while response end
		while (-1 != (len = response.read(tempBuffer))) {
			for (int i = 0; i < len; i++) {
				compressedResponse[size++] = tempBuffer[i];
			}
		}

		byte[] responsebody = new byte[32767];
		// read response from 19th byte as compressed data starts from
		// 19 byte
		for (int i = 19, j = 0; i < compressedResponse.length; i++, j++) {
			responsebody[j] = compressedResponse[i];
		}
		LogUtil.i(TAG, "request for keyvalue completed :: read values ::"
				+ responsebody);
		return responsebody;
	}

	/**
	 * Method to make request Server 
	 * @throws IOException
	 * @throws SocketTimeoutException
	 */
	public static void makeRequestToGetDBFile() throws IOException,
			SocketTimeoutException {
		LogUtil.i(TAG, "making request for DB");

		byte[] buffer = new byte[32767 + 1];
		URL url = new URL(Constants._URL);

		InputStream response;

		HttpURLConnection httpURLConnection = (HttpURLConnection) url
				.openConnection();

		httpURLConnection.setConnectTimeout(CONNECTION_TIMEOUT);
		httpURLConnection.setReadTimeout(READ_TIMEOUT);

		httpURLConnection.setRequestMethod("PUT");// = "PUT";
		httpURLConnection.setRequestProperty("Content-Type",
				"application/x-www-form-urlencoded");// =

		FileInputStream packFile = new FileInputStream(Constants.PROJECT_PATH
				+ "/pack.pack");
		httpURLConnection.setDoOutput(true);
		OutputStream reqDataStream = new DataOutputStream(
				httpURLConnection.getOutputStream());
		int len;
		while ((len = packFile.read(buffer)) != -1) {
			reqDataStream.write(buffer, 0, len);
		}
		packFile.close();
		response = httpURLConnection.getInputStream();

		readResponseInTofile(response);

		Log.i(TAG, "makeRequestToGetDBFile - Constants.Force_Download_Sync_STATUS : " + Constants.Force_Download_Sync_STATUS);
		//Backup Server Response Information
		backupResponse(Constants.Force_Download_Sync_STATUS);


		FileOperations.decompressGzipFile(Constants.PROJECT_PATH
				+ Constants.RESPONSEFILE_PATH, Constants.PROJECT_PATH + "/"
				+ Constants.USER + "_.db3");
	}

	/**
	 * API to download the file
	 * 
	 * @param response
	 * @throws IOException
	 */
	private static void readResponseInTofile(InputStream response)
			throws IOException {
		LogUtil.i(TAG, "sending request for db now");
		int lengthRead;
		byte[] responseBuffer = new byte[32767];
		Log.e("HttpResponse_Ejob", "Reading bytes ");
		FileOutputStream responsefile = new FileOutputStream(
				Constants.PROJECT_PATH + Constants.RESPONSEFILE_PATH);

		// read the first iteration
		lengthRead = response.read(responseBuffer);
		// for vodafone
		// FIX for 3G Vodafone UK the Sync was not working, Issue: The packets
		// was getting in big size so added filter.
		if (lengthRead > 600) {
			LogUtil.i(TAG, "length > 600 " + lengthRead);
			processResponseWithMultipleWaitingHeader(lengthRead, responsefile,
					responseBuffer, response);
		} else {
			// needs to execute atleast one so do-while loop
			do {
				if (lengthRead > 200) {
					LogUtil.i(TAG, "length > 200 " + lengthRead);
					processResponseWithMultipleWaitingHeader(lengthRead,
							responsefile, responseBuffer, response);
					break;
				}
				// download response if not waiting
				if (!readResponseIsWaiting(responseBuffer)) {
					LogUtil.i(TAG, "download response if not waiting" + lengthRead);
					processDownloadResponse(19, lengthRead, responsefile,
							responseBuffer, response);
					break;
				} else {
					continue;
				}
			} while (-1 != (lengthRead = response.read(responseBuffer)));
		}
		Log.e("HttpResponse_Ejob", "Finished Reading bytes ");
		LogUtil.i("HttpResponse_Ejob", "Finished Reading bytes ");
	}

	/**
	 * process download response
	 * 
	 * @param offset
	 *            offset for reading the half reponse
	 * @param bytesToRead
	 *            bytes to read from half response
	 * @param responsefile
	 *            file to be written
	 * @param responseBuffer
	 *            response buffer read
	 * @param response
	 *            response object
	 * @throws IOException
	 */
	private static void processDownloadResponse(int offset, int bytesToRead,
			FileOutputStream responsefile, byte[] responseBuffer,
			InputStream response) throws IOException {
		Log.e("HttpResponse_Ejob", "Length Read::" + bytesToRead);
		LogUtil.i(TAG, "starting download -- response -- " + responseBuffer[15] + " , status : " + responseBuffer[16]);
		try {
			Constants.finalResponseFromServer = responseBuffer[16];
		}
		catch (Exception e)
		{
			LogUtil.i(TAG,"processDownloadResponse : Exception - "+e.getMessage());
		}
		if (responseBuffer[15] == BodyType.database) {
			readDatabaseResponseWithOffset(offset, bytesToRead, responsefile,
					responseBuffer);
			readDatabaseResponse(responsefile, response);
		}
		responsefile.close();
		LogUtil.i(TAG, "processDownloadResponse" + "Success");

	}

	/**
	 * check if upload is waiting
	 * 
	 * @param buffer
	 *            buffer to check
	 * @return
	 */
	private static boolean readResponseIsWaiting(byte[] buffer) {
		if (buffer[16] == Result.waitingDownload
				|| buffer[16] == Result.waitingUpload) {
			return true;
		}
		return false;
	}

	/**
	 * write database file from response
	 * 
	 * @param responsefile
	 *            file to be written
	 * @param response
	 *            reponse object
	 * @throws IOException
	 */
	private static void readDatabaseResponse(FileOutputStream responsefile,
			InputStream response) throws IOException {
		LogUtil.i(TAG, "reading response -- response -- ");
		int lengthRead;
		byte[] responseBuffer = new byte[32767];
		while (-1 != (lengthRead = response.read(responseBuffer))) {
			responsefile.write(responseBuffer, 0, lengthRead);
		}
		LogUtil.i(TAG, "read response -- response -- ");
	}

	/**
	 * read response with offset
	 * 
	 * @param offset
	 *            offset for reading the half reponse
	 * @param bytesToRead
	 *            bytes to read from half response
	 * @param responsefile
	 *            file to be written
	 * @param responseBuffer
	 *            response buffer read
	 * @throws IOException
	 */
	private static void readDatabaseResponseWithOffset(int offset,
			int bytesToRead, FileOutputStream responsefile,
			byte[] responseBuffer) throws IOException {
		for (int i = offset; i < bytesToRead; i++) {
			responsefile.write(responseBuffer[i]);
		}
		LogUtil.i(TAG, "readDatabaseResponseWithOffset" + "Success");

	}

	/**
	 * Method to process the response with multiple waiting header, until there is upload or download response
	 * @param lengthRead data length
	 * @param responsefile response text file
	 * @param responseBuffer buffer
	 * @param response response from server
	 * @throws IOException
	 */
	private static void processResponseWithMultipleWaitingHeader(
			int lengthRead, FileOutputStream responsefile,
			byte[] responseBuffer, InputStream response) throws IOException {
		// read upto 600 bytes
		for (int itrator = 0; itrator < lengthRead - 20; itrator = itrator + 19) {
			byte[] headbuff = new byte[19];
			// rebuild upload response byte
			for (int byteCountForUpload = 0; byteCountForUpload < 19; byteCountForUpload++) {
				headbuff[byteCountForUpload] = responseBuffer[itrator
						+ byteCountForUpload];
			}
			// if upload response then continue
			if (readResponseIsWaiting(headbuff)) {
				continue;
			}
			// start download to file
			processDownloadResponse(itrator + 19, lengthRead, responsefile,
					responseBuffer, response);
			break;
		}
	}

	/**
	 * Method to back up Server Response information
	 * @param doBackup doBackup
     */
	private static void backupResponse(Boolean doBackup) {
		if (doBackup) {
			LogUtil.i(TAG, "Response Backup");
			String DestinationFileName = Constants.PROJECT_PATH + "/ResponseBackup/" + Constants.USER + DateTimeUTC.getCurrentDataAndTime() + ".txt";
			String SourceFileName = Constants.PROJECT_PATH + Constants.RESPONSEFILE_PATH;
			try {
				File file = new File(DestinationFileName);
				if (!file.exists()) {
					file.getParentFile().mkdirs();
					file.createNewFile();

				}
				//Copy source to Destination
				FileOperations.copyFile(SourceFileName, DestinationFileName);
			} catch (Exception e) {
				LogUtil.i(TAG, "Response Exception - " + e.getMessage());
			}
		}
	}
}
