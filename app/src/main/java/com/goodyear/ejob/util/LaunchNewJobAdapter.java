package com.goodyear.ejob.util;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.goodyear.ejob.LaunchNewJob;
import com.goodyear.ejob.R;

/**
 * @author johnmiya.s
 * 
 */
public class LaunchNewJobAdapter extends BaseAdapter {

	private static final String Layout_INFLATER_SERVICE = null;
	private ArrayList<HashMap<String, String>> itemDetailsrrayList;
	private LayoutInflater l_Inflater;

	private int selectedPos = -1;
	public Context type_context;

	/**
	 * @param launchNewJob
	 * @param tyreInfo
	 */
	public LaunchNewJobAdapter(LaunchNewJob launchNewJob,
			ArrayList<Tyre> tyreInfo) {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param context
	 * @param results
	 * @return
	 */
	public void JobList_Adapter_Saved(Context context,
			ArrayList<HashMap<String, String>> results) {
		type_context = context;
		itemDetailsrrayList = results;
		l_Inflater = LayoutInflater.from(context);

	}

	public void JobList_Adapter_refresh(
			ArrayList<HashMap<String, String>> results) {
		itemDetailsrrayList = results;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	public int getCount() {
		return itemDetailsrrayList.size();
	}

	/**
	 * @param pos
	 */
	public void setSelectedPosition(int pos) {
		selectedPos = pos;
		// inform the view of this change
		notifyDataSetChanged();
	}

	/**
	 * @return
	 */
	public int getSelectedPosition() {
		return selectedPos;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	public Object getItem(int position) {
		return itemDetailsrrayList.get(position);
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	public long getItemId(int position) {
		return position;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {

			convertView = l_Inflater.inflate(R.layout.items_newjob_saved, null);

			holder = new ViewHolder();
			holder.txt_lp = (TextView) convertView
					.findViewById(R.id.TextView06);
			holder.txt_cust = (TextView) convertView
					.findViewById(R.id.textView1);
			holder.txt_vendor = (TextView) convertView
					.findViewById(R.id.textView2);
			holder.txt_customer = (TextView) convertView
					.findViewById(R.id.TextView05);
			/*
			 * if(Constant._imageVisiblity == false){
			 * holder.image =
			 * (ImageView)convertView.findViewById(R.id.tick_save_back);
			 * holder.image.setVisibility(View.INVISIBLE);
			 * }else{
			 */
			holder.image = (ImageView) convertView
					.findViewById(R.id.tick_save_back);

			// }
			/*
			 * holder.txt_date= (TextView)
			 * convertView.findViewById(R.id.TextView08);
			 * holder.txt_ref_no= (TextView)
			 * convertView.findViewById(R.id.TextView07);
			 */

			holder.txt_lp.setSelected(true);
			holder.txt_customer.setSelected(true);
			/*
			 * holder.txt_date.setSelected(true);
			 * holder.txt_ref_no.setSelected(true);
			 */
			convertView.setTag(holder);
		} else {

			holder = (ViewHolder) convertView.getTag();
		}

		try {
			holder.txt_cust.setText(itemDetailsrrayList.get(position).get(
					"customername"));
			holder.txt_vendor.setText(itemDetailsrrayList.get(position).get(
					"vendor"));
			holder.txt_lp.setText(itemDetailsrrayList.get(position).get(
					"licence"));
			holder.txt_customer.setText(itemDetailsrrayList.get(position).get(
					"cname"));
			if (itemDetailsrrayList.get(position).get("status")
					.equalsIgnoreCase("false")) {
				holder.image.setVisibility(View.INVISIBLE);
			} else {
				holder.image.setVisibility(View.VISIBLE);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * holder.txt_date.setText(itemDetailsrrayList.get(position).get(
		 * "created_date"));
		 * holder.txt_ref_no.setText(itemDetailsrrayList.get(position).get(
		 * "cc_ref_no"));
		 */
		return convertView;
	}

	static class ViewHolder {
		TextView txt_customer, txt_ref_no, txt_date, txt_lp;
		TextView txt_cust, txt_vendor;
		ImageView image;

	}
}
