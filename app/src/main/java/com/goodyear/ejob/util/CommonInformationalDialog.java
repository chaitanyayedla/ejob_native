package com.goodyear.ejob.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goodyear.ejob.R;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;

/**
 * @author Good Year
 * 
 */
public class CommonInformationalDialog extends DialogFragment {

	Context mContext;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mContext = activity;
	}

	public CommonInformationalDialog() {
		// Empty constructor required for DialogFragment
	}

	public static CommonInformationalDialog newInstance(String title) {
		CommonInformationalDialog frag = new CommonInformationalDialog();
		Bundle args = new Bundle();
		args.putString("title", title);
		frag.setArguments(args);
		return frag;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
		View dialoglayout = inflater.inflate(
				R.layout.dialog_common_informational, null);
		// update user activity for dialog layout
		LinearLayout rootNode = (LinearLayout) dialoglayout
				.findViewById(R.id.layout_root);
		rootNode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				InactivityUtils.updateActivityOfUser();
			}
		});
		String title = getArguments().getString("title", "Alert!!");
		TextView txtTitle = (TextView) dialoglayout
				.findViewById(R.id.textViewTitle);
		txtTitle.setText(title);
		final EjobAlertDialog alertDialog = new EjobAlertDialog(mContext);
		alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
		alertDialog.setView(dialoglayout);
		alertDialog.setPositiveButton(R.string.lbl_ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						alertDialog.updateInactivityForDialog();
						okListener.onOkButtonClick();
					}
				});
		alertDialog.setNegativeButton(R.string.lbl_Cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						alertDialog.updateInactivityForDialog();
						okListener.onCancelButtonClick();
					}
				});
		return alertDialog.create();
	}

	public MyOnClickListener okListener;

	/**
	 * @return the cancelListener
	 */
	public MyOnClickListener getCancelListener() {
		return okListener;
	}

	/**
	 * @param cancelListener
	 *            the cancelListener to set
	 */
	public void setCancelListener(MyOnClickListener cancelListener) {
		this.okListener = cancelListener;
	}

	/**
	 * @return the setOkListener
	 */
	public MyOnClickListener getOkListener() {
		return okListener;
	}

	/**
	 * @param setOkListener
	 *            the setOkListener to set
	 */
	public void setOkListener(MyOnClickListener setOkListener) {
		this.okListener = setOkListener;
	}

	public interface MyOnClickListener {
		void onOkButtonClick();

		void onCancelButtonClick();
	}

}
