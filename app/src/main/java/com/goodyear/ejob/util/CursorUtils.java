package com.goodyear.ejob.util;

import android.database.Cursor;

/**
 * Created by shailesh.p on 10/31/2014.
 */
public class CursorUtils {
    public static boolean isValidCursor(Cursor cursor) {
        boolean isValid = cursor != null && cursor.getCount() > 0;
        if (!isValid) {
            closeCursor(cursor);
        }
        return isValid;
    }

    public static void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }
}
