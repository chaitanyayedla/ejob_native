package com.goodyear.ejob.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * @author amitkumar.h
 * 
 */
public class DecimalDigitsInputFilter implements InputFilter {

	Pattern mPattern;

	public DecimalDigitsInputFilter(int digitsBeforeZero, int digitsAfterZero) {
		mPattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1)
				+ "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?");
	}

	/*
	 * (non-Javadoc)
	 * @see android.text.InputFilter#filter(java.lang.CharSequence, int, int,
	 * android.text.Spanned, int, int)
	 */
	@Override
	public CharSequence filter(CharSequence arg0, int arg1, int arg2,
			Spanned arg3, int arg4, int arg5) {
		// TODO Auto-generated method stub

		Matcher matcher = mPattern.matcher(arg3);
		if (!matcher.matches())
			return "";
		return null;
	}

}