package com.goodyear.ejob.util;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author shailesh.p
 * 
 */
public class VehicleSkeletonInfo {

	// private String vehicleConfiguration;

	private boolean isSpare;
	private int axlecount;

	private String vehicleType;
	private ArrayList<String> axleInfo = new ArrayList<String>();
	private String[] vehicleSkeletonInfo;

	private static final int VEHICLE_TYPE = 0;
	private static final int NUMBER_OF_AXLE = 1;
	private static final int DRIVEN_POS = 2;
	private static final int STEER_POS = 3;
	private static final int LIFTED_POS = 4;
	private static final int DOUBLE_POS = 5;
	private static final int SPARE_POS = 6;

	/**
	 * constructor
	 * 
	 * @param vehicleConfiguration the vehicle configuration code (for e.g.
	 *        TR0000000000)
	 */
	public VehicleSkeletonInfo(String vehicleConfiguration) {
		decodeVehicleConfig(vehicleConfiguration);
	}

	/**
	 * decode the configuration
	 * 
	 * @param vehicleConfig VC as string
	 */
	private void decodeVehicleConfig(String vehicleConfig) {

		// split the code and save in an arraylist
		for (int i = 0; i < vehicleConfig.length(); i = i + 2) {
			StringBuilder sb = new StringBuilder();
			axleInfo.add(sb.append(vehicleConfig.charAt(i))
					.append(vehicleConfig.charAt(i + 1)).toString());
		}

		// get the axle count
		getAxleCount();
		// get which axle are driven
		getDrivenAxles();
		// get which axle are Steer
		getSteerAxles();
		// get which axle are Lifted
		getLiftedAxles();
		// get which axle are Mounted
		getDoubleMountedAxles();
		// get which axle are vehicle type
		getVehicleType();
		// get if spare tire are available
		checkifSpare();
	}

	/**
	 * Get vehicle type (TU, TR etc)
	 */
	private void getVehicleType() {
		vehicleType = axleInfo.get(VEHICLE_TYPE);
	}

	/**
	 * check if spares are available
	 */
	private void checkifSpare() {
		// get spare info from array list
		int spare = Integer.parseInt(axleInfo.get(SPARE_POS));
		if (spare == 1) {
			isSpare = true;
		}
	}

	/**
	 * get steer axles
	 */
	private void getSteerAxles() {
		// get steer axle code in decimal
		int steerAxlesInDecimal = Integer.parseInt(axleInfo.get(STEER_POS));
		// get steer axle info in binary
		String steerAxleInBinary = Integer.toBinaryString(steerAxlesInDecimal);
		// reverse the order of code
		StringBuffer sb = new StringBuffer(steerAxleInBinary);
		steerAxleInBinary = sb.reverse().toString();

		// for each char identify the axle pos
		for (int i = 0; i < steerAxleInBinary.length(); i++) {
			if (steerAxleInBinary.charAt(i) == '1') {
				// get current info about the axle
				String currentAxleValue = vehicleSkeletonInfo[i];
				if (currentAxleValue.equals(""))
					currentAxleValue = currentAxleValue + "ST";
				else
					currentAxleValue = currentAxleValue + "|ST";
				// add the steer info to the vehicle skeleton
				vehicleSkeletonInfo[i] = currentAxleValue;
			}
		}
	}

	/**
	 * get driven axles
	 */
	private void getDoubleMountedAxles() {
		// get double mounted axles code in decimal
		int doubleAxlesInDecimal = Integer.parseInt(axleInfo.get(DOUBLE_POS));
		// get double mounted axles code in binary
		String doubleAxleInBinary = Integer
				.toBinaryString(doubleAxlesInDecimal);
		// reverse the code
		StringBuffer sb = new StringBuffer(doubleAxleInBinary);
		doubleAxleInBinary = sb.reverse().toString();

		// for each char in code
		for (int i = 0; i < doubleAxleInBinary.length(); i++) {
			if (doubleAxleInBinary.charAt(i) == '1') {
				// get current info about the axle
				String currentAxleValue = vehicleSkeletonInfo[i];
				if (currentAxleValue.equals(""))
					currentAxleValue = currentAxleValue + "DU";
				else
					currentAxleValue = currentAxleValue + "|DU";
				// add the steer info to the vehicle skeleton
				vehicleSkeletonInfo[i] = currentAxleValue;
			}
		}
	}

	/**
	 * get lifted axle
	 */
	private void getLiftedAxles() {
		// get lifted axle code in decimal
		int liftedAxlesInDecimal = Integer.parseInt(axleInfo.get(LIFTED_POS));
		// get lifted axle code in binary
		String liftedAxleInBinary = Integer
				.toBinaryString(liftedAxlesInDecimal);
		// reverse code
		StringBuffer sb = new StringBuffer(liftedAxleInBinary);
		liftedAxleInBinary = sb.reverse().toString();
		// for each char in code
		for (int i = 0; i < liftedAxleInBinary.length(); i++) {
			if (liftedAxleInBinary.charAt(i) == '1') {
				// get current info about the axle
				String currentAxleValue = vehicleSkeletonInfo[i];
				if (currentAxleValue.equals(""))
					currentAxleValue = currentAxleValue + "LI";
				else
					currentAxleValue = currentAxleValue + "|LI";
				// add the lifted axle info to the vehicle skeleton
				vehicleSkeletonInfo[i] = currentAxleValue;
			}
		}
	}

	/**
	 * get driven axle from code
	 */
	private void getDrivenAxles() {
		// Initialise vehicle skeleton
		vehicleSkeletonInfo = new String[axlecount];
		// by default will be null, so make blank ("")
		Arrays.fill(vehicleSkeletonInfo, "");
		// get driven axle in demical
		int drivenAxlesInDecimal = Integer.parseInt(axleInfo.get(DRIVEN_POS));
		// get driven axle in binary
		String drivenAxleInBinary = Integer
				.toBinaryString(drivenAxlesInDecimal);
		// reverse the code
		StringBuffer sb = new StringBuffer(drivenAxleInBinary);
		drivenAxleInBinary = sb.reverse().toString();
		// for each char in code
		for (int i = 0; i < drivenAxleInBinary.length(); i++) {
			if (drivenAxleInBinary.charAt(i) == '1') {
				String currentAxleValue = vehicleSkeletonInfo[i];
				currentAxleValue = currentAxleValue + "DR";
				// add code to the vehicle skeleton
				vehicleSkeletonInfo[i] = currentAxleValue;
			}
		}
	}

	/**
	 * get axle count
	 */
	private void getAxleCount() {
		// get axle count code from arraylist
		axlecount = Integer.parseInt(axleInfo.get(NUMBER_OF_AXLE));
	}

	/**
	 * @return the isSpare
	 */
	public boolean isSpare() {
		return isSpare;
	}

	/**
	 * @return the axlecount
	 */
	public int getAxlecount() {
		return axlecount;
	}

	/**
	 * @return the vehicleSkeletonInfo {ST-Steer, DR-Driven, LI-Lifted,
	 *         DU-Double
	 * 
	 */
	public String[] getVehicleSkeletonInfo() {
		return vehicleSkeletonInfo;
	}

	/* Bug 665 : getNumberOfSpareTyresInVehicle() will calculate total
	   number of spare tyres in a vehicle, maximum can be 2 in
	   a vehicle.
	 */
	public int getNumberOfSpareTyresInVehicle() {
		int spareTyreNumber = 0;

		int spareTyre = Integer.parseInt(axleInfo.get(SPARE_POS));
		// get spare tyre code in binary
		String spareTyreCountBin = Integer
				.toBinaryString(spareTyre);
		// reverse the code
		StringBuffer sb = new StringBuffer(spareTyreCountBin);
		spareTyreCountBin = sb.reverse().toString();
		// for each char in code
		for (int i = 0; i < spareTyreCountBin.length(); i++) {
			if (spareTyreCountBin.charAt(i) == '1') {
				//a vehicle can have maximum 2 spare tyres.
				if (i == 0) {
					spareTyreNumber = 1;
				} else if (i == 1) {
					spareTyreNumber = 2;
				}
			}
		}
		return spareTyreNumber;
	}

	/* Bug 665 : getNumberOfTyresInVehicle() will calculate total
	 * number of tyres in a vehicle.
	 */
	public int getNumberOfTyresInVehicle() {
		int doubleMountedTyre = 0;
		int tyreNumber = 0;
		tyreNumber = getAxlecount() * 2;

		int doubleAxlesInDecimal = Integer.parseInt(axleInfo.get(DOUBLE_POS));
		// get double mounted axles code in binary
		String doubleAxleInBinary = Integer
				.toBinaryString(doubleAxlesInDecimal);
		// reverse the code
		StringBuffer sb = new StringBuffer(doubleAxleInBinary);
		doubleAxleInBinary = sb.reverse().toString();
		// for each char in code
		for (int i = 0; i < doubleAxleInBinary.length(); i++) {
			if (doubleAxleInBinary.charAt(i) == '1') {
				doubleMountedTyre = doubleMountedTyre + 2;
			}
		}
		tyreNumber = tyreNumber + doubleMountedTyre + getNumberOfSpareTyresInVehicle();
		return tyreNumber;
	}
}
