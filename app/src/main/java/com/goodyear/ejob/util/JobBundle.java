package com.goodyear.ejob.util;

/**
 * @author johnmiya.s
 * 
 */
public class JobBundle {

	public static String location;
	public static String CallerPhone;
	public static String mDefectAuthNumber;
	public static String mDriverName;
	public static String mVehicleODO;
	public static String mValueCallCenterRefNo;
	public static String mActionLineN;
	public static String mHudometer;
	public static String mStockLocation;
	public static String mRefNO;
	public static String mLPlate;
	public static String mSAPCode;
	public static String mAccountName;

	/**
	 * @return the mSAPCode
	 */
	public static String getmSAPCode() {
		return mSAPCode;
	}

	/**
	 * @param mSAPCode the mSAPCode to set
	 */
	public static void setmSAPCode(String mSAPCode) {
		JobBundle.mSAPCode = mSAPCode;
	}

	/**
	 * @return the mAccountName
	 */
	public static String getmAccountName() {
		return mAccountName;
	}

	/**
	 * @param mAccountName the mAccountName to set
	 */
	public static void setmAccountName(String mAccountName) {
		JobBundle.mAccountName = mAccountName;
	}

	/**
	 * @return the mFleetNO
	 */
	public static String getmFleetNO() {
		return mFleetNO;
	}

	/**
	 * @param mFleetNO the mFleetNO to set
	 */
	public static void setmFleetNO(String mFleetNO) {
		JobBundle.mFleetNO = mFleetNO;
	}

	public static String mFleetNO;

	/**
	 * @return the mLPlate
	 */
	public static String getmLPlate() {
		return mLPlate;
	}

	/**
	 * @param mLPlate the mLPlate to set
	 */
	public static void setmLPlate(String mLPlate) {
		JobBundle.mLPlate = mLPlate;
	}

	/**
	 * @return the mRefNO
	 */
	public static String getmRefNO() {
		return mRefNO;
	}

	/**
	 * @param mRefNO the mRefNO to set
	 */
	public static void setmRefNO(String mRefNO) {
		JobBundle.mRefNO = mRefNO;
	}

	/**
	 * @return the mStockLocation
	 */
	public static String getmStockLocation() {
		return mStockLocation;
	}

	/**
	 * @param mStockLocation the mStockLocation to set
	 */
	public static void setmStockLocation(String mStockLocation) {
		JobBundle.mStockLocation = mStockLocation;
	}

	/**
	 * @return the mHudometer
	 */
	public static String getmHudometer() {
		return mHudometer;
	}

	/**
	 * @param mHudometer the mHudometer to set
	 */
	public static void setmHudometer(String mHudometer) {
		JobBundle.mHudometer = mHudometer;
	}

	/**
	 * @return the mActionLineN
	 */
	public static String getmActionLineN() {
		return mActionLineN;
	}

	/**
	 * @param mActionLineN the mActionLineN to set
	 */
	public static void setmActionLineN(String mActionLineN) {
		JobBundle.mActionLineN = mActionLineN;
	}

	/**
	 * @return the mvalueCallCenterRefNo
	 */
	public static String getmValueCallCenterRefNo() {
		return mValueCallCenterRefNo;
	}

	/**
	 * @param mvalueCallCenterRefNo the mvalueCallCenterRefNo to set
	 */
	public static void setmValueCallCenterRefNo(String mvalueCallCenterRefNo) {
		JobBundle.mValueCallCenterRefNo = mvalueCallCenterRefNo;
	}

	/**
	 * @return the location
	 */
	public static String getLocation() {
		return location;
	}

	/**
	 * @return the mDefectAuthNumber
	 */
	public static String getmDefectAuthNumber() {
		return mDefectAuthNumber;
	}

	/**
	 * @param mDefectAuthNumber the mDefectAuthNumber to set
	 */
	public static void setmDefectAuthNumber(String mDefectAuthNumber) {
		JobBundle.mDefectAuthNumber = mDefectAuthNumber;
	}

	/**
	 * @return the mDriverName
	 */
	public static String getmDriverName() {
		return mDriverName;
	}

	/**
	 * @param mDriverName the mDriverName to set
	 */
	public static void setmDriverName(String mDriverName) {
		JobBundle.mDriverName = mDriverName;
	}

	/**
	 * @return the mVehicleODO
	 */
	public static String getmVehicleODO() {
		return mVehicleODO;
	}

	/**
	 * @param mVehicleODO the mVehicleODO to set
	 */
	public static void setmVehicleODO(String mVehicleODO) {
		JobBundle.mVehicleODO = mVehicleODO;
	}

	/**
	 * @param location the location to set
	 */
	public static void setLocation(String location) {
		JobBundle.location = location;
	}

	/**
	 * @return the callerPhone
	 */
	public static String getCallerPhone() {
		return CallerPhone;
	}

	/**
	 * @param callerPhone the callerPhone to set
	 */
	public static void setCallerPhone(String callerPhone) {
		CallerPhone = callerPhone;
	}

	/**
	 * @return the callerName
	 */
	public static String getCallerName() {
		return CallerName;
	}

	/**
	 * @param callerName the callerName to set
	 */
	public static void setCallerName(String callerName) {
		CallerName = callerName;
	}

	public static String CallerName;

}
