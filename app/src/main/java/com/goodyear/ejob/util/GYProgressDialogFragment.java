package com.goodyear.ejob.util;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.goodyear.ejob.R;

/**
 * Created by Rajesh on 10/13/2014.
 */
public class GYProgressDialogFragment extends DialogFragment {

	private ImageView mProgressIcon;
	private RotateAnimation mAnim;
	private boolean mShouldShow;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		Dialog dialog = getDialog();
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(Color.TRANSPARENT));

		View rootView = inflater.inflate(R.layout.frag_progress_dialog, null);
		mProgressIcon = (ImageView) rootView.findViewById(R.id.progress_icon);

		mAnim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF,
				.5f, Animation.RELATIVE_TO_SELF, .5f);
		mAnim.setInterpolator(new LinearInterpolator());
		mAnim.setRepeatCount(Animation.INFINITE);
		mAnim.setDuration(3000);
		mProgressIcon.setAnimation(mAnim);

		if (mShouldShow) {
			mProgressIcon.startAnimation(mAnim);
		}

		setCancelable(false);

		return rootView;
	}

	@Override
	public void show(FragmentManager manager, String tag) {
		super.show(manager, tag);
		if (mProgressIcon == null) {
			mShouldShow = true;
			return;
		}
		mProgressIcon.startAnimation(mAnim);
	}
}
