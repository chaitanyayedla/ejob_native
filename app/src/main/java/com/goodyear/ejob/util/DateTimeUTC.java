package com.goodyear.ejob.util;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import android.content.Context;
import android.widget.TextView;

/*
 * Using The API : joda-time-2.1.jar
 */

public class DateTimeUTC {
	private static final long TICKS_AT_EPOCH = 621355968000000000L;
	private static final long TICKS_PER_MILLISECOND = 10000;

	public static DateTime toDateTime(long milliseconds) {
		return new DateTime((milliseconds - DateTimeUTC.TICKS_AT_EPOCH)
				/ DateTimeUTC.TICKS_PER_MILLISECOND, DateTimeZone.UTC);
	}

	public static Date toDate(long ticks) {
		Date date = null;
		try {

			date = convertMillisecondsToUTCDate(ticks);
			// final SimpleDateFormat sdf = new
			// SimpleDateFormat("HH:mm:ss",Locale.getDefault());
			// System.out.println("UTC time: " + sdf.format(date));
			// System.out.println("Date : "+date);

		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Check=>toDate(long ticks) : ");
			e.printStackTrace();
		}
		return date;
	}

	public static Date convertMillisecondsToUTCDate(long milliseconds) {
		Date date = null;
		try {
			date = new Date((milliseconds - TICKS_AT_EPOCH)
					/ TICKS_PER_MILLISECOND);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	public static Date differenceFromTodayMilli(long milliseconds) {
		Date date = null;
		try {
			date = new Date((milliseconds - TICKS_AT_EPOCH)
					/ TICKS_PER_MILLISECOND);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	public static long getMillisecondsFromUTCdateLogin() {
		long elapsedTicks = 0L;
		try {
			DateTime baseTime = new DateTime(0001, 1, 1, 0, 0, 0);
			// DateTime currentDate = DateTime.now();
			DateTime nowInUTC = DateTime.now();//
			// Long val = 621355968000000000L + nowInUTC.getMillis() * 10000;
			elapsedTicks = ((nowInUTC.getMillis() - baseTime.getMillis()) * TICKS_PER_MILLISECOND);
			elapsedTicks = elapsedTicks
					- (25000000000L - 12500000000L + 500000000L);
		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Test=>getMillisecondsFromUTCdate) : ");
			e.printStackTrace();
		}
		return elapsedTicks;
	}

	public static String getDate(Date date) {
		String d = "";
		try {
			// DateFormat f = new SimpleDateFormat("EE");
			// DateFormat f = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
			DateFormat f = new SimpleDateFormat("E,MMM dd,yyyy",
					Locale.getDefault());
			d = f.format(date);
		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Check=>getDate(Date date) : ");
			e.printStackTrace();
		}
		return d;
	}

	public static String getTime(Date date) {
		String d = "";
		try {
			// DateFormat f = new SimpleDateFormat("EE");
			// DateFormat f = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
			DateFormat f = new SimpleDateFormat("HH:mm a", Locale.getDefault());
			d = f.format(date);
		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Check=>getDate(Date date) : ");
			e.printStackTrace();
		}
		return d;
	}

	// Getting the no. of days between 2 DateTimes
	public static int daysBetween2DateTimes(DateTime startDate, DateTime endDate) {
		int days = 0;
		try {
			days = Days.daysBetween(startDate.withTimeAtStartOfDay(),
					endDate.withTimeAtStartOfDay()).getDays();
		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Test=>daysBetween2DateTimes(DateTime startDate,DateTime endDate) : ");
			e.printStackTrace();
		}

		return days;
	}

	// Getting The ticks of DateTime After n Days from Current Date Time
	public static long getMillisecondsAfterNdaysFromCurDateTime(int days) {
		long elapsedTicks = 0L;
		try {
			// DateTime baseTime = new DateTime(1970,1,1,0,0,0);
			// DateTime baseTime = new DateTime(0001,1,1,0,0,0);//Local Time(US)
			DateTime baseTime = new DateTime(0001, 1, 1, 0, 0, 0,
					DateTimeZone.UTC);
			// DateTime currentDate = DateTime.now();
			DateTime nowInUTC = DateTime.now(DateTimeZone.UTC).plusDays(days);//
			elapsedTicks = ((nowInUTC.getMillis() - baseTime.getMillis()) * TICKS_PER_MILLISECOND);
		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Test=>getMillisecondsAfterNdaysFromCurDateTime(int days) : ");
			e.printStackTrace();
		}
		return elapsedTicks;
	}

	public static DateTime getCurrentUTCDateTime() {
		long ticks = 0;
		DateTime date = null;
		try {
			ticks = getMillisecondsFromUTCdate();
			date = new DateTime((ticks - TICKS_AT_EPOCH)
					/ TICKS_PER_MILLISECOND);
		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Test=>convertMillisecondsToDateTime(long milliseconds) : ");
			e.printStackTrace();
		}

		return date;
	}

	public static DateTime convertMillisecondsToUTCDateTime(long milliseconds) {
		DateTime date = null;
		try {
			date = new DateTime((milliseconds - TICKS_AT_EPOCH)
					/ TICKS_PER_MILLISECOND);
		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Test=>convertMillisecondsToDateTime(long milliseconds) : ");
			e.printStackTrace();
		}

		return date;
	}

	public static long getMillisecondsFromUTCdate() {
		long elapsedTicks = 0L;
		try {
			DateTime baseTime = new DateTime(0001, 1, 1, 0, 0, 0,
					DateTimeZone.UTC);
			DateTime nowInUTC = DateTime.now(DateTimeZone.UTC);//
			elapsedTicks = ((nowInUTC.getMillis() - baseTime.getMillis()) * TICKS_PER_MILLISECOND);
		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Test=>getMillisecondsFromUTCdate) : ");
			e.printStackTrace();
		}
		return elapsedTicks;
	}

	public static long getMillisecondsFromDate(int year, int month, int day,
			int hour, int minute) {
		long elapsedTicks = 0L;
		try {
			DateTime baseTime = new DateTime(0001, 1, 1, 0, 0, 0,
					DateTimeZone.UTC);
			DateTime selectedInUTC = new DateTime(year, month, day, hour,
					minute, 0, DateTimeZone.UTC);
			elapsedTicks = ((selectedInUTC.getMillis() - baseTime.getMillis()) * TICKS_PER_MILLISECOND);
		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Test=>getMillisecondsFromUTCdate) : ");
			e.printStackTrace();
		}
		return elapsedTicks;
	}

	public static int daysBetweenCurDateTimeNgivenDateTime(long ticks) {
		int days = 0;
		DateTime startDate = null, endDate = null;
		try {
			startDate = getCurrentUTCDateTime();
			endDate = convertMillisecondsToUTCDateTime(ticks);

			// days = Days.daysBetween(startDate.toLocalDate(),
			// endDate.toLocalDate()).getDays();
			days = Days.daysBetween(startDate.withTimeAtStartOfDay(),
					endDate.withTimeAtStartOfDay()).getDays();
		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Test=>daysBetween2DateTimes(DateTime startDate,DateTime endDate) : ");
			e.printStackTrace();
		}

		return days;
	}

	public static long getMillisecondsFromDevicedate() {
		long elapsedTicks = 0L;
		try {
			DateTime nowInUTC = DateTime.now(DateTimeZone.UTC);
			DateTime nowIn = DateTime.now();//
			elapsedTicks = ((nowIn.getMillis() - nowInUTC.getMillis()));
		} catch (Exception e) {
			System.out
					.println("Opps,Exception In Test=>getMillisecondsFromUTCdate) : ");
			e.printStackTrace();
		}
		return elapsedTicks;
	}

	public static long getTimeOffsetUTCminutes() {
		long offsetMinutes = 0;
		try {
			offsetMinutes = getMillisecondsFromDevicedate() / 60;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return offsetMinutes;
	}

	/**
	 * This method will check the Date respective to provided ticks is 3 days
	 * older?
	 * 
	 * @param ticks
	 * @return
	 */
	public static boolean isDateOlderThanToday(long ticks) {
		boolean flag = false;
		DateTime serverDate, currentDate = null;
		try {
			currentDate = getCurrentUTCDateTime();
			serverDate = convertMillisecondsToUTCDateTime(ticks);
			flag = serverDate.isBefore(currentDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * This method returns current date pattern set in device's date
	 * 
	 * @return pattern
	 * @param context
	 */
	private static String getLocalizedDatePattern(Context context) {
		String pattern = "yyyy/MM/dd";
		if (context != null) {
			try {
				Format dateFormat = android.text.format.DateFormat
						.getDateFormat(context);
				pattern = ((SimpleDateFormat) dateFormat).toLocalizedPattern();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return pattern;
	}

	/**
	 * This method returns current time pattern HH:mm
	 * 
	 * @return pattern
	 * @param context
	 */
	private static String getLocalizedTimePattern() {
		String pattern = "HH:mm";
		return pattern;
	}

	/**
	 * This method returns current time SDF in HH:mm format
	 * 
	 * @return pattern
	 * @param context
	 */
	private static SimpleDateFormat getTimePattern() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf;
	}

	/**
	 * This method returns current date formatted with device's date pattern:
	 * yyyy/mm/dd or mm/dd/yyyy or etc.
	 * 
	 * @return date
	 * @param context
	 */
	public static String getCurrentDate(Context context) {
		DateTime date = new DateTime();
		return date.toString(getLocalizedDatePattern(context));
	}

	/**
	 * Get current time with format HH:mm i.e, 03:50PM >> 15:50 and 03:50AM >>
	 * 03:50
	 * 
	 * @return currentTime
	 */
	public static String getCurrentTime() {
		Date date = new Date();
		String currentTime = getTimePattern().format(date);
		return currentTime;
	}

	/**
	 * This method returns date in string format
	 * 
	 * @return String
	 * @param date
	 * @param context
	 */
	public static String convertDateToString(Date date, Context context) {
		SimpleDateFormat parser = new SimpleDateFormat(
				getLocalizedDatePattern(context));
		return parser.format(date);
	}

	/**
	 * This method returns time in string format
	 * 
	 * @param time
	 * @param context
	 * @return String
	 */
	public static String convertTimeToString(Date time, Context context) {
		return getTimePattern().format(time);
	}

	/**
	 * This method returns array having day, month & year
	 * 
	 * @return String[]
	 * @param dateStr
	 * @param context
	 */
	public static String[] getSplittedDate(String dateStr, Context context) {
		String[] splittedDate = null;
		SimpleDateFormat parser = new SimpleDateFormat(
				getLocalizedDatePattern(context));
		Date date;
		try {
			date = parser.parse(dateStr);
			String month = (String) android.text.format.DateFormat.format("MM",
					date); // 06
			String year = (String) android.text.format.DateFormat.format(
					"yyyy", date); // 2013
			String day = (String) android.text.format.DateFormat.format("dd",
					date); // 20
			splittedDate = new String[3];
			splittedDate[0] = day;
			splittedDate[1] = month;
			splittedDate[2] = year;
		} catch (ParseException e) {
			LogUtil.e("Ex getSplittedDate", e.getMessage());
		}
		return splittedDate;
	}

	/**
	 * This method converts string array to integer array
	 * 
	 * @param string
	 * @return Integer
	 */
	public static int[] convertStringArrayToIntArray(String[] string) { // Note
																		// the
																		// []
																		// after
																		// the
																		// String.
		int number[] = new int[string.length];

		for (int i = 0; i < string.length; i++) {
			number[i] = Integer.parseInt(string[i]);
		}
		return number;
	}

	/**
	 * This method returns array having day, month & year
	 * 
	 * @return String[]
	 * @param timeStr
	 * @param context
	 */
	public static String[] getSplittedTime(String timeStr, Context context) {
		String[] splittedTime = null;

		splittedTime = timeStr.split(":");

		return splittedTime;
	}

	/**
	 * This method accepts day, month & year separately and returns date string
	 * in desired format
	 * 
	 * @return String
	 * @param year
	 * @param month
	 * @param day
	 * @param context
	 */
	public static String splittedDateToString(int year, int month, int day,
			Context context) {
		String dateStr = "";

		dateStr = year + "/" + month + "/" + day; // yyyy/MM/dd
		SimpleDateFormat inputParser = new SimpleDateFormat("yyyy/MM/dd");
		try {
			Date date = inputParser.parse(dateStr);
			SimpleDateFormat outputParser = new SimpleDateFormat(
					getLocalizedDatePattern(context));
			dateStr = outputParser.format(date);
		} catch (ParseException e) {
			LogUtil.e("Ex ConvertToDateString", e.getMessage());
		}

		return dateStr;
	}

	public static long getMillisecondsForSpecificDate(String dateStr,
			String timeStr, Context context) {
		long elapsedTicks = 0L;
		try {
			String specificDateStr = (dateStr + " " + timeStr).trim();
			String dateFormateInUTC = "";
			TimeZone localTimeZone = TimeZone.getDefault();
			String pattern;
			SimpleDateFormat formatter;
			SimpleDateFormat parser;
			Date specificDate = null;
			long specificDateInUTC;
			String patternZ;

			// create a new Date object using the timezone of the specified city
			pattern = getLocalizedDatePattern(context) + " "
					+ getLocalizedTimePattern();
			parser = new SimpleDateFormat(pattern);
			parser.setTimeZone(localTimeZone);
			specificDate = parser.parse(specificDateStr);
			patternZ = pattern + " " + "z'('Z')'";
			formatter = new SimpleDateFormat(patternZ);
			formatter.setTimeZone(localTimeZone);

			LogUtil.i(
					"convertLocalTimeToUTC: The Date in the local time zone ",
					formatter.format(specificDate));

			// Convert the date from the local timezone to UTC timezone
			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			dateFormateInUTC = formatter.format(specificDate);
			LogUtil.i("convertLocalTimeToUTC: The Date in the UTC time zone ",
					dateFormateInUTC);

			specificDateInUTC = formatter.parse(dateFormateInUTC).getTime();
			LogUtil.i("convertLocalTimeToUTC in Millisecond: ", ""
					+ elapsedTicks);

			// Provide hours in 24 hour format
			DateTime baseTime = new DateTime(0001, 1, 1, 0, 0, 0,
					DateTimeZone.UTC);

			elapsedTicks = ((specificDateInUTC - baseTime.getMillis()) * TICKS_PER_MILLISECOND);
		} catch (Exception e) {
			LogUtil.e("Exception thrown for getMillisecondsForSpecificDate2: ",
					e.getMessage());
		}
		return elapsedTicks;
	}

	public static long getMillisecondsAfterNdaysFromAspecificDate(
			String dateStr, String timeStr, int days, Context context) {
		long elapsedTicks = 0L;
		try {
			String specificDateStr = (dateStr + " " + timeStr).trim();
			String dateFormateInUTC = "";
			TimeZone localTimeZone = TimeZone.getDefault();
			String pattern;
			SimpleDateFormat formatter;
			SimpleDateFormat parser;
			Date specificDate = null;
			long specificDateInUTC;
			String patternZ;

			// create a new Date object using the timezone of the specified city
			pattern = getLocalizedDatePattern(context) + " "
					+ getLocalizedTimePattern();
			parser = new SimpleDateFormat(pattern);
			parser.setTimeZone(localTimeZone);
			specificDate = parser.parse(specificDateStr);
			patternZ = pattern + " " + "z'('Z')'";
			formatter = new SimpleDateFormat(patternZ);
			formatter.setTimeZone(localTimeZone);

			LogUtil.i(
					"convertLocalTimeToUTC: The Date in the local time zone ",
					formatter.format(specificDate));

			// Convert the date from the local timezone to UTC timezone
			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			dateFormateInUTC = formatter.format(specificDate);
			LogUtil.i("convertLocalTimeToUTC: The Date in the UTC time zone ",
					dateFormateInUTC);

			Calendar cal = Calendar.getInstance();
			cal.setTime(specificDate);
			cal.add(Calendar.DATE, days);
			String convertedDate = formatter.format(cal.getTime());

			specificDateInUTC = formatter.parse(convertedDate).getTime();
			LogUtil.i("convertLocalTimeToUTC in Millisecond: ", ""
					+ elapsedTicks);

			// Provide hours in 24 hour format
			DateTime baseTime = new DateTime(0001, 1, 1, 0, 0, 0,
					DateTimeZone.UTC);

			elapsedTicks = ((specificDateInUTC - baseTime.getMillis()) * TICKS_PER_MILLISECOND);
		} catch (Exception e) {
			LogUtil.e("Exception thrown for getMillisecondsForSpecificDate2: ",
					e.getMessage());
		}
		return elapsedTicks;
	}

	/**
	 * convert given ticks to UTC ticks
	 * 
	 * @param ticks
	 *            ticks to be converted
	 * @param context
	 * @return ticks in UTC
	 */
	public static long toDatesTicks(long ticks, Context context) {
		Date date = null;
		try {

			date = convertMillisecondsToUTCDate(ticks);
			String dateStr = convertDateToString(date, context);
			String timeStr = convertTimeToString(date, context);

			return getMillisecondsForSpecificDate(dateStr, timeStr, context);

		} catch (Exception e) {
			LogUtil.e("Exception thrown for toDatesTicks: ", e.getMessage());
			return 0;
		}
	}

	/**
	 * @return Date in milli seconds
	 * @param dateView
	 * @param timeView
	 * @param context
	 * @throws NumberFormatException
	 */
	public static long getDateInTicks(TextView dateView, TextView timeView,
			Context context) throws NumberFormatException {
		String dateViewStr = dateView.getText().toString().trim();
		String timeViewStr = timeView.getText().toString().trim();
		return DateTimeUTC.getMillisecondsForSpecificDate(dateViewStr,
				timeViewStr, context);
	}

	/**
	 * @return Date in milli seconds after n days
	 * @param dateView
	 * @param timeView
	 * @param days
	 * @param context
	 * @throws NumberFormatException
	 */
	public static long getDateInTicksAfterNdays(TextView dateView,
			TextView timeView, int days, Context context)
			throws NumberFormatException {

		String dateViewStr = dateView.getText().toString().trim();
		String timeViewStr = timeView.getText().toString().trim();
		return DateTimeUTC.getMillisecondsAfterNdaysFromAspecificDate(
				dateViewStr, timeViewStr, days, context);
	}

	/**
	 * This method returns the current date with time using
	 * {@link SimpleDateFormat} format --> dd-MM-yyyy_HH:mm:ss
	 * 
	 * @return current date and time
	 */
	public static String getCurrentDateWithTime() {
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss",
				Locale.ENGLISH).format(Calendar.getInstance().getTime());
		return timeStamp;
	}

	/**
	 * This method returns the current date using {@link SimpleDateFormat}
	 * format --> yyyy_MM_dd
	 * 
	 * @return current date and time
	 */
	public static String getCurrentDate() {
		String timeStamp = "";
		try {
			timeStamp = new SimpleDateFormat("yyyy_MM_dd", Locale.ENGLISH)
					.format(Calendar.getInstance().getTime());
		} catch (Exception e){
			e.printStackTrace();
		}
		return timeStamp;
	}

	/**
	 * This method returns the current date using {@link SimpleDateFormat}
	 * format --> yyyy_MM_dd-hh_mm_ss
	 *
	 * @return current date and time
	 */
	public static String getCurrentDataAndTime() {
		String timeStamp = "";
		try {
			timeStamp = new SimpleDateFormat("yyyy_MM_dd-hh_mm_ss", Locale.ENGLISH)
					.format(Calendar.getInstance().getTime());
			return timeStamp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return timeStamp;
	}

	/**
	 * get current date in milliseconds
	 * 
	 * @return current date in milliseconds
	 */
	public static long getCurrentDateMilisecond() {
		return new Date().getTime();
	}

	/**
	 * get the given date with given minutes offset.
	 * 
	 * @param minOffset
	 *            minutes offset required
	 * @param date
	 *            date to changed
	 * @param isAdding
	 *            if true offset minutes are added, if false offset minutes are
	 *            subtracted
	 * @return Date with offset minutes
	 */
	public static DateTime getDateWithMinituesOffset(int minOffset,
			DateTime date, boolean isAdding) {
		if (!isAdding)
			return new DateTime(date).minusMinutes(5);
		else
			return new DateTime(date).plusMinutes(5);
	}

	/**
	 * Get UTC date object from given time and date parameters
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @param hours
	 * @param mins
	 * @param seconds
	 * @return Date in UTC
	 */
	public static DateTime getDate(int year, int month, int day, int hours,
			int mins, int seconds) {
		DateTime date = new DateTime(year, month, day, hours, mins, seconds,
				DateTimeZone.UTC);
		return date;
	}

	/**
	 * This method returns current date-time as a JodaDate
	 * 
	 * @return current date in "y M d H:m:s" format.
	 */
	public static DateTime getCurrentJodaDate() {
		DateTime currentDate = new DateTime(DateTimeZone.UTC);
		DateTimeFormatter dateTimeFormatter = DateTimeFormat
				.forPattern("y M d H:m:s");
		String[] dayInfo = dateTimeFormatter.print(currentDate).split(" ");
		String[] timeInfo = dayInfo[3].split(":");
		currentDate = DateTimeUTC
				.getDate(Integer.parseInt(dayInfo[0]),
						Integer.parseInt(dayInfo[1]),
						Integer.parseInt(dayInfo[2]),
						Integer.parseInt(timeInfo[0]),
						Integer.parseInt(timeInfo[1]), 0);
		return currentDate;
	}

	/**
	 * This method compares two dates and returns correct code for identifying
	 * which comes first
	 * 
	 * @param dateOne
	 *            first date to be compared
	 * @param dateTwo
	 *            second date to be compared
	 * @return 0 if both are equal, 1 if First date comes before Second date and
	 *         2 if Second date comes before First date
	 */
	public static int compareDates(DateTime dateOne, DateTime dateTwo) {
		boolean isBefore = dateOne.isBefore(dateTwo);
		boolean isAfter = dateOne.isAfter(dateTwo);

		// if both are equal (same)
		if (!isBefore && !isAfter) {
			return 0;
		}
		// if first is before
		else if (isBefore) {
			return 1;
		}
		// if second is before
		else
			return 2;
	}
}
