package com.goodyear.ejob.util;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.goodyear.ejob.R;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.interfaces.InactivityHandler;

/**
 * @author pragati.pj
 */
public class OfflinePinDialog extends Activity implements OnClickListener, InactivityHandler {

    Button ok_btn;
    private static String pin;
    EditText editPin;
    Context mContext;
	final String TAG = "OfflinePinDialog";
    private ImageView mPinVisibility;
    private boolean isTextVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_offline_pin);

        editPin = (EditText) findViewById(R.id.editTextDialogUserInput);

        ok_btn = (Button) findViewById(R.id.dialogButtonOK);
        mPinVisibility = (ImageView) findViewById(R.id.dialog_pin_visibility);
        ok_btn.setOnClickListener(this);

        editPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0) {
                    mPinVisibility.setVisibility(View.VISIBLE);
                } else {
                    mPinVisibility.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mPinVisibility.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editPin.getText().toString().length() > 0 && !isTextVisible) {
                    isTextVisible = true;
                    mPinVisibility.setImageResource(R.drawable.visibility_off);
                    editPin.setTransformationMethod(null);
                } else {
                    isTextVisible = false;
                    mPinVisibility.setImageResource(R.drawable.visibility);
                    editPin.setTransformationMethod(new PasswordTransformationMethod());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.dialogButtonOK:
                LogUtil.d(TAG," onClick ");
                pin = editPin.getText().toString();
                if (pin == null || TextUtils.isEmpty(pin) || pin == "" || pin.length()<6 || pin.length()>6) {
                    CommonUtils.notify(Constants.SET_OFFLINE_PIN, this);
                } else {
                    CommonUtils.notify(Constants.SET_PIN_SUCCESSFUL, this);
                    if(pin.length() == 6) {
                        updatePinToDB(pin);
                    }
                    this.finish();
                }
                break;
        }

    }

    private void updatePinToDB(String pin){
        DatabaseAdapter dbAdapter = new DatabaseAdapter(this);
        try {
            dbAdapter.open();
            dbAdapter.updateAccountWithPin(pin);
            dbAdapter.copyFinalDB();
        }catch (Exception e) {
            LogUtil.d(TAG ," updatePinToDB Exception "+e);
        } finally {
            dbAdapter.close();
        }
    }

    public void logUserOutDueToInactivity() {
        InactivityUtils.logoutFromActivityBelowEjobForm(this);
    }

    @Override
    public void onBackPressed() {
        //do nothing
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (MotionEvent.ACTION_OUTSIDE == event.getAction()) {
            //do nothing
        }
        return true;
    }

}