
package com.goodyear.ejob.util;

import java.io.ByteArrayOutputStream;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.fragment.VehicleSkeletonFragment;
import com.goodyear.ejob.job.operation.helpers.TireImageItem;

/**
 * @author amitkumar.h
 *
 */
public class CameraUtility {

	public static final String TAG = CameraUtility.class.getSimpleName();
	
		/**
		 * Get bitmap image in bytes
		 */
		public static byte[] getByteFromBitmap(Bitmap bmp, int quality) {
			byte[] byteArray = null;
			try {
				if(null == bmp){
					return null;
				}
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bmp.compress(CompressFormat.JPEG, quality, stream);
				byteArray = stream.toByteArray();
				return byteArray;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
		/**
		 * Get Current Image Count
		 */
		public static int getImageCount() {
			int curntImgCount = 0;
			if (null != Constants.sBitmapPhoto1) {
				curntImgCount++;
			}
			if (null != Constants.sBitmapPhoto2) {
				curntImgCount++;
			}
			if (null != Constants.sBitmapPhoto3) {
				curntImgCount++;
			}
			return curntImgCount;
		}


	/**
	 * Reseting Parameters
	 */
	public static void resetParams() {
		if(null != Constants.sBitmapPhoto1){
		Constants.sBitmapPhoto1 = null;
		}
		if(null != Constants.sBitmapPhoto2){
		Constants.sBitmapPhoto2 = null;
		}
		if(null != Constants.sBitmapPhoto3){
		Constants.sBitmapPhoto3 = null;
		}
		Constants.DAMAGE_NOTES1 = "";
		Constants.DAMAGE_NOTES2 = "";
		Constants.DAMAGE_NOTES3 = "";
		
	}
}
