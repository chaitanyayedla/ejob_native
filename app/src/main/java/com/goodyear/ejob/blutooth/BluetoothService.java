package com.goodyear.ejob.blutooth;

/**
 * @author sandeep.p
 * 
 */
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.Set;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.provider.Settings;
import android.text.TextUtils;
import android.widget.Toast;

import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.LogUtil;

/*
 * Class for all bluetooth service related functions
 */
public class BluetoothService {

	private BluetoothAdapter myBluetoothAdapter;
	private BluetoothDevice connectedDevice;
	private BluetoothSocket connectedSocket;
	private static final UUID uuidSpp = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");
	private OutputStream outputStream;
	private InputStream inputStream;
	private int readBufferPosition = 0;
	private boolean stopWorker = false;
	private Thread workerThread;
	private String address;
	private int type;
	private Context ctx;

	int EPCheader;
	int filter;
	int partition;
	boolean michelin = false;
	int companyPrefix;
	int cai;
	Long serialNo;
	String EANCode;
	Set<BluetoothDevice> pairedDevices;
	int retryCount = 0;
	int RETRY_MAXLIMIT = Constants.RETRY_MAXLIMIT;
	
	/**
	 * Vibrator object 
	 */
	private Vibrator mVibrator = null;
	/**
	 * Media Player Object 
	 */
	private static MediaPlayer mMediaPlayer = null;

	/*
	 * Constructor to initialize bluetooth adapter
	 */
	public BluetoothService(Context ctx) {
		this.ctx = ctx;
		myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (myBluetoothAdapter == null) {
			Toast.makeText(ctx, Constants.sLblDeviceDontSupportBT,
					Toast.LENGTH_LONG).show();
		} else {
			if (!myBluetoothAdapter.isEnabled()) {
				myBluetoothAdapter.enable();
			}
		}
		
		/**
		 * Enhancement code 393 and 394 issues
		 * TODO : uncomment mVibra	tor and mMediaPlayer object declaration
		 */
		mVibrator = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
		mMediaPlayer = buildMediaPlayer();
	}

	// Method to notify any of tyre operation regarding its type and value
	public void sendData(String value, int type) {
		if(TextUtils.isEmpty(value)) {
			return;
		}
		Intent intent = new Intent("BLUETOOTH_SENDER");
		if (type == 0) {
			try {
				Double val = CommonUtils.parseDouble(value);
				if(val <= 30) {
					/**
					 * Enhancement code 393 and 394 issues
					 * TODO : uncomment makeVibrateAndBeep()
					 */
					makeVibrateAndBeep();
					intent.putExtra("BT_DATA_NSK", value);					
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		} else if (type == 1) {
			/**
			 * Enhancement code 393 and 394 issues
			 * TODO : uncomment makeVibrateAndBeep()
			 */
			makeVibrateAndBeep();
			intent.putExtra("BT_DATA_PRE", value);
		} else if (type == 2)
			intent.putExtra("BT_DATA_RFID", value);
		/*else
			intent.putExtra("BT_CONN_STATUS", value);*/
		else if(type == 3)
			intent.putExtra("BT_RETRY_FAIL", value);
		ctx.sendBroadcast(intent);
	}
	
	/**
	 * Method to notify the user regarding the pairing status
	 * return status
	 *  -1 : Bluetooth not enabled / BluetoothAdapter may be null
	 *  1 : TLOGIK device found
	 *  2 : Please pair a TLOGIK device
	 *  3 : No paired devices
	 *  
	 */
	public int getPairedStatus() {
		if (myBluetoothAdapter != null) {
			//If Bluetooth not enabled return -1, then it's not try to connect device
			if(!myBluetoothAdapter.isEnabled()) {
				return -1;
			}			
			pairedDevices = myBluetoothAdapter.getBondedDevices();
			if (pairedDevices.size() > 0) {
				for (BluetoothDevice device : pairedDevices) {
					if(device.getName().startsWith("TLOGIK") || device.getName().startsWith("Pneu Logic")) {
						connectedDevice = device;
						address = connectedDevice.getAddress();
						LogUtil.i("Connected Probe device details :: " , device.getName() + " :: " + address);
						return 1;
					}
				}				
				return 2;
			} else {
				return 3;
			}
		} else
			return -1;
	}

	/**
	 * 
	 */
	public void stopReading() {
		stopWorker = true;
		LogUtil.e("BT", "****** Stop Reading> "+stopWorker);
	}
	public void startReading(){
		stopWorker = false;
		LogUtil.e("BT", "****** Start Reading> "+stopWorker);
	}
	/*
	 * Method to connect to bluetooth device based on type of data required
	 * @Parameter - type
	 */
	// type 0 - NSK, Pressure
	// type 2 - RFID
	public synchronized void connectToDevice(int type) {
		LogUtil.i("BT", "****** CoonectToDevice stopWorker > "+stopWorker + ":isAsyncTaskCanceled :: :> "+isAsyncTaskCanceled);
		
		 
		if(stopWorker && isAsyncTaskCanceled){
			isAsyncTaskCanceled = false;
			if (connectedSocket!=null && connectedSocket.isConnected()) {
				try {
					connectedSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			connectedSocket = null;
			return;
		}
		isAsyncTaskCanceled = false;
		BluetoothSocket tempSocket = null;
		boolean isExceptionThrown = false;;
		this.type = type;
		retryCount++;
		try {
			
			if (connectedSocket!=null && connectedSocket.isConnected()) {
				
				connectedSocket.close();
			}

			connectedDevice = myBluetoothAdapter.getRemoteDevice(address);
			connectedSocket = connectedDevice
					.createInsecureRfcommSocketToServiceRecord(uuidSpp);
			tempSocket = connectedSocket;
			LogUtil.e("BT", "****** Inside CoonectToDevice stopWorker > "
					+ stopWorker + "connectedSocket:>" + connectedSocket);
			// System.out.println("connectToDevice::: "+stopWorker +
			// " :connectedSocket:"+connectedSocket);
			if (myBluetoothAdapter.isDiscovering()) {
				myBluetoothAdapter.cancelDiscovery();
			}
			if (connectedSocket.isConnected()) {
				connectedSocket.close();
			}
			connectedSocket.connect();
			beginListeningData(type);
		} /*catch (IOException e) {			
			if (retryCount < RETRY_MAXLIMIT) {
				connectToDevice(type);
			} else {
				e.printStackTrace();
				sendData("false", 3);
			}
		}*/ catch(Exception e) {
			isExceptionThrown = true;
			LogUtil.e("BT", "****** Inside Exception  > " + " Exception: "+e.getMessage() + ":connectedSocket>"+connectedSocket);
//			if (retryCount < RETRY_MAXLIMIT)
//				connectToDevice(type);
//			else {
//				e.printStackTrace();
//				sendData("false", 3);
//			}
		}finally{
			if(!isExceptionThrown){
				connectedSocket = tempSocket;
			}
		}
		
		
		if(isExceptionThrown){
			try {
				Thread.sleep(10000L);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			{
				if (connectedSocket!=null && connectedSocket.isConnected()) {
					try {
						connectedSocket.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				connectedSocket = null;
			//	if(stopWorker)
				connectToDevice(type);
			}
		}
		
	}
	

	/*
	 * Method to listen to bluetooth data continuously based on type of data
	 * required
	 * @Parameter - type
	 */
	// type 0 - NSK, Pressure
	// type 2 - RFID
	boolean isOpen = false; 
	public void beginListeningData(final int type) {
		LogUtil.v("BTService", "### Started beginListeningData  > "+stopWorker);
		try {
			final byte delimiter = 10;
			final byte[] readBuffer = new byte[1024];
			//workerThread = null;
			if(workerThread!=null && !workerThread.isInterrupted()){
				workerThread.interrupt();
			}
			workerThread = new Thread(new Runnable() {
				public void run() {
					if (connectedSocket != null && connectedSocket.isConnected()) {
						try {
							outputStream = connectedSocket.getOutputStream();
							inputStream = connectedSocket.getInputStream();
							if (type == 2) {
								String s = "GR\r";
								outputStream.write(s.getBytes());
								outputStream.flush();
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					isOpen = true;
					while (!Thread.currentThread().isInterrupted() && !stopWorker && null != connectedSocket && connectedSocket.isConnected()) {
						try {
							//LogUtil.d("BT", "### Inside Listen isInterrupted & stopWorker > "+stopWorker);
							if (inputStream != null &&!stopWorker) {
								int bytesAvailable = inputStream.available();
								if (bytesAvailable > 0) {
									
									byte[] packetBytes = new byte[bytesAvailable];
									inputStream.read(packetBytes);
									for (int i = 0; i < bytesAvailable; i++) {
										byte b = packetBytes[i];
										if (b == delimiter) {
											byte[] encodedBytes = new byte[readBufferPosition];
											System.arraycopy(readBuffer, 0,
													encodedBytes, 0,
													encodedBytes.length);
											final String data = new String(
													encodedBytes, "US-ASCII");
											readBufferPosition = 0;
											if (data.startsWith("T") && type == 0) {
												String temp = data.substring(1);
												sendData(temp, 0);
											} else if (data.startsWith("P")
													&& type == 0) {
												String temp = data.substring(1);
												sendData(temp, 1);
											} else if (data.startsWith("G")
													&& type == 2) {
												String temp = data.substring(2);
												temp = Long
														.toString(decodeRFIDData(temp));
												sendData(temp, 2);
											}
										} else {
											readBuffer[readBufferPosition++] = b;
										}
									}
								}
							}
						} catch (IOException ex) {
							ex.printStackTrace();
							stop();
							stopWorker = true;
						} catch(NullPointerException ne) {
							LogUtil.d("beginListeningData NullPointer Ex :: " , ne.getMessage());
							ne.printStackTrace();
							stop();
							stopWorker = true;
						}
					}
				}
			});
			workerThread.start();
		} catch(NullPointerException nep) {
			LogUtil.d("beginListeningData NullPointerException :: " , nep.getMessage());
			nep.printStackTrace();
			stop();
			stopWorker = true;
		} catch (Exception e) {
			LogUtil.d("beginListeningData Exception :: " , e.getMessage());
			e.printStackTrace();
			stop();
			stopWorker = true;
		}
	}

	// Method to decode RFID Data from the Bluetooth device.
	private Long decodeRFIDData(String encoded) {
		String binNumber = hex2bin(encoded);
		binNumber = "00" + binNumber;
		int companyNumberLength = 0;
		int caiLength = 0;
		EPCheader = Integer.parseInt(binNumber.substring(0, 8), 2);
		filter = Integer.parseInt(binNumber.substring(8, 11), 2);
		partition = Integer.parseInt(binNumber.substring(11, 14), 2);
		michelin = false;

		if ((encoded.substring(0, 9) == "301854AAC" || encoded.substring(0, 9) == "307854AAC")) {
			michelin = true;
		} else
			michelin = false;

		companyNumberLength = getCompanyLength(partition);
		caiLength = getCAILength(partition);

		companyPrefix = Integer.parseInt(
				binNumber.substring(14, 14 + companyNumberLength), 2);
		cai = Integer.parseInt(
				binNumber.substring(14 + companyNumberLength, 14
						+ companyNumberLength + caiLength), 2);
		serialNo = Long.parseLong(binNumber.substring(14 + companyNumberLength
				+ caiLength, binNumber.length()), 2);
		EANCode = getEANCode(String.valueOf(companyPrefix)
				+ String.valueOf(cai));
		return serialNo;
	}

	private String getEANCode(String str) {
		int iSum = 0;
		int iDigit = 0;
		for (int i = str.length() - 1; i >= 0; i--) {
			Character c = str.charAt(i);
			iDigit = Character.getNumericValue(c);
			if (i % 2 != 0) { // odd
				iSum += iDigit * 3;
			} else { // even
				iSum += iDigit * 1;
			}
		}
		int iCheckSum = (10 - (iSum % 10)) % 10;
		return (new Integer(iCheckSum)).toString();
	}

	private String hex2bin(String hex) {
		String bin;
		bin = new BigInteger(hex, 16).toString(2);
		return bin;
	}

	private int getCompanyLength(int partition) {
		int companyNumberLength = 0;

		switch (partition) {
		case 0:
			companyNumberLength = 40;
			break;
		case 1:
			companyNumberLength = 37;
			break;
		case 2:
			companyNumberLength = 34;
			break;
		case 3:
			companyNumberLength = 30;
			break;
		case 4:
			companyNumberLength = 27;
			break;
		case 5:
			companyNumberLength = 24;
			break;
		case 6:
			companyNumberLength = 20;
			break;
		}
		return companyNumberLength;
	}

	private int getCAILength(int partition) {
		int caiLength = 0;

		switch (partition) {
		case 0:
			caiLength = 4;
			break;
		case 1:
			caiLength = 7;
			break;
		case 2:
			caiLength = 10;
			break;
		case 3:
			caiLength = 14;
			break;
		case 4:
			caiLength = 17;
			break;
		case 5:
			caiLength = 20;
			break;
		case 6:
			caiLength = 24;
			break;
		}
		return caiLength;
	}

	public void stop() {
			try {
				stopWorker = true;
				LogUtil.i("Stop", "stopWorker::: "+stopWorker + " :connectedSocket:"+connectedSocket);
				if (outputStream != null )
					outputStream.close();
				if (inputStream != null)
					inputStream.close();
				if (null != connectedSocket && connectedSocket.isConnected())
					connectedSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch(Exception e) {
				LogUtil.d("BluetoothService Ex : ", e.getMessage());
			}
		connectedSocket = null;
		//connectedDevice = null;
		//outputStream = null;
		//inputStream = null;
		//myBluetoothAdapter = null;
		//workerThread = null;
		try {
		/**
		 * Enhancement code 393 and 394 issues
		 * TODO : uncomment followig code related to mVibrator and mMediaPlayer objects
		 */
		if(null != mVibrator) {
			mVibrator.cancel();
			mVibrator = null;
		}		
		if(null != mMediaPlayer) {
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return boolean variable is device connected or not
	 */
	public boolean isConnected() {
		boolean isConnected = false;
		if(null != connectedSocket && connectedSocket.isConnected()) {
			isConnected =  true;
		}
		//If connected to the device the start the thread to read.
		if(isConnected){
			//startReading();
		}
		LogUtil.i("Bluetooth Connection Status :: " , isConnected + "");
		return isConnected;
	}
	
	
	/**
	 * Make vibration and beep sound 
	 */
	private void makeVibrateAndBeep() {
		try {
			AudioManager am = (AudioManager) ctx.getSystemService(Context.AUDIO_SERVICE);
			switch(am.getRingerMode()) {
				case AudioManager.RINGER_MODE_SILENT: //No Sound and No Vibrate
					break;
				case AudioManager.RINGER_MODE_VIBRATE: // only Vibrate
					makeVibrate();
					break;
				case AudioManager.RINGER_MODE_NORMAL: // Ring and may or may not vibrate
					makeSound();
					/*vibrate_when_ringing is On return "1" otherwise return 0 */
					int val = Settings.System.getInt(ctx.getContentResolver(), "vibrate_when_ringing", 0);
					/* For tablet*/
					boolean isVibrationEnabled = Settings.System.getInt(ctx.getContentResolver(), Settings.System.HAPTIC_FEEDBACK_ENABLED, 1) != 0;
					if(val != 0 || isVibrationEnabled) {
						makeVibrate();
					}
					break;
			}
		} catch(Exception e) {
			LogUtil.e("BluetoothService :: makeVibrateAndBeep", e.getMessage());
		}
	}
	
	/**
	 * make vibrate after receive the NSK value
	 */
	private void makeVibrate() {
		//Vibrate the device
		new Thread() {
			public void run() {
				try {
					if(null != mVibrator && mVibrator.hasVibrator()) {
						mVibrator.cancel();
					}
					if(null != mVibrator) {
						mVibrator.vibrate(1000); // vibrating 1 sec
					}
				} catch(Exception e) {
					LogUtil.e("BluetoothService :: Vibration", e.getMessage());
				}
			}
		}.start();
	}
	
	/**
	 * make sound while after receive the NSK value 
	 */
	private void makeSound() {
		//Play Beep sound
		//start a new sound 
		//bug 385
		mMediaPlayer = buildMediaPlayer();
		new Thread() {
			public void run() {
				try {
					LogUtil.i("media-----", "creating new ");
					if (null != mMediaPlayer) {
						mMediaPlayer.start();
						LogUtil.i("media-----", "starting new ");
					}
				} catch(Exception e) {
					LogUtil.e("BluetoothService :: play sound :: IllegalStateException", e.getMessage());
				}
			}
		}.start();
	}
	
	/**
	 *building mediaplayer object
	 */
	private MediaPlayer buildMediaPlayer() {
		MediaPlayer mediaPlayer = new MediaPlayer();
		float BEEP_VOLUME = 1.0f;
		try {
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
			mediaPlayer.setOnCompletionListener(mediaPlayerCompletionListner);
			mediaPlayer.setOnErrorListener(mediaPlayerErrorListner);
			Uri notificationURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			mediaPlayer.setDataSource(ctx, notificationURI);
			mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
			mediaPlayer.prepare();
			return mediaPlayer;
		} catch (Exception e) {
			LogUtil.e("BluetoothService buildMediaPlayer()", "Exception ::: "+ e.getMessage());
			mediaPlayer.release();
		}
		return null;
	}
	
	/**
	 * Listener for beep sound completion 
	 */
	OnCompletionListener mediaPlayerCompletionListner = new OnCompletionListener() {
		@Override
		public void onCompletion(MediaPlayer mp) {
			mp.seekTo(0);
		}
	};
	
	/**
	 * Listener for media player error happens
	 */
	OnErrorListener mediaPlayerErrorListner = new OnErrorListener() {
		
		@Override
		public boolean onError(MediaPlayer mp, int what, int extra) {
			mp.release();
			mMediaPlayer = null;
			return true;
		}
	};

	/**
	 * @return
	 */
	public boolean isWorkerThread() {
		LogUtil.e("BT", "Getting: stopWorker> "+stopWorker);
		return stopWorker;
	}

	/**
	 * @param isWorkerThread
	 */
	public void setWorkerThread(boolean isWorkerThread) {
		LogUtil.e("BT", "setting: stopWorker > "+stopWorker);
		stopWorker = isWorkerThread;
		
	}

	/**
	 * 
	 */
	public void resetSocket() {
		connectedSocket = null;
		
	}

	boolean isAsyncTaskCanceled = false;
	/**
	 * @param b
	 */
	public void asyncCancel(boolean b) {
		LogUtil.i("BT", ":isAsyncTaskCanceled:"+isAsyncTaskCanceled );
		isAsyncTaskCanceled = b;
		
	}
}
