package com.goodyear.ejob;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Bundle;
import android.app.Activity;
import android.os.StatFs;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.service.UserInactivityService;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.FileOperations;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.OfflinePinDialog;
import com.goodyear.ejob.util.Validation;
import com.goodyear.ejob.GoodYear_eJob_MainActivity;

import java.io.File;
import java.io.IOException;

public class EjobLandingPage extends Activity implements InactivityHandler {
    private final String TAG = "EjobLandingPage";
    private AlertDialog mLogoutPopUp;
    private static String mLoginTimeDatabasePath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejob_landing_page);
        SharedPreferences sharedPref = getSharedPreferences(Constants.FIRST_TIME_LOGIN_PREFERENCE, getApplicationContext().MODE_PRIVATE);
        Boolean isFirstTime = sharedPref.getBoolean(Constants.FIRST_TIME_LOGIN_KEY + "_" + Constants.VALIDATE_USER, false);
        LogUtil.d(TAG, "1 Constants.VALIDATE_USER : " + Constants.VALIDATE_USER);
        LogUtil.d(TAG, " isFirstTime boolean : " + isFirstTime);

        try {
            if (Validation.isNetworkAvailable(this) && isFirstTime) {
                LogUtil.d(TAG, " Db doesn't exist, showing offline pin dialog");
                Intent intent = new Intent(EjobLandingPage.this, OfflinePinDialog.class);
                startActivity(intent);
            } else {
                LogUtil.d(TAG, " Db exists NOT showing offline pin dialog");
            }
        } catch (Exception e) {
            Log.d(TAG, "Exception " + e);
        } finally {
            SharedPreferences.Editor editor = sharedPref.edit();
            LogUtil.d(TAG, "2 Constants.VALIDATE_USER : " + Constants.VALIDATE_USER);
            editor.putBoolean(Constants.FIRST_TIME_LOGIN_KEY + "_" + Constants.VALIDATE_USER, false);
            editor.commit();
        }
    }

    public void logUserOutDueToInactivity() {
        InactivityUtils.logoutFromActivityBelowEjobForm(this);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        LogUtil.d(TAG, "pressed back button");
        logoutPopUp();
    }

    private void logoutPopUp() {
        try {
            LayoutInflater li = LayoutInflater.from(EjobLandingPage.this);
            View promptsView = li
                    .inflate(R.layout.dialog_logout_confirmation, null);
            LinearLayout rootNode = (LinearLayout) promptsView
                    .findViewById(R.id.layout_root);
            TextView infoTv = (TextView) promptsView.findViewById(R.id.logout_msg);
            infoTv.setText(R.string.lbl_logout_msg);
            rootNode.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    InactivityUtils.updateActivityOfUser();
                }
            });
            LogUtil.TraceInfo(TAG, "Dialog", "Logout", false, true, false);

            final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(
                    EjobLandingPage.this);
            alertDialogBuilder.setView(promptsView);
            alertDialogBuilder.setCancelable(false).setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Constants._CHECKFORLOGINAUTOFILL = true;
                            DatabaseAdapter db = new DatabaseAdapter(
                                    getApplicationContext());
                            //db.createDatabase();
                            db.open();
                            LogUtil.TraceInfo(TAG, "Dialog", "BT - Yes", false, true, false);
                            try {
                                db.copyFinalDB();
                            } catch (SQLException | IOException e) {
                                e.printStackTrace();
                            }
                            db.close();
                            // stop inactivity service
                            InactivityUtils.stopUserActivityUpdates();
                            stopService(new Intent(EjobLandingPage.this,
                                    UserInactivityService.class));

                            Intent intent = new Intent(EjobLandingPage.this,
                                    GoodYear_eJob_MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            alertDialogBuilder.updateInactivityForDialog();
                        }
                    });

            alertDialogBuilder.setCancelable(false).setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            alertDialogBuilder.updateInactivityForDialog();
                            LogUtil.TraceInfo(TAG, "Logout", "BT - No", false, true, false);
                            dialog.cancel();

                        }
                    });
            mLogoutPopUp = alertDialogBuilder.create();
            mLogoutPopUp.show();
        } catch (Exception e) {
            LogUtil.d(TAG,"...... exception...."+e);
        }
    }

    public boolean dbExists() throws Exception {

        SharedPreferences preferences = getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);
        if (preferences.getString(Constants.DATABASE_LOCATION, "not found")
                .equals(Constants.EXTERNAL_DB_LOCATION)) {
            String sdCardPath = System.getenv("SECONDARY_STORAGE");
           /* try {
                StatFs stat = new StatFs(sdCardPath);
                // if sd card is not available
                if (0 == stat.getAvailableBlocks()) {
                    return canChangeSDCardLocationInSettings();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return canChangeSDCardLocationInSettings();
            }*/

            String sdCardList[] = sdCardPath.split(":");
            String path = sdCardList[0]
                    + Constants.PROJECT_PATH_FOR_BELOW_KITKAT;

            FileOperations.moveFile(Constants.PROJECT_PATH + "/"
                    + Constants.USER + ".db3", path, Constants.USER + ".db3");
            /*Variable to stores database path during Login*/
            mLoginTimeDatabasePath = path + "/" + Constants.USER + ".db3";

            return fileExist(path + "/" + Constants.USER + ".db3");
        }
        // phone memory
        else {
            String path = Constants.PROJECT_PATH + "/" + Constants.USER
                    + ".db3";
            /*Variable to stores database path during Login*/
            mLoginTimeDatabasePath = path;
            return fileExist(path);
        }
    }

    private boolean fileExist(String path) {
        File tempFile = new File(path);
        System.out.println(tempFile.getAbsolutePath());
        return tempFile.exists();
    }
}
