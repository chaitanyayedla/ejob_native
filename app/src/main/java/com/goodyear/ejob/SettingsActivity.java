package com.goodyear.ejob;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.StatFs;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.inactivity.EjobAlertDialog;
import com.goodyear.ejob.inactivity.InactivityUtils;
import com.goodyear.ejob.inactivity.LogoutHandler;
import com.goodyear.ejob.interfaces.InactivityHandler;
import com.goodyear.ejob.sync.Constant;
import com.goodyear.ejob.util.CommonUtils;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DecimalDigitsInputFilterNSK;
import com.goodyear.ejob.util.FileOperations;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.Security;
import com.goodyear.ejob.util.Settings;
import com.goodyear.ejob.util.SettingsLoader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author amitkumar.h
 * @version 1.0
 *          <p/>
 *          SettingsActivity provides the interface to change the user
 *          preferences like Language, Pressure unit, Database file location.
 */
public class SettingsActivity extends Activity implements InactivityHandler {

    private static final String LOG_TAG = SettingsActivity.class
            .getSimpleName();
    private String mSettingRfIdTimeout;
    /**
     * Contains app version name
     */
    private String mSettingAppVersion;
    /**
     * Contains new language selected in mLanguageSpinner
     */
    private String mChangedLanguage;
    /**
     * Contains new database file location selected in mDBSpinner
     */
    private String mChangedDBLocation;
    private String mChangedRFID;
    /**
     * Contains new pressure unit selected in mPressureUnitSwitch
     */
    private String mChangedPressureUnit;
    /**
     * Contains new torque unit selected in mToruqeSettingSwitch
     */
    private String mChangedTorqueSetting;
    private String mChangedRFIDTimeOut;
    private String mOriginalDbLocation;
    private boolean mSecondLoad;
    String strsettingssaved, strERROR_DBFILE_NOT_MOVED, strNODISKSPACE;
    /**
     * Saves all the new changes into the database
     */
    private MenuItem mSaveButton;
    /**
     * Holds the selected language index in the spinner
     */
    private int mLangSelected;
    /**
     * Holds the selected database file location index in the spinner
     */
    private int mDbSelected;
    private int mRfIdSelected;
    /**
     * Holds the selected pressure unit.
     */
    private boolean mPressureUnit;
    /**
     * Holds the selected mTorqueSetting .
     */
    private boolean mTorqueSetting;
    /**
     * Holds the selected mInspectionSetting .
     */
    private boolean mInspectionSetting;
    private String mRfIdTimeoutValue;
    /**
     * Flag indicating whether is save menu icon is enabled or not
     */
    private boolean mSaveButtonEnabled;
    private String mOriginalLanguage;
    // VIEWS
    /**
     * Spinner for choosing language setting
     */
    private Spinner mLanguageSpinner;
    private Spinner mRFIDSpinner;
    /**
     * Spinner for choosing database file location setting
     */
    private Spinner mDBSpinner;
    private EditText mRFIDEdit;
    /**
     * Switch for choosing preferred pressure unit, ON represents PSI and OFF
     * representsBar
     */
    private Switch mPressureUnitSwitch;
    /**
     * Switch for choosing preferred Torque, ON represents YES and OFF
     * represents NO
     */
    private Switch mTorqueSettingSwitch;
    /**
     * Switch for choosing preferred Inspection, ON represents YES and OFF
     * represents NO
     */
    private Switch mInspectionSettingSwitch;
    private String mYESLabel;
    private String mNoLabel;
    private EditText mTDLevel;

    /*#ForHalosys*/
    private EditText mEdittextPin;

    /**
     * Contains new torque unit selected in mToruqeSettingSwitch
     */
    private String mChangedCameraSetting;
    /**
     * Switch for choosing preferred Torque, ON represents YES and OFF
     * represents NO
     */
    private Switch mCameraSettingSwitch;
    private RelativeLayout mNSKSettingLayout;
    private boolean mCameraSetting;

    /**
     * Contains new Inspection unit selected in mToruqeSettingSwitch
     */
    private String mChangedInspectionSetting;

    private TextView mInspectionlabel;
    private TextView mCameraLabel;
    private TextView mThersoldLabel;
    private String mWrongNsk;
    /*#ForHalosys*/
    private String mOfflinePin;

    //User Trace logs trace tag
    private static final String TRACE_TAG = "SettingsActivity";
    private ImageView mPinVisibility;
    private boolean isTextVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        //User Trace logs
        LogUtil.TraceInfo(TRACE_TAG, "none", TRACE_TAG, false, false, true);
        mLanguageSpinner = (Spinner) findViewById(R.id.languagevalues);
        mRFIDSpinner = (Spinner) findViewById(R.id.rfidvalues);
        mRFIDSpinner.setEnabled(false);
        mDBSpinner = (Spinner) findViewById(R.id.databasevalues);
        mPressureUnitSwitch = (Switch) findViewById(R.id.pressurevalues);
        mCameraSettingSwitch = (Switch) findViewById(R.id.cameravalues);
        mNSKSettingLayout = (RelativeLayout) findViewById(R.id.nsksettingcontainer);
        mNSKSettingLayout.setVisibility(View.GONE);
        mTDLevel = (EditText) findViewById(R.id.td_values);
        mTorqueSettingSwitch = (Switch) findViewById(R.id.torquevalues);
        mTorqueSettingSwitch.setChecked(true);
        mInspectionSettingSwitch = (Switch) findViewById(R.id.inspectionvalues);
        mInspectionSettingSwitch.setChecked(false);
        mRFIDEdit = (EditText) findViewById(R.id.rfidvalue);
        mTDLevel.setFilters(new InputFilter[]{new DecimalDigitsInputFilterNSK(
                3, 2)});
        mTDLevel.addTextChangedListener(new ThresholdNSKChanged());
        mInspectionlabel = (TextView) findViewById(R.id.inspectionlabel);
        mCameraLabel = (TextView) findViewById(R.id.cameralabel);
        mThersoldLabel = (TextView) findViewById(R.id.td_label);
        /* #ForHalosys */
        mEdittextPin = (EditText) findViewById(R.id.offlinepintext);
        mPinVisibility = (ImageView) findViewById(R.id.pin_visibility);

        mEdittextPin.addTextChangedListener(new OfflinePinChanged());

        // initialize all your visual fields
        if (null != savedInstanceState) {
            mSecondLoad = true;
            mLangSelected = savedInstanceState.getInt("language", 0);
            mDbSelected = savedInstanceState.getInt("db", 0);
            mRfIdSelected = savedInstanceState.getInt("rfid", 0);
            mPressureUnit = savedInstanceState.getBoolean("pressureunit");
            mTorqueSetting = savedInstanceState.getBoolean("torquesetting");
            mInspectionSetting = savedInstanceState.getBoolean("inspectionsetting");
            mRfIdTimeoutValue = savedInstanceState.getString("rfidtimeout");
            mChangedPressureUnit = savedInstanceState
                    .getString("changed_pressure");
            mChangedTorqueSetting = savedInstanceState
                    .getString("changed_torque_setting");
            mSaveButtonEnabled = savedInstanceState.getBoolean("savebutton");
            mChangedCameraSetting = savedInstanceState
                    .getString("camerasettingsting");
            mCameraSetting = savedInstanceState.getBoolean("camerasetting");

            //Setting Visibility for mNSKSettingLayout
            switch (savedInstanceState.getInt("mNSKSettingLayout")) {
                case 0:
                    mNSKSettingLayout.setVisibility(View.GONE);
                    break;
                case 1:
                    mNSKSettingLayout.setVisibility(View.VISIBLE);
                    break;
                case 2:
                    mNSKSettingLayout.setVisibility(View.INVISIBLE);
                    break;
                default:
                    mNSKSettingLayout.setVisibility(View.GONE);
            }

            mChangedInspectionSetting = savedInstanceState.getString("changed_inspection_setting");
        }
        updateLanguage();
        getOverflowMenu();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initialiseSpinners();
        loadScreenLabelsFromDB();
        // remove all RFID labels and Drop downs
        RelativeLayout rfidContainer = (RelativeLayout) findViewById(R.id.rfidsettingscontainer);
        rfidContainer.setVisibility(RelativeLayout.GONE);
        restoreDialogState(savedInstanceState);
        mEdittextPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0) {
                    mPinVisibility.setVisibility(View.VISIBLE);
                } else {
                    mPinVisibility.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mPinVisibility.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEdittextPin.getText().toString().length() > 0 && !isTextVisible) {
                    isTextVisible = true;
                    mPinVisibility.setImageResource(R.drawable.visibility_off);
                    mEdittextPin.setTransformationMethod(null);
                } else {
                    isTextVisible = false;
                    mPinVisibility.setImageResource(R.drawable.visibility);
                    // mEdittextPin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    mEdittextPin.setTransformationMethod(new PasswordTransformationMethod());
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        LogoutHandler.setCurrentActivity(this);
    }

    @Override
    public void onUserInteraction() {
        InactivityUtils.updateActivityOfUser();
    }

    /**
     * Updates language settings
     */
    private void updateLanguage() {
        DatabaseAdapter dbAdapter = new DatabaseAdapter(this);
        dbAdapter.createDatabase();
        dbAdapter.open();
        SharedPreferences preferences = getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);
        String currentLanguageInSettings = preferences.getString(
                Constants.LANGUAGE_SETTINGS, "notfound");
        Settings setting = dbAdapter.getSettingsInfo();
        String languageSetInDB = Constants.mapLanguageToSettings(setting
                .getLanguage());
        if (!currentLanguageInSettings.equals(languageSetInDB)) {
            // update last langauge in settings
            HashMap<String, String> languageUpdate = new HashMap<>();
            languageUpdate.put("Language", languageSetInDB);
            SettingsLoader.updateSettings(languageUpdate, this);
        }
        dbAdapter.close();
    }

    /**
     * Enable the overflow menu in action even when hardware menu is present
     */
    private void getOverflowMenu() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            if (null != menuKeyField) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (NoSuchFieldException | IllegalAccessException
                | IllegalArgumentException e) {
            LogUtil.e(LOG_TAG, e.toString());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("language", mLanguageSpinner.getSelectedItemPosition());
        outState.putInt("rfid", mRFIDSpinner.getSelectedItemPosition());
        outState.putString("changed_pressure", mChangedPressureUnit);
        outState.putString("changed_torque_setting", mChangedTorqueSetting);
        outState.putString("changed_inspection_setting", mChangedInspectionSetting);
        outState.putInt("db", mDBSpinner.getSelectedItemPosition());
        outState.putBoolean("pressureunit", mPressureUnitSwitch.isChecked());
        outState.putBoolean("torquesetting", mTorqueSettingSwitch.isChecked());
        outState.putBoolean("inspectionsetting", mInspectionSettingSwitch.isChecked());
        outState.putString("rfidtimeout", mRFIDEdit.getText().toString());
        outState.putBoolean("savebutton", mSaveButton.isEnabled());
        outState.putBoolean("camerasetting", mCameraSettingSwitch.isChecked());
        outState.putString("camerasettingsting", mChangedCameraSetting);

        //Saving the mNSKSettingLayout Visibility
        if (mNSKSettingLayout.getVisibility() == View.VISIBLE) {
            outState.putInt("mNSKSettingLayout", 1);
        } else if (mNSKSettingLayout.getVisibility() == View.INVISIBLE) {
            outState.putInt("mNSKSettingLayout", 2);
        } else {
            outState.putInt("mNSKSettingLayout", 0);
        }

        saveDialogState(outState);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        // get shared preferences
        SharedPreferences preferences = getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);
        // get the setting values
        String setting_language = preferences.getString(
                Constants.LANGUAGE_SETTINGS, "notfound");
        String setting_dblocation = preferences.getString(
                Constants.DATABASE_LOCATION, "notfound");
        String setting_rfid = preferences.getString(Constants.RFIDREADER,
                "notfound");
        String setting_pressureunit = preferences.getString(Constants.PRESSURE,
                "notfound");
        String setting_torque = preferences.getString(Constants.TORQUE,
                "notfound");
        String setting_inspection = preferences.getString(Constants.INSPECTION,
                "notfound");
        String setting_camera = preferences.getString(
                Constants.CAMERA_FOR_MIN_NSK, "notfound");

        Constants.TORQUE_ENABLE = setting_torque;
        Constants.INSPECTION_ENABLE = setting_inspection;
        Constants.CAMERA_ENABLE_FOR_MIN_TD = setting_camera;
        // set the current db location
        mOriginalDbLocation = setting_dblocation;
        mChangedLanguage = setting_language;
        mOriginalLanguage = setting_language;
        // set the current language
        if (mSecondLoad) {
            mLanguageSpinner.setTag(2);
            mLanguageSpinner.setSelection(mLangSelected, true);
            mDBSpinner.setTag(2);
            mDBSpinner.setSelection(mDbSelected, true);
            mRFIDSpinner.setTag(2);
            mRFIDSpinner.setSelection(mRfIdSelected, true);
            mPressureUnitSwitch.setChecked(mPressureUnit);
            mTorqueSettingSwitch.setChecked(mTorqueSetting);
            mInspectionSettingSwitch.setChecked(mInspectionSetting);
            mRFIDEdit.setText(mRfIdTimeoutValue);
            mCameraSettingSwitch.setChecked(mCameraSetting);
        }
        // get the list of languages
        else {
            List<String> languagelist = Arrays.asList(getResources()
                    .getStringArray(R.array.languagevalues));
            if (languagelist.indexOf(setting_language) != -1) {
                // sync the save icon
                // set the spinner to setting value
                mLanguageSpinner.setTag(1);
                mLanguageSpinner.setSelection(
                        languagelist.indexOf(setting_language), true);
            }
            // get the list of db location
            List<String> dbList = Arrays.asList(getResources().getStringArray(
                    R.array.dbsettingsvalues));
            if (dbList.indexOf(setting_dblocation) != -1) {
                // set the spinner to setting value
                mDBSpinner.setTag(1);
                mDBSpinner.setSelection(dbList.indexOf(setting_dblocation),
                        true);
            }
            // get the list of rfid values
            List<String> rfidlist = Arrays.asList(getResources()
                    .getStringArray(R.array.rfidsettingsvalues));
            if (rfidlist.indexOf(setting_rfid) != -1) {
                // set the spinner to setting value
                mRFIDSpinner.setTag(1);
                mRFIDSpinner.setSelection(rfidlist.indexOf(setting_rfid), true);
            }
            // get the pressure values and set the setting file value
            // to switch
            mPressureUnitSwitch
                    .setOnCheckedChangeListener(new PressureUnitChangeHandler());
            if (setting_pressureunit.equals(Constants.PSI_PRESSURE_UNIT)) {
                mChangedPressureUnit = Constants.PSI_PRESSURE_UNIT;
                mPressureUnitSwitch.setChecked(true);
            } else {
                mChangedPressureUnit = Constants.BAR_PRESSURE_UNIT;
                mPressureUnitSwitch.setChecked(false);
            }

            // get the torque values and set the setting file value
            // to switch
            mTorqueSettingSwitch
                    .setOnCheckedChangeListener(new TorqueSettingHandler());
            if (setting_torque.equals(Constants.SWITCH_OFF)) {
                mChangedTorqueSetting = Constants.SWITCH_OFF;
                mTorqueSettingSwitch.setChecked(true);
            } else {
                mChangedTorqueSetting = Constants.SWITCH_ON;
                mTorqueSettingSwitch.setChecked(false);
            }
            Constants.TORQUE_ENABLE = mChangedTorqueSetting;

            // get the Inspection values and set the setting file value
            // to switch
            mInspectionSettingSwitch
                    .setOnCheckedChangeListener(new InspectionSettingHandler());
            if (setting_inspection.equals(Constants.SWITCH_OFF)) {
                mChangedInspectionSetting = Constants.SWITCH_OFF;
                mInspectionSettingSwitch.setChecked(false);
            } else {
                mChangedInspectionSetting = Constants.SWITCH_ON;
                mInspectionSettingSwitch.setChecked(true);
            }
            Constants.INSPECTION_ENABLE = mChangedInspectionSetting;

            // get the camera enable values and set the setting file value
            // to switch
            mCameraSettingSwitch
                    .setOnCheckedChangeListener(new CameraSettingHandler());
            if (setting_camera.equals(Constants.SWITCH_OFF)) {
                mChangedCameraSetting = Constants.SWITCH_OFF;
                mCameraSettingSwitch.setChecked(false);
            } else {
                mChangedCameraSetting = Constants.SWITCH_ON;
                mCameraSettingSwitch.setChecked(true);
            }
            Constants.CAMERA_ENABLE_FOR_MIN_TD = mChangedCameraSetting;
        }
    }

    /**
     * call when pressure unit is selected
     *
     * @param view
     */
    public void pressureUnitChanged(View view) {
        enableSaveIcon();
        boolean isPSISelected = ((Switch) view).isChecked();
        mChangedPressureUnit = isPSISelected ? Constants.PSI_PRESSURE_UNIT
                : Constants.BAR_PRESSURE_UNIT;
    }

    private class OfflinePinChanged implements TextWatcher {
        @Override
        public void afterTextChanged(Editable pin) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            String enteredPin = mEdittextPin.getText().toString();
            if (enteredPin != "") {
                enableSaveIcon();
            }
        }
    }

    /**
     * call when TORQUE is selected
     *
     * @param view
     */
    public void torqueSettingChanged(View view) {
        enableSaveIcon();
        boolean isNoSelected = ((Switch) view).isChecked();
        mChangedTorqueSetting = isNoSelected ? Constants.SWITCH_OFF
                : Constants.SWITCH_ON;
        Constants.TORQUE_ENABLE = mChangedTorqueSetting;
    }

    /**
     * call when TORQUE is selected
     *
     * @param view
     */
    public void inspectionSettingChanged(View view) {
        enableSaveIcon();
        boolean isNoSelected = ((Switch) view).isChecked();
        mChangedInspectionSetting = isNoSelected ? Constants.SWITCH_ON
                : Constants.SWITCH_OFF;
        Constants.INSPECTION_ENABLE = mChangedInspectionSetting;
    }

    /**
     * call when Camera is selected for dismount on min TD
     *
     * @param view
     */
    public void cameraSettingChanged(View view) {
        //enableSaveIcon();
        boolean isNoSelected = ((Switch) view).isChecked();
        mChangedCameraSetting = isNoSelected ? Constants.SWITCH_ON
                : Constants.SWITCH_OFF;
        Constants.CAMERA_ENABLE_FOR_MIN_TD = mChangedCameraSetting;

        if (mCameraSettingSwitch.isChecked()) {
            mNSKSettingLayout.setVisibility(View.VISIBLE);
        } else {
            mNSKSettingLayout.setVisibility(View.GONE);
        }
    }

    /**
     * load labels from DB3 file
     */
    private void loadScreenLabelsFromDB() {
        DatabaseAdapter check = new DatabaseAdapter(this);
        check.createDatabase();
        check.open();
        // set settings activity name
        String settingsActivityName = check
                .getLabel(Constants.SETTINGS_ACTIVITY_NAME_LABEL);
        setTitle(settingsActivityName);
        // get Language label from db
        String lang_label = check.getLabel(Constants.LANGUAGE_LABEL_DB_ROW);
        TextView tempLabelText = (TextView) findViewById(R.id.languagelabel);
        tempLabelText.setText(lang_label);

        strERROR_DBFILE_NOT_MOVED = check
                .getLabel(Constants.ERROR_DBFILE_NOT_MOVED);
        strNODISKSPACE = check.getLabel(Constants.ERROR_NO_DISK_SPACE);
        strsettingssaved = check.getLabel(Constants.SETTING_SAVED);
        // get data base location label from db
        String db_label = check.getLabel(Constants.DATABASE_LABEL_DB_ROW);
        tempLabelText = (TextView) findViewById(R.id.databaselabel);
        tempLabelText.setText(db_label);
        // get pressure unit label from db
        String pressure_label = check.getLabel(Constants.PRESSURE_LABEL_DB_ROW);
        tempLabelText = (TextView) findViewById(R.id.pressurelabel);
        tempLabelText.setText(pressure_label);
        // get version label from db
        String versiontext_label = check
                .getLabel(Constants.VERSION_LABEL_DB_ROW);
        tempLabelText = (TextView) findViewById(R.id.versionLabel);
        tempLabelText.setText(versiontext_label);
        // get ejob config label from db
        String ejobConfigLabel = check.getLabel(Constants.EJOB_CONFIG_LABEL);
        tempLabelText = (TextView) findViewById(R.id.ejonconfig);
        tempLabelText.setText(ejobConfigLabel);
        // get rfid type label from db
        String rfidTypeLabel = check.getLabel(Constants.RFID_TYPE_LABEL);
        tempLabelText = (TextView) findViewById(R.id.rfidlabel);
        tempLabelText.setText(rfidTypeLabel);
        // get RFID time out label from db
        String rfidTimeOutLabel = check.getLabel(Constants.RFID_TIME_OUT_LABEL);
        tempLabelText = (TextView) findViewById(R.id.rfidtimeoutlabel);
        tempLabelText.setText(rfidTimeOutLabel);
        // get seconds label from db
        String seconds = check.getLabel(Constants.SECONDS_LABEL);
        tempLabelText = (TextView) findViewById(R.id.rfidtimeoutunitlabel);
        tempLabelText.setText(seconds);
        // set current app version
        tempLabelText = (TextView) findViewById(R.id.versionInfo);
        tempLabelText.setText(mSettingAppVersion);
        // set torque label
        String torque_label = check.getLabel(Constants.TORQUE_LABEL_DB_ROW);
        tempLabelText = (TextView) findViewById(R.id.torquelabel);
        tempLabelText.setText(torque_label);

        //Inspection label
        mInspectionlabel.setText(check.getLabel(Constants.LABEL_INSPECTION));
        //Photo at min TD removal Label
        mCameraLabel.setText(check.getLabel(Constants.Label_Photo_at_min_TD_Removal));
        mThersoldLabel.setText(check.getLabel(Constants.Label_Thresold_TD));
        mWrongNsk = check.getLabel(Constants.Label_Wrong_NSK);
        Constants.WRONG_NSK_LEVEL = mWrongNsk;
        // set timeout
        mRFIDEdit.setText(mSettingRfIdTimeout);
        mChangedRFIDTimeOut = mSettingRfIdTimeout;
        mYESLabel = check.getLabel(Constants.YES_LABEL);
        mNoLabel = check.getLabel(Constants.NO_LABEL);
        Constants.CONFIRM_SAVE = check
                .getLabel(Constants.SAVE_CHANGE_CONFIRMATION_LABEL);
        check.close();
    }

    /**
     * initialize the spinners with default data from settings.xml
     */
    private void initialiseSpinners() {
        SharedPreferences preferences = getSharedPreferences(
                Constants.GOODYEAR_CONF, 0);
        mSettingRfIdTimeout = preferences.getString(Constants.RFID_TIMOUT,
                "notfound");
        /*
         * mSettingAppVersion = preferences.getString(Constants.APP_VERSION,
		 * "notfound");
		 */
        PackageInfo pinfo;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String mVersionNane = pinfo.versionName;
            mSettingAppVersion = mVersionNane;

        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // fill language drop down
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.languagevalues,
                android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mLanguageSpinner.setAdapter(adapter);
        mLanguageSpinner
                .setOnItemSelectedListener(new LanguageChangeListener());
        // fill DB drop down
        try {
            String externalStorage = System.getenv("SECONDARY_STORAGE");
            String sdCardList[] = externalStorage.split(":");
            if (null == sdCardList[0]) {
                mDBSpinner.setEnabled(false);
            } else {
                try {
                    StatFs stat = new StatFs(sdCardList[0]);
                    if (0 == stat.getAvailableBlocks()) {
                        mDBSpinner.setTag(1);
                        mDBSpinner.setSelection(1, true);
                        mDBSpinner.setEnabled(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    mDBSpinner.setTag(1);
                    mDBSpinner.setSelection(1, true);
                    mDBSpinner.setEnabled(false);
                    LogUtil.e(LOG_TAG, "could not check sd card");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            mDBSpinner.setTag(1);
            mDBSpinner.setSelection(1, true);
            mDBSpinner.setEnabled(false);
            LogUtil.e(LOG_TAG, "could not check sd card");
        }
        ArrayAdapter<CharSequence> dbValuesAdapter = ArrayAdapter
                .createFromResource(this, R.array.dbsettingsvalues,
                        android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        dbValuesAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mDBSpinner.setAdapter(dbValuesAdapter);
        mDBSpinner
                .setOnItemSelectedListener(new DatabaseLocationChangeListener());
        // fill RFID drop down
        adapter = ArrayAdapter.createFromResource(this,
                R.array.rfidsettingsvalues,
                android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mRFIDSpinner.setAdapter(adapter);
        mRFIDSpinner.setOnItemSelectedListener(new RFIDChangeListner());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        System.out.println("onCreateOptionsMenu::::");
        mSaveButton = menu.findItem(R.id.action_save);
        if (mSecondLoad) {
            if (mSaveButtonEnabled) {
                enableSaveIcon();
            } else {
                mSaveButton.setEnabled(false);
            }
        } else {
            mSaveButton.setEnabled(false);
        }
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                LogUtil.TraceInfo(TRACE_TAG, "Option", "Save", false, true, false);
                saveSettingsWithCameraStatus();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * CR :: 455 Method saving the changes made by the user in settings hashmap
     * It checks all the preconditions before calling saveSettingsOnSave()
     * method
     */
    private void saveSettingsWithCameraStatus() {
        try {
            LogUtil.d(TRACE_TAG, "  saveSettingsWithCameraStatus  pin " + mEdittextPin.getText().toString());
            String enteredPin = mEdittextPin.getText().toString().trim();
            // Start if :: It will be true when user entered value is smaller
            // than the the threshold TD
            if (mCameraSettingSwitch.isChecked()
                    && ((!TextUtils.isEmpty(mTDLevel.getText().toString()) && Float
                    .valueOf(mTDLevel.getText().toString()) < Constants.THRESHOLD_TD_VALUE) || TextUtils
                    .isEmpty(mTDLevel.getText().toString()))) {

                LogUtil.TraceInfo(TRACE_TAG, "none", "Save BT : Msg : " + Constants.WRONG_NSK_LEVEL, false, false, false);
                CommonUtils.notify(Constants.WRONG_NSK_LEVEL,
                        SettingsActivity.this);
            } else if (enteredPin == "" || enteredPin.equals("") || enteredPin.isEmpty() || enteredPin == null) {
                saveSettingsOnSave();
            } else if (enteredPin.length() < 6 || enteredPin.length() > 6) {
			    CommonUtils.notify(Constants.SET_OFFLINE_PIN, SettingsActivity.this);
            } else if (enteredPin.length() == 6 && isPinChanged()) {
                updateChangedPin(mEdittextPin.getText().toString());
                CommonUtils.notify(Constants.SET_PIN_SUCCESSFUL, this);
                saveSettingsOnSave();
            }
            // else :: It will be true when all other conditions related to
            // camera are true
            else {
                saveSettingsOnSave();
            }
        } catch (NumberFormatException e) {
            LogUtil.e(LOG_TAG, "could not save the changes on yes");
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Method saving the new preferences as per the user selection in the local
     * setting file
     */
    private void saveSettingsOnSave() throws IOException {
        // create a hash map of selected options
        HashMap<String, String> settingsUpdatedHashMap = new HashMap<>();
        // updateDBtable();
        moveDBFile();
        // update selected values
        settingsUpdatedHashMap.put(Constants.PRESSURE, mChangedPressureUnit);
        settingsUpdatedHashMap.put(Constants.TORQUE, mChangedTorqueSetting);
        settingsUpdatedHashMap.put(Constants.INSPECTION, mChangedInspectionSetting);
        settingsUpdatedHashMap.put(Constants.CAMERA_FOR_MIN_NSK, mChangedCameraSetting);
        settingsUpdatedHashMap.put(Constants.MIN_TD_LEVEL_FOR_CAMERA, Constants.THRESHOLD_TD_FOR_CAMERA);
        settingsUpdatedHashMap.put(Constants.DATABASE_LOCATION,
                mChangedDBLocation);
        settingsUpdatedHashMap.put(Constants.RFIDREADER, mChangedRFID);
        settingsUpdatedHashMap.put(Constants.RFID_TIMOUT, mChangedRFIDTimeOut);
        settingsUpdatedHashMap.put(Constants.LANGUAGE_SETTINGS,
                mChangedLanguage);
        settingsUpdatedHashMap.put(Constants.OFFLINE_PIN_SETTINGS,
                mEdittextPin.getText().toString());
        try {
            String Trace_Data = mChangedDBLocation + " | "
                    + mChangedLanguage + " | "
                    + mChangedPressureUnit + " | ";

            if (mChangedTorqueSetting.equalsIgnoreCase("ON")) {
                Trace_Data = Trace_Data + "OFF" + " | ";
            } else {
                Trace_Data = Trace_Data + "ON" + " | ";
            }

            Trace_Data = Trace_Data
                    + mChangedCameraSetting + " | "
                    + Constants.THRESHOLD_TD_FOR_CAMERA + " | "
                    + mChangedInspectionSetting;

            LogUtil.TraceInfo(TRACE_TAG, "Save BT", "Msg : " + Trace_Data, false, true, false);
        } catch (Exception e) {
            LogUtil.TraceInfo(TRACE_TAG, "Save BT", "Exception : " + e.getMessage(), false, true, false);
        }

        makeChanges(settingsUpdatedHashMap);
        CommonUtils.notify(strsettingssaved, this);
        disableSaveIcon();
        startSyncActivity(mapLanguage());

    }

    /**
     * make change to the db3 file, setting.xml and SP
     *
     * @param settingsUpdatedHashMap
     */
    private void makeChanges(HashMap<String, String> settingsUpdatedHashMap)
            throws IOException {
        boolean wasSuccessful;
        // write the changes to the settings file and shared preferences
        wasSuccessful = SettingsLoader.updateSettings(settingsUpdatedHashMap,
                this);
        if (wasSuccessful) {
            CommonUtils.notify(strsettingssaved, this);
        }
    }

    @Override
    public void onBackPressed() {
        createDialogOnBackPress();
    }

    private AlertDialog mBackAlertDialog;

    private void saveDialogState(Bundle state) {
        state.putBoolean("mBackAlertDialog",
                (mBackAlertDialog != null && mBackAlertDialog.isShowing()));
    }

    private void restoreDialogState(Bundle state) {
        if (state != null) {
            if (state.getBoolean("mBackAlertDialog")) {
                createDialogOnBackPress();
            }
        }
    }

    @Override
    protected void onDestroy() {
        try {
            if (mBackAlertDialog != null && mBackAlertDialog.isShowing()) {
                mBackAlertDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    /**
     * Creates a alert dialog prompting to save before exit
     */
    private void createDialogOnBackPress() {
        LogUtil.TraceInfo(TRACE_TAG, "Dialog", "Back Press", false, true, false);
        // FIX:: onOrientation change if dialog was shown the mSaveButton was
        // coming null, added boolean in below condition if button is null
        if ((mSaveButton != null && mSaveButton.isEnabled())
                || mSaveButtonEnabled) {
            LayoutInflater li = LayoutInflater.from(SettingsActivity.this);
            View promptsView = li.inflate(R.layout.dialog_logout_confirmation,
                    null);
            final EjobAlertDialog alertDialogBuilder = new EjobAlertDialog(this);
            alertDialogBuilder.setView(promptsView);
            // update user activity for dialog layout
            LinearLayout rootNode = (LinearLayout) promptsView
                    .findViewById(R.id.layout_root);
            rootNode.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    InactivityUtils.updateActivityOfUser();
                }
            });

            // Setting The Info
            TextView infoTv = (TextView) promptsView
                    .findViewById(R.id.logout_msg);
            infoTv.setText(Constants.CONFIRM_SAVE);
            alertDialogBuilder.setCancelable(false).setPositiveButton(
                    mYESLabel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // update user activity on button click
                            alertDialogBuilder.updateInactivityForDialog();
                            LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - Yes", false, true, false);
                            // if this button is clicked, close
                            // current activity
                            saveSettingsWithCameraStatus();
                        }
                    });
            alertDialogBuilder.setCancelable(false).setNegativeButton(mNoLabel,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // update user activity on button click
                            alertDialogBuilder.updateInactivityForDialog();
                            LogUtil.TraceInfo(TRACE_TAG, "Back Press", "BT - No", false, true, false);
                            // dialog.cancel();
                            startSyncActivity(mapLanguage(mOriginalLanguage));
                            // dialog.cancel();
                        }
                    });
            // create alert dialog
            mBackAlertDialog = alertDialogBuilder.create();
            // show it
            mBackAlertDialog.show();
            mBackAlertDialog.setCanceledOnTouchOutside(false);
        } else {
            startSyncActivity(mapLanguage());
        }
    }

    private void startSyncActivity(String lang) {
        Intent intent = new Intent(this, SyncActivity.class);
        intent.putExtra("sentfrom", "settings");
        intent.putExtra("language", lang);
        startActivity(intent);
    }

    private String mapLanguage(String language) {
        mChangedLanguage = language;
        return mapLanguage();
    }

    private String mapLanguage() {
        switch (mChangedLanguage) {
            case Constants.ENGLISH:
                return Constants.ENGLISH_SHORT;
            case Constants.FRENCH:
                return Constants.FRENCH_SHORT;
            case Constants.GERMAN:
                return Constants.GERMAN_SHORT;
            case Constants.POLISH:
                return Constants.POLISH_SHORT;
            case Constants.NETHERLANDS:
                return Constants.NETHERLANDS_SHORT;
            case Constants.ITALY:
                return Constants.ITALY_SHORT;
            case Constants.SPANISH:
                return Constants.SPANISH_SHORT;
            case Constants.PORTUGUESE:
                return Constants.PORTUGUESE_SHORT;
            case Constants.GREEK:
                return Constants.GREEK_SHORT;
            case Constants.TURKISH:
                return Constants.TURKISH_SHORT;
            case Constants.RUSSIAN:
                return Constants.RUSSIAN_SHORT;
            case Constants.AUSTRIAN:
                return Constants.AUSTRIAN_SHORT;
            case Constants.BELGIAN:
                return Constants.BELGIAN_SHORT;
            case Constants.BULGARIAN:
                return Constants.BULGARIAN_SHORT;
            case Constants.SWISS:
                return Constants.SWISS_SHORT;
            case Constants.CZECH:
                return Constants.CZECH_SHORT;
            case Constants.DANISH:
                return Constants.DANISH_SHORT;
            case Constants.ESTONIAN:
                return Constants.ESTONIAN_SHORT;
            case Constants.FINISH:
                return Constants.FINISH_SHORT;
            case Constants.CROATIAN:
                return Constants.CROATIAN_SHORT;
            case Constants.HUNGARIAN:
                return Constants.HUNGARIAN_SHORT;
            case Constants.IRISH:
                return Constants.IRISH_SHORT;
            case Constants.LITHUANIAN:
                return Constants.LITHUANIAN_SHORT;
            case Constants.LUXEMBOURG:
                return Constants.LUXEMBOURG_SHORT;
            case Constants.LATVIAN:
                return Constants.LATVIAN_SHORT;
            case Constants.NORWEGIAN:
                return Constants.NORWEGIAN_SHORT;
            case Constants.ROMANIAN:
                return Constants.ROMANIAN_SHORT;
            case Constants.SWEDISH:
                return Constants.SWEDISH_SHORT;
            case Constants.SLOVENIAN:
                return Constants.SLOVENIAN_SHORT;

            case Constants.SLOVAKIAN:
                return Constants.SLOVAKIAN_SHORT;

            default:
                return null;
        }
    }

    /**
     * Moves the database file from SdCard and Internal Memory based on
     * preference
     */
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    private void moveDBFile() {// moveDBs(Constants.USER + ".db3");
        // check if change is made
        String sdcardPath = null;
        if (!mOriginalDbLocation.equals(mChangedDBLocation)) {
            int SDK_INT = android.os.Build.VERSION.SDK_INT;
            if (SDK_INT >= 19) {
                try {
                    sdcardPath = getExternalFilesDirs(null)[1]
                            .getAbsolutePath();
                } catch (Exception e) {
                    mChangedDBLocation = mOriginalDbLocation;
                    return;
                }

                if (sdcardPath == null) {
                    CommonUtils.notify(strERROR_DBFILE_NOT_MOVED, this);
                    mChangedDBLocation = mOriginalDbLocation;
                    LogUtil.e(LOG_TAG, "no sd card found");
                    return;
                }
            } else {
                String externalStorage = System.getenv("SECONDARY_STORAGE");
                if (!TextUtils.isEmpty(externalStorage)) {
                    String sdCardList[] = externalStorage.split(":");
                    sdcardPath = sdCardList[0]
                            + Constants.PROJECT_PATH_FOR_BELOW_KITKAT;
                    File sdCardPathDir = new File(sdcardPath);
                    if (!sdCardPathDir.exists()) {
                        sdCardPathDir.mkdirs();
                    }
                }
            }

            String source;
            String destination;
            if (mChangedDBLocation.equals(Constants.EXTERNAL_DB_LOCATION)) {
                source = Constants.PROJECT_PATH;
                destination = sdcardPath;

            } else {
                source = sdcardPath;
                destination = Constants.PROJECT_PATH;
            }
            // get input and output files are ready
            InputStream mInput = null;
            try {
                File db3DestinationDir = new File(destination);
                if (!db3DestinationDir.exists())
                    db3DestinationDir.mkdir();
                StatFs stat = new StatFs(db3DestinationDir.getPath());
                int availBlocks = stat.getAvailableBlocks();
                int blockSize = stat.getBlockSize();
                long free_memory = (long) availBlocks * (long) blockSize;
                // get free memory in MB
                if (free_memory / (1024 * 1024) < 1.5) {
                    mChangedDBLocation = mOriginalDbLocation;
                    CommonUtils.notify(strNODISKSPACE, this);
                } else {
                    FileOperations.checkAndMoveAllDBFilesIfExists(source,
                            destination);
                    // make the changed file as saved
                    mOriginalDbLocation = mChangedDBLocation;
                }
            } catch (Exception e) {
                CommonUtils.notify(strERROR_DBFILE_NOT_MOVED, this);
                mChangedDBLocation = mOriginalDbLocation;
                LogUtil.e(LOG_TAG, "could not move db3 file, check SD card");
            }
        }
    }

    /**
     * Method Enabling the Save menu icon in action bar as per the user
     * selection or if any changes made in the application settings
     */
    private void enableSaveIcon() {
        if (mSaveButton != null) {
            mSaveButton.setEnabled(true);
            mSaveButton.setIcon(R.drawable.savejob_navigation);
        }
    }

    /**
     * Disables the Save menu icon in action bar as per the user selection or if
     * any changes made in the application settings
     */
    private void disableSaveIcon() {
        if (mSaveButton != null) {
            mSaveButton.setEnabled(false);
            mSaveButton.setIcon(R.drawable.unsavejob_navigation);
        }
    }

    private void checkSaveButton(int val, AdapterView<?> parent) {
        // if loading for first time setting xml
        // is being loaded so dont update save icon
        switch (val) {
            case 1:
                parent.setTag(0);
                break;
            case 2:
                parent.setTag(0);
                break;
            default:
                enableSaveIcon();
                break;
        }
    }

    private class LanguageChangeListener implements OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos,
                                   long id) {
            try {
                mChangedLanguage = (String) parent.getItemAtPosition(pos);
                int val = (int) parent.getTag();
                // if loading for first time setting xml
                // is being loaded so dont update save icon
                checkSaveButton(val, parent);
            } catch (Exception e) {
                LogUtil.e(LOG_TAG, "langauge change listener");
                e.printStackTrace();
                checkSaveButton(0, parent);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }

    private class DatabaseLocationChangeListener implements
            OnItemSelectedListener {
        @SuppressWarnings("deprecation")
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos,
                                   long id) {
            int val = (int) parent.getTag();
            if (pos == 0) {
                String externalStorage = System.getenv("SECONDARY_STORAGE");
                String sdCardList[] = externalStorage.split(":");
                try {
                    StatFs stat = new StatFs(sdCardList[0]);
                    if (stat.getAvailableBlocks() != 0) {
                        mChangedDBLocation = (String) parent
                                .getItemAtPosition(pos);
                        checkSaveButton(val, parent);
                    }
                } catch (Exception e) {
                    LogUtil.e(LOG_TAG, "Database change listener");
                }
            } else {
                mChangedDBLocation = (String) parent.getItemAtPosition(pos);
                checkSaveButton(val, parent);
            }
            // if loading for first time setting xml
            // is being loaded so dont update save icon
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }

    private class RFIDChangeListner implements OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos,
                                   long id) {
            // disable RFID if RFID type is set to None
            mRFIDEdit.setEnabled(pos != 0);
            mChangedRFID = (String) parent.getItemAtPosition(pos);
            int val = (int) parent.getTag();
            // if loading for first time setting xml
            // is being loaded so dont update save icon
            checkSaveButton(val, parent);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }

    private class PressureUnitChangeHandler implements OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            enableSaveIcon();
            mChangedPressureUnit = isChecked ? Constants.PSI_PRESSURE_UNIT
                    : Constants.BAR_PRESSURE_UNIT;
        }
    }

    private class TorqueSettingHandler implements OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton torqueButtonView,
                                     boolean isRetorqueChecked) {
            enableSaveIcon();
            mChangedTorqueSetting = isRetorqueChecked ? Constants.SWITCH_OFF
                    : Constants.SWITCH_ON;

            Constants.TORQUE_ENABLE = mChangedTorqueSetting;
        }
    }

    private class InspectionSettingHandler implements OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton inspectionButtonView,
                                     boolean isInspectionChecked) {
            enableSaveIcon();
            mChangedInspectionSetting = isInspectionChecked ? Constants.SWITCH_ON
                    : Constants.SWITCH_OFF;

            Constants.INSPECTION_ENABLE = mChangedInspectionSetting;
        }
    }

    private class CameraSettingHandler implements OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            enableSaveIcon();
            mChangedCameraSetting = isChecked ? Constants.SWITCH_ON
                    : Constants.SWITCH_OFF;

            Constants.CAMERA_ENABLE_FOR_MIN_TD = mChangedCameraSetting;
            SharedPreferences preferences = getSharedPreferences(
                    Constants.GOODYEAR_CONF, 0);
            if (mChangedCameraSetting.equals("ON")) {
                if (!TextUtils.isEmpty(preferences.getString(
                        Constants.MIN_TD_LEVEL_FOR_CAMERA, ""))) {
                    Constants.THRESHOLD_TD_FOR_CAMERA = preferences.getString(
                            Constants.MIN_TD_LEVEL_FOR_CAMERA, "");
                }
                mNSKSettingLayout.setVisibility(View.VISIBLE);
                mTDLevel.setText(Constants.THRESHOLD_TD_FOR_CAMERA);
            } else {
                if (!TextUtils.isEmpty(preferences.getString(
                        Constants.MIN_TD_LEVEL_FOR_CAMERA, ""))
                        || !TextUtils
                        .isEmpty(Constants.THRESHOLD_TD_FOR_CAMERA)) {
                    Constants.THRESHOLD_TD_FOR_CAMERA = "0";
                    mNSKSettingLayout.setVisibility(View.GONE);
                    enableSaveIcon();
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.goodyear.ejob.interfaces.InactivityHandler#logUserOutDueToInactivity
     * ()
     */
    @Override
    public void logUserOutDueToInactivity() {
        InactivityUtils.logoutFromActivityBelowEjobForm(this);
    }

    /**
     * CR: 455 Class implementing TextWatcher handling the value to be entered
     * for threshold NSK
     *
     * @author amitkumar.h
     */
    private class ThresholdNSKChanged implements TextWatcher {
        @Override
        public void afterTextChanged(Editable nskOne) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            String strEnteredVal = mTDLevel.getText().toString();
            float maxNskVal = CommonUtils.parseFloat(strEnteredVal);
            /*
             * Start if :: It will be true when user entered value is either
			 * smaller than the the threshold or left blank
			 */
            if (!(strEnteredVal.equals("") || (strEnteredVal.equals(".")))) {
                if (maxNskVal > Constants.MAX_NSK_LEVEL) {
                    mTDLevel.setText("");
                    CommonUtils.notify(Constants.WRONG_NSK_LEVEL, SettingsActivity.this);
                    mTDLevel.setText("");
                } else if (maxNskVal != 0) {
                    enableSaveIcon();
                    Constants.THRESHOLD_TD_FOR_CAMERA = mTDLevel.getText()
                            .toString();
                } else {
                    if (null != mTDLevel) {
                        mTDLevel.setText("");
                    }
                    //based on requirement (enable save if there is an action)
                    //disableSaveIcon();
                }
                // else if :: It will be true when user is entering dot(.)
            } else if (strEnteredVal.equals(".")) {
                mTDLevel.setText("");
                //based on requirement (enable save if there is an action)
                //disableSaveIcon();
            } else {
                //based on requirement (enable save if there is an action)
                //disableSaveIcon();
            }
        }
    }

    /* #ForHalosys */
    private boolean isPinChanged() {
        LogUtil.i(TRACE_TAG, ":: inside isPinChanged ");
        String userName = Constants.USER;
        LogUtil.i(TRACE_TAG, " userName "+userName);
        String pinInDb = "";
        String userEnteredPin = "";
        DatabaseAdapter dbAdapter = new DatabaseAdapter(getApplication());
        try {
            dbAdapter.open();
            pinInDb = dbAdapter.getPinForUser(userName);
            userEnteredPin = Security.encryptMessage(mEdittextPin.getText().toString());
        } catch (Exception e) {
            LogUtil.e("SyncActivity ", " Exception " + e);
        } finally {
            dbAdapter.close();
        }
        if (userEnteredPin == pinInDb) {
            return false;
        } else {
            return true;
        }
    }

    private void updateChangedPin(String pin) {

        LogUtil.d(TRACE_TAG,":: inside updateChangedPin");
        DatabaseAdapter dbAdapter = new DatabaseAdapter(getApplicationContext());
        try {
            dbAdapter.open();
            dbAdapter.updateAccountWithPin(pin);
            dbAdapter.copyFinalDB();
        } catch (Exception e) {
            LogUtil.e(TRACE_TAG, " Exception " + e);
        } finally {
            dbAdapter.close();
        }
    }
}