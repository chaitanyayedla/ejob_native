package com.goodyear.ejob.authenticator;
import android.app.Activity;
import android.content.Context;

import java.io.IOException;

import com.goodyear.ejob.halosys.HalosysCallStatus;
import com.goodyear.ejob.halosys.HalosysServiceCall;
import com.goodyear.ejob.halosys.HalosysStatusModel;
import com.goodyear.ejob.util.Account;
import com.goodyear.ejob.util.FileOperations;
import com.goodyear.ejob.util.LogUtil;
import android.database.SQLException;
import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.util.Constants;


/**
 * Created by giriprasanth.vp on 10/24/2016.
 * This class handles the user authentication from DB and Hybris Server.
 * It starts the login request to the Hybris server.
 */
public class HybrisAuthentication {

    private final static String TAG = "HybrisAuthentication";

    /**
     * This method verifies the user from the DB Account table
     *
     * @param userName
     *            username to check
     * @param pin
     *            password to check
     * @param context
     *            context of the application
     * @return Account object if user is verified. Error string otherwise
     */
    public static Object verifyUserFromDB(String userName, String pin,
                                          Context context) {
        LogUtil.d(TAG," verifyUserFromDB : verifying offline with pin");
        DatabaseAdapter check = new DatabaseAdapter(context);
        try {
            check.deleteDB();
            check.close();
            check.createDatabase();
            check.open();
            //CR-447 DB Upgrade
            check.doUpgrade(context);
        } catch (SQLException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // get account object from db
        Account account = check.getAccountInfo(userName);
        if (account == null) {
            return Constants.NO_USER_FOUND;
        }
        //String passwordFromDB = account.getPassword();
        String pinFromDB = account.getaPin();
        if(check!=null) {
            check.close();
        }

        /*if (passwordFromDB.equals(password))
            return account;
        else
            return Constants.PASSWORD_INCORRECT;*/

        if(pinFromDB == ""){
            return Constants.ERROR_CONNECTION_WITH_SERVER;
        }
        if (pinFromDB.equals(pin))
            return account;
        else
            return Constants.PASSWORD_INCORRECT;

    }


    /**
     * This method verifies user from Hybris server.
     * @param mUserName
     *            username to check
     * @param mPassword
     *            password to check
     * @param mContext
     *            context of the application
     * @return Account object if user is verified. Error string otherwise.
     */
    public static Object verifyUserFromHybrisServer(String mUserName, String mPassword,
                                              Activity mContext) {
        LogUtil.d(TAG," verifyUserFromHybrisServer :: verify user online");
        Object mServerResponse = null;
        try {
            mServerResponse = HalosysServiceCall.hybrisLogin(mContext, mUserName, mPassword);
            FileOperations.cleanUp(); // clean temp files
        }
        catch (Exception e)
        {
            LogUtil.i(TAG,"verifyUserFromHybrisServer : Exception - "+e.getMessage());
        }
        if(mServerResponse!=null)
        {
            try {
                HalosysStatusModel mStatus = new HalosysStatusModel();
                mStatus = (HalosysStatusModel) mServerResponse;
                if (mStatus.getsCode() == HalosysCallStatus.CODE_SUCCESS) {
                    Account account = new Account();
                    account.setIsAccountPinSet(0);
                    account.setLogin(mUserName);
                    return account;
                }
            }
            catch (Exception e)
            {
                LogUtil.e(TAG," verifyUserFromHybrisServer; exception :: "+e);

            }
        }
       // return null;
        return mServerResponse;
    }
}
