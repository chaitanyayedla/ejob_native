package com.goodyear.ejob.authenticator;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import android.app.Activity;
import android.content.Context;
import android.database.SQLException;
import android.util.Log;
import android.widget.Toast;

import com.goodyear.ejob.dbhelpers.DatabaseAdapter;
import com.goodyear.ejob.sync.AuthSync;
import com.goodyear.ejob.util.Account;
import com.goodyear.ejob.util.Constants;
import com.goodyear.ejob.util.DateTimeUTC;
import com.goodyear.ejob.util.FileOperations;
import com.goodyear.ejob.util.LogUtil;
import com.goodyear.ejob.util.Security;
import com.halomem.android.api.HalomemException;
import com.halomem.android.api.ISession;
import com.halomem.android.api.impl.Session;
import com.halomem.android.api.listeners.OnVerified;

/**
 * This class handles the user authentication from DB and Server. It starts the
 * login request to the server. For more details see {@link AuthSync}
 */
public class Authentication {

	private final static String LOGIN = "login";
	private final static String PASSWORD = "password";
	public final static String SOCKET_TIMEOUT_EXCEPTION = "socket_timeout";
	private final static String TAG = "Authentication";

	/**
	 * This method verifies the user from the DB Account table
	 * 
	 * @param userName
	 *            username to check
	 * @param password
	 *            password to check
	 * @param context
	 *            context of the application
	 * @return Account object if user is verified. Error string otherwise
	 */
	public static Object verifyUserFromDB(String userName, String password,
			Context context) {
		DatabaseAdapter check = new DatabaseAdapter(context);
		try {
			check.deleteDB();
			check.close();
			check.createDatabase();
			check.open();
			//CR-447 DB Upgrade
			check.doUpgrade(context);
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// get account object from db
		Account account = check.getAccountInfo(getUserName(userName));
		if (account == null) {
			return Constants.NO_USER_FOUND;
		}
		String passwordFromDB = account.getPassword();
		if(check!=null) {
			check.close();
		}

		if (passwordFromDB.equals(password))
			return account;
		else
			return Constants.PASSWORD_INCORRECT;

	}

	/**
	 * This method verifies user from server. It users the encryption api AES to
	 * encrypt the password and username before sending them to server. For more
	 * details on encryption see {@link Security}.
	 * 
	 * @param userName
	 *            username to check
	 * @param password
	 *            password to check
	 * @param context
	 *            context of the application
	 * @return Account object if user is verified. Error string otherwise.
	 */
	public static Object verifyUserFromServer(String userName, String password,
			Context context) {
		String encrypt_uname = null;
		String encrypt_pass = null;
		/*try {
			encrypt_uname = "SV2__"
					+ Security.encryptMessage(getUserName(userName));
			encrypt_pass = "SV2__" + Security.encryptMessage(password);

		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | InvalidAlgorithmParameterException
				| IllegalBlockSizeException | BadPaddingException | IOException e) {

		}*/

		Object passExpirationDate = AuthSync.loginSync(LOGIN, encrypt_uname,
				PASSWORD, encrypt_pass);
		FileOperations.cleanUp(); // clean temp files

		try
		{
			LogUtil.i("Authentication", "verifyUserFromServer -  PassExpirationDate : "+passExpirationDate);
		}catch(Exception e)
		{
			LogUtil.i("Authentication", "verifyUserFromServer - Exception (PassExpirationDate) : " + e.getMessage());
		}

		if (passExpirationDate == null) {
			return Constants.PASSWORD_INCORRECT;
		} else if (passExpirationDate.equals(SOCKET_TIMEOUT_EXCEPTION)) {
			return Constants.ERROR_CONNECTION_WITH_SERVER;
		} else {
			// parse date
			try
			{
				LogUtil.i("Authentication", "verifyUserFromServer -  PassExpirationDate - Parse Long : "+Long.parseLong((String) passExpirationDate));
			}catch(Exception e)
			{
				LogUtil.i("Authentication", "verifyUserFromServer - Exception (PassExpirationDate) - Parse Long : " + e.getMessage());
			}

			long expirationDate = Long.parseLong((String) passExpirationDate);
			DateTimeUTC.convertMillisecondsToUTCDate(expirationDate);

			Account account = new Account();
			account.setPassword(password);
			//account.setLogin(getUserName(userName));
			account.setPassExpirationDate(expirationDate);

			return account;
		}
	}

	/**
	 * This method takes the username and returns the correct user name in
	 * different environment (Prod, Cons, Dev :: cons environment the username
	 * ends with C, Dev - D and in Prod - no suffix).
	 * 
	 * @param userName
	 *            for environment
	 * @return user name correct username in selected environment
	 */
	public static String getUserName(String userName) {
		// if user name contains
		LogUtil.i("Authentication ", " Inside getUserName");
		if (userName != null
				&& (userName.endsWith("D") || userName.endsWith("C")))
			return userName.substring(0, userName.length() - 1);
		else
			return userName;
	}

	/* #ForHalosys */
	public static void attemptApplicationVerification(final Activity activity, String paramUserName, String paramPassword) {

		try {
			Log.d(TAG, " inside attemptApplicationVerification...");
			final ISession session = Session.getInstance();
			final String userName = paramUserName;
			final String password = paramPassword;
			session.verify(activity, new OnVerified() {

				@Override
				public void onSuccess() {
					try {
						Log.d(TAG, "username;password :: " + userName + ";" + password);
						session.login(userName, password);
						if (session.getAuthToken() == null) {
							LogUtil.e(TAG, " No Auth Token ");
						} else {
							Toast.makeText(activity.getApplicationContext(), " loggin successful ", Toast.LENGTH_SHORT).show();
							Log.e(TAG, " Token : " + session.getAuthToken());
						}
						LogUtil.d(TAG, " isTokenValid : ");
					} catch (HalomemException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onFailure(String s) {
					Toast.makeText(activity.getApplicationContext(), "Incorrect username/password", Toast.LENGTH_SHORT).show();
					Log.e(TAG, "Application verification failed ---" + s);
				}
			});

		} catch (Exception e) {
			Log.e(TAG, "Application verification failed", e);
		}
	}
}
