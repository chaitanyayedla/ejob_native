/**
 * 
 */
package com.goodyear.crashhandling;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.goodyear.ejob.EjobFormActionActivity;
import com.goodyear.ejob.GoodYear_eJob_MainActivity;
import com.goodyear.ejob.R;
import com.goodyear.ejob.TurnOnRim;
import com.goodyear.ejob.util.LogUtil;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Environment;
import android.os.Looper;
import android.os.StatFs;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

/**
 * Handle Crash Application
 * 
 * @author Nilesh.Pawate
 * 
 */
public class HandleUncaughtException implements UncaughtExceptionHandler {
	/**
	 * Activity Context
	 */
	private Context mContext;
	/**
	 * App Version.
	 */
	private static String APP_CODE_VERSION = "1.0.0.20.2";

	/**
	 * Constructor.
	 * 
	 * @param context
	 */
	public HandleUncaughtException(Context context) {
		this.mContext = context;
	}

	private StatFs getStatFs() {
		File path = Environment.getDataDirectory();
		return new StatFs(path.getPath());
	}

	private long getAvailableInternalMemorySize(StatFs stat) {
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return availableBlocks * blockSize;
	}

	private long getTotalInternalMemorySize(StatFs stat) {
		long blockSize = stat.getBlockSize();
		long totalBlocks = stat.getBlockCount();
		return totalBlocks * blockSize;
	}

	private void addInformation(StringBuilder message) {
		message.append("Locale: ").append(Locale.getDefault()).append('\n');
		try {
			PackageManager pm = mContext.getPackageManager();
			PackageInfo pi;
			pi = pm.getPackageInfo(mContext.getPackageName(), 0);

			message.append("Version: ").append(pi.versionName).append('\n');
			message.append("Package: ").append(pi.packageName).append('\n');
		} catch (Exception e) {
			Log.e("HandleUncaughtException", "Error", e);
			message.append("Could not get Version information for ").append(
					mContext.getPackageName());
		}
		message.append("Phone Model: ").append(android.os.Build.MODEL)
				.append('\n');
		message.append("Application Code Version: ").append(APP_CODE_VERSION)
				.append('\n');
		message.append("Android Version:")
				.append(android.os.Build.VERSION.RELEASE).append('\n');
		message.append("Board: ").append(android.os.Build.BOARD).append('\n');
		message.append("Brand: ").append(android.os.Build.BRAND).append('\n');
		message.append("Device: ").append(android.os.Build.DEVICE).append('\n');
		message.append("Host: ").append(android.os.Build.HOST).append('\n');
		message.append("ID: ").append(android.os.Build.ID).append('\n');
		message.append("Model: ").append(android.os.Build.MODEL).append('\n');
		message.append("Product: ").append(android.os.Build.PRODUCT)
				.append('\n');
		message.append("Type: ").append(android.os.Build.TYPE).append('\n');
		StatFs stat = getStatFs();
		message.append("Total Internal memory: ")
				.append(getTotalInternalMemorySize(stat)).append('\n');
		message.append("Available Internal memory: ")
				.append(getAvailableInternalMemorySize(stat)).append('\n');
	}

	@Override
	public void uncaughtException(Thread t, Throwable e) {
		try {
			//createDialogOnCrash();
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			LogUtil.e("CrashHandler", sw.toString());
			Intent intent = new Intent(mContext, GoodYear_eJob_MainActivity.class);
			intent.putExtra("CRASH_STATUS", "true");
			intent.putExtra("exit", true);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			mContext.startActivity(intent);
			System.exit(0);
			/*
			
			StringBuilder report = new StringBuilder();
			Date curDate = new Date();
			report.append("Error Report collected on : ")
					.append(curDate.toString()).append('\n').append('\n');
			report.append("Informations :").append('\n');
			addInformation(report);
			report.append('\n').append('\n');
			report.append("Stack:\n");
			final Writer result = new StringWriter();
			final PrintWriter printWriter = new PrintWriter(result);
			e.printStackTrace(printWriter);
			report.append(result.toString());
			printWriter.close();
			report.append('\n');
			report.append("**** End of current Report ***");
			Log.e(HandleUncaughtException.class.getName(),
					"Error while sendErrorMail" + report);
			sendErrorMail(report);*/
		} catch (Throwable ignore) {
			Log.e(HandleUncaughtException.class.getName(),
					"Error while sending error e-mail", ignore);
		}
		// previousHandler.uncaughtException(t, e);
	}

	/**
	 * This method will open the gmail composer when application crashed!
	 * 
	 */
	public void sendErrorMail(final StringBuilder errorContent) {
		if (CrashCommons.mHandleCrash == false) {
			System.out.println("" + errorContent);
			System.exit(0);
			return;
		}
		new Thread() {
			@Override
			public void run() {
				try {
					Looper.prepare();

					StringBuilder body = new StringBuilder(
							"Good Year APP Crashed");
					body.append('\n').append('\n');
					body.append(errorContent).append('\n').append('\n');

					String RECIPIENT1 = "naveenpnkumar@gmail.com";
					String RECIPIENT2 = "prashant.c.kumar@gmail.com";
					// String RECIPIENT3 = "nilesh.pawate@gmail.com";
					String RECIPIENT4 = "nilesh.mp@sonata-software.com";

					// String RECIPIENT1 = "";
					// String RECIPIENT2 = "";
					// // String RECIPIENT3 = "nilesh.pawate@gmail.com";
					// String RECIPIENT4 = "";

					String[] email = { RECIPIENT1, RECIPIENT2, RECIPIENT4 };
					Intent emailIntent = new Intent(Intent.ACTION_SEND);
					emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
					emailIntent.putExtra(Intent.EXTRA_SUBJECT,
							"GoodYear Application Crashed! Please Fix it!");
					emailIntent.setType("text/plain");
					emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
							body.toString());
					emailIntent.setType("message/rfc822");

					final PackageManager pm = mContext.getPackageManager();
					final List<ResolveInfo> matches = pm.queryIntentActivities(
							emailIntent, 0);
					ResolveInfo best = null;
					for (final ResolveInfo info : matches) {
						if (info.activityInfo.packageName.endsWith(".gm")
								|| info.activityInfo.name.toLowerCase()
										.contains("gmail")) {
							best = info;
						}
					}
					if (best != null) {
						emailIntent.setClassName(best.activityInfo.packageName,
								best.activityInfo.name);
					}
					emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					mContext.startActivity(emailIntent);
					System.exit(0);
					Looper.loop();
				} catch (Exception e) {
					System.out.println("Email didnt send");
					e.printStackTrace();
					System.exit(0);
				}
			}
		}.start();
	}
}
