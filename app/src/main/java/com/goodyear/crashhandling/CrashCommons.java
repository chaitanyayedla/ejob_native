/**
 * 
 */
package com.goodyear.crashhandling;

import android.content.Context;

/**
 * Common method to handle the crash issues. #mHandleCrash is to manage the
 * crash enable/disable.
 * 
 * @author Nilesh.Pawate
 * @version 1.0
 */
public class CrashCommons {
	/**
	 * boolean to manage crash handling. true to enable , false otherwise.
	 */
	public static boolean mHandleCrash = true;
	/**
	 * Activity Context
	 */
	private static Context mContext;
	/**
	 * Singleton Instance of CrashCommons
	 */
	private static CrashCommons mSmsInstance;

	/**
	 * Returns the instance of CrashCommons
	 * 
	 * @param contextContext
	 * @return CrashCommons
	 */
	public static CrashCommons getMyInstance(Context context) {
		mSmsInstance = new CrashCommons();
		CrashCommons.mContext = context;
		handleApplicationCrash();
		return mSmsInstance;
	}

	/**
	 * this is to handle application crash.
	 */

	public static void handleApplicationCrash() {
		if (mContext != null) {
			Thread.setDefaultUncaughtExceptionHandler(new HandleUncaughtException(
					mContext));
		}
	}
}
